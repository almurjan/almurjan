<?php

class test extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view("layouts/head");
        $this->load->view("layouts/header");
        $this->load->view("home");
        $this->load->view("layouts/footer");
        $this->load->view("layouts/footer_scripts");
    }

    public function about_us()
    {
        $this->load->view("layouts/head");
        $this->load->view("layouts/header");
        $this->load->view("about_us");
        $this->load->view("layouts/footer");
        $this->load->view("layouts/footer_scripts");
    }

    public function sectors()
    {
        $this->load->view("layouts/head");
        $this->load->view("layouts/header");
        $this->load->view("other_sectors");
        $this->load->view("layouts/footer");
        $this->load->view("layouts/footer_scripts");
    }

    public function rental_sector()
    {
        $this->load->view("layouts/head");
        $this->load->view("layouts/header");
        $this->load->view("rental_sector");
        $this->load->view("layouts/footer");
        $this->load->view("layouts/footer_scripts");
    }
}