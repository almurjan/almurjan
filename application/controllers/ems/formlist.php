<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Formlist extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        checkAdminSession();
}

	public function index() {

        $this->manage();

    }
	public function manage(){
		
	$this->load->model('ems/model_formlist');
	$rec['res']=$this->model_formlist->fetchRow();
		{
			$this->load->view('ems/formlist/edit',$rec);
		}
	}
  public function save(){

        $data['eng_city']=trim(implode(',',html_escape($this->input->post('eng_city'))));
        $data['arb_city']=trim(implode(',',html_escape($this->input->post('arb_city'))));
        $data['eng_category']=trim(implode(',',html_escape($this->input->post('eng_category'))));
        $data['arb_category']=trim(implode(',',html_escape($this->input->post('arb_category'))));
        $data['eng_department']=trim(implode(',',html_escape($this->input->post('eng_department'))));
        $data['arb_department']=trim(implode(',',html_escape($this->input->post('arb_department'))));
        $data['eng_source']=trim(implode(',',html_escape($this->input->post('eng_source'))));
        $data['arb_source']=trim(implode(',',html_escape($this->input->post('arb_source'))));
        $data['eng_level']=trim(implode(',',html_escape($this->input->post('eng_level'))));
        $data['arb_level']=trim(implode(',',html_escape($this->input->post('arb_level'))));
        $data['eng_gender']=trim(implode(',',html_escape($this->input->post('eng_gender'))));
        $data['arb_gender']=trim(implode(',',html_escape($this->input->post('arb_gender'))));
		$data['eng_time']=trim(implode(',',html_escape($this->input->post('eng_time'))));
        $data['arb_time']=trim(implode(',',html_escape($this->input->post('arb_time'))));

        $data['updated_at'] = date('Y-m-d');
   		$this->load->model('ems/model_formlist');
		$this->model_formlist->update($data);
		redirect($this->config->item('base_url') . 'ems/formlist/manage');
  }

    /*
        $data = array();
        $eng_category = trim(html_escape($this->input->post('eng_category')));
        $arb_category = trim(html_escape($this->input->post('arb_category')));
      
          $i=0;
          foreach($eng_category as $value)
          {
              $data['eng_category'] = $value;
              $data['arb_category'] = $arb_category[$i];
              $data['created_at'] = date('Y-m-d');
              $this->model_formlist->saveValues($data);
              $i++;
          }*/
	}
?>