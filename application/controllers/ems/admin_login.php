<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_login extends CI_Controller
{

    //public $layout = 'admin';

    function __construct()
    {
        parent::__construct();
        //$this->layout = 'admin';

    }

    public function set_admin_lang()
    {
        $admin_lang = $_POST['admin_lang'];
        if ($admin_lang) {
            $this->session->set_userdata(array('admin_lang' => $admin_lang));
        }
    }
    //main index function for the controller admin-login
    //loding the main view
    public function index()
    {
        $this->load->view('ems/admin_login');

    }

    public function get_pw($hash)
    {
        echo generatePassword($hash);
    }



    //query the credentials from the database
    //and add the user details in session in case of success
    //or redirect in case of wrong credentials
    function login()
    {
        $this->form_validation->set_rules('username', 'username', 'trim|required', '');
        $this->form_validation->set_rules('password', 'password', 'trim|required', '');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == FALSE) {
            redirect($this->config->item('base_url') . 'ems');
        } else {
            $this->load->model('ems/model_login');
            $hash_passwd = generatePassword(html_escape($this->input->post('password')));

            $result = $this->model_login->admin_login(html_escape($this->input->post('username')), $hash_passwd);

            if ($result) {

                foreach ($result as $row) {
                    $this->load->model('ems/model_cmsuser');
                    $site_name = $this->model_cmsuser->getsitename(1);
                    $this->session->set_userdata(array('logged_in' => TRUE, 'id' => $row->id, 'usr_uname' => $row->usr_uname, 'usr_level' => $row->usr_level, 'usr_grp_id' => $row->usr_grp_id, 'site_id' => '1', 'site_name' => $site_name));
                    $usr_level = $row->usr_level;

                    $this->db->query("update cms_users set wrong_attempts = 0 WHERE id = ".$row->id);

                }

                // user level restrction for Super user & normal cms user
                $this->session->set_userdata("usr_level", $usr_level);
                $grp = check_user_grp_id();

                if ($grp != '6') {
                    redirect($this->config->item('base_url') . 'ems/dashboard/manage');//for cms user
                } else {
                    redirect($this->config->item('base_url') . 'ems/mediclinic');//for cms user
                }


            } else {

                $user_exist_with_email = $this->model_login->user_exist_with_email(html_escape($this->input->post('username')));
                if ($user_exist_with_email) {
                    if ($user_exist_with_email->wrong_attempts >= 3) {
                        $this->session->set_flashdata('message', _erMsg2('<p>Your access is blocked. Kindly contact admin for further assistance.</p>'));
                        redirect($this->config->item('base_url') . 'ems');
                    } else {
                        $wrong_attempts = $user_exist_with_email->wrong_attempts + 1;
                        $sql = "update cms_users set wrong_attempts = ".$wrong_attempts." WHERE usr_email = '".html_escape($this->input->post('username'))."'";
                        $this->db->query($sql);
                        if ($wrong_attempts == 3) {
                            $this->db->query("update cms_users set usr_pub_status = 0 WHERE usr_email = '".html_escape($this->input->post('username'))."'");
                            $this->session->set_flashdata('message', _erMsg2('<p>Your access is blocked. Kindly contact admin for further assistance.</p>'));
                            redirect($this->config->item('base_url') . 'ems');
                        }
                    }
                }
                $this->session->set_flashdata('message', _erMsg2('<p>Invalid username or password.</p>'));
                redirect($this->config->item('base_url') . 'ems');
            }
        }
    }


    //function to logout the user
    function logout()
    {

        // $this->session->unset_userdata(array('logged_in' => FALSE, 'id' => '', 'usr_uname' => '','usr_level' => ''));
        $this->session->userdata = array();
        $this->session->sess_destroy();
        $this->session->sess_create();
        //$this->session->set_flashdata('message', _okMsg2('You have successfully logged out.'));
        redirect($this->config->item('base_url') . 'ems');
    }


    function forgot_password()
    {
        $lang = 'eng';
        $config_settings = getConfigurationSetting();
        $this->load->model('ems/model_configuration');
        $conf = $this->model_configuration->fetchRow();
        $this->load->model('ems/model_login');
        $res = $this->model_login->check_user(html_escape($this->input->post('name')), html_escape($this->input->post('email')));
        if ($res == 1) {
            $pass = generateRandomString();
            $hash_passwd = generatePassword($pass);
            $this->model_login->update_user(html_escape($this->input->post('name')), html_escape($this->input->post('email')), $hash_passwd);

            $fr = 'admin@gmail.com';
            $em = html_escape($this->input->post('email'));
            $this->load->library('email');

            /* $config['protocol'] = 'mail';
            $config['charset'] = 'iso-8859-1';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE; */


            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $conf->smtp_host, 'smtp_port' => $conf->smtp_port,
                'smtp_user' => $conf->smtp_email, 'smtp_pass' => $conf->smtp_password);

            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from($conf->smtp_email, ($lang == 'eng' ? $config_settings->project_name : $config_settings->project_name_arb) . ' Admin Panel');
            $this->email->to($em);
            $this->email->subject('Recover Password');
            $this->email->set_mailtype("html");
            $this->email->message($body);
            $this->email->send();


            $link = base_url() . 'ems';
            $message = '
						<style>
						@charset "utf-8";
						/* CSS Document */
						
						body{ width:100%; background:#fff; margin:0; padding:0; }
						
						.mainWrap{ width:600px; height:auto; margin:auto;}
						
						.topbar{ float:left; width:600px; height:20px; background:#0085b2;}
						
						.topLogoContainer{ float:left; width:600px; height:auto; padding:8px 0px 8px 0px;}
						
						.NesmaEmailLogo{ float:left; background:url(images/nesmaLogoEmailTemp.png) no-repeat; border:none; width:160px; height:53px;}
						
						.ColorBg{ background:#444444; width:440px; height:53px; float:left;}
						
						.descWrap{ width:580px; height:auto; margin-top:30px; padding:10px; background:#eeeeee; float:left;margin-bottom: 10px; }
						
						.title{ float:left; width:550px; height:auto; font-family:Arial, Helvetica, sans-serif; color:#666; font-size:12px; font-weight:bold;}
						
						.detail{ color: #333333;
							float: left;
							font-family: Arial,Helvetica,sans-serif;
							font-size: 12px;
						 
							height: auto;
							line-height: 1.6;
							padding-top: 10px;
							width: 575px;}
							
							.signature{float:left; width:550px; height:auto; font-family:Arial, Helvetica, sans-serif; color:#666; font-size:12px; font-weight:bold;}
							
							.footerStrip{ float:left; width:600px; height:30px; background:#444444;}
						
						</style>
						
						</head>
						
						<body>
						
						<div class="mainWrap">
						
						<div class="topbar"></div>
						
						
						<div class="topLogoContainer">
						
						<div class="NesmaEmailLogo"> </div>
						
						<div class="ColorBg"> </div>
						
						
						 </div>
						 
						 
						 
						 <div class="descWrap">
						 
						 
						 <div class="title"> Dear ' . html_escape($this->input->post('name')) . ' ,<br></div>
						 
						 Your new Password to access the system is generated by the system which is ' . $pass . '<br>
						 Now you can login to your account with new Password<br>' . $link . '</div>
						 <div class="signature"> Aljazira Capital </div>
						 </div>
						 
						<div class="footerStrip"> </div>
						</div>
						
						';

            $this->email->message($message);
            $this->email->send();


            $this->session->set_flashdata('message', _okMsg2('<p>Email Sent Successfully.</p>'));
            redirect($this->config->item('base_url') . 'ems');

        } else {

            $this->session->set_flashdata('message', _erMsg2('<p>Invalid username or email .</p>'));
            redirect($this->config->item('base_url') . 'ems');
        }
    }


}

/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */