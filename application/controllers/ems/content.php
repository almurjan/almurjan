<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        checkAdminSession();
		check_permission(8, 'any');
		$this->load->model('ems/model_contents');
		$this->load->model('ems/model_content_images');
        check_permission(8, 'any');
    }
     public function index() {
		check_permission(8, 'read');
		$this->manage();
    }
    public function manage() {
        check_permission(8, 'read');
		$data = array();
		$move_to_trash = 0;
        $data['pages'] = $this->model_contents->fetchAll($move_to_trash);
		
		$this->load->view('ems/contents/manage', $data);
    }
    public function gallerySorting() {

        $data = array();
        $move_to_trash = 0;

        $field_name = $this->uri->segment(4);;
        $id = $this->uri->segment(5);
        $result = $this->model_contents->getGalleryImages($id,$field_name);

        $result->meta_value = trim($result->meta_value, '___');
        $explodedValues = explode(',',trim($result->meta_value, ','));

        $i = 1;
        foreach($explodedValues as $explodedValueSingle){
            if(strpos($explodedValueSingle, '___') !== false && strlen($explodedValueSingle) > 15){
                $explodedValueTitle = explode('___',$explodedValueSingle);

                if($explodedValueTitle[1] == ''){
                    $explodedValueTitle[1] = $explodedValues[$i];
                }
                $titles[] = $explodedValueTitle[0];
                $images[] = $explodedValueTitle[1];

            } else{
                $titles[] = '';
                $images[] = $explodedValueSingle;
            }

            $i++;
        }

        $newTitles = array();
        foreach($titles as $singleTitle){
            if($singleTitle){
                $newTitles[] = $singleTitle;
            }
            else{
                $newTitles[] = '';
            }
        }
        foreach($images as $imageNew){
            if($imageNew !='' && $imageNew != '___'){
                $imageNews[] = $imageNew;
            }
        }

        /* if(isset($newTitles[0]) && $newTitles[0] == ''){
            unset($newTitles[0]);
        } */

        $data['parent_id'] 		= $result->post_id;
        $data['list_content'] 	= $imageNews;
        $data['titles'] 		= $newTitles;

        $this->load->view('ems/contents/manage_gallery', $data);
    }
    public function funds_reports() {
        
		$data = array();
		$move_to_trash = 0;
        $data['pages'] = $this->model_contents->fetchAll($move_to_trash);
		
		$this->load->view('ems/contents/manage_funds_reports', $data);
    }
	public function funds_announcements() {
        
		$data = array();
		$move_to_trash = 0;
        $data['pages'] = $this->model_contents->fetch_list_content_all();
		
		$this->load->view('ems/announcement_and_reports/manage', $data);
    }
	public function funds_reportings() {
        
		$data = array();
		$move_to_trash = 0;
        $data['pages'] = $this->model_contents->fetch_list_content_all();
		
		$this->load->view('ems/announcement_and_reports/manage', $data);
    } 
	
	public function services() {
        
		$data = array();
		$move_to_trash = 0;
        $data['pages'] = $this->model_contents->fetchAll($move_to_trash);
		
		$this->load->view('ems/contents/manage_services', $data);
    }
	public function mutual_funds() {
        
		$data = array();
		$move_to_trash = 0;
        $data['pages'] = $this->model_contents->fetchAll($move_to_trash);
		
		$this->load->view('ems/contents/manage_mutual_funds', $data);
    } 
	public function edit_mutual_funds() {
        
		$data = array();
		$id = $this->uri->segment(5);
		$data['result'] = $this->model_contents->fetchRow($id);
		$this->load->view('ems/contents/edit_mutual_funds', $data);
    } 
	public function reporting_and_disclosures() {
        
		$data = array();
		$move_to_trash = 0;
        $data['pages'] = $this->model_contents->fetchAll($move_to_trash);
		
		$this->load->view('ems/contents/manage_reports', $data);
    }
	
	public function versions() {
        
		$data = array();
        $id = $this->uri->segment(4);
		$data['versions'] = $this->model_contents->fetchAllVersion($id);
		
		$data['page_id'] = $id;
		$this->load->view('ems/contents/manageVersions', $data);
    }
	
	public function versionsListing() {
        
		$data = array();
        $id = $this->uri->segment(4);
		$data['versions'] = $this->model_contents->fetchAllVersion($id);
		
		$data['page_id'] = $id;
		$this->load->view('ems/contents/manageVersionsListing', $data);
    }
	
	public function trashPages() {
        
		$data = array();
		$move_to_trash = 1;
        $data['pages'] = $this->model_contents->fetchAll($move_to_trash);
		$this->load->view('ems/contents/manageTrashPages', $data);
    }
	
	 public function edit() {
        check_permission(8, 'update');
        //var_dump('here');die;
		$id = $this->uri->segment(7);
		if($id == ''){
		$id = $this->uri->segment(5); }
		$data = array();
		
		
       	$data['page_id'] = $this->uri->segment(5);
         check_page_permission($data['page_id'], 'allow');
        $data['result'] = $this->model_contents->fetchRow($id);
		if($data['template']=='contact-us' || true){
			$this->load->library('google_maps');
				$map_cordinates = content_detail('location',$data['result']->id);
				$config['center'] = $map_cordinates;
				$map_cordinates = explode(',',$map_cordinates);
				$config['zoom'] = '5';
				$config['onclick'] = '

				var lat = event.latLng.lat();
				var lng = event.latLng.lng();
				$("#location").val(lat+","+ lng);

				$("#overlay_map").fadeOut(500);
				';
				$this->google_maps->initialize($config);
				
				$data['map']= $this->google_maps->create_map();
		}
		$data['total_versions'] =  $this->model_contents->countPageVersions($id);
		$this->load->view('ems/contents/edit', $data);
    }

   
	public function addNewPage() {

        check_permission(8, 'create');
		if($data['template']=='contact-us' || true){
			$this->load->library('google_maps');

			$config['center'] = '37.4419, -122.1419';
			$config['zoom'] = '5';
			$config['onclick'] = '
			
			var lat = event.latLng.lat();
					var lng = event.latLng.lng();
			$("#location").val(lat+","+ lng);
			
			$("#overlay_map").fadeOut(500);
			';
			$this->google_maps->initialize($config);
			
			$data['map'] = $this->google_maps->create_map();
		}
	
       		$this->load->view('ems/contents/add', $data);
	 }
   
	public function addNewPageCustom() {
		
		
		if($data['template']=='contact-us' || true){
			$this->load->library('google_maps');

			$config['center'] = '37.4419, -122.1419';
			$config['zoom'] = '5';
			$config['onclick'] = '
			
			var lat = event.latLng.lat();
					var lng = event.latLng.lng();
			$("#location").val(lat+","+ lng);
			
			$("#overlay_map").fadeOut(500);
			';
			$this->google_maps->initialize($config);
			
			$data['map'] = $this->google_maps->create_map();
		}
	
       		$this->load->view('ems/contents/add_new', $data);
	 }
   
     public function savePage() {
         check_permission(8, 'create');
		$admin_lang = check_admin_lang();
		$title =addslashes(html_escape($this->input->post('eng_title')));
		$check_title = $this->model_contents->checkPageTitleRepeat($title,'');

		$data = array();
        $loggedInUserId = $this->session->userdata('id');
		if($check_title>0){
            $data['is_flag'] = 1;
        }else{
            $data['is_flag'] = 0;
        }
        $data['eng_title'] = addslashes(html_escape($this->input->post('eng_title')));
		$data['arb_title'] = addslashes(html_escape($this->input->post('arb_title')));
		$data['eng_sub_title'] = addslashes(html_escape($this->input->post('eng_sub_title')));
		$data['arb_sub_title'] = addslashes(html_escape($this->input->post('arb_sub_title')));
		$data['chn_title'] = addslashes(html_escape($this->input->post('chn_title')));
		
        if ($this->input->post('display_to_home')) $data['display_to_home'] = addslashes(html_escape($this->input->post('display_to_home')));

        $data['meta_title_eng'] = addslashes(html_escape($this->input->post('meta_title_eng')));
        $data['meta_desc_eng'] = addslashes(html_escape($this->input->post('meta_desc_eng')));
        $data['meta_keywords_eng'] = addslashes(html_escape($this->input->post('meta_keywords_eng')));
		

        $data['meta_title_arb'] = addslashes(html_escape($this->input->post('meta_title_arb')));
        $data['meta_desc_arb'] = addslashes(html_escape($this->input->post('meta_desc_arb')));
        $data['meta_keywords_arb'] = addslashes(html_escape($this->input->post('meta_keywords_arb')));
		
		$data['meta_title_chn'] = addslashes(html_escape($this->input->post('meta_title_chn')));
        $data['meta_desc_chn'] = addslashes(html_escape($this->input->post('meta_desc_chn')));
        $data['meta_keywords_chn'] = addslashes(html_escape($this->input->post('meta_keywords_chn')));

		$data['date'] = addslashes(html_escape($this->input->post('date')));
		$data['cms_country'] = addslashes(html_escape($this->input->post('cms_country')));
		$data['cms_city'] = addslashes(html_escape($this->input->post('cms_city')));
		$data['start_date'] = addslashes(html_escape($this->input->post('start_date')));
		$data['end_date'] = addslashes(html_escape($this->input->post('end_date')));
         if ($this->input->post('archive')) $data['archive'] = addslashes(html_escape($this->input->post('archive')));
		//$data['color_code'] = addslashes(html_escape($this->input->post('color_code')));
		$data['eng_location'] = addslashes(html_escape($this->input->post('location')));
        $data['arb_location'] = addslashes(html_escape($this->input->post('arb_location')));
         if ($this->input->post('city')) $data['city'] = addslashes(html_escape($this->input->post('city')));
        $data['category'] = addslashes(html_escape($this->input->post('category')));
         if ($this->input->post('is_show_home')) $data['is_show_home'] = addslashes(html_escape($this->input->post('is_show_home')));
        $data['show_other_news'] = (addslashes(html_escape($this->input->post('show_other_news')))?addslashes(html_escape($this->input->post('show_other_news'))):0);
         if ($this->input->post('year')) $data['year'] = addslashes(html_escape($this->input->post('year')));
         if ($this->input->post('is_archieve')) $data['is_archieve'] = addslashes(html_escape($this->input->post('is_archieve')));
        $data['country_name'] = addslashes(html_escape($this->input->post('country_name')));
        $data['country_name_ar'] = addslashes(html_escape($this->input->post('country_name_ar')));
         if ($this->input->post('category_id')) $data['category_id'] = addslashes(html_escape($this->input->post('category_id')));
        $data['sectors_logo_sorting'] = addslashes(html_escape($this->input->post('eng_sectors_logo_sorting')));
         if ($this->input->post('show_tags')) $data['show_tags'] = addslashes(html_escape($this->input->post('show_tags')));
		//product multi-select
		$multiProductArray = html_escape($this->input->post('multiple_select'));
		$multi_cat = implode(",",$multiProductArray);
		$data['multi_category'] = $multi_cat;
		unset($_POST['multiple_select']);
		//Hidden Field
        $data['listing_level'] = $listing_level = addslashes(html_escape($this->input->post('listing_level')));
         if ($this->input->post('is_benefits')) $data['is_benefits'] = addslashes(html_escape($this->input->post('is_benefits')));
         if ($this->input->post('selectSlideOptions')) $data['selectSlideOptions'] = addslashes(html_escape($this->input->post('selectSlideOptions')));
	    $data['parant_id'] = addslashes(html_escape($this->input->post('tpl_id')));
		$redirect_check = $data['parant_id'];
		if(html_escape($this->input->post('tpl')) == 'profile' && $listing_level>0){
            $data['companies_listing'] = 1 ;
        }else{
            $data['companies_listing'] = 0 ;
        }
		/* if($data['parant_id'] == 391){
			$list_content = $this->model_contents->fetch_list_content($data['parant_id']);
			if($list_content){
				$i=0;
				foreach ($list_content as $result => $val) {
					$result = $this->model_contents->publishStatus(0, $val->id);
				}
			}
		} */
		
		$data['tpl'] = addslashes(html_escape($this->input->post('tpl')));
		$data['version'] = 0 ;
		$data['type'] = 'page';	
        $data['pub_status'] = 1;
        $data['content_created_at'] = date('Y-m-d H:i:s');
		$data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_created_by'] = $loggedInUserId;
		
		
		if (html_escape($this->input->post('tpl')) == 'home') {

			if ($_FILES['file']['name'] != '') {
				
				$file_name_eng =  date('Ymdhsi') . '-' . $_FILES['file']['name'];
				$file_size1 = $_FILES['file']['size'];
				$file_tmp_eng = $_FILES['file']['tmp_name'];
				$type1 = $_FILES['file']['type'];
				
				move_uploaded_file($file_tmp_eng, "uploads/videos/" . $file_name_eng);
				
				$data['file'] = $file_name_eng;
			} else {
				$empty = "";
				$data['file'] = html_escape($this->input->post('file_hdn'));
			}

			if ($_FILES['file_arb']['name'] != '') {

				$file_name_eng =  date('Ymdhsi') . '-' . $_FILES['file_arb']['name'];
				$file_size1 = $_FILES['file_arb']['size'];
				$file_tmp_eng = $_FILES['file_arb']['tmp_name'];
				$type1 = $_FILES['file_arb']['type'];
				move_uploaded_file($file_tmp_eng, "uploads/videos/" . $file_name_eng);
				$data['file_arb'] = $file_name_eng;
			} else {
				$empty = "";
				$data['file_arb'] = html_escape($this->input->post('file_arb_hdn'));
			}
		}
		
		if (html_escape($this->input->post('tpl')) == 'faqs') {
			
			if ($_FILES['file']['name'] != '') {
			
				$file_name_eng =  date('Ymdhsi') . '-' . $_FILES['file']['name'];
				$file_size1 = $_FILES['file']['size'];
				$file_tmp_eng = $_FILES['file']['tmp_name'];
				$type1 = $_FILES['file']['type'];
				move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
				
				$data['file'] = $file_name_eng;
			} else {
				$empty = "";
				$data['file'] = html_escape($this->input->post('file_hdn'));
			}

			if ($_FILES['file_arb']['name'] != '') {

				$file_name_eng =  date('Ymdhsi') . '-' . $_FILES['file_arb']['name'];
				$file_size1 = $_FILES['file_arb']['size'];
				$file_tmp_eng = $_FILES['file_arb']['tmp_name'];
				$type1 = $_FILES['file_arb']['type'];
				move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
				$data['file_arb'] = $file_name_eng;
			} else {
				$empty = "";
				$data['file_arb'] = html_escape($this->input->post('file_arb_hdn'));
			}
		}
		foreach($data as $key => $value)
		{
			if($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response')
			{
				$value = removeJsScripts($value);
				$data[$key] = $value;	
			}
		}
		
		/*echo '<pre>'; print_r($data);exit();*/
        $id = $this->model_contents->save($data);
		
		foreach($_POST as $key => $val){

			$ar = array(
			'meta_title_eng','meta_desc_eng',
			'meta_keywords_eng','meta_title_arb',
			'meta_desc_arb','meta_keywords_arb','meta_title_chn',
			'meta_desc_chn','meta_keywords_chn','tpl','pub_status','eng_title','eng_sub_title','arb_title','arb_sub_title','chn_title','tpl_id','display_to_home','eng_pro_date','fleet_day','eng_location','arb_location','cv_pdf_file','event_document_file','event_document_file_arb','archive','job_type','career_level','career_location','country_id','multiple_select');
			if(!in_array($key ,$ar) and !empty($val)){
			
			$arr = array(
			'post_id' => $id,
			'meta_key' => $key,
			'meta_value' => removeJsScripts($val)
				
			);
		
		if(strpos($arr['meta_key'],"_mkey_hdn") !== false){
			$image_explode = explode('script/',$arr['meta_value']); 
			$arr['meta_value'] = $image_explode[1];
			$arr['meta_value'] = str_replace(',','',$arr['meta_value']);
			}
		
			$arr['meta_value'] = str_replace('src="../../','src="../../../../',$arr['meta_value']);	
				
		$this->model_contents->save_content_detail($arr);
			}
		}
		
		$data['type'] = 'version';
		$data['parant_id'] = $id ;
		$data['version'] = 1 ;
		//$data['content_updated_at'] = date('Y-m-d H:i:s');
       // $data['content_updated_by'] = $loggedInUserId;
		$data['content_created_at'] = date('Y-m-d H:i:s');
		$data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_created_by'] = $loggedInUserId;
		
		 $version_id = $this->model_contents->save($data);
		 $this->saveVersionData($_POST,$version_id);
		
        log_insert($this->uri->segment(2), 'add a record in');
        if ($version_id > 0) {
            $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_saved']."</b></p>"));
        }
		$lang = $admin_lang['admin_lang'];
		
		if($data['tpl'] == 'news_room')
		{
			$data['name'] = 'DTC';
			if ($lang == 'eng'){
				$subject = 'News Room added with a news!'; 
				$title 	 = 'New news added in news room'; 
				
				$subject2 = 'News Room added with a news!'; 
				$title2 	 = 'New news added in news room';
			}else{
				$subject = 'تم استلام نموذج التواصل الخاص بك'; 
				$title   = 'تم ارسال نموذج بنجاح';
				
				$subject2 = 'اتصل بنا عبر البريد الإلكتروني'; 
				$title2   = 'اتصل بنا عبر البريد الإلكتروني'; 
			}
			
			$dataNew['title'] = $data['eng_title'];
			$dataNew['date']  = $_POST['date'];
			
			adminGeneralEmail($dataNew,$subject2,$title2,$lang,'');	
		}
		$listing_level = html_escape($this->input->post('listing_level'));
		$is_benefits = html_escape($this->input->post('is_benefits'));

		$dataRight['page_id'] = $id;
		$dataRight['grp_id'] = 1;
		$dataRight['allow'] = 1;
		$this->model_contents->saveRightOfNewPage($dataRight);
		
		
		if($data['tpl'] == 'home' || html_escape($this->input->post('tpl_name')) == 'home' || html_escape($this->input->post('tpl')) == 'history' || html_escape($this->input->post('tpl_name')) == 'history' || html_escape($this->input->post('tpl')) == 'ceo_clients' || html_escape($this->input->post('tpl_name')) == 'ceo_clients' || html_escape($this->input->post('tpl')) == 'our_services' || html_escape($this->input->post('tpl_name')) == 'our_services' || html_escape($this->input->post('tpl')) == 'careers' || html_escape($this->input->post('tpl_name')) == 'careers' || html_escape($this->input->post('tpl')) == 'faqs' || html_escape($this->input->post('tpl_name')) == 'faqs' || html_escape($this->input->post('tpl')) == 'contact_us' || html_escape($this->input->post('tpl_name')) == 'contact_us' || html_escape($this->input->post('tpl')) == 'media_center' || html_escape($this->input->post('tpl_name')) == 'media_center' || html_escape($this->input->post('tpl')) == 'news' || html_escape($this->input->post('tpl_name')) == 'news' || html_escape($this->input->post('tpl')) == 'events' || html_escape($this->input->post('tpl_name')) == 'events'  || html_escape($this->input->post('tpl_name')) == 'bod' || html_escape($this->input->post('tpl')) == 'bod'  || html_escape($this->input->post('tpl_name')) == 'our_approach'  || html_escape($this->input->post('tpl')) == 'news_room' || html_escape($this->input->post('tpl_name')) == 'landmarks' || html_escape($this->input->post('tpl_name')) == 'other_sectors' || html_escape($this->input->post('tpl_name')) == 'murjan_holding' || html_escape($this->input->post('tpl_name')) == 'profile' || html_escape($this->input->post('tpl_name')) == 'murjan_group' || html_escape($this->input->post('tpl')) == 'murjan_group' || html_escape($this->input->post('tpl_name')) == 'companies_map' || html_escape($this->input->post('tpl')) == 'companies_map' || html_escape($this->input->post('tpl_name')) == 'csr' || html_escape($this->input->post('tpl')) == 'csr'){
			if($redirect_check == ''){
			    if(html_escape($this->input->post('tpl_name')) == 'other_sectors' && $listing_level!=''){
                    redirect($this->config->item('base_url') . 'ems/list_content/edit/id/'.$id);
                }else{
                    redirect($this->config->item('base_url') . 'ems/content/edit/id/'.$id);
                }
			}
			else{
                if (html_escape($this->input->post('tpl_name')) == 'careers') {
                    $is_benefits= '/'.html_escape($this->input->post('is_benefits'));
                }elseif (html_escape($this->input->post('tpl_name')) == 'landmarks' || html_escape($this->input->post('tpl_name')) == 'csr' || (html_escape($this->input->post('tpl_name')) == 'profile' && $listing_level=='')) {
                    $is_benefits= '';
                }elseif (html_escape($this->input->post('tpl')) == 'profile' && $listing_level > 0) {
                    $is_benefits= '/1';
                }else{
                    $is_benefits= '/0';
                }
				redirect($this->config->item('base_url') . 'ems/list_content/edit/id/'.$id.''.$is_benefits);
			}
		}else{
			redirect($this->config->item('base_url') . 'ems/content/edit/id/'.$id);
		}
    }
	
	
	private function mp4VideoUpload($n,$fileName,$videoName){
		
		if($videoName != ''){
			$ext = pathinfo($fileName['name'], PATHINFO_EXTENSION);
			
            $file_name_eng = 'video-'.date('Ymdhsi').'.'.$ext;
            $file_size1 = $fileName['size'];
            $file_tmp_eng = $fileName['tmp_name'];
            $type1 = $fileName['type'];
            move_uploaded_file($file_tmp_eng, "uploads/videos/" . $file_name_eng);
            return $file_name_eng;

        }

    }
     public function savePageVideo() {
		/* $data = array();
		foreach($_FILES['album_videos']['name'] as $key =>$val){
			
			$dataNew['name'] = $val;
			$dataNew['type'] = $_FILES['album_videos']['type'][$key];
			$dataNew['tmp_name'] = $_FILES['album_videos']['tmp_name'][$key];
			
			$mp4File = $this->mp4VideoUpload($key, $dataNew, 'video'.$key);
            $data['video_links'][] = $mp4File;
            //$dataNew  = '';
		}
		$data['video_links'] = implode(',',$data['video_links']);
		 */
		$admin_lang = check_admin_lang();
		
        $loggedInUserId = $this->session->userdata('id');
		
        $data['eng_title'] = addslashes(html_escape($this->input->post('eng_title')));
		$data['arb_title'] = addslashes(html_escape($this->input->post('arb_title')));
		$data['eng_sub_title'] = addslashes(html_escape($this->input->post('eng_sub_title')));
		$data['arb_sub_title'] = addslashes(html_escape($this->input->post('arb_sub_title')));
		$data['chn_title'] = addslashes(html_escape($this->input->post('chn_title')));
		
        if ($this->input->post('display_to_home')) $data['display_to_home'] = addslashes(html_escape($this->input->post('display_to_home')));

        $data['meta_title_eng'] = addslashes(html_escape($this->input->post('meta_title_eng')));
        $data['meta_desc_eng'] = addslashes(html_escape($this->input->post('meta_desc_eng')));
        $data['meta_keywords_eng'] = addslashes(html_escape($this->input->post('meta_keywords_eng')));
		

        $data['meta_title_arb'] = addslashes(html_escape($this->input->post('meta_title_arb')));
        $data['meta_desc_arb'] = addslashes(html_escape($this->input->post('meta_desc_arb')));
        $data['meta_keywords_arb'] = addslashes(html_escape($this->input->post('meta_keywords_arb')));
		
		$data['meta_title_chn'] = addslashes(html_escape($this->input->post('meta_title_chn')));
        $data['meta_desc_chn'] = addslashes(html_escape($this->input->post('meta_desc_chn')));
        $data['meta_keywords_chn'] = addslashes(html_escape($this->input->post('meta_keywords_chn')));

		//$data['date'] = addslashes(html_escape($this->input->post('date')));
		$data['cms_country'] = addslashes(html_escape($this->input->post('cms_country')));
		$data['cms_city'] = addslashes(html_escape($this->input->post('cms_city')));
		$data['start_date'] = addslashes(html_escape($this->input->post('start_date')));
		$data['end_date'] = addslashes(html_escape($this->input->post('end_date')));
		$data['archive'] = addslashes(html_escape($this->input->post('archive')));
		//$data['color_code'] = addslashes(html_escape($this->input->post('color_code')));
		$data['eng_location'] = addslashes(html_escape($this->input->post('location')));
        $data['arb_location'] = addslashes(html_escape($this->input->post('arb_location')));
        $data['city'] = addslashes(html_escape($this->input->post('city')));
        $data['category'] = addslashes(html_escape($this->input->post('category')));

		 
	    $data['parant_id'] = addslashes(html_escape($this->input->post('tpl_id')));
		$redirect_check = $data['parant_id'];
		
		if($data['parant_id'] == 391){
			$list_content = $this->model_contents->fetch_list_content($data['parant_id']);
			if($list_content){
				$i=0;
				foreach ($list_content as $result => $val) {
					$result = $this->model_contents->publishStatus(0, $val->id);
				}
			}
		}
		
		if(html_escape($this->input->post('tpl_name')) == 'blog'){
			$data['tpl'] = addslashes('blog_listing');
		}
		else if (html_escape($this->input->post('tpl_name')) == 'video_album') {
           $data['tpl'] = addslashes('video_album_listing');
        }
		else if (html_escape($this->input->post('tpl_name')) == 'media') {
           $data['tpl'] = addslashes('media_detail_images');
        }
	    else{
			$data['tpl'] = addslashes(html_escape($this->input->post('tpl')));
		}

		$data['version'] = 0 ;
		
		$data['type'] = 'page';	
	
        $data['pub_status'] = 1;
        $data['content_created_at'] = date('Y-m-d H:i:s');
		$data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_created_by'] = $loggedInUserId;

		 if ($_FILES['file']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file']['name'];
			 $file_size1 = $_FILES['file']['size'];
			 $file_tmp_eng = $_FILES['file']['tmp_name'];
			 $type1 = $_FILES['file']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file'] = $file_name_eng;
		 }
		 if ($_FILES['file_arb']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file_arb']['name'];
			 $file_size1 = $_FILES['file_arb']['size'];
			 $file_tmp_eng = $_FILES['file_arb']['tmp_name'];
			 $type1 = $_FILES['file_arb']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file_arb'] = $file_name_eng;
		 }

		 //echo '<pre>'; print_r($data);exit();

        $id = $this->model_contents->save($data);
		
		foreach($_POST as $key => $val){

			$ar = array(
			'meta_title_eng','meta_desc_eng',
			'meta_keywords_eng','meta_title_arb',
			'meta_desc_arb','meta_keywords_arb','meta_title_chn',
			'meta_desc_chn','meta_keywords_chn','tpl','pub_status','eng_title','eng_sub_title','arb_title','arb_sub_title','chn_title','tpl_id','display_to_home','eng_pro_date','fleet_day','eng_location','arb_location','cv_pdf_file','event_document_file','event_document_file_arb','archive','job_type','career_level','career_location','country_id');
			if(!in_array($key ,$ar) and !empty($val)){
			
			$arr = array(
			'post_id' => $id,
			'meta_key' => $key,
			'meta_value' => $val
				
			);
		
		if(strpos($arr['meta_key'],"_mkey_hdn") !== false){
			$image_explode = explode('script/',$arr['meta_value']); 
			$arr['meta_value'] = $image_explode[1];
			$arr['meta_value'] = str_replace(',','',$arr['meta_value']);
			}
		
			$arr['meta_value'] = str_replace('src="../../','src="../../../../',$arr['meta_value']);	
				
		$this->model_contents->save_content_detail($arr);
			}
		}
		
		 
		
		$data['type'] = 'version';
		$data['parant_id'] = $id ;
		$data['version'] = 1 ;
		//$data['content_updated_at'] = date('Y-m-d H:i:s');
       // $data['content_updated_by'] = $loggedInUserId;
		$data['content_created_at'] = date('Y-m-d H:i:s');
		$data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_created_by'] = $loggedInUserId;
		
		 $version_id = $this->model_contents->save($data);
		 $this->saveVersionData($_POST,$version_id);
		
        log_insert($this->uri->segment(2), 'add a record in');
        if ($version_id > 0) {
            $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_saved']."</b></p>"));
        }

		if($data['tpl'] == 'home' || html_escape($this->input->post('tpl_name')) == 'home' ||  html_escape($this->input->post('tpl')) == 'latest_news' || html_escape($this->input->post('tpl_name')) == 'media_center' || html_escape($this->input->post('tpl')) == 'media_center'){
			
			
			 if($redirect_check == ''){

				redirect($this->config->item('base_url') . 'ems/content/edit/id/'.$id); 
				 }else{
				 redirect($this->config->item('base_url') . 'ems/list_content/edit_video/id/'.$id);
					 }

        
				 }else{
		redirect($this->config->item('base_url') . 'ems/content/edit/id/'.$id);
				 }
    }
	
	public function savePageProject() {
		/* $data = array();
		foreach($_FILES['album_videos']['name'] as $key =>$val){
			
			$dataNew['name'] = $val;
			$dataNew['type'] = $_FILES['album_videos']['type'][$key];
			$dataNew['tmp_name'] = $_FILES['album_videos']['tmp_name'][$key];
			
			$mp4File = $this->mp4VideoUpload($key, $dataNew, 'video'.$key);
            $data['video_links'][] = $mp4File;
            //$dataNew  = '';
		}
		$data['video_links'] = implode(',',$data['video_links']);
		 */
		$admin_lang = check_admin_lang();
		
        $loggedInUserId = $this->session->userdata('id');
		
        $data['eng_title'] = addslashes(html_escape($this->input->post('eng_title')));
		$data['arb_title'] = addslashes(html_escape($this->input->post('arb_title')));
		$data['eng_sub_title'] = addslashes(html_escape($this->input->post('eng_sub_title')));
		$data['arb_sub_title'] = addslashes(html_escape($this->input->post('arb_sub_title')));
		$data['chn_title'] = addslashes(html_escape($this->input->post('chn_title')));
		
        if ($this->input->post('display_to_home')) $data['display_to_home'] = addslashes(html_escape($this->input->post('display_to_home')));

        $data['meta_title_eng'] = addslashes(html_escape($this->input->post('meta_title_eng')));
        $data['meta_desc_eng'] = addslashes(html_escape($this->input->post('meta_desc_eng')));
        $data['meta_keywords_eng'] = addslashes(html_escape($this->input->post('meta_keywords_eng')));
		

        $data['meta_title_arb'] = addslashes(html_escape($this->input->post('meta_title_arb')));
        $data['meta_desc_arb'] = addslashes(html_escape($this->input->post('meta_desc_arb')));
        $data['meta_keywords_arb'] = addslashes(html_escape($this->input->post('meta_keywords_arb')));
		
		$data['meta_title_chn'] = addslashes(html_escape($this->input->post('meta_title_chn')));
        $data['meta_desc_chn'] = addslashes(html_escape($this->input->post('meta_desc_chn')));
        $data['meta_keywords_chn'] = addslashes(html_escape($this->input->post('meta_keywords_chn')));

		//$data['date'] = addslashes(html_escape($this->input->post('date')));
		$data['cms_country'] = addslashes(html_escape($this->input->post('cms_country')));
		$data['cms_city'] = addslashes(html_escape($this->input->post('cms_city')));
		$data['start_date'] = addslashes(html_escape($this->input->post('start_date')));
		$data['end_date'] = addslashes(html_escape($this->input->post('end_date')));
		$data['archive'] = addslashes(html_escape($this->input->post('archive')));
		//$data['color_code'] = addslashes(html_escape($this->input->post('color_code')));
		$data['eng_location'] = addslashes(html_escape($this->input->post('location')));
        $data['arb_location'] = addslashes(html_escape($this->input->post('arb_location')));
        $data['city'] = addslashes(html_escape($this->input->post('city')));
        $data['category'] = addslashes(html_escape($this->input->post('category')));
        $data['is_project'] = addslashes(html_escape($this->input->post('is_project')));
        $data['is_feature'] = addslashes(html_escape($this->input->post('is_feature')));

		 
	    $data['parant_id'] = addslashes(html_escape($this->input->post('tpl_id')));
		$redirect_check = $data['parant_id'];
		
		if($data['parant_id'] == 391){
			$list_content = $this->model_contents->fetch_list_content($data['parant_id']);
			if($list_content){
				$i=0;
				foreach ($list_content as $result => $val) {
					$result = $this->model_contents->publishStatus(0, $val->id);
				}
			}
		}
		
		if(html_escape($this->input->post('tpl_name')) == 'blog'){
			$data['tpl'] = addslashes('blog_listing');
		}
		else if (html_escape($this->input->post('tpl_name')) == 'video_album') {
           $data['tpl'] = addslashes('video_album_listing');
        }
		else if (html_escape($this->input->post('tpl_name')) == 'media') {
           $data['tpl'] = addslashes('media_detail_images');
        }
	    else{
			$data['tpl'] = addslashes(html_escape($this->input->post('tpl')));
		}

		$data['version'] = 0 ;
		
		$data['type'] = 'page';	
	
        $data['pub_status'] = 1;
        $data['content_created_at'] = date('Y-m-d H:i:s');
		$data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_created_by'] = $loggedInUserId;

		 if ($_FILES['file']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file']['name'];
			 $file_size1 = $_FILES['file']['size'];
			 $file_tmp_eng = $_FILES['file']['tmp_name'];
			 $type1 = $_FILES['file']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file'] = $file_name_eng;
		 }
		 if ($_FILES['file_arb']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file_arb']['name'];
			 $file_size1 = $_FILES['file_arb']['size'];
			 $file_tmp_eng = $_FILES['file_arb']['tmp_name'];
			 $type1 = $_FILES['file_arb']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file_arb'] = $file_name_eng;
		 }

		 //echo '<pre>'; print_r($data);exit();

        $id = $this->model_contents->save($data);
		
		foreach($_POST as $key => $val){

			$ar = array(
			'meta_title_eng','meta_desc_eng',
			'meta_keywords_eng','meta_title_arb',
			'meta_desc_arb','meta_keywords_arb','meta_title_chn',
			'meta_desc_chn','meta_keywords_chn','tpl','pub_status','eng_title','eng_sub_title','arb_title','arb_sub_title','chn_title','tpl_id','display_to_home','eng_pro_date','fleet_day','eng_location','arb_location','cv_pdf_file','event_document_file','event_document_file_arb','archive','job_type','career_level','career_location','country_id');
			if(!in_array($key ,$ar) and !empty($val)){
			
			$arr = array(
			'post_id' => $id,
			'meta_key' => $key,
			'meta_value' => $val
				
			);
		
		if(strpos($arr['meta_key'],"_mkey_hdn") !== false){
			$image_explode = explode('script/',$arr['meta_value']); 
			$arr['meta_value'] = $image_explode[1];
			$arr['meta_value'] = str_replace(',','',$arr['meta_value']);
			}
		
			$arr['meta_value'] = str_replace('src="../../','src="../../../../',$arr['meta_value']);	
				
		$this->model_contents->save_content_detail($arr);
			}
		}
		
		 
		
		$data['type'] = 'version';
		$data['parant_id'] = $id ;
		$data['version'] = 1 ;
		//$data['content_updated_at'] = date('Y-m-d H:i:s');
       // $data['content_updated_by'] = $loggedInUserId;
		$data['content_created_at'] = date('Y-m-d H:i:s');
		$data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_created_by'] = $loggedInUserId;
		
		 $version_id = $this->model_contents->save($data);
		 $this->saveVersionData($_POST,$version_id);
		
        log_insert($this->uri->segment(2), 'add a record in');
        if ($version_id > 0) {
            $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_saved']."</b></p>"));
        }

		if($data['tpl'] == 'home' || html_escape($this->input->post('tpl_name')) == 'home' ||  html_escape($this->input->post('tpl')) == 'latest_news' || html_escape($this->input->post('tpl_name')) == 'media_center' || html_escape($this->input->post('tpl')) == 'media_center'){
			
			
			 if($redirect_check == ''){

				redirect($this->config->item('base_url') . 'ems/list_content/edit_content_project/id/'.$id); 
				 }else{
				 redirect($this->config->item('base_url') . 'ems/list_content/edit_content_project/id/'.$id);
					 }

        
				 }else{
		redirect($this->config->item('base_url') . 'ems/list_content/edit_content_project/id/'.$id);
				 }
    }

    public function savePageCategory() {
        $admin_lang = check_admin_lang();

        $loggedInUserId = $this->session->userdata('id');

        $data['category_title'] = addslashes(html_escape($this->input->post('eng_title')));
        $data['category_title_ar'] = addslashes(html_escape($this->input->post('arb_title')));
        $data['category_desc_eng'] = addslashes(html_escape($this->input->post('category_desc_eng')));
        $data['category_desc_arb'] = addslashes(html_escape($this->input->post('category_desc_arb')));
        $data['parant_id'] = addslashes(html_escape($this->input->post('tpl_id')));

        $id = $this->model_contents->saveCategory($data);

        log_insert($this->uri->segment(2), 'add a record in');

        redirect($this->config->item('base_url') . 'ems/list_content/edit_content_category/id/'.$id);
    }
	
	public function saveVersionData($data,$id){
		foreach($data as $key => $val){
			$ar = array(
			'meta_title_eng','meta_desc_eng',
			'meta_keywords_eng','meta_title_arb',
			'meta_desc_arb','meta_keywords_arb','meta_title_chn',
			'meta_desc_chn','meta_keywords_chn','tpl','pub_status','eng_title','eng_sub_title','arb_title','arb_sub_title','chn_title','tpl_id','display_to_home','eng_pro_date','fleet_day','eng_location','arb_location','cv_pdf_file','event_document_file','event_document_file_arb','country_id');
			if(!in_array($key ,$ar) and !empty($val)){
			
			$arr = array(
			'post_id' => $id,
			'meta_key' => $key,
			'meta_value' => $val
				
			);
		
		if( strpos($arr['meta_key'],"_mkey_hdn") !== false){
			$image_explode = explode('script/',$arr['meta_value']); 
			$arr['meta_value'] = $image_explode[1];
			$arr['meta_value'] = str_replace(',','',$arr['meta_value']);
			}
		
			$arr['meta_value'] = str_replace('src="../../','src="../../../../',$arr['meta_value']);	
				
		$this->model_contents->save_content_detail($arr);
			}
		}
		
		}
   
    public function update() {
        check_permission(8, 'update');
		$admin_lang = check_admin_lang();
        $id = html_escape($this->input->post('page_id'));
        $tpl_id = html_escape($this->input->post('tpl_id'));
        $title = addslashes(html_escape($this->input->post('eng_title')));
		$data = array();
        $loggedInUserId = $this->session->userdata('id');
        #todo:: think about new logic to repeated flag for it in edit
        //$check_title = $this->model_contents->checkPageTitleRepeat($title,$id);
        $check_title = 0;

        $data = array();
        $loggedInUserId = $this->session->userdata('id');
        /*if($check_title>0){
            $data['is_flag'] = 1;
        }else{
            $data['is_flag'] = 0;
        }*/
		//unset(html_escape($this->input->post('date')));

        $data['eng_title'] = addslashes(html_escape($this->input->post('eng_title')));
		$data['arb_title'] = addslashes(html_escape($this->input->post('arb_title')));
		$data['eng_sub_title'] = stripslashes(html_escape($this->input->post('eng_sub_title')));
		$data['arb_sub_title'] = addslashes(html_escape($this->input->post('arb_sub_title')));
		$data['chn_title'] = addslashes(html_escape($this->input->post('chn_title')));

        if ($this->input->post('display_to_home')) $data['display_to_home'] = addslashes(html_escape($this->input->post('display_to_home')));

        $data['meta_title_eng'] = addslashes(html_escape($this->input->post('meta_title_eng')));
        $data['meta_desc_eng'] = addslashes(html_escape($this->input->post('meta_desc_eng')));
        $data['meta_keywords_eng'] = addslashes(html_escape($this->input->post('meta_keywords_eng')));

		$data['date'] = addslashes(html_escape($this->input->post('date')));
		$data['cms_country'] = addslashes(html_escape($this->input->post('cms_country')));
		$data['cms_city'] = addslashes(html_escape($this->input->post('cms_city')));
		$data['start_date'] = addslashes(html_escape($this->input->post('start_date')));
		$data['end_date'] = addslashes(html_escape($this->input->post('end_date')));

		if ($this->input->post('archive')) $data['archive'] = addslashes(html_escape($this->input->post('archive')));
		//$data['color_code'] = addslashes(html_escape($this->input->post('color_code')));
		$data['eng_location'] = addslashes(html_escape($this->input->post('location')));
        $data['arb_location'] = addslashes(html_escape($this->input->post('arb_location')));

        $data['meta_title_arb'] = addslashes(html_escape($this->input->post('meta_title_arb')));
        $data['meta_desc_arb'] = addslashes(html_escape($this->input->post('meta_desc_arb')));
        $data['meta_keywords_arb'] = addslashes(html_escape($this->input->post('meta_keywords_arb')));
		
		$data['meta_title_chn'] = addslashes(html_escape($this->input->post('meta_title_chn')));
        $data['meta_desc_chn'] = addslashes(html_escape($this->input->post('meta_desc_chn')));
        $data['meta_keywords_chn'] = addslashes(html_escape($this->input->post('meta_keywords_chn')));
		if ($this->input->post('city')) $data['city'] = addslashes(html_escape($this->input->post('city')));
        $data['category'] = addslashes(html_escape($this->input->post('category')));
        if ($this->input->post('is_archieve')) $data['is_archieve'] = addslashes(html_escape($this->input->post('is_archieve')));
        if ($this->input->post('is_benefits')) $data['is_benefits'] = addslashes(html_escape($this->input->post('is_benefits')));
        $data['show_other_news'] = (addslashes(html_escape($this->input->post('show_other_news')))?addslashes(html_escape($this->input->post('show_other_news'))):0);
        if ($this->input->post('year')) $data['year'] = addslashes(html_escape($this->input->post('year')));
        $data['country_name'] = addslashes(html_escape($this->input->post('country_name')));
        $data['country_name_ar'] = addslashes(html_escape($this->input->post('country_name_ar')));
        if ($this->input->post('category_id')) $data['category_id'] = addslashes(html_escape($this->input->post('category_id')));
        $data['sectors_logo_sorting'] = addslashes(html_escape($this->input->post('eng_sectors_logo_sorting')));
        if ($this->input->post('selectSlideOptions')) $data['selectSlideOptions'] = addslashes(html_escape($this->input->post('selectSlideOptions')));

		//product multi-select
		$multiProductArray = html_escape($this->input->post('multiple_select'));
		$multi_cat = implode(",",$multiProductArray);
		$data['multi_category'] = $multi_cat;
		unset($_POST['multiple_select']);
		
		//Hidden Field
        $data['listing_level'] = $listing = addslashes(html_escape($this->input->post('listing_level')));
		
		//file_upload($_FILES,$id);
		//$data['parant_id'] = addslashes(html_escape($this->input->post('tpl_id')));
		//$redirect_check = $data['parant_id'];
		$data['type'] = 'page';
		
		$data['tpl'] = addslashes(html_escape($this->input->post('tpl')));
		
		foreach($data as $key => $value)
		{
			if($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response')
			{
				$value = removeJsScripts(addslashes($value));
				$data[$key] = $value;	
			}
		}
		if (html_escape($this->input->post('tpl')) == 'home') {

			if ($_FILES['file']['name'] != '') {
			
				$file_name_eng =  date('Ymdhsi') . '-' . $_FILES['file']['name'][0];
				$file_size1 = $_FILES['file']['size'][0];
				$file_tmp_eng = $_FILES['file']['tmp_name'][0];
				$type1 = $_FILES['file']['type'][0];
				move_uploaded_file($file_tmp_eng, "uploads/videos/" . $file_name_eng);
				
				$data['file'] = $file_name_eng;
			} else {
				$empty = "";
				$data['file'] = html_escape($this->input->post('file_hdn'));
			}

			if ($_FILES['file_arb']['name'] != '') {

				$file_name_eng =  date('Ymdhsi') . '-' . $_FILES['file_arb']['name'];
				$file_size1 = $_FILES['file_arb']['size'][0];
				$file_tmp_eng = $_FILES['file_arb']['tmp_name'][0];
				$type1 = $_FILES['file_arb']['type'][0];
				move_uploaded_file($file_tmp_eng, "uploads/videos/" . $file_name_eng);
				$data['file_arb'] = $file_name_eng;
			} else {
				$empty = "";
				$data['file_arb'] = html_escape($this->input->post('file_arb_hdn'));
			}
		}	
		if (html_escape($this->input->post('tpl')) == 'faqs') {
			
			if ($_FILES['file']['name'] != '') {
			
				$file_name_eng =  date('Ymdhsi') . '-' . $_FILES['file']['name'];
				$file_size1 = $_FILES['file']['size'];
				$file_tmp_eng = $_FILES['file']['tmp_name'];
				$type1 = $_FILES['file']['type'];
				move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
				
				$data['file'] = $file_name_eng;
			} else {
				$empty = "";
				$data['file'] = html_escape($this->input->post('file_hdn'));
			}

			if ($_FILES['file_arb']['name'] != '') {

				$file_name_eng =  date('Ymdhsi') . '-' . $_FILES['file_arb']['name'];
				$file_size1 = $_FILES['file_arb']['size'];
				$file_tmp_eng = $_FILES['file_arb']['tmp_name'];
				$type1 = $_FILES['file_arb']['type'];
				move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
				$data['file_arb'] = $file_name_eng;
			} else {
				$empty = "";
				$data['file_arb'] = html_escape($this->input->post('file_arb_hdn'));
			}
		}
		
        $data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_updated_by'] = $loggedInUserId;
        $res['result'] = $this->model_contents->update($data, $id);
		$data['parant_id'] = addslashes(html_escape($this->input->post('tpl_id')));
		$redirect_check = $data['parant_id'];
		$data['tpl'] = addslashes(html_escape($this->input->post('tpl')));

	    $this->model_contents->delete_content_detail($id);
	    //unset($_POST['date']);
	  
		foreach($_POST as $key => $val){

			$ar = array(
			'meta_title_eng','meta_desc_eng',
			'meta_keywords_eng','meta_title_arb',
			'meta_desc_arb','meta_keywords_arb','meta_title_chn',
			'meta_desc_chn','meta_keywords_chn','tpl','pub_status','eng_title','eng_sub_title','arb_title','arb_sub_title','chn_title','tpl_id','display_to_home','eng_pro_date','fleet_day','eng_location','arb_location','cv_pdf_file','event_document_file','event_document_file_arb','archive','job_type','career_level','career_location','country_id','multiple_select');
			if(!in_array($key ,$ar) and !empty($val)){
			
            $arr = array(
            'post_id' => $id,
            'meta_key' => $key,
            'meta_value' => removeJsScripts($val)

            );
		if( strpos($arr['meta_key'],"_mkey_hdn") !== false){
			$image_explode = explode('script/',$arr['meta_value']); 
			$arr['meta_value'] = $image_explode[1];
			$arr['meta_value'] = str_replace(',','',$arr['meta_value']);
			}
			$arr['meta_value'] = str_replace('src="../../','src="../../../../',$arr['meta_value']);	

		   $this->model_contents->save_content_detail($arr);
			}
		}

		$countVersion =  $this->model_contents->countPageVersions($id);
		
		$data['version'] = $countVersion + 1;
		$data['type'] = 'version';
		$data['pub_status'] = 0;
		$data['parant_id'] = $id;
		$version_id = $this->model_contents->save($data);
        $this->saveVersionData($_POST,$version_id);
		log_insert($this->uri->segment(2), 'edit a record in');
        $is_benefits =html_escape($this->input->post('is_benefits'));
        if ($res == true) {
            $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_updated']."</b></p>"));
        }
		if($data['tpl'] == 'home' || html_escape($this->input->post('tpl_name')) == 'home' || html_escape($this->input->post('tpl_name')) == 'history' || html_escape($this->input->post('tpl_name')) == 'history' || html_escape($this->input->post('tpl_name')) == 'ceo_clients' || html_escape($this->input->post('tpl_name')) == 'ceo_clients' || html_escape($this->input->post('tpl_name')) == 'our_services' || html_escape($this->input->post('tpl_name')) == 'our_services' || html_escape($this->input->post('tpl_name')) == 'careers' || html_escape($this->input->post('tpl_name')) == 'careers' || html_escape($this->input->post('tpl_name')) == 'faqs' || html_escape($this->input->post('tpl_name')) == 'faqs' || html_escape($this->input->post('tpl_name')) == 'contact_us' || html_escape($this->input->post('tpl_name')) == 'contact_us' || html_escape($this->input->post('tpl_name')) == 'projects' || html_escape($this->input->post('tpl_name')) == 'projects' || html_escape($this->input->post('tpl_name')) == 'media_center' || html_escape($this->input->post('tpl_name')) == 'media_center' || html_escape($this->input->post('tpl_name')) == 'news' || html_escape($this->input->post('tpl_name')) == 'news' || html_escape($this->input->post('tpl_name')) == 'events' || html_escape($this->input->post('tpl_name')) == 'events' || html_escape($this->input->post('tpl_name')) == 'bod' || html_escape($this->input->post('tpl_name')) == 'our_approach' || html_escape($this->input->post('tpl')) == 'news_room' || html_escape($this->input->post('tpl_name')) == 'landmarks' || html_escape($this->input->post('tpl_name')) == 'other_sectors' || html_escape($this->input->post('tpl_name')) == 'murjan_holding' || html_escape($this->input->post('tpl_name')) == 'profile' || html_escape($this->input->post('tpl_name')) == 'murjan_group' || html_escape($this->input->post('tpl')) == 'murjan_group' || html_escape($this->input->post('tpl_name')) == 'companies_map' || html_escape($this->input->post('tpl')) == 'companies_map' || html_escape($this->input->post('tpl_name')) == 'csr' || html_escape($this->input->post('tpl')) == 'csr'){
			
			 if($redirect_check == ''){
				 $this->session->set_flashdata('historygobackmain', _okMsg("go back to main listing screen"));
				redirect($this->config->item('base_url') . 'ems/content/edit/id/'.$id); 
				 }else{
						if(html_escape($this->input->post('tpl_name')) == 'careers' ){
							redirect($this->config->item('base_url') . 'ems/list_content/edit/id/'.$id.'/'.$is_benefits);
						}elseif(html_escape($this->input->post('tpl_name')) == 'profile' && $listing > 0){
                            redirect($this->config->item('base_url') . 'ems/list_content/edit/id/'.$id.'/'.$listing);
                        }else{
							redirect($this->config->item('base_url') . 'ems/list_content/edit/id/'.$id);
						}
						redirect($this->config->item('base_url') . 'ems/list_content/edit/id/'.$id);
					 }

				 }else{
					redirect($this->config->item('base_url') . 'ems/content/edit/id/'.$id);
				 }
    }
	
	public function getCities()
    {
		$this->load->model('ems/model_inquries');
		
        $data = array();
        $result = array();
        $citiesList = '';
        $region_id = html_escape($this->input->post('id'));
		$admin_lang = check_admin_lang();
		$lang = $admin_lang['admin_lang'];
        
		$cities = $this->model_inquries->fetchAllRegionsCities($region_id);
		
        if (count($cities) > 0) {
            /*$citiesList .= '<option value="" selected>' . ($lang == 'eng' ? 'Please Select' : 'اختر من فضلك') . '</option>';*/
            foreach ($cities as $city) {
                $citiesList .=  $city->id . '|' . ($lang == 'eng' ? $city->name : $city->name_arb) . ',';
            }
        }
        $result['cities'] = rtrim($citiesList, ',');
        $result['error'] = 'false';
        echo json_encode($result);
		exit();
    }
	public function getFourthLevelListing()
    {
		$this->load->model('ems/model_inquries');
		
        $data = array();
        $result = array();
        $services = '';
        $service_id = html_escape($this->input->post('id'));
		$admin_lang = check_admin_lang();
		$lang = $admin_lang['admin_lang'];
        $l = 0;
		$subSubPagesListing = ListingContent($service_id);
		
        if (count($subSubPagesListing) > 0) {
            foreach ($subSubPagesListing as $city) {
                $services .=  $city->id . '|' . ($lang == 'eng' ? $city->eng_title : $city->arb_title) . ',';
            }
        }
        $result['services'] = rtrim($services, ',');
        $result['error'] = 'false';
        echo json_encode($result);
		exit();
    }
	
	public function getCitiesSelected()
    {
		$this->load->model('ems/model_inquries');
		
		$city_id = $this->uri->segment(4);
        $data = array();
        $result = array();
        $citiesList = '';
        $region_id = html_escape($this->input->post('id'));
		$admin_lang = check_admin_lang();
		$lang = $admin_lang['admin_lang'];
        
		$cities = $this->model_inquries->fetchAllRegionsCities($region_id);
		
        if (count($cities) > 0) {
            /*$citiesList .= '<option value="" selected>' . ($lang == 'eng' ? 'Please Select' : 'اختر من فضلك') . '</option>';*/
            foreach ($cities as $city) {
                $citiesList .=  $city->id . '|' . ($lang == 'eng' ? $city->name : $city->name_arb) . ',';
            }
        }
        $result['cities'] = rtrim($citiesList, ',');
        $result['error'] = 'false';
        echo json_encode($result);
		exit();
    }
	
    public function publish() {
        check_permission(8, 'publish');
        $this->layout = '';
        $this->load->model('ems/model_contents');
        $id = $_POST['id'];
        $status = $_POST['pub_status'];
        $result = $this->model_contents->publishStatus($status, $id);
    }
	
	public function checkUniquePageTitle() {
        $this->layout = '';
        $this->load->model('ems/model_contents');
        $title = $_POST['page_title'];
      echo $result = $this->model_contents->checkUniquePageTitle($title).'@@'; exit;
    }
	public function checkUniquePageTitleEdit($id) {
        $this->layout = '';
        $this->load->model('ems/model_contents');
        $title = $_POST['page_title'];
      echo $result = $this->model_contents->checkUniquePageTitleEdit($title,$id).'@@'; exit;
    }
	
	 public function trash() {
        $this->layout = '';
        $id = $_POST['id'];
		$data['move_to_trash'] = $_POST['move_to_trash'];
        log_insert($this->uri->segment(2), 'move to trash in');
		$result = $this->model_contents->update($data, $id);
        echo 'true';
    }
	 public function delete() {
         check_permission(8, 'delete');
        $this->layout = '';
		
         $id = $_POST['id'];
         $page_details = get_content_data($id);
         if($page_details->is_flag!=1){
             $title = pageTitle($id,'eng');
             $repeatedDatas = $this->model_contents->RepeatedPagesDetail($title,$id);
             if($repeatedDatas){
                 $i=1;
                 foreach($repeatedDatas as $repeatedData){

                     if($i==1){
                         $update_data=array(
                            "is_flag"=>0
                         );
                         $update = $this->model_contents->update($update_data,$repeatedData->id);
                     }
                     $i++;
                 }
             }
         }/*else{
             $title = pageTitle($id,'eng');
             $repeatedDatas = $this->model_contents->RepeatedButNotIsFlag($title,$id);
             if($repeatedDatas){
                 $i=1;
                 foreach($repeatedDatas as $repeatedData){

                     if($i==1){
                         $update_data=array(
                             "is_flag"=>0
                         );
                         $update = $this->model_contents->update($update_data,$repeatedData->id);
                     }
                     $i++;
                 }
             }
         }*/
		log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_contents->delete($id);
       echo 'true';
    }
    public function categoryDelete() {
        check_permission(8, 'delete');
        $this->layout = '';

        $id = $_POST['id'];
        $page_details = get_content_data($id);

        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_contents->categoryDelete($id);
        echo 'true';
    }
	 public function deleteImage() {
        $this->layout = '';
		$id = $_POST['id'];
		$imageName = $_POST['image_name'];
		$imageType = $_POST['image_type'];
       
		log_insert($this->uri->segment(2), 'delete a record in');
     
	   $photos =  content_detail($imageType,$id);
	   
	   $photo = str_replace($imageName,'',$photos);
	    $result = $this->model_contents->deleteImage($photo,$imageType,$id);
        echo 'true' ;
    }
	
	public function sortable() {
	
		if(isset($_POST['id']) and !empty($_POST['id'])){

			$id = $_POST['id'];

			for ($i = 0; $i < count($_POST['id']); $i++) {
			
				$data = array(
						"sort_order" => $i,
						);
				
				$result = $this->model_contents->update($data, $_POST['id'][$i]);
			}

		}
	}
	
	public function sortableImageGallery() {
		
		/* echo "<pre>";
		print_r($_REQUEST);
		exit; */
		$admin_lang = check_admin_lang();
		$field_name = $_REQUEST['field'];
		$id = $_REQUEST['id'];
		$field_values = str_replace('|',',',$_REQUEST['field_values']);
		
		$field_values = str_replace('[object Object]','',rtrim($field_values, ','));
		//$field_values = str_replace('___qwe','',$field_values);
		
		$result = $this->model_contents->updateGalleryImages($field_values, $id,$field_name);
		$this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_saved']."</b></p>"));
		
		$result2['field_values'] = $field_values;
        $result2['error'] = 'false';
        echo json_encode($result2);
		exit();
	}
}

/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */