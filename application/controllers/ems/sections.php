<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start();
class sections extends CI_Controller {
	public $layout = 'admin_inner';
	function __construct() {
		parent::__construct();
		$this->layout = 'admin_inner';
		$this->load->model('model_custom');
		$this->load->library('email','excel');
		checkAdminSession();
		check_permission(18, 'any');
	}
	//main index function for the controller dashboard
	//loding the main view
	public function index(){
		check_permission(18, 'read');
		//$this->load->view('ems/inquries/manage');
$this->manage();
	}
	public function manage(){
		check_permission(18, 'read');
		$res='';
		$this->load->model('ems/model_inquries');
		$data['res']=$this->model_inquries->fetchAllSections();
		if(sizeof($data)>0)
		{
			$this->load->view('ems/sections/manage',$data);
		}
	}
	public function cities(){
		check_permission(18, 'read');
		$id = $this->uri->segment(5);
		
		$this->load->model('ems/model_inquries');
		$data['res'] = $this->model_inquries->fetchAllRegionsCities($id);
		
		$this->load->view('ems/sections/manage_cities',$data);
	}
	
	public function add() {
		check_permission(18, 'create');
        $data = array();
		//$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/sections/add', $data);
    }
	
	public function addRegionCities() {
		check_permission(18, 'create');
        $data = array();
		//$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/sections/add_city', $data);
    }
	
	public function manageQuotes(){
  $res='';
  $this->load->model('ems/model_inquries');
  $data['res']=$this->model_inquries->fetchAllQuotes();
  if(sizeof($data)>0)
  {
   $this->load->view('ems/inquries/manageQuotes',$data);
  }
 }
 
 public function viewQuote()
 { //Ckeditor's configuration
  $id = $this->uri->segment(5);
  if($id)
  {
   $this->load->model('ems/model_inquries');
   $data['res']=$this->model_inquries->fetchQuote($id);
   //echo "<pre>";print_r($data);echo "</pre>";exit;
   if($data!=false)
   {
    $this->load->view('ems/inquries/viewQuote',$data);
   }
  }
  else {
   $this->load->view('ems/inquries/viewQuote');
  }
 }
 
  public function deleteQuote()
 {
	
  $this->layout = '';
  $this->load->model('ems/model_inquries');
  $id = $_POST['id'];
  $result=$this->model_inquries->deleteQuote($id);
 }
	public function view()
	{ //Ckeditor's configuration
		
		$id = $this->uri->segment(5);
		if($id)
		{
			$this->load->model('ems/model_inquries');
			$data['res']=$this->model_inquries->fetchRow($id);
                        //echo "<pre>";print_r($data);echo "</pre>";exit;
			if($data!=false)
			{
				$this->load->view('ems/inquries/view',$data);
			}
		}
		else {
				$this->load->view('ems/inquries/manage');
		}
	}
	public function publish()
	{
		check_permission(18, 'publish');
		$this->load->model('ems/model_inquries');
		$id = $_POST['id'];
		$status= $_POST['pub_status'];
		$result=$this->model_inquries->publishStatus($status,$id);
	}
	public function update() {
		check_permission(18, 'update');
        $this->load->model('ems/model_inquries');
        $id = $this->uri->segment(5); 
		
		$data = array();
        $data['name'] = 	removeJsScripts(html_escape($this->input->post('name'))); 
        $data['name_arb'] = removeJsScripts(html_escape($this->input->post('name_arb'))); 
        $data['pub_status'] = html_escape($this->input->post('pub_status')); 
		
		
		$res['result'] = $this->model_inquries->updateRegion($data, $id);
		log_insert($this->uri->segment(2), 'edit a record in');
        
        if ($res == true) {
            $this->session->set_flashdata('message', _okMsg('<p>Record Updated Successfully.</p>'));
        }
        redirect($this->config->item('base_url') . 'ems/sections/edit/id/'.$id);
    }
	public function updateRegionCity() {
		check_permission(18, 'update');
        $this->load->model('ems/model_inquries');
        $id = $this->uri->segment(5); 
		
		$data = array();
        $data['name'] = removeJsScripts(html_escape($this->input->post('name'))); 
        $data['name_arb'] = removeJsScripts(html_escape($this->input->post('name_arb'))); 
        //$data['pub_status'] = html_escape($this->input->post('pub_status')); 
		unset($data['pub_status']);
		$res['result'] = $this->model_inquries->updateRegionCity($data, $id);
		log_insert($this->uri->segment(2), 'edit a record in');
        
        if ($res == true) {
            $this->session->set_flashdata('message', _okMsg('<p>Record Updated Successfully.</p>'));
        }
        redirect($this->config->item('base_url') . 'ems/sections/edit_city/id/'.$id);
    }
	
	public function edit() {
        check_permission(18, 'update');
		$id = $this->uri->segment(5); 
		
		$this->load->model('ems/model_inquries');
		
		$data['result'] = $this->model_inquries->fetchRegion($id);
		
		$this->load->view('ems/sections/edit', $data);
    }
	
	public function edit_city() {
        check_permission(18, 'update');
		$id = $this->uri->segment(5); 
		
		$this->load->model('ems/model_inquries');
		
		$data['result'] = $this->model_inquries->fetchRegionCity($id);
		
		$this->load->view('ems/sections/edit_city', $data);
    }
	
	
	public function delete()
	{
		check_permission(18, 'delete');
		$this->layout = '';
		$this->load->model('ems/model_inquries');
		$id = $_POST['id'];
		$result=$this->model_inquries->deleteRegion($id);
	}
	
	public function deleteCity()
	{
		check_permission(18, 'delete');
		$this->layout = '';
		$this->load->model('ems/model_inquries');
		$id = $_POST['id'];
		$result=$this->model_inquries->deleteRegionCity($id);
	}
	
	public function save() {
        
		check_permission(18, 'create');
		$this->load->model('ems/model_inquries');
		$data = array();
		$loggedInUserId = $this->session->userdata('id');
		$data = $this->input->post();
		foreach($data as $key => $value)
		{
			if($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response')
			{
				$value = removeJsScripts($value);
				$data[$key] = $value;	
			}
		}
		$insert_id = $this->model_inquries->saveRegions($data);
		
		log_insert($this->uri->segment(2), 'add a record in');
		$this->session->set_flashdata('message', _okMsg('<p>Region added successfully.</p>'));
        redirect($this->config->item('base_url') . 'ems/sections');
    }
	
	public function saveRegionCity() {
        check_permission(18, 'create');
		$this->load->model('ems/model_inquries');
		$data = array();
		$loggedInUserId = $this->session->userdata('id');
		$data = $this->input->post();
		foreach($data as $key => $value)
		{
			if($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response')
			{
				$value = removeJsScripts($value);
				$data[$key] = $value;	
			}
		}
		unset($data['pub_status']);
		$insert_id = $this->model_inquries->saveRegionsCity($data);
		
		log_insert($this->uri->segment(2), 'add a record in');
		$this->session->set_flashdata('message', _okMsg('<p>Added successfully.</p>'));
        redirect($this->config->item('base_url') . 'ems/sections/cities/id/'.$data['region_id']);
    }
	
	/* 
	public function export_excel()
    {
		$this->load->library('excel');	
		$id_arr = array();
        if(html_escape($this->input->post('excel_type')) == 0)
        {
			$table = 'reservation_info';
            $user_ids = $this->model_custom->fetchAll_Ids($table);
            foreach($user_ids as $val)
            {
                $id_arr[] = $val->id;	
            }
        }
        else
        {
            $id_arr = html_escape($this->input->post('id'));
        }	
		$i=0;
		$users_arr = array();
        foreach($id_arr as $id)
        {
			$table = 'reservation_info';
            $users_arr[] = $this->model_custom->fetch_single($id,$table);	
			$i++;
        }
        $this->gen_xl($users_arr, 'users-excel'); 	
	} */
	
	public function export_excel()
    {
		$this->load->model('ems/model_inquries');
		$this->load->library('excel');	
		$id_arr = array();
        if(html_escape($this->input->post('excel_type')) == 0)
        {
			$table = 'contacts';
            $user_ids = $this->model_custom->fetchAll_Ids($table);
            foreach($user_ids as $val)
            {
                $id_arr[] = $val->id;	
            }
        }
        else
        {
            $id_arr = html_escape($this->input->post('id'));
        }	
		$i=0;
		$users_arr = array();
        foreach($id_arr as $id)
        {
			$table = 'contacts';
            $users_arr[] = $this->model_custom->fetch_single($id,$table);	
			$i++;
        }
		
		$a2=$this->model_inquries->fetchAllUsersExcel();
		foreach($a2 as $a2_new){
			$new_array[] = (object)$a2_new;
		}
		$users_arr=array_merge($users_arr,$new_array);
		
        $this->gen_xl($users_arr, 'contacts-excel'); 	
	}
	
	
	public function gen_xl($rr, $filename){
			$this->excel->getProperties()->setCreator("Soumya Biswas")
				 ->setLastModifiedBy("Soumya Biswas")
				 ->setTitle("Office 2007 XLSX Test Document")
				 ->setSubject("Office 2007 XLSX Test Document")
				 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
				 ->setKeywords("office 2007 openxml php")
				 ->setCategory("Test result file");				
		$border = array(
			'borders' => array(
			'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		); 				
		$ans = array(
		'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					  )
		);	 		 				 
		$arr = array(
			'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
		),
		'font'  => array(
			'bold'  => true,
			"color" => array("rgb" => "903")
			)
		);
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('35');
		$this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($border);
		$this->excel->getActiveSheet()->setCellValue('A2', 'Name');
		$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('A2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('35');
		$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue('B2', 'Email');
		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('B2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('35');
		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($border);
		$this->excel->getActiveSheet()->setCellValue('C2', 'Comments');
		$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('C2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($border);
	
		 $n = 2;
		 foreach($rr as $ss){
		 $n++;
        									
		$this->excel->getActiveSheet()->setCellValue('A'.$n, $ss->name);
		$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($border);
		
		
		$this->excel->getActiveSheet()->setCellValue('B'.$n, $ss->email);
		$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue( 'C' .$n, $ss->comments);
		$this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('c'.$n)->applyFromArray($border);
					
   } 
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$filename=$filename.".xls";
		
		$folder_path = '/home/public_html/uploads/exports/';
        $file = $folder_path.$filename;
		//Setting the header type
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
        $this->manage();
		
	}
}
/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */