<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Category extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        $this->load->model('ems/model_category');
		$this->load->library('email');
        checkAdminSession();
    }

    public function index() {
        $this->manage();
    }

    public function manage() {
          $data = array();
          $data['user_data'] = $this->model_category->fetchAll('category');
          $this->load->view('ems/category/manage', $data);
    }
    public function add() {

        $this->load->view('ems/category/add');
    }

    public function edit() {
        $data = array();
        $id = $this->uri->segment(4);
        
		if ($id) {
            $data['user_data'] = $this->model_category->fetch($id,'category');
			 if (!empty($data)) {
                $this->load->view('ems/category/edit', $data);
            }
        }
    }

    public function save() {

        $data = array();

        $data['eng_name']  = html_escape($this->input->post('eng_name'));
        $data['arb_name']  = html_escape($this->input->post('arb_name'));
       
		$insert_id = $this->model_category->saveData($data);
        $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_saved']."</b></p>"));
        redirect($this->config->item('base_url').'ems/category/edit/'.$insert_id);

    }
    public function update() {

        $data = array();

        $id  = html_escape($this->input->post('id'));
        $data['eng_name']  = html_escape($this->input->post('eng_name'));
        $data['arb_name']  = html_escape($this->input->post('arb_name'));
       
		$insert_id = $this->model_category->updateData($id, $data);
        $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_saved']."</b></p>"));
        redirect($this->config->item('base_url').'ems/category/edit/'.$id);

    }
    public function updateUser() {

        $id = $_POST['id'];
        $data = array();
        $data['full_name']  = $name = html_escape($this->input->post('full_name'));
        $data['username']  = $username = html_escape($this->input->post('user_name'));
        $data['password']  = $password = html_escape($this->input->post('password'));
        $data['email']  = $email = html_escape($this->input->post('email'));

        if ($_FILES['staff_image']['name'] != '') {
            $file_name_eng =  date('Ymdhsi').'-'.$_FILES['staff_image']['name'];
            $file_size1 = $_FILES['staff_image']['size'];
            $file_tmp_eng = $_FILES['staff_image']['tmp_name'];
            $type1 = $_FILES['staff_image']['type'];
            move_uploaded_file($file_tmp_eng, "uploads/staff/" . $file_name_eng);
            $data1['staff_image'] = $file_name_eng;
        }else{
            $data1['staff_image'] = html_escape($this->input->post('hidden_image1'));
        }

        $data['updated_at'] = date('Y-m-d H:i:s');

        $insert_id = $this->model_category->updateData($id, $data);
        if ($insert_id)
        {
            $this->session->set_flashdata('message', _okMsg('<p>Detail updated successfully.</p>'));
            redirect($this->config->item('base_url') . 'ems/category/edit/id/'.$id);

        }
    }

    public function view() {
        $data = array();
        $id = $this->uri->segment(5);
        if ($id) {
            $data['user_data'] = $this->model_category->fetch($id,'motoon_logins');
            if (!empty($data)) {
                $this->load->view('ems/category/view', $data);
            }
        }
    }

    public function publish()
    {
        $this->layout = '';
        $this->load->model('ems/model_category');
        $id = $_POST['id'];
        $status= $_POST['pub_status'];
        $result=$this->model_category->publishStatus($status,$id);
    }

    public function delete() {
        $lang = $this->session->userdata('lang');
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_category->delete($id);
        echo $result;
        // $this->deletePhotos($id);
    }
    //Images end

    public function activeStatus()
    {
        $this->layout = '';
        $this->load->model('ems/model_configuration');
        $this->load->model('ems/model_category');
        $id = $_POST['id'];
        $status= $_POST['pub_status'];
        $result=$this->model_category->activeStatus($status,$id);
    }

    public function check_valid_email(){
        $email = $_POST['email'];
        $results = $this->model_category->checkEmail($email);
        if($results == '1'){
            die('<img src="'.base_url().'assets/images/not-available.png" /><p>This Email is already Existing.</p>');
        }else{
            die('<img src="'.base_url().'assets/images/available.png" />');
        }
    }

    public function send_email_staff($to, $subject, $message)
    {
        $this->load->model('ems/model_configuration');
        $conf = $this->model_configuration->fetchRow();

        /*$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'engmohsinshaikh@gmail.com',
            'smtp_pass' => 'password'
        );
        $this->load->library('email', $config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from($conf->from_email, 'Metro');
        /*$this->email->to($to);
        $this->email->to("ahsan@astutesol.com");
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        }*/

        $headers  = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Motoon <motoon@motoon.ed.sa>' . "\r\n";
        mail($to,$subject,$message,$headers);
    }
}
/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */