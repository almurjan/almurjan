<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start();

class request_quotation extends CI_Controller {



	public $layout = 'admin_inner';







	function __construct() {

		parent::__construct();

		$this->layout = 'admin_inner';
		$this->load->model('model_custom');
		$this->load->library('email','excel');
		checkAdminSession();
        check_permission(20, 'any');

	}



	//main index function for the controller dashboard

	//loding the main view

	public function index(){





		//$this->load->view('ems/inquries/manage');

$this->manage();



	}



	public function manage(){
        check_permission(20, 'read');
		$date_request = '';
		$country = '';
		$city = '';
		$services = '';
		
		if(isset($_GET['show_all']) && $_GET['show_all'] = 'clear'){
			unset($_COOKIE['date_request']);
			unset($_COOKIE['country']);
			unset($_COOKIE['city']);
			unset($_COOKIE['services']);
			
			setcookie('date_request', null, -1, '/');
			setcookie('country', null, -1, '/');
			setcookie('services', null, -1, '/');
			setcookie('city', null, -1, '/');
		}
		
		$users='';
		
		if(isset($_COOKIE['date_request'])){
			$date_request = $_COOKIE['date_request'];
			
		}
		
		if(isset($_COOKIE['country'])){
			$country = $_COOKIE['country'];
		}
		
		if(isset($_COOKIE['city'])){
			$city = $_COOKIE['city'];	
			
		}
		
		if(isset($_COOKIE['services'])){
			$services = $_COOKIE['services'];
		}

		$this->load->model('ems/model_contents');

		$data['users']=$this->model_contents->fetchQuotation($date_request,$country,$city,$services);
		
		$data['cities']=$this->model_contents->fetchUniqueCities();

		if(sizeof($data)>0)

		{

			$this->load->view('ems/request_quotation/manage',$data);

		}

			

	}
	
	public function manageQuotes(){
        check_permission(20, 'read');
  $res='';
  $this->load->model('ems/model_inquries');
  $data['res']=$this->model_inquries->fetchAllQuotes();
  if(sizeof($data)>0)
  {
   $this->load->view('ems/inquries/manageQuotes',$data);
  }

 }
 
 public function viewQuote()

 { //Ckeditor's configuration


     check_permission(20, 'read');


  $id = $this->uri->segment(5);

  if($id)

  {

   $this->load->model('ems/model_inquries');

   $data['res']=$this->model_inquries->fetchQuote($id);
   //echo "<pre>";print_r($data);echo "</pre>";exit;

   if($data!=false)

   {

    $this->load->view('ems/inquries/viewQuote',$data);

   }



  }

  else {

   $this->load->view('ems/inquries/viewQuote');

  }







 }
 
  public function deleteQuote()
 {
     check_permission(20, 'delete');
  $this->layout = '';
  $this->load->model('ems/model_inquries');
  $id = $_POST['id'];
  $result=$this->model_inquries->deleteQuote($id);

 }



	public function view()

	{ //Ckeditor's configuration

        check_permission(20, 'read');

		

		$id = $this->uri->segment(5);

		if($id)

		{

			$this->load->model('ems/model_inquries');

			$data['res']=$this->model_inquries->fetchRowQuote($id);
                        //echo "<pre>";print_r($data);echo "</pre>";exit;

			if($data!=false)

			{

				$this->load->view('ems/request_quotation/view',$data);

			}



		}

		else {

				$this->load->view('ems/inquries/manage');

		}







	}



	public function publish()

	{
        check_permission(20, 'publish');
                //echo print_r($_POST['id']);

		$this->layout = '';

		$this->load->model('ems/model_inquries');

		$id = $_POST['id'];

		$status= $_POST['pub_status'];

		$result=$this->model_inquries->publishStatus($status,$id);

	}

	public function delete()

	{
        check_permission(20, 'delete');
		$this->layout = '';

		$this->load->model('ems/model_inquries');

		$id = $_POST['id'];

		$result=$this->model_inquries->delete($id);



	}
	
	public function export_excel()
    {
        check_permission(20, 'read');
		$this->load->library('excel');	
		$id_arr = array();
        if(html_escape($this->input->post('excel_type')) == 0)
        {
			$table = 'request_quotation';
            $user_ids = $this->model_custom->fetchAll_Ids($table);
            foreach($user_ids as $val)
            {
                $id_arr[] = $val->id;	
            }
        }
        else
        {
            $id_arr = html_escape($this->input->post('id'));
        }	
		$i=0;
		$users_arr = array();
        foreach($id_arr as $id)
        {
			$table = 'request_quotation';
            $users_arr[] = $this->model_custom->fetch_single($id,$table);	
			$i++;
        }
        $this->gen_xl($users_arr, 'request_quotation_excel'); 	
	}
	
	public function gen_xl($rr, $filename){
        check_permission(20, 'read');
			$this->excel->getProperties()->setCreator("Soumya Biswas")
				 ->setLastModifiedBy("Soumya Biswas")
				 ->setTitle("Office 2007 XLSX Test Document")
				 ->setSubject("Office 2007 XLSX Test Document")
				 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
				 ->setKeywords("office 2007 openxml php")
				 ->setCategory("Test result file");				
		$border = array(
			'borders' => array(
			'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		); 				
		$ans = array(
		'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					  )
		);	 		 				 
		$arr = array(
			'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
		),
		'font'  => array(
			'bold'  => true,
			"color" => array("rgb" => "903")
			)
		);

		//Organization Name
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('A2', 'Organization Name');
		$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('A2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($border);
		
		//Job Title
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('B2', 'Job Title');
		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('B2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($border);
		
		//First Name
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('C1')->applyFromArray($border);

		
		$this->excel->getActiveSheet()->setCellValue('C2', 'First Name');
		$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('C2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($border);

		//Last Name
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('D1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('D2', 'Last Name');
		$this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('D2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($border);

		
		//Email
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('E1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('E2', 'Email');
		$this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('E2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($border);
		
		
		//Phone
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('F1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('F2', 'Phone');
		$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('F2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($border);
		
		//City
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('G1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('G2', 'City');
		$this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('G2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($border);
		
		//Country
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('H1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('H2', 'Country');
		$this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('H2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($border);
		
		//Requested Service
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('I1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('I2', 'Requested Service');
		$this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('I2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($border);
		
		//Message
		$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth('60');
		$this->excel->getActiveSheet()->getStyle('J1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('J2', 'Message');
		$this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('J2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth('60');
		$this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($border);
		
		 $n = 2;
		 foreach($rr as $ss){
		 $n++;
             
		 
		$this->excel->getActiveSheet()->setCellValue('A'.$n, $ss->organization_name);
		$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue( 'B' .$n, $ss->job_title);
		$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue( 'C' .$n, $ss->first_name);
		$this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue( 'D' .$n, $ss->last_name);
		$this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue( 'E' .$n, $ss->email);
		$this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue( 'F' .$n, $ss->phone);
		$this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($border);
			
		$this->excel->getActiveSheet()->setCellValue( 'G' .$n, $ss->city);
		$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);
		
		
		$this->excel->getActiveSheet()->setCellValue( 'H' .$n, $ss->country);
		$this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue( 'I' .$n, $ss->product_service);
		$this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue( 'J' .$n, $ss->message);
		$this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($border);
   } 

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$filename=$filename.".xls";
		
		$folder_path = '/home/public_html/uploads/exports/';
        $file = $folder_path.$filename;

		//Setting the header type
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
        $this->manage();
		
	}



}



/* End of file admin-login.php */

/* Location: ./application/controllers/ems/admin-login.php */