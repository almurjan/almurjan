<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start();

class user_profiles extends CI_Controller {



	public $layout = 'admin_inner';







	function __construct() {

		parent::__construct();

		$this->layout = 'admin_inner';
		$this->load->model('model_custom');
		$this->load->library('email','excel');
		checkAdminSession();

	}



	//main index function for the controller dashboard

	//loding the main view

	public function index(){
		
		$this->manage();
	}


	public function get_user_info_sub_admin()
	{ 
		$admin_lang = check_admin_lang();
		$id = $this->uri->segment(4);
		$this->load->model('ems/model_user_profiles');
		$this->load->model('ems/model_configuration');
		$configuration = $this->model_configuration->fetchRow();
		
		$data = $this->model_user_profiles->get_user_info_sub_admin($id);
        
		$data->created_at = date("Y-m-d H:i:s A", strtotime($data->created_at));
        $data->dob = date("Y-m-d", strtotime($data->dob));
		if($admin_lang['admin_lang'] == 'eng'){
			$data->cat_name = $data->cat_eng_name;
		}
		else{
			$data->cat_name = $data->cat_arb_name;
		}
		$user_files_exploded = explode('|||',$data->user_files);
		foreach($user_files_exploded as $user_file){
			$html_user_files .= '<a href="'.base_url()."uploads/user_files/".$user_file.'" download><img class="thumbnail gallery_images" height="50" width="50" style="max-height:120px; cursor:pointer;" src="'.base_url('uploads/download.png').'"></img>';
		}
		
		$other_materials_exploded = explode('|||',$data->other_materials);
		foreach($other_materials_exploded as $other_materials){
			$html_other_materials .= '<a href="'.base_url()."uploads/other_materials/".$other_materials.'" download><img class="thumbnail gallery_images" height="50" width="50" style="max-height:120px; cursor:pointer;" src="'.base_url('uploads/download.png').'"></img>';
		}
		
		$val['average_ratings'] = $data->average_ratings;
		$x = $val['average_ratings'];
		$y = 10;
		
		$val['average_ratings'] = number_format( $val['average_ratings'], 1 );
		
		$percent = $x/$y;
		//$percent_friendly = number_format( $percent * 100, 2 );
		$percent_friendly = $percent * 100;
		
		$percentage = $configuration->percentage;
		$percentage = str_replace('%', '', $percentage); // Replaces all spaces with hyphens.
		
		$minimum_percentage =  $percentage/10;
		
		$html_button = '';
		if($val['average_ratings'] > $minimum_percentage){
			$html_button = '<button class="md-btn md-btn-success" type="button" data-uk-button="">'.$val['ratings'] .$val['average_ratings'].'</button>';
		}
		else if($val['average_ratings'] < $minimum_percentage && $val['average_ratings'] > 0){
			$html_button = '<button class="md-btn md-btn-danger" type="button" data-uk-button="">'.$val['ratings'] . $val['average_ratings'] .'</button>';
		}
		else if($val['average_ratings'] == $minimum_percentage){
			$html_button = '<button class="md-btn md-btn-success" type="button" data-uk-button="">'.$val['ratings'] . $val['average_ratings'] .'</button>';
		}
		else if($val['average_ratings'] == 0){
			$html_button = '<button class="md-btn md-btn-warning" type="button" data-uk-button="">'.$val['ratings'] . $val['average_ratings'] .'</button>';
		}else{
			$html_button = '<button class="md-btn md-btn-warning" type="button" data-uk-button="">'.$admin_lang['label']['pending'] . '</button>';
		}
		
		$loggedInUserId = $this->session->userdata('id');
		$app_ratings = getSubAdminRatingOnApplication($id,$loggedInUserId);
		$profile_ratings = getSubAdminRatingOnProfile($id,$loggedInUserId);
		
		$data->app_ratings = ($app_ratings->ratings ? number_format( $app_ratings->ratings, 1 ) : 0);
		$data->profile_ratings = ($profile_ratings->ratings ? number_format( $profile_ratings->ratings, 1 ) : 0);
		$data->other_materials = $html_other_materials;
		$data->user_files = $html_user_files;
		$data->html_button = $html_button;
		
		echo json_encode($data);
		exit;
	}
	

	public function manage(){
		
		$this->load->model('ems/model_configuration');
		$data['configuration'] = $this->model_configuration->fetchRow();
		if($this->uri->segment(4) != ''){
			$filteration_type = $this->uri->segment(4);
			$data['filteration_type'] = $filteration_type;
			$filteration_type = $this->uri->segment(4);
			$percentage = $data['configuration']->percentage;
			$percentage = str_replace('%', '', $percentage);
			$percentage =  $percentage/10;
			$x = $percentage;
			$y = 10;
		}
		else{
			$percentage = '';
		}
		
		if($this->session->userdata('usr_grp_id') == 3){
			$res='';
			$this->load->model('ems/model_user_profiles');
			$data['res']=$this->model_user_profiles->fetchAll('judge',$filteration_type,$percentage);
			
			if(sizeof($data)>0)
			{
				$this->load->view('ems/user_profiles/manage',$data);
			}
		}
		else{
			
			$res='';
			$this->load->model('ems/model_user_profiles');
			$data['res']=$this->model_user_profiles->fetchAllCreatedProfiles('admin',$filteration_type,$percentage);
			
			if(sizeof($data)>0)
			{
				$this->load->view('ems/user_profiles/manage_admin',$data);
			}	
		}
		
	}
	
	
	public function profile_ratings(){
		$id = $this->uri->segment(4);
		$res='';
		$this->load->model('ems/model_user_profiles');
		$this->load->model('ems/model_configuration');
		
		$x = $percentage;
		$y = 10;
		$data['configuration'] = $this->model_configuration->fetchRow();
		$percentage = $data['configuration']->percentage;
		$percentage = str_replace('%', '', $percentage);
		$percentage =  $percentage/10;
		$data['percentage'] = $percentage;
		$data['res']=$this->model_user_profiles->fetchAllProfileRatings($id);
		if(sizeof($data)>0)
		{
			$this->load->view('ems/user_profiles/manage_profile_ratings',$data);
		}	
	}
	
	public function manageQuotes(){
  $res='';
  $this->load->model('ems/model_inquries');
  $data['res']=$this->model_inquries->fetchAllQuotes();
  if(sizeof($data)>0)
  {
   $this->load->view('ems/inquries/manageQuotes',$data);
  }

 }
 
 public function view()

 { //Ckeditor's configuration



  $id = $this->uri->segment(5);

  if($id)

  {

   $this->load->model('ems/model_user_profiles');

   $data['res']=$this->model_user_profiles->fetchRow($id);
   //echo "<pre>";print_r($data);echo "</pre>";exit;

   if($data!=false)

   {

    $this->load->view('ems/user_profiles/view',$data);

   }



  }

  else {

   $this->load->view('ems/inquries/viewQuote');

  }
 }
 
 public function updateUserProfileByAdmin()
 {
	$this->load->model('ems/model_contents'); 
	$data = array();
	$post_data = $this->input->post();
	foreach($post_data as $key => $value)
	{
		if($key != 'form_type' && $key != 'submit' && $key != 'terms' && $key != 'video' && $key != 'image' && $key != 'audio' && $key != 'radio')
		{
			$data[$key] = $value;	
		}
	}
	
	$id = $data['hidden_id'];
	$updateArray['facebook'] = $data['facebook'];
	$updateArray['twitter'] = $data['twitter'];
	$updateArray['instagram'] = $data['instagram'];
	$updateArray['linkedin'] = $data['linkedin'];
	$updateArray['website'] = $data['website'];
	$updateArray['video_link'] = $data['video_link'];
	$updateArray['impact_level'] = $data['impact_level'];
	$updateArray['intro'] = $data['intro'];
	$updateArray['description'] = $data['description'];
	$updateArray['summary'] = $data['summary'];
	$updateArray['creativity'] = $data['creativity'];
	$updateArray['slogan'] = $data['slogan'];
	if($_FILES['user_image_new']['name'] != ''){
		$img_name = $_FILES['user_image_new']['name'];			
		$img_name = explode('.',$img_name);			
		$img = $img_name[0].'-'.time().'.'.$img_name[1];			
		$type = $_FILES['user_image_new']['type'];			
		$img_temp = $_FILES['user_image_new']['tmp_name'];			
		$error = $_FILES['user_image_new']['error'];			
		if  ($type == "image/png" || $type == "image/jpeg" || $type == "image/jpg"){
			move_uploaded_file($img_temp,"uploads/registration_form/$img");        			
		}			
		$updateArray['user_image'] = $img;			
	}
	
	$response = $userRecord = $this->model_contents->updateRegistraionProfile($updateArray,$id);
	$resArr['success'] = 1;
	$resArr['status'] = 1;
	$resArr['id'] = $id;
	$resArr['message'] = "Update Successfully";
	echo json_encode($resArr);
	exit();
 }
 
  public function deleteQuote()
 {
	
  $this->layout = '';
  $this->load->model('ems/model_inquries');
  $id = $_POST['id'];
  $result=$this->model_inquries->deleteQuote($id);

 }



	public function get_user_info()
	{ 
		$admin_lang = check_admin_lang();
		$id = $this->uri->segment(4);
		$this->load->model('ems/model_inquries');
		$data = $this->model_inquries->get_user_info($id);
        $data->created_at = date("Y-m-d H:i:s A", strtotime($data->created_at));
        $data->dob = date("Y-m-d", strtotime($data->dob));
		if($admin_lang['admin_lang'] == 'eng'){
			$data->cat_name = $data->cat_eng_name;
		}
		else{
			$data->cat_name = $data->cat_arb_name;
		}
		$user_files_exploded = explode('|||',$data->user_files);
		foreach($user_files_exploded as $user_file){
			$html_user_files .= '<a href="'.base_url()."uploads/user_files/".$user_file.'" download><img class="thumbnail gallery_images" height="50" width="50" style="max-height:120px; cursor:pointer;" src="'.base_url('uploads/download.png').'"></img>';
		}
		$data->user_files = $html_user_files;
		echo json_encode($data);
		exit;
	}
	public function get_judge_info()
	{ 
		$admin_lang = check_admin_lang();
		$id = $this->uri->segment(4);
		$this->load->model('ems/model_inquries');
		$data = $this->model_inquries->get_judge_info($id);
		$data->job = content_detail('arb_job_name',$id);
		$data->description = content_detail('arb_judge_desc',$id);
		if(content_detail('eng_judge_image_mkey_hdn',$id)){
			$html_user .= '<img src="'.base_url().'assets/script/'.content_detail('eng_judge_image_mkey_hdn',$id).'"></img>';
		}
		else{
			$html_user .= '<img src="'.base_url().'assets/script/mlib-uploads/thumbs/any.jpg'.'"></img>';
		}
        
		$data->judge_image = $html_user;
		echo json_encode($data);
		exit;
	}
	public function rateUser()
    {
		$this->load->model('ems/model_inquries');
		
		$loggedInUserId = $this->session->userdata('id');
     	$data = array();
		$data_post = $this->input->post();	
		$update['ratings']  = $data_post['ratings'];	
		$update['judge_id'] = $loggedInUserId;	
		$update['app_accepted_time'] = date('Y-m-d h:i:s');	
		$id = $data_post['id'];	
		$this->model_inquries->update_user_rating('registrations',$update,$id);
		$this->session->set_flashdata('message', 'Rate Successfully');
		redirect($this->config->item('base_url') . 'ems/registrations');       
    }



	public function publish()

	{

                //echo print_r($_POST['id']);

		$this->layout = '';

		$this->load->model('ems/model_inquries');

		$id = $_POST['id'];

		$status= $_POST['pub_status'];

		$result=$this->model_inquries->publishStatus($status,$id);

	}

	public function acceptApplication()

	{
		
        //echo print_r($_POST['id']);

		$this->layout = '';

		$this->load->model('ems/model_inquries');

		$id = $_POST['id'];

		$status= $_POST['pub_status'];
		$data_user = $this->model_inquries->get_user_info($id);
		
		$lang = 'arb';
		if ($lang == 'eng'){
			$subject = 'Your application is accepted'; 
			$title 	 = 'You can upload your video by below link'; 
		}else{
			$subject = 'يتم قبول طلبك'; 
			$title   = 'يمكنك تحميل الفيديو الخاص بك عن طريق الرابط أدناه';
		
		}
		$data['name'] = $data_user->name;
		$data['email'] = $data_user->email;
		$data['id'] = $data_user->id;
		
		userGeneralProfileEmail($data,$subject,$title,$lang);
		$result=$this->model_inquries->acceptApplication($status,$id);
		
	}
	
	public function acceptAllApplication_old()

	{
		$this->load->model('ems/model_configuration');
		$data['configuration'] = $this->model_configuration->fetchRow();
		$percentage = $data['configuration']->percentage;
		$percentage = str_replace('%', '', $percentage);
		$percentage =  $percentage/10;
		$this->layout = '';

		$this->load->model('ems/model_inquries');
		$allQualifiedApplications = $this->model_inquries->fetchAllQualifiedApplications($percentage);
		
		$lang = 'arb';
		foreach($allQualifiedApplications as $allQualifiedApplication){
			$data_user = $this->model_inquries->get_user_info($allQualifiedApplication->id);
			if ($lang == 'eng'){
				$subject = 'Your application is accepted'; 
				$title 	 = 'You can upload your video by below link'; 
			}else{
				$subject = 'يتم قبول طلبك'; 
				$title   = 'يمكنك تحميل الفيديو الخاص بك عن طريق الرابط أدناه';
			
			}
			$data['name'] = $data_user->name;
			$data['email'] = $data_user->email;
			$data['id'] = $data_user->id;
			
			userGeneralProfileEmail($data,$subject,$title,$lang);
			$result=$this->model_inquries->acceptApplication(1,$allQualifiedApplication->id);
		}
		$this->session->set_flashdata('message', 'Applications are accepted Successfully');
		redirect($this->config->item('base_url') . 'ems/registrations');

	}
	
	public function delete()

	{

		$this->layout = '';

		$this->load->model('ems/model_inquries');

		$id = $_POST['id'];

		$result=$this->model_inquries->delete($id);



	}
	

	public function acceptAllApplication()

	{
		$this->load->model('ems/model_configuration');
		$data['configuration'] = $this->model_configuration->fetchRow();
		$percentage = $data['configuration']->percentage;
		$percentage = str_replace('%', '', $percentage);
		$percentage =  $percentage/10;
		$this->layout = '';

		$this->load->model('ems/model_inquries');
		$allQualifiedApplications = $this->model_inquries->fetchAllQualifiedProfiles($percentage);
		$lang = 'arb';
		foreach($allQualifiedApplications as $allQualifiedApplication){
			$data_user = $this->model_inquries->get_user_info($allQualifiedApplication->id);
			if ($lang == 'eng'){
				$subject = 'Your application is published'; 
				$title 	 = 'You can upload your video by below link'; 
			}else{
				$subject = 'يتم قبول طلبك'; 
				$title   = 'يمكنك تحميل الفيديو الخاص بك عن طريق الرابط أدناه';
			
			}
			$data['name'] = $data_user->name;
			$data['email'] = $data_user->email;
			$data['id'] = $data_user->id;
			
			//userGeneralProfileEmail($data,$subject,$title,$lang);
			$result=$this->model_inquries->acceptProfile(1,$allQualifiedApplication->id);
		}
		$this->session->set_flashdata('message', 'Profiles accepted successfully');
		redirect($this->config->item('base_url') . 'ems/user_profiles');

	}	
	public function export_excel()
    {
		
		$this->load->library('excel');	
		$id_arr = array();
        if(html_escape($this->input->post('excel_type')) == 0)
        {
			$table = 'registrations';
            $user_ids = $this->model_custom->fetchAll_Ids($table);
            foreach($user_ids as $val)
            {
                $id_arr[] = $val->id;	
            }
        }
        else
        {
            $id_arr = html_escape($this->input->post('id'));
        }	
		$i=0;
		$users_arr = array();
        foreach($id_arr as $id)
        {
			$table = 'registrations';
            $users_arr[] = $this->model_custom->fetch_single_reg($id,$table,1);	
			$i++;
        }
        $this->gen_xl($users_arr, 'users-excel'); 	
	}
	
	public function gen_xl($rr, $filename){

			$this->excel->getProperties()->setCreator("Soumya Biswas")
				 ->setLastModifiedBy("Soumya Biswas")
				 ->setTitle("Office 2007 XLSX Test Document")
				 ->setSubject("Office 2007 XLSX Test Document")
				 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
				 ->setKeywords("office 2007 openxml php")
				 ->setCategory("Test result file");				
		$border = array(
			'borders' => array(
			'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		); 				
		$ans = array(
		'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					  )
		);	 		 				 
		$arr = array(
			'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
		),
		'font'  => array(
			'bold'  => true,
			"color" => array("rgb" => "903")
			)
		);


		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($border);


		$this->excel->getActiveSheet()->setCellValue('A2', 'Name');
		$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('A2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($border);

		
		$this->excel->getActiveSheet()->setCellValue('B2', 'Email');
		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('B2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('C2', 'Phone no');
		$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('C2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('D2', 'Category');
		$this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('D2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('E2', 'Dob');
		$this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('E2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue('F2', 'Impact Level');
		$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('F2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue('G2', 'Introduction');
		$this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('G2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue('H2', 'Description');
		$this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('H2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($border);
			
		$this->excel->getActiveSheet()->setCellValue('I2', 'Summary');
		$this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('I2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($border);
			
		$this->excel->getActiveSheet()->setCellValue('J2', 'Creativity');
		$this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('J2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($border);
			
		$this->excel->getActiveSheet()->setCellValue('K2', 'Application Average Rating');
		$this->excel->getActiveSheet()->getStyle('K2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('K2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('K2')->applyFromArray($border);
			
		$this->excel->getActiveSheet()->setCellValue('L2', 'Profile Average Rating');
		$this->excel->getActiveSheet()->getStyle('L2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('L2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('L2')->applyFromArray($border);
			
		$this->excel->getActiveSheet()->setCellValue('M2', 'Profile Average Rating');
		$this->excel->getActiveSheet()->getStyle('M2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('M2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('M2')->applyFromArray($border);
			
		$this->excel->getActiveSheet()->setCellValue('N2', 'Status');
		$this->excel->getActiveSheet()->getStyle('N2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('N2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('N2')->applyFromArray($border);
			
		$n = 2;
		foreach($rr as $ss){
			if($ss->name != ''){	
				$n++;
					 
				$reg_date = strtotime($ss->created_at);
				$date_as = date( 'M d, Y', $reg_date );
				$time_as = date( 'H:i', $reg_date );
				$ss->average_ratings = round($ss->average_ratings, 1);
				 
				$page_details = get_content_data($ss->cat_id);
				
				$status = '';
				if($ss->pub_status == 0){
					$status = 'Pending';
				}
				else if($ss->pub_status == 1){
					$status = 'Published';
				}
				else if($ss->pub_status == 2){
					$status = 'Rejected';
				}
				
				$ss->average_ratings = number_format( $ss->average_ratings, 1 );
				$ss->average_ratings_profile = number_format( $ss->average_ratings_profile, 1 );
				
				$this->excel->getActiveSheet()->setCellValue('A'.$n, $ss->name);
				$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($border);
				
				
				$this->excel->getActiveSheet()->setCellValue('B'.$n, $ss->email);
				$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue( 'C' .$n, $ss->phone_no);
				$this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('c'.$n)->applyFromArray($border);

				$this->excel->getActiveSheet()->setCellValue( 'D' .$n, $page_details->eng_title);
				$this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($border);

				$this->excel->getActiveSheet()->setCellValue( 'E' .$n, $ss->dob);
				$this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('F'.$n,$ss->impact_level);
				$this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('G'.$n,$ss->intro);
				$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('H'.$n,$ss->description);
				$this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('I'.$n,$ss->summary);
				$this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('J'.$n,$ss->creativity);
				$this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('K'.$n,$ss->average_ratings);
				$this->excel->getActiveSheet()->getStyle('K'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('K'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('L'.$n,$ss->average_ratings_profile);
				$this->excel->getActiveSheet()->getStyle('L'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('L'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('M'.$n,$ss->video_link);
				$this->excel->getActiveSheet()->getStyle('M'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('M'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('N'.$n,$status);
				$this->excel->getActiveSheet()->getStyle('N'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('N'.$n)->applyFromArray($border);
			}						
		} 

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$filename=$filename.".xls";
		
		$folder_path = '/home/public_html/uploads/exports/';
        $file = $folder_path.$filename;

		//Setting the header type
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
        $this->manage();
		
	}




}



/* End of file admin-login.php */

/* Location: ./application/controllers/ems/admin-login.php */