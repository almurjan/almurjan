<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Cities extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        $this->load->model('ems/model_cities');
		$this->load->library('email');
        checkAdminSession();
    }


    
    public function index() {
        $this->manage();
    }

    public function manage() {
		$data = array();
        $cid = $this->uri->segment(4);
			  $data['cities'] = $this->model_cities->fetchAllAnnouncement('cms_cities', $cid);
			  $this->load->view('ems/cities/manage', $data);
    }

	public function add() {

			  $this->load->view('ems/cities/add');

    }

	public function addEventType() {

        $data = array();
		$data['eng_title'] = html_escape($this->input->post('eng_title'));
		$data['arb_title'] = html_escape($this->input->post('arb_title'));
		$data['country_id'] = html_escape($this->input->post('country_id'));
		$data['status'] = '1';

        if ($_FILES['image']['name'] != '') {
            $file_name = $_FILES['image']['name'].'-'.date('Ymdhsi');
            $file_size = $_FILES['image']['size'];
            $file_tmp = $_FILES['image']['tmp_name'];
            $type = $_FILES['image']['type'];
            $element1 = explode('/', $type);
            $file_type = $element1[1];
            $new_file_name = $file_name . '.' . $file_type;
            move_uploaded_file($file_tmp, "uploads/event_types/" . $file_name);
            $data['icon'] = $file_name;
        }

		$insert_id = $this->model_cities->saveEventType($data);
		if ($insert_id)
		{
			$this->session->set_flashdata('message', _okMsg('<p>City added successfully.</p>'));
			redirect($this->config->item('base_url') . 'ems/cities/manage/'.$data['country_id']);
		}
    }

	public function updateEventType() {

		$id = $_POST['id'];
        $data = array();
        $data['eng_title'] = html_escape($this->input->post('eng_title'));
        $data['arb_title'] = html_escape($this->input->post('arb_title'));
        $data['country_id'] = html_escape($this->input->post('country_id'));

        /*if ($_FILES['image']['name'] != '') {
            $file_name = $_FILES['image']['name'].'-'.date('Ymdhsi');
            $file_size = $_FILES['image']['size'];
            $file_tmp = $_FILES['image']['tmp_name'];
            $type = $_FILES['image']['type'];
            $element1 = explode('/', $type);
            $file_type = $element1[1];
            $new_file_name = $file_name . '.' . $file_type;
            move_uploaded_file($file_tmp, "uploads/event_types/" . $file_name);
            $data['icon'] = $file_name;
        }else{
            $data['icon'] = html_escape($this->input->post('file_name'));
        }*/

		$insert_id = $this->model_cities->updateEventType($id, $data);
		if ($insert_id)
		{
			$this->session->set_flashdata('message', _okMsg('<p>City updated successfully.</p>'));
			redirect($this->config->item('base_url') . 'ems/cities/manage/'.$data['country_id']);
		}
    }

    public function edit() {
          $data = array();
        $id = $this->uri->segment(5);
        if ($id) {
            $data['event_type'] = $this->model_cities->fetch($id,'cms_cities');
            if (!empty($data)) {
                $this->load->view('ems/cities/edit', $data);
            }
        }
    }

    public function view() {
        $data = array();
        $id = $this->uri->segment(5);
        if ($id) {
            $data['event_type'] = $this->model_cities->fetch($id,'event_type');
            if (!empty($data)) {
                $this->load->view('ems/cities/view', $data);
            }
        }
    }

    public function publish()
    {
        $this->layout = '';
        $this->load->model('ems/model_cities');
        $id = $_POST['id'];
        $status['status']= $_POST['pub_status'];
        $result=$this->model_cities->publishStatus($status,$id);
    }

  public function delete() {
	    $lang = $this->session->userdata('lang');
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_cities->deleteEventType($id);
        echo $result;
       // $this->deletePhotos($id);
    }

    public function getCityForEms()
    {
        $data = array();
        $result = array();
        $citiesList = '';
        $data['id'] = $country_id = html_escape($this->input->post('id'));
        $cities = getCitiesForCountryEms($country_id);
        /*echo '<pre>';
        print_r($cities);
        exit();*/
        if ($cities) {
            //$citiesList .= '<option value="">' . ($lang == 'eng' ? 'Please Select' : 'اختر من فضلك') . '</option>';
            foreach ($cities as $city) {
                $citiesList .= '<option value="' . $city->id . '">' . $city->eng_title . '</option>';
            }
        }
        $result['cities'] = $citiesList;
        $result['error'] = 'false';
        echo json_encode($result);
        exit();
    }
}
/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */