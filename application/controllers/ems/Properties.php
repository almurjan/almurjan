<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Properties extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        $this->load->model('ems/model_properties');
		$this->load->library('email');
        checkAdminSession();
    }

    public function index() {
        $this->manage();
    }

    public function manage() {
          $data = array();
          $data['property_data'] = $this->model_properties->fetchAll('properties');
          $this->load->view('ems/Properties/manage', $data);
    }
    public function add() {

        $this->load->view('ems/Properties/add');
    }

    public function edit() {
        $data = array();
        $id = $this->uri->segment(5);
        if ($id) {
            $data['property_data'] = $this->model_properties->fetch($id,'properties');
            if (!empty($data)) {
                $this->load->view('ems/Properties/edit', $data);
            }
        }
    }

    public function saveProperty() {

        $data = array();
        $data['property_title']  = html_escape($this->input->post('property_title'));
        $data['property_type']  = html_escape($this->input->post('property_type'));
        $data['city']  = html_escape($this->input->post('city'));
        $data['area']  = html_escape($this->input->post('area'));
        $data['access']  = html_escape($this->input->post('access'));
        $data['space']  = html_escape($this->input->post('space'));
        $data['facilities']  = html_escape($this->input->post('facilities'));
        $data['property_title_arb']  = html_escape($this->input->post('property_title_arb'));
        $data['property_type_arb']  = html_escape($this->input->post('property_type_arb'));
        $data['city_arb']  = html_escape($this->input->post('city_arb'));
        $data['area_arb']  = html_escape($this->input->post('area_arb'));
        $data['access_arb']  = html_escape($this->input->post('access_arb'));
        $data['space_arb']  = html_escape($this->input->post('space_arb'));
        $data['facilities_arb']  = html_escape($this->input->post('facilities_arb'));
        $data['area']  = html_escape($this->input->post('area'));

        $data['status'] = '1';
        $data['created_at'] = date('Y-m-d H:i:s');
        /*echo '<pre>';
        print_r($data); exit();*/
        $insert_id = $this->model_properties->saveData($data);
        if ($insert_id)
        {
            $this->session->set_flashdata('message', _okMsg('<p>Property details added successfully.</p>'));
            redirect($this->config->item('base_url') . 'ems/Properties');
        }

    }
    public function updateProperty() {

        $id = $_POST['id'];
        $data = array();
        $data['property_title']  = html_escape($this->input->post('property_title'));
        $data['property_type']  = html_escape($this->input->post('property_type'));
        $data['city']  = html_escape($this->input->post('city'));
        $data['area']  = html_escape($this->input->post('area'));
        $data['access']  = html_escape($this->input->post('access'));
        $data['space']  = html_escape($this->input->post('space'));
        $data['facilities']  = html_escape($this->input->post('facilities'));
        $data['property_title_arb']  = html_escape($this->input->post('property_title_arb'));
        $data['property_type_arb']  = html_escape($this->input->post('property_type_arb'));
        $data['city_arb']  = html_escape($this->input->post('city_arb'));
        $data['area_arb']  = html_escape($this->input->post('area_arb'));
        $data['access_arb']  = html_escape($this->input->post('access_arb'));
        $data['space_arb']  = html_escape($this->input->post('space_arb'));
        $data['facilities_arb']  = html_escape($this->input->post('facilities_arb'));
        $data['area']  = html_escape($this->input->post('area'));

        $data['updated_at'] = date('Y-m-d H:i:s');

        $insert_id = $this->model_properties->updateData($id, $data);
        if ($insert_id > 0)
        {
            $this->session->set_flashdata('message', _okMsg('<p>Property details Updated successfully.</p>'));
            redirect($this->config->item('base_url') . 'ems/Properties/edit/id/'.$id);
        }

    }
    
    public function delete() {
        $lang = $this->session->userdata('lang');
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_properties->delete($id);
        echo $result;
    }

    /*Front end Functions*/
    public function fetchAllProperties() {
        $data = array();
        $property_data = $this->model_properties->fetchAll('properties');
        return $property_data;
    }
    /*Front end Functions*/

    /*Start Request a Quotation*/
    public function R_Quotation() {
        $data = array();
        $data['data_arr'] = $this->model_properties->fetchAll('request_quotation');
        $this->load->view('ems/Quotations/manage', $data);
    }
    public function view_quotation() {
        $data = array();
        $id = $this->uri->segment(5);
        if ($id) {
            $data['data_arr'] = $this->model_properties->fetch($id,'request_quotation');
            if (!empty($data)) {
                $this->load->view('ems/Quotations/edit', $data);
            }
        }
    }
    public function delete_quotation() {
        $lang = $this->session->userdata('lang');
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_properties->delete_request($id,'request_quotation');
        echo $result;
    }
    /*End Request a Quotation*/
}
/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */