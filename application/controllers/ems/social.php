<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social extends CI_Controller {

	public $layout = 'admin_inner';
	
	
	 
	function __construct() {
		parent::__construct();
		$this->layout = 'admin_inner'; 	
                checkAdminSession();
				check_permission(7, 'any');		
    }
	
	//main index function for the controller dashboard
	//loding the main view
	public function index(){
    check_permission(7, 'read');
	$this->load->model('ems/model_configuration');
	$rec['res']=$this->model_configuration->fetchRowSocialLink();
		{
			$this->load->view('ems/social/edit',$rec);
		}
	
		
	}
        
	public function manage(){
	    check_permission(7, 'read');	
	$this->load->model('ems/model_configuration');
	$rec['res']=$this->model_configuration->fetchRowSocialLink();
		{
			$this->load->view('ems/social/edit',$rec);
		}
	
		
	}        

	
public function save(){
		check_permission(7, 'update');
		$this->load->model('ems/model_configuration');
		$rec['res']=$this->model_configuration->fetchRowSocialLink();

		$id = $this->uri->segment(5);

		$data=array();
		$loggedInUserId=$this->session->userdata('id');
                                
				/////// social setting /////////////
				 $data['soc_fb']= removeJsScripts(html_escape($this->input->post('soc_fb')));
				 $data['soc_tw']=removeJsScripts(html_escape($this->input->post('soc_tw')));
				 $data['soc_insta']=removeJsScripts(html_escape($this->input->post('soc_insta')));
                 $data['soc_lin']=removeJsScripts(html_escape($this->input->post('soc_lin')));
                 $data['soc_you']=removeJsScripts(html_escape($this->input->post('soc_you')));
                 $data['soc_google']=removeJsScripts(html_escape($this->input->post('soc_google')));
                 $data['soc_pinterest']=removeJsScripts(html_escape($this->input->post('soc_pinterest')));
                 $data['soc_snapchat']=removeJsScripts(html_escape($this->input->post('soc_snapchat')));

				$data['soc_created_at']=date('Y-m-d H:i:s');
				$data['soc_created_by']=$loggedInUserId;
				$data['pub_status']=html_escape($this->input->post('pub_val'));
				log_insert($this->uri->segment(2),'update a record in');
				$data['soc_updated_at']=date('Y-m-d H:i:s');
				$data['soc_updated_by']=$loggedInUserId;
				$result=$this->model_configuration->updateSocial($data,$rec['res']->id);
				$this->session->set_flashdata('message', _okMsg('<p>Record Updated.</p>'));
					
			redirect($this->config->item('base_url') . 'ems/social');
			}
		
}

/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */