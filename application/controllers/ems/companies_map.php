<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start();

class companies_map extends CI_Controller {



    public $layout = 'admin_inner';

    function __construct() {

        parent::__construct();

        $this->layout = 'admin_inner';
        $this->load->model('model_custom');
        $this->load->library('email','excel');
        checkAdminSession();
        /*check_permission(27, 'any');*/

    }



    //main index function for the controller dashboard

    //loding the main view

    public function index(){
        //$this->load->view('ems/inquries/manage');
        $this->view();
    }
    public function view()

    { //Ckeditor's configuration
       /* check_permission(27, 'any');*/
        /*$id = $this->uri->segment(5);*/
        $id = 1;
        if($id)
        {
            $this->load->model('ems/model_contents');
            $data['res']=$this->model_contents->getCompany($id);
            if($data!=false)
            {
                $this->load->view('ems/companies_map/edit',$data);
            }
        }
        else {
            $this->load->view('ems/companies_map/add');
        }
    }
    public function saveCompanies(){
        /*check_permission(27, 'any');*/
        $id = html_escape($this->input->post('id'));
        $this->load->model('ems/model_contents');
        $data = array();
        $post_data = $this->input->post();
        $data['eng_companyone']= html_escape($this->input->post('eng_companyone'));
        $data['arb_companyone']= html_escape($this->input->post('arb_companyone'));
        $data['eng_companytwo']= html_escape($this->input->post('eng_companytwo'));
        $data['arb_companytwo']= html_escape($this->input->post('arb_companytwo'));
        $data['eng_companythree']= html_escape($this->input->post('eng_companythree'));
        $data['arb_companythree']= html_escape($this->input->post('arb_companythree'));
        $data['eng_companyfour']= html_escape($this->input->post('eng_companyfour'));
        $data['arb_companyfour']= html_escape($this->input->post('arb_companyfour'));
        $data['eng_companyfive']= html_escape($this->input->post('eng_companyfive'));
        $data['arb_companyfive']= html_escape($this->input->post('arb_companyfive'));
        $data['eng_companysix']= html_escape($this->input->post('eng_companysix'));
        $data['arb_companysix']= html_escape($this->input->post('arb_companysix'));
        $data['eng_companyseven']= html_escape($this->input->post('eng_companyseven'));
        $data['arb_companyseven']= html_escape($this->input->post('arb_companyseven'));
        $data['eng_companyeight']= html_escape($this->input->post('eng_companyeight'));
        $data['arb_companyeight']= html_escape($this->input->post('arb_companyeight'));
        $data_insert=$this->model_contents->updateCompanies($data,$id);
        if($id)
        {
            if($data_insert!=false)
            {
                $this->index();
            }
        }
        else {
            if($data_insert>0)
            {
                $this->index();
            }
        }
    }
}
?>