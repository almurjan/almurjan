<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//session_start();

class mediclinic extends CI_Controller {



    public $layout = 'admin_inner';







    function __construct() {

        parent::__construct();

        $this->layout = 'admin_inner';
        $this->load->model('model_custom');
        $this->load->library('email','excel');
        checkAdminSession();
        check_permission(21, 'any');

    }



    //main index function for the controller dashboard

    //loding the main view

    public function index(){

        //$this->load->view('ems/inquries/manage');

        $this->manage();



    }



    public function manage(){
        check_permission(21, 'read');

        $this->load->model('ems/model_inquries');

        $data['users']=$this->model_inquries->fetchAllSelfApplication($date_request);

        if(sizeof($data)>0)

        {

            $this->load->view('ems/self_application/manage_mediclinic',$data);

        }

    }

    public function manageQuotes(){
        check_permission(21, 'read');
        $res='';
        $this->load->model('ems/model_inquries');
        $data['res']=$this->model_inquries->fetchAllQuotes();
        if(sizeof($data)>0)
        {
            $this->load->view('ems/inquries/manageQuotes',$data);
        }

    }

    public function viewQuote()

    { //Ckeditor's configuration

        check_permission(20, 'read');



        $id = $this->uri->segment(5);

        if($id)

        {

            $this->load->model('ems/model_inquries');

            $data['res']=$this->model_inquries->fetchQuote($id);
            //echo "<pre>";print_r($data);echo "</pre>";exit;

            if($data!=false)

            {

                $this->load->view('ems/inquries/viewQuote',$data);

            }



        }

        else {

            $this->load->view('ems/inquries/viewQuote');

        }







    }

    public function deleteQuote()
    {
        check_permission(20, 'delete');
        $this->layout = '';
        $this->load->model('ems/model_inquries');
        $id = $_POST['id'];
        $result=$this->model_inquries->deleteself($id);

    }



    public function view()

    { //Ckeditor's configuration
        check_permission(20, 'read');

        $id = $this->uri->segment(5);

        if($id)

        {

            $this->load->model('ems/model_contents');

            $data['res']=$this->model_contents->fetchRowSelf($id);

            if($data!=false)

            {

                $this->load->view('ems/self_application/view',$data);

            }



        }

        else {

            $this->load->view('ems/self_application/manage');

        }







    }



    public function publish()

    {
        check_permission(20, 'publish');
        //echo print_r($_POST['id']);

        $this->layout = '';

        $this->load->model('ems/model_inquries');

        $id = $_POST['id'];

        $status= $_POST['pub_status'];

        $result=$this->model_inquries->publishStatus($status,$id);

    }

    public function delete()

    {
        check_permission(20, 'delete');
        $this->layout = '';

        $this->load->model('ems/model_inquries');

        $id = $_POST['id'];

        $result=$this->model_inquries->deleteCareers($id);



    }

    public function export_excel()
    {
        check_permission(21, 'read');//ahmed
        $this->load->library('excel');
        $id_arr = array();
        if(html_escape($this->input->post('excel_type')) == 0)
        {
            $table = 'med_records';
            $user_ids = $this->model_custom->fetchAll_Ids($table);
            foreach($user_ids as $val)
            {
                $id_arr[] = $val->id;
            }
        }
        else
        {
            $id_arr = html_escape($this->input->post('id'));
        }
        $i=0;
        $users_arr = array();
        foreach($id_arr as $id)
        {
            $table = 'med_records';
            $users_arr[] = $this->model_custom->fetch_single($id,$table);
            $i++;
        }
        $this->gen_xl($users_arr, 'mediclinic-excel');
    }

    public function gen_xl($rr, $filename){
        check_permission(21, 'read');
        $this->excel->getProperties()->setCreator("Soumya Biswas")
            ->setLastModifiedBy("Soumya Biswas")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $border = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $ans = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $arr = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'font'  => array(
                'bold'  => true,
                "color" => array("rgb" => "903")
            )
        );


        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($border);


        $this->excel->getActiveSheet()->setCellValue('A2', 'First Name:');
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('A2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('B2', 'Middle Name');
        $this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('B2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
        $this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('C2', 'Last Name');
        $this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('C2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('30');
        $this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('D2', 'Age');
        $this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('D2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('15');
        $this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('E2', 'Mobile Number');
        $this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('E2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('F2', 'Email');
        $this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('F2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('35');
        $this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('G2', 'Nationality');
        $this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('G2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('H2', 'Country of Residence');
        $this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('H2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($border);

       
        $this->excel->getActiveSheet()->setCellValue('I2', 'Education Level');
        $this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('I2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('J2', 'Other Education Level');
        $this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('J2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('K2', 'Specialization');
        $this->excel->getActiveSheet()->getStyle('K2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('K2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('K2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('L2', 'Institution Name');
        $this->excel->getActiveSheet()->getStyle('L2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('L2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('L2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('M2', 'Last Job Title');
        $this->excel->getActiveSheet()->getStyle('M2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('M2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('M2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('N2', 'Work Experience');
        $this->excel->getActiveSheet()->getStyle('N2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('N2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('N2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('O2', 'Last Employees');
        $this->excel->getActiveSheet()->getStyle('O2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('O2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('O2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('P2', 'English Proficiency');
        $this->excel->getActiveSheet()->getStyle('P2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('P2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('P2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('Q2', 'Additional Information');
        $this->excel->getActiveSheet()->getStyle('Q2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('Q2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth('30');
        $this->excel->getActiveSheet()->getStyle('Q2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('R2', 'Certified By Saudi?');
        $this->excel->getActiveSheet()->getStyle('R2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('r2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('R2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('S2', 'Any Other Certiification ?');
        $this->excel->getActiveSheet()->getStyle('S2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('S2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth('30');
        $this->excel->getActiveSheet()->getStyle('S2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('T2', 'City of Residence');
        $this->excel->getActiveSheet()->getStyle('T2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('T2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('T2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('U2', 'Created at');
        $this->excel->getActiveSheet()->getStyle('U2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('U2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('u2')->applyFromArray($border);


        $n = 2;
        foreach($rr as $ss){
            $n++;

            $reg_date = strtotime($ss->created_at);
            $date_as = date( 'M d, Y', $reg_date );
            $time_as = date( 'H:i', $reg_date );

            $this->excel->getActiveSheet()->setCellValue('A'.$n, $ss->f_name);
            $this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue( 'B' .$n, $ss->m_name);
            $this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue( 'C' .$n, $ss->l_name);
            $this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue( 'D' .$n, $ss->age);
            $this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('E'.$n, $ss->mob_no);
            $this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('F'.$n,$ss->email);
            $this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('G'.$n,$ss->nationality);
            $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('H'.$n,$ss->c_residence);
            $this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('I'.$n,$ss->education_level);
            $this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('J'.$n,$ss->education_level_other);
            $this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('K'.$n,$ss->specialization);
            $this->excel->getActiveSheet()->getStyle('K'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('K'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('L'.$n,$ss->institute_name);
            $this->excel->getActiveSheet()->getStyle('L'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('L'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('M'.$n,$ss->last_job);
            $this->excel->getActiveSheet()->getStyle('M'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('M'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('N'.$n,$ss->experience);
            $this->excel->getActiveSheet()->getStyle('N'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('N'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('O'.$n,$ss->last_employees);
            $this->excel->getActiveSheet()->getStyle('O'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('O'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('P'.$n,$ss->eng_prof);
            $this->excel->getActiveSheet()->getStyle('P'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('P'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('Q'.$n,$ss->additional_info);
            $this->excel->getActiveSheet()->getStyle('Q'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('Q'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('R'.$n,$ss->saudi_comm);
            $this->excel->getActiveSheet()->getStyle('R'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('R'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('S'.$n,$ss->any_other_Cert);
            $this->excel->getActiveSheet()->getStyle('S'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('S'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('T'.$n,$ss->ct_residence);
            $this->excel->getActiveSheet()->getStyle('T'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('T'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('U'.$n,$date_as);
            $this->excel->getActiveSheet()->getStyle('U'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('U'.$n)->applyFromArray($border);



        }

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $filename=$filename.".xls";

        $folder_path = '/home/public_html/uploads/exports/';
        $file = $folder_path.$filename;

        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
        $this->manage();

    }



}



/* End of file admin-login.php */

/* Location: ./application/controllers/ems/admin-login.php */