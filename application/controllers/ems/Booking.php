<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Booking extends CI_Controller {
	public $layout = 'admin_inner';

	function __construct() {

		parent::__construct();

		$this->layout = 'admin_inner';
		$this->load->model('model_custom');
		$this->load->library('email','excel');
		checkAdminSession();

	}

	public function index(){

		//$this->load->view('ems/inquries/manage');

        $this->manage();

	}

	public function manage(){

		$res='';

		$this->load->model('ems/model_inquries');

		$data['res']=$this->model_inquries->fetchAllBookings();

		if(sizeof($data)>0)

		{

			$this->load->view('ems/bookings/manage',$data);

		}

	}

	public function view()

	{ //Ckeditor's configuration

		$id = $this->uri->segment(5);

		if($id)

		{

			$this->load->model('ems/model_inquries');

			$data['res']=$this->model_inquries->fetch($id);
                        //echo "<pre>";print_r($data);echo "</pre>";exit;

			if($data!=false)

			{

				$this->load->view('ems/bookings/view',$data);

			}

		}

		else {

				$this->load->view('ems/bookings/manage');
		}

	}

	public function delete()

	{
		$this->layout = '';

		$this->load->model('ems/model_inquries');

		$id = $_POST['id'];

		$result=$this->model_inquries->deleteBooking($id);

	}
	
	public function export_excel()
    {
		$this->load->library('excel');	
		$id_arr = array();
        if(html_escape($this->input->post('excel_type')) == 0)
        {
			$table = 'booking_comedian';
            $user_ids = $this->model_custom->fetchAll_Ids($table);
            foreach($user_ids as $val)
            {
                $id_arr[] = $val->id;	
            }
        }
        else
        {
            $id_arr = html_escape($this->input->post('id'));
        }	
		$i=0;
		$users_arr = array();
        foreach($id_arr as $id)
        {
			$table = 'booking_comedian';
            $users_arr[] = $this->model_custom->fetch_single($id,$table);	
			$i++;
        }
        $this->gen_xl($users_arr, 'booking-comedian-excel');
	}
	
	public function gen_xl($rr, $filename){

			$this->excel->getProperties()->setCreator("Soumya Biswas")
				 ->setLastModifiedBy("Soumya Biswas")
				 ->setTitle("Office 2007 XLSX Test Document")
				 ->setSubject("Office 2007 XLSX Test Document")
				 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
				 ->setKeywords("office 2007 openxml php")
				 ->setCategory("Test result file");				
		$border = array(
			'borders' => array(
			'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		); 				
		$ans = array(
		'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					  )
		);	 		 				 
		$arr = array(
			'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
		),
		'font'  => array(
			'bold'  => true,
			"color" => array("rgb" => "903")
			)
		);


		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($border);
			
	 
		$this->excel->getActiveSheet()->setCellValue('A2', 'Name');
		$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
			->getStyle('A2')
			->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()
			->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('B2', 'Email');
		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('B2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('C2', 'Phone');
		$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('C2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('D2', 'Comedian Name');
		$this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('D2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('E2', 'Activity');
		$this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('E2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('30');
		$this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('F2', 'Message');
		$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('F2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('G2', 'Submit Date');
		$this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($arr);
		$this->excel->getActiveSheet()
				->getStyle('G2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('20');
		$this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($border);
				
		 $n = 2;
		 foreach($rr as $ss){
		 $n++;
						  
		 $reg_date = strtotime($ss->created_at);
		 $date_as = date( 'M d, Y', $reg_date );
		 $time_as = date( 'H:i', $reg_date );

		$this->excel->getActiveSheet()->setCellValue('A'.$n, $ss->name);
		$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue( 'B' .$n, $ss->email);
		$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue( 'C' .$n, '"'.$ss->phone.'"');
		$this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue( 'D' .$n, pageTitle($ss->comedian_id,$ss->lang));
		$this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue( 'E' .$n, $ss->activity);
		$this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue('F'.$n, $ss->message);
		$this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($border);
		
		$this->excel->getActiveSheet()->setCellValue('G'.$n,$date_as);
		$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);
								
   } 

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$filename=$filename.".xls";
		
		$folder_path = '/home/public_html/uploads/exports/';
        $file = $folder_path.$filename;

		//Setting the header type
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
        $this->manage();
		
	}



}



/* End of file admin-login.php */

/* Location: ./application/controllers/ems/admin-login.php */