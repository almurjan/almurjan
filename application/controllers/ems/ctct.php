<?php

class ctct extends CI_Controller
{
    public function __construct()
    {
       parent::__construct();
	   $this->layout = 'admin_inner';
	   //$this->load->library('cc');
	   	$this->load->model('ems/model_configuration');
		$this->load->library('MCAPI');

		checkAdminSession();
        check_permission(17, 'any');
    }
    
	
	 public function index() {

        $this->manage();

    }
	
	 public function manage() {
         check_permission(17, 'read');
        $data = array();
        $settings = getConfigurationSetting();
        $list_id = $settings->mailChimp_list_id;
        $mc = new MCAPI($settings->mailChimp_api_key);
        //$data['users'] = $mc->listMembers($list_id, 'DESC');
        //$data['users'] = $this->model_user->fetchAllUsers('users');
		
		
		$api_key = $settings->mailChimp_api_key;
		$list_id = $settings->mailChimp_list_id;
		$dc = substr($api_key,strpos($api_key,'-')+1); // us5, us8 etc
		 
		// URL to connect
		$url = 'https://'.$dc.'.api.mailchimp.com/3.0/lists/'.$list_id;
		 
		// connect and get results
		$body = json_decode( $this->rudr_mailchimp_curl_connect( $url, 'GET', $api_key ) );
		 
		// number of members in this list
		$member_count = $body->stats->member_count;
		$emails = array();
		 
		for( $offset = 0; $offset < $member_count; $offset += 50 ) :
		 
			$data = array(
				'offset' => $offset,
				'count'  => 5000
			);
		 
			// URL to connect
			$url = 'https://'.$dc.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members';
		 
			// connect and get results
			$body = json_decode( $this->rudr_mailchimp_curl_connect( $url, 'GET', $api_key, $data ) );
		 
			foreach ( $body->members as $member ) {
				$emails[] = $member->email_address;
			}
		 
		endfor;
		$data['users'] = $emails;
		
        $this->load->view('ems/ctct/manage',$data);

    }
	
	
	function rudr_mailchimp_curl_connect( $url, $request_type, $api_key, $data = array() ) {
		if( $request_type == 'GET' )
			$url .= '?' . http_build_query($data);
	 
		$mch = curl_init();
		$headers = array(
			'Content-Type: application/json',
			'Authorization: Basic '.base64_encode( 'user:'. $api_key )
		);
		curl_setopt($mch, CURLOPT_URL, $url );
		curl_setopt($mch, CURLOPT_HTTPHEADER, $headers);
		//curl_setopt($mch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
		curl_setopt($mch, CURLOPT_RETURNTRANSFER, true); // do not echo the result, write it into variable
		curl_setopt($mch, CURLOPT_CUSTOMREQUEST, $request_type); // according to MailChimp API: POST/GET/PATCH/PUT/DELETE
		curl_setopt($mch, CURLOPT_TIMEOUT, 10);
		curl_setopt($mch, CURLOPT_SSL_VERIFYPEER, false); // certificate verification for TLS/SSL connection
	 
		if( $request_type != 'GET' ) {
			curl_setopt($mch, CURLOPT_POST, true);
			curl_setopt($mch, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
		}
	 
		return curl_exec($mch);
	}
	
	public function contactForm(){

		$id  = $this->uri->segment(4);
		$data['lists']  = $this->cc->view_Lits();
		  if(!empty($id)){
		$data['contact'] = $this->cc->edit_Contacts($id);  
		  }
        $this->load->view('ems/ctct/addContact',$data);  
   
		
	}
	
	  public function addcontact()
    {

        $this->cc->addContacts();
		
		redirect($this->config->item('base_url') . 'ems/ctct/manage');

    }
	
	
	 public function subscribe()
    {
		
		/*if(isset($_POST['email']) and !empty($_POST['list'])){
		
		if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		    echo  _erMsg2('<p>Please enter a valid email address...</p>');

			}else{*/
          
		    $this->cc->addContacts();
			
		
		  /*  echo  _okMsg('<p>Your email has been Subscribed Successfully.</p>');
		    
		  }*/
		if(isset($_POST['from_frontend']))
		{
			echo "User Subscribed";
			exit();
		}
		else
		{
			redirect($this->config->item('base_url'));
		}

		/*}*/
		
	 }
	
	  public function deletecontact()
    {
        check_permission(13, 'delete');
		  $this->cc->delete_Contacts($_POST['id']);
		
    }
	
	 public function campaign()
    {
        check_permission(13, 'read');
        $data['campaign_list'] = $this->cc->view_campaign();
        $this->load->view('ems/ctct/view_campaign',$data);  
                
    }
	public function campaignForm(){
		
		 $id   = $this->uri->segment(4);
		
		 $data = array();
		 $this->data['ckeditor'] = array(
            //ID of the textarea that will be replaced
            'id' => 'email_content',
            //Optionnal values
            'config' => array(
                'width' => "95%", 
                'height' => '200px',
				'fullpage' => true,
				'allowedContent' => true,
            )
        );
		$data = $this->data;
        $data['lists']  = $this->cc->view_Lits();
		$data['res']    = $this->model_configuration->fetchRow();

		if(!empty($id)){
		 
		$data['campaigns'] = $this->cc->edit_campaign($id);  
		}
		$this->load->view('ems/ctct/createCampaign',$data);  


		
	}
	
	 public function addCampaign()
    {
        check_permission(13, 'create');
		if (isset($_POST['name'])) {

      $this->cc->createCampaign($_POST);
    	
			
		}
       	redirect($this->config->item('base_url') . 'ems/ctct/campaign');
       
    }
	
	  public function deletecampaign()
    {
        check_permission(13, 'delete');
		  $this->cc->delete_Campaign($_POST['id']);
		
    }
  
}