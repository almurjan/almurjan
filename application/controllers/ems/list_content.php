<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class list_content extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        checkAdminSession();
		$this->load->model('ems/model_contents');
		$this->load->model('ems/model_content_images');
		$this->load->model('ems/model_career');
		$this->load->model('ems/model_inquries');

    }

    //main index function for the controller dashboard
    //loding the main view
    public function index() {
       $this->manage();
    }

    public function manage() {
        $data = array();
	    $id = $this->uri->segment(4);
        $is_benefits = $this->uri->segment(5);
        ($is_benefits!='')?:$is_benefits='';

        $data['parent_id'] = $id;
		$data['tpl_name'] = fetchTemplate($id);	
		
		$list_content_check = $this->model_contents->fetchRow($id);
        if($data['tpl_name'] == 'our_services' && ($list_content_check->listing_level == 1 || $list_content_check->listing_level == 2)){
			$data['list_content'] = $this->model_contents->front_end_content_service($id);
		}elseif($data['tpl_name'] == 'careers' && $is_benefits !=''){
            $data['list_content'] = $this->model_contents->backend_end_content($id,$is_benefits);
        }elseif($data['tpl_name'] == 'profile' && $is_benefits !=''){
            $data['list_content'] = $this->model_contents->backend_end_content($id,$is_benefits,$data['tpl_name']);
        }elseif($data['tpl_name'] == 'profile' && $is_benefits ==''){
            $data['list_content'] = $this->model_contents->backend_end_content($id,0,$data['tpl_name']);
        }else{
			$data['list_content'] = $this->model_contents->backend_end_content($id);
		}
		$this->load->view('ems/list_content/manage', $data);
    }

    //main index function for the controller dashboard
    //loding the main view
    public function category_index() {
       $this->manage_category();
    }
    public function manage_category() {
        $data = array();
        $id = $this->uri->segment(4);

        $data['list_content'] = $this->model_contents->fetch_list_content_category();

        $data['parent_id'] = $id;
        $data['tpl_name'] = fetchTemplate($id);
        $this->load->view('ems/list_content/manage_categories', $data);
    }
    public function index_video() {
       $this->manage_video();
    }
	
	public function list_content_projects() {
       $this->manage_projects();
    }

    public function manage_projects() {
        $data = array();
	    $id = $this->uri->segment(4);
		
	    $data['list_content'] = $this->model_contents->fetch_list_content_projects($id);
		
        $data['parent_id'] = $id;
		$data['tpl_name'] = fetchTemplate($id);		
        $this->load->view('ems/list_content/manage_projects', $data);
    }
    public function manage_video() {
        $data = array();
	    $id = $this->uri->segment(4);
		
	    $data['list_content'] = $this->model_contents->fetch_list_content($id);
        $data['parent_id'] = $id;
		$data['tpl_name'] = fetchTemplate($id);		
        $this->load->view('ems/list_content/manage_video', $data);
    }

    public function add() {

        $data = array();
		$data['tpl_id'] = $this->uri->segment(4);
		$data['template'] = fetchTemplate($data['tpl_id']);
		if($data['template']=='contact-us' || true){
			$this->load->library('google_maps');

			$config['center'] = '23.8859, 45.0792';
			$config['zoom'] = '5';
			$config['onclick'] = '
			
			var lat = event.latLng.lat();
					var lng = event.latLng.lng();
			$("#location").val(lat+","+ lng);
			
			$("#overlay_map").fadeOut(500);
			';
			$this->google_maps->initialize($config);
			
			$data['map'] = $this->google_maps->create_map();
		}
		$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/list_content/add', $data);
    }
	
    public function add_video() {

        $data = array();
		$data['tpl_id'] = $this->uri->segment(4);
		$data['template'] = fetchTemplate($data['tpl_id']);
		if($data['template']=='contact-us' || true){
			$this->load->library('google_maps');

			$config['center'] = '23.8859, 45.0792';
			$config['zoom'] = '5';
			$config['onclick'] = '
			
			var lat = event.latLng.lat();
					var lng = event.latLng.lng();
			$("#location").val(lat+","+ lng);
			
			$("#overlay_map").fadeOut(500);
			';
			$this->google_maps->initialize($config);
			
			$data['map'] = $this->google_maps->create_map();
		}
		$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/list_content/add_video', $data);
    }
    public function add_project() {

        $data = array();
		$data['tpl_id'] = $this->uri->segment(4);
		$data['template'] = fetchTemplate($data['tpl_id']);
		$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/list_content/add_project', $data);
    }

    public function add_category() {

        $data = array();
        $data['tpl_id'] = $this->uri->segment(4);
        $data['template'] = fetchTemplate($data['tpl_id']);
        $data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
        $this->load->view('ems/list_content/add_category', $data);
    }
	
	public function add_report() {

        $data = array();
		$data['tpl_id'] = $this->uri->segment(4);
		$data['template'] = fetchTemplate($data['tpl_id']);
		if($data['template']=='contact-us' || true){
			$this->load->library('google_maps');

			$config['center'] = '23.8859, 45.0792';
			$config['zoom'] = '5';
			$config['onclick'] = '
			
			var lat = event.latLng.lat();
					var lng = event.latLng.lng();
			$("#location").val(lat+","+ lng);
			
			$("#overlay_map").fadeOut(500);
			';
			$this->google_maps->initialize($config);
			
			$data['map'] = $this->google_maps->create_map();
		}
		$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/list_content/add_report', $data);
    }
	public function add_new_report() {

        $data = array();
		$data['tpl_id'] = $this->uri->segment(4);
		$data['template'] = fetchTemplate($data['tpl_id']);
		$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/announcement_and_reports/add', $data);
    }
	public function add_online_services_listing() {

        $data = array();
		$data['tpl_id'] = $this->uri->segment(4);
		$data['template'] = fetchTemplate($data['tpl_id']);
		if($data['template']=='contact-us' || true){
			$this->load->library('google_maps');

			$config['center'] = '23.8859, 45.0792';
			$config['zoom'] = '5';
			$config['onclick'] = '
			
			var lat = event.latLng.lat();
					var lng = event.latLng.lng();
			$("#location").val(lat+","+ lng);
			
			$("#overlay_map").fadeOut(500);
			';
			$this->google_maps->initialize($config);
			
			$data['map'] = $this->google_maps->create_map();
		}
		$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/list_content/add_online_services_listing', $data);
    }

    public function edit() {
        
        $id = $this->uri->segment(7);
		
		if($id == ''){
		$id = $this->uri->segment(5); }
		$data = array();
		
        if ($id) {  
			 $data['result'] = $this->model_contents->fetchRow($id);
			 
			if($data['result']->version != 0){
				$forTemplateOfVersion = $this->model_contents->fetchRow($this->uri->segment(5));
				$data['template'] = fetchTemplate($forTemplateOfVersion->parant_id);
				$data['check_request_to_view_version_page'] = 1;
				
				}else{
					$data['template'] = fetchTemplate($data['result']->parant_id);
					$data['total_versions'] =  $this->model_contents->countPageVersions($id);
					}
					
			if($data['template'] == 'our_services'){
				
				
				
			}		
			if($data['template']=='contact-us' || true){
				$this->load->library('google_maps');
				$map_cordinates = content_detail('location',$data['result']->id);
				$config['center'] = $map_cordinates;
				$map_cordinates = explode(',',$map_cordinates);
				$config['zoom'] = '5';
				$config['onclick'] = '

				var lat = event.latLng.lat();
				var lng = event.latLng.lng();
				$("#location").val(lat+","+ lng);

				$("#overlay_map").fadeOut(500);
				';
				$this->google_maps->initialize($config);
				
				$data['map']= $this->google_maps->create_map();
			}
			
			$data['page_id'] = $this->uri->segment(5);
			$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
                       if (!empty($data)) {
                $this->load->view('ems/list_content/edit', $data);
            }
        }
    }

    public function edit_video() {
          
        $id = $this->uri->segment(7);
		if($id == ''){
		$id = $this->uri->segment(5); }
		$data = array();
		
        if ($id) {            
            $data['result'] = $this->model_contents->fetchRow($id);
			if($data['result']->version != 0){
				$forTemplateOfVersion = $this->model_contents->fetchRow($this->uri->segment(5));
				$data['template'] = fetchTemplate($forTemplateOfVersion->parant_id);
				$data['check_request_to_view_version_page'] = 1;
				
				}else{
					$data['template'] = fetchTemplate($data['result']->parant_id);
					$data['total_versions'] =  $this->model_contents->countPageVersions($id);
					}
			if($data['template']=='contact-us' || true){
				$this->load->library('google_maps');
				$map_cordinates = content_detail('location',$data['result']->id);
				$config['center'] = $map_cordinates;
				$map_cordinates = explode(',',$map_cordinates);
				$config['zoom'] = '5';
				$config['onclick'] = '

				var lat = event.latLng.lat();
				var lng = event.latLng.lng();
				$("#location").val(lat+","+ lng);

				$("#overlay_map").fadeOut(500);
				';
				$this->google_maps->initialize($config);
				
				$data['map']= $this->google_maps->create_map();
			}
			
			$data['page_id'] = $this->uri->segment(5);
			$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
                       if (!empty($data)) {
                $this->load->view('ems/list_content/edit_video', $data);
            }
        }
    }
	
	
    public function updateProject() {
		
		$admin_lang = check_admin_lang();
        $id = html_escape($this->input->post('page_id'));
        $tpl_id = html_escape($this->input->post('tpl_id'));
		$data = array();
        $loggedInUserId = $this->session->userdata('id');
		
		//unset(html_escape($this->input->post('date')));
		
        $data['eng_title'] = addslashes(html_escape($this->input->post('eng_title')));
		$data['arb_title'] = addslashes(html_escape($this->input->post('arb_title')));
		$data['eng_sub_title'] = stripslashes(html_escape($this->input->post('eng_sub_title')));
		$data['arb_sub_title'] = addslashes(html_escape($this->input->post('arb_sub_title')));
		$data['chn_title'] = addslashes(html_escape($this->input->post('chn_title')));
		$data['display_to_home'] = html_escape($this->input->post('display_to_home'));

        $data['meta_title_eng'] = addslashes(html_escape($this->input->post('meta_title_eng')));
        $data['meta_desc_eng'] = addslashes(html_escape($this->input->post('meta_desc_eng')));
        $data['meta_keywords_eng'] = addslashes(html_escape($this->input->post('meta_keywords_eng')));

		$data['date'] = addslashes(html_escape($this->input->post('date')));
		$data['cms_country'] = addslashes(html_escape($this->input->post('cms_country')));
		$data['cms_city'] = addslashes(html_escape($this->input->post('cms_city')));
		$data['start_date'] = addslashes(html_escape($this->input->post('start_date')));
		$data['end_date'] = addslashes(html_escape($this->input->post('end_date')));

		$data['archive'] = addslashes(html_escape($this->input->post('archive')));
		//$data['color_code'] = addslashes(html_escape($this->input->post('color_code')));
		$data['eng_location'] = addslashes(html_escape($this->input->post('location')));
        $data['arb_location'] = addslashes(html_escape($this->input->post('arb_location')));

        $data['meta_title_arb'] = addslashes(html_escape($this->input->post('meta_title_arb')));
        $data['meta_desc_arb'] = addslashes(html_escape($this->input->post('meta_desc_arb')));
        $data['meta_keywords_arb'] = addslashes(html_escape($this->input->post('meta_keywords_arb')));
		
		$data['meta_title_chn'] = addslashes(html_escape($this->input->post('meta_title_chn')));
        $data['meta_desc_chn'] = addslashes(html_escape($this->input->post('meta_desc_chn')));
        $data['meta_keywords_chn'] = addslashes(html_escape($this->input->post('meta_keywords_chn')));
		$data['city'] = addslashes(html_escape($this->input->post('city')));
        $data['category'] = addslashes(html_escape($this->input->post('category')));
        $data['is_archieve'] = addslashes(html_escape($this->input->post('is_archieve')));
        $data['year'] = addslashes(html_escape($this->input->post('year')));
        $data['is_project'] = addslashes(html_escape($this->input->post('is_project')));
        $data['is_feature'] = addslashes(html_escape($this->input->post('is_feature')));
      
		//file_upload($_FILES,$id);
		//$data['parant_id'] = addslashes(html_escape($this->input->post('tpl_id')));
		//$redirect_check = $data['parant_id'];
		$data['type'] = 'page';
		
		$data['tpl'] = addslashes(html_escape($this->input->post('tpl')));
		if(html_escape($this->input->post('tpl_name')) == 'downloads' || html_escape($this->input->post('tpl_name')) == 'awareness'){
			if ($_FILES['file']['name'] != '') {

				$file_name_eng =  date('Ymdhsi').'-'.$_FILES['file']['name'];
				$file_size1 = $_FILES['file']['size'];
				$file_tmp_eng = $_FILES['file']['tmp_name'];
				$type1 = $_FILES['file']['type'];
				move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
				$data['file'] = $file_name_eng;
			}else{
				$empty = "";
				$data['file'] = html_escape($this->input->post('file_hdn'));
			}

			if ($_FILES['file_arb']['name'] != '') {

				$file_name_eng =  date('Ymdhsi').'-'.$_FILES['file_arb']['name'];
				$file_size1 = $_FILES['file_arb']['size'];
				$file_tmp_eng = $_FILES['file_arb']['tmp_name'];
				$type1 = $_FILES['file_arb']['type'];
				move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
				$data['file_arb'] = $file_name_eng;
			}else{
				$empty = "";
				$data['file_arb'] = html_escape($this->input->post('file_arb_hdn'));
			}
		}

        $data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_updated_by'] = $loggedInUserId;
        $res['result'] = $this->model_contents->update($data, $id);
		$data['parant_id'] = addslashes(html_escape($this->input->post('tpl_id')));
		$redirect_check = $data['parant_id'];
		$data['tpl'] = addslashes(html_escape($this->input->post('tpl')));

	    $this->model_contents->delete_content_detail($id);
	    //unset($_POST['date']);
	  
		foreach($_POST as $key => $val){

			$ar = array(
			'meta_title_eng','meta_desc_eng',
			'meta_keywords_eng','meta_title_arb',
			'meta_desc_arb','meta_keywords_arb','meta_title_chn',
			'meta_desc_chn','meta_keywords_chn','tpl','pub_status','eng_title','eng_sub_title','arb_title','arb_sub_title','chn_title','tpl_id','display_to_home','eng_pro_date','fleet_day','eng_location','arb_location','cv_pdf_file','event_document_file','event_document_file_arb','archive','job_type','career_level','career_location','country_id');
			if(!in_array($key ,$ar) and !empty($val)){
			
            $arr = array(
            'post_id' => $id,
            'meta_key' => $key,
            'meta_value' => $val

            );
		if( strpos($arr['meta_key'],"_mkey_hdn") !== false){
			$image_explode = explode('script/',$arr['meta_value']); 
			$arr['meta_value'] = $image_explode[1];
			$arr['meta_value'] = str_replace(',','',$arr['meta_value']);
			}
			$arr['meta_value'] = str_replace('src="../../','src="../../../../',$arr['meta_value']);	

		   $this->model_contents->save_content_detail($arr);
			}
		}

		$countVersion =  $this->model_contents->countPageVersions($id);
		
		$data['version'] = $countVersion + 1;
		$data['type'] = 'version';
		$data['pub_status'] = 0;
		$data['parant_id'] = $id;
		
		$version_id = $this->model_contents->save($data);
		log_insert($this->uri->segment(2), 'edit a record in');
        
        if ($res == true) {
            $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_updated']."</b></p>"));
        }
		redirect($this->config->item('base_url') . 'ems/list_content/edit_content_project/id/'.$id);
    }

    public function updateCategory() {

        $admin_lang = check_admin_lang();
        $id = html_escape($this->input->post('page_id'));
        $tpl_id = html_escape($this->input->post('tpl_id'));
        $data = array();
        $loggedInUserId = $this->session->userdata('id');

        //unset(html_escape($this->input->post('date')));

        $data['category_title'] = addslashes(html_escape($this->input->post('eng_title')));
        $data['category_title_ar'] = addslashes(html_escape($this->input->post('arb_title')));
        $data['category_desc_eng'] = addslashes(html_escape($this->input->post('category_desc_eng')));
        $data['category_desc_arb'] = addslashes(html_escape($this->input->post('category_desc_arb')));
        $data['parant_id'] = addslashes(html_escape($this->input->post('tpl_id')));
        $redirect_check = $data['parant_id'];
        $res['result'] = $this->model_contents->updateCategory($data, $id);

        log_insert($this->uri->segment(2), 'edit a record in');

        if ($res == true) {
            $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_updated']."</b></p>"));
        }
        redirect($this->config->item('base_url') . 'ems/list_content/edit_content_category/id/'.$id);
    }
	
    public function edit_content_project() {
          
        $id = $this->uri->segment(7);
		if($id == ''){
		$id = $this->uri->segment(5); }
		$data = array();
		
        if ($id) {            
            $data['result'] = $this->model_contents->fetchRow($id);
			if($data['result']->version != 0){
				$forTemplateOfVersion = $this->model_contents->fetchRow($this->uri->segment(5));
				$data['template'] = fetchTemplate($forTemplateOfVersion->parant_id);
				$data['check_request_to_view_version_page'] = 1;
				
				}else{
					$data['template'] = fetchTemplate($data['result']->parant_id);
					$data['total_versions'] =  $this->model_contents->countPageVersions($id);
					}
			if($data['template']=='contact-us' || true){
				$this->load->library('google_maps');
				$map_cordinates = content_detail('location',$data['result']->id);
				$config['center'] = $map_cordinates;
				$map_cordinates = explode(',',$map_cordinates);
				$config['zoom'] = '5';
				$config['onclick'] = '

				var lat = event.latLng.lat();
				var lng = event.latLng.lng();
				$("#location").val(lat+","+ lng);

				$("#overlay_map").fadeOut(500);
				';
				$this->google_maps->initialize($config);
				
				$data['map']= $this->google_maps->create_map();
			}
			
			$data['page_id'] = $this->uri->segment(5);
			$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
                       if (!empty($data)) {
                $this->load->view('ems/list_content/edit_project', $data);
            }
        }
    }

    public function edit_content_category() {

        $id = $this->uri->segment(7);
        if($id == ''){
            $id = $this->uri->segment(5); }
        $data = array();

        if ($id) {
            $data['result'] = $this->model_contents->fetchRowCategory($id);
            $data['template'] = fetchTemplate($data['result']->parant_id);
            //$data['total_versions'] =  $this->model_contents->countPageVersions($id);
            $data['page_id'] = $this->uri->segment(5);
            if (!empty($data)) {
                $this->load->view('ems/list_content/edit_category', $data);
            }
        }
    }

    public function view_event_registraion($id) {
          
        $id = $this->uri->segment(5);
		$data = array();
        if ($id) {            
            $data['res'] = $this->model_contents->fetchRowReservation($id);
		    if (!empty($data)) {
				$this->load->view('ems/event_registraion/view', $data);
			}
        }
    }

    public function publish() {
        $this->layout = '';
        $id = $_POST['id'];
        $status = $_POST['pub_status'];
        $result = $this->model_contents->publishStatus($status, $id);
    }
    public function categoryPublish() {
        $this->layout = '';
        $id = $_POST['id'];
        $status = $_POST['pub_status'];
        $result = $this->model_contents->categoryPublishStatus($status, $id);
    }

	
    public function activeSlider() {
        $this->layout = '';
        $id = $_POST['id'];
        $status = $_POST['pub_status'];
		
		if($status == 0){
			$status = 1;
		}
		else if($status == 1){
			$status = 0;
		}
		
        $result = $this->model_contents->activeSlider($status, $id);
    }

    public function attendence() {
        $this->layout = '';
        $id = $_POST['id'];
        $status = $_POST['pub_status'];
        $result = $this->model_contents->attendenceStatus($status, $id);
		
    }

    public function publish_compitition() {
        $this->layout = '';
        $id = $_POST['id'];
        $status = $_POST['pub_status'];
        $compitition = $this->model_contents->fetchRow($id);
		$list_content = $this->model_contents->fetch_list_content($compitition->parant_id);
        if($list_content){
			$i=0;
			foreach ($list_content as $result => $val) {
				$result = $this->model_contents->publishStatus(0, $val->id);
			}
		}	
		$result = $this->model_contents->publishStatus($status, $id);
    }

    public function delete() {
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_contents->delete($id);
        $this->deletePhotos($id);
    }

    public function deleteRegistration() {
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_contents->deleteRegistration($id);
        $this->deletePhotos($id);
    }

}

/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */