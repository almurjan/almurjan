<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class configuration extends CI_Controller {

	public $layout = 'admin_inner';
	
	
	 
	function __construct() {
		parent::__construct();
		$this->layout = 'admin_inner'; 	
                checkAdminSession();
				check_permission(2, 'any');
    }
	
	//main index function for the controller dashboard
	//loding the main view
	public function index(){
	check_permission(2, 'read');	
	$this->load->model('ems/model_configuration');
	$rec['res']=$this->model_configuration->fetchRow();
	//$rec['lists']  = $this->cc->view_Lits();
		{
			$this->load->view('ems/configuration/edit',$rec);
		}
	
		
	}
        
	public function manage(){
	check_permission(2, 'read');	

	$this->load->model('ems/model_configuration');
	$rec['res']=$this->model_configuration->fetchRow();
		{
			$this->load->view('ems/configuration/edit',$rec);
		}
	
		
	}        

	
	public function save(){
		check_permission(2, 'update');
		$admin_lang = check_admin_lang();
		$this->load->model('ems/model_configuration');
		$id = $this->uri->segment(5);
		$data=array();
		$loggedInUserId=$this->session->userdata('id');
		/////// email setting /////////////
		$data['reachus_email']=removeJsScripts(html_escape($this->input->post('reachus_email')));
		$data['sendinquiry_email']=removeJsScripts(html_escape($this->input->post('sendinquiry_email')));
		$data['jobapp_email']=removeJsScripts(html_escape($this->input->post('jobapp_email')));
		$data['from_email']=html_escape($this->input->post('from_email'));
		$data['newsletter_section']=removeJsScripts(html_escape($this->input->post('newsletter_section')));
		$data['project_name']= addslashes(removeJsScripts(html_escape($this->input->post('project_name'))));
		$data['project_name_arb']=addslashes(removeJsScripts(html_escape($this->input->post('project_name_arb'))));
		
		///////// google analytics ///////////////
		$data['ga_user']=removeJsScripts(html_escape($this->input->post('ga_user')));
		if(str_replace(' ','',html_escape($this->input->post('ga_password'))) != ''){
			$data['ga_password'] = html_escape($this->input->post('ga_password'));
		}
		$data['ga_tracking_id']=removeJsScripts(html_escape($this->input->post('ga_tracking_id')));
		$data['ga_view_id']=removeJsScripts(html_escape($this->input->post('ga_view_id'))); 
		$data['our_email']=html_escape($this->input->post('our_email')); 
		$data['phone']=html_escape($this->input->post('phone')); 
										
		///////// smtp settings ///////////////
		$data['smtp_host']=html_escape($this->input->post('smtp_host'));
		$data['smtp_email']=html_escape($this->input->post('smtp_email'));
		$data['smtp_password']=html_escape($this->input->post('smtp_password'));
		$data['smtp_port']=html_escape($this->input->post('smtp_port'));
		$data['ctct_api_key']=html_escape($this->input->post('ctct_api_key'));
		$data['ctct_token']=html_escape($this->input->post('ctct_token'));
		$data['ctct_from_email']=html_escape($this->input->post('ctct_from_email'));
		$data['ctct_list']=html_escape($this->input->post('ctct_list'));				
		$data['mailChimp_api_key']=html_escape($this->input->post('mailChimp_api_key'));				
		$data['mailChimp_list_id']=html_escape($this->input->post('mailChimp_list_id'));
		///////// sms settings ///////////////
		$data['arb_sms']=html_escape($this->input->post('arb_sms'));
		$data['eng_sms']=html_escape($this->input->post('eng_sms'));
						
		$data['config_created_at']=date('Y-m-d H:i:s');
		$data['config_created_by']=$loggedInUserId;
		$data['pub_status']=html_escape($this->input->post('pub_val'));
		$data['contact_email']=html_escape($this->input->post('contact_email'));
		$data['career_email']=html_escape($this->input->post('career_email'));
		$data['newsletter_email']=html_escape($this->input->post('newsletter_email'));
		$data['smtp_crypto']=html_escape($this->input->post('smtp_crypto'));
		$data['live_chat']=html_escape($this->input->post('live_chat'));
		$data['in_head_tag']= addslashes(html_escape($this->input->post('in_head_tag')));
		$data['in_body_tag']= addslashes(html_escape($this->input->post('in_body_tag')));
						
		$numRows=$this->model_configuration->fetchRow();
		if($numRows==false)
		{
			log_insert($this->uri->segment(2),'add a record in');	
			$result=$this->model_configuration->save($data);
			$this->session->set_flashdata('message', _okMsg("<p>".$admin_lang['label']['record_saved']."</p>"));
		}
		else 
		{
			log_insert($this->uri->segment(2),'update a record in');
//					debug($numRows);
			$rowId=$numRows->id;
			$data['config_updated_at']=date('Y-m-d H:i:s');
			$data['config_updated_by']=$loggedInUserId;
			$result=$this->model_configuration->update($data,$rowId);
			$this->session->set_flashdata('message', _okMsg("<p>".$admin_lang['label']['record_updated']."</p>"));
		}
			
		$data2['res'][0]=$data;
			
			
			
		redirect($this->config->item('base_url') . 'ems/configuration');
			
			
	}
	
}

/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */