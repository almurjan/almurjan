<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Countries extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        $this->load->model('ems/model_countries');
		$this->load->library('email');
        checkAdminSession();
    }

    public function index() {
        $this->manage();
    }

    public function manage() {
		$data = array();
			  $data['countries'] = $this->model_countries->fetchAllAnnouncement('cms_countries');
			  $this->load->view('ems/countries/manage', $data);
    }

	public function add() {

			  $this->load->view('ems/countries/add');

    }

	public function addEventType() {

        $data = array();
		$data['eng_title'] = html_escape($this->input->post('eng_title'));
		$data['arb_title'] = html_escape($this->input->post('arb_title'));
		$data['status'] = '1';

        if ($_FILES['image']['name'] != '') {
            $file_name = $_FILES['image']['name'].'-'.date('Ymdhsi');
            $file_size = $_FILES['image']['size'];
            $file_tmp = $_FILES['image']['tmp_name'];
            $type = $_FILES['image']['type'];
            $element1 = explode('/', $type);
            $file_type = $element1[1];
            $new_file_name = $file_name . '.' . $file_type;
            move_uploaded_file($file_tmp, "uploads/event_types/" . $file_name);
            $data['icon'] = $file_name;
        }

		$insert_id = $this->model_countries->saveEventType($data);
		if ($insert_id)
		{
			$this->session->set_flashdata('message', _okMsg('<p>Country added successfully.</p>'));
			redirect($this->config->item('base_url') . 'ems/countries/manage');
		}
    }

	public function updateEventType() {

		$id = $_POST['id'];
        $data = array();
        $data['eng_title'] = html_escape($this->input->post('eng_title'));
        $data['arb_title'] = html_escape($this->input->post('arb_title'));

        /*if ($_FILES['image']['name'] != '') {
            $file_name = $_FILES['image']['name'].'-'.date('Ymdhsi');
            $file_size = $_FILES['image']['size'];
            $file_tmp = $_FILES['image']['tmp_name'];
            $type = $_FILES['image']['type'];
            $element1 = explode('/', $type);
            $file_type = $element1[1];
            $new_file_name = $file_name . '.' . $file_type;
            move_uploaded_file($file_tmp, "uploads/event_types/" . $file_name);
            $data['icon'] = $file_name;
        }else{
            $data['icon'] = html_escape($this->input->post('file_name'));
        }*/

		$insert_id = $this->model_countries->updateEventType($id, $data);
		if ($insert_id)
		{
			$this->session->set_flashdata('message', _okMsg('<p>Country updated successfully.</p>'));
			redirect($this->config->item('base_url') . 'ems/countries/manage');
		}
    }

    public function edit() {
          $data = array();
        $id = $this->uri->segment(5);
        if ($id) {
            $data['event_type'] = $this->model_countries->fetch($id,'cms_countries');
            if (!empty($data)) {
                $this->load->view('ems/countries/edit', $data);
            }
        }
    }

    public function view() {
        $data = array();
        $id = $this->uri->segment(5);
        if ($id) {
            $data['event_type'] = $this->model_countries->fetch($id,'cms_countries');
            if (!empty($data)) {
                $this->load->view('ems/countries/view', $data);
            }
        }
    }

    public function publish()
    {
        $this->layout = '';
        $this->load->model('ems/model_countries');
        $id = $_POST['id'];
        $status['status']= $_POST['pub_status'];
        $result=$this->model_countries->publishStatus($status,$id);
    }

  public function delete() {
	    $lang = $this->session->userdata('lang');
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_countries->deleteEventType($id);
        echo $result;
       // $this->deletePhotos($id);
    }
}
/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */