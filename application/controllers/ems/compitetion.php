<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class compitetion extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        checkAdminSession();
		$this->load->model('ems/model_contents');
		$this->load->model('ems/model_content_images');
		$this->load->model('ems/model_career');



    }

    //main index function for the controller dashboard
    //loding the main view
    public function index() {
       $this->manage();
    }

    public function objectives() {
        $data = array();
	    $id = $this->uri->segment(4);
		
	    $data['compitetion'] = $this->model_contents->fetch_compitition_objectives($id);
		$data['parent_id'] = $id;
		$data['tpl_name'] = fetchTemplate($id);		
        $this->load->view('ems/compitition_objectives/manage', $data);
    }

    public function add() {

        $data = array();
		$data['tpl_id'] = $this->uri->segment(4);
		$data['template'] = fetchTemplate($data['tpl_id']);
		if($data['template']=='contact-us' || true){
			$this->load->library('google_maps');

			$config['center'] = '23.8859, 45.0792';
			$config['zoom'] = '5';
			$config['onclick'] = '
			
			var lat = event.latLng.lat();
					var lng = event.latLng.lng();
			$("#location").val(lat+","+ lng);
			
			$("#overlay_map").fadeOut(500);
			';
			$this->google_maps->initialize($config);
			
			$data['map'] = $this->google_maps->create_map();
		}
		$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/compitition_objectives/add', $data);
    }

    public function edit() {
          
        $id = $this->uri->segment(7);
		if($id == ''){
		$id = $this->uri->segment(5); }
		$data = array();
		
        if ($id) {            
            $data['result'] = $this->model_contents->fetchRowObjectives($id);
			if($data['result']->version != 0){
				$forTemplateOfVersion = $this->model_contents->fetchRow($this->uri->segment(5));
				$data['template'] = fetchTemplate($forTemplateOfVersion->parant_id);
				$data['check_request_to_view_version_page'] = 1;
				
				}else{
					$data['template'] = fetchTemplate($data['result']->parant_id);
					$data['total_versions'] =  $this->model_contents->countPageVersions($id);
					}
			if($data['template']=='contact-us' || true){
				$this->load->library('google_maps');
				$map_cordinates = content_detail('location',$data['result']->id);
				$config['center'] = $map_cordinates;
				$map_cordinates = explode(',',$map_cordinates);
				$config['zoom'] = '5';
				$config['onclick'] = '

				var lat = event.latLng.lat();
				var lng = event.latLng.lng();
				$("#location").val(lat+","+ lng);

				$("#overlay_map").fadeOut(500);
				';
				$this->google_maps->initialize($config);
				
				$data['map']= $this->google_maps->create_map();
			}
			
			$data['page_id'] = $this->uri->segment(5);
			$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
                       if (!empty($data)) {
                $this->load->view('ems/compitition_objectives/edit', $data);
            }
        }
    }
	
	
     public function saveCompetition() {
		 $admin_lang = check_admin_lang();
		$data = array();
        $loggedInUserId = $this->session->userdata('id');
		
        $data['eng_title'] = addslashes(html_escape($this->input->post('eng_title')));
        $data['eng_desc'] = addslashes(html_escape($this->input->post('eng_desc')));
		$data['arb_title'] = addslashes(html_escape($this->input->post('arb_title')));
		$data['arb_desc'] = addslashes(html_escape($this->input->post('arb_desc')));
		$data['eng_sub_title'] = addslashes(html_escape($this->input->post('eng_sub_title')));
		$data['arb_sub_title'] = addslashes(html_escape($this->input->post('arb_sub_title')));
		
        $data['meta_title_eng'] = addslashes(html_escape($this->input->post('meta_title_eng')));
        $data['meta_desc_eng'] = addslashes(html_escape($this->input->post('meta_desc_eng')));
        $data['meta_keywords_eng'] = addslashes(html_escape($this->input->post('meta_keywords_eng')));
		

        $data['meta_title_arb'] = addslashes(html_escape($this->input->post('meta_title_arb')));
        $data['meta_desc_arb'] = addslashes(html_escape($this->input->post('meta_desc_arb')));
        $data['meta_keywords_arb'] = addslashes(html_escape($this->input->post('meta_keywords_arb')));
		
		$data['date'] = addslashes(html_escape($this->input->post('date')));
		$data['parant_id'] = addslashes(html_escape($this->input->post('parant_id')));
		
        $data['pub_status'] = 1;
		$data['logo_image'] = addslashes(html_escape($this->input->post('eng_image_mkey_hdn')));
		
		 if ($_FILES['file']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file']['name'];
			 $file_size1 = $_FILES['file']['size'];
			 $file_tmp_eng = $_FILES['file']['tmp_name'];
			 $type1 = $_FILES['file']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file'] = $file_name_eng;
		 }
		 if ($_FILES['file_arb']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file_arb']['name'];
			 $file_size1 = $_FILES['file_arb']['size'];
			 $file_tmp_eng = $_FILES['file_arb']['tmp_name'];
			 $type1 = $_FILES['file_arb']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file_arb'] = $file_name_eng;
		 }
		 
		$id = $this->model_contents->save_objectives($data);	
		
		if($id){
			redirect($this->config->item('base_url') . 'ems/compitetion/edit/id/'.$id);
		}
    }
	
	
    public function update() {
		$data = array();
        $loggedInUserId = $this->session->userdata('id');

        $id = addslashes(html_escape($this->input->post('id')));
        $data['eng_title'] = addslashes(html_escape($this->input->post('eng_title')));
        $data['eng_desc'] = addslashes(html_escape($this->input->post('eng_desc')));
		$data['arb_title'] = addslashes(html_escape($this->input->post('arb_title')));
		$data['arb_desc'] = addslashes(html_escape($this->input->post('arb_desc')));
		$data['eng_sub_title'] = addslashes(html_escape($this->input->post('eng_sub_title')));
		$data['arb_sub_title'] = addslashes(html_escape($this->input->post('arb_sub_title')));
		
        $data['meta_title_eng'] = addslashes(html_escape($this->input->post('meta_title_eng')));
        $data['meta_desc_eng'] = addslashes(html_escape($this->input->post('meta_desc_eng')));
        $data['meta_keywords_eng'] = addslashes(html_escape($this->input->post('meta_keywords_eng')));
		

        $data['meta_title_arb'] = addslashes(html_escape($this->input->post('meta_title_arb')));
        $data['meta_desc_arb'] = addslashes(html_escape($this->input->post('meta_desc_arb')));
        $data['meta_keywords_arb'] = addslashes(html_escape($this->input->post('meta_keywords_arb')));
		
		$data['date'] = addslashes(html_escape($this->input->post('date')));
		$data['parant_id'] = addslashes(html_escape($this->input->post('parant_id')));
		$data['logo_image'] = addslashes(html_escape($this->input->post('eng_image_mkey_hdn')));
		
        $data['pub_status'] = 1;
       
		 if ($_FILES['file']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file']['name'];
			 $file_size1 = $_FILES['file']['size'];
			 $file_tmp_eng = $_FILES['file']['tmp_name'];
			 $type1 = $_FILES['file']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file'] = $file_name_eng;
		 }
		 if ($_FILES['file_arb']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file_arb']['name'];
			 $file_size1 = $_FILES['file_arb']['size'];
			 $file_tmp_eng = $_FILES['file_arb']['tmp_name'];
			 $type1 = $_FILES['file_arb']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file_arb'] = $file_name_eng;
		 }

        $data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_updated_by'] = $loggedInUserId;
        $res['result'] = $this->model_contents->updateObjectives($data, $id);
		
        if ($res == true) {
            $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_updated']."</b></p>"));
        }
		redirect($this->config->item('base_url') . 'ems/compitetion/edit/id/'.$id);
    }

	
    public function publish() {
        $this->layout = '';
        $id = $_POST['id'];
        $status = $_POST['pub_status'];
        $result = $this->model_contents->publishStatusObjectives($status, $id);
    }

    public function delete() {
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_contents->deleteObjectives($id);
        $this->deletePhotos($id);
    }
	
	
	
    public function awards() {
        $data = array();
	    $id = $this->uri->segment(4);
		
	    $data['compitetion'] = $this->model_contents->fetch_compitition_awards($id);
		$data['parent_id'] = $id;
		$data['tpl_name'] = fetchTemplate($id);		
        $this->load->view('ems/awards/manage', $data);
    }
	
	
    public function submit_participates() {
        $data = array();
	    $id = $this->uri->segment(4);
		
	    $data['compitetion_winners'] = $this->model_contents->fetch_compitition_participates($id);
		$data['parent_id'] = $id;
		$data['tpl_name'] = fetchTemplate($id);		
        $this->load->view('ems/awards/manage_winner', $data);
    }
	
    public function addAward() {

        $data = array();
		$data['tpl_id'] = $this->uri->segment(4);
		$data['template'] = fetchTemplate($data['tpl_id']);
		if($data['template']=='contact-us' || true){
			$this->load->library('google_maps');

			$config['center'] = '23.8859, 45.0792';
			$config['zoom'] = '5';
			$config['onclick'] = '
			
			var lat = event.latLng.lat();
					var lng = event.latLng.lng();
			$("#location").val(lat+","+ lng);
			
			$("#overlay_map").fadeOut(500);
			';
			$this->google_maps->initialize($config);
			
			$data['map'] = $this->google_maps->create_map();
		}
		$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
		$this->load->view('ems/awards/add', $data);
    }
	
	
	
     public function saveAward() {
		 $admin_lang = check_admin_lang();
		$data = array();
        $loggedInUserId = $this->session->userdata('id');
		
        $data['eng_title'] = addslashes(html_escape($this->input->post('eng_title')));
        $data['eng_desc'] = addslashes(html_escape($this->input->post('eng_desc')));
		$data['arb_title'] = addslashes(html_escape($this->input->post('arb_title')));
		$data['arb_desc'] = addslashes(html_escape($this->input->post('arb_desc')));
		$data['eng_sub_title'] = addslashes(html_escape($this->input->post('eng_sub_title')));
		$data['arb_sub_title'] = addslashes(html_escape($this->input->post('arb_sub_title')));
		
        $data['meta_title_eng'] = addslashes(html_escape($this->input->post('meta_title_eng')));
        $data['meta_desc_eng'] = addslashes(html_escape($this->input->post('meta_desc_eng')));
        $data['meta_keywords_eng'] = addslashes(html_escape($this->input->post('meta_keywords_eng')));
		

        $data['meta_title_arb'] = addslashes(html_escape($this->input->post('meta_title_arb')));
        $data['meta_desc_arb'] = addslashes(html_escape($this->input->post('meta_desc_arb')));
        $data['meta_keywords_arb'] = addslashes(html_escape($this->input->post('meta_keywords_arb')));
		
		$data['date'] = addslashes(html_escape($this->input->post('date')));
		$data['parant_id'] = addslashes(html_escape($this->input->post('parant_id')));
		
        $data['pub_status'] = 1;
		$data['logo_image'] = addslashes(html_escape($this->input->post('eng_image_mkey_hdn')));
		
		 if ($_FILES['file']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file']['name'];
			 $file_size1 = $_FILES['file']['size'];
			 $file_tmp_eng = $_FILES['file']['tmp_name'];
			 $type1 = $_FILES['file']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file'] = $file_name_eng;
		 }
		 if ($_FILES['file_arb']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file_arb']['name'];
			 $file_size1 = $_FILES['file_arb']['size'];
			 $file_tmp_eng = $_FILES['file_arb']['tmp_name'];
			 $type1 = $_FILES['file_arb']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file_arb'] = $file_name_eng;
		 }
		 
		$id = $this->model_contents->save_award($data);	
		
		if($id){
			redirect($this->config->item('base_url') . 'ems/compitetion/edit_award/id/'.$id);
		}
    }
	
	
    public function edit_award() {
          
        $id = $this->uri->segment(7);
		if($id == ''){
		$id = $this->uri->segment(5); }
		$data = array();
		
        if ($id) {            
            $data['result'] = $this->model_contents->compitition_awards($id);
			if($data['result']->version != 0){
				$forTemplateOfVersion = $this->model_contents->fetchRow($this->uri->segment(5));
				$data['template'] = fetchTemplate($forTemplateOfVersion->parant_id);
				$data['check_request_to_view_version_page'] = 1;
				
				}else{
					$data['template'] = fetchTemplate($data['result']->parant_id);
					$data['total_versions'] =  $this->model_contents->countPageVersions($id);
					}
			if($data['template']=='contact-us' || true){
				$this->load->library('google_maps');
				$map_cordinates = content_detail('location',$data['result']->id);
				$config['center'] = $map_cordinates;
				$map_cordinates = explode(',',$map_cordinates);
				$config['zoom'] = '5';
				$config['onclick'] = '

				var lat = event.latLng.lat();
				var lng = event.latLng.lng();
				$("#location").val(lat+","+ lng);

				$("#overlay_map").fadeOut(500);
				';
				$this->google_maps->initialize($config);
				
				$data['map']= $this->google_maps->create_map();
			}
			
			$data['page_id'] = $this->uri->segment(5);
			$data['list_content_dropdown'] = $this->model_contents->list_content_dropdown();
                       if (!empty($data)) {
                $this->load->view('ems/awards/edit', $data);
            }
        }
    }
	
	
	
    public function update_awards() {
		$data = array();
        $loggedInUserId = $this->session->userdata('id');

        $id = addslashes(html_escape($this->input->post('id')));
        $data['eng_title'] = addslashes(html_escape($this->input->post('eng_title')));
        $data['eng_desc'] = addslashes(html_escape($this->input->post('eng_desc')));
		$data['arb_title'] = addslashes(html_escape($this->input->post('arb_title')));
		$data['arb_desc'] = addslashes(html_escape($this->input->post('arb_desc')));
		$data['eng_sub_title'] = addslashes(html_escape($this->input->post('eng_sub_title')));
		$data['arb_sub_title'] = addslashes(html_escape($this->input->post('arb_sub_title')));
		
        $data['meta_title_eng'] = addslashes(html_escape($this->input->post('meta_title_eng')));
        $data['meta_desc_eng'] = addslashes(html_escape($this->input->post('meta_desc_eng')));
        $data['meta_keywords_eng'] = addslashes(html_escape($this->input->post('meta_keywords_eng')));
		

        $data['meta_title_arb'] = addslashes(html_escape($this->input->post('meta_title_arb')));
        $data['meta_desc_arb'] = addslashes(html_escape($this->input->post('meta_desc_arb')));
        $data['meta_keywords_arb'] = addslashes(html_escape($this->input->post('meta_keywords_arb')));
		
		$data['date'] = addslashes(html_escape($this->input->post('date')));
		$data['parant_id'] = addslashes(html_escape($this->input->post('parant_id')));
		$data['logo_image'] = addslashes(html_escape($this->input->post('eng_image_mkey_hdn')));
		
        $data['pub_status'] = 1;
       
		 if ($_FILES['file']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file']['name'];
			 $file_size1 = $_FILES['file']['size'];
			 $file_tmp_eng = $_FILES['file']['tmp_name'];
			 $type1 = $_FILES['file']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file'] = $file_name_eng;
		 }
		 if ($_FILES['file_arb']['name'] != '') {

			 $file_name_eng =  date('Ymdhsi').'-'.$_FILES['file_arb']['name'];
			 $file_size1 = $_FILES['file_arb']['size'];
			 $file_tmp_eng = $_FILES['file_arb']['tmp_name'];
			 $type1 = $_FILES['file_arb']['type'];
			 move_uploaded_file($file_tmp_eng, "uploads/pdf/" . $file_name_eng);
			 $data['file_arb'] = $file_name_eng;
		 }

        $data['content_updated_at'] = date('Y-m-d H:i:s');
        $data['content_updated_by'] = $loggedInUserId;
        $res['result'] = $this->model_contents->updateAwards($data, $id);
		
        if ($res == true) {
            $this->session->set_flashdata('message', _okMsg("<p><b>".$admin_lang['label']['record_updated']."</b></p>"));
        }
		redirect($this->config->item('base_url') . 'ems/compitetion/edit_award/id/'.$id);
    }

	
    public function publish_awards() {
        $this->layout = '';
        $id = $_POST['id'];
        $status = $_POST['pub_status'];
        $result = $this->model_contents->publishStatusObjectives($status, $id);
    }

    public function delete_awards() {
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_contents->deleteObjectives($id);
        $this->deletePhotos($id);
    }
	

}

/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */