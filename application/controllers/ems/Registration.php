<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Registration extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        $this->load->model('ems/model_user');
		$this->load->library('email');
        checkAdminSession();
    }

    public function index() {
        $this->manage();
    }

    public function manage() {
		$data = array();
			  $data['registrations'] = $this->model_user->fetchAllRegistrations('users');
			  $this->load->view('ems/Registrations/manage', $data);
    }

    public function view() {
        $data = array();
        $id = $this->uri->segment(5);
        if ($id) {
            $data['registration_data'] = $this->model_user->fetchRegistration($id,'users');
            if (!empty($data)) {
                $this->load->view('ems/Registrations/view', $data);
            }
        }
    }

    public function publish()
    {
        $this->layout = '';
        $this->load->model('ems/model_user');
        $id = $_POST['id'];
        $status= $_POST['pub_status'];
        $result=$this->model_user->publishStatus($status,$id);
    }

  public function delete() {
	  $lang = $this->session->userdata('lang');
        $this->layout = '';
        $id = $_POST['id'];
        log_insert($this->uri->segment(2), 'delete a record in');
        $result = $this->model_user->delete($id);
        echo $result;
       // $this->deletePhotos($id);
    }
}
/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */