<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu extends CI_Controller {

    public $layout = 'admin_inner';

    function __construct() {
        parent::__construct();
        $this->layout = 'admin_inner';
        checkAdminSession();
		check_permission(14, 'any');
		$this->load->model('ems/model_menu');
		$this->load->model('ems/model_contents');
		
    }
     public function index() {
        check_permission(14, 'read');
		$this->manage();
    }
    public function manage() {
        check_permission(14, 'read');
		$data = array();
		$data['menus'] =  $this->model_menu->fetchAll();
		$this->load->view('ems/menu/manage',$data);
		
    }
	 public function addMenu() {
        
		check_permission(14, 'create');
		$data = array();
		
		$this->load->view('ems/menu/add',$data);
		
    }
	
	public function save() {
        check_permission(14, 'create');
		$data = array();
		$loggedInUserId = $this->session->userdata('id');
		$data = $this->input->post();
		foreach($data as $key => $value)
		{
			if($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response')
			{
				$value = removeJsScripts(html_escape($value));
				$data[$key] = $value;	
			}
		}
		$data['menu_created_at'] = date('Y-m-d H:i:s');
		$data['menu_created_by'] = $loggedInUserId;
		
		$insert_id = $this->model_menu->save($data);
		
		log_insert($this->uri->segment(2), 'add a record in');
		$this->session->set_flashdata('message', _okMsg('<p>Menu added successfully.</p>'));
        redirect($this->config->item('base_url') . 'ems/menu/manage/');
    }
	
	public function edit() {
        
		check_permission(14, 'update');
		$id = $this->uri->segment(5); 
		$data['result'] = $this->model_menu->fetchRow($id);
		$this->load->view('ems/menu/edit', $data);
    }
	public function publish()
	{
		check_permission(14, 'publish');
		$this->layout = '';
		$id     = $_POST['id'];
		$status = $_POST['pub_status'];
		$result = $this->model_menu->publishStatus($status, $id);
	}
	public function delete()
	{
		check_permission(14, 'delete');
		$this->layout = '';
		$id     = $_POST['id'];
		$result = $this->model_menu->delete($id);
	}
	 public function update() {
		check_permission(14, 'update');
        $id = $this->uri->segment(5); 
		$loggedInUserId = $this->session->userdata('id');
		
		$data = array();
        $data['title'] = removeJsScripts(html_escape($this->input->post('title'))); 
        $data['arb_title'] = removeJsScripts(html_escape($this->input->post('arb_title'))); 
        $data['menu_updated_at'] = date('Y-m-d H:i:s');
        $data['menu_updated_by'] = $loggedInUserId;
        
		
		$res['result'] = $this->model_menu->update($data, $id);
		log_insert($this->uri->segment(2), 'edit a record in');
        
        if ($res == true) {
            $this->session->set_flashdata('message', _okMsg('<p>Record Updated Successfully.</p>'));
        }
        redirect($this->config->item('base_url') . 'ems/menu/edit/id/'.$id);
    }
	
	public function addMenuPages() {
        
		
		
		$id = $this->uri->segment(5); 
		$move_to_trash = 0;
		$data = array();
		$menu = array();
		$data['result'] = $this->model_menu->fetchRow($id);
		$data['pages'] = $this->model_contents->fetchAll($move_to_trash);
		$data['menu_pages'] = $this->model_menu->fetchMenuPages($id);
		
		foreach($data['menu_pages'] as $page){
			$menu[] = $page->page_id;
			}
		$data['menu_ids'] = $menu;	
		$this->load->view('ems/menu/addMenuPages', $data);
    }
	
		public function saveMenuPages() {
         
		 $data  = array();
		 $i = 0;
		 $data['menu_id'] = html_escape($this->input->post('menu_id'));
		 $parant_ids = html_escape($this->input->post('parant_id'));
		 $position = html_escape($this->input->post('position'));
		 $data['position'] = 0;
		 $previous_pages = $this->model_menu->fetchMenuPages($data['menu_id']);
		 $previous_pages_ids = array();
		 $pages = html_escape($this->input->post('page_id'));
		 foreach($previous_pages as $value){
			 $previous_pages_ids[] = $value->page_id;
			 
			 }
			
		 // $this->model_menu->deleteSaveMenu($data['menu_id']);
		 foreach($pages as $page){
			 
			 if(!in_array($page,$previous_pages_ids)){
				
			  $data['parent_id'] = $parant_ids[$i];
			  $data['position'] = $position[$i];
			  $data['page_id'] = $page;
			  if($data['position'] == 0){
				  $data['position'] = $this->model_menu->maxPosition() + 1;
				  }
			  $this->model_menu->saveMenuPage($data);
			  //$data['position'] = $data['position'] + 1;
			 }
			 $i++;
			 }
			 foreach($previous_pages_ids as $page){
				  if(!in_array($page,$pages)){
					  $this->model_menu->deleteSaveMenuSinglePage($data['menu_id'],$page); 
					  
					  }
				 
				 }
			 
			 $this->session->set_flashdata('message', _okMsg('<p>Page Added Successfully.</p>'));
			// redirect($this->config->item('base_url') . 'ems/menu/manageMenuPages/'.html_escape($this->input->post('menu_id')));
			 redirect($this->config->item('base_url') . 'ems/menu');
    }
	
	public function manageMenuPages() {
        
		$data = array();
		$id = $this->uri->segment(4); 
		$move_to_trash = 0;
		$parrent_id = 0;
		$data['menu_id'] = $id;
		$data['menus'] =  $this->model_menu->fetchAll();
		$data['pages'] = $this->model_contents->fetchAll($move_to_trash);
		$data['menu_pages'] = $this->model_menu->fetchMenuParrentPages($id,$parrent_id);
		$this->load->view('ems/menu/manageMenuPages',$data);
		
    }
	
	
	 public function menu_pos_update() {
	  $this->model_menu->delete_menu($_REQUEST['id']);
       foreach($_REQUEST['sortable'] as $key => $val){
		if(!empty($val['item_id'])){
		$data = array();
		$data['page_id']   = $val['item_id']; 
        $data['menu_id']   = $_REQUEST['id']; 
        $data['parent_id'] = isset($val['parent_id'])?$val['parent_id']:0;
        $data['position']  = $key;
		$this->model_menu->saveMenuPage($data);
		
	   }

	   }
          log_insert($this->uri->segment(2), 'save a record in');
        
        if ($res == true) {
            $this->session->set_flashdata('message', _okMsg('<p>Record Saved Successfully.</p>'));
        }
		
	
    }
	
	

}

/* End of file admin-login.php */
/* Location: ./application/controllers/ems/admin-login.php */