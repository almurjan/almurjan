<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ajax extends CI_Controller
{

	function __construct()
	{

		parent::__construct();
		$this->load->library('email');
		$this->load->model('ems/model_contents');
		$this->load->model('ems/model_user');
		$this->load->model('ems/model_properties');
	}

	//main index function for the controller admin_login
	//loding the main view

	public function calendar()
	{

		$base_url = base_url();

		$calendarHTML = '';

		$lang = $this->session->userdata('lang');




		(intval($_GET["month"]) > 0) ? $cMonth = $_GET["month"] : $cMonth = date("n");

		(intval($_GET["year"]) > 0) ? $cYear = $_GET["year"] : $cYear = date("Y");



		if ($cMonth < 10) $cMonth = '0' . $cMonth;



		// calculate next and prev month and year used for next / prev month navigation links and store them in respective variables

		$prev_year = $cYear;

		$next_year = $cYear;

		$prev_month = intval($cMonth) - 1;

		$next_month = intval($cMonth) + 1;



		// if current month is Decembe or January month navigation links have to be updated to point to next / prev years

		if ($cMonth == 12) {

			$next_month = 1;

			$next_year = $cYear + 1;
		} elseif ($cMonth == 1) {

			$prev_month = 12;

			$prev_year = $cYear - 1;
		}



		/* 

		$getYear = date("Y",strtotime($cYear));

		$getMonth = date("F",strtotime($cMonth."-01"));

		$getMonth = $lang == 'eng'? $getMonth : month_arabic($getMonth); 

		 */

		//'.$getMonth.', '.$getYear.'

		if ($lang == 'eng') {

			$date_month = date("F, Y", strtotime($cYear . "-" . $cMonth . "-01"));
		} else {

			$date_month = date("F", strtotime($cYear . "-" . $cMonth . "-01"));

			$date_month = month_arabic($date_month) . ', ' . date("Y", strtotime($cYear . "-" . $cMonth . "-01"));
		}



		$calendarHTML .= '';

		$calendarHTML .= '';

		$calendarHTML .= '<table class="table"><thead>';



		$calendarHTML .= '';

		$html_years = '';
		$html_years .= '<select name="year" id="year" onchange="LoadMonth(' . date('m') . ' , ' . date('Y') . ')">';
		$cur_year = date('Y');
		for ($year = ($cur_year - 20); $year <= ($cur_year + 20); $year++) {
			if ($_GET["year"] == $year) {
				$selected = 'selected="selected"';
			} else {
				$selected = "";
			}
			if ($year == $cur_year) {
				$html_years .= '<option value="' . $year . '" ' . $selected . '>' . ($lang == 'eng' ? $year : convertToArabic($year)) . '</option>';
			} else {
				$html_years .= '<option value="' . $year . '"  ' . $selected . '>' . ($lang == 'eng' ? $year : convertToArabic($year)) . '</option>';
			}
		}
		$html_years .= '<select>';

		$month_select = (intval($_GET["month"]) > 0) ? $cMonth = $_GET["month"] : $cMonth = date("n");;
		if ($month_select == 1 || $month_select == 01) {
			$month_select_jan = 'selected="selected"';
		} else {
			$month_select_jan = '';
		}
		if ($month_select == 2 || $month_select == 02) {
			$month_select_feb = 'selected="selected"';
		} else {
			$month_select_feb = '';
		}
		if ($month_select == 3 || $month_select == 03) {
			$month_select_mar = 'selected="selected"';
		} else {
			$month_select_mar = '';
		}
		if ($month_select == 4 || $month_select == 04) {
			$month_select_apr = 'selected="selected"';
		} else {
			$month_select_apr = '';
		}
		if ($month_select == 5 || $month_select == 05) {
			$month_select_may = 'selected="selected"';
		} else {
			$month_select_may = '';
		}
		if ($month_select == 6 || $month_select == 06) {
			$month_select_june = 'selected="selected"';
		} else {
			$month_select_june = '';
		}
		if ($month_select == 7 || $month_select == 07) {
			$month_select_jul = 'selected="selected"';
		} else {
			$month_select_jul = '';
		}
		if ($month_select == 8 || $month_select == '08') {
			$month_select_aug = 'selected="selected"';
		} else {
			$month_select_aug = '';
		}
		if ($month_select == 9 || $month_select == '09') {
			$month_select_sept = 'selected="selected"';
		} else {
			$month_select_sept = '';
		}
		if ($month_select == 10 || $month_select == 10) {
			$month_select_oct = 'selected="selected"';
		} else {
			$month_select_oct = '';
		}
		if ($month_select == 11 || $month_select == 11) {
			$month_select_nov = 'selected="selected"';
		} else {
			$month_select_nov = '';
		}
		if ($month_select == 12 || $month_select == 12) {
			$month_select_dec = 'selected="selected"';
		} else {
			$month_select_dec = '';
		}
		$calendarHTML .= ' <tr>
	
      <td colspan="7" class="cMonth" style="padding:10px 5px;text-align:center; ">
		<select name="month" id="month" onchange="LoadMonth(' . date('m') . ' , ' . date('Y') . ')">
			<option value="1" ' . $month_select_jan . '>' . ($lang == 'eng' ? 'January' : 'يناير') . '</option>
			<option value="2" ' . $month_select_feb . '>' . ($lang == 'eng' ? 'February' : 'فبراير') . '</option>
			<option value="3" ' . $month_select_mar . '>' . ($lang == 'eng' ? 'March' : 'مارس') . '</option>
			<option value="4" ' . $month_select_apr . '>' . ($lang == 'eng' ? 'April' : 'أبريل') . '</option>
			<option value="5" ' . $month_select_may . '>' . ($lang == 'eng' ? 'May' : 'مايو') . '</option>
			<option value="6" ' . $month_select_june . '>' . ($lang == 'eng' ? 'June' : 'يونيو') . '</option>
			<option value="7" ' . $month_select_jul . '>' . ($lang == 'eng' ? 'July' : 'يولية') . '</option>
			<option value="8" ' . $month_select_aug . '>' . ($lang == 'eng' ? 'August' : 'أغسطس') . '</option>
			<option value="9" ' . $month_select_sept . '>' . ($lang == 'eng' ? 'September' : 'سبتمبر') . '</option>
			<option value="10" ' . $month_select_oct . '>' . ($lang == 'eng' ? 'October' : 'أكتوبر') . '</option>
			<option value="11" ' . $month_select_nov . '>' . ($lang == 'eng' ? 'November' : 'نوفمبر') . '</option>
			<option value="12" ' . $month_select_dec . '>' . ($lang == 'eng' ? 'December' : 'ديسمبر') . '</option>
		</select>' . $html_years . '
		
	  </td>


  </tr></thead><tbody>';



		$calendarHTML .= '

		<tr>

            <td class="wDays"><span class="eng">' . translate($lang, 'Saturday', 'السبت') . '</span></td>

            <td class="wDays"><span class="eng">' . translate($lang, 'Sunday', 'الأحد') . '</span></td>

            <td class="wDays"><span class="eng">' . translate($lang, 'Monday', 'الإثنين') . '</span></td>

            <td class="wDays"><span class="eng">' . translate($lang, 'Tuesday', 'الثلاثاء') . '</span></td>

            <td class="wDays"><span class="eng">' . translate($lang, 'Wednesday', 'الإربعاء') . '</span></td>

            <td class="wDays"><span class="eng">' . translate($lang, 'Thursday', 'الخميس') . '</span></td>

            <td class="wDays"><span class="eng">' . translate($lang, 'Friday', 'الجمعة') . '</span></td>

        </tr>';

		$first_day_timestamp = mktime(0, 0, 0, $cMonth, 1, $cYear); // time stamp for first day of the month used to calculate

		$maxday = date("t", $first_day_timestamp); // number of days in current month

		$thismonth = getdate($first_day_timestamp); // find out which day of the week the first date of the month is

		$startday = $thismonth['wday']; // 0 is for Sunday and as we want week to start on Mon we subtract 1

		//if (!$thismonth['wday']) $startday = 7;



		$arrayMonthStart = array(6, 0, 1, 2, 3, 4, 5);

		$flagCalendar = false;



		for ($i = 1, $current_day = 1; $current_day <= ($maxday); $i++, $current_day++) {



			$month_with_zero = $current_day < 10 ? '0' . $current_day : $current_day;



			if (($i % 7) == 1) echo "<tr>";



			if ($flagCalendar == false) {

				if (count($arrayMonthStart) < $i - 1) {

					$flagCalendar = true;
				} else

					if ($arrayMonthStart[($i - 1) % 7] == $startday) {

					$flagCalendar = true;
				} else {

					$calendarHTML .= "<td class='empty'>&nbsp;</td>";

					$current_day = 0;

					continue;
				}
			}



			$checkDate = $cYear . '-' . $cMonth . '-' . $current_day;

			$day = date('Y-m-j');



			if ($day == $checkDate) {

				$dayActiveCurrent = 'current';

				$bg_color = '#43b3d8';
			} else {

				$dayActiveCurrent = '';

				$bg_color = '#9b9b9b';
			}

			$eventCheckDate = $cYear . '-' . $cMonth . '-' . $month_with_zero;

			$eventCheckDate = str_replace('/0', '/', ltrim(date('m/d/Y', strtotime($eventCheckDate)), '0'));



			$news_id = getPageIdbyTemplate('event_calender');

			$getAll_events = array();

			//$getAll_events = ListingCalendarEvents($news_id, $eventCheckDate);

			//echo $this->db->last_query() . "<br>";

			//$getAll_events = '';

			$countEvents = count($getAll_events);

			//echo $countEvents  . ' - ' . $eventCheckDate . "<br>"; 

			$dayActive = $getAll_events ? 'dayActive' : 'noData';


			$haveData = 'haveData';

			$calendarHTML .= "<td class='available_date " . $haveData . " " . $dayActive . " " . $dayActiveCurrent . "' data-date='" . $eventCheckDate . "'>

                    <div onClick=fetchEventsOnDate('" . $eventCheckDate . "'); data-date='" . $eventCheckDate . "'' class='date available '><span class='eng'>" . ($lang == 'eng' ? $current_day : convertToArabic($current_day)) . "</span></div>
					
                    <div class='clearfix'></div>

                    <div class='box-container'>

                    <ul class='noOfEvents'>";



			foreach ($getAll_events as $event) {
				$eventUrl = lang_base_url() . 'page/Event/' . $event->id;
				$id = content_detail('city', $event->id);

				$city_name = getCityName($id);
				$calendarHTML .= '<h3><a target="_blank" href="' . lang_base_url() . 'page/event_details/' . $event->id . '">' . pageTitle($event->id, $lang) . '</a><span>' . ($lang == 'eng' ? $city_name->eng_name : $city_name->arb_name) . '</span></h3>';
			}



			$calendarHTML .= "</ul></div></td>";



			if (($i % 7) == 0) $calendarHTML .= "</tr>";
		}



		$calendarHTML .= "</tbody></table>";



		echo $calendarHTML;
	}
}
/* End of file admin_login.php */
/* Location: ./application/controllers/ems/admin_login.php */
