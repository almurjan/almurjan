<?php

class Page extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('ems/model_configuration');
        $this->load->model('ems/model_contents');
        $this->load->model('ems/model_content_images');
        $this->load->model('ems/model_registrations');
        $this->load->model('model_custom');
        $this->load->model('model_login');
        $this->load->model('job_model');
        $this->load->library('pagination');
        $this->load->library('email');

        /* echo $this->session->userdata('lang');
		$this->session->sess_destroy();
		Exit; */

        /* if (!$this->session->userdata('lang')) {
            $this->session->set_userdata(array('lang' => 'arb'));
            if ($this->uri->segment(1) == 'en') {
               $this->session->set_userdata(array('lang' => 'eng'));
            } else {
                $this->session->set_userdata(array('lang' => 'arb'));
            }
        } else {

            $lang = $this->session->userdata('lang');
            $segment = $this->uri->segment(1);
            if ($lang == 'eng') {
                $url = base_url();

                if ($segment == 'ar') {
                    $this->session->set_userdata(array('lang' => 'arb'));
                }
			    else if ($segment == 'page') {
                    $this->session->set_userdata(array('lang' => 'arb'));
                }
				else if ($segment == '') {
                    $this->session->set_userdata(array('lang' => 'arb'));
                }
				else if ($segment == 'en') {
                    $this->session->set_userdata(array('lang' => 'eng'));
                }
            } else if ($lang == 'arb') {
                if ($segment == 'ar') {
                    $this->session->set_userdata(array('lang' => 'arb'));
                } else if ($segment == 'page') {
                    $this->session->set_userdata(array('lang' => 'arb'));
                } else if ($segment == 'en') {
                    $this->session->set_userdata(array('lang' => 'eng'));
                } else if ($segment == '') {
                    $this->session->set_userdata(array('lang' => 'arb'));
                }
            }
        } */


        if (!$this->session->userdata('lang')) {
            $this->session->set_userdata(array('lang' => 'eng')); // If no language is selected, set to default ENGLISH /
        } else {
            $lang = $this->session->userdata('lang');
            $segment = $this->uri->segment(1);

            if ($lang == 'eng') {
                if ($segment == 'ar') {
                    $this->session->set_userdata(array('lang' => 'arb'));
                }
            } else if ($lang == 'arb') {
                if ($segment == '' || $segment == 'page') {
                    $this->session->set_userdata(array('lang' => 'eng'));
                } else if ($segment == 'ar') {
                    $this->session->set_userdata(array('lang' => 'arb'));
                }
            }
        }
		
		if($this->uri->segment(1) == 'med'){
			$this->session->set_userdata(array('lang' => 'eng')); // If no language is selected, set to default ENGLISH /
		}
		if($this->uri->segment(2) == 'med'){
			$this->session->set_userdata(array('lang' => 'arb')); // If no language is selected, set to default ENGLISH /
		}

    }

public function med_submit()
    {
        $self = getPageIdbyTemplate('self');

        $lang = html_escape($this->input->post('lang'));
        $this->load->library('email');
        $conf = $this->model_configuration->fetchRow();
        $data = array();
        $post_data = $this->input->post();

        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response') {
                $data[$key] = $value;
            }
        }

        $siteKey = $this->input->post('g-recaptcha-response');
        $res = $this->VerifyRecaptcha($siteKey);

        $resArr = array();
        if ($res == false) {
            if ($lang == 'eng') {
                $message = 'Please use the captcha!';
            } else {
                $message = 'نرجو التأكد من تعبئة خانة التحقق.';
            }
            //$resArr['status'] = false;
            $resArr['error_captcha'] = $message;
            $resArr['status'] = 2;
            echo json_encode($resArr);
            exit();
        }

        $userRecord = $this->model_contents->saveMedRecords($data);
		if ($userRecord > 0) {

			if ($lang == 'eng') {
				$subject = 'Your mediclinic form has been received!';
				$title = 'Your mediclinic form submitted';

				$subject2 = 'New mediclinic email';
				$title2 = 'New mediclinic email';
			} else {
				$subject = 'تم استلام نموذج التواصل الخاص بك';
				$title = 'تم ارسال نموذج بنجاح';

				$subject2 = 'اتصل بنا عبر البريد الإلكتروني';
				$title2 = 'اتصل بنا عبر البريد الإلكتروني';
			}
			$hideSocialLinks = 1;
			unset($data['terms']);
			userGeneralEmail($data, $subject, $title, $lang,$hideSocialLinks);
			adminGeneralEmail($data, $subject2, $title2, $lang, 'med',$hideSocialLinks);

			if ($lang == 'eng') {
				$message['status'] = 1;
				$message['message'] = "Thank you for your submission please send us your CV to this email career.med@almurjan.com";
			} else {
				$message['status'] = 1;
				$message['message'] = "شكرا ، الرجاء إرسال سيرتك الذاتية إلى البريد الإلكتروني career.med@almurjan.com";
			}
			echo json_encode($message);
			exit;
		   /* $this->session->set_flashdata('success_msg', $usermsg);
			redirect( lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($self, 'eng')) );*/

		}

    }
	public function med()
    {
		$data = array();
        $lang = $this->session->userdata('lang');

        $config_settings = getConfigurationSetting();
		
        if ($lang == 'eng') {
            $segment = $this->uri->segment(1);
        } else {

            $segment = $this->uri->segment(2);
        }
		
        $data['lang'] = $lang;
        $data['content'] = 'med';
        $this->load->view("layouts/default", $data);
	}
    public function get_judge_info()
    {
        $admin_lang = 'arb';
        $id = $this->uri->segment(3);

        $this->load->model('ems/model_inquries');
        $data = $this->model_inquries->get_judge_info($id);
        $data->job = content_detail('arb_job_name', $id);
        $data->description = content_detail('arb_judge_desc', $id);
        if (content_detail('eng_judge_image_mkey_hdn', $id)) {
            $html_user .= '<img src="' . base_url() . 'assets/script/' . content_detail('eng_judge_image_mkey_hdn', $id) . '"></img>';
        } else {
            $html_user .= '<img src="' . base_url() . 'assets/script/mlib-uploads/thumbs/any.jpg' . '"></img>';
        }

        $data->judge_image = $html_user;
        echo json_encode($data);
        exit;
    }

    public function contactUsRegistrationForm()
    {
        $contact_us = getPageIdbyTemplate('contact_us');
        if (isset($_POST)) {

            $this->load->model('model_contents');
            $lang = html_escape($this->input->post('lang'));
            $userData = array(
                'contactpurpose' => html_escape($this->input->post('contactpurpose')),
                'name' => html_escape($this->input->post('name')),
                'mobile' => html_escape($this->input->post('mobile')),
                'email' => html_escape($this->input->post('email')),
                'message' => html_escape($this->input->post('message'))
            );

            $queryResult = $this->model_contents->contactUsFormData($userData);
            if ($lang == 'eng') {
                $subject = 'Contact Us Enquiry';
                $title = 'Your Enquiry has been received';

                $subject2 = 'Contact us Application';
                $title2 = 'New Contact';
            } else {
                $subject = 'تم استلام نموذج التواصل الخاص بك';
                $title = 'تم ارسال نموذج بنجاح';

                $subject2 = 'اتصل بنا عبر البريد الإلكتروني';
                $title2 = 'اتصل بنا عبر البريد الإلكتروني';
            }
            userGeneralEmail($userData, $subject, $title, $lang);
            adminGeneralEmail($userData, $subject2, $title2, $lang, 'contact');

            if ($queryResult) {
                if ($lang == 'eng') {
                    $usermsg = "Thank You!! We will get back to you soon.";
                } else {
                    $usermsg = "شكرا لك! سوف نعود اليك قريبا.";
                }
                $this->session->set_flashdata('success_msg', $usermsg);


                if ($lang == 'arb') {
                    redirect(lang_base_url() . 'ar/page/' . str_replace(' ', '_', pageTitle($contact_us, 'eng')));
                } else {
                    redirect(lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($contact_us, 'eng')));
                }
            }
        }
    }

    //load more news
    public function loadMoreNews()
    {

        $this->load->model('ems/model_contents');

        $lang = $this->session->userdata('lang');

        $tpl = html_escape($this->input->post('tpl'));
        if ($tpl == 'news_room') {
            $parant_id = getPageIdbyTemplate('news_room');
            $page_type = 'news_room_detail';
        } else {
            $parant_id = getPageIdbyTemplate('news');
            $page_type = 'news_detail';
        }
        $offset = html_escape($this->input->post('offset'));
        $limit = 4;

        $news_listings = $this->model_contents->loadMoreData($parant_id, $offset, $limit, $tpl);

        $html = '';
        $i = 1;
        $news_listings_count = 0;

        if ($news_listings) {
            foreach ($news_listings as $news_listing) {
                $date = '';
                $date = content_detail('date', $news_listing->id);

                if ($date != '') {
                    if ($lang == 'eng') {
                        $date = date('d M Y', strtotime($date));
                    } else {
                        $day = convertToArabic(date('d', strtotime($date)));
                        $month = month_arabic(date('M', strtotime($date)));
                        $year = convertToArabic(date('Y', strtotime($date)));
                        $date = $day . ' ' . $month . ' ' . $year;
                    }
                }
                $image = '';
                if (content_detail('eng_thumb_mkey_hdn', $news_listing->id) != '') {
                    $image = base_url() . 'assets/script/' . content_detail('eng_thumb_mkey_hdn', $news_listing->id);
                } else {
                    $image = base_url() . 'assets/script/noImageNew.png';
                }
                $html .= '
				<div class="col-6 mt-2">
				<a href=' . lang_base_url() . 'page/' . $page_type . '/' . $news_listing->id . '>
					<figure class="figure FourImage position-relative d-block mb-0">
						<img src="' . $image . '" class="w-100" alt="Products Images" />
						<figcaption class="figure-caption px-3">
							<span class="d-block font-italic mb-2 font-weight-light">' . $date . '</span>
							<p>' . get_x_words(strip_tags(content_detail($lang . '_desc', $news_listing->id)), 5) . '</p>
						</figcaption>
					</figure>
				</a>
			</div>';
                $news_listings_count++;
            }
        }

        $resArr['html'] = $html;
        $resArr['count_records'] = $news_listings_count;
        /*
			$resArr['offset_value'] = $offset_value + 6;
			$resArr['count_users'] = count($data['events']);

			if($resArr != ''){
				$resArr['success'] = "Events Retrieved Successfully";
			}
		*/
        echo json_encode($resArr);
        exit();

    }

    public function getRegionCities()
    {
        $data = array();
        $lang = html_escape($this->input->post('lang'));
        $id = html_escape($this->input->post('id'));
        $region_cities = $this->model_contents->get_region_cities($id);
        $html_cities = '';
        $html_cities .= '<option value="">' . ($lang == 'eng' ? 'Select City' : 'إختيار المدينة') . '</option>';
        foreach ($region_cities as $region_city) {
            $html_cities .= '<option value="' . $region_city->id . '">' . ($lang == 'eng' ? $region_city->name : $region_city->name_arb) . '</option>';
        }

        $data['html_cities'] = $html_cities;
        echo json_encode($data);
        exit;
    }

    public function index($page_title = 'home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');

        $config_settings = getConfigurationSetting();

        if ($lang == 'eng') {
            $segment = $this->uri->segment(2);
            $data['project_name'] = $config_settings->project_name;
        } else {

            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name_arb;
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        if (is_numeric($page_title)) {
            $page = $page_title;
        }
        elseif (html_escape($this->input->get('id'))==1293) {
            $page = 1293;
        }else {
            $page_title = str_replace('_', ' ', $page_title);
            $page = getPageId($page_title);

        }



        $page_id = $page;
        $data['page_id'] = $page_id;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_id);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }
        //pagination
        /*if($contents[0]->tpl == 'landmarks'){

            $segmentLast = 0;
            if($lang == 'eng') {
                $segmentLast = $this->uri->segment(3);
            } else {
                $segmentLast = $this->uri->segment(4);
            }
			
            $page_id = $page_title;
            $data['lang'] = $lang;
            $this->db->last_query();
            $listings = $this->model_contents->fetchAllFilterLands($offset='0',$limit='200',$contents[0]->id);
            $this->load->library("pagination");
            
            $config = array();
			$config["base_url"] = lang_base_url() . "page/Landmarks_&_Assets";
			$config["total_rows"] = count($listings);
			$config["per_page"] = 2;
			if($lang == 'eng'){
			    $config["cur_link"] = 3;    
			}
			else{
			     $config["cur_link"] = 4; 
			     $config["uri_segment"] = 4; 
			}
			$config['full_tag_open'] = '';
            $config['full_tag_close'] = '';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item"><a class="page-link active" href="#">';//this is active tab
			$config['first_link'] = ($lang == 'eng' ? 'First' : 'الاخيرة');
			$config['last_link'] = ($lang == 'eng' ? 'Last' : 'الاخيرة');
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['anchor_class'] = 'class="page-link"';
			$this->pagination->initialize($config);

            $page = $segmentLast;
			$this->pagination->initialize($config);
            $data["links"] = $this->pagination->create_links();
if($segmentLast == ''){
    $segmentLast=0;
}
            $data['records'] = $this->model_contents->fetchAllFilterLands($segmentLast,$config["per_page"],$contents[0]->id);


        }*/
        $this->load->model('ems/model_contents');
        $data['content'] = $contents[0]->tpl;
        if($page_id==26){

            if(html_escape($this->input->get('location'))&&html_escape($this->input->get('location'))!='All'){
                $location =html_escape($this->input->get('location'));
                if($lang=='eng'){
                    $field = 'country_name';
                }else{
                    $field = 'country_name_ar';
                }
                $search = $location;
                $data['listings'] = assetsListingContent($page_id,'','',$location,$field);
            }else{
                $search = '';
                $data['listings'] = assetsListingContent($page_id,'','','','');
            }
        }else{
            $search = '';
            $data['listings'] = ListingContent($page_id);
        }

        $data['location_listings'] = $this->model_contents->getPageLocations($parent_id);
        $data['categories'] = ListingContent(8);
        $data['search'] = $search;
        $data['tpl_name'] = $contents[0]->tpl;
        $data['contents'] = $contents;
        $data['parent_id'] = $parent_id;
        //$data['navbar'] = $this->load->view('layouts/navbar', NULL, TRUE);
        $this->load->view("layouts/default", $data);
    }

    public function contestant_detail($page_title = 'Home')
    {

        $data = array();
        $lang = 'arb';
        if ($lang == 'eng') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $user_info = $this->model_registrations->fetchRow($segment);

        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['user_info'] = $user_info;
        $data['contents'] = $contents;
        $data['content'] = 'contestants-details';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function cat_details($page_title = 'Home')
    {

        $data = array();
        $lang = 'arb';
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;

        if ($this->uri->segment(2) == 'Registration' || $this->uri->segment(3) == 'Registration') {
            $data['content'] = 'registration';
        } else {
            $data['content'] = 'category-inner';
        }
        $data['categories'] = ListingContent(8);

        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function thankYou($page_title = 'Home')
    {

        $data = array();
        $lang = 'arb';
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;

        $data['content'] = 'thank_you';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function create_profile($page_title = 'Home')
    {

        $data = array();
        $lang == 'arb';
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $segment = $this->uri->segment(2);
        } else {
            $segment = $this->uri->segment(2);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $user_id = $this->uri->segment(3);
        $data['user_id'] = base64_decode($user_id);
        $data['user_info'] = $this->model_registrations->fetchRow($data['user_id']);
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'create-profile';
        $data['page_id'] = $page_id;
        $data['config_settings'] = $config_settings;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function detail_service($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'single-service';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function news_detail($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'arb') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'single-news';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function news_room_detail($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'arb') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'news_room_detail';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function vacancies_detail($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'arb') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'vacancies_detail';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function vacancy_application($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'arb') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'vacancy_application';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function single_album($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'single_album';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function job_openings($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'job_openings';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function job_detail($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['job_id'] = $segment;
        $data['contents'] = $contents;
        $data['content'] = 'job_detail';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function service_detail($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(4);
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['job_id'] = $segment;
        $data['contents'] = $contents;
        $data['content'] = 'service_detail';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function media_center_event_details($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(4);
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['job_id'] = $segment;
        $data['contents'] = $contents;

        $data['content'] = 'media_center_event_details';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;
        $data['tpl_load_more'] = $contents[0]->tpl;

        $this->load->view("layouts/default", $data);

    }

    public function service_request($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(4);
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);

        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['job_id'] = $segment;
        $data['contents'] = $contents;
        $data['content'] = 'service_request';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }
    public function other_sector_detail($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(4);
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }
        
        $data['contents'] = $contents;
        $data['content'] = 'other_sector_detail';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function almurjan_holding_detail($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(4);
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'holding_detail';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function other_sector_project_detail($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(4);
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'other_sector_project_detail';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function NewsRoomLogin()
    {

        $news_room = getPageIdbyTemplate('news_room'); //page_id
        $lang = html_escape($this->input->post('lang'));

        $this->form_validation->set_rules('username_news', 'Username', 'trim|required');
        $this->form_validation->set_rules('password_news', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            if ($lang == 'arb') {
                redirect(lang_base_url() . 'ar/page/' . str_replace(' ', '_', pageTitle($news_room, 'eng')));
            } else {
                redirect(lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($news_room, 'eng')));
            }
        } else {
            $this->load->model('model_configuration');


            $result = $this->model_configuration->newsRoomLogin(html_escape($this->input->post('username_news')), html_escape($this->input->post('password_news')));
            if ($result) {
                $sess_check = true;
                /* foreach ($result as $row)
				{
				    $username_news=$row->username_news;
                } */

                $this->session->set_userdata("news_room_login", $sess_check);
                if ($lang == 'arb') {
                    redirect(lang_base_url() . 'ar/page/' . str_replace(' ', '_', pageTitle($news_room, 'eng')));
                } else {
                    redirect(lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($news_room, 'eng')));
                }
            } else {
                if ($lang == 'eng') {
                    $usermsg = "Incorrect Username or Password";
                } else {
                    $usermsg = "اسم المستخدم أو كلمة المرور غير صحيحة";
                }
                $this->session->set_flashdata('failed_login', $usermsg);
                if ($lang == 'arb') {
                    redirect(lang_base_url() . 'ar/page/' . str_replace(' ', '_', pageTitle($news_room, 'eng')));
                } else {
                    redirect(lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($news_room, 'eng')));
                }
            }
        }

    }

    public function NewsRoomLogout()
    {
        $news_room = getPageIdbyTemplate('news_room'); //page_id
        if (isset($_POST)) {
            $lang = html_escape($this->input->post('lang'));
            $this->session->unset_userdata('news_room_login');
            if ($lang == 'arb') {
                redirect(lang_base_url() . 'ar/page/' . str_replace(' ', '_', pageTitle($news_room, 'eng')));
            } else {
                redirect(lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($news_room, 'eng')));
            }
        }
    }

    public function saveForm()
    {

        if (isset($_POST)) {
            $this->load->model('model_contents');

            $lang = html_escape($this->input->post('lang'));
            $country_code = html_escape($this->input->post('country'));
            $country_explode = explode('|', $country_code);
            $country = $country_explode[0];
            $userData = array(
                'organization_name' => html_escape($this->input->post('organization_name')),
                'job_title' => html_escape($this->input->post('job_title')),
                'first_name' => html_escape($this->input->post('first_name')),
                'last_name' => html_escape($this->input->post('last_name')),
                'email' => html_escape($this->input->post('email')),
                'phone' => html_escape($this->input->post('phone')),
                'city' => html_escape($this->input->post('city')),
                'country' => $country,
                'lang' => html_escape($this->input->post('lang')),
                'product_service' => html_escape($this->input->post('product_dropdown')),
                'message' => html_escape($this->input->post('message'))
            );
            $queryResult = $this->model_contents->requestQuotation($userData);

            if ($lang == 'eng') {
                $subject = 'Your quotation has been received!';
                $title = 'Your quotation form submitted';

                $subject2 = 'New quotation form';
                $title2 = 'New quotation form';
            } else {
                $subject = 'تم استلام نموذج التواصل الخاص بك';
                $title = 'تم ارسال نموذج بنجاح';

                $subject2 = 'اتصل بنا عبر البريد الإلكتروني';
                $title2 = 'اتصل بنا عبر البريد الإلكتروني';
            }
            userGeneralEmail($userData, $subject, $title, $lang);
            adminGeneralEmail($userData, $subject2, $title2, $lang, 'request_quotation');
            if ($queryResult) {
                if ($lang == 'eng') {
                    $usermsg = "Thank You!! Quotation received successfully.";
                } else {
                    $usermsg = "شكرا لك! تم استلام التسعير بنجاح.";
                }
                $this->session->set_flashdata('success_msg', $usermsg);

                if (html_escape($this->input->post('page_id')) != '') {
                    if ($lang == 'arb') {
                        redirect(lang_base_url() . 'ar/page/service_request/' . html_escape($this->input->post('page_id')));
                    } else {
                        redirect(lang_base_url() . 'page/service_request/' . html_escape($this->input->post('page_id')));
                    }
                } else {
                    $request_now = getPageIdbyTemplate('request_now');
                    if ($lang == 'arb') {
                        redirect(lang_base_url() . 'ar/page/' . str_replace(' ', '_', pageTitle($request_now, 'eng')));
                    } else {
                        redirect(lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($request_now, 'eng')));
                    }
                }
            }
        }
    }

    public function getCitiesDtc()
    {
        $country = html_escape($this->input->post('countrycode'));
        $lang = html_escape($this->input->post('lang'));
        $country_code = explode('|', $country);
        $code = $country_code[1];
        $cities = $this->model_custom->getCities($code);
        $selected = "";
        $html_cities = '';
        foreach ($cities as $city) {
            /* if($country_code == $city->countrycode){
			$selected = 'selected';
		}
		else{
			$selected = '';
		} */
            $html_cities .= '<option value="' . ($lang == 'eng' ? $city->eng_name : $city->arb_name) . '" ' . $selected . '>' . ($lang == 'eng' ? $city->eng_name : $city->arb_name) . '</option>';
        }
        echo $html_cities;
    }

    public function job_application_form()
    {

        $job_id = html_escape($this->input->post('job_id'));
        $lang = html_escape($this->input->post('lang'));
        $this->load->library('email');
        $conf = $this->model_configuration->fetchRow();
        $data = array();
        $post_data = $this->input->post();

        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response') {
                $data[$key] = $value;
            }
        }

        $siteKey = $this->input->post('g-recaptcha-response');
        $res = $this->VerifyRecaptcha($siteKey);

        $resArr = array();
        if ($res == false) {
            if ($lang == 'eng') {
                $message = 'Please use the captcha!';
            } else {
                $message = 'نرجو التأكد من تعبئة خانة التحقق.';
            }
            //$resArr['status'] = false;
            $resArr['error_captcha'] = $message;
            $resArr['status'] = 2;
            echo json_encode($resArr);
            exit();
        }

        $filename1 = $_FILES['c_v']['name'];
        $filename = str_replace(' ', '-', $filename1);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        if ($ext == 'pdf' ) {
            if ($_FILES['c_v']['name'] != '') {
                $file_name_eng = date('Ymdhsi') . '-' . $filename;
                $file_size1 = $_FILES['c_v']['size'];
                $file_tmp_eng = $_FILES['c_v']['tmp_name'];
                $type1 = $_FILES['c_v']['type'];
                move_uploaded_file($file_tmp_eng, "uploads/cvs/" . $file_name_eng);
                $data['c_v'] = $file_name_eng;
            }


            /* $resArr['error'] = "";
				$resArr['status'] = false;
				$resArr['success'] = "";  */


            $userRecord = $this->model_contents->saveCareer($data);

            if ($userRecord > 0) {

                if ($lang == 'eng') {
                    $subject = 'Your career form has been received!';
                    $title = 'Your career form submitted';

                    $subject2 = 'New career email';
                    $title2 = 'New career email';
                } else {
                    $subject = 'تم استلام نموذج التواصل الخاص بك';
                    $title = 'تم ارسال نموذج بنجاح';

                    $subject2 = 'اتصل بنا عبر البريد الإلكتروني';
                    $title2 = 'اتصل بنا عبر البريد الإلكتروني';
                }

                unset($data['terms']);
                userGeneralEmail($data, $subject, $title, $lang);
                adminGeneralEmail($data, $subject2, $title2, $lang, 'careers');

                if ($lang == 'eng') {
                    $message['status'] = 1;
                    $message['message'] = "Thank You!! Application submitted successfully";
                } else {
                    $message['status'] = 1;
                    $message['message'] = "شكرا لك ! تم تقديم الطلب بنجاح";
                }
                echo json_encode($message);
                exit;
                /* $this->session->set_flashdata('success_msg', $usermsg);
                 redirect( lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($self, 'eng')) );*/

            }else{

                if ($lang == 'eng') {
                    $message['status'] = 0;
                    $message['message'] = "Some Thing Goes Wrong!";
                } else {
                    $message['status'] = 0;
                    $message['message'] = "Some Thing Goes Wrong!";
                }
                echo json_encode($message);
                exit;
                /*$this->session->set_flashdata('error_msg', $usermsg);
                redirect( lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($self, 'eng')) );*/
            }
        }else{
            if ($lang == 'eng') {
                $message['status'] = 0;
                $message['message'] = "Please Upload Only PDF File";
            } else {
                $message['status'] = 0;
                $message['message'] = "Please Upload Only PDF File";
            }
            echo json_encode($message);
            exit;
            /*$this->session->set_flashdata('error_msg', $usermsg);
            redirect( lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($self, 'eng')) );*/
        }

    }

    public function submit_self_application()
    {
        $self = getPageIdbyTemplate('self');

        $lang = html_escape($this->input->post('lang'));
        $this->load->library('email');
        $conf = $this->model_configuration->fetchRow();
        $data = array();
        $post_data = $this->input->post();

        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response') {
                $data[$key] = $value;
            }
        }

        $siteKey = $this->input->post('g-recaptcha-response');
        $res = $this->VerifyRecaptcha($siteKey);

        $resArr = array();
        if ($res == false) {
            if ($lang == 'eng') {
                $message = 'Please use the captcha!';
            } else {
                $message = 'نرجو التأكد من تعبئة خانة التحقق.';
            }
            //$resArr['status'] = false;
            $resArr['error_captcha'] = $message;
            $resArr['status'] = 2;
            echo json_encode($resArr);
            exit();
        }

            $userRecord = $this->model_contents->saveSelfApplication($data);
            if ($userRecord > 0) {

                if ($lang == 'eng') {
                    $subject = 'Your career form has been received!';
                    $title = 'Your career form submitted';

                    $subject2 = 'New career email';
                    $title2 = 'New career email';
                } else {
                    $subject = 'تم استلام نموذج التواصل الخاص بك';
                    $title = 'تم ارسال نموذج بنجاح';

                    $subject2 = 'اتصل بنا عبر البريد الإلكتروني';
                    $title2 = 'اتصل بنا عبر البريد الإلكتروني';
                }

                unset($data['terms']);
                userGeneralEmail($data, $subject, $title, $lang);
                adminGeneralEmail($data, $subject2, $title2, $lang, 'careers');

                if ($lang == 'eng') {
                    $message['status'] = 1;
                    $message['message'] = "Thank You!! Application submitted successfully";
                } else {
                    $message['status'] = 1;
                    $message['message'] = "شكرا لك ! تم تقديم الطلب بنجاح";
                }
                echo json_encode($message);
                exit;
               /* $this->session->set_flashdata('success_msg', $usermsg);
                redirect( lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($self, 'eng')) );*/

            }else{

                if ($lang == 'eng') {
                    $message['status'] = 0;
                    $message['message'] = "Some Thing Goes Wrong!";
                } else {
                    $message['status'] = 0;
                    $message['message'] = "Some Thing Goes Wrong!";
                }
                echo json_encode($message);
                exit;
                /*$this->session->set_flashdata('error_msg', $usermsg);
                redirect( lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($self, 'eng')) );*/
            }

    }

    public function detail($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        if ($lang == 'eng') {
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['job_id'] = $segment;
        $data['contents'] = $contents;
        $data['content'] = 'detail';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    function VerifyRecaptcha($g_recaptcha_response)
    {
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => "https://www.google.com/recaptcha/api/siteverify",
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => array(
                'secret' => '6LcI0rUkAAAAAEbsNja9AMPXUckpayyxuLPKzJG0',
                'response' => $g_recaptcha_response,
                'remoteip' => $_SERVER['REMOTE_ADDR']
            )
        );
        curl_setopt_array($ch, $curlConfig);
        if ($result = curl_exec($ch)) {
            curl_close($ch);
            $response = json_decode($result);
            return $response->success;
        } else {
            var_dump(curl_error($ch)); // this for debug remove after you test it
            return false;
        }
    }

    public function applyJob()
    {
        $this->load->library('email');
        $lang = html_escape($this->input->post('lang'));
        $conf = $this->model_configuration->fetchRow();
        $data = array();
        $post_data = $this->input->post();
        $siteKey = $this->input->post('g-recaptcha-response');
        $res = $this->VerifyRecaptcha($siteKey);

        $resArr = array();
        if ($res == false) {
            if ($lang == 'eng') {
                $message = 'Please use the captcha!';
            } else {
                $message = 'نرجو التأكد من تعبئة خانة التحقق.';
            }
            //$resArr['status'] = false;
            $resArr['error_captcha'] = $message;
            $resArr['status'] = 2;
            echo json_encode($resArr);
            exit();
        } else {

            foreach ($post_data as $key => $value) {
                if ($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response') {
                    $data[$key] = removeJsScripts($value);
                }
            }
            $filename = $_FILES['c_v']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if ($ext == 'pdf' || $ext == 'doc' || $ext == 'docx' || $ext == 'PDF' || $ext == 'DOC' || $ext == 'DOCX') {
                if ($_FILES['c_v']['name'] != '') {
                    $file_name_eng = date('Ymdhsi') . '-' . $_FILES['c_v']['name'];
                    $file_size1 = $_FILES['c_v']['size'];
                    $file_tmp_eng = $_FILES['c_v']['tmp_name'];
                    $type1 = $_FILES['c_v']['type'];
                    move_uploaded_file($file_tmp_eng, "uploads/cvs/" . $file_name_eng);
                    $data['c_v'] = $file_name_eng;
                }


                $resArr['error'] = "";
                $resArr['status'] = false;
                $resArr['success'] = "";
                if ($lang == 'eng') {
                    $subject = 'Your career form has been received!';
                    $title = 'Your career form submitted';

                    $subject2 = 'New career email';
                    $title2 = 'New career email';
                } else {
                    $subject = 'تم استلام نموذج التواصل الخاص بك';
                    $title = 'تم ارسال نموذج بنجاح';

                    $subject2 = 'اتصل بنا عبر البريد الإلكتروني';
                    $title2 = 'اتصل بنا عبر البريد الإلكتروني';
                }


                userGeneralEmail($data, $subject, $title, $lang);
                adminGeneralEmail($data, $subject2, $title2, $lang);

                $userRecord = $this->model_contents->saveCareer($data);

                if ($userRecord > 0) {
                    if ($lang == 'eng') {
                        $message['status'] = 1;
                        $message['message'] = "Form Submit Successful";
                    } else {
                        $message['status'] = 1;
                        $message['message'] = "تم التحميل بنجاح";
                    }
                } else {
                    if ($lang == 'eng') {
                        $message['status'] = 0;
                        $message['message'] = "Form Not Submit, Please try again";
                    } else {
                        $message['status'] = 0;
                        $message['message'] = 'النموذج لم يتم الإرسال ، يرجى المحاولة مرة أخرى';
                    }
                }
            } else {
                if ($lang == 'eng') {
                    $message['status'] = 0;
                    $message['message'] = "<p style='color:red'>Form Not Submitted File Format is Incorrect</p>";
                } else {
                    $message['status'] = 0;
                    $message['message'] = "<p style='color:red'>تم التحميل بنجاح</p>";
                }
            }
            /* if($lang == 'eng'){
				$this->session->set_flashdata('message',$message['message']);
				//redirect($this->config->item('base_url').'en/page/job_detail/'.$data['job_id'].'#success_message');
			}
			else{
				$this->session->set_flashdata('message',$message['message']);
				//redirect($this->config->item('base_url').'/page/job_detail/'.$data['job_id'].'#success_message');
			} */
        }

        echo json_encode($message);
        exit();

    }

    public function loadMoreUsers()
    {
        $lang = html_escape($this->input->post('lang'));
        if ($lang == 'eng') {
            $url = 'en/';
        } else {
            $url = '';
        }
        $offset_value = html_escape($this->input->post('offset_value'));
        $city = html_escape($this->input->post('city'));
        $limit = 6;
        $data['events'] = $this->model_contents->front_end_event_future_next($offset_value, $limit, $city);
        $html = '';
        foreach ($data['events'] as $event_listing) {
            $city = content_detail('city', $event_listing->id);
            $city_name = getCityName(content_detail('city', $event_listing->id));

            if (content_detail('eng_event_thumb_image_mkey_hdn', $event_listing->id) != '') {
                $image = base_url('assets/script/' . content_detail('eng_event_thumb_image_mkey_hdn', $event_listing->id));
            } else {
                $image = base_url('assets/frontend/images/black.jpg');
            }

            $categor_id = content_detail('category', $event_listing->id);
            $category = getCategoryName($categor_id);
            if ($lang == 'eng') {
                $city = $city_name->eng_name;
            } else {
                $city = $city_name->arb_name;
            }

            $event_dates = getEventDatesAndTimes($event_listing->id);
            if (count($event_dates) != 0) {
                $k = 0;
                foreach ($event_dates as $event_date) {
                    $k++;
                }

                if ($lang == 'eng') {
                    if ($event_dates[0]->event_date == $event_dates[$k - 1]->event_date) {
                        $date_ranges = (date('d M', strtotime($event_dates[0]->event_date)));

                        if ($event_date->start_time != '00:00:00') {
                            $dates = '<p><strong>' . $date_ranges . '</strong></p>
							<p>' . date('h:i a', strtotime($event_dates[0]->start_time)) . ' - ' . date('h:i a', strtotime($event_dates[0]->end_time)) . '</p>';
                        } else {
                            $dates = '<p><strong>' . $date_ranges . '</strong></p>';
                        }

                    } else {
                        if ($event_date->start_time != '00:00:00') {
                            $date_ranges = (date('d M', strtotime($event_dates[0]->event_date))) . ' - ' . (date('d M', strtotime($event_dates[$k - 1]->event_date)));

                            $dates = '<p><strong>' . $date_ranges . '</strong></p>
							
							<p>' . date('h:i a', strtotime($event_dates[0]->start_time)) . ' - ' . date('h:i a', strtotime($event_dates[0]->end_time)) . '</p>';
                        } else {
                            $date_ranges = (date('d M', strtotime($event_dates[0]->event_date))) . ' - ' . (date('d M', strtotime($event_dates[$k - 1]->event_date)));

                            $dates = '<p><strong>' . $date_ranges . '</strong></p>';
                        }
                    }

                } else {

                    $date0 = date(' d', strtotime($event_dates[0]->event_date));
                    $month0 = date('M', strtotime($event_dates[0]->event_date));
                    $date_month0 = arabic_w2e($date0) . ' ' . month_arabic($month0);

                    $date1 = date(' d', strtotime($event_dates[$k - 1]->event_date));
                    $month1 = date('M', strtotime($event_dates[$k - 1]->event_date));
                    $date_month1 = arabic_w2e($date1) . ' ' . month_arabic($month1);

                    if ($event_dates[0]->event_date == $event_dates[$k - 1]->event_date) {
                        $date_ranges = $date_month0;
                    } else {
                        $date_ranges = $date_month0 . ' - ' . $date_month1;
                    }

                    $arabic_format = arabic_w2e(date('h:i a', strtotime($event_dates[0]->start_time))) . ' - ' . arabic_w2e(date('h:i a', strtotime($event_dates[0]->end_time)));

                    $arabic_format = str_replace('am', 'صباحا', $arabic_format);
                    $arabic_format = str_replace('pm', 'مساءا', $arabic_format);

                    if ($event_date->start_time != '00:00:00') {
                        $dates = '<p><strong>' . $date_ranges . '</strong></p>
						<p>' . $arabic_format . '</p>';
                    } else {
                        $dates = '<p><strong>' . $date_ranges . '</strong></p>';
                    }
                }

            }

            $location = content_detail('location', $event_listing->id);
            $location_cordinates = explode(',', $location);

            if ($location != '') {
                $maping_area = '<div class="box">
					<div class="icon"><i class="edSprite pinIcon"></i></div>
					
					<p><strong>' . $city . '</strong></p>
					<p>' . ($lang == 'eng' ? 'View on map' : 'عرض الخريطة ') . '</p>
				</div>';
            } else {
                $maping_area = '<div class="box">
					<p><strong>' . $city . '</strong></p>
					<p></p>
				</div>';
            }

            $html .= '
			<li class="col-lg-4 col-sm-6 col-xs-12" style="display: inline-block;">
			   <a href="' . lang_base_url() . $url . 'page/event_details/' . $event_listing->id . '">
					<div class="eventCard">
						<div class="imgBox">
							<img src="' . $image . '" alt="Event" height="269" width="412" />
						</div>
						<div class="text">
							<h3>' . pageTitle($event_listing->id, $lang) . '</h3>
							<h4>' . ($lang == 'eng' ? $category->eng_name : $category->arb_name) . '</h4>
						</div>
						<div class="timeLoc">
							<div class="box">
								<div class="icon"><i class="edSprite clockIcon"></i></div>
								' . $dates . '
							</div>
							' . $maping_area . '
							<div class="clearfix"></div>
						</div>
					</div>
				</a>
			</li>';
        }

        $resArr['html'] = $html;
        $resArr['offset_value'] = $offset_value + 6;
        $resArr['count_users'] = count($data['events']);

        if ($resArr != '') {
            $resArr['success'] = "Events Retrieved Successfully";
        }

        echo json_encode($resArr);
        exit();

    }


    public function loadMoreUsersArchieve()
    {
        $lang = html_escape($this->input->post('lang'));
        if ($lang == 'eng') {
            $url = 'en/';
        } else {
            $url = '';
        }
        $offset_value = html_escape($this->input->post('offset_value'));
        $city = html_escape($this->input->post('city'));
        $limit = 6;
        $data['events'] = $this->model_contents->front_end_event_archived_past($offset_value, $limit, $city);

        $html = '';
        foreach ($data['events'] as $event_listing) {
            $city = content_detail('city', $event_listing->id);
            $city_name = getCityName(content_detail('city', $event_listing->id));

            if (content_detail('eng_event_thumb_image_mkey_hdn', $event_listing->id) != '') {
                $image = base_url('assets/script/' . content_detail('eng_event_thumb_image_mkey_hdn', $event_listing->id));
            } else {
                $image = base_url('assets/frontend/images/black.jpg');
            }

            $categor_id = content_detail('category', $event_listing->id);
            $category = getCategoryName($categor_id);
            if ($lang == 'eng') {
                $city = $city_name->eng_name;
            } else {
                $city = $city_name->arb_name;
            }

            $event_dates = getEventDatesAndTimes($event_listing->id);
            if (count($event_dates) != 0) {
                $k = 0;
                foreach ($event_dates as $event_date) {
                    $k++;
                }

                if ($lang == 'eng') {
                    if ($event_dates[0]->event_date == $event_dates[$k - 1]->event_date) {
                        $date_ranges = (date('d M', strtotime($event_dates[0]->event_date)));

                        if ($event_date->start_time != '00:00:00') {
                            $dates = '<p><strong>' . $date_ranges . '</strong></p>
							<p>' . date('h:i a', strtotime($event_dates[0]->start_time)) . ' - ' . date('h:i a', strtotime($event_dates[0]->end_time)) . '</p>';
                        } else {
                            $dates = '<p><strong>' . $date_ranges . '</strong></p>';
                        }

                    } else {
                        if ($event_date->start_time != '00:00:00') {
                            $date_ranges = (date('d M', strtotime($event_dates[0]->event_date))) . ' - ' . (date('d M', strtotime($event_dates[$k - 1]->event_date)));

                            $dates = '<p><strong>' . $date_ranges . '</strong></p>
							
							<p>' . date('h:i a', strtotime($event_dates[0]->start_time)) . ' - ' . date('h:i a', strtotime($event_dates[0]->end_time)) . '</p>';
                        } else {
                            $date_ranges = (date('d M', strtotime($event_dates[0]->event_date))) . ' - ' . (date('d M', strtotime($event_dates[$k - 1]->event_date)));

                            $dates = '<p><strong>' . $date_ranges . '</strong></p>';
                        }
                    }

                } else {

                    $date0 = date(' d', strtotime($event_dates[0]->event_date));
                    $month0 = date('M', strtotime($event_dates[0]->event_date));
                    $date_month0 = arabic_w2e($date0) . ' ' . month_arabic($month0);

                    $date1 = date(' d', strtotime($event_dates[$k - 1]->event_date));
                    $month1 = date('M', strtotime($event_dates[$k - 1]->event_date));
                    $date_month1 = arabic_w2e($date1) . ' ' . month_arabic($month1);

                    if ($event_dates[0]->event_date == $event_dates[$k - 1]->event_date) {
                        $date_ranges = $date_month0;
                    } else {
                        $date_ranges = $date_month0 . ' - ' . $date_month1;
                    }

                    $arabic_format = arabic_w2e(date('h:i a', strtotime($event_dates[0]->start_time))) . ' - ' . arabic_w2e(date('h:i a', strtotime($event_dates[0]->end_time)));

                    $arabic_format = str_replace('am', 'صباحا', $arabic_format);
                    $arabic_format = str_replace('pm', 'مساءا', $arabic_format);

                    if ($event_date->start_time != '00:00:00') {
                        $dates = '<p><strong>' . $date_ranges . '</strong></p>
						<p>' . $arabic_format . '</p>';
                    } else {
                        $dates = '<p><strong>' . $date_ranges . '</strong></p>';
                    }
                }

            }

            $location = content_detail('location', $event_listing->id);
            $location_cordinates = explode(',', $location);

            if ($location != '') {
                $maping_area = '<div class="box">
					<div class="icon"><i class="edSprite pinIcon"></i></div>
					
					<p><strong>' . $city . '</strong></p>
					<p>' . ($lang == 'eng' ? 'View on map' : 'عرض الخريطة ') . '</p>
				</div>';
            } else {
                $maping_area = '<div class="box">
					<p><strong>' . $city . '</strong></p>
					<p></p>
				</div>';
            }

            $html .= '
			<li class="col-lg-4 col-sm-6 col-xs-12" style="display: inline-block;">
			   <a href="' . lang_base_url() . $url . 'page/event_details/' . $event_listing->id . '">
					<div class="eventCard">
						<div class="imgBox">
							<img src="' . $image . '" alt="Event" height="269" width="412" />
						</div>
						<div class="text">
							<h3>' . pageTitle($event_listing->id, $lang) . '</h3>
							<h4>' . ($lang == 'eng' ? $category->eng_name : $category->arb_name) . '</h4>
						</div>
						<div class="timeLoc">
							<div class="box">
								<div class="icon"><i class="edSprite clockIcon"></i></div>
								' . $dates . '
							</div>
							' . $maping_area . '
							<div class="clearfix"></div>
						</div>
					</div>
				</a>
			</li>';
        }

        $resArr['html'] = $html;
        $resArr['offset_value'] = $offset_value + 6;
        $resArr['count_users'] = count($data['events']);

        if ($resArr != '') {
            $resArr['success'] = "Events Retrieved Successfully";
        }

        echo json_encode($resArr);
        exit();

    }


    public function archived($page_title = 'Home')
    {

        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();

        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name;
        } else {

            $segment = $this->uri->segment(2);
            $data['project_name'] = $config_settings->project_name_arb;
        }

        if ($segment != '') {
            $page_title = $segment;
        }
        if (is_numeric($page_title)) {
            $page = $page_title;
        } else {
            $page_title = str_replace('_', ' ', $page_title);
            $page = getPageId($page_title);

        }


        $page_id = $page;
        $data['page_id'] = $page_id;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_id);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['content'] = 'archived';
        $data['cities'] = $this->model_contents->get_only_event_cities();
        $data['categories'] = $this->model_contents->getAllCategories();
        $data['listings'] = ListingContent($page_id);

        $data['contents'] = $contents;

        $data['parent_id'] = $parent_id;
        $this->load->view("layouts/default", $data);
    }

    public function search()
    {

        $data = array();
        $items = array();
        $itemsContent = array();
        $itemsInsurances = array();
        $lang = $this->session->userdata('lang');
        // $item_name = html_escape($this->input->get('item_title'));
        $item_name = $this->db->escape_str(html_escape($this->input->get('item_title')));
        $config_settings = getConfigurationSetting();

        if ($lang == 'eng') {
            $data['project_name'] = $config_settings->project_name;
        } else {
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if (html_escape($this->input->get('item_title'))) {
            $itemsContent = getAllData($item_name);
        }

        $allRecords = $itemsContent;

        $data['item_title'] = $item_name;
        $data['lang'] = $lang;

        if ($allRecords) {
            $data['items'] = $allRecords;

        } else {

            if (html_escape($this->input->get('item_title'))) {

                if ($lang == 'eng') {
                    $data['message'] = 'No result found';
                } else {
                    $data['message'] = 'لم يتم العثور على نتائج';
                }
            }
        }
        $data['tpl_name'] = 'search';
        $data['content'] = 'search';
        $this->load->view("layouts/default", $data);
    }

    public function searchEventsOld()
    {
        $lang = 'eng';
        $data = array();
        $items = array();

        //$lang = $this->session->userdata('lang');

        $city_id = html_escape($this->input->post('city_id'));
        $event_date = html_escape($this->input->post('event_date'));
        $category_id = html_escape($this->input->post('category_id'));

        $searched_events = array();
        if (html_escape($this->input->post('category_id')) || html_escape($this->input->post('city_id')) || html_escape($this->input->post('event_date'))) {
            $events = getAllEvents($category_id, $city_id, $event_date);
            //echo $this->db->last_query();
            //Exit;
            foreach ($events as $event) {
                $event_id = $event->post_id;
                // check event on date.
                $checkEventOnDate = $this->model_contents->checkEventDate($event_id, $event_date);

                if ($checkEventOnDate) {
                    $searched_events[] = $event;
                }
            }
        }

        if ($events) {
            $data['events'] = $searched_events;

        } else {
            if ($lang == 'eng') {
                $data['message'] = 'No result found';
            } else {
                $data['message'] = 'لم يتم العثور على نتائج';
            }
        }

        /* echo "<pre>";
		print_r($data['events']);
		exit; */

        $data['content'] = 'search';
        $this->load->view("layouts/default", $data);
    }

    public function searchEvents()
    {
        $lang = 'eng';
        $data = array();
        $items = array();

        //$lang = $this->session->userdata('lang');
        $lang = $this->session->userdata('lang');

        $city_id = html_escape($this->input->post('city'));
        $category_id = html_escape($this->input->post('categories'));
        $event_date = html_escape($this->input->post('event_date'));

        $events = array();
        if (html_escape($this->input->post('categories')) || html_escape($this->input->post('city')) || html_escape($this->input->post('event_date'))) {
            $events = $this->model_contents->checkEventDate($city_id, $category_id, $event_date);
        }
        if ($events) {
            $data['event_listings'] = $events;

        } else {
            if ($lang == 'eng') {
                $data['message'] = 'No result found';
            } else {
                $data['message'] = 'لم يتم العثور على نتائج';
            }
        }
        $data['cities'] = $this->model_contents->get_only_event_cities();
        $data['categories'] = $this->model_contents->getAllCategories();
        $data['lang'] = $lang;
        $data['content'] = 'search';
        $this->load->view("layouts/default", $data);
    }

    public function service($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $data['project_name'] = $config_settings->project_name;
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'service';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;
        $data['tpl_name'] = $contents[0]->tpl;
        $this->load->view("layouts/default", $data);

    }

    public function sub_service($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $data['project_name'] = $config_settings->project_name;
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }
        $data['list_content_sections'] = $this->model_contents->fetch_list_content_sections($page_id);
        $data['contents'] = $contents;
        $data['content'] = 'sub_service';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;
        $this->load->view("layouts/default", $data);

    }

    public function sub_sub_service($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $data['project_name'] = $config_settings->project_name;
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);

        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $sub_service_contents = $this->model_contents->page_content($contents[0]->parant_id);
        $main_service_contents = $this->model_contents->page_content($sub_service_contents[0]->parant_id);

        if ($main_parent_id == '' || $main_parent_id == 0) {
            $main_parent_id = $contents[0]->id;
        }

        $data['list_content_sections'] = $this->model_contents->fetch_list_content_sections($page_id);

        $data['contents'] = $contents;
        $data['content'] = 'sub_sub_service';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;
        $data['service_parent_id'] = $main_service_contents[0]->id;
        $data['sub_service_id'] = $sub_service_contents[0]->parant_id;
        $this->load->view("layouts/default", $data);

    }

    public function sub_sub_sub_service($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $data['project_name'] = $config_settings->project_name;
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);

        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $sub_service_contents = $this->model_contents->page_content($contents[0]->parant_id);
        $main_service_contents = $this->model_contents->page_content($sub_service_contents[0]->parant_id);

        $first_service_contents = $this->model_contents->page_content($main_service_contents[0]->id);

        if ($main_parent_id == '' || $main_parent_id == 0) {
            $main_parent_id = $contents[0]->id;
        }
        $data['list_content_sections'] = $this->model_contents->fetch_list_content_sections($page_id);
        $data['contents'] = $contents;
        $data['content'] = 'sub_sub_sub_service';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;
        $data['service_parent_id'] = $main_service_contents[0]->id;
        $data['sub_service_id'] = $sub_service_contents[0]->parant_id;
        $data['service_id'] = $first_service_contents[0]->id;
        $data['s_id'] = $first_service_contents[0]->parant_id;
        $this->load->view("layouts/default", $data);

    }

    public function reportings($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $data['project_name'] = $config_settings->project_name;
            $segment = $this->uri->segment(4);
        } else {
            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'reportings';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;
        $this->load->view("layouts/default", $data);

    }

    public function comedian($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $data['project_name'] = $config_settings->project_name;
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(4);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'comedian_detail';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;
        $this->load->view("layouts/default", $data);

    }

    public function book($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $data['project_name'] = $config_settings->project_name;
            $segment = $this->uri->segment(3);
        } else {
            $segment = $this->uri->segment(4);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        $data['contents'] = $contents;
        $data['content'] = 'comedian_contact';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;
        $this->load->view("layouts/default", $data);

    }

    public function event_details($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name;
        } else {
            $segment = $this->uri->segment(4);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }
        $data['cities'] = $this->model_contents->get_only_event_cities();
        $data['categories'] = $this->model_contents->getAllCategories();
        $data['contents'] = $contents;
        $data['content'] = 'event_details';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function bod_details($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name;
        } else {
            $segment = $this->uri->segment(4);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }
        $data['cities'] = $this->model_contents->get_only_event_cities();
        $data['categories'] = $this->model_contents->getAllCategories();
        $data['contents'] = $contents;
        $data['content'] = 'bod_details';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function awareness_detail($page_title = 'Home')
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $config_settings = getConfigurationSetting();
        if ($lang == 'eng') {
            $segment = $this->uri->segment(4);
            $data['project_name'] = $config_settings->project_name;
        } else {
            $segment = $this->uri->segment(3);
            $data['project_name'] = $config_settings->project_name_arb;
        }
        if ($segment != '') {
            $page_title = $segment;
        }
        $page_id = $page_title;
        $data['lang'] = $lang;
        $contents = $this->model_contents->page_content($page_title);
        $parent_id = getParentPageId($contents[0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }
        $data['cities'] = $this->model_contents->get_only_event_cities();
        $data['categories'] = $this->model_contents->getAllCategories();
        $data['contents'] = $contents;
        $data['content'] = 'awareness_news';
        $data['page_id'] = $page_id;
        $data['parent_id'] = $contents[0]->parant_id;

        $this->load->view("layouts/default", $data);

    }

    public function loadMoreReports($page_title = 'Home')
    {

        $tbl = html_escape($this->input->post("tbl"));
        $limit = html_escape($this->input->post("limit"));
        $offset = html_escape($this->input->post("offset"));
        $fund_type = html_escape($this->input->post("type"));
        $lang = html_escape($this->input->post("lang"));
        $parant_id = html_escape($this->input->post("parant_id"));

        $tbl = base64_decode($tbl);

        $news_listings = $this->model_contents->fetchMoreReports($parant_id, $offset, $limit);

        $html = '';
        foreach ($news_listings as $listingsReport) {
            $file_fund_reports = '';
            if ($lang == 'eng') {
                $file_fund_reports = content_detail('file_hdn', $listingsReport->id);
            } else {
                $file_fund_reports = content_detail('file_arb_hdn', $listingsReport->id);
            }
            if ($file_fund_reports == '') {
                $file_fund_reports = 'javascript:void(0);';
            } else {
                $file_fund_reports = base_url() . 'uploads/pdf/' . $file_fund_reports;
            }

            $icon = '';
            if ($lang == 'eng') {
                $icon = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
            } else {
                $icon = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
            }

            $html .= '<a href="$file_fund_reports;" target="_blank" download="">
				<div class="newsbox">
					<span>' . $listingsReport->date . '</span>
					<h4>' . pageTitle($listingsReport->id, $lang) . '</h4>
					<p>' . strip_tags(content_detail($lang . '_report_desc', $listingsReport->id)) . '</p>
					<button class="btn readmore">' . $icon . '</button>
				</div>
			</a>';
        }

        $response = array(
            'total_rows' => count($news_listings),
            'html' => $html,
        );

        echo json_encode($response);
        exit();

    }

    public function sendToFriend()
    {

        $yr_email = html_escape($this->input->post('yr_email'));
        $fr_email = html_escape($this->input->post('fr_email'));
        $event_link = html_escape($this->input->post('page_url'));
        $lang = html_escape($this->input->post('lang'));
        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";
        $msg = "";
        if ($lang == 'eng') {
            $subject = "Share";
            $msg .= $yr_email . " " . "shared a link with you.";
            $msg .= "<a target='_blank' href=" . $event_link . ">" . $event_link . "</a>";


        } else {
            $subject = "مشاركة الفعالية";
            $msg .= " لقد تم مشاركة هذه الصفحة من قبل " . ' ' . $yr_email;
            $msg .= '<br/>';
            $msg .= "<a target='_blank' href=" . $event_link . ">" . $event_link . "</a>";

        }
        //$send_email = $this->send_email_to_friend($subject, $msg, $yr_email, $fr_email);

        if ($lang == 'eng') {
            $subject = 'Share';
            $title = 'Share';
        } else {
            $subject = 'مشاركة';
            $title = 'مشاركة';
        }

        $send_email = userGeneralEmailShare($subject, $msg, $yr_email, $fr_email, $lang);

        if ($send_email) {
            $resArr['status'] = true;
        } else {
            $resArr['status'] = false;
        }
        echo json_encode($resArr);
        exit();
    }

    public function SaveEventUserAndGuestRegistration()
    {
        $data = array();
        $event_ids = html_escape($this->input->post('event_id'));
        $langs = html_escape($this->input->post('lang'));
        $user_names = html_escape($this->input->post('user_name'));
        $emails = html_escape($this->input->post('email'));
        $mobile_nos = html_escape($this->input->post('mobile_no'));
        $passwords = html_escape($this->input->post('password'));
        $interests = html_escape($this->input->post('interest'));
        $lang = $langs[0];
        $event_id = $event_ids[0];

        $sold_seats = getSoldSeats($event_id, 'all');
        $total_seats = content_detail('eng_seats', $event_id);
        $remaining_seats = ($total_seats - $sold_seats);

        if ($remaining_seats < count($user_names)) {
            $resArr['status'] = false;
            $resArr['message_email_exist'] = ($lang == 'eng' ? 'Not enough seats, only ' . $remaining_seats . ' seats left' : 'ايوجد مقاعد كافية، ' . $remaining_seats . ' مقاعد متبقية فقط');
            $resArr['message_email_exist'] .= '<br/> <a href="javascript:void(0);" class="go_back_link">' . ($lang == 'eng' ? 'click here to go back' : 'اضغط هنا للرجوع') . '</a>';
        } else {
            $i = 0;
            foreach ($user_names as $user_name) {
                $user_data['user_name'] = $user_names[$i];
                $user_data['email'] = $emails[$i];
                $user_data['mobile_no'] = $mobile_nos[$i];
                $user_data['password'] = $passwords[$i];
                $user_data['created_at'] = date('Y-m-d h:i:s');

                // Register user save data 1st
                if ($i == 0) {

                    $userRecord = $this->model_contents->saveUser($user_data);

                    $dataNew['user_id'] = $userRecord;
                    $dataNew['event_id'] = $event_id;
                    $dataNew['no_of_reservations'] = count($user_names);
                    $userEventRegistration = $this->model_contents->saveEventRegistraions($dataNew);

                    // This is to save the register user and guest user information in reservation table.
                    $password = $user_data['password'];
                    unset($user_data['password']);
                    unset($user_data['lang']);
                    $dataReservation = $user_data;
                    $dataReservation['user_id'] = $userRecord;
                    $dataReservation['role'] = 1;
                    $dataReservation['user_event_id'] = $userEventRegistration;
                    $userAndGuestUserRegistration = $this->model_contents->saveReservations($dataReservation);

                    $user_qr_code['qr_code'] = $this->generateQr($userAndGuestUserRegistration);
                    $user_qr_code['qr_code'] = str_replace('uploads/qr_codes/', '', $user_qr_code['qr_code']);

                    // update user qr_code which is saved in guest table.
                    $updated_qr_code = $this->model_contents->updateUserQrCode($userAndGuestUserRegistration, $user_qr_code);

                    // if user saved then save there interests
                    $interests = explode(',', $interests[0]);

                    foreach ($interests as $interest) {
                        $user_interests['user_id'] = $userRecord;
                        $user_interests['interest_id'] = $interest;
                        $user_interests['event_id'] = '';
                        $this->model_contents->saveUserInterests($user_interests);
                    }


                    $getReservationDetails = $this->model_contents->getReservationDetails($userAndGuestUserRegistration);

                    $emailData['user_name'] = $user_data['user_name'];
                    $emailData['email'] = $user_data['email'];
                    $emailData['mobile_no'] = $user_data['mobile_no'];
                    $emailData['password'] = $password;
                    $emailData['event_name'] = ($lang == 'eng' ? $getReservationDetails->eng_title : $getReservationDetails->arb_title);
                    $emailData['event_id'] = $getReservationDetails->event_id;
                    $emailData['reservation_no'] = $getReservationDetails->reservation_no;
                    $emailData['booking_date'] = strtotime($getReservationDetails->created_at);
                    $emailData['booking_date'] = date('M d, Y', $emailData['booking_date']);
                    $emailData['interest'] = $userRecord;
                    $emailData['lang'] = $lang;
                    $emailData['qr_code'] = $user_qr_code['qr_code'];

                    $reg_date = strtotime($getReservationDetails->event_date);
                    $date_as = date('M d, Y', $reg_date);

                    $emailData['event_date'] = $date_as;
                    $emailData['event_time'] = date('h:i a', strtotime($getReservationDetails->start_time)) . ' - ' . date('h:i a', strtotime($getReservationDetails->end_time));

                    if ($lang == 'eng') {
                        $subject = "Event Registraion";
                        $msg = "";
                        $msg = $name . "! " . "Thanks for your registraion";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    } else {
                        $subject = "تأكيد تسجيل";
                        $msg = "";
                        $msg = $name . "! " . "شكرا للتسجيل الخاص بك";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    }

                    $to = $emails[$i];
                    $this->send_email($subject, $message_hr, $to);
                    $conf = $this->model_configuration->fetchRow();
                    //$this->send_email_admin($subject, $message_hr, $conf->sendinquiry_email);


                } else {
                    // This is to save the register user and guest user information in reservation table.
                    unset($user_data['password']);
                    unset($emailData['password']);
                    unset($user_data['lang']);
                    //unset($user_data['qr_code']);


                    $dataReservation = $user_data;
                    $dataReservation['user_id'] = $userRecord;
                    $dataReservation['role'] = 0;
                    $dataReservation['user_event_id'] = $userEventRegistration;
                    $userAndGuestUserRegistration = $this->model_contents->saveReservations($dataReservation);

                    $user_qr_code['qr_code'] = $this->generateQrCopy($userAndGuestUserRegistration);
                    $user_qr_code['qr_code'] = str_replace('uploads/qr_codes/', '', $user_qr_code['qr_code']);

                    // update user qr_code which is saved in guest table.
                    $updated_qr_code = $this->model_contents->updateUserQrCode($userAndGuestUserRegistration, $user_qr_code);

                    $getReservationDetails = $this->model_contents->getReservationDetails($userAndGuestUserRegistration);

                    $emailData['user_name'] = $user_data['user_name'];
                    $emailData['email'] = $user_data['email'];
                    $emailData['mobile_no'] = $user_data['mobile_no'];
                    $emailData['event_name'] = ($lang == 'eng' ? $getReservationDetails->eng_title : $getReservationDetails->arb_title);
                    $emailData['event_id'] = $getReservationDetails->event_id;
                    $emailData['reservation_no'] = $getReservationDetails->reservation_no;
                    $emailData['booking_date'] = strtotime($getReservationDetails->created_at);
                    $emailData['booking_date'] = date('M d, Y', $emailData['booking_date']);
                    $emailData['interest'] = $userRecord;
                    $emailData['lang'] = $lang;
                    $emailData['password'] = '';
                    $emailData['qr_code'] = $user_qr_code['qr_code'];

                    $reg_date = strtotime($getReservationDetails->event_date);
                    $date_as = date('M d, Y', $reg_date);

                    $emailData['event_date'] = $date_as;
                    $emailData['event_time'] = date('h:i a', strtotime($getReservationDetails->start_time)) . ' - ' . date('h:i a', strtotime($getReservationDetails->end_time));

                    if ($lang == 'eng') {
                        $subject = "Event Registraion";
                        $msg = "";
                        $msg = $name . "! " . "Thanks for your registraion";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    } else {
                        $subject = "تأكيد تسجيل";
                        $msg = "";
                        $msg = $name . "! " . "شكرا للتسجيل الخاص بك";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    }

                    $to = $emails[$i];
                    $this->send_email($subject, $message_hr, $to);
                    $conf = $this->model_configuration->fetchRow();
                    //$this->send_email_admin($subject, $message_hr, $conf->sendinquiry_email);

                }

                $i++;
            }
        }

        if ($userRecord > 0) {
            $resArr['status'] = true;
            $resArr['message'] = ($lang == 'eng' ? 'Registration Successful' : 'تم التسجيل بنجاح');
            $resArr['seats_left'] = $seats_left;
            $resArr['message_email_exist'] = '';
            $resArr['no_of_reservations'] = $no_of_reservations;
        } else {
            $resArr['status'] = false;
            $resArr['message'] = ($lang == 'eng' ? 'Registration failed please try again' : 'حدث خطأ في التسجيل، نرجو المحاولة مرة أخرى');
        }
        echo json_encode($resArr);
        exit();
    }

    public function saveContactData()
    {
        $lang_session = $this->session->userdata('lang');
        $user_data['lang'] = $lang = html_escape($this->input->post('lang'));
        $user_data['name'] = $name = html_escape($this->input->post('name'));
        $user_data['country'] = $country = html_escape($this->input->post('country'));
        $user_data['email'] = $email = html_escape($this->input->post('email'));
        $user_data['phone'] = $mobile = html_escape($this->input->post('phone'));
        $user_data['message'] = $message_form = html_escape($this->input->post('message'));
        $user_data['created_at'] = date("Y-m-d H:i:s");

        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";

        if ($lang == 'eng') {
            $subject = "Contact us inquiry";
            $msg = $name . "! " . "Thanks for your message, you will be contacted soon.";
            $message_hr = "";
            $message_hr .= "<!DOCTYPE html><html>";
            $message_hr .= "<head><meta http-equiv='Content-Type' content='text/html charset=UTF-8'/></head>";
            $message_hr .= "<body>";
            $message_hr .= "<img src='" . base_url() . "assets/frontend/images/logo.png' height='80' width='80'>";
            $message_hr .= "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            $message_hr .= "<tr style='background: #eee;'><td><strong>Full Name:</strong> </td><td>" . $name . "</td></tr>";
            $message_hr .= "<tr><td><strong>Email:</strong> </td><td>" . $email . "</td></tr>";
            $message_hr .= "<tr><td><strong>Mobile:</strong> </td><td>" . $mobile . "</td></tr>";
            $message_hr .= "<tr><td><strong>Message:</strong> </td><td>" . $message_form . "</td></tr>";
            $message_hr .= "</table>";
            $message_hr .= "</body></html>";
        } else {
            $subject = "تواصل معنا";
            $msg = "";
            $msg .= "<!DOCTYPE html><html>";
            $msg .= "<head><meta http-equiv='Content-Type' content='text/html charset=UTF-8'/></head>";
            $msg .= "<body style='direction:rtl;'>";
            $msg .= "<div class='email-text'>";
            $msg .= $name . "! " . "شكراً على رسالتك, سوف يتم الإتصال بك قريباً.";
            $msg .= "</div>";
            $msg .= "</body></html>";

            $message_hr = "";
            $message_hr .= "<!DOCTYPE html><html>";
            $message_hr .= "<head><meta http-equiv='Content-Type' content='text/html charset=UTF-8'/></head>";
            $message_hr .= "<body style='direction:rtl;'>";
            $message_hr .= "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            $message_hr .= "<tr style='background: #eee;'><td><strong>الإسم الكامل:</strong> </td><td>" . $name . "</td></tr>";
            $message_hr .= "<tr><td><strong>البريد الإلكترونى:</strong> </td><td>" . $email . "</td></tr>";
            $message_hr .= "<tr><td><strong>رقم الجوال:</strong> </td><td>" . $mobile . "</td></tr>";
            $message_hr .= "<tr><td><strong>الرسالة:</strong> </td><td>" . $message_form . "</td></tr>";
            $message_hr .= "</table>";
            $message_hr .= "</body></html>";
        }
        $to = $email;
        $this->send_email($subject, $msg, $to);
        $conf = $this->model_configuration->fetchRow();
        $this->send_email_admin($subject, $message_hr, $conf->sendinquiry_email);

        $userRecord = $this->model_contents->saveContactUs($user_data);
        if ($userRecord > 0) {
            $resArr['status'] = true;
        } else {
            $resArr['status'] = false;
        }
        echo json_encode($resArr);
        exit();
    }

    public function SaveEventRegistraion()
    {
        $lang_session = $this->session->userdata('lang');
        $event_id = html_escape($this->input->post('event_id'));
        $user_data['lang'] = $lang = html_escape($this->input->post('lang'));
        $user_data['user_name'] = $name = html_escape($this->input->post('user_name'));
        $user_data['no_of_reservations'] = $no_of_reservations = html_escape($this->input->post('no_of_reservations'));
        $user_data['email'] = $email = html_escape($this->input->post('email'));
        $user_data['mobile_no'] = $mobile_no = html_escape($this->input->post('mobile_no'));
        $user_data['password'] = $password = html_escape($this->input->post('password'));
        $interests = html_escape($this->input->post('interest'));
        $user_data['created_at'] = date("Y-m-d H:i:s");
        //$user_data['event_id'] = $event_id;

        $emailData = array();
        $emailData = $user_data;

        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";

        $email_exist = $this->model_contents->checkEmailExist($email);

        if ($email_exist != '') {
            $resArr['status'] = false;
            $resArr['message_email_exist'] = ($lang == 'eng' ? 'Email already exist please login with your email.' : 'البريد الإلكتروني موجود بالفعل يرجى تسجيل الدخول بالبريد الإلكتروني الخاص بك للتسجيل في هذا الحدث.');
        } else {

            if ($event_id != '') {

                $sold_seats = getSoldSeats($event_id, 'all');
                $total_seats = content_detail('eng_seats', $event_id);
                $remaining_seats = ($total_seats - $sold_seats);

                if ($remaining_seats < $no_of_reservations) {
                    $resArr['status'] = false;
                    $resArr['message_email_exist'] = ($lang == 'eng' ? 'Not enough seats, only ' . $remaining_seats . ' seats left' : 'ايوجد مقاعد كافية، ' . $remaining_seats . ' مقاعد متبقية فقط');
                } else {

                    // New Registration New flow

                    if ($no_of_reservations > 1) {
                        $guest_forms_html = '';
                        for ($i = 1; $i <= $no_of_reservations; $i++) {
                            if ($i == 1) {
                                $readonly = 'readonly';
                                $name = $user_data['user_name'];
                                $email = $user_data['email'];
                                $mobile_no = $user_data['mobile_no'];
                                $password = $user_data['password'];
                                $lang = $user_data['lang'];
                                $u_interest = implode(',', $interests);
                                $label = ($lang == 'eng' ? 'Your Information' : 'المعلومات الشخصية ');

                            } else {
                                $readonly = '';
                                $name = '';
                                $email = '';
                                $mobile_no = '';
                                $password = '';
                                $u_interest = '';
                                $lang = $lang;
                                $label = ($lang == 'eng' ? 'Guest Information ' : ' معلومات الزائرين ') . ($i - 1);
                            }

                            $guest_forms_html .= '
							<div class="col-md-4">
								<div class="form-group">
									<label class="col-form-label">' . $label . '</label>
									<input type="text" placeholder="' . ($lang == 'eng' ? 'Enter name' : 'ادخل الاسم ') . '" class="form-control custom_validation" value="' . $name . '" name="user_name[]" attr-name="user_name" ' . $readonly . '>
								</div>
								<div class="form-group">
									<input type="text" placeholder="' . ($lang == 'eng' ? 'Enter email' : 'ادخل الإيميل ') . '" class="form-control custom_validation" value="' . $email . '" name="email[]" attr-name="email" ' . $readonly . '>
								</div>
								<div class="form-group">
									<input type="text" placeholder="' . ($lang == 'eng' ? 'Enter mobile no' : 'ادخل رقم الجوال') . '" class="form-control custom_validation" value="' . $mobile_no . '" name="mobile_no[]" attr-name="mobile_no" ' . $readonly . '>
								</div>
								<div class="form-group">
									<input type="hidden" class="form-control" value="' . $password . '" name="password[]" attr-name="mobile_no" ' . $readonly . '>
								</div>
								<div class="form-group">
									<input type="hidden" class="form-control" value="' . $u_interest . '" name="interest[]" attr-name="mobile_no" ' . $readonly . '>
								</div>
								<div class="form-group">
									<input type="hidden" class="form-control" value="' . $lang . '" name="lang[]" attr-name="lang" ' . $readonly . '>
								</div>
								<div class="form-group">
									<input type="hidden" class="form-control" value="' . $event_id . '" name="event_id[]" attr-name="event_id" ' . $readonly . '>
								</div>
							</div>
							';
                        }
                        $guest_forms_html .= '<div class="clearfix"> </div>';
                        $resArr['guest_forms'] = $guest_forms_html;
                        $resArr['no_of_reservations'] = $no_of_reservations;
                        $resArr['message_email_exist'] = '';
                    } else {
                        unset($user_data['no_of_reservations']);

                        $userRecord = $this->model_contents->saveUser($user_data);

                        if ($userRecord > 0) {

                            $dataNew['user_id'] = $userRecord;
                            $dataNew['event_id'] = $event_id;
                            $dataNew['no_of_reservations'] = 1;
                            $userEventRegistration = $this->model_contents->saveEventRegistraions($dataNew);

                            // This is to save the register user and guest user information in reservation table.
                            $password = $user_data['password'];
                            unset($user_data['password']);
                            unset($user_data['lang']);
                            $dataReservation = $user_data;
                            $dataReservation['user_id'] = $userRecord;
                            $dataReservation['role'] = 1;
                            $dataReservation['user_event_id'] = $userEventRegistration;
                            $userAndGuestUserRegistration = $this->model_contents->saveReservations($dataReservation);

                            $user_qr_code['qr_code'] = $this->generateQr($userAndGuestUserRegistration);
                            $user_qr_code['qr_code'] = str_replace('uploads/qr_codes/', '', $user_qr_code['qr_code']);

                            // update user qr_code which is saved in guest table.
                            $updated_qr_code = $this->model_contents->updateUserQrCode($userAndGuestUserRegistration, $user_qr_code);

                            // if user saved then save there interests
                            $interests = explode(',', $interests[0]);

                            foreach ($interests as $interest) {
                                $user_interests['user_id'] = $userRecord;
                                $user_interests['interest_id'] = $interest;
                                $user_interests['event_id'] = '';
                                $this->model_contents->saveUserInterests($user_interests);
                            }


                            $getReservationDetails = $this->model_contents->getReservationDetails($userAndGuestUserRegistration);

                            $emailData['user_name'] = $user_data['user_name'];
                            $emailData['email'] = $user_data['email'];
                            $emailData['mobile_no'] = $user_data['mobile_no'];
                            $emailData['password'] = $password;
                            $emailData['event_name'] = ($lang == 'eng' ? $getReservationDetails->eng_title : $getReservationDetails->arb_title);
                            $emailData['event_id'] = $getReservationDetails->event_id;
                            $emailData['reservation_no'] = $getReservationDetails->reservation_no;
                            $emailData['booking_date'] = strtotime($getReservationDetails->created_at);
                            $emailData['booking_date'] = date('M d, Y', $emailData['booking_date']);
                            $emailData['interest'] = $userRecord;
                            $emailData['lang'] = $lang;
                            $emailData['qr_code'] = $user_qr_code['qr_code'];

                            $reg_date = strtotime($getReservationDetails->event_date);
                            $date_as = date('M d, Y', $reg_date);

                            $emailData['event_date'] = $date_as;
                            $emailData['event_time'] = date('h:i a', strtotime($getReservationDetails->start_time)) . ' - ' . date('h:i a', strtotime($getReservationDetails->end_time));


                            $emailData['user_name'] = $user_data['user_name'];
                            $emailData['email'] = $email = $user_data['email'];
                            $emailData['mobile_no'] = $user_data['mobile_no'];
                            $emailData['password'] = $user_data['password'];
                            $emailData['interest'] = $userRecord;

                            if ($lang == 'eng') {
                                $subject = "Event Registraion";
                                $msg = "";
                                $msg = $name . "! " . "Thanks for your registraion";
                                $message_hr = "";
                                $message_hr = $this->load->view('table-email', $emailData, TRUE);
                            } else {
                                $subject = "تأكيد تسجيل";
                                $msg = "";
                                $msg = $name . "! " . "شكرا للتسجيل الخاص بك";
                                $message_hr = "";
                                $message_hr = $this->load->view('table-email', $emailData, TRUE);
                            }

                            $to = $email;
                            $this->send_email($subject, $message_hr, $to);
                            $conf = $this->model_configuration->fetchRow();
                            //$this->send_email_admin($subject, $message_hr, $conf->sendinquiry_email);

                            $resArr['status'] = true;
                            $resArr['message'] = ($lang == 'eng' ? 'Registration Successful' : 'تم التسجيل بنجاح');
                            $resArr['seats_left'] = $seats_left;
                            $resArr['message_email_exist'] = '';
                            $resArr['no_of_reservations'] = $no_of_reservations;
                        } else {
                            $resArr['status'] = false;
                            $resArr['message'] = ($lang == 'eng' ? 'Registration failed please try again' : 'حدث خطأ في التسجيل، نرجو المحاولة مرة أخرى');
                        }
                    }

                }
            } else {

                unset($user_data['no_of_reservations']);
                $userRecord = $this->model_contents->saveUser($user_data);

                if ($userRecord > 0) {

                    // if user saved then save there interests
                    foreach ($interests as $interest) {
                        $user_interests['user_id'] = $userRecord;
                        $user_interests['interest_id'] = $interest;
                        $user_interests['event_id'] = '';
                        $this->model_contents->saveUserInterests($user_interests);
                    }


                    $emailData['user_name'] = $user_data['user_name'];
                    $emailData['email'] = $email = $user_data['email'];
                    $emailData['mobile_no'] = $user_data['mobile_no'];
                    $emailData['password'] = $user_data['password'];
                    $emailData['interest'] = $userRecord;

                    if ($lang == 'eng') {
                        $subject = "Event Registraion";
                        $msg = "";
                        $msg = $name . "! " . "Thanks for your registraion";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    } else {
                        $subject = "تأكيد تسجيل";
                        $msg = "";
                        $msg = $name . "! " . "شكرا للتسجيل الخاص بك";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    }

                    $to = $email;
                    $this->send_email($subject, $message_hr, $to);
                    $conf = $this->model_configuration->fetchRow();
                    //$this->send_email_admin($subject, $message_hr, $conf->sendinquiry_email);

                    $resArr['status'] = true;
                    $resArr['message'] = ($lang == 'eng' ? 'Registration Successful' : 'تم التسجيل بنجاح');
                    $resArr['seats_left'] = $seats_left;
                    $resArr['message_email_exist'] = '';
                    $resArr['no_of_reservations'] = $no_of_reservations;
                } else {
                    $resArr['status'] = false;
                    $resArr['message'] = ($lang == 'eng' ? 'Registration failed please try again' : 'حدث خطأ في التسجيل، نرجو المحاولة مرة أخرى');
                }
            }


        }

        echo json_encode($resArr);
        exit();
    }

    public function SaveUserEventRegistration()
    {
        $lang_session = $this->session->userdata('lang');
        $lang = html_escape($this->input->post('lang'));
        $event_id = html_escape($this->input->post('event_id'));
        $user_id = html_escape($this->input->post('user_id'));
        $no_of_reservations = html_escape($this->input->post('no_of_reservations'));

        $sold_seats = getSoldSeats($event_id, 'all');
        $total_seats = content_detail('eng_seats', $event_id);
        $remaining_seats = ($total_seats - $sold_seats);

        if ($remaining_seats < $no_of_reservations) {
            $resArr['status'] = false;
            $resArr['message_email_exist'] = ($lang == 'eng' ? 'Not enough seats' : 'البريد الإلكتروني موجود بالفعل يرجى تسجيل الدخول بالبريد الإلكتروني الخاص بك للتسجيل في هذا الحدث.');
        } else {
            $dataNew['user_id'] = $user_id;
            $dataNew['event_id'] = $event_id;
            $dataNew['no_of_reservations'] = $no_of_reservations;
            $userEventRegistraion = $this->model_contents->saveEventRegistraions($dataNew);

            // This is to save the register user and guest user information in reservation table.

            $user_data = $this->model_contents->get_user_profile($user_id);
            $user_data->no_of_reservations = $no_of_reservations;

            /* unset($user_data->lang);
			unset($user_data->password);
			unset($user_data->status);
			unset($user_data->id); */

            $dataReservation['user_name'] = $user_data->user_name;
            $dataReservation['email'] = $user_data->email;
            $dataReservation['mobile_no '] = $user_data->mobile_no;
            $dataReservation['user_id'] = $user_id;
            $dataReservation['role'] = 1;
            $dataReservation['user_event_id'] = $userEventRegistraion;

            $userAndGuestUserRegistration = $this->model_contents->saveReservations($dataReservation);

            $sold_seats = getSoldSeats($event_id, 'all');
            $total_seats = content_detail('eng_seats', $event_id);
            $remaining_seats = ($total_seats - $sold_seats);

            if ($lang == 'eng') {
                $subject = "Event Registraion";
                $msg = "";
                $msg = $name . "! " . "Thanks for your registraion";
                $message_hr = "";
                $message_hr = $this->load->view('table-email', $user_data, TRUE);
            } else {
                $subject = "تأكيد تسجيل";
                $msg = "";
                $msg = $name . "! " . "شكرا للتسجيل الخاص بك";
                $message_hr = "";
                $message_hr = $this->load->view('table-email', $user_data, TRUE);
            }

            $to = $user_data->email;
            $this->send_email($subject, $message_hr, $to);
            $conf = $this->model_configuration->fetchRow();
            $this->send_email_admin($subject, $message_hr, $conf->sendinquiry_email);

            if ($lang == 'eng') {
                $seats_left = 'Remaining Seats: ' . $remaining_seats;
            } else {
                $seats_left = 'المقاعد المتبقية: ' . $remaining_seats;
            }

            if ($userEventRegistraion > 0) {
                $resArr['status'] = true;
                $resArr['message'] = ($lang == 'eng' ? 'Registration Successful' : 'تم التسجيل بنجاح');
                $resArr['seats_left'] = $seats_left;
            } else {
                $resArr['status'] = false;
                $resArr['message'] = ($lang == 'eng' ? 'Registration failed please try again' : 'حدث خطأ في التسجيل، نرجو المحاولة مرة أخرى');
            }
        }

        echo json_encode($resArr);
        exit();
    }

    public function showGuestUserForm()
    {
        // New Registration New flow
        $data = array();
        $event_id = html_escape($this->input->post('event_id'));
        $user_id = html_escape($this->input->post('user_id'));
        $no_of_reservations = html_escape($this->input->post('no_of_reservations'));
        $lang = html_escape($this->input->post('lang'));


        $sold_seats = getSoldSeats($event_id, 'all');
        $total_seats = content_detail('eng_seats', $event_id);
        $remaining_seats = ($total_seats - $sold_seats);

        if ($remaining_seats < $no_of_reservations) {
            $resArr['status'] = false;
            $resArr['message_email_exist'] = ($lang == 'eng' ? 'Not enough seats, only ' . $remaining_seats . ' seats left' : 'ايوجد مقاعد كافية، ' . $remaining_seats . ' مقاعد متبقية فقط');
        } else {
            $user_info = $this->model_contents->get_user_profile($user_id);
            $user_data['user_name'] = $user_info->user_name;
            $user_data['email'] = $user_info->email;
            $user_data['mobile_no'] = $user_info->mobile_no;

            $guest_forms_html = '';
            for ($i = 1; $i <= $no_of_reservations; $i++) {
                if ($i == 1) {
                    $readonly = 'readonly';
                    $name = $user_data['user_name'];
                    $email = $user_data['email'];
                    $mobile_no = $user_data['mobile_no'];
                    $label = ($lang == 'eng' ? 'Your Information' : 'المعلومات الشخصية ');

                } else {
                    $readonly = '';
                    $name = '';
                    $email = '';
                    $mobile_no = '';
                    $password = '';
                    $u_interest = '';
                    $lang = $lang;
                    $label = ($lang == 'eng' ? 'Guest Information ' : ' معلومات الزائرين ') . ($i - 1);
                }

                $guest_forms_html .= '
				<div class="col-md-4">
					<div class="form-group">
						<label class="col-form-label">' . $label . '</label>
						<input type="text" placeholder="' . ($lang == 'eng' ? 'Enter name' : 'ادخل الاسم ') . '" class="form-control custom_validation" value="' . $name . '" name="user_name[]" attr-name="user_name" ' . $readonly . '>
					</div>
					<div class="form-group">
						<input type="text" placeholder="' . ($lang == 'eng' ? 'Enter email' : 'ادخل الإيميل ') . '" class="form-control custom_validation" value="' . $email . '" name="email[]" attr-name="email" ' . $readonly . '>
					</div>
					<div class="form-group">
						<input type="text" placeholder="' . ($lang == 'eng' ? 'Enter mobile no' : 'ادخل رقم الجوال') . '" class="form-control custom_validation" value="' . $mobile_no . '" name="mobile_no[]" attr-name="mobile_no" ' . $readonly . '>
					</div>
					<div class="form-group">
						<input type="hidden" class="form-control" value="' . $lang . '" name="lang[]" attr-name="lang" ' . $readonly . '>
					</div>
					<div class="form-group">
						<input type="hidden" class="form-control" value="' . $event_id . '" name="event_id[]" attr-name="event_id" ' . $readonly . '>
					</div>
				</div>
				';
            }
            $guest_forms_html .= '<div class="clearfix"> </div>';
            $resArr['guest_forms'] = $guest_forms_html;
            $resArr['no_of_reservations'] = $no_of_reservations;
            $resArr['status'] = 1;
            $resArr['message_email_exist'] = '';
        }
        echo json_encode($resArr);
        exit();
    }

    public function updateProfile()
    {
        $data = array();
        $id = html_escape($this->input->post('user_id'));
        $data = $this->input->post();
        $interests = html_escape($this->input->post('interest'));

        unset($data['user_id']);
        unset($data['interest']);
        //This is to update the Registered user information in users table.
        $result = $this->model_contents->updateProfile($data, $id);

        //This is to update the registered user information in reservations table.
        unset($data['password']);
        $this->model_contents->updateProfileReservation($data, $id);

        // update Interests 1st delete old then save updated interests
        $this->model_contents->deleteUserInterests($id);

        foreach ($interests as $interest) {
            $user_interests['user_id'] = $id;
            $user_interests['interest_id'] = $interest;
            $user_interests['event_id'] = '';
            $this->model_contents->saveUserInterests($user_interests);
        }
        $lang == 'eng';
        if ($result == 1) {
            if ($lang == 'eng') {
                $result['msg'] = 'Update successfully';
            } else {
                $result['msg'] = 'تم تسجيل الدخول بنجاح';
            }

            $result['status'] = 1;
            $result['success'] = true;
            echo json_encode($result);
            exit();

        } else {
            if ($lang == 'eng') {
                $result['msg'] = 'Not Updated';
            } else {
                $result['msg'] = 'البريد الإلكتروني أو كلمة المرور غير صحيحة';
            }
            $result['status'] = 0;
            $result['success'] = false;

        }

        echo json_encode($result);
        exit();
    }

    public function SaveEventReservation()
    {

        /*
			$user_details = $this->model_contents->get_user_profile($data['user_id']);
			$event_details = $this->model_contents->fetchRow($data['event_id']);
			$event_date_times = $this->model_contents->fetchRowDateTime($data['event_id']);
		*/

        $data = array();
        $event_ids = html_escape($this->input->post('event_id'));
        $langs = html_escape($this->input->post('lang'));
        $user_names = html_escape($this->input->post('user_name'));
        $emails = html_escape($this->input->post('email'));
        $mobile_nos = html_escape($this->input->post('mobile_no'));
        $lang = $langs[0];
        $event_id = $event_ids[0];

        if ($this->session->userdata('user_id') != '') {
            $user_id = $this->session->userdata('user_id');
        }

        $sold_seats = getSoldSeats($event_id, 'all');
        $total_seats = content_detail('eng_seats', $event_id);
        $remaining_seats = ($total_seats - $sold_seats);

        if ($remaining_seats < count($user_names)) {
            $resArr['status'] = false;
            $resArr['message_email_exist'] = ($lang == 'eng' ? 'Not enough seats, only ' . $remaining_seats . ' seats left' : 'ايوجد مقاعد كافية، ' . $remaining_seats . ' مقاعد متبقية فقط');
            $resArr['message_email_exist'] .= '<br/> <a href="javascript:void(0);" class="go_back_link_after_login">' . ($lang == 'eng' ? 'click here to go back' : 'اضغط هنا للرجوع') . '</a>';
        } else {
            $i = 0;
            foreach ($user_names as $user_name) {
                $user_data['user_name'] = $user_names[$i];
                $user_data['email'] = $emails[$i];
                $user_data['mobile_no'] = $mobile_nos[$i];
                $user_data['created_at'] = date('Y-m-d h:i:s');

                // Register user save data 1st
                if ($i == 0) {

                    $userRecord = $user_id;

                    $dataNew['user_id'] = $userRecord;
                    $dataNew['event_id'] = $event_id;
                    $dataNew['no_of_reservations'] = count($user_names);
                    $userEventRegistration = $this->model_contents->saveEventRegistraions($dataNew);

                    // This is to save the register user and guest user information in reservation table.
                    $password = $user_data['password'];
                    unset($user_data['password']);
                    unset($user_data['lang']);
                    $dataReservation = $user_data;
                    $dataReservation['user_id'] = $userRecord;
                    $dataReservation['role'] = 1;
                    $dataReservation['user_event_id'] = $userEventRegistration;
                    $userAndGuestUserRegistration = $this->model_contents->saveReservations($dataReservation);

                    $user_qr_code['qr_code'] = $this->generateQr($userAndGuestUserRegistration);
                    $user_qr_code['qr_code'] = str_replace('uploads/qr_codes/', '', $user_qr_code['qr_code']);

                    // update user qr_code which is saved in guest table.
                    $updated_qr_code = $this->model_contents->updateUserQrCode($userAndGuestUserRegistration, $user_qr_code);


                    $getReservationDetails = $this->model_contents->getReservationDetails($userAndGuestUserRegistration);

                    $emailData['user_name'] = $user_data['user_name'];
                    $emailData['email'] = $user_data['email'];
                    $emailData['mobile_no'] = $user_data['mobile_no'];
                    $emailData['event_name'] = ($lang == 'eng' ? $getReservationDetails->eng_title : $getReservationDetails->arb_title);
                    $emailData['event_id'] = $getReservationDetails->event_id;
                    $emailData['reservation_no'] = $getReservationDetails->reservation_no;
                    $emailData['booking_date'] = strtotime($getReservationDetails->created_at);
                    $emailData['booking_date'] = date('M d, Y', $emailData['booking_date']);
                    $emailData['interest'] = $userRecord;
                    $emailData['lang'] = $lang;
                    $emailData['qr_code'] = $user_qr_code['qr_code'];

                    $reg_date = strtotime($getReservationDetails->event_date);
                    $date_as = date('M d, Y', $reg_date);

                    $emailData['event_date'] = $date_as;
                    $emailData['event_time'] = date('h:i a', strtotime($getReservationDetails->start_time)) . ' - ' . date('h:i a', strtotime($getReservationDetails->end_time));

                    if ($lang == 'eng') {
                        $subject = "Event Registraion";
                        $msg = "";
                        $msg = $name . "! " . "Thanks for your registraion";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    } else {
                        $subject = "تأكيد تسجيل";
                        $msg = "";
                        $msg = $name . "! " . "شكرا للتسجيل الخاص بك";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    }

                    $to = $emails[$i];
                    $this->send_email($subject, $message_hr, $to);
                    $conf = $this->model_configuration->fetchRow();
                    //$this->send_email_admin($subject, $message_hr, $conf->sendinquiry_email);


                } else {
                    // This is to save the register user and guest user information in reservation table.
                    unset($user_data['password']);
                    unset($emailData['password']);
                    unset($user_data['lang']);
                    //unset($user_data['qr_code']);


                    $dataReservation = $user_data;
                    $dataReservation['user_id'] = $userRecord;
                    $dataReservation['role'] = 0;
                    $dataReservation['user_event_id'] = $userEventRegistration;
                    $userAndGuestUserRegistration = $this->model_contents->saveReservations($dataReservation);

                    $user_qr_code['qr_code'] = $this->generateQrCopy($userAndGuestUserRegistration);
                    $user_qr_code['qr_code'] = str_replace('uploads/qr_codes/', '', $user_qr_code['qr_code']);

                    // update user qr_code which is saved in guest table.
                    $updated_qr_code = $this->model_contents->updateUserQrCode($userAndGuestUserRegistration, $user_qr_code);


                    $getReservationDetails = $this->model_contents->getReservationDetails($userAndGuestUserRegistration);

                    $emailData['user_name'] = $user_data['user_name'];
                    $emailData['email'] = $user_data['email'];
                    $emailData['mobile_no'] = $user_data['mobile_no'];
                    $emailData['event_name'] = ($lang == 'eng' ? $getReservationDetails->eng_title : $getReservationDetails->arb_title);
                    $emailData['event_id'] = $getReservationDetails->event_id;
                    $emailData['reservation_no'] = $getReservationDetails->reservation_no;
                    $emailData['booking_date'] = strtotime($getReservationDetails->created_at);
                    $emailData['booking_date'] = date('M d, Y', $emailData['booking_date']);
                    $emailData['interest'] = $userRecord;
                    $emailData['lang'] = $lang;
                    $emailData['password'] = '';
                    $emailData['qr_code'] = $user_qr_code['qr_code'];

                    $reg_date = strtotime($getReservationDetails->event_date);
                    $date_as = date('M d, Y', $reg_date);

                    $emailData['event_date'] = $date_as;
                    $emailData['event_time'] = date('h:i a', strtotime($getReservationDetails->start_time)) . ' - ' . date('h:i a', strtotime($getReservationDetails->end_time));

                    if ($lang == 'eng') {
                        $subject = "Event Registraion";
                        $msg = "";
                        $msg = $name . "! " . "Thanks for your registraion";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    } else {
                        $subject = "تأكيد تسجيل";
                        $msg = "";
                        $msg = $name . "! " . "شكرا للتسجيل الخاص بك";
                        $message_hr = "";
                        $message_hr = $this->load->view('table-email', $emailData, TRUE);
                    }

                    $to = $emails[$i];
                    $this->send_email($subject, $message_hr, $to);
                    $conf = $this->model_configuration->fetchRow();
                    //$this->send_email_admin($subject, $message_hr, $conf->sendinquiry_email);

                }

                $i++;
            }
        }

        if ($userRecord > 0) {
            $resArr['status'] = true;
            $resArr['message'] = ($lang == 'eng' ? 'Registration Successful' : 'تم التسجيل بنجاح');
            $resArr['seats_left'] = $seats_left;
            $resArr['message_email_exist'] = '';
            $resArr['no_of_reservations'] = $no_of_reservations;
        } else {
            $resArr['status'] = false;
            $resArr['message'] = ($lang == 'eng' ? 'Registration failed please try again' : 'حدث خطأ في التسجيل، نرجو المحاولة مرة أخرى');
        }
        echo json_encode($resArr);
        exit();

    }

    public function userforgotPassword()
    {
        $data = array();
        $email = html_escape($this->input->post('email'));
        $lang = html_escape($this->input->post('lang'));
        $check_email = $this->model_contents->checkEmailExist($email);

        $user_data = array();
        $user_data['lang'] = $lang;
        $user_data['email_id'] = $check_email->id;

        if ($check_email) {
            if ($lang == 'eng') {
                $subject = 'Forgot password';
            } else {
                $subject = 'هل نسيت كلمة المرور';
            }
            $message_hr = "";
            $message_hr = $this->load->view('forgot-email', $user_data, TRUE);
            $to = $email;
            $this->send_email($subject, $message_hr, $to);

            if ($lang == 'eng') {
                $result['msg'] = 'Email is sent to you please check for further details.';
            } else {
                $result['msg'] = 'يتم إرسال البريد الإلكتروني إليك ، يرجى التحقق من وجود المزيد من التفاصيل.';
            }

            $result['status'] = 1;
            $result['success'] = true;
            echo json_encode($result);
            exit();

        } else {
            if ($lang == 'eng') {
                $result['msg'] = 'You are not registered user';
            } else {
                $result['msg'] = 'أنت غير مسجل';
            }
            $result['status'] = 0;
            $result['success'] = false;
        }

        echo json_encode($result);
        exit();
    }


    public function updateUserPassword()
    {
        $data = array();
        $user_id = html_escape($this->input->post('user_id'));
        $email = html_escape($this->input->post('email'));
        $pass = html_escape($this->input->post('change_pass'));
        $confirm_pass = html_escape($this->input->post('con_change_pass'));
        $lang = html_escape($this->input->post('lang'));

        $check_user = $this->model_contents->checkUserExist($user_id);

        if ($check_user) {
            if ($pass != $confirm_pass) {
                if ($lang == 'eng') {
                    $result['msg'] = 'Password and confirm password not matched.';
                } else {
                    $result['msg'] = 'كلمة المرور وتأكيد كلمة المرور غير متطابقة';
                }
                $result['status'] = 0;
                $result['success'] = false;
            } else {
                $this->model_contents->updateUserPassword($user_id, $confirm_pass);
                if ($lang == 'eng') {
                    $result['msg'] = 'Password changed successfully.';
                } else {
                    $result['msg'] = 'تم تغيير الرقم السري بنجاح';
                }
                $result['status'] = 1;
                $result['success'] = true;
            }
        }
        echo json_encode($result);
        exit();
    }


    public function login_front()
    {

        $lang = html_escape($this->input->post('lang'));

        $result = $this->model_login->login_front(html_escape($this->input->post('login_email')), html_escape($this->input->post('login_password')));

        if ($result) {
            foreach ($result as $row) {
                $user_id = $row->id;
                $user_name = $row->user_name;
                $this->session->set_userdata(array('user_id' => $user_id, 'user_name' => $user_name));
            }
            if ($lang == 'eng') {
                $result['msg'] = 'Logged in successfully';
            } else {
                $result['msg'] = 'تم تسجيل الدخول بنجاح';
            }

            $result['status'] = 1;
            $result['success'] = true;
            /*redirect($this->config->item('base_url').'page/home');*/
            echo json_encode($result);
            exit();

        } else {
            if ($lang == 'eng') {
                $result['msg'] = 'Email or Password is incorrect';
            } else {
                $result['msg'] = 'البريد الإلكتروني أو كلمة المرور غير صحيحة';
            }
            $result['status'] = 0;
            $result['success'] = false;
            echo json_encode($result);
            exit();

        }

    }


    public function bookComedian()
    {
        $lang_session = $this->session->userdata('lang');
        $user_data['lang'] = $lang = html_escape($this->input->post('lang'));
        $user_data['comedian_id'] = $comedian_id = html_escape($this->input->post('comedian_id'));
        $user_data['name'] = $name = html_escape($this->input->post('name'));
        $user_data['activity'] = $activity = html_escape($this->input->post('activity'));
        $user_data['email'] = $email = html_escape($this->input->post('email'));
        $user_data['phone'] = $phone = html_escape($this->input->post('phone'));
        $user_data['message'] = $message_form = html_escape($this->input->post('message'));
        $user_data['created_at'] = date("Y-m-d H:i:s");

        $comedian_email = trim(content_detail('eng_comedian_email', $comedian_id));

        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";

        if ($lang == 'eng') {
            $subject = "Comedian Booking";
            $msg = $name . "! " . "Thanks for your message, you will be contacted soon.";
            $message_hr = "";
            $message_hr .= "<!DOCTYPE html><html>";
            $message_hr .= "<head><meta http-equiv='Content-Type' content='text/html charset=UTF-8'/></head>";
            $message_hr .= "<body>";
            $message_hr .= "<img src='" . base_url() . "assets/frontend/images/logo.png' height='80' width='80'>";
            $message_hr .= "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            $message_hr .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
            $message_hr .= "<tr><td><strong>Email:</strong> </td><td>" . $email . "</td></tr>";
            $message_hr .= "<tr><td><strong>Phone:</strong> </td><td>" . $phone . "</td></tr>";
            $message_hr .= "<tr><td><strong>Message:</strong> </td><td>" . $message_form . "</td></tr>";
            $message_hr .= "</table>";
            $message_hr .= "</body></html>";
        } else {
            $subject = "حجز الكوميديين";
            $msg = "";
            $msg .= "<!DOCTYPE html><html>";
            $msg .= "<head><meta http-equiv='Content-Type' content='text/html charset=UTF-8'/></head>";
            $msg .= "<body style='direction:rtl;'>";
            $msg .= "<div class='email-text'>";
            $msg .= $name . "! " . "شكراً على رسالتك, سوف يتم الإتصال بك قريباً.";
            $msg .= "</div>";
            $msg .= "</body></html>";

            $message_hr = "";
            $message_hr .= "<!DOCTYPE html><html>";
            $message_hr .= "<head><meta http-equiv='Content-Type' content='text/html charset=UTF-8'/></head>";
            $message_hr .= "<body style='direction:rtl;'>";
            $message_hr .= "<img src='" . base_url() . "assets/frontend/images/logo.png' height='80' width='80'>";
            $message_hr .= "<table rules='all' style='border-color: #666;' cellpadding='10'>";
            $message_hr .= "<tr style='background: #eee;'><td><strong>الإسم الكامل:</strong> </td><td>" . $name . "</td></tr>";
            $message_hr .= "<tr><td><strong>البريد الإلكترونى:</strong> </td><td>" . $email . "</td></tr>";
            $message_hr .= "<tr><td><strong>رقم الجوال:</strong> </td><td>" . $phone . "</td></tr>";
            $message_hr .= "<tr><td><strong>الرسالة:</strong> </td><td>" . $message_form . "</td></tr>";
            $message_hr .= "</table>";
            $message_hr .= "</body></html>";
        }

        $to = $email;
        $this->send_email($subject, $msg, $to);
        $conf = $this->model_configuration->fetchRow();
        $this->send_email_admin($subject, $message_hr, $comedian_email);

        $userRecord = $this->model_contents->saveComedianBooking($user_data);
        if ($userRecord > 0) {
            $resArr['status'] = true;
        } else {
            $resArr['status'] = false;
        }
        echo json_encode($resArr);
        exit();
    }

    public function newsletter_old()//ahmed
    {
        $user_data['lang'] = $lang = html_escape($this->input->post('lang'));
//        $user_data['name'] = $name = html_escape($this->input->post('name'));
        $user_data['email'] = $email = html_escape($this->input->post('email'));
        $user_data['created_at'] = date("Y-m-d H:i:s");

        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";

        $userRecord = $this->model_contents->saveNewsletterSubscriber($user_data);
        if ($userRecord > 0) {

            if ($lang == 'eng') {
                $subject = "Newsletter Subscription";
                $msg = "Thank you! Your subscription has been confirmed. You have been added to our list and will hear from us soon.";
                $message_hr = "";
                $message_hr .= "<!DOCTYPE html><html>";
                $message_hr .= "<head><meta http-equiv='Content-Type' content='text/html charset=UTF-8'/></head>";
                $message_hr .= "<body>";
                $message_hr .= "<table rules='all' style='border-color: #666;' cellpadding='10'>";
                $message_hr .= "<tr><td><strong>Email:</strong> </td><td>" . $email . "</td></tr>";
                $message_hr .= "</table>";
                $message_hr .= "</body></html>";
            } else {
                $subject = "الاشتراك في النشرة الإخبارية";
                $msg = "";
                $msg .= "<!DOCTYPE html><html>";
                $msg .= "<head><meta http-equiv='Content-Type' content='text/html charset=UTF-8'/></head>";
                $msg .= "<body style='direction:rtl;'>";
                $msg .= "<div class='email-text'>";
                $msg .= "شكرا لك! لقد تم تأكيد اشتراكك بنجاح. لقد تم اضافتك الى القائمة لدينا و ستسمع مننا قريبا.";
                $msg .= "</div>";
                $msg .= "</body></html>";

                $message_hr = "";
                $message_hr .= "<!DOCTYPE html><html>";
                $message_hr .= "<head><meta http-equiv='Content-Type' content='text/html charset=UTF-8'/></head>";
                $message_hr .= "<body style='direction:rtl;'>";
                $message_hr .= "<table rules='all' style='border-color: #666;' cellpadding='10'>";
                $message_hr .= "<tr><td><strong>البريد الإلكترونى:</strong> </td><td>" . $email . "</td></tr>";
                $message_hr .= "</table>";
                $message_hr .= "</body></html>";
            }

            $to = $email;

            $this->send_email($subject, $msg, $to);
            $conf = $this->model_configuration->fetchRow();
            $this->send_email_admin($subject, $message_hr, $conf->sendinquiry_email);

            $resArr['status'] = 1;
        } else {
            $resArr['status'] = 0;
        }
        echo json_encode($resArr);
        exit();
    }


    // function for news letter subscribe on mail chimp
    public function newsLetter()
    {

        $lang = html_escape($this->input->post('lang'));

        $config = getConfigurationSetting();
        $email = html_escape($this->input->post('email'));

        $apiKey = $config->mailChimp_api_key;
        $listID = $config->mailChimp_list_id;

        if ($lang == 'eng') {
            $subject = "Newsletter Subscription";
            $newsletter_subscribe_msg = 'Thank you for subscribing to the newsletter';
            $newsletter_subscribe_error_msg = 'An error occurred, please try again.';
            $user_already_exist_msg = 'This e-mail is already registered';
        } else {
            $newsletter_subscribe_msg = 'شكرا لإشتراكك في النشرة الإخبارية';
            $newsletter_subscribe_error_msg = 'حدث خطأ، نرجو المحاولة مرة أخرى.';
            $user_already_exist_msg = 'هذا البريد الالكتروني مسجل مسبقا';
            $subject = ' الاشتراك في النشرة الإخبارية';
        }

        // MailChimp API URL
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey, strpos($apiKey, '-') + 1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

        $check_subscribed = $this->mc_checklist($email, false, $apiKey, $listID, $dataCenter);
        if ($check_subscribed == '404' || $check_subscribed == '' || !$check_subscribed) {
            $json = json_encode([
                'email_address' => $email,
                'status' => 'subscribed',
                'merge_fields' => [
                    'FNAME' => '',
                    'LNAME' => ''
                ]
            ]);
            // send a HTTP POST request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // store the status message based on response code
            if ($httpCode == 200) {
                $post_data['email'] = $email;
                userGeneralEmailNewsletter($post_data, $subject, $newsletter_subscribe_msg, $lang);
                //$this->send_email($subject, $newsletter_subscribe_msg, $email);
                $message = '<p style="color: #00FF00">' . $newsletter_subscribe_msg . '</p>';
                $status = true;
            } else {
                switch ($httpCode) {
                    case 214:
                        $message = '<p style="color: #ff7676">' . $user_already_exist_msg . '</p>';
                        $status = false;
                        break;
                    default:
                        $message = '<p style="color: #ff7676">' . $newsletter_subscribe_error_msg . '</p>';
                        $status = false;
                        break;
                }
            }
        } else {
            $message = '<p style="color: #ff7676">' . $user_already_exist_msg . '</p>';
            $status = false;

        }
        $response['status'] = $status;
        $response['message'] = $message;
        echo json_encode($response);
        exit();
    }


    public function checkCaptcha()
    {
        $response = $this->input->post('g-recaptcha-response');
        $req = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=6Ld5EgUTAAAAAPFS0E5wLMTe3ncrF0SB7nCNidZU&response=' . $response);
        $req = json_decode($req);
        if (!$req->success) {
            echo 0;
        } else {
            echo 1;
        }
    }

    public function preview($page_id)
    {
        $data = array();
        $lang = $this->session->userdata('lang');
        $data['page_id'] = $page_id;
        //$data = array();
        // $page_id =  $this->uri->segment(4);
        $data['form_content'] = $this->input->post();
        //print_r($data['form_content']);exit();
        $data['preview'] = 1;
        $data['page_id'] = $page_id;
        $data['lang'] = $this->session->userdata('lang');
        $contents = $this->model_contents->page_content($page_id);
        $data['contents'] = $contents;
        $data['content'] = $contents[0]->tpl;
        $parent_id = getParentPageId($data['contents'][0]->id);
        if ($parent_id == '' || $parent_id == 0) {
            $parent_id = $contents[0]->id;
        }

        if ($data['contents'][0]->parant_id != 0) {
            $contents_parant = $this->model_contents->page_content($data['contents'][0]->parant_id);
            $parent_id = $contents[0]->parant_id;
            if ($contents_parant[0]->tpl == 'news') {
                $data['content'] = 'news-detail';
            } elseif ($contents_parant[0]->tpl == 'hotels') {
                $data['content'] = 'hotel-detail';
            } elseif ($contents_parant[0]->tpl == 'career') {
                $data['content'] = 'career_detail';
            } elseif ($contents_parant[0]->tpl == 'csr') {
                $data['content'] = 'csr_event_detail';
            } elseif ($contents_parant[0]->tpl == 'industory') {
                $data['content'] = 'industory-detail';
            } elseif ($contents_parant[0]->tpl == 'companies-listing') {
                $data['content'] = 'company-detail';
            } elseif ($contents_parant[0]->tpl == 'projects-listing') {
                $data['content'] = 'project-detail';
            }
        }

        $data['parent_id'] = $parent_id;
        $this->load->view("layouts/default", $data);
    }

    public function detailPreview($page_id)
    {
        //  $page_id =  $this->uri->segment(3);
        if (!$this->session->userdata('lang')) {
            $this->session->set_userdata(array('lang' => 'arb')); /* If no language is selected, set to default ENGLISH */
        }

        $data = array();
        $data['form_content'] = $this->input->post();
        $data['preview'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['result'] = $this->model_contents->fetchRow($page_id);
        foreach ($this->model_contents->front_end_content($data['result']->parant_id) as $val) {
            $data['pagination'][] = $val->id;
        }
        $this->load->view("layouts/header", $data);
        $this->load->view("detail", $data);
        $this->load->view("layouts/footer", $data);
    }


    public function login()
    {

        $lang = $this->session->userdata('lang');

        $result = $this->model_login->login(html_escape($this->input->post('username')), html_escape($this->input->post('password')));

        if ($result) {
            foreach ($result as $row) {
                $user_id = $row->id;
                $user_role = $row->role;
                $this->session->set_userdata(array('user_id' => $user_id, 'user_role' => $user_role));
            }
            if ($lang == 'eng') {
                $result['msg'] = 'Logged in successfully';
            } else {
                $result['msg'] = 'تم تسجيل الدخول بنجاح';
            }

            $result['success'] = true;
            /*redirect($this->config->item('base_url').'page/home');*/
            echo json_encode($result);
            exit();

        } else {
            if ($lang == 'eng') {
                $result['msg'] = 'Email or Password is incorrect';
            } else {
                $result['msg'] = 'البريد الإلكتروني أو كلمة المرور غير صحيحة';
            }

            $result['success'] = false;
            echo json_encode($result);
            exit();

        }

    }

    public function login_employee()
    {
        $lang_session = $this->session->userdata('lang');
        $lang = html_escape($this->input->post('lang'));
        $result = $this->model_login->emp_login(html_escape($this->input->post('email')), html_escape($this->input->post('password')));

        if ($result) {
            foreach ($result as $row) {
                $user_id = $row->id;
                $user_role = $row->role;
                $this->session->set_userdata(array('user_id' => $user_id, 'user_role' => $user_role));
            }
            if ($lang == 'eng') {
                $result['msg'] = 'Logged in successfully';
            } else {
                $result['msg'] = 'تم تسجيل الدخول بنجاح';
            }

            $result['success'] = true;
            /*redirect($this->config->item('base_url').'page/home');*/
            echo json_encode($result);
            exit();

        } else {
            if ($lang == 'eng') {
                $result['msg'] = 'Email or Password is incorrect';
            } else {
                $result['msg'] = 'البريد الإلكتروني أو كلمة المرور غير صحيحة';
            }

            $result['success'] = false;
            echo json_encode($result);
            exit();

        }

    }

    public function logout()
    {

        $this->session->unset_userdata(array('user_id' => ''));
        $this->session->userdata = array();
        $this->session->sess_destroy();
        $this->session->sess_create();
        redirect($this->config->item('base_url'));
    }

    public function check_captcha()
    {
        $code = html_escape($this->input->post('captcha_code'));
        $word = $this->session->userdata('captchaWord');

        if (strcmp(strtoupper($code), strtoupper($word)) == 0) {
            $result['captcha'] = true;
            echo json_encode($result);
            exit();
        } else {
            $result['captcha'] = false;
            echo json_encode($result);
            exit();
        }
    }

    public function generateQr($name)
    {
        $base_path = realpath(BASEPATH . '../');
        $_REQUEST['data'] = $name;

        //set it to writable location, a place for temp generated PNG files
        $PNG_TEMP_DIR = APPPATH . '/libraries/phpqrcode/temp/';

        //html PNG location prefix
        $PNG_WEB_DIR = $base_path . '/uploads/qr_codes/';

        require APPPATH . "/libraries/phpqrcode/qrlib.php";

        $filename = $PNG_WEB_DIR . 'test.png';

        //processing form input
        //remember to sanitize user input in real-life solution !!!
        $errorCorrectionLevel = 'H';
        if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L', 'M', 'Q', 'H')))
            $errorCorrectionLevel = $_REQUEST['level'];

        $matrixPointSize = 4;
        if (isset($_REQUEST['size']))
            $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);

        //it's very important!
        if (trim($_REQUEST['data']) == '')
            die('data cannot be empty! <a href="?">back</a>');

        // user data
        $filename = $PNG_WEB_DIR . 'test' . md5($_REQUEST['data'] . '|' . $errorCorrectionLevel . '|' . $matrixPointSize) . '.png';
        QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);

        //display generated file
        return $image_name = 'uploads/qr_codes/' . basename($filename);

        // benchmark
        //QRtools::timeBenchmark();

    }

    public function generateQrCopy($name)
    {

        $base_path = realpath(BASEPATH . '../');
        $_REQUEST['data'] = $name;

        //set it to writable location, a place for temp generated PNG files
        $PNG_TEMP_DIR = APPPATH . '/libraries/phpqrcode/temp/';

        //html PNG location prefix
        $PNG_WEB_DIR = $base_path . '/uploads/qr_codes/';

        APPPATH . "/libraries/phpqrcode/qrlib.php";

        $filename = $PNG_WEB_DIR . 'test.png';

        //processing form input
        //remember to sanitize user input in real-life solution !!!
        $errorCorrectionLevel = 'H';
        if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L', 'M', 'Q', 'H')))
            $errorCorrectionLevel = $_REQUEST['level'];

        $matrixPointSize = 4;
        if (isset($_REQUEST['size']))
            $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);


        // user data
        $filename = $PNG_WEB_DIR . 'test' . md5($_REQUEST['data'] . '|' . $errorCorrectionLevel . '|' . $matrixPointSize) . '.png';
        QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);

        //display generated file
        return $image_name = 'uploads/qr_codes/' . basename($filename);

        // benchmark
        //QRtools::timeBenchmark();

    }

    public function forgot_password()
    {

        $lang = $this->session->userdata('lang');
        $data['lang'] = $lang;
        $this->load->view("layouts/header", $data);
        $this->load->view("forgot_password", $data);
        $this->load->view("layouts/footer", $data);
    }


    public function logged_in()
    {
        $lang = $this->session->userdata('lang');
        $data['lang'] = $lang;
        if ($this->session->userdata('user')) {
            $this->load->view("layouts/header", $data);
            $this->load->view("login", $data);
            $this->load->view("layouts/footer", $data);
        } else {
            redirect(lang_base_url());
        }
    }


    public function recoverPassword()
    {
        $lang = $this->session->userdata('lang');
        $email = html_escape($this->input->post('email'));
        $check_pass = $this->model_login->check_password($email);
        if ($check_pass) {
            $random_password = generateRandomString();
            $data['password'] = $random_password;
            $result = $this->model_login->new_password($data, $email);
            $mail_to = $this->send_new_email($email, $random_password);
            if ($mail_to) {
                $this->session->set_flashdata('message', ($lang == 'eng' ? 'Your password has been changed and login detail has been sent to your registered email address' : 'لقد تم إنشاء حسابك و معلومات تسجيل الدخول أرسلت إلى بريدك الإلكتروني'));
            }
            redirect(lang_base_url() . 'page/forgot_password');

        } else {
            $this->session->set_flashdata('message', ($lang == 'eng' ? 'Email Dont Exist' : 'البريد الإلكتروني غير موجود'));
            redirect(lang_base_url() . 'page/forgot_password');
        }
    }


    public function send_new_email($email, $random_password)
    {
        $message = '';
        $message .= "Use below detail to login";
        $message .= "<br/><br/><b>User Name &nbsp;:</b>&nbsp;" . $email;
        $message .= "<br/><b>User Password &nbsp;:</b>&nbsp;" . $random_password;
        $body = $message;
        $subject = "Your New Password and Login Details are: ";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From:<FAS Hotels>' . "\r\n";
        if (mail($email, $subject, $body, $headers)) {
            return true;
        }

    }

    public function uploadCv($file = 'cv')
    {
        $this->load->library('ali_upload');
        $config['upload_path'] = './uploads/resume';
        $config['allowed_types'] = 'pdf|doc|docx';
        $this->ali_upload->setConfig($config);
        if ($this->ali_upload->multi_uploadContents($_FILES[$file], $file . '[]')) {
            $files = $this->ali_upload->files;
            return $files[0];
        }
    }

    public function cities()
    {
        $lang = $this->session->userdata('lang');
        $country_code = html_escape($this->input->post('country_code'));
        $cities = $this->job_model->fetchCities($country_code);
        foreach ($cities as $city) {
            $city_name = ($lang == 'eng' ? $city->name : $city->arb_name);
            ?>
            <option value="<?php echo $city->name; ?>"><?php echo $city_name; ?></option>
        <?php }
    }

    public function cities_select()
    {
        $lang = $this->session->userdata('lang');
        $country_code = html_escape($this->input->post('country_code'));
        $city_select = html_escape($this->input->post('city_select'));
        $cities = $this->job_model->fetchCities($country_code);
        foreach ($cities as $city) {
            $city_name = ($lang == 'eng' ? $city->name : $city->arb_name);
            ?>
            <option value="<?php echo $city->name; ?>" <?php if ($city_select == $city->id) {
                echo "selected";
            } ?>><?php echo $city_name; ?></option>

        <?php }
    }

    public function getCity()
    {
        $data = array();
        $result = array();
        $citiesList = '';
        $data['id'] = $country_code = html_escape($this->input->post('id'));
        $language = html_escape($this->input->post('language'));
        $lang = $this->session->userdata('lang');
        $cities = getCities($country_code);
        if ($cities) {
            $citiesList .= '<option value="">' . ($language == 'eng' ? 'Please Select' : 'اختر من فضلك') . '</option>';
            foreach ($cities as $city) {
                $citiesList .= '<option value="' . $city->id . '">' . ($language == 'eng' ? $city->eng_name : $city->arb_name) . '</option>';
            }
        }
        $result['cities'] = $citiesList;
        $result['error'] = 'false';
        //echo '<pre>'; print_r($result);exit();
        echo json_encode($result);
    }

    public function getCities()
    {
        $data = array();
        $result = array();
        $citiesList = '';
        $data['name'] = $country_name = html_escape($this->input->post('name'));
        $lang = $this->session->userdata('lang');
        $cities = getCitiesForCountry($country_name);
        /*echo '<pre>';
        print_r($cities);
        exit();*/
        if (count($cities) > 0) {
            /*$citiesList .= '<option value="" selected>' . ($lang == 'eng' ? 'Please Select' : 'اختر من فضلك') . '</option>';*/
            foreach ($cities as $city) {
                $citiesList .= '<option value="' . ($lang == 'eng' ? $city->eng_city : $city->arb_city) . '">' . ($lang == 'eng' ? $city->eng_city : $city->arb_city) . '</option>';
            }
        } else {
            $citiesList .= '<option value="" selected>' . ($lang == 'eng' ? 'Please Select' : 'اختر من فضلك') . '</option>';
        }
        $result['cities'] = $citiesList;
        $result['error'] = 'false';
        echo json_encode($result);
    }


    public function submit_registration()
    {
        $lang = 'arb';
        $conf = $this->model_configuration->fetchRow();

        $data = array();
        $post_data = $this->input->post();
        $category_data = $this->model_contents->page_content($post_data['cat_id']);
        $data['category'] = $category_data[0]->arb_title;
        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'submit' && $key != 'terms' && $key != 'video' && $key != 'image' && $key != 'audio' && $key != 'radio') {
                $data[$key] = $value;
            }
        }
        if ($data['terms_condition'] == 'on') {
            $data['terms_condition'] == 1;
        }
        if ($data['terms_condition'] == 'off') {
            $data['terms_condition'] == 0;
        }
        $hidden_file_names = trim($data['hidden_file_names'], ",");
        $hidden_file_names_exploded = explode(',', $hidden_file_names);

        if ($_FILES['user_image']['name'] != '') {
            $img_name = $_FILES['user_image']['name'];
            $img_name = explode('.', $img_name);
            $img = $img_name[0] . '-' . time() . '.' . $img_name[1];
            $type = $_FILES['user_image']['type'];
            $img_temp = $_FILES['user_image']['tmp_name'];
            $error = $_FILES['user_image']['error'];
            if ($type == "image/png" || $type == "image/jpeg" || $type == "image/jpg") {
                move_uploaded_file($img_temp, "uploads/registration_form/$img");
            }
            $data['user_image'] = $img;
        }

        if (empty($hidden_file_names_exploded)) {
            if ($_FILES['user_files'] != '') {
                $i = 0;
                foreach ($_FILES['user_files'] as $user_file_name) {
                    if ($_FILES['user_files']['name'][$i] != '') {
                        $img_name = $_FILES['user_files']['name'][$i];
                        $img_name = explode('.', $img_name);
                        $img = $img_name[0] . '-' . time() . '.' . $img_name[1];
                        $img_temp = $_FILES['user_files']['tmp_name'][$i];
                        move_uploaded_file($img_temp, "uploads/user_files/$img");
                        $data['user_files'] .= $img . '|||';
                        $i++;
                    }
                }
                $data['user_files'] = trim($data['user_files'], '|||');
            }
        } else {

            if ($_FILES['user_files'] != '') {
                $i = 0;
                foreach ($_FILES['user_files'] as $user_file_name) {
                    if ($_FILES['user_files']['name'][$i] != '') {
                        if (in_array($_FILES['user_files']['name'][$i], $hidden_file_names_exploded)) {

                        } else {
                            $img_name = $_FILES['user_files']['name'][$i];
                            $img_name = explode('.', $img_name);
                            $img = $img_name[0] . '-' . time() . '.' . $img_name[1];
                            $img_temp = $_FILES['user_files']['tmp_name'][$i];
                            move_uploaded_file($img_temp, "uploads/user_files/$img");
                            $data['user_files'] .= $img . '|||';
                        }
                        $i++;
                    }
                }
                $data['user_files'] = trim($data['user_files'], '|||');
            }
        }

        unset($data['hidden_file_names']);
        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";


        if ($lang == 'eng') {
            $subject = 'Your contact us email has been received!';
            $title = 'Your contact form submitted';

            $subject2 = 'New Contact us email';
            $title2 = 'New Contact us email';
        } else {
            $subject = 'تم استلام نموذج التواصل الخاص بك';
            $title = 'شكرا لك، تم ارسال النموذج بنجاح';

            $subject2 = 'اتصل بنا عبر البريد الإلكتروني';
            $title2 = 'اتصل بنا عبر البريد الإلكتروني';
        }

        userGeneralEmail($data, $subject, $title, $lang);
        adminGeneralEmail($data, $subject2, $title2, $lang);
        send_sms($data['phone_no'], 'Thank you For Registration in She"s Mercedes.');
        unset($data['category']);
        $response = $userRecord = $this->model_contents->saveRegistrations($data);

        $resArr['success'] = 1;
        $resArr['status'] = 1;
        $resArr['message'] = "تم استلام النموذج الخاص بك، سنقوم بمراجعته و اعلامك بحالة طلبك";
        echo json_encode($resArr);
        exit();
    }

    public function update_profile()
    {
        $lang = 'arb';
        $id = $this->uri->segment(3);
        $conf = $this->model_configuration->fetchRow();
        $this->load->model('ems/model_inquries');

        $user_info = $this->model_inquries->getUserRecord($id);
        $data = array();
        $post_data = $this->input->post();
        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'submit' && $key != 'terms' && $key != 'video' && $key != 'image' && $key != 'audio' && $key != 'radio') {
                $data[$key] = $value;
            }
        }
        $hidden_file_names = trim($data['hidden_file_names'], ",");
        $hidden_file_names_exploded = explode(',', $hidden_file_names);

        if (empty($hidden_file_names_exploded)) {
            if ($_FILES['other_materials'] != '') {
                $i = 0;
                foreach ($_FILES['other_materials'] as $user_file_name) {
                    if ($_FILES['other_materials']['name'][$i] != '') {
                        $img_name = $_FILES['other_materials']['name'][$i];
                        $img_name = explode('.', $img_name);
                        $img = $img_name[0] . '-' . time() . '.' . $img_name[1];
                        $img_temp = $_FILES['other_materials']['tmp_name'][$i];
                        move_uploaded_file($img_temp, "uploads/other_materials/$img");
                        $data['other_materials'] .= $img . '|||';
                        $i++;
                    }
                }
                $data['other_materials'] = trim($data['other_materials'], '|||');
            }
        } else {

            if ($_FILES['other_materials'] != '') {
                $i = 0;
                foreach ($_FILES['other_materials'] as $user_file_name) {
                    if ($_FILES['other_materials']['name'][$i] != '') {
                        if (in_array($_FILES['other_materials']['name'][$i], $hidden_file_names_exploded)) {

                        } else {
                            $img_name = $_FILES['other_materials']['name'][$i];
                            $img_name = explode('.', $img_name);
                            $img = $img_name[0] . '-' . time() . '.' . $img_name[1];
                            $img_temp = $_FILES['other_materials']['tmp_name'][$i];
                            move_uploaded_file($img_temp, "uploads/other_materials/$img");
                            $data['other_materials'] .= $img . '|||';
                        }
                        $i++;
                    }
                }

                $other_materials_exploded = explode('|||', $user_info->other_materials);

                /* $j = 1;
				foreach($other_materials_exploded as $other_materials){
					if($other_materials != $name){
						$newMaterialFiles .= $other_materials.'|||';
					}
					$data['other_materials'] .= $newMaterialFiles;
				}
				$j++; */
                $data['other_materials'] .= '|||' . $user_info->other_materials;
                $data['other_materials'] = trim($data['other_materials'], '|||');
            }
        }
        if ($_FILES['user_image']['name'] != '') {
            $img_name = $_FILES['user_image']['name'];
            $img_name = explode('.', $img_name);
            $img = $img_name[0] . '-' . time() . '.' . $img_name[1];
            $type = $_FILES['user_image']['type'];
            $img_temp = $_FILES['user_image']['tmp_name'];
            $error = $_FILES['user_image']['error'];
            if ($type == "image/png" || $type == "image/jpeg" || $type == "image/jpg") {
                move_uploaded_file($img_temp, "uploads/registration_form/$img");
            }
            $data['user_image'] = $img;
        }
        unset($data['hidden_file_names']);
        $data['is_profile_createad'] = 1;
        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";


        /* if ($lang == 'eng'){
			$subject = 'Your contact us email has been received!';
			$title 	 = 'Your contact form submitted';

			$subject2 = 'New Contact us email';
			$title2 	 = 'New Contact us email';
		}else{
			$subject = 'تم استلام نموذج التواصل الخاص بك';
			$title   = 'تم ارسال نموذج بنجاح';

			$subject2 = 'اتصل بنا عبر البريد الإلكتروني';
			$title2   = 'اتصل بنا عبر البريد الإلكتروني';
		} */

        //userGeneralEmail($data,$subject,$title,$lang);
        //adminGeneralEmail($data,$subject2,$title2,$lang);
        $response = $userRecord = $this->model_contents->updateRegistraionProfile($data, $id);

        $resArr['success'] = 1;
        $resArr['status'] = 1;
        $resArr['message'] = "تم استلام النموذج الخاص بك، سنقوم بمراجعته و اعلامك بحالة طلبك";
        echo json_encode($resArr);
        exit();
    }


    public function submit_contact()
    {
        $lang = html_escape($this->input->post('lang'));
        $conf = $this->model_configuration->fetchRow();
        $data = array();

        $siteKey = $this->input->post('g-recaptcha-response');
        $res = $this->VerifyRecaptcha($siteKey);

        $resArr = array();
        if ($res == false) {
            if ($lang == 'eng') {
                $message = 'Please use the captcha!';
            } else {
                $message = 'نرجو التأكد من تعبئة خانة التحقق.';
            }
            //$resArr['status'] = false;
            $resArr['error_captcha'] = $message;
            $resArr['status'] = 2;
            echo json_encode($resArr);
            exit();
        }
        $post_data = $this->input->post();
        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'submit' && $key != 'g-recaptcha-response') {
                $data[$key] = removeJsScripts($value);
            }
        }
        if ($lang == 'eng') {

            $subject2 = 'New Contact us email';
            $title2 = 'New Contact us email';
        } else {

            $subject2 = 'اتصل بنا عبر البريد الإلكتروني';
            $title2 = 'اتصل بنا عبر البريد الإلكتروني';
        }
        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";

        adminGeneralEmail($data, $subject2, $title2, $lang,'contact');
        userGeneralEmail($data, $subject2, $title2, $lang);
        $userRecord = $this->model_contents->saveContactUs($data);

        if ($userRecord > 0) {
            if ($lang == 'eng') {
                $message['status'] = 1;
                $message['message'] = "Form Submit Successful, you will be contact soon";
            } else {
                $message['status'] = 1;
                $message['message'] = "شكرا، تم ارسال النموذج بنجاح";
            }
        } else {
            if ($lang == 'eng') {
                $message['status'] = 0;
                $message['message'] = "Form Not Submit, Please try again";
            } else {
                $message['status'] = 0;
                $message['message'] = 'النموذج لم يتم الإرسال ، يرجى المحاولة مرة أخرى';
            }
        }
        echo json_encode($message);
        exit();

    }

    public function submit_voting_form()
    {
        $this->load->model('ems/model_registrations');
        $lang = 'arb';
        $conf = $this->model_configuration->fetchRow();
        $data = array();
        $post_data = $this->input->post();
        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'submit') {
                $data[$key] = $value;
            }
        }

        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";

        $checkVotingOnProfile = $this->model_contents->checkVotingOnProfile($data['email'], $data['profile_id']);

        $profileInformation = $this->model_registrations->fetchRow($data['profile_id']);

        if ($profileInformation->email == $data['email']) {
            $message['status'] = 0;
            $message['message'] = '<span id="voting_message" style="color:red;  font-size:16px;">نعتذر، لا يمكنك التصويت باستخدام نفس البريد الإلكتروني للمتشاركات</span>';
        } else if ($checkVotingOnProfile) {
            $message['status'] = 0;
            $message['message'] = '<span id="voting_message" style="color:red;  font-size:16px;">نعتذر، لا يمكنك التصويت بسبب قيامك بالتصويت مسبقا</span>';
        } else {
            $userRecord = $this->model_contents->saveProfileVoting($data);
            if ($userRecord > 0) {
                if ($lang == 'eng') {
                    $message['status'] = 1;
                    $message['message'] = "Form Submit Successful, you will be contact soon";
                } else {
                    $message['status'] = 1;
                    $message['message'] = '<span id="voting_message" style="color:#36b8d3; font-size:16px;">	
					تم ارسال التصويت الخاص بك، شكرا على الدعم 
					</span>';
                }
            } else {
                if ($lang == 'eng') {
                    $message['status'] = 0;
                    $message['message'] = "Form Not Submit, Please try again";
                } else {
                    $message['status'] = 0;
                    $message['message'] = '<span id="voting_message" style="color:red; font-size:16px;">النموذج لم يتم الإرسال ، يرجى المحاولة مرة أخرى</span>';
                }
            }
        }

        echo json_encode($message);
        exit();

    }


    public function submit_career()
    {
        $this->load->library('email');
        $lang = html_escape($this->input->post('lang'));
        $conf = $this->model_configuration->fetchRow();
        $data = array();
        $post_data = $this->input->post();
        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'submit') {
                $data[$key] = $value;
            }
        }

        $file_name_eng = date('Ymdhsi') . '-' . $_FILES['c_v']['name'];
        $file_size1 = $_FILES['c_v']['size'];
        $file_tmp_eng = $_FILES['c_v']['tmp_name'];
        $type1 = $_FILES['c_v']['type'];
        move_uploaded_file($file_tmp_eng, "uploads/cvs/" . $file_name_eng);
        $data['c_v'] = $file_name_eng;

        $resArr = array();
        $resArr['error'] = "";
        $resArr['status'] = false;
        $resArr['success'] = "";

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $conf->smtp_host, 'smtp_port' => $conf->smtp_port,
            'smtp_user' => $conf->smtp_email, 'smtp_pass' => $conf->smtp_password);
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('info@stom.ed.sa');
        $this->email->to($data['email']);
        //$this->email->to($conf->sendinquiry_email);
        $this->email->subject('Stom');
        if ($lang == 'eng') {
            $this->email->message('Hello, you have applied successfully');
        } else {
            $this->email->message('مرحبًا ، لقد قمت بالتقديم بنجاح');
        }
        $this->email->send();
        $userRecord = $this->model_contents->saveCareer($data);
        if ($userRecord > 0) {
            if ($lang == 'eng') {
                $message['status'] = 1;
                $message['message'] = "Form Submit Successful";
            } else {
                $message['status'] = 1;
                $message['message'] = "تم التحميل بنجاح";
            }
        } else {
            if ($lang == 'eng') {
                $message['status'] = 1;
                $message['message'] = "Form Not Submit, Please try again";
            } else {
                $message['status'] = 1;
                $message['message'] = 'النموذج لم يتم الإرسال ، يرجى المحاولة مرة أخرى';
            }
        }
        if ($lang == 'eng') {
            redirect($this->config->item('base_url') . 'en/page/Careers?message_career=done');
        } else {
            redirect($this->config->item('base_url') . 'page/Careers?message_career=done');
        }

    }

    public function send_email_to_friend($subject, $message, $from, $to)
    {
        $conf = $this->model_configuration->fetchRow();

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: She"s Mercedes <' . $from . '>' . "\r\n";
        mail($to, $subject, $message, $headers);
    }

    public function send_email($subject, $message, $to)
    {
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Cultural Activities <info@gcaactivities.com>' . "\r\n";
        mail($to, $subject, $message, $headers);

        /*  $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://gca.nred.co',
            'smtp_port' => 465,
            'smtp_user' => 'info@gcaactivities.com',
            'smtp_pass' => '2j)CLt!sH%jh',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('Cultural Activities <info@gcaactivities.com>');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } */
    }

    public function send_email_admin($subject, $message, $to)
    {
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Cultural Activities <info@gcaactivities.com>' . "\r\n";
        mail($to, $subject, $message, $headers);
    }

    public function share_with_friend_email($subject, $message, $to, $from)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://gca.nred.co',
            'smtp_port' => 465,
            'smtp_user' => 'info@gcaactivities.com',
            'smtp_pass' => '2j)CLt!sH%jh',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        }
    }

    public function mc_checklist($email, $debug, $apikey, $listid, $server)
    {
        $userid = md5($email);
        $auth = base64_encode('user:' . $apikey);
        $data = array(
            'apikey' => $apikey,
            'email_address' => $email
        );
        $json_data = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://' . $server . '.api.mailchimp.com/3.0/lists/' . $listid . '/members/' . $userid);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic ' . $auth));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        $result = curl_exec($ch);
        if ($debug) {
            var_dump($result);
        }
        $json = json_decode($result);
        return $json->{'status'};
    }
    function getFrontendCities()
    {
        $lang = $this->session->userdata('lang');
        $CI = &get_Instance();

        $CI->load->model('model_custom');
        $country = html_escape($this->input->post('country'));

        /*$country_explode = explode('|', $country);
        $country_code = $country_explode[1];
        $country_id = $country_explode[0];*/

        $cities = $CI->model_custom->getCities($country);

        //return $cities;
        //Munim Changes
        $selected = "";
        $html_cities = '';
        foreach ($cities as $city) {
            /* if($country_code == $city->countrycode){
                $selected = 'selected';
            }
            else{
                $selected = '';
            } */
            $html_cities .= '<option value="' . ($lang == 'eng' ? $city->eng_name : $city->arb_name) . '" ' . $selected . '>' . ($lang == 'eng' ? $city->eng_name : $city->arb_name) . '</option>';
        }
        $data['status'] = 1;
        $data['html'] = $html_cities;

        echo json_encode($data);
        exit;
    }

}