<?php

class Logout extends CI_Controller {

    function index()
    {
       $this->session->unset_userdata('user_id');
       $this->session->unset_userdata('user_role');
        //redirect(lang_base_url());
       /* redirect('index.php');*/
	   
		$resArr['status'] = true;
		$resArr['msg'] = ($lang == 'eng' ? 'Logout Successful' : 'إعادة النجاح ناجحة');
		echo json_encode($resArr);
        exit();
    }
}