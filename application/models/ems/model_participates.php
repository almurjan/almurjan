<?php

class Model_participates extends CI_Model {

	function _construct() {
		// Call the Model constructor
		parent::_construct();
	}

	function save($data) {
		$this->db->set($data);
		$this->db->insert('submit_participate');
		$insertId=$this->db->insert_id();
		if($insertId>0)
		{
			return $insertId;
		}
		else{
			return false;
		}
			
	}

	public function fetchAll() {

		$this->db->select('*');
		$this->db->from('submit_participate');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}
			
	}
	public function fetchAllBookings() {

		$this->db->select('*');
		$this->db->from('submit_participate');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}

	}

    public function fetchRow($id){

        $this->db->select('*');
        $this->db->from('submit_participate');
        $this->db->where('id',$id);
        $query = $this->db->get();
        if ($query->num_rows()==1) {
            $result = $query->row();
        } else {
            $result = false;
        }
            return $result;
    }
    public function fetch($id){

        $this->db->select('*');
        $this->db->from('submit_participate');
        $this->db->where('id',$id);
        $query = $this->db->get();
        if ($query->num_rows()==1) {
            $result = $query->row();
        } else {
            $result = false;
        }
            return $result;
    }

	public function delete($id){
			
		$query=$this->db->query("delete from submit_participate WHERE id =".$id);
		if ($query) {
			return true;
		} else {
			return false;
		}

	}

}//end