<?php

class Model_inquries extends CI_Model {

	function _construct() {
		// Call the Model constructor
		parent::_construct();
	}

	function save($data) {
		$this->db->set($data);
		$this->db->insert('contacts');
		$insertId=$this->db->insert_id();
		if($insertId>0)
		{
			return $insertId;
		}
		else{
			return false;
		}	
	}
	
	function saveSubAdminRatings($data) {
		$this->db->set($data);
		$this->db->insert('reg_application_ratings');
		$insertId=$this->db->insert_id();
		if($insertId>0)
		{
			return $insertId;
		}
		else{
			return false;
		}	
	}
	
	function saveSubAdminRatingsProfile($data) {
		$this->db->set($data);
		$this->db->insert('reg_profile_ratings');
		$insertId=$this->db->insert_id();
		if($insertId>0)
		{
			return $insertId;
		}
		else{
			return false;
		}	
	}
	
	function updateRegionCity($data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update('cms_regions_city', $data);
        return ($query>0)?true:false;
    }function updateRegion($data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update('cms_regions', $data);
        return ($query>0)?true:false;
    }
	function update_user_rating($table ,$data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update($table, $data);
        return ($query>0)?true:false;
    }	
	function update_registration_admin_count($table ,$data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update($table, $data);
        return ($query>0)?true:false;
    }
	
	public function publishStatus($val, $id) {
        $query = $this->db->query("update registrations set pub_status='" . $val . "' WHERE id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	public function makeWinner($val, $id) {
        $query = $this->db->query("update registrations set is_winner='" . $val . "' WHERE id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	public function updateUserMaterials($id,$newMaterialFiles){
        $query = $this->db->query("update registrations set other_materials ='" . $newMaterialFiles . "' WHERE id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	public function fetchAllGroup() {

		$this->db->select('reservation_info.*,contents.eng_title as eng_title,contents.arb_title as arb_title');
		$this->db->from('reservation_info');		
		$this->db->join('users', 'reservation_info.user_id = users.id','left');		
		$this->db->join('user_events', 'user_events.id = reservation_info.user_event_id');		
		$this->db->join('contents', 'contents.id = user_events.event_id');
		$this->db->group_by('reservation_info.email');
		$this->db->order_by('reservation_info.id','DESC');
		
		$query = $this->db->get();
		
		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}
			
	}
	public function fetchAll($date) {
		if($date != ''){
			$date = date('Y-m-d',strtotime($date));
			
			$query=$this->db->query("SELECT * FROM `contacts` WHERE created_at LIKE '%{$date}%' order by id desc");
		}
		else{
			$query=$this->db->query("SELECT * FROM `contacts` order by id desc");	
		}	
		
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}			
	}
	public function fetchAllSections() {

		$query=$this->db->query("SELECT * FROM `cms_regions` order by name asc");
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}			
	}
	public function getAllRatingsOfProfile($id) {

		$query=$this->db->query("SELECT * FROM `reg_application_ratings` where reg_id = ".$id);
		if ($query) {
			return $query->result();
		} else {
			return array();
		}			
	}
	public function getAllRatingsOfProfiles($id) {

		$query=$this->db->query("SELECT * FROM `reg_profile_ratings` where reg_id = ".$id);
		if ($query) {
			return $query->result();
		} else {
			return array();
		}			
	}
	public function getUserRecord($id) {

		$query=$this->db->query("SELECT * FROM `registrations` where id = ".$id);
		if ($query) {
			return $query->row();
		} else {
			return array();
		}			
	}
	public function fetchAllCareers($date_request, $job_id, $education, $gender) {
		
		$concat_query = '';
		$date = $date_request;
		if($date != ''){
			
			$date = date('Y-m-d',strtotime($date_request));
			$concat_query .= " AND created_at LIKE '%{$date}%' ";
		}
		if($job_id != ''){
			$concat_query .= " AND job_id = '{$job_id}' ";
		}
		
		if($education != ''){
			if($education == 'High School' || $education == 'المدرسة الثنوية'){
				$concat_query .= " AND (education = 'High School' || education = 'المدرسة الثنوية') ";
			}
			else if($education == 'Diploma' || $education == 'دبلوم'){
				$concat_query .= " AND (education = 'Diploma' || education = 'دبلوم') ";
			}
			else if($education == 'Bachelor' || $education == 'بكالوريوس'){
				$concat_query .= " AND (education = 'Bachelor' || education = 'بكالوريوس') ";
			}
			else if($education == 'Master' || $education == 'ماجستير'){
				$concat_query .= " AND (education = 'Master' || education = 'ماجستير') ";
			}
			
		}
		
		if($gender != ''){
			if($gender == 'male'){
				$concat_query .= " AND (gender = 'male' || gender = 'ذكر') ";
			}
			if($gender == 'female'){
				$concat_query .= " AND (gender = 'female' || gender = 'انثى') ";
			}
		}
		
		if($job_id != '' || $education != '' || $gender != '' || $date != ''){
			$where = ' Where ';
			$concat_query = trim($concat_query," AND");
			
			//$concat_query = str_replace('TE', 'DATE',$concat_query);
			//$concat_query = str_replace('DADATE', 'DATE',$concat_query);
			$where .= $concat_query; 
			
		}
		
		$query=$this->db->query("SELECT * FROM `careers` " .$where . " order by id desc");
		
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
			
	}
    public function fetchAllSelfApplication($date_request) {

        $concat_query = '';
        $date = $date_request;
        if($date != ''){

            $date = date('Y-m-d',strtotime($date_request));
            $concat_query .= " AND created_at LIKE '%{$date}%' ";
        }


        /*if($job_id != '' || $education != '' || $gender != '' || $date != ''){
            $where = ' Where ';
            $concat_query = trim($concat_query," AND");

            //$concat_query = str_replace('TE', 'DATE',$concat_query);
            //$concat_query = str_replace('DADATE', 'DATE',$concat_query);
            $where .= $concat_query;

        }*/

       /* $query=$this->db->query("SELECT * FROM `careers` " .$where . " order by id desc");*/
        $query=$this->db->query("SELECT * FROM `self_application` order by id desc");

        if ($query) {
            return $query->result_array();
        } else {
            return false;
        }

    }
	public function fetchAllUsersExcel() {

		$query=$this->db->query("SELECT * FROM `users` WHERE id NOT IN (SELECT user_id FROM `reservation_info`)");
		if ($query) {
			return $query->result();
		} else {
			return false;
		}
			
	}
	public function fetchAllQualifiedApplications($percentage) {

		$this->db->select('*');
		$this->db->from('registrations');
		$this->db->where('status',0);
		$this->db->where('is_profile_createad',0);
		$this->db->where('average_ratings >=',$percentage);
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		if ($query->num_rows() >0) {
			return $query->result();
		} else {
			return false;
		}

	}
	public function fetchAllQualifiedProfiles($percentage) {

		$this->db->select('*');
		$this->db->from('registrations');
		$this->db->where('pub_status',0);
		$this->db->where('is_profile_createad',1);
		$this->db->where('average_ratings_profile >=',$percentage);
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		if ($query->num_rows() >0) {
			return $query->result();
		} else {
			return false;
		}

	}
	public function get_user_info($id)
    {
		$query=$this->db->query("SELECT reg_application_ratings.ratings as ratings, registrations.* , contents.eng_title as cat_eng_name, contents.arb_title as cat_arb_name FROM `registrations` JOIN contents on registrations.cat_id = contents.id  LEFT JOIN reg_application_ratings on reg_application_ratings.reg_id = registrations.id WHERE registrations.id = " . $id);
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	
	public function get_user_info_sub_admin($id)
    {
		$loggedInUserId = $this->session->userdata('id');
		
		$query=$this->db->query("SELECT reg_application_ratings.ratings as ratings, registrations.* , contents.eng_title as cat_eng_name, contents.arb_title as cat_arb_name FROM `registrations` LEFT JOIN contents on registrations.cat_id = contents.id LEFT JOIN reg_application_ratings on reg_application_ratings.reg_id = registrations.id AND reg_application_ratings.sub_admin_id = ".$loggedInUserId." WHERE registrations.id = ".$id."");
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	
	public function checkSubAdminRatingOnProfile($registration_id,$sub_admin_id)
    {
		$query=$this->db->query("SELECT * FROM reg_application_ratings WHERE reg_id = ".$registration_id." AND sub_admin_id = ".$sub_admin_id."");
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	
	public function checkSubAdminRatingOnProfiles($registration_id,$sub_admin_id)
    {
		$query=$this->db->query("SELECT * FROM reg_profile_ratings WHERE reg_id = ".$registration_id." AND sub_admin_id = ".$sub_admin_id."");
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	
	public function get_judge_info($id)
    {
		$query=$this->db->query("SELECT * FROM contents WHERE id = " . $id);
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	
	public function get_profile_info($id)
    {
		$query=$this->db->query("SELECT * FROM registrations WHERE id = " . $id);
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	public function get_category_winner_count($cat_id,$id)
    {
		$query=$this->db->query("SELECT count(*) as winner_count FROM registrations WHERE cat_id = ".$cat_id." AND `is_profile_createad` = 1 AND `is_winner` = 1 AND id != " . $id);
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	public function getCategoryProfiles($cat_id)
    {
		$query=$this->db->query("SELECT registrations.* , contents.arb_title as cat_name FROM registrations LEFT Join contents on registrations.cat_id = contents.id WHERE registrations.cat_id = ".$cat_id." AND registrations.is_profile_createad = 1 AND registrations.is_winner = 1 LIMIT 2");
		if ($query) {
			return $query->result();
		} else {
			return false;
		}
    }
	public function emailToRejectedProfiles()
    {
		$query=$this->db->query("SELECT registrations.* , contents.arb_title as cat_name FROM registrations LEFT Join contents on registrations.cat_id = contents.id WHERE registrations.is_profile_createad = 1 AND registrations.is_winner != 1");
		if ($query) {
			return $query->result();
		} else {
			return false;
		}
    }
	
    public function fetchRowQuote($id){
		$this->db->select('*');
        $this->db->from('request_quotation');		
        $this->db->where('id',$id);
        $query = $this->db->get();
        if ($query->num_rows()==1) {
            $result = $query->row();
        } else {
            $result = false;
        }
            return $result;
    }
    public function fetchRow($id){
		$this->db->select('reservation_info.*,contents.eng_title as eng_title,contents.arb_title as arb_title');
        $this->db->from('reservation_info');		$this->db->join('user_events', 'user_events.id = reservation_info.user_event_id');		$this->db->join('contents', 'contents.id = user_events.event_id');
        $this->db->where('reservation_info.id',$id);
        $query = $this->db->get();
        if ($query->num_rows()==1) {
            $result = $query->row();
        } else {
            $result = false;
        }
            return $result;
    }
    public function fetch($id){

        $this->db->select('*');
        $this->db->from('contacts');
        $this->db->where('id',$id);
        $query = $this->db->get();
        if ($query->num_rows()==1) {
            $result = $query->row();
        } else {
            $result = false;
        }
            return $result;
    }
    public function fetchRegion($id){

        $this->db->select('*');
        $this->db->from('cms_regions');
        $this->db->where('id',$id);
        $query = $this->db->get();
        if ($query->num_rows()==1) {
            $result = $query->row();
        } else {
            $result = false;
        }
            return $result;
    }    
	
	public function fetchRegionCity($id){

        $this->db->select('*');
        $this->db->from('cms_regions_city');
        $this->db->where('id',$id);
        $query = $this->db->get();
        if ($query->num_rows()==1) {
            $result = $query->row();
        } else {
            $result = false;
        }
            return $result;
    }
	public function fetchCountryCode($code){

        $this->db->select('*');
        $this->db->from('cms_regions');
        $this->db->where('code',$code);
        $query = $this->db->get();
        if ($query->num_rows()==1) {
            $result = $query->row();
        } else {
            $result = false;
        }
            return $result;
    }
	
	public function fetchAllRegionsCities($id){
		$this->db->select('*');
        $this->db->from('cms_regions_city');
		if($id != ''){
			$this->db->where('region_id',$id);
		}
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
        } else {
            $result = false;
        }
            return $result;
    }
	
	function saveRegions($data) {
        $this->db->set($data);
        $this->db->insert('cms_regions');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
	function saveRegionsCity($data) {
        $this->db->set($data);
        $this->db->insert('cms_regions_city');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
	public function delete($id){
			
		$query=$this->db->query("delete from contacts WHERE id =".$id);
		if ($query) {
			return true;
		} else {
			return false;
		}

	}
	public function deleteCareers($id){
			
		$query=$this->db->query("delete from careers WHERE id =".$id);
		if ($query) {
			return true;
		} else {
			return false;
		}

	}
    public function deleteself($id){

        $query=$this->db->query("delete from self_application WHERE id =".$id);
        if ($query) {
            return true;
        } else {
            return false;
        }

    }
	
	public function deleteRequest($id){
			
		$query=$this->db->query("delete from request_quotation WHERE id =".$id);
		if ($query) {
			return true;
		} else {
			return false;
		}

	}
	public function deleteContact($id){
			
		$query=$this->db->query("delete from contacts WHERE id =".$id);
		if ($query) {
			return true;
		} else {
			return false;
		}

	}
	public function deleteRegion($id){
			
		$query=$this->db->query("delete from cms_regions WHERE id =".$id);
		if ($query) {
			return true;
		} else {
			return false;
		}

	}
	public function deleteRegionCity($id){
			
		$query=$this->db->query("delete from cms_regions_city WHERE id =".$id);
		if ($query) {
			return true;
		} else {
			return false;
		}

	}
	public function acceptApplication($val, $id) {
        $loggedInUserId = $this->session->userdata('id');
		
        $query = $this->db->query("update registrations set status='" . $val . "',content_updated_at='" . date('Y-m-d H:i:s').rand(5) . "',content_updated_by='" . $loggedInUserId . "' WHERE id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	public function acceptProfile($val, $id) {
        $loggedInUserId = $this->session->userdata('id');
		
        $query = $this->db->query("update registrations set pub_status='" . $val . "',content_updated_at='" . date('Y-m-d H:i:s').rand(5) . "',content_updated_by='" . $loggedInUserId . "' WHERE id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

}//end