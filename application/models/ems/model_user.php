<?php

class Model_user extends CI_Model {
    
    public function fetch($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
	   public function fetchAllCountries() {

        $this->db->select('*');
        $this->db->from('countries');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function fetchAll($tb) {
		
		
        $query = $this->db->query("select * from ".$tb." ORDER By `id` DESC");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    /*Project Quotations*/
    public function fetchAllProjects($tb) {
        $query = $this->db->query("select * from ".$tb." ORDER By `id` DESC");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetchProject($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    /*Project Quotations*/

    /*Business Partners*/
    public function fetchAllRequests($tb) {
        $query = $this->db->query("select * from ".$tb." ORDER By `id` DESC");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetchRequest($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    /*Business Partners*/
    
    public function get_careers($title) {

        $this->db->select('*');
        $this->db->from('users');

        $this->db->where("(nationality='".$title."' OR gender='".$title."' OR applied_for='".$title."')");

        $query = $this->db->get();

        if (!empty($query)) {
            return $query->result();
        } else {
            return false;
        }

    }

	public function fetchAllUsersId() {

        $query = $this->db->query("select id from users");

        $result = $query->result();

        if(!empty($result))

        {

            return $result;

        }else{

            return false;

        }

    }
	public function fetchUser($id) {

        $query = $this->db->query("select * from users where id=".$id);

        $result = $query->row();

        if(!empty($result))

        {

            return $result;

        }else{

            return false;

        }

    }

   public function check_email($email) {

       $query = $this->db->query("select * from users where `email` = '".$email."'");
       $result = $query->row();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }

    }
    public function fetchAllUsers($tb) {
		
		$this->db->select('*');
        $this->db->from($tb);
		$this->db->order_by("id", "desc"); 
        $query = $this->db->get();
        //$result = $query->result();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function fetchAllEventRegistrations($tb) {

        $this->db->select('*');
        $this->db->from($tb);
        $this->db->where("type","event");
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        //$result = $query->result();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function fetchEventRegistration($id) {

        $query = $this->db->query("select * from users where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetchAllRegistrations($tb) {

        $this->db->select('*');
        $this->db->from($tb);
        $this->db->where("type","register");
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        //$result = $query->result();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function fetchRegistration($id) {

        $query = $this->db->query("select * from users where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetchAdress($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where loc_id=".$id);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
	
	
	  public function fetch_dep_detail($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where dep_id=".$id);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }

     public function fetch_number($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where id=".$id);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
	
	
    public function saveUser($data)
    {
        $this->db->insert('users',$data);
        return $this->db->insert_id();
    }
	
    
    public function save($tb,$data,$where = NULL)
    {
        if(empty($where)){
		$this->db->insert($tb,$data);
        return $this->db->insert_id();
		}else{
		$this->db->update($tb,$data,array('id'=>$where));
        return $where;
		}
    }
	
		public function updateUser($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('users', $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->affected_rows();
		} else {
			return false;
		}
	}
	
		function FetchUserPassword($id){
		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {

			return false;
		}
	}
	
    
    public function publishStatus($status,$id)
    {
        $this->db->query("update contact set pub_status=".$status." where id=".$id);
    }
    
    public function delete($id,$tb)
    {
        $this->db->query("delete from ".$tb." where id=".$id);
    }
	public function deleteUser($id)
    {
        $this->db->query("delete from users where id=".$id);
    }
	
}

//end