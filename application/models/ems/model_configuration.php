<?php

class Model_configuration extends CI_Model {

	function _construct() {
		// Call the Model constructor
		parent::_construct();
	}

	function save($data) {
		$this->db->set($data);
		$this->db->insert('configuration');
		$insertId=$this->db->insert_id();
		if($insertId>0)
		{
			return $insertId;
		}
		else{
			return false;
		}
			
	}

	function update($data,$id) {
		
		$pass_contact = '';
		if(str_replace(' ','',$data['ga_password']) != ''){
			$pass_contact = ",ga_password='".$data['ga_password']."'";
		}
		
		$loggedInUserId=$this->session->userdata('id');	
		$query=$this->db->query("UPDATE configuration SET 
        newsletter_section='".$data['newsletter_section']."',
        reachus_email='".$data['reachus_email']."',
        sendinquiry_email='".$data['sendinquiry_email']."',
        jobapp_email='".$data['jobapp_email']."',
        from_email='".$data['from_email']."',
        project_name='".$data['project_name']."',
        project_name_arb='".$data['project_name_arb']."',                  
        ga_user='".$data['ga_user']."'
		".$pass_contact.",
        ga_tracking_id='".$data['ga_tracking_id']."',
        ga_view_id='".$data['ga_view_id']."',
        ctct_api_key='".$data['ctct_api_key']."', 
        mailChimp_api_key='".$data['mailChimp_api_key']."', 				
        mailChimp_list_id='".$data['mailChimp_list_id']."',
        ctct_token='".$data['ctct_token']."',											                     
        ctct_from_email='".$data['ctct_from_email']."',												
        ctct_list='".$data['ctct_list']."',
        arb_sms='".$data['arb_sms']."',												
        eng_sms='".$data['eng_sms']."',
        smtp_host='".$data['smtp_host']."',
        smtp_email='".$data['smtp_email']."',
        phone='".$data['phone']."',
        our_email='".$data['our_email']."',
        smtp_password='".$data['smtp_password']."',
        smtp_port='".$data['smtp_port']."',        date='".$data['date']."',
        config_updated_at='".date('Y-m-d H:i:s')."',
        config_updated_by='".$loggedInUserId."',
		username_news='".$data['username_news']."',
		password_news='".$data['password_news']."',
		contact_email='".$data['contact_email']."',
		career_email='".$data['career_email']."',
		request_email='".$data['request_email']."',
        pub_status='".$data['pub_status']."' WHERE id = '$id'");
						
		if($query>0)
		{
			return true;
		}
		else{
			return false;
		}
			
	}           

	function updateSocial($data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update('social_links', $data);
        return ($query>0)?true:false;
    }
        
        
	public function fetchRow(){
			
			
		$this->db->select('*');
		$this->db->from('configuration');
                $this->db->order_by('id','DESC');
                $this->db->limit (1);
		$query = $this->db->get();
		
		if ($query->num_rows()==1) {
			return $query->row();
		} else {
			return false;
		}
	} 
	
	public function newsRoomLogin($uname, $password) {
	
		    $this->db->select('username_news,password_news');
            $this->db->from('configuration');
           // $this->db->where("`usr_uname` = '" . $uname . "' AND  `usr_pass` = '" . $password . "' AND `usr_pub_status`='1' ORDER BY `usr_created_at` DESC");           
            $this->db->where('username_news',$uname);
			$this->db->where('password_news',$password);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
	
	
	}

	public function fetchRowSocialLink(){
			
		$this->db->select('*');
		$this->db->from('social_links');
                $this->db->order_by('id','DESC');
                $this->db->limit (1);
		$query = $this->db->get();
		if ($query->num_rows()==1) {
			return $query->row();
		} else {
			return false;
		}
	} 

}//end