<?php

class Model_formlist extends CI_Model {

	function _construct() {
		// Call the Model constructor
		parent::_construct();
	}

	function save($data) {
		$this->db->set($data);
		$this->db->insert('configuration');
		$insertId=$this->db->insert_id();
		if($insertId>0)
		{
			return $insertId;
		}
		else{
			return false;
		}
			
	}
    public function saveValues($data)
    {
        echo '';
        $this->db->insert('formlist',$data);
        return $this->db->insert_id();
    }
	public function update($data) {

        $this->db->update('formlist',$data,array('id'=>1));
		/*$this->db->where('id', 1);
		$this->db->update('formlist', $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->affected_rows();
		} else {
			return false;
		}*/	
	}           
        
        
	public function fetchRow(){
		$query = $this->db->query("select * from formlist");
		 $result = $query->row();
		if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
	}
        
        function search_result($res){
            $this->db->select('*');
        $this->db->from('formlist');
        $this->db->like('skill_en', $res);
        $this->db->or_like('skill_ar', $res);
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
        }
        function search_result_all(){
            $this->db->select('*');
            $this->db->from('formlist');
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }

}//end