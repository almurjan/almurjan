<?php

class Model_consultant extends CI_Model {

    public function fetchAll($tb) {
		
		
        $query = $this->db->query("select * from ".$tb."");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetchAllQuestion($tb) {


        $query = $this->db->query("select * from ".$tb."");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetch($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetchAllConsultant($tb) {
        $query = $this->db->query("select * from ".$tb);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function saveConsultant($data)
    {
        $this->db->insert('Consultants',$data);
        return $this->db->insert_id();
    }
	
    
    public function save($tb,$data,$where = NULL)
    {
        if(empty($where)){
		$this->db->insert($tb,$data);
        return $this->db->insert_id();
		}else{
		$this->db->update($tb,$data,array('id'=>$where));
        return $where;
		}
    }
	
		public function updateConsultant($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('Consultants', $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->affected_rows();
		} else {
			return false;
		}
	}
    public function publishStatus($val,$id){
		$query1=$this->db->query("update consultant set pub_status=0 WHERE pub_status=1");
        $query=$this->db->query("update consultant set pub_status='".$val."',updated_at='".date('Y-m-d H:i:s')."' WHERE id =".$id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	public function deleteConsultant($id)
    {
        $this->db->query("delete from consultants where id=".$id);
    }
	
}
//end