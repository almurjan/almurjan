<?php

class Model_user_profiles extends CI_Model {

	function _construct() {
		// Call the Model constructor
		parent::_construct();
	}

	function save($data) {
		$this->db->set($data);
		$this->db->insert('contacts');
		$insertId=$this->db->insert_id();
		if($insertId>0)
		{
			return $insertId;
		}
		else{
			return false;
		}
			
	}

	public function fetchAllGroup() {

		$this->db->select('reservation_info.*,contents.eng_title as eng_title,contents.arb_title as arb_title');
		$this->db->from('reservation_info');		
		$this->db->join('users', 'reservation_info.user_id = users.id','left');		
		$this->db->join('user_events', 'user_events.id = reservation_info.user_event_id');		
		$this->db->join('contents', 'contents.id = user_events.event_id');
		$this->db->group_by('reservation_info.email');
		$this->db->order_by('reservation_info.id','DESC');
		
		$query = $this->db->get();
		
		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}
			
	}
	
	public function get_user_info_sub_admin($id)
    {
		$loggedInUserId = $this->session->userdata('id');
		
		$query=$this->db->query("SELECT reg_profile_ratings.ratings as ratings, registrations.* , contents.eng_title as cat_eng_name, contents.arb_title as cat_arb_name FROM `registrations` LEFT JOIN contents on registrations.cat_id = contents.id LEFT JOIN reg_profile_ratings on reg_profile_ratings.reg_id = registrations.id AND reg_profile_ratings.sub_admin_id = ".$loggedInUserId." WHERE registrations.id = ".$id."");
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	public function getSubAdminRatingOnApplication($id,$sub_admin_id)
    {
		$query=$this->db->query("SELECT * FROM reg_application_ratings WHERE reg_id = ".$id." AND sub_admin_id = ".$sub_admin_id."");
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	public function getSubAdminRatingOnProfile($id,$sub_admin_id)
    {
		$query=$this->db->query("SELECT * FROM reg_profile_ratings WHERE reg_id = ".$id." AND sub_admin_id = ".$sub_admin_id."");
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
	public function fetchAll($type,$filteration_type,$percentage) {
		
		$loggedInUserId = $this->session->userdata('id');
		$where_filteration = '';
		
		if($filteration_type == 'Pending'){
			$where_filteration.= '  Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.sub_admin_rating < 1 AND table1.is_profile_createad = 1';
		}
		else if($filteration_type == 'Already_rated'){
			$where_filteration.= '  Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.sub_admin_rating > 0  AND table1.is_profile_createad = 1';
		}
		else if($filteration_type == 'Published'){
			$where_filteration.= '  Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.sub_admin_rating >= '.$percentage.'  AND table1.is_profile_createad = 1 AND pub_status = 1';
		}
		else if($filteration_type == 'Rejected'){
			$where_filteration.= '  Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.sub_admin_rating >= '.$percentage.' AND table1.is_profile_createad = 1 AND pub_status = 2';
		}
		else{
			$where_filteration.= ' Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.is_profile_createad = 1';
		}
		if($type == 'admin'){
			//$where = ' WHERE sub_admin_rating < 1 AND registrations.is_profile_createad = 0';
		}
		$where = $where.$where_filteration;
		
		$query=$this->db->query("SELECT * from (SELECT IFNULL(reg_profile_ratings.ratings, '0') AS sub_admin_rating , registrations.* , contents.eng_title as eng_name, contents.arb_title as arb_name FROM `registrations` JOIN contents on registrations.cat_id = contents.id LEFT JOIN reg_profile_ratings on registrations.id = reg_profile_ratings.reg_id and reg_profile_ratings.sub_admin_id = ".$loggedInUserId." ".$where."");
		
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
			
	} 
	
	public function fetchAllCreatedProfiles($type,$filteration_type,$percentage) {
		
		$loggedInUserId = $this->session->userdata('id');
		$where_filteration = '';
		
		if($filteration_type == 'Pending'){
			$where_filteration.= '  Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.is_profile_createad = 1 AND table1.pub_status = 0';
		}
		else if($filteration_type == 'Already_rated'){
			$where_filteration.= '  Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.sub_admin_rating > 0  AND table1.is_profile_createad = 1';
		}
		else if($filteration_type == 'Published' || $filteration_type == 'Accepted'){
			$where_filteration.= '  Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.sub_admin_rating >= '.$percentage.'  AND table1.is_profile_createad = 1 AND pub_status = 1';
		}
		else if($filteration_type == 'Rejected'){
			$where_filteration.= '  Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.sub_admin_rating >= '.$percentage.' AND table1.is_profile_createad = 1 AND pub_status = 2';
		}
		else{
			$where_filteration.= ' Group BY registrations.id order by registrations.id DESC ) as table1 WHERE table1.is_profile_createad = 1';
		}
		
		$where = $where.$where_filteration;
		
		$query=$this->db->query("SELECT * from (SELECT IFNULL(reg_profile_ratings.ratings, '0') AS sub_admin_rating , count(profile_votings.profile_id) as voting_count, registrations.* , contents.eng_title as eng_name, contents.arb_title as arb_name FROM `registrations` JOIN contents on registrations.cat_id = contents.id LEFT JOIN reg_profile_ratings on registrations.id = reg_profile_ratings.reg_id LEFT JOIN profile_votings on registrations.id = profile_votings.profile_id ".$where."");
			
		
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
			
	} 
	
	/*
	public function fetchAll() {

		$query=$this->db->query("SELECT registrations.* , contents.eng_title as eng_name, contents.arb_title as arb_name FROM `registrations` JOIN contents on registrations.cat_id = contents.id order by id desc");
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
			
	} 
	*/
	
	public function fetchAllProfileRatings($id) {
		
		$query=$this->db->query("SELECT cms_users.usr_uname, cms_users.usr_email as usr_email, reg_profile_ratings.ratings FROM `cms_users` LEFT JOIN reg_profile_ratings on reg_profile_ratings.sub_admin_id = cms_users.id AND reg_profile_ratings.reg_id = ".$id." WHERE cms_users.usr_grp_id = 3 order by reg_profile_ratings.created_at desc");
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
			
	} 
	
	public function fetchAllCareers() {

		$query=$this->db->query("SELECT * FROM `careers` order by id desc");
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
			
	}
	public function fetchAllUsersExcel() {

		$query=$this->db->query("SELECT * FROM `users` WHERE id NOT IN (SELECT user_id FROM `reservation_info`)");
		if ($query) {
			return $query->result();
		} else {
			return false;
		}
			
	}
	public function fetchAllBookings() {

		$this->db->select('*');
		$this->db->from('contacts');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}

	}

    public function fetchRow($id){
		$query=$this->db->query("SELECT registrations.* , contents.eng_title as eng_name, contents.arb_title as arb_name FROM `registrations` JOIN contents on registrations.cat_id = contents.id WHERE registrations.id = ".$id." order by id desc");
		if ($query) {
			return $query->row();
		} else {
			return false;
		}
    }
    public function fetch($id){

        $this->db->select('*');
        $this->db->from('contacts');
        $this->db->where('id',$id);
        $query = $this->db->get();
        if ($query->num_rows()==1) {
            $result = $query->row();
        } else {
            $result = false;
        }
            return $result;
    }

	public function delete($id){
			
		$query=$this->db->query("delete from contacts WHERE id =".$id);
		if ($query) {
			return true;
		} else {
			return false;
		}

	}

}//end