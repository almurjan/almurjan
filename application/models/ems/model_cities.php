<?php

class Model_cities extends CI_Model {

    public function fetchAll($tb) {
		
        $query = $this->db->query("select * from ".$tb."");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }

    public function fetch($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetchAllAnnouncement_front($tb) {
        $this->db->select('*');
        $this->db->from($tb);
        $this->db->where('pub_status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function fetchAllAnnouncement($tb, $cid) {
        $this->db->select('*');
        $this->db->from($tb);
        $this->db->where('country_id', $cid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function saveEventType($data)
    {
        $this->db->insert('cms_cities',$data);
        return $this->db->insert_id();
    }
	
    
    public function save($tb,$data,$where = NULL)
    {
        if(empty($where)){
		$this->db->insert($tb,$data);
        return $this->db->insert_id();
		}else{
		$this->db->update($tb,$data,array('id'=>$where));
        return $where;
		}
    }
	
		public function updateEventType($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('cms_cities', $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->affected_rows();
		} else {
			return false;
		}
	}

    public function publishStatus($data,$id){

        $this->db->where('id', $id);
        $this->db->update('cms_cities', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->affected_rows();
        } else {
            return false;
        }

    }

	public function deleteEventType($id)
    {
        $this->db->query("delete from event_types where id=".$id);
    }
	
}
//end