<?php
class Model_properties extends CI_Model {

    public function fetchAll($tb) {
        $query = $this->db->query("SELECT * FROM ".$tb." ORDER By `id` DESC");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetchIds() {
        $query = $this->db->query("SELECT id FROM properties");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    /*Start Functions Frontend*/
    public function fetchCity_eng($id) {
        $query = $this->db->query("select city from properties where id=".$id);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function fetchCity_arb($id) {
        $query = $this->db->query("select city_arb from properties where id=".$id);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
	public function fetchCities() {
        $query = $this->db->query("SELECT DISTINCT `city` FROM `properties`");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function get_areas($id) {
        $query = $this->db->query("select * from properties where city=".$id);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function get_types($city,$area) {
        $query = $this->db->query("select * from properties where city=".$city." AND area=".$area);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function get_spaces($type) {
        $query = $this->db->query("select * from properties where `property_type`='".$type."' OR `property_type_arb`='".$type."'");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    
    /*End Functions Frontend*/

    public function fetch($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }

    public function saveData($data)
    {
        $this->db->insert('properties',$data);
        return $this->db->insert_id();
    }
	
    public function updateData($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('properties', $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->affected_rows();
		} else {
			return false;
		}
	}
	
	public function delete($id)
    {
        $this->db->query("delete from properties where id=".$id);
    }
    public function delete_request($id,$tb)
    {
        $this->db->query("delete from ".$tb." where id=".$id);
    }

    public function activeStatus($val,$id){
        $query=$this->db->query("update programs set pub_status='".$val."',updated_at='".date('Y-m-d H:i:s')."' WHERE id =".$id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
//end