<?php
class Model_category extends CI_Model {

    public function fetchAll($tb) {
        $query = $this->db->query("SELECT * FROM ".$tb."");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }

    public function fetch($id,$tb) {
        $query = $this->db->query("select * from ".$tb." where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }

    public function checkEmail($email) {
        $this->db->select('*');
        $this->db->from('motoon_logins');
        $this->db->where('email',$email);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
	public function checkUsername($username) {
        $this->db->select('*');
        $this->db->from('motoon_logins');
        $this->db->where('username',$username);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function saveData($data)
    {
        $this->db->insert('category',$data);
        return $this->db->insert_id();
    }
	
    public function updateData($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('category', $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->affected_rows();
		} else {
			return false;
		}
	}
	
	public function delete($id)
    {
        $this->db->query("delete from category where id=".$id);
    }

    public function activeStatus($val,$id){
        $query=$this->db->query("update programs set pub_status='".$val."',updated_at='".date('Y-m-d H:i:s')."' WHERE id =".$id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
//end