<?php

class Model_contents extends CI_Model {

    function _construct() {
        // Call the Model constructor
        parent::_construct();
    }
   public function updateGalleryImages($field_values, $id,$field_name)
	{
		$query = $this->db->query("update content_detail  set meta_value ='" . $field_values . "' WHERE meta_key = '" . $field_name . "' && post_id = " . $id);

		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function getGalleryImages($id,$field_name) {
		$sql="SELECT * FROM content_detail WHERE meta_key = '" . $field_name . "' && post_id = " . $id . " order by id desc";
       
		$query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
	public function fetchMoreReports($parant_id,$offset,$limit) {
		$sql="SELECT * FROM contents WHERE parant_id = '".$parant_id."' AND pub_status = 1 AND version =0 ORDER BY id DESC LIMIT {$limit} OFFSET {$offset}" ;
       
		$query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	public function loadMoreData($parant_id,$offset,$limit,$tpl) {
		$sql="SELECT * FROM contents WHERE parant_id = '".$parant_id."' AND pub_status = 1 AND version =0 AND tpl = '".$tpl."' ORDER BY sort_order ASC LIMIT {$limit} OFFSET {$offset}" ;
       
		$query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	public function front_end_content_news_media($id,$tpl) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->where('tpl', $tpl);
		$this->db->limit('4');
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function fetchUniqueCities() {

        $this->db->select('*');
        $this->db->from('request_quotation');
		$this->db->group_by('city');
		$this->db->order_by('city','asc');
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    } 
	
	public function getAllCountries() {

        $this->db->select('*');
        $this->db->from('countries');
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }  
	public function get_user_interests($id) {

        $this->db->select('*');
        $this->db->from('user_interest');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	public function getPublishedProfiles($cat_id) {
        $query = $this->db->query("SELECT * FROM (SELECT contents.arb_title, registrations.* , count(profile_votings.email) as voted_counts From registrations LEFT JOIN profile_votings on registrations.id = profile_votings.profile_id LEFT JOIN contents on registrations.cat_id = contents.id WHERE registrations.cat_id = '".$cat_id."' AND registrations.pub_status = 1 GROUP BY registrations.id ) as table1 ORDER BY voted_counts ASC");        $result = $query->result();        if(!empty($result))        {            return $result;        }else{            return array();        }
    }		public function getPublishedProfilesHighestVoting($cat_id) {		$query = $this->db->query("SELECT * FROM (SELECT contents.arb_title, registrations.* , count(profile_votings.email) as voted_counts From registrations LEFT JOIN profile_votings on registrations.id = profile_votings.profile_id LEFT JOIN contents on registrations.cat_id = contents.id WHERE registrations.cat_id = '".$cat_id."' AND registrations.pub_status = 1 GROUP BY registrations.id LIMIT 3 ) as table1 ORDER BY voted_counts DESC");        $result = $query->result();        if(!empty($result))        {            return $result;        }else{            return array();        }    }
	public function get_user_profile($id) {

        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
	public function getEventReservations($event_id,$user_id) {

        $this->db->select('*');
        $this->db->from('user_events');
        $this->db->where('event_id', $event_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
	
	public function fetch_event_dates($event_id) {

        $this->db->select('*');
        $this->db->from('event_dates');
        $this->db->where('event_id', $event_id);
        $this->db->order_by('event_date', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }
	
	public function fetch_event_dates_calendar($event_id,$eventCheckDate) {

        $this->db->select('*');
        $this->db->from('event_dates');
        $this->db->where('event_date',$eventCheckDate);
        $this->db->order_by('event_date', 'ASC');
        $this->db->group_by('event_date');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }
	
	public function check_archieved($event_id,$current_date) {

        $this->db->select('*');
        $this->db->from('event_dates');
        $this->db->where('event_date >=',$current_date);
        $this->db->where('event_id',$event_id);
        $this->db->order_by('event_date', 'ASC');
        $this->db->group_by('event_date');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }
	
	public function publishStatus($val, $id) {
        $loggedInUserId = $this->session->userdata('id');
        $query = $this->db->query("update contents set pub_status='" . $val . "',content_updated_at='" . date('Y-m-d H:i:s') . "',content_updated_by='" . $loggedInUserId . "' WHERE id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function categoryPublishStatus($val, $id) {
        $loggedInUserId = $this->session->userdata('id');
        $query = $this->db->query("update categories set status='" . $val . "' WHERE category_id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	
	public function activeSlider($val, $id) {
		$loggedInUserId = $this->session->userdata('id');
		
        $query = $this->db->query("update contents set is_slider_show='" . $val . "',content_updated_at='" . date('Y-m-d H:i:s') . "',content_updated_by='" . $loggedInUserId . "' WHERE id =" . $id);
		if($query) {
			return true;
        } else {
            return false;
        }
    }
	
	public function deleteEventDates($id) {

        $query = $this->db->query("delete from event_dates WHERE event_id =" . $id);
		if ($query) {
            return true;
        } else {
            return false;
        }
    }
	public function deleteUserInterests($id) {

        $query = $this->db->query("delete from user_interest WHERE user_id =" . $id);
		if ($query) {
            return true;
        } else {
            return false;
        }
    }
    function saveRegistrations($event_array) {

        $this->db->set($event_array);
        $this->db->insert('registrations');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveCompanies($data_array) {
        $this->db->set($data_array);
        $this->db->insert('companies_map');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function updateCompanies($data_array,$id) {//ahmed
        $this->db->where('post_id', $id);
        $query = $this->db->update('companies_map', $data_array);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    public function getCompany($id) {
        $this->db->select('*');
        $this->db->from('companies_map');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row();
        } else {
            return false;
        }
    }
    public function getCompanies() {
        $this->db->select('*');
        $this->db->from('companies_map');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row();
        } else {
            return false;
        }
    }
	
    function saveRightOfNewPage($data) {
        $this->db->set($data);
        $this->db->insert('page_group_rights');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function save($data) {
        $this->db->set($data);
        $this->db->insert('contents');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveCategory($data) {
        $this->db->set($data);
        $this->db->insert('categories');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
	public function get_Date($page_id) {			
	$this->db->select('*');       
	$this->db->from('contents');				
	$this->db->where('id', $page_id);		
	$query = $this->db->get();	        
	if ($query->num_rows() > 0) 
	{            
	return $query->result();       
	} else {           
	return false;      
	}    }

    public function get_catogries_data($cat_id) {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        } else {
            return false;
        }
    }
	
	function saveNewsletterSubscriber($data) {
        $this->db->set($data);
        $this->db->insert('users');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }

	function saveComedianBooking($data) {
        $this->db->set($data);
        $this->db->insert('booking_comedian');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }

	
	 function save_content_detail($data) {
				 
        $this->db->set($data);
        $this->db->insert('content_detail');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
	
	//$
	 public function getPageIdbyTemplate($page_title) {
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
        $this->db->where('tpl', $page_title);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	//$
	function getMultipleProductCategory() {
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('tpl', 'our_services');
		$this->db->where('listing_level', '1');
		$this->db->where('type','page');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
	
	}
	public function contactUsFormData($data = array()){
        $this->table = "contacts";
        $result = $this->db->insert($this->table, $data);
        return $result ? $this->db->insert_id() : false;
    }

    public function getContactUsFormData() {
        $query = $this->db->query('SELECT * FROM "contacts" order by id desc');

        if($query) {
            return $query->result_array();
        }else {
            return false;
        }
    }
	
	function requestQuotation($data = array())
	{
		$this->table = "request_quotation";
		$result = $this->db->insert($this->table, $data);
		return $result?$this->db->insert_id():false;
	}
	
	function job_application_data($data = array())
	{
		$this->table = "careers";
		$result = $this->db->insert($this->table, $data);
		return $result?$this->db->insert_id():false;
	}
	
	function fetchQuotation($date_request,$country,$city,$services)
	{
		$concat_query = '';
		$date = $date_request;
		if($date != ''){
			
			$date = date('Y-m-d',strtotime($date_request));
			$concat_query .= " AND created_at LIKE '%{$date}%' ";
		}
		if($country != ''){
			$concat_query .= " AND country = '{$country}' ";
		}
		
		if($city != ''){
			$concat_query .= " AND city = '{$city}' ";
		}
		
		if($services != ''){
			$concat_query .= " AND product_service = '{$services}' ";
		}
		
		if($country != '' || $city != '' || $services != '' || $date != ''){
			$where = ' Where ';
			$concat_query = trim($concat_query," AND");
			
			//$concat_query = str_replace('TE', 'DATE',$concat_query);
			//$concat_query = str_replace('DADATE', 'DATE',$concat_query);
			$where .= $concat_query; 
			
		}
		
		$query=$this->db->query("SELECT * FROM `request_quotation` " .$where . " order by id desc");
		
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
	}	
	/* function getProductServiceName($page_id) {
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('id', $page_id);
		$this->db->where('type', 'page');
		$query = $this->db->get();
		return $query;
	} */
    function update($data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update('contents', $data);
        return ($query>0)?true:false;
    }
    function updateCategory($data, $id) {
        $this->db->where('category_id', $id);
        $query = $this->db->update('categories', $data);
        return ($query>0)?true:false;
    }
    function updateRegistraionProfile($data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update('registrations', $data);
        return ($query>0)?true:false;
    }
	
    function updateProfile($data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update('users', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
    function updateUserQrCode($id,$data) {
        $this->db->where('id', $id);
        $query = $this->db->update('reservation_info', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
    function updateProfileReservation($data, $id) {
        $this->db->where('user_id', $id);
        $this->db->where('role', 1);
        $query = $this->db->update('reservation_info', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
    function updateReservation($data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update('user_events', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	public function front_end_content_by_template($template,$order_by='DESC') {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('tpl', $template);
		$this->db->where('version', 0);
		$this->db->where('pub_status', 1);
		$this->db->order_by('id',$order_by); 
		//$this->db->limit(1);   
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	function saveContactUs($data) { 
		$this->db->set($data);
		$this->db->insert('contacts');
		$insertId=$this->db->insert_id();
		if($insertId>0)  {   
		return $insertId;
		}  else{  
		return false;
		}    
	}
	function saveProfileVoting($data) { 
		$this->db->set($data);
		$this->db->insert('profile_votings');
		$insertId=$this->db->insert_id();
		if($insertId>0)  {   
		return $insertId;
		}  else{  
		return false;
		}    
	}
	function saveCareer($data) { 
		$this->db->set($data);
		$this->db->insert('careers');
		$insertId=$this->db->insert_id();
		if($insertId>0)  {   
		return $insertId;
		}  else{  
		return false;
		}    
	}
    function saveSelfApplication($data) {
        $this->db->set($data);
        $this->db->insert('self_application');
        $insertId=$this->db->insert_id();
        if($insertId>0)  {
            return $insertId;
        }  else{
            return false;
        }
    }
    function saveMedRecords($data) {
        $this->db->set($data);
        $this->db->insert('med_records');
        $insertId=$this->db->insert_id();
        if($insertId>0)  {
            return $insertId;
        }  else{
            return false;
        }
    }
	function saveUser($data) { 
		$this->db->set($data);
		$this->db->insert('users');
		$insertId=$this->db->insert_id();
		if($insertId>0)  {   
		return $insertId;
		}  else{  
		return false;
		}    
	}
	function saveUserInterests($user_interests) { 
		$this->db->set($user_interests);
		$this->db->insert('user_interest');
		$insertId=$this->db->insert_id();
		if($insertId>0)  {   
		return $insertId;
		}  else{  
		return false;
		}    
	}
	function saveEventRegistraions($data) { 
		$this->db->set($data);
		$this->db->insert('user_events');
		$insertId=$this->db->insert_id();
		if($insertId>0)  {   
		return $insertId;
		}  else{  
		return false;
		}    
	}
	function saveReservations($dataReservation) { 
		$this->db->set($dataReservation);
		$this->db->insert('reservation_info');
		$insertId=$this->db->insert_id();
		if($insertId>0)  {   
		return $insertId;
		}  else{  
		return false;
		}    
	}
    function saveCareerData($data) {
        $this->db->set($data);
        $this->db->insert('users');
        $insertId=$this->db->insert_id();
        if($insertId>0)  {
            return $insertId;
        }  else{
            return false;
        }
    }
    function saveComedianForm($data) {
        $this->db->set($data);
        $this->db->insert('consultants');
        $insertId=$this->db->insert_id();
        if($insertId>0)  {
            return $insertId;
        }  else{
            return false;
        }
    }
	function update_headings($data, $id) {
        $this->db->where('id', $id);
        $query = $this->db->update('client_slider', $data);
        return ($query>0)?true:false;
    }
	
	public function fetch_products_categories(){
		$this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		$this->db->where('move_to_trash', 0);
		$this->db->where('tpl', 'products-page');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	public function getAllData($item_name = ''){
        $query = $this->db->query("SELECT contents.* FROM (`contents`) LEFT JOIN content_detail on contents.id = content_detail.post_id WHERE (contents.type = 'page' AND contents.move_to_trash = 0 AND contents.`id` != 948 AND contents.`id` != 32 AND contents.`parant_id` != 32 AND contents.`id` != 30 AND contents.`parant_id` != 30 AND contents.`id` != 214 AND contents.pub_status = '1') AND (contents.eng_title LIKE '%{$item_name}%' || contents.arb_title LIKE '%{$item_name}%' || content_detail.meta_value LIKE '%{$item_name}%') AND contents.tpl IN ('about_us', 'contact_us', 'csr', 'events', 'history', 'home', 'other_sectors') GROUP BY contents.id");

        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return array();
        }
	}
	public function getAllEvents($category_id,$city_id,$event_date){
		$this->db->select('*');
        $this->db->from('content_detail');
		
		if($category_id){
			$this->db->where('meta_key', 'category');
			$this->db->where_in('meta_value', $category_id);
		}
		if($city_id){
			$this->db->or_where('meta_key', 'city');
			$this->db->or_where('meta_value', $city_id);
		}
        $this->db->group_by('post_id');
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	
	public function checkEventDateOld($event_id,$event_date) {
        $query = $this->db->query("select * from event_dates where event_id = ".$event_id." AND event_date='".$event_date."'");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
	
	public function checkEventDate($city_id,$category_id,$event_date) {
		if($city_id != ''){
			$where_city = ' AND `contents`.`city` = "'.$city_id.'"';
		}
		else{
			$where_city = '';
		}
		
		if($category_id != ''){
			$where_category = ' AND `contents`.`category` IN ("'.implode('","',$category_id).'")';
		}
		else{
			$where_category = '';
		}
		
		if($event_date != ''){
			$where_event_date = ' AND `event_dates`.`event_date` = "'.$event_date.'"';
		}
		else{
			$where_event_date = '';
		}
		
        $query = $this->db->query("SELECT contents.* FROM `content_detail` JOIN `event_dates` on `content_detail`.post_id = `event_dates`.event_id JOIN `contents` on `content_detail`.post_id = `contents`.id WHERE `contents`.tpl='event' AND version=0 ".$where_city.$where_category.$where_event_date." GROUP BY `contents`.`eng_title` order by `event_dates`.`event_date` ASC");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
	
	function SaveQuote($data) {

        $this->db->set($data);
        $this->db->insert('quote_requests');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {

            return $insertId;

        } else {

            return false;
        }

    }

    public function fetchAllProjects($page_id){
        $query = $this->db->query("SELECT c.* FROM `contents` c LEFT OUTER JOIN `contents` co ON co.id = c.parant_id 
        WHERE co.parant_id = ".$page_id." AND co.pub_status = '1' AND c.version = 0 AND c.type = 'page'");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }

    public function fetchAllFilter($offset_value,$limit,$id) {

        $query = $this->db->query("SELECT `contents`.* FROM `contents` WHERE parant_id = {$id} AND type = 'page' AND pub_status = '1'  ORDER BY id DESC LIMIT {$limit} OFFSET {$offset_value} ");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function fetchAllFilterLands($offset_value,$limit,$id) {

        $query = $this->db->query("SELECT `contents`.* FROM `contents` WHERE parant_id = {$id} AND type = 'page' AND pub_status = '1'  ORDER BY contents.sort_order ASC LIMIT {$limit} OFFSET {$offset_value} ");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }

	public function fetchAllProjects_home($page_id){
        $query = $this->db->query("SELECT c.* FROM `contents` c LEFT OUTER JOIN `contents` co ON co.id = c.parant_id 
        WHERE co.parant_id = ".$page_id." AND co.pub_status = '1' AND c.version = 0 AND c.type = 'page' AND c.display_to_home = '1' AND c.pub_status = '1'");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }

    public function fetchAll($move_to_trash) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		$this->db->where('move_to_trash', $move_to_trash);
		$this->db->where('parant_id', 0);
		$this->db->where('version', 0);
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function fetchPropertyTypes($city_id) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
        $this->db->where('pub_status', 1);
        $this->db->where('property_city', $city_id);
        $this->db->where('version', 0);
        $this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function fetchPropertyTitles($city_id,$parent_id) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
        $this->db->where('pub_status', 1);
        $this->db->where('property_city', $city_id);
        $this->db->where('parant_id', $parent_id);
        $this->db->where('version', 0);
        $this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function fetchPropertyServices($title_id) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
        $this->db->where('pub_status', 1);
        $this->db->where('id', $title_id);
        $this->db->where('version', 0);
        $this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	public function homeService() {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		$this->db->where('move_to_trash', 0);
		$this->db->where('tpl', 'services');
		$this->db->where('display_to_home',1);
		$this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
		public function homeProject() {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		$this->db->where('move_to_trash', 0);
		$this->db->where('tpl', 'projects');
		$this->db->limit(1);
	    $query = $this->db->get();
        if ($query->num_rows() > 0) {
			$res = $query->result();
			return $this->getChlidProject($res[0]->id);
           
        } else {
            return false;
        }
    }
	public function getChlidProject($id) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		$this->db->where('move_to_trash', 0);
		$this->db->where('display_to_home',1);
		$this->db->where('tpl', '');
		$this->db->where('parant_id',$id);
		$this->db->limit(1);
	    $query = $this->db->get();
        if ($query->num_rows() > 0) {
			return $query->result();
			
           
        } else {
            return false;
        }
    }

	public function checkEmailExist($email) {

        $this->db->select('*');
        $this->db->from('users');
		$this->db->where('email', $email);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
			return $query->row();  
        } else {
            return false;
        }
    }

	public function fetchRowDateTime($id) {

        $this->db->select('*');
        $this->db->from('event_dates');
		$this->db->where('event_id', $id);
		$this->db->limit(1);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
			return $query->row();  
        } else {
            return false;
        }
    }
	public function checkVotingOnProfile($email,$profile_id) {

        $this->db->select('*');
        $this->db->from('profile_votings');
		$this->db->where('email', $email);
		$this->db->where('profile_id', $profile_id);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
			return $query->result();  
        } else {
            return false;
        }
    }

	public function checkUserExist($id) {

        $this->db->select('*');
        $this->db->from('users');
		$this->db->where('id', $id);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
			return $query->row();  
        } else {
            return false;
        }
    }

	public function getLocations() {

        $query = $this->db->query("select DISTINCT arb_location,eng_location from contents where type='page'");

        $result = $query->result();

        if(!empty($result))

        {

            return $result;

        }else{

            return false;

        }

    }
    public function getLocation($id) {

        $query = $this->db->query("select eng_location from contents where type='page' AND id =".$id);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
       public function fetchSorted($city,$date) {
             

        $query = "select * from contents where 1=1 and type = 'page'";
        if($date!="")
          $query=$query." and pro_date like '%$date%'"; 
        if($city!="")
           $query=$query." and eng_location like '%$city%'";
         $query = $this->db->query($query);
    
	 $result = $query->result();
    
        if(!empty($result))

        {

            return $result;

        }else{

            return false;

        }

    }	
   
     public function getDates() {

        $query = $this->db->query("select DISTINCT pro_date from contents where type ='page' ORDER BY pro_date ");
		 $result = $query->result();
        if(!empty($result))

        { 
		return $result;

        }else{

            return false;

        }

    }
    public function getStartDate($id) {
        $query = $this->db->query("select start_date from contents where type ='page' AND id =".$id);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
    public function getEndDate($id) {
        $query = $this->db->query("select end_date from contents where type ='page' AND id=".$id);
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
	 public function page_content($page_id) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		$this->db->where('id', $page_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
    public function checkUniquePageTitleEdit($page_title,$id = '') {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		if($id != ''){
			$this->db->where('id !=', $id);
		}
		$this->db->where('eng_title', $page_title);
        $query = $this->db->get();
		if ($query->num_rows() > 0) {
          return 1;
        } else {
           return 0;
        }
    }
    public function checkPageTitleRepeat($page_title,$id = '') {
        //var_dump('here');die;
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
        if($id != ''){
            $this->db->where('id !=', $id);
        }
        $this->db->where('eng_title', $page_title);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public function RepeatedButNotIsFlag($page_title,$id = '') {
        //var_dump('here');die;
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
        if($id != ''){
            $this->db->where('id !=', $id);
        }
        $this->db->where('eng_title', $page_title);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public function RepeatedPagesDetail($page_title,$id = '') {
        //var_dump('here');die;
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
        if($id != ''){
            $this->db->where('id !=', $id);
        }
        $this->db->where('eng_title', $page_title);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	public function checkUniquePageTitle($page_title) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		$this->db->where('eng_title', $page_title);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          return 1;
        } else {
           return 0;
        }
    }
	
	
	public function getPageTitle($page_id) {

		$this->db->select('*');
        $this->db->from('contents');
		
		$this->db->where('id', $page_id);
      
		$query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function getPageLocations($page_id) {
        $this->db->select('country_name,country_name_ar');
        $this->db->from('contents');
        $this->db->where('parant_id', $page_id);
        $this->db->where('country_name != ""');
        $this->db->where('country_name_ar != ""');
        $this->db->group_by('country_name');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	
	public function getPageId($page_title) {

     

		//echo $page_title;
        // $query = $this->db->query("SELECT * FROM `contents` WHERE `type` = 'page' AND `eng_title` = '".$page_title."'");
		//$this->db->cache_off();
		$this->db->select('*');
        $this->db->from('contents');
		// $this->db->where('type', 'page');
		// $this->db->where('eng_title', html_entity_decode($page_title));
        // $this->db->where('eng_title', htmlentities($page_title));
        // $this->db->or_where('eng_title', $page_title);
        $this->db->where("type = 'page' AND (eng_title = '".htmlentities($page_title)."' OR eng_title = '".$page_title."')");
       //$query = $this->db->_compile_select();
		$query = $this->db->get();
		 // echo 'dfgdsg';
		/*  echo $this->db->last_query();
		  $res = $query->result();
		  echo '<pre>';
		  print_r($res);
		  exit();*/
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	    public function fetchAllVersion($id) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'version');
		$this->db->where('parant_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

	 public function countPageVersions($id) {

        $this->db->select('version');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
		$this->db->order_by('version','desc');
        $query = $this->db->get();
		if ($query->num_rows() > 0) {
            $result = $query->result();
			return $result[0]->version;
        } else {
            return false;
        }
    }
    public function fetchRow($id) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchRowCategory($id) {

        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('category_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
	   public function fetchRowCareer($id) {

        $this->db->select('*');
        $this->db->from('careers');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchRowSelf($id) {

        $this->db->select('*');
        $this->db->from('self_application');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

	public function get_bookings($val) {
		
		if($val == 'today'){
			$current_date = date('Y-m-d');
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info WHERE created_at LIKE '%$current_date%'");
		}
		if($val == 'yesterday'){
			$current_date = date('Y-m-d',strtotime("-1 days"));
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info WHERE created_at LIKE '%$current_date%'");
		}
		if($val == 'week'){
			$current_date = date('Y-m-d') .' 24:60:60';
			$week_date = date('Y-m-d',strtotime("-7 days")) .' 00:00:00';
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info WHERE `created_at` <= '{$current_date}' AND `created_at` >= '{$week_date}'");
		}
		
		if($val == 'last_month'){
			$current_date = date('Y-m-d') .' 24:60:60';
			$week_date = date('Y-m-d',strtotime("-30 days")) .' 00:00:00';
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info WHERE `created_at` <= '{$current_date}' AND `created_at` >= '{$week_date}'");
		}
		else if($val == 'total'){
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info");
		}
		
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return array();
        }
    }
	
	public function get_event_bookings($val,$event_id) {
		
		if($val == 'today'){
			$current_date = date('Y-m-d');
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info JOIN user_events on reservation_info.user_event_id = user_events.id WHERE reservation_info.created_at LIKE '%$current_date%' AND user_events.event_id = {$event_id} ");
		}
		if($val == 'yesterday'){
			$current_date = date('Y-m-d',strtotime("-1 days"));
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info JOIN user_events on reservation_info.user_event_id = user_events.id WHERE reservation_info.created_at LIKE '%$current_date%' AND user_events.event_id = {$event_id} ");
		}
		if($val == 'week'){
			$current_date = date('Y-m-d') .' 24:60:60';
			$week_date = date('Y-m-d',strtotime("-7 days")) .' 00:00:00';
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info JOIN user_events on reservation_info.user_event_id = user_events.id WHERE reservation_info.created_at <= '{$current_date}' AND reservation_info.created_at >= '{$week_date}' AND user_events.event_id = {$event_id}");
		}
		
		if($val == 'last_month'){
			$current_date = date('Y-m-d') .' 24:60:60';
			$week_date = date('Y-m-d',strtotime("-30 days")) .' 00:00:00';
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info JOIN user_events on reservation_info.user_event_id = user_events.id WHERE reservation_info.created_at <= '{$current_date}' AND reservation_info.created_at >= '{$week_date}' AND user_events.event_id = {$event_id}");
		}
		else if($val == 'total'){
			$query = $this->db->query("SELECT COUNT(*) as count_users FROM reservation_info JOIN user_events on reservation_info.user_event_id = user_events.id WHERE user_events.event_id = {$event_id}");
		}
		
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return array();
        }
    }
	
	public function get_users_of_category($interest_id) {
		$query = $this->db->query("SELECT COUNT(*) as count_users FROM user_interest WHERE interest_id = " . $interest_id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return array();
        }
    }
	
	  public function fetchRowReservation($id){
		$this->db->select('reservation_info.*,contents.eng_title as eng_title,contents.arb_title as arb_title');
        $this->db->from('reservation_info');
		$this->db->join('user_events', 'user_events.id = reservation_info.user_event_id');
		$this->db->join('contents', 'contents.id = user_events.event_id');

        $this->db->where('reservation_info.id',$id);

        $query = $this->db->get();

        if ($query->num_rows()==1) {

            $result = $query->row();

        } else {
            $result = false;
        }
            return $result;
    }
	
	 public function fetchHeadings($id) {

        $this->db->select('*');
        $this->db->from('client_slider');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function fetch_list_content($id) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
		$this->db->where('parant_id', $id);
	   // $this->db->where('tpl', '');
		$this->db->where('version', '0');
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function fetch_list_content_projects($id) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
		$this->db->where('parant_id', $id);
	    $this->db->where('is_project', 1);
		$this->db->where('version', '0');
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function fetch_list_content_category() {

        $this->db->select('*');
        $this->db->from('categories');
        $this->db->order_by('category_id','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function fetch_list_content_all() {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
	   // $this->db->where('tpl', '');
		$this->db->where('version', '0');
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function fetch_list_content_sections($id) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
		$this->db->where('parant_id', $id);
	    $this->db->where('pub_status', 1);
		$this->db->where('version', '0');
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    } 
	public function fetch_list_content_sections_backend($id) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
		$this->db->where('parant_id', $id);
	    //$this->db->where('pub_status', 1);
		$this->db->where('version', '0');
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	
    public function fetch_event_registrations($id) {

       $query = $this->db->query("SELECT r_i.* , u_e.no_of_reservations FROM `reservation_info` r_i LEFT OUTER JOIN `user_events` u_e ON r_i.user_event_id = u_e.id WHERE u_e.event_id = ".$id."");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return array();
        }
    }
	
    public function fetch_category($categor_id) {

        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('id', $categor_id);
	     $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->row();
        } else {
            return false;
        }
    }
	
    public function fetch_event_name($id) {

        $this->db->select('contents.id,contents.eng_title as eng_title,contents.arb_title as arb_title');
        $this->db->from('reservation_info');
		$this->db->join('user_events', 'user_events.id = reservation_info.user_event_id');
		$this->db->join('contents', 'contents.id = user_events.event_id');
		$this->db->where('reservation_info.user_event_id', $id);
	     $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->row();
        } else {
            return false;
        }
    }
	
    public function getReservationDetails($id) {
		$current_date = date('Y-m-d');
        $this->db->select('contents.id as id,contents.eng_title as eng_title,contents.arb_title as arb_title,reservation_info.id as reservation_no,event_dates.*');
        $this->db->from('reservation_info');
		$this->db->join('user_events', 'user_events.id = reservation_info.user_event_id');
		$this->db->join('contents', 'contents.id = user_events.event_id');
		$this->db->join('event_dates', 'event_dates.event_id = user_events.event_id');
		$this->db->where('reservation_info.id', $id);
		$this->db->where('event_dates.event_date >=', $current_date);
		$this->db->limit(1);
	     $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->row();
        } else {
            return false;
        }
    }
	
	public function getParentId($id) {

        $this->db->select('parant_id');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		$this->db->where('id', $id);
		$this->db->where('version', '0');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
			$result = $query->result();
          return $result[0]->parant_id;
        } else {
            return false;
        }
    }
	
	
	 public function list_content_dropdown() {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('type', 'page');
		$this->db->where('tpl', 'projects');
		$this->db->where('parant_id', '0');
		$this->db->where('version', '0');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
			
          return $query->result();
        } else {
            return false;
        }
    }
	
	public function front_end_event($id,$limit) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$this->db->where('type', 'page');
		$this->db->where('pub_status', 1);
		$this->db->order_by('sort_order','asc');
		if($limit != ''){
			$this->db->limit($limit);
		}
	   $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	
	// this is the function which is made by sir waqas to fetch all the events of future dates.
	
	public function front_end_event_future_old($id,$limit) {
		$current_date = date('Y-m-d');
        $query = $this->db->query('SELECT * FROM `contents` where tpl="event" and pub_status = 1 and id in (SELECT event_id FROM ( SELECT * FROM `event_dates` where event_date >= "'.$current_date.'" order by MONTH(event_date) asc limit 99999999999 ) AS sub GROUP BY event_id order by MONTH(event_date) asc) and version=0 ');
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_event_archived($id,$limit,$city) {
		$current_date = date('Y-m-d');
		if($city != '' AND $city != 'page'){
			$concat_city = ' AND city = '.$city;
		}
		else{
			$concat_city = '';
		}
        $query = $this->db->query("SELECT `contents`.* , `event_dates`.event_date FROM `contents` left join `event_dates` on `contents`.id = `event_dates`.`event_id` where tpl='event' and pub_status = 1 and `type`='page' and `event_dates`.event_date IS NOT NULL {$concat_city} and `event_dates`.event_date != '0000-00-00' and `event_dates`.event_date < '".$current_date."' GROUP BY contents.id order by `event_dates`.event_date DESC LIMIT 6");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_event_archived_past($offset_value,$limit,$city) {
		
		$current_date = date('Y-m-d');
		if($city != ''){
			$concat_city = ' AND city = '.$city;
		}
		else{
			$concat_city = '';
		}
        $query = $this->db->query("SELECT `contents`.* , `event_dates`.event_date FROM `contents` left join `event_dates` on `contents`.id = `event_dates`.`event_id` where tpl='event' and pub_status = 1 and `type`='page' and `event_dates`.event_date IS NOT NULL {$concat_city} and `event_dates`.event_date != '0000-00-00' and `event_dates`.event_date < '".$current_date."'  GROUP BY contents.id order by `event_dates`.event_date DESC LIMIT 6 OFFSET {$offset_value}");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	
	public function front_end_reports($id) {
        $query = $this->db->query("SELECT `contents`.* , `countries`.eng_country_name, `countries`.arb_country_name FROM `contents` left join `countries` on `contents`.cms_country = `countries`.`id`  where  pub_status = 1 and `type`='page' and parant_id = ".$id." GROUP BY contents.id");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }	
	public function front_end_event_future_next($offset_value,$limit,$city) {
		$current_date = date('Y-m-d');
		if($city != ''){
			$concat_city = ' AND city = '.$city;
		}
		else{
			$concat_city = '';
		}
        $query = $this->db->query("SELECT `contents`.* , `event_dates`.event_date FROM `contents` left join `event_dates` on `contents`.id = `event_dates`.`event_id` where tpl='event' and pub_status = 1 and `type`='page' and `event_dates`.event_date IS NOT NULL {$concat_city} and `event_dates`.event_date != '0000-00-00' and `event_dates`.event_date >= '".$current_date."'  GROUP BY contents.id order by `event_dates`.event_date LIMIT 6 OFFSET {$offset_value}");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }	
	
	public function get_city_name($id) {

        $this->db->select('*');
        $this->db->from('city');
		$this->db->where('id', $id);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->row();
        } else {
            return false;
        }
    }
	public function getSaudiCities() {

        $this->db->select('*');
        $this->db->from('city');
		$this->db->where('countrycode', 'SAU');
		$this->db->order_by('eng_name', 'ASC');
		//$this->db->limit(20);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_content($id,$tpl='',$listing_level='') {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		//$this->db->where('service_section', 0);
		//$this->db->where('service_type', 0);
        if($tpl == 'profile' && $listing_level!=''){
            $this->db->where('companies_listing', $listing_level);
        }
		if($tpl == 'bod'){
            $this->db->order_by('category','asc');
        }
		if($tpl == 'homepage_sector_sorting'){
            $this->db->order_by('sectors_logo_sorting','asc');
        }else{
            $this->db->order_by('sort_order','asc');
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function assets_front_end_content($id,$tpl='',$listing_level='',$location='',$field='') {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->join('categories', 'contents.category_id = categories.category_id');
        $this->db->where('contents.parant_id', $id);
        $this->db->where('contents.pub_status', 1);
        $this->db->where('contents.version', 0);
        $this->db->where('categories.status', 1);

        //$this->db->where('service_section', 0);
        //$this->db->where('service_type', 0);
        if($location && $field){
            $this->db->where($field, $location);
        }
        $this->db->order_by('contents.sort_order','asc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
    public function career_front_end_content($id,$benefits='') {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        //$this->db->where('service_section', 0);
        //$this->db->where('service_type', 0);
            $this->db->where('is_benefits', $benefits);
            $this->db->where('type', 'page');

        $this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_content_upcomming_events($id) {
	
	  $date = date('m/j/Y');
      $query = $this->db->query("SELECT content_detail.meta_value,contents.* FROM (`contents`) LEFT JOIN `content_detail` ON `content_detail`.`post_id` = `contents`.`id` WHERE `contents`.`parant_id` = '146' AND `contents`.`pub_status` = 1 AND `contents`.`version` = 0 AND content_detail.meta_key = 'start_date' AND content_detail.meta_value >= '".$date."' GROUP BY `contents`.`id` ORDER BY `contents`.`sort_order` asc");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function backend_end_content($id,$is_benefits='',$tpl='') {
        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		//$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		if($tpl=='profile' && $is_benefits>=0){
            $this->db->where('companies_listing', $is_benefits);
        }elseif($is_benefits!=''){
            $this->db->where('is_benefits', $is_benefits);
        }
		//$this->db->where('service_type', 0);
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function backend_end_content_section($id,$section) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		//$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->where('service_section', $section);
		//$this->db->where('service_type', 0);
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->row();
        } else {
            return false;
        }
    }
	public function getRegions() {

        $this->db->select('*');
        $this->db->from('cms_regions');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function get_region_cities($id) {

        $this->db->select('*');
        $this->db->from('cms_regions_city');
		$this->db->where('region_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_regions($id,$reg_id,$city_id) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		if($reg_id){
			$this->db->where('region_id', $reg_id);
		}
		if($city_id){
			$this->db->where('city_id', $city_id);
		}
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_content_objectives($id) {

        $this->db->select('*');
        $this->db->from('compitition_objectives');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		//$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_content_awards($id) {

        $this->db->select('*');
        $this->db->from('awards');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		//$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_content_winners($id) {

        $this->db->select('*');
        $this->db->from('submit_participate');
		$this->db->where('compitition_id', $id);
		//$this->db->where('pub_status', 1);
		$this->db->order_by('user_rank','asc');
		
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_content_ordering($id,$limit,$ordering) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->order_by('id',$ordering);
		if($limit != 'all'){
			  $this->db->limit($limit);
		}
        $query = $this->db->get();
		if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }

    public function front_end_content_custom_ordering($id, $order_by = 'id', $order_as = 'asc', $limit = false) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->order_by($order_by,$order_as);
        if($limit){
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }

    public function front_end_content_tenders($id,$status) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('program_status', $status);
        $this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_content_reports($section_type) {
        $query = $this->db->query('SELECT main_parant_contents.eng_title as "service_name", main_parant_contents.arb_title as "service_name_arb" , parent_contents.id as "parant_record_id", parent_contents.eng_title as 
		"parant_eng_title", parent_contents.arb_title as "parant_arb_title", contents.* FROM contents AS contents INNER JOIN contents AS parent_contents ON parent_contents.id = contents.parant_id INNER JOIN contents AS main_parant_contents ON parent_contents.parant_id = main_parant_contents.id WHERE contents.`service_section` = '.$section_type.' AND contents.`type` = "page"');
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function get_all_services() {
        $query = $this->db->query('SELECT * FROM `contents` WHERE type = "page" and tpl = "services" and service_section = "0"');
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function get_only_event_cities() {
        $query = $this->db->query('SELECT ct.* FROM `contents` c JOIN `city` ct ON ct.id = c.city WHERE type = "page" GROUP BY c.city');
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_programs($id) {
        $query = $this->db->query('SELECT c.* FROM `contents` c LEFT OUTER JOIN `contents` co ON co.id = c.parant_id 
        WHERE co.parant_id = '.$id.' AND c.version = 0 AND c.type = \'page\'');
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_content_displayHome($id) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->where('display_to_home', 1);
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function getBranches($city) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('career_location', $city);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	
	public function getConstructions($type) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('construction_type', $type);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function getAnswers($id){
        $this->db->select('*');
        $this->db->from('consultants');
        $this->db->where('consultant_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function fetchAllCategoriesData($ids) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where_in('parant_id', $ids);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->order_by('sort_order','asc');
        $query = $this->db->get();
		
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	

	public function front_end_content_news($id) {
		
        $query = $this->db->query("SELECT * FROM (`contents`) WHERE `parant_id` = ".$id." AND `pub_status` = 1 AND `version` = 0 ORDER BY STR_TO_DATE(date, '%c/%e/%Y') DESC");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_content_service($id) {
		
        $query = $this->db->query("SELECT * FROM (`contents`) WHERE `multi_category` LIKE '%".$id."%' AND `pub_status` = 1 AND `version` = 0 ORDER BY STR_TO_DATE(date, '%c/%e/%Y') DESC");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_contentNews($id,$order_by) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->order_by('pro_date', $order_by);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function news_current_month($id,$order_by) {

        $this->db->select('*');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$month = date('m');
		if ($month != ''){         
			$this->db->where('MONTH(pro_date)', $month);    
		}
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->order_by('pro_date', $order_by);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_NewsYears($id,$order_by) {
		
        $this->db->select('pro_date');
		$this->db->group_by('YEAR(pro_date)');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->distinct();
		$this->db->order_by('pro_date', $order_by);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function front_end_Events($id,$order_by,$date='') {
	$this->db->select('*');
	$this->db->from('contents');
	if($date != ''){
        $this->db->where('date >=', $date);
    }else{
        $this->db->where('date <', date('Y-m-d'));
    }
	$this->db->where('parant_id', $id);		
	$this->db->where('pub_status', 1);		
	$this->db->where('version', 0);
	$this->db->order_by('date', $order_by);
	$query = $this->db->get();
	if ($query->num_rows() > 0) {            
	return  $query->result();        
	} else {           
	return false;      
	}  
	}

	public function ListingServicesDisplayHome() {
	$this->db->select('*');
	$this->db->from('contents');
	$this->db->where('pub_status', 1);
	$this->db->where('version', 0);
	$this->db->where('display_to_home', 1);
	$query = $this->db->get();
	if ($query->num_rows() > 0) {
	return  $query->result();
	} else {
	return false;
	}
	}
	
	public function front_end_NewsMonths($id,$order_by) {
		
        $this->db->select('pro_date');
		$this->db->group_by('Month(pro_date)');
        $this->db->from('contents');
		$this->db->where('parant_id', $id);
		$this->db->where('pub_status', 1);
		$this->db->where('version', 0);
		$this->db->distinct();
		$this->db->order_by('pro_date', $order_by);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	public function getNewsByYear($year, $page_id) {
	
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $page_id);
        if ($year != ''){
            $this->db->where('YEAR(pro_date)', $year);
        }
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
		$this->db->order_by('pro_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }	
	
	public function getListingByYearnMonth($year, $month, $page_id) {
	
	$query = $this->db->query("SELECT *
								FROM `contents`
								WHERE `parant_id` =  '".$page_id."'
								AND `pub_status` =  1
								AND `version` =  0
								AND YEAR(pro_date) = '".$year."'
								AND MONTH(pro_date) = '".$month."'
								ORDER BY `pro_date` DESC");
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
		
    }

    public function getListingVacancies($clocation, $clevel, $page_id) {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $page_id);
        if ($clocation != ''){
            $this->db->where('career_location', $clocation);
        }
        if ($clevel != ''){
            $this->db->where('career_level', $clevel);
        }
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
	
    public function getActiveCompitition() {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', 391);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->row();
        } else {
            return '0';
        }
    }
		
	
	/*    
	public function get_event_attendence($id,$cat) {

        $this->db->select('SUM(no_of_reservations) as attendence');
        $this->db->from('event_registration');
        $this->db->where('event_id', $id);
        if($cat != 'all'){
			$this->db->where('status', 1);
		}
		$this->db->order_by('id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->row();
        } else {
            return '0';
        }
    } */
	
	public function get_event_attendence($event_id){
        $query = $this->db->query("SELECT r_i.* FROM `reservation_info` r_i LEFT OUTER JOIN `user_events` u_e ON r_i.user_event_id = u_e.id WHERE u_e.event_id = ".$event_id." AND r_i.attendence = '1'");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }	
		
    public function get_sold_seats_events($id) {

        $this->db->select('SUM(no_of_reservations) as sold_seats');
        $this->db->from('user_events');
        $this->db->where('event_id', $id);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->row();
        } else {
            return '0';
        }
    }
		
		
    public function check_user_event_registration($id,$event_id) {

        $this->db->select('*');
        $this->db->from('user_events');
        $this->db->where('user_id', $id);
        $this->db->where('event_id', $event_id);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
    }
		
	
  public function delete($id) {

        $query = $this->db->query("delete from contents WHERE id =" . $id);
		$query = $this->db->query("delete from contents WHERE parant_id =" . $id);
		$query = $this->db->query("delete from clientmenus WHERE page_id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    public function categoryDelete($id) {

        $query = $this->db->query("delete from categories WHERE category_id =" . $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	
  public function deleteRegistration($id) {

        $query = $this->db->query("delete from users WHERE id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteImage($photo,$imageType,$id) {

      
	   	
	  
	   $query = $this->db->query("update content_detail  set meta_value ='".$photo."' WHERE meta_key = '".$imageType."' && post_id = " . $id);
		 
		  if ($query) {
            return true;
        } else {
            return false;
        }	
	   /* $query = $this->db->query("delete from contents WHERE id =" . $id);
		$query = $this->db->query("delete from contents WHERE parant_id =" . $id);
		$query = $this->db->query("delete from clientmenus WHERE page_id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        } */
    }

 public function delete_content_detail($id) {

        $query = $this->db->query("delete from content_detail WHERE post_id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function deletePhoto($id) {
        $loggedInUserId = $this->session->userdata('id');
        $query = $this->db->query("update contents  set content_photo ='',content_updated_at='" . date('Y-m-d H:i:s') . "',content_updated_by='" . $loggedInUserId . "' WHERE id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function attendenceStatus($val, $id) {
        $loggedInUserId = $this->session->userdata('id');
        $query = $this->db->query("update reservation_info set attendence='" . $val . "' WHERE id =" . $id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
    public function updateUserPassword($user_id,$confirm_pass) {
        $loggedInUserId = $this->session->userdata('id');
        $query = $this->db->query("update users set password='" . $confirm_pass . "' WHERE id =" . $user_id);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	public function getAllCategories() {			
		$this->db->select('*');       
		$this->db->from('category');				
		$query = $this->db->get();	        
		if ($query->num_rows() > 0) 
		{            
			return $query->result();       
		} 
		else {           
			return false;      
		}    
	}
    public function getIsFlagValue($id ) {
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('type', 'page');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return 0;
        }
    }
    public function other_newsfront_end_content($id,$tpl='',$listing_level='') {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->order_by('rand()');
        $this->db->limit(4);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return  $query->result();
        } else {
            return false;
        }
    }
 

}

//end