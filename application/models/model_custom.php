<?php

class Model_custom extends CI_Model {

	function _construct() {
		// Call the Model constructor
		parent::_construct();
	}
	public function getid() {

		$this->db->select('*');
		$this->db->from('products_category');
		$query = $this->db->get();
		if ($query->num_rows() >0) {
			$data=$query->result_array();
			return $id=$data[0]['id'];
		} else {
			return 0;
		}

	}

    public function getCountryName($id) {

        $this->db->select('eng_country_name');
        $this->db->from('countries');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $data = $query->row();
            return $data;
        } else {
            return false;
        }
    }

    public function getCatName($id) {

        $this->db->select('*');
        $this->db->from('products_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $data = $query->row();
            return $data;
        } else {
            return false;
        }
    }
	
	
    public function getCatalogCatName($id) {

        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $data = $query->row();
            return $data;
        } else {
            return false;
        }
    }	


    public function getSubCatName($id) {

        $this->db->select('subcateg_title,arb_name,chn_name');
        $this->db->from('sub_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $data = $query->result_array();
        } else {
            return false;
        }
    }
	
    public function getSubCatalogName($id) {

        $this->db->select('subcateg_title,arb_name,chn_name');
        $this->db->from('sub_categories');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $data = $query->result_array();
        } else {
            return false;
        }
    }	



	public function fetchAllCategories() {

		$this->db->select('*');
		$this->db->from('products_category');
        $this->db->where('pub_status','1');
        $this->db->order_by('sort_col','ASC');
		$query = $this->db->get();
		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}

	}
	
	
	public function fetch_CatalogCats() {

		$this->db->select('*');
		$this->db->from('categories');
        $this->db->where('pub_status','1');
        $this->db->order_by('sort_col','ASC');
		$query = $this->db->get();
		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}

	}	

	public function fetchsubCategories($id){

		$this->db->select('*');
		$this->db->from('sub_category');
		$this->db->where('categ_id',$id);
		$this->db->where('pub_status','1');
        $this->db->order_by('sort_col','ASC');		
		$query = $this->db->get();

		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}

	}
	
	public function fetchCatalogSubCats($id){

		$this->db->select('*');
		$this->db->from('sub_categories');
		$this->db->where('categ_id',$id);
		$this->db->where('pub_status','1');
        $this->db->order_by('sort_col','ASC');
		$query = $this->db->get();

		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}

	}	
	
	


	public function fetchRow($id){
		$this->db->select('*');
		$this->db->from('products_category');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if ($query->num_rows()==1) {
			return $query->row();
		} else {
			return false;
		}
	}

        public function fetchRowSub($id){
		$this->db->select('*');
		$this->db->from('sub_category');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if ($query->num_rows()==1) {
			return $query->row();
		} else {
			return false;
		}
	}


        public function fetchProducts(){

		$this->db->select('*');
		$this->db->from('products');
                $this->db->order_by('sort_col','ASC');
		$query = $this->db->get();
                //echo $query;exit;
		if ($query->num_rows() >0) {
			return $query->result_array();
		} else {
			return false;
		}

	}

        public function fetchProductsName($pro_ID){

		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('id',$pro_ID);
		$query = $this->db->get();
		if ($query->num_rows()==1) {
			return $query->row();
		} else {
			return false;
		}
	}




	public function getSocailLinks($id){

		$this->db->select('*');
		$this->db->from('social_links');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if ($query->num_rows()==1) {
			return $query->row();
		} else {
			return false;
		}
	}

        public function fetchContactSubject() {
		$this->db->select('*');
		$this->db->from('email_subjects');
		$this->db->where('sub_pub_status',1);
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		if ($query->num_rows() >0) {
                    return $query->result_array();
		} else {
			return false;
		}

	}

	public function fetchSubTitle($id){

		$this->db->select('*');
		$this->db->from('email_subjects');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if ($query->num_rows()==1) {
			return $query->row();
		} else {
			return false;
		}
	}

    public function fetchLastupdated($table,$col) {
        $this->db->select("$col");
        $this->db->from($table);
        $this->db->order_by("$col","DESC");
        $this->db->limit("1");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


	public function fetchConfig(){

		$this->db->select('*');
		$this->db->from('configuration');
                $this->db->order_by('id','DESC');
                $this->db->limit (1);
		$query = $this->db->get();
		if ($query->num_rows()==1) {
			return $query->row();
		} else {
			return false;
		}
	}
	
	public function getAllCountries()
    {
		if($this->session->userdata('lang') != ''){
			$lang = $this->session->userdata('lang');
		}
		else{
			$lang = 'eng';
		}
        
		$this->db->select('*');
        $this->db->from('countries');
        $this->db->order_by($lang.'_country_name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }

    public function getCities($code)
    {
        $lang = $this->session->userdata('lang');
        $this->db->select('*');
        $this->db->from('city');
        $this->db->where('countrycode', $code);
        $this->db->order_by($lang.'_name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }
	public function getCitiesForCountry($country)
    {
        $lang = $this->session->userdata('lang');
        $this->db->select($lang.'_city');
        $this->db->from('contents');
        $this->db->where($lang.'_country', $country);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('type', 'page');
        $this->db->order_by($lang.'_city', 'ASC');
        $this->db->group_by($lang.'_city');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }
	public function getCountryNameById($id) {
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('code', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCityNameById($id) {
        $this->db->select('*');
        $this->db->from('city');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function getUserData($id)
    {
        $this->db->select('*');
        $this->db->from('metro_staff');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }

    }
    function saveFormData($data,$tb) {
        $this->db->set($data);
        $this->db->insert($tb);
        $insertId=$this->db->insert_id();
        if($insertId>0)  {
            return $insertId;
        }  else{
            return false;
        }
    }
	public function fetchAll_Ids($table) {
        $query = $this->db->query("select id from $table");
        $result = $query->result();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }
	
	public function fetch_single($id,$table) {
        $query = $this->db->query("select * from $table where id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }

	public function fetch_single_reg($id,$table,$status) {
        $query = $this->db->query("select * from $table where is_profile_createad = ".$status." AND id=".$id);
        $result = $query->row();
        if(!empty($result))
        {
            return $result;
        }else{
            return false;
        }
    }

}//end