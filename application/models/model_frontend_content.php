<?php

class Model_frontend_content extends CI_Model
{

    function _construct()
    {
        parent::_construct();
    }

    function SaveUserInfo($user)
    {

        $this->db->set($user);
        $this->db->insert('users');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {

            return $insertId;

        } else {

            return false;
        }

    }

    public function getUserDataById($userId) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function checkEmail($email) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }


    function saveContactUs($data)
    {
        $this->db->set($data);
        $this->db->insert('contact_forms');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }

    }
	/*Courses functions */
	function saveCourse($data)
    {
        $this->db->set($data);
        $this->db->insert('courses');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }

    }
	public function fetchAllCourses()
    {
        $this->db->select('*');
        $this->db->from('courses');
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }
	public function fetch_CourseRow($id)
    {
        $this->db->select('*');
        $this->db->from('courses');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function deleteCourse($id)
    {
        $this->db->query("delete from courses where id=" . $id);
    }
	/*Courses functions */
    public function fetchAllrows()
    {
        $this->db->select('*');
        $this->db->from('contact_forms');
        $this->db->order_by('created_at', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }

    public function getAllCountries()
    {
        $lang = $this->session->userdata('lang');
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->order_by($lang.'_country_name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }

    public function getCities($code)
    {
        $lang = $this->session->userdata('lang');
        $this->db->select('*');
        $this->db->from('city');
        $this->db->where('countrycode', $code);
        $this->db->order_by($lang.'_name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }


    public function getCitiesForCountry($country)
    {
        $lang = $this->session->userdata('lang');
        $this->db->select($lang.'_city');
        $this->db->from('contents');
        $this->db->where($lang.'_country', $country);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('type', 'page');
        $this->db->order_by($lang.'_city', 'ASC');
        $this->db->group_by($lang.'_city');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }

    public function fetchRow($id)
    {
        $this->db->select('*');
        $this->db->from('contact_forms');
        $this->db->where('id', $id);
        $this->db->order_by('created_at', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function delete($id)
    {
        $this->db->query("delete from contact_forms where id=" . $id);
    }

    public function getFeaturedCoursesBySearchAtHome($data, $lang)
    {
        $type = $data['type'];
        $category = $data['category'];
        $country = $data['country'];
		$instructors = $data['instructors'];
        $insitutes = $data['insitutes'];
        $city = $data['city'];
        $parent_id = $data['parent_id'];
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('show_as_featured', 1);
        $this->db->where('parant_id', $parent_id);
        $this->db->where('type', $type);
        if ($country != ''){
            $this->db->where('eng_country', $country);
        }
        if ($category != ''){
            $this->db->where("find_in_set('".$category."',course_field) <> 0");
        }
        if ($city != ''){
            $this->db->where('eng_city', $city);
        }
		if ($instructors != ''){
            $this->db->where($lang.'_instructor', $instructors);
        }
		if ($insitutes != ''){
            $this->db->where($lang.'_insitute', $insitutes);
        }
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $arr = $query->result();
            return $arr;
        } else {
            return false;
        }
    }
	
	 public function getCourseNameById($id) {
        $this->db->select('*');
        $this->db->from('fields');
        $this->db->where('id', $id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getAllCoursesBySearchAtHome($data, $lang)
    {
        $type = $data['type'];
        $category = $data['category'];
        $country = $data['country'];
		$instructors = $data['instructors'];
        $insitutes = $data['insitutes'];
        $city = $data['city'];
        $parent_id = $data['parent_id'];
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('parant_id', $parent_id);
        $this->db->where('type', $type);
        if ($country != ''){
            $this->db->where('eng_country', $country);
        }
        if ($category != ''){
            $this->db->where("find_in_set('".$category."',course_field) <> 0");
        }
        if ($city != ''){
            $this->db->where('eng_city', $city);
        }
		if ($instructors != ''){
            $this->db->where($lang.'_instructor', $instructors);
        }
		if ($insitutes != ''){
            $this->db->where($lang.'_insitute', $insitutes);
        }
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $arr = $query->result();
            return $arr;
        } else {
            return false;
        }
    }

    public function getNewsListing($id)
    {
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->order_by('pro_date', 'desc');
        $this->db->group_by('pro_date');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /*public function getNewsByDate($date, $id)
    {
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->order_by('pro_date', 'desc');
        $this->db->group_by('pro_date');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }*/

    public function getCountryNameById($id) {
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('code', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCityNameById($id) {
        $this->db->select('*');
        $this->db->from('city');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	public function getCitiesByContriesEms($cid) {
        $this->db->select('*');
        $this->db->from('cms_cities');
        $this->db->where('id', $cid);
        $this->db->where('status', '1');
        $this->db->group_by('id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }
    }
	
	

	
	public function getAllCoursesBySearch($data, $lang='eng')
    {
        $type = $data['type'];
        $category = $data['category'];
        $country = $data['country'];
        $city = $data['city'];
		$instructors = $data['instructors'];
        $insitutes = $data['insitutes'];
        $date_from = $data['date_from'];
        $date_to = $data['date_to'];
        $parent_id = $data['parent_id'];
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('parant_id', $parent_id);
        $this->db->where('type', $type);
        //$this->db->where("from_date BETWEEN $date_from AND $date_to");
        if ($country != ''){
            $this->db->where('eng_country', $country);
        }
        if ($category != ''){
            $this->db->where("find_in_set('".$category."',course_field) <> 0");
        }
        if ($city != ''){
            $this->db->where('eng_city', $city);
        }
		if ($instructors != ''){
            $this->db->where($lang.'_instructor', $instructors);
        }
		if ($insitutes != ''){
            $this->db->where($lang.'_insitute', $insitutes);
        }
		if ($date_from != '' && $date_to != ''){
			$this->db->where("from_date BETWEEN $date_from AND $date_to");
            //$this->db->where('from_date >=', $date_from);
            //$this->db->where('to_date <=', $date_to);
		}else if ($date_from != '' && $date_to == ''){
            $this->db->where('from_date >=', $date_from);
        }else if ($date_from == '' && $date_to != ''){
            $this->db->where('to_date <=', $date_to);
        }
        $this->db->order_by('from_date', 'asc');
        $query = $this->db->get();
        //echo $this->db->last_query();
        //exit();
        if ($query->num_rows() > 0) {
            $arr = $query->result();
            return $arr;
        } else {
            return false;
        }
    }

	public function getAllDiplomaBySearch($data, $lang='eng')
    {
        $type = $data['type'];
        $category = $data['category'];
        $country = $data['country'];
        $city = $data['city'];
		$instructors = $data['instructors'];
        $insitutes = $data['insitutes'];
        $date_from = $data['date_from'];
        $date_to = $data['date_to'];
        $parent_id = $data['parent_id'];
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('parant_id', $parent_id);
        $this->db->where('type', $type);
        //$this->db->where("from_date BETWEEN $date_from AND $date_to");
        if ($country != ''){
            $this->db->where('eng_country', $country);
        }
        if ($category != ''){
            $this->db->where("find_in_set('".$category."',diploma_field) <> 0");
        }
        if ($city != ''){
            $this->db->where('eng_city', $city);
        }
		if ($instructors != ''){
            $this->db->where($lang.'_instructor', $instructors);
        }
		if ($insitutes != ''){
            $this->db->where($lang.'_insitute', $insitutes);
        }
		if ($date_from != '' && $date_to != ''){
			$this->db->where("from_date BETWEEN $date_from AND $date_to");
            //$this->db->where('from_date >=', $date_from);
            //$this->db->where('to_date <=', $date_to);
		}else if ($date_from != '' && $date_to == ''){
            $this->db->where('from_date >=', $date_from);
        }else if ($date_from == '' && $date_to != ''){
            $this->db->where('to_date <=', $date_to);
        }
        $this->db->order_by('from_date', 'asc');
        $query = $this->db->get();
        //echo $this->db->last_query();
        //exit();
        if ($query->num_rows() > 0) {
            $arr = $query->result();
            return $arr;
        } else {
            return false;
        }
    }

    public function calendarSearch($data)
    {
        $type = $data['type'];
        $date_from = $data['date_from'];
        $parent_id = $data['parent_id'];
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('parant_id', $parent_id);
        $this->db->where('type', $type);
        //$this->db->where("from_date BETWEEN $date_from AND $date_to");
        if ($date_from){
            $this->db->where('from_date', $date_from);
        }
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $arr = $query->result();
            return $arr;
        } else {
            return false;
        }
    }


    public function checkIfCourseExist($date, $parent_id)
    {
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('parant_id', $parent_id);
        $this->db->where('type', 'page');
        //$this->db->where("from_date BETWEEN $date_from AND $date_to");
        $this->db->where("(from_date <= '". $date."' AND to_date >= '". $date."')");
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function checkIfUsersCourseExist($date, $parent_id)
    {
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('parant_id', $parent_id);
        $this->db->where('type', 'page');
        //$this->db->where("from_date BETWEEN $date_from AND $date_to");
        $this->db->where("(from_date <= '". $date."' AND to_date >= '". $date."')");
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }


    public function countCourses($data)
    {
        $type = $data['type'];
        $date_from = $data['date_from'];
        $parent_id = $data['parent_id'];
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('parant_id', $parent_id);
        $this->db->where('type', $type);
        //$this->db->where("from_date BETWEEN $date_from AND $date_to");
        if ($date_from){
            $this->db->where('from_date', $date_from);
        }
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $arr = $query->num_rows();
            return $arr;
        } else {
            return false;
        }
    }

    public function getHomeCourseListing($id)
    {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where('clickable_at_home', 1);
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getFile($id)
    {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function SaveCorporateUserCourses($course)
    {

        $this->db->set($course);
        $this->db->insert('user_courses');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {

            return $insertId;

        } else {

            return false;
        }

    }


    public function getCoursesByCategory($id, $category)
    {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where("find_in_set('".$category."',course_field) <> 0");
        //$this->db->where("(eng_industry = '".$category."' OR arb_industry = '".$category."')");
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get();
        /*echo $this->db->last_query();
        exit();*/
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	
	public function getDiplomasByCategory($id, $category)
    {

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('parant_id', $id);
        $this->db->where('pub_status', 1);
        $this->db->where('version', 0);
        $this->db->where("find_in_set('".$category."',diploma_field) <> 0");
        //$this->db->where("(eng_industry = '".$category."' OR arb_industry = '".$category."')");
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get();
        /*echo $this->db->last_query();
        exit();*/
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function UpdateUserInfo($data, $id)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('users', $data);
        return ($query > 0) ? true : false;
    }

    function deleteUserCourses($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user_courses');
    }

    function saveAppliedCourses($data)
    {	if(isset($data['type'])){
			unset($data['type']);
		}
        $this->db->set($data);
        $this->db->insert('user_applied_courses');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }

    }
	function saveAppliedDiplomas($data)
    {
        $this->db->set($data);
        $this->db->insert('user_applied_diplomas');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }

    }
	function updateApplyStatusDiploma($data, $id)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('user_applied_diplomas', $data);
        return ($query > 0) ? true : false;
    }

    public function getCourseDetail($id) {
        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where('id', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function updateApplyStatus($data, $id)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('user_applied_courses', $data);
        return ($query > 0) ? true : false;
    }

    public function getCourseAppliedForCurrentUser($id) {
        $this->db->select('*');
        $this->db->from('user_applied_courses');
        $this->db->where('user_id', $id);
        $this->db->where('status', '1');
        $this->db->group_by('course_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getUpcomingSessions($id) {
        $this->db->select('*');
        $this->db->from('user_applied_courses');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function checkIfUserAppliedForCourse($user_id, $course_id) {
        $this->db->select('*');
        $this->db->from('user_applied_courses');
        $this->db->where('user_id', $user_id);
        $this->db->where('course_id', $course_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
	public function checkIfUserAppliedForDiploma($user_id, $diploma_id) {
        $this->db->select('*');
        $this->db->from('user_applied_diplomas');
        $this->db->where('user_id', $user_id);
        $this->db->where('diploma_id', $diploma_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getCorporateCourses($id) {
        $this->db->select('*');
        $this->db->from('user_courses');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function updateUserStatus($data, $id)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('users', $data);
        return ($query > 0) ? true : false;
    }
	
	function SaveRequestCourses($data)
    {

        $this->db->set($data);
        $this->db->insert('request_courses');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {

            return $insertId;

        } else {

            return false;
        }

    }
	
	public function requestCourses()
    {
        $this->db->select('*');
        $this->db->from('request_courses');
        $this->db->order_by('date', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }
	
	public function fetchCourse($id)
    {
        $this->db->select('*');
        $this->db->from('request_courses');
        $this->db->where('id', $id);
        $this->db->order_by('date', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }
	
	public function delete_course($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('request_courses'); 
	}

    public function getAllCourseFields()
    {
        $lang = $this->session->userdata('lang');
        $this->db->select('*');
        $this->db->from('fields');
        $this->db->where('status', '1');

       $this->db->order_by('order', 'ASC');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }

    public function getSingleCourseFieldInfo($id)
    {
        $lang = $this->session->userdata('lang');
        $this->db->select('*');
        $this->db->from('fields');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }

    public function getAllCountriesEms()
    {
        $this->db->select('*');
        $this->db->from('cms_countries');
        $this->db->where('status', '1');
        $this->db->group_by('id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }

    public function getCitiesForCountryEms($cid)
    {
        $this->db->select('*');
        $this->db->from('cms_cities');
        $this->db->where('country_id', $cid);
        $this->db->where('status', '1');
        $this->db->group_by('id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }
	
	//Kashif
	public function getCitiesEms($cid)
    {
        $this->db->select('*');
        $this->db->from('cms_cities');
        $this->db->where('country_id', $cid);
        $this->db->where('status', '1');
        $this->db->group_by('id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }
	
	

    public function getSingleCityData($id)
    {
        $this->db->select('*');
        $this->db->from('cms_cities');
        $this->db->where('id', $id);
        $this->db->group_by('id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }

    public function getSingleCountryData($id)
    {
        $this->db->select('*');
        $this->db->from('cms_countries');
        $this->db->where('id', $id);
        $this->db->group_by('id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }
	public function getUserType($id)
    {
        $this->db->select('page_type');
        $this->db->from('contents');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }

    }
}