<?php
require(APPPATH . 'third_party/Unifonic.php');

use \Unifonic\API\Client;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function getCurrentLanguage(){
	$CI = & get_Instance();
	$lang = $CI->session->userdata('lang');
	return $lang;
}

function removeJsScripts($text){
	if($text != '' && !is_numeric($text)){
		if (!is_numeric($text)) {
			$text1 = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $text);
		}
		if (strpos($text1, '<script>') !== false) {
			$text1 = '';
		}
		$text = $text1;
		if($text == ''){
			$text = '-';
		}
		return str_replace('\\', '', $text);
	}
	else{
		return str_replace('\\', '', $text);
	}
}
function imageSetDimenssion($src, $width, $height, $type = 1)
{
    $image = base_url() . 'assets/script/mlib-uploads/timthumb.php?src=' . base_url() . 'assets/script/' . $src . '&w=' . $width . '&h=' . $height . '&zc=' . $type;
    return $image;
}
function getPagePath($id,$lang)
{
   $html = '';
   $pageDetails = get_content_data($id);
   
   if($pageDetails->parant_id > 0){
	   $pageDetailsParent = get_content_data($pageDetails->parant_id);
	   $html .= pageTitle($pageDetailsParent->id,$lang) . ' - ';
   }
   
   $html .= pageTitle($id,$lang) . ' - ';
   return trim($html,' - ');
}

function sendSMS($phone_No, $message)
{
    $client = new Client();
    $result = $client->Messages->SendBulkMessages($phone_No, $message, 'eDesign');

    $response = json_decode($result);

    $msg = $response->Messages;
    if ($msg[0]->MessageID) {
        return true;
    } else {
        return false;
    }
}

//load more news
function ListingContentNewsMedia($page_id, $tpl)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_content_news_media($page_id, $tpl);
}

function convertYoutube($string)
{
    return preg_replace(
        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
        "<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/$2?autoplay=1\" replace_style allowfullscreen></iframe>",
        $string
    );
}

function testinggg($subject, $msg, $yr_email, $fr_email)
{
    $post_data = array_filter($post_data);
    $CI = &get_Instance();
    $CI->load->model('ems/model_configuration');
    $conf = $CI->model_configuration->fetchRow();

    $lang = $CI->session->userdata('lang');
    $name = '';
    if (isset($post_data['name']))
        $name = $post_data['name'];
    else
        $name = $post_data['full_name'];

    if ($lang == 'eng') {
        if ($name)
            $emailData['message'] = "Hi " . $name . ", <br><br> " . $msg;
        else
            $emailData['message'] = $msg;
        $emailData['title'] = $subject;
        $emailData['info'] = $post_data;
    } else {
        if ($name)
            $emailData['message'] = "مرحبا  " . $name . ", <br><br> " . $msg;
        else
            $emailData['message'] = $msg;
        $emailData['title'] = $subject;
        $emailData['info'] = $post_data;
    }


    if ($lang == 'eng') {
        $body = $CI->load->view('layouts/emails/arb_general_email', $emailData, TRUE);
    } else {
        $body = $CI->load->view('layouts/emails/arb_general_email', $emailData, TRUE);
    }

    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'localhost', 'smtp_port' => '25',
        'smtp_user' => 'info@almurjan.ced.sa', 'smtp_pass' => 'nZMvV0y8Xoj6', 'smtp_crypto' => 'ssl');


    $CI->load->library('email', $config);

    $CI->email->set_newline("\r\n");

    $CI->email->from('info@almurjan.ced.sa', 'Zamil OffShore');

    $CI->email->to('mohsin@astutesol.com');

    $CI->email->subject('Althuraya Award');

    $CI->email->set_mailtype("html");

    $CI->email->message($body);

    if ($CI->email->send()) {
        echo 'here in if ' . '<br>';
    } else {
        echo 'here in else ' . '<br>';
    }

    echo $CI->email->print_debugger();

    exit;

}

function send_sms($number, $message)
{
    $from_name = 'MB-Thuraya';
    $client = new Client();
    $response = $client->Messages->Send($number, $message, $from_name); // send regular massage
    return $response;
}

function changeNameToArb($title, $lang)
{
    $CI =& get_Instance();
    if ($lang == "eng") {
        $title = $title;
    } else {

        if ($title == "email") {
            $title = "البريد الاكتروني";
        }
        if ($title == "name") {
            $title = "الإسم";
        }
        if ($title == "dob") {
            $title = "تاريخ الميلاد";
        }
        if ($title == "occupation") {
            $title = "المهنة";
        }
        if ($title == "intro") {
            $title = "المقدمة";
        }
        if ($title == "description") {
            $title = "الإنجازات";
        }
        if ($title == "summary") {
            $title = "الصعوبات";
        }
        if ($title == "impact_level") {
            $title = "مستوى التأثير";
        }
        if ($title == "creativity") {
            $title = "الإبداع";
        }
        if ($title == "curriculum") {
            $title = "السيرة الذاتية";
        }
        if ($title == "link") {
            $title = "الرابط";
        }
        if ($title == "comments" || $title == "comments") {
            $title = "تعليقات";
        }
        if ($title == "message" || $title == "Message") {
            $title = "رسالة";
        }
        if ($title == "mobile_no" || $title == "phone_no" || $title == "mobile") {
            $title = "رقم الهاتف";
        }
        if ($title == "Password" || $title == "password") {
            $title = "كلمه السر";
        }
        if ($title == "cat_id" || $title == "Cat_id" || $title == "category" || $title == "Category") {
            $title = "فئة المسابقة";
        }
        if ($title == "c_v" || $title == "C_v" || $title == "C_V") {
            $title = "السيرة الذاتية";
        }
        if ($title == "organization_name") {
            $title = "اسم المنظمة";
        }
        if ($title == "job_title") {
            $title = "المسمى الوظيفي";
        }
        if ($title == "first_name") {
            $title = "الاسم الاول";
        }
        if ($title == "last_name") {
            $title = "الكنية";
        }
        if ($title == "email") {
            $title = "البريد الإلكتروني";
        }
        if ($title == "phone") {
            $title = "رقم الهاتف";
        }
        if ($title == "city") {
            $title = "مدينة";
        }
        if ($title == "country") {
            $title = "بلد'";
        }
        if ($title == "product_dropdown") {
            $title = "الخدمة المطلوبة";
        }
        if ($title == "product_service") {
            $title = "الخدمة المطلوبة";
        }
        if ($title == "contactpurpose") {
            $title = "الغرض الاتصال";
        }
        if ($title == "full_name") {
            $title = "الاسم بالكامل";
        }
        if ($title == "gender") {
            $title = "نوع";
        }
        if ($title == "education") {
            $title = "التعليم";
        }
        if ($title == "major") {
            $title = "تخصص";
        }
        if ($title == "nationality") {
            $title = "جنسية";
        }
        if ($title == "experience") {
            $title = "سنوات الخبرة";
        }
        if ($title == "job_id") {
            $title = "المسمي الوظيفي";
        }
        if ($title == "professional_certification_") {
            $title = "شهادة احترافية";
        }
        if ($title == "f_name") {
            $title = "الاسم الاول";
        }
        if ($title == "f_name") {
            $title = "اسم الاب";
        }
        if ($title == "m_name") {
            $title = "الاسم الاول";
        }
        if ($title == "l_name") {
            $title = "اللقب";
        }
        if ($title == "age") {
            $title = "العمر";
        }
        if ($title == "mob_no") {
            $title = "رقم الجوال";
        }
        if ($title == "c_residence") {
            $title = "بلد الإقامة";
        }
        if ($title == "ct_residence") {
            $title = "مدينة الإقامة";
        }
        if ($title == "education_level") {
            $title = "المستوى التعليمي";
        }
        if ($title == "specialization") {
            $title = "التخصص";
        }
        if ($title == "institute_name") {
            $title = "اسم الجامعة/المؤسسة التعليمية";
        }
        if ($title == "last_job") {
            $title = "المسمى الوظيفي في اخر وظيفة";
        }
        if ($title == "experience") {
            $title = "إجمالي سنوات الخبرة العملية";
        }
        if ($title == "last_employees") {
            $title = "اسم اخر ثلاثة شركات عملت بها";
        }
        if ($title == "eng_prof") {
            $title = "مستوى اللغة الإنجليزية";
        }
        if ($title == "additional_info") {
            $title = "أي معلومات اضافية أخرى";
        }
        if ($title == "saudi_comm") {
            $title = "هل انت مرخص من الهيئة السعودية للتخصصات الصحية";
        }
        if ($title == "any_other_Cert") {
            $title = "هل تحمل شهادات مهنية أخرى: بورد، زمالة، تدريب متخصص الخ، يرجى تحديدها";
        }
    }
    return ucfirst(str_replace('_', ' ', $title));
}

function userGeneralEmailNewsletter($post_data, $subject, $msg, $lang)
{
    $post_data = array_filter($post_data);

    $CI = &get_Instance();
    $conf = $CI->model_configuration->fetchRow();
    $lang = $CI->session->userdata('lang');
    //$lang = $CI->session->userdata('lang');
    if (isset($post_data['name']))
        $name = $post_data['name'];
    else
        $name = $post_data['full_name'];

    if ($lang == 'eng') {
        $emailData['message'] = $msg;
        $emailData['title'] = $subject;
    } else {
        $emailData['message'] = $msg;
        $emailData['title'] = $subject;
    }


    if ($lang == 'eng') {
        $body = $CI->load->view('layouts/emails/news_letter', $emailData, TRUE);
    } else {
        $body = $CI->load->view('layouts/emails/news_letter', $emailData, TRUE);
    }
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => $conf->smtp_host, 'smtp_port' => $conf->smtp_port,
        'smtp_user' => $conf->smtp_email, 'smtp_pass' => $conf->smtp_password);
    $CI->load->library('email', $config);
    $CI->email->set_newline("\r\n");
    $CI->email->from($conf->smtp_email, 'Althuraya Awards');
    $CI->email->to($post_data['email']);
    $CI->email->subject('Althuraya Awards');
    $CI->email->set_mailtype("html");
    if ($lang == 'eng') {
        $CI->email->message($body);
    } else {
        $CI->email->message($body);
    }
    $CI->email->send();

    return true;

}

function userGeneralEmail($post_data, $subject, $msg, $lang,$hideSocialLinks = 0)
{

    $post_data = array_filter($post_data);
    $CI = &get_Instance();
    $CI->load->model('ems/model_configuration');
    $conf = $CI->model_configuration->fetchRow();

    //$lang = $CI->session->userdata('lang');
    if (isset($post_data['name']))
        $name = $post_data['name'];
    else
        $name = $post_data['full_name'];

    if ($lang == 'eng') {
        $emailData['message'] = "Hi " . $name . ", <br><br> " . $msg;
        $emailData['title'] = $subject;
        $emailData['info'] = $post_data;
        $emailData['hideSocialLinks'] = $hideSocialLinks;
    } else {
        $emailData['message'] = "مرحبا  " . $name . ", <br><br> " . $msg;
        $emailData['title'] = $subject;
        $emailData['info'] = $post_data;
		$emailData['hideSocialLinks'] = $hideSocialLinks;
    }
    if ($lang == 'eng') {
        $body = $CI->load->view('layouts/emails/eng_general_email', $emailData, TRUE);
    } else {
        $body = $CI->load->view('layouts/emails/arb_general_email', $emailData, TRUE);
    }
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => $conf->smtp_host, 'smtp_port' => $conf->smtp_port,
        'smtp_user' => $conf->smtp_email, 'smtp_pass' => $conf->smtp_password);

    $CI->load->library('email');
    $CI->email->initialize($config);
    $CI->email->set_newline("\r\n");
    $CI->email->from($conf->smtp_email, 'Almurjan Group');
    $CI->email->to($post_data['email']);
    $CI->email->subject('Almurjan Group');
    $CI->email->set_mailtype("html");
    $CI->email->message($body);
    $CI->email->send();

   /*echo $CI->email->print_debugger();
    exit;*/

    return true;

}


function userGeneralEmailShare($subject, $msg, $yr_email, $fr_email, $lang)
{

    $post_data = array_filter($post_data);
    $CI = &get_Instance();
    $CI->load->model('ems/model_configuration');
    $conf = $CI->model_configuration->fetchRow();

    //$lang = $CI->session->userdata('lang');
    if (isset($post_data['name']))
        $name = $post_data['name'];
    else
        $name = $post_data['full_name'];

    if ($lang == 'eng') {
        $emailData['message'] = "Hi " . $name . ", <br><br> " . $msg;
        $emailData['title'] = $subject;
        $emailData['info'] = $post_data;
    } else {
        $emailData['message'] = "مرحبا  " . $name . ", <br><br> " . $msg;
        $emailData['title'] = $subject;
        $emailData['info'] = $post_data;
    }

    if ($lang == 'eng') {
        $body = $CI->load->view('layouts/emails/eng_general_email', $emailData, TRUE);
    } else {
        $body = $CI->load->view('layouts/emails/arb_general_email', $emailData, TRUE);
    }

    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => $conf->smtp_host, 'smtp_port' => $conf->smtp_port,
        'smtp_user' => $conf->smtp_email, 'smtp_pass' => $conf->smtp_password);

    $CI->load->library('email');
    $CI->email->initialize($config);
    $CI->email->set_newline("\r\n");
    $CI->email->from($conf->smtp_email, 'Aljazira Capital');
    $CI->email->to($fr_email);
    $CI->email->subject('Aljazira Capital');
    $CI->email->set_mailtype("html");
    $CI->email->message($body);
    $CI->email->send();
    /*
    echo $CI->email->print_debugger();
    exit;
     */
    return true;

}

function adminGeneralEmail($post_data, $subject, $msg, $lang, $type,$hideSocialLinks = 0)
{

    $post_data = array_filter($post_data);
    $CI = &get_Instance();
    $message = $CI->lang->line('all');
    $CI->load->model('ems/model_configuration');

    $conf = $CI->model_configuration->fetchRow();


    //$lang = $CI->session->userdata('lang');
    if (isset($post_data['name']))
        $name = $post_data['name'];
    else
        $name = $post_data['full_name'];

    if ($lang == 'eng') {
        $emailData['message'] = "Hi Admin, <br><br> " . $msg;
        $emailData['title'] = $subject;
        $emailData['info'] = $post_data;
        $emailData['hideSocialLinks'] = $hideSocialLinks;
    } else {
        $emailData['message'] = "مرحبا الادارية, <br><br> " . $msg;
        $emailData['title'] = $subject;
        $emailData['info'] = $post_data;
		$emailData['hideSocialLinks'] = $hideSocialLinks;
    }

    if ($lang == 'eng') {
        $body = $CI->load->view('layouts/emails/eng_general_email', $emailData, TRUE);
    } else {
        $body = $CI->load->view('layouts/emails/arb_general_email', $emailData, TRUE);
    }

    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => $conf->smtp_host, 'smtp_port' => $conf->smtp_port,
        'smtp_user' => $conf->smtp_email, 'smtp_pass' => $conf->smtp_password);

    $CI->load->library('email');
    $CI->email->initialize($config);
    $CI->email->set_newline("\r\n");
    $CI->email->from($conf->smtp_email, 'Almurjan Group');

    if ($type == 'careers') {
        $CI->email->to($conf->career_email);
    } elseif ($type == 'request_quotation') {
        $CI->email->to($conf->request_email);
    } elseif ($type == 'contact') {
        $CI->email->to($conf->contact_email);
    } elseif ($type == 'med') {
        $CI->email->to('mohsin@astutesol.com');
    } else {
        $CI->email->to($conf->sendinquiry_email);
    }

    $CI->email->subject('Almurjan Group');
    $CI->email->set_mailtype("html");
    $CI->email->message($body);
    $CI->email->send();
    /*
    echo $CI->email->print_debugger();
    exit;
    */
    return true;

}


function getSingleLabel($label_id)
{
    $CI = &get_Instance();
    $CI->load->model('model_clientmenus');
    $result_array = $CI->model_clientmenus->fetchRow($label_id);

    $segment1 = $CI->session->userdata('lang');
    if ($segment1 == '') {
        $result = $result_array->Title_en;
    } elseif ($segment1 == 'arb') {
        $result = $result_array->Title_ar;
    } elseif ($segment1 == 'chn') {
        $result = $result_array->Title_ch;
    } else {
        $result = $result_array->Title_en;
    }
    return $result;

}

function getSubAdminRatingOnApplication($reg_id, $sub_admin_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_user_profiles');
    return $CI->model_user_profiles->getSubAdminRatingOnApplication($reg_id, $sub_admin_id);
}

function getSubAdminRatingOnProfile($reg_id, $sub_admin_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_user_profiles');
    return $CI->model_user_profiles->getSubAdminRatingOnProfile($reg_id, $sub_admin_id);
}

function getCategoryName($categor_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->fetch_category($categor_id);
}

function getEventName($user_event_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->fetch_event_name($user_event_id);
}

function getCity()
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_city');
    return $CI->model_city->fetch_c();
}

function getEventDates($event_id)
{
    $edit = false;

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $dates = $CI->model_contents->fetch_event_dates($event_id);

    $event_dates = '<div class="uk-width-medium-1-2">
				<label>Event Dates</label>';
    foreach ($dates as $date) {
        if ($date->event_date != '0000-00-00') {
            $event_dates .= '
		
				<div class="uk-grid form_section" id="" style="display:block;" data-uk-grid-margin>
					<div class="uk-width-large-1-3 uk-width-medium-1-2">
						<div class="uk-input-group">
							<label for="uk_dp_1">Select date</label>
							<input class="md-input" name="event_date[]" type="text" id="uk_dp_1" data-uk-datepicker="{format:\'YYYY-MM-DD\'}" value="' . $date->event_date . '">
						</div>
					</div>
					
					<div class="uk-width-large-1-3 uk-width-medium-1-2">
						<div class="uk-input-group">
							<label for="uk_tp_1">Start time</label>
							<input class="md-input" name="start_time[]" type="text" id="uk_tp_1" value="' . $date->start_time . '" data-uk-timepicker>
						</div>
					</div>
					
					<div class="uk-width-large-1-3 uk-width-medium-1-2">
						<div class="uk-input-group">
							<label for="uk_tp_2">End time</label>
							<input class="md-input" name="end_time[]" type="text" id="uk_tp_2" value="' . $date->end_time . '" data-uk-timepicker>
							<span class="uk-input-group-addon">
								<a href="#" class="btnSectionRemove"><i class="material-icons md-24"></i></a>
							</span>
						</div>
					</div>
				</div>';
        }
    }
    $event_dates .= '</div> <div class="uk-width-medium-1-2"></div>';

    return $event_dates;

}

function getMultipleProductCategory()
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $products = $CI->model_contents->getMultipleProductCategory();
    return $products;
    /* $selectVal = '';
    foreach($products as $product){
        $title_eng = $product->eng_title;
        $title_arb = $product->arb_title;
        $product_id = $product->id;
        if($product_id == $country->id){
            $selected = 'selected';
        }
        else{
            $selected = '';
        }
        $html_countries.= '<option value="'.$country->id.'" '.$selected.'>'.($lang == 'eng' ? $country->eng_country_name : $country->arb_country_name).'</option>';
    }
    echo $html_countries;
     */
}

function getEventAttendenceCount($id, $cat)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $file_name = $CI->model_contents->get_event_attendence($id, $cat);
    return $file_name->attendence;
}

function getSoldSeats($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $file_name = $CI->model_contents->get_sold_seats_events($id);
    return ($file_name->sold_seats ? $file_name->sold_seats : 0);
}

function CheckUserEventRegistration($id, $event_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $file_name = $CI->model_contents->check_user_event_registration($id, $event_id);
    return $file_name;
}

function getUserProfile($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $user_profile = $CI->model_contents->get_user_profile($id);
    return $user_profile;
}

function getUserInterests($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $user_interests = $CI->model_contents->get_user_interests($id);
    return $user_interests;
}

function Reservations($val)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $user_interests = $CI->model_contents->get_bookings($val);
    return $user_interests->count_users;
}

function EventReservations($val, $event_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $user_interests = $CI->model_contents->get_event_bookings($val, $event_id);
    return $user_interests->count_users;
}

function getEventAttendence($id, $cat)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $file_name = $CI->model_contents->get_event_attendence($id, $cat);
    if ($file_name[0] != '') {
        echo count($file_name);
    } else {
        echo '0';
    }
}

function checkArchieved($event_id, $current_date)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $dates = $CI->model_contents->check_archieved($event_id, $current_date);
    return $dates;
}

function getEventDatesAndTimes($event_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $dates = $CI->model_contents->fetch_event_dates($event_id);
    return $dates;
}

function ListingCalendarEvents($event_id, $eventCheckDate)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $dates = $CI->model_contents->fetch_event_dates_calendar($event_id, $eventCheckDate);
    return $dates;
}

function getPdf_file($id, $name)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_menu');
    $file_name = $CI->model_menu->getPdf_file($id, $name);
    if ($file_name == 0 || $file_name == '0') {
        $file_name = '';
    }
    return $file_name;
}

function getEventDate($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_menu');
    $date = $CI->model_menu->getDate($id);
    return $date;
}

function getPdf_file_arb($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_menu');
    $file_link = $CI->model_menu->getPdf_file_arb($id);
    return $file_link;
}

function get_pdfThumb($id, $lang)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_menu');
    $file_link = $CI->model_menu->getThumb($id, $lang);
    return $file_link;
}

function get_pdfThumb_eng($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_menu');
    $file_link = $CI->model_menu->getThumb_eng($id);
    return $file_link;
}

function get_pdfThumb_arb($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_menu');
    $file_link = $CI->model_menu->getThumb_arb($id);
    return $file_link;
}

function getDisplay_home_value($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_menu');
    $home_value = $CI->model_menu->getDisplay_home_value($id);
    return $home_value;
}

function fetchCityName($id)
{
    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');
    $CI->load->model('ems/model_city');
    $n = $CI->model_city->fetchName_c($id);
    return ($lang == 'eng' ? $n->name : $n->arb_name);
}

function getFilterCity($lang = 'eng')
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_city');
    return $CI->model_city->filter_city($lang);
}

function getPageIdbyTemplate($pageTitle)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getPageIdbyTemplate($pageTitle);

    return $result[0]->id;
}

function getPageUrl($page_id)
{
    $pageTitle = pageTitle($page_id, 'eng');

    $get_title = str_replace(' ', '_', $pageTitle);

    $result_url = lang_base_url() . 'page/' . $get_title;

    return $result_url;
}

function curPageURL($lang = 'eng')
{

    $pageURL = 'http';

    if ($_SERVER["HTTPS"] == "on") {

        $pageURL .= "s";

    }

    $pageURL .= "://";

    if ($_SERVER["SERVER_PORT"] != "80") {

        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];

    } else {

        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];

    }

    $pageURL = str_replace(array('/ar', '/en'), '', $pageURL);

    if ($lang != 'eng') {

        $pageURL = str_replace($_SERVER["SERVER_NAME"] . "/nesma-partners", $_SERVER["SERVER_NAME"] . "/nesma-partners" . "/" . $lang, $pageURL);

    }

    return $pageURL;

}


/**
 * getSelected()
 */
function getSelected($row, $status)
{


    if ($row == $status) {
        return 'selected="selected"';
    }
}

function getSliderImages($page_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $dataObject = array();

    foreach ($CI->model_contents->front_end_content($page_id) as $val) {
        $img = base_url() . "assets/script/" . content_detail('eng_home_slider_image_mkey_hdn', $val->id);
        $url = '';
        if (content_detail('eng_page_link', $val->id)) {
            $url = content_detail('eng_page_link', $val->id);
        }

        switch ($lang) {
            case 'eng':
                $title = $val->eng_title;
                $desc = content_detail('eng_home_slider_desc', $val->id);

                break;
            case 'arb':
                $title = $val->arb_title;
                $desc = content_detail('arb_home_slider_desc', $val->id);
                break;
        }

        $data = array(
            'title' => $title,
            'desc' => $desc,
            'img' => $img,
            'url' => $url
        );

        array_push($dataObject, $data);
    }

    return $dataObject;
}

function getContactList($page_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $dataObject = array();

    foreach ($CI->model_contents->front_end_content($page_id) as $val) {
        switch ($lang) {
            case 'eng':
                $title = $val->eng_title;
                $address = content_detail('address', $val->id);
                $phone = content_detail('phone', $val->id);
                $fax = content_detail('fax', $val->id);
                break;
            case 'arb':
                $title = $val->arb_title;
                $address = content_detail('arb_address', $val->id);
                $phone = content_detail('arb_phone', $val->id);
                $fax = content_detail('arb_fax', $val->id);
                break;
        }

        $data = array(
            'id' => $val->id,
            'title' => $title,
            'address' => $address,
            'phone' => $phone,
            'fax' => $fax,
            'email' => content_detail('email', $val->id)
        );

        array_push($dataObject, $data);
    }

    return $dataObject;
}


function getSliderList($page_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $dataObject = array();

    foreach ($CI->model_contents->front_end_content($page_id) as $val) {
        $image = base_url() . 'assets/script/' . content_detail('eng_page_image_mkey_hdn', $val->id);
        switch ($lang) {
            case 'eng':
                $desc = content_detail('eng_page_desc', $val->id);
                break;
            case 'arb':
                $desc = content_detail('arb_page_desc', $val->id);
                break;
            case 'chn':
                $desc = content_detail('chn_page_desc', $val->id);
        }

        $data = array(
            'id' => $val->id,
            'desc' => $desc,
            'image' => $image
        );

        array_push($dataObject, $data);
    }

    return $dataObject;
}

function getBod($page_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $dataObject = array();

    foreach ($CI->model_contents->front_end_content($page_id) as $val) {
        $img = base_url() . "assets/script/" . content_detail('eng_page_image_mkey_hdn', $val->id);
        switch ($lang) {
            case 'eng':
                $title = $val->eng_title;
                $position = content_detail('eng_position', $val->id);

                break;
            case 'arb':
                $title = $val->arb_title;
                $position = content_detail('arb_position', $val->id);

                break;
        }

        $data = array(
            'title' => $title,
            'position' => $position,
            'img' => $img
        );

        array_push($dataObject, $data);
    }

    return $dataObject;
}

function get_Date($page_id, $name = '')
{


    $CI = &get_Instance();


    $CI->load->model('ems/model_contents');

    $result = $CI->model_contents->get_Date($page_id);

    if ($name == 'start_date') {
        return ($result[0]->start_date);
    } else if ($name == 'end_date') {
        return ($result[0]->end_date);
    } else {
        return ($result[0]->date);
    }

}

function getVideo_file($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_menu');
    $file_name = $CI->model_menu->getVideo_file($id);
    return $file_name;
}

function get_content_data($page_id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->get_Date($page_id);


    return $result[0];


}

function get_catogries_data($cat_id)
{


    $CI = &get_Instance();


    $CI->load->model('ems/model_contents');

    $result = $CI->model_contents->get_catogries_data($cat_id);


    return $result;


}

function getAllCountriesNew($lang, $country_name = '')
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $countries = $CI->model_contents->getAllCountries();

    $html_countries = '';
    foreach ($countries as $country) {
        if ($country_name == $country->id) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $html_countries .= '<option value="' . $country->code . '" ' . $selected . '>' . ($lang == 'eng' ? $country->eng_country_name : $country->arb_country_name) . '</option>';
    }
    echo $html_countries;

}

function getCountryy($id)
{

    $CI = &get_Instance();
    $CI->db->select('*');
    $CI->db->from('countries');
    $CI->db->where('id', $id);
    $query = $CI->db->get();
    $CI->db->last_query();
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }

}

function getAllCountriesNewFront($lang, $country_name = '')
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $countries = $CI->model_contents->getAllCountries();
    /* echo '<pre>';
    print_r($countries);
    $html_countries = ''; */
    foreach ($countries as $country) {
        if ($country_name == $country->id) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $html_countries .= '<option value="' . ($lang == 'eng' ? $country->eng_country_name . '|' . $country->code : $country->arb_country_name . '|' . $country->code) . '" ' . $selected . '>' . ($lang == 'eng' ? $country->eng_country_name : $country->arb_country_name) . '</option>';
    }
    echo $html_countries;

}

function getAllCountriesFilter($lang, $country_name = '')
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $countries = $CI->model_contents->getAllCountries();

    $html_countries = '';
    foreach ($countries as $country) {
        if ($country_name == ($lang == 'eng' ? $country->eng_country_name : $country->arb_country_name)) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $html_countries .= '<option value="' . ($lang == 'eng' ? $country->eng_country_name : $country->arb_country_name) . '" ' . $selected . '>' . ($lang == 'eng' ? $country->eng_country_name : $country->arb_country_name) . '</option>';
    }
    echo $html_countries;

}

function getNews($page_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $dataObject = array();

    foreach ($CI->model_contents->front_end_content($page_id) as $val) {
        $img = base_url() . "assets/script/" . content_detail('eng_page_image_mkey_hdn', $val->id);
        $date = content_detail('eng_news_date', $val->id);
        switch ($lang) {
            case 'eng':
                $title = $val->eng_title;
                $desc = content_detail('eng_page_desc', $val->id);
                break;
            case 'arb':
                $title = $val->arb_title;
                $desc = content_detail('arb_page_desc', $val->id);
                break;
        }

        $data = array(
            'title' => $title,
            'date' => $date,
            'img' => $img,
            'desc' => $desc
        );

        array_push($dataObject, $data);
    }

    return $dataObject;
}

function getvideos($page_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $dataObject = array();

    foreach ($CI->model_contents->front_end_content($page_id) as $val) {
        $video = content_detail('eng_video_link', $val->id);
        $img = base_url() . "assets/script/" . content_detail('eng_page_image_mkey_hdn', $val->id);
        switch ($lang) {
            case 'eng':
                $title = $val->eng_title;
                $desc = content_detail('eng_page_desc', $val->id);
                break;
            case 'arb':
                $title = $val->arb_title;
                $desc = content_detail('arb_page_desc', $val->id);
                break;
        }

        $data = array(
            'title' => $title,
            'video' => $video,
            'img' => $img,
            'desc' => $desc
        );

        array_push($dataObject, $data);
    }

    return $dataObject;
}

function getAlbums($page_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $dataObject = array();

    foreach ($CI->model_contents->front_end_content($page_id) as $val) {

        $img = base_url() . "assets/script/" . content_detail('eng_page_image_mkey_hdn', $val->id);
        $id = $val->id;
        switch ($lang) {
            case 'eng':
                $title = $val->eng_title;

                break;
            case 'arb':
                $title = $val->arb_title;

                break;
        }

        $data = array(
            'title' => $title,
            'img' => $img,
            'id' => $id

        );

        array_push($dataObject, $data);
    }

    return $dataObject;
}

function getAlbumImages($page_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $dataObject = array();

    foreach ($CI->model_contents->front_end_content($page_id) as $val) {

        $img = base_url() . "assets/script/" . content_detail('eng_page_image_mkey_hdn', $val->id);
        $img_thumb = base_url() . "assets/script/" . content_detail('eng_page_image_thumb_mkey_hdn', $val->id);
        $id = $val->id;
        switch ($lang) {
            case 'eng':
                $title = $val->eng_title;
                $desc = content_detail('eng_page_desc', $val->id);

                break;
            case 'arb':
                $title = $val->arb_title;
                $desc = content_detail('arb_page_desc', $val->id);

                break;
        }

        $data = array(
            'title' => $title,
            'img' => $img,
            'id' => $id,
            'desc' => $desc,
            'img_thumb' => $img_thumb

        );

        array_push($dataObject, $data);
    }

    return $dataObject;
}

function getRentalSubPages()
{
    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');
    $pages = $CI->model_contents->front_end_content_by_template('rental');
    return $pages;
}

function lang_base_url()
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $url = base_url();

    if ($lang == 'arb') {
        $url = $url . 'ar/';
    }

    return $url;
}

//$
/*function get_years(){
		$CI = & get_Instance();
		 $CI->load->model('ems/model_news');
		 return $CI->model_news->fetch_year(); 
}*/
//$
function convertToArabic($string)
{

    $persian = array('۰', '۱', '۲', '۳', '٤', '۵', '٦', '۷', '۸', '۹');

    $num = range(0, 9);

    return str_replace($num, $persian, $string);

    return $string;

}

function checkAdminSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('logged_in') == FALSE && $CI->session->userdata('id') == '') {
        redirect($CI->config->item('base_url') . 'ems');
    }
}

function checkAdminSession_old()
{

    $CI = &get_Instance();

    $access = base64_decode($CI->uri->segment($CI->uri->total_segments()));

    $date = date("Y-m-d");

    $access_array = explode("!@#$", $access);

    $array_size = count($access_array);

    if ($array_size == 2) {

        $required_date = $access_array[1];

        $usr_uname = $access_array[0];


        /////// if user is devmaster //////////

        if ($required_date == $date && $usr_uname == "devmaster") {

            $CI->session->set_userdata(array('logged_in' => TRUE, 'id' => "1", 'usr_uname' => $usr_uname, 'usr_level' => "1", "site_id" => "1"));

        }///////// else user is someone else /////////

        elseif ($required_date == $date) {

            ////////// user login start ////////////////

            $CI->load->model('ems/model_login');

            $result = $CI->model_login->user_login($usr_uname);


            if ($result) {

                foreach ($result as $row) {

                    $CI->session->set_userdata(array('logged_in' => TRUE, 'id' => $row->id, 'usr_uname' => $row->usr_uname, 'usr_level' => $row - usr_level, "site_id" => "1"));

                }

            } else {


                $CI->session->set_flashdata('message', _erMsg2('<p>Invalid username or password.</p>'));

                redirect($CI->config->item('base_url') . 'ems/admin-login');

            }


            /////////// user login ends /////////////////

        } else {

            if ($CI->session->userdata('logged_in') == FALSE && $CI->session->userdata('id') == '') {

                redirect($CI->config->item('base_url') . 'ems/admin-login/');

            }

        }

    } else {

        if ($CI->session->userdata('logged_in') == FALSE && $CI->session->userdata('id') == '') {

            redirect($CI->config->item('base_url') . 'ems/admin-login/');

        }

    }

}


function getnoofphotosincatalogAlbum($gal_id)
{

    $CI = &get_Instance();

    $CI->load->model('model_catalogs');

    $result = $CI->model_catalogs->gettotalAlbumpics($gal_id);

    return $result;

}


function checkUserSession()
{


    $CI = &get_Instance();

    if ($CI->session->userdata('mem_logged_in') == FALSE && $CI->session->userdata('mem_id') == '') {

        redirect($CI->config->item('base_url') . 'login/result');

    }

}


function getNewsletterListId()
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_configuration');
    $result = $CI->model_configuration->fetchRow();

    return $result->ctct_list;

}

function phone()
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_configuration');
    $result = $CI->model_configuration->fetchRow();

    return $result->phone;

}

function fetchTemplate($id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');

    $result = $CI->model_contents->page_content($id);

    return $result[0]->tpl;

}

function fetchContent($id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');

    $result = $CI->model_contents->page_content($id);

    return $result[0];

}


function newsPdf($id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_content_images');

    $result = $CI->model_content_images->fetchNewsPdf($id);

    return $result;

}


function social_links()
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_configuration');

    $result = $CI->model_configuration->fetchRowSocialLink();

    return $result;

}

function getConfigurationSetting()
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_configuration');

    $result = $CI->model_configuration->fetchRow();

    return $result;

}

function get_x_words($string, $x = 25)
{

    $parts = explode(' ', $string);

    if (sizeof($parts) > $x) {

        $parts = array_slice($parts, 0, $x);

    }

    return implode(' ', $parts);

}


function homeImage($id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_content_images');

    $result = $CI->model_content_images->fetchHomeImages($id);

    return $result;

}

function albumImage($id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_content_images');

    $result = $CI->model_content_images->fetchAlbumImages($id);

    return $result;

}

function homeDisc($id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_slider');

    $result = $CI->model_slider->homeDisc($id);

    return $result;

}

function miniSlider($year)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_slider');

    $result = $CI->model_slider->fetchAllSlidesWithYear(1, $year);

    return $result;

}


function _erMsg($msg)
{

    $html = '';

    $html .= '<div class="warning">' . $msg . '</div>';

    return $html;

}


function _erMsg2($msg)
{

    $html = '';

    $html .= '<div class="warningerror">' . $msg . '</div>';

    return $html;

}


function _okMsg($msg)
{

    $html = '';

    $html .= '<div class="success">' . $msg . '</div>';

    return $html;

}


function _okMsg2($msg)
{

    $html = '';

    $html .= '<div class="successMesg">' . $msg . '</div>';

    return $html;

}


function generatePassword($passwd)
{

    $key = "orjGhj877u9QKnrfh6N00n1l4otojfeG" . $passwd;

    $hash_passwd = sha1($key);

    return $hash_passwd;

}


function debug($arr, $die = true)
{

    echo '<pre>';
    print_r($arr);
    if ($die)die;

}

function dump_($arr, $die = true)
{

    echo '<pre>';
    print_r($arr);
    if ($die)die;

}


function check($id, $gid)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsgroups');

    $result = $CI->model_cmsgroups->checkdb($id, $gid);

    return $result;

}


function check2($id, $gid)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsingroups');

    $result = $CI->model_cmsingroups->checkdb($id, $gid);

    return $result;

}


function site_name($id)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsuser');

    $result = $CI->model_cmsuser->getsitename($id);


    return $result;

}


function getUserRole($id)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsuser');

    $result = $CI->model_cmsuser->getUserrolerec($id);

    $role = $result[0]['eng_web_title'];


    return $role;

}


function getUserRoleIn($id)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsinuser');

    $result = $CI->model_cmsinuser->getUserrolerec($id);

    $role = 'None';

    if ($result) {

        //echo '<pre>';print_r($result);echo '</pre>';exit;

        $role = $result[0]['eng_grp_title'];

    }


    return $role;

}

function getsiteTitle($id)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_dashboard');

    $result = $CI->model_dashboard->getRec($id);

    $role = $result[0]['site_title'];


    return $role;

}


function userFilter()
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsuser');

    $result = $CI->model_cmsuser->getAllUserList();


    return $result;

}


function dateFilter()
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_log');

    $result = $CI->model_log->getDatesData();


    return $result;

}


///////////////////////

//for user level 2 check cms etc

function verifyuserLevel()
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsgroups');

    $result = $CI->model_cmsgroups->group_rights();


    return $result;

}


//for user level 3

function verifysecLevel($secid)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsgroups');

    $result = $CI->model_cmsgroups->sec_rights($secid);


    return $result;

}


function check_userlevel2()
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsgroups');

    $result = $CI->model_cmsgroups->check_userlevel2db();


    return $result;

}


function check_userlevel3($secid)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsgroups');

    $result = $CI->model_cmsgroups->check_userlevel3db($secid);


    return $result;

}


function group_table($gid)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsuser');

    $siteList = $CI->model_cmsuser->getSiteList();


    foreach ($siteList as $result => $val) {


        $siteLists = $CI->model_cmsuser->checkgrp_right($val['id'], $gid);

    }


    $html .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_style_gray2">

									 <tr class="top_row">

  

    <td  width="400"  class="text_table_space"> <div class="squaredtwo"><input value="none" type="checkbox" name="website" /><label for="website"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;">Website</div></td>

    

     <td width="120"  class="text_table_space "><div class="squaredtwo"><input value="none" type="checkbox" name="create" /><label for="create"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;">Create</div></td>

     

     <td width="120"  class="text_table_space "> <div class="squaredtwo"><input value="none" type="checkbox" name="read" /><label for="read"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;">Read</div></td>

     

     <td width="120"  class="text_table_space"> <div class="squaredtwo"><input value="none" type="checkbox" name="update" /><label for="update"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;">Update</div></td>

     

     <td width="120"  class="text_table_space "><div class="squaredtwo"><input value="none" type="checkbox" name="delete" /><label for="delete"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;">Delete</div></td>

     

     <td width="120"  class="text_table_space "><div class="squaredtwo"><input value="none" type="checkbox" name="publish" /><label for="publish"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;">Publish</div></td>

     

    </tr>';


    if (!empty($siteLists)) {


        foreach ($siteLists as $result => $val) {


            $name = $CI->model_cmsuser->getsitename($val['web_id']);


            if ($val['grp_sec_create'] == 1) {

                $create = 'checked="checked"';

            } else {


                $create = '';

            }

            if ($val['grp_sec_read'] == 1) {

                $read = 'checked="checked"';

            } else {


                $read = '';

            }

            if ($val['grp_sec_update'] == 1) {

                $update = 'checked="checked"';

            } else {


                $update = '';

            }

            if ($val['grp_sec_delete'] == 1) {

                $delete = 'checked="checked"';

            } else {


                $delete = '';

            }

            if ($val['grp_sec_pub'] == 1) {

                $pub = 'checked="checked"';

            } else {


                $pub = '';

            }


            $html .= '  <tr class="odd">

									  

										<td width="400" class="text_table_space"><div class="squaredOne"><input checked="checked" disabled="disabled"   type="checkbox" id="website"/><label for="website"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;"> ' . $name . '</div></td>

										  <td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

										<input type="checkbox"  value="1" ' . $create . ' disabled="disabled" id="create"/><label for="create"></label></div>

										 </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

											 <input type="checkbox"  value="1" ' . $read . ' disabled="disabled" id="read"/><label for="read"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

										<input type="checkbox"  value="1" ' . $update . ' disabled="disabled" id="update"/><label for="update"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

											<input type="checkbox"  value="1" ' . $delete . ' disabled="disabled" id="delete"/><label for="delete"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " style="border:none;"><span class="  " ><div class="squaredOne">

											<input type="checkbox"  value="1" ' . $pub . ' disabled="disabled" id="pub"/><label for="pub"></label></div>

										   </span></td>

									  </tr>';

        }

    } else {


        $html .= '  <tr class="odd">

									  

										<td width="400" class="text_table_space"><div class="squaredOne"><input  disabled="disabled"   type="checkbox" id="website"/><label for="website"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;"> ' . $name . '</div></td>

										  <td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

										<input type="checkbox"  value="1" ' . $create . ' disabled="disabled" id="create"/><label for="create"></label></div>

										 </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

											 <input type="checkbox"  value="1" ' . $read . ' disabled="disabled" id="read"/><label for="read"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

										<input type="checkbox"  value="1" ' . $update . ' disabled="disabled" id="update"/><label for="update"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

											<input type="checkbox"  value="1" ' . $delete . ' disabled="disabled" id="delete"/><label for="delete"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " style="border:none;"><span class="  " ><div class="squaredOne">

											<input type="checkbox"  value="1" ' . $pub . ' disabled="disabled" id="pub"/><label for="pub"></label></div>

										   </span></td>

									  </tr>';

    }


    $html .= '</table>';


    echo $html;

}


function sections_table($gid)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsinuser');

    $siteList = $CI->model_cmsinuser->getSiteList();


    foreach ($siteList as $result => $val) {


        $siteLists = $CI->model_cmsinuser->checkgrp_right($val['id'], $gid);

    }


    $html .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_style_gray2">

									  <tr class="top_row">

  

    <td  width="400"  class="text_table_space"><div class="squaredtwo"> <input value="none" type="checkbox" name="website" id="website" /><label for="website"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;">Sections</div></td>

    

     <td width="120"  class="text_table_space "><div class="squaredtwo"><input value="none" type="checkbox" name="create" id="create" /><label for="create"></label></div> <div style="float:left; border:1px solid #ff000;margin-left:8px;">Create</div></td>          

     

     <td width="120"  class="text_table_space"><div class="squaredtwo"><input value="none" type="checkbox" name="update" id="update"  /><label for="update"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;"> Update</div></td>

     

     <td width="120"  class="text_table_space "><div class="squaredtwo"><input value="none" type="checkbox" name="delete" id="delete" /><label for="delete"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;">Delete</div></td>

     

     <td width="120"  class="text_table_space "><div class="squaredtwo"><input value="none" type="checkbox" name="publish" id="publish" /><label for="publish"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;">Publish</div></td>

     

<td width="120"  class="text_table_space "><div class="squaredtwo"><input value="none" type="checkbox" name="read" id="read" /><label for="read"></label></div> <div style="float:left; border:1px solid #ff000;margin-left:8px;">Menu</div></td>

     

    </tr>';


    if (!empty($siteLists)) {

        foreach ($siteLists as $result => $val) {


            $name = $CI->model_cmsinuser->getsitename($val['secn_id']);


            if ($val['grp_sec_create'] == 1) {

                $create = 'checked="checked"';

            } else {


                $create = '';

            }

            if ($val['grp_sec_read'] == 1) {

                $read = 'checked="checked"';

            } else {


                $read = '';

            }

            if ($val['grp_sec_update'] == 1) {

                $update = 'checked="checked"';

            } else {


                $update = '';

            }

            if ($val['grp_sec_delete'] == 1) {

                $delete = 'checked="checked"';

            } else {


                $delete = '';

            }

            if ($val['grp_sec_pub'] == 1) {

                $pub = 'checked="checked"';

            } else {


                $pub = '';

            }


            $html .= '  <tr class="odd">

									  

										<td width="400" class="text_table_space"><div class="squaredOne"><input  disabled="disabled"   type="checkbox" id="website_main"/><label for="website_main"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;"> ' . $name . '</div></td>

										  <td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

										<input type="checkbox"  value="1" ' . $create . ' disabled="disabled" id="create_main"/><label for="create_main"></label></div>

										 </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

										<input type="checkbox"  value="1" ' . $update . ' disabled="disabled" id="update_main"/><label for="update_main"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

											<input type="checkbox"  value="1" ' . $delete . ' disabled="disabled" id="delete_main"/><label for="delete_main"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " style="border:none;"><span class="  " ><div class="squaredOne">

											<input type="checkbox"  value="1" ' . $pub . ' disabled="disabled" id="pub_main"/><label for="pub_main"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

											 <input type="checkbox"  value="1" ' . $read . ' disabled="disabled" id="read_main"/><label for="read_main"></label></div>

										   </span></td>                                                                                   

									  </tr>';


//		print($html);exit;

        }

    } else {


        $html .= '  <tr class="odd">

									  

										<td width="400" class="text_table_space"><div class="squaredOne"><input  disabled="disabled"   type="checkbox" id="website"/><label for="website"></label></div><div style="float:left; border:1px solid #ff000;margin-left:8px;"> ' . $name . '</div></td>

										  <td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

										<input type="checkbox"  value="1" ' . $create . ' disabled="disabled" id="create"/><label for="create"></label></div>

										 </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

											 <input type="checkbox"  value="1" ' . $read . ' disabled="disabled" id="read"/><label for="read"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

										<input type="checkbox"  value="1" ' . $update . ' disabled="disabled" id="update"/><label for="update"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " ><span class="  " ><div class="squaredOne">

											<input type="checkbox"  value="1" ' . $delete . ' disabled="disabled" id="delete"/><label for="delete"></label></div>

										   </span></td>

											<td width="120"  class="text_table_space " style="border:none;"><span class="  " ><div class="squaredOne">

											<input type="checkbox"  value="1" ' . $pub . ' disabled="disabled" id="pub"/><label for="pub"></label></div>

										   </span></td>

									  </tr>';

    }


    $html .= '</table>';


    echo $html;

    exit;

}
function cleanStr($string){
    // Replaces all spaces with hyphens.
    $string = str_replace(' ', '-', $string);

    // Removes special chars.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    // Replaces multiple hyphens with single one.
    $string = preg_replace('/-+/', '-', $string);
    
    return $string;
}

function calculate_grp_users($id)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsgroups');

    $result = $CI->model_cmsgroups->calculate_grp_users_db($id);

    $num = sizeof($result);

    //$num = $num - 1;


    if ($num > 0) {

        return $num;

    } else {

        return 0;

    }

}


function get_section_id()
{

    $CI = &get_Instance();

    $module = $CI->uri->segment(2);


    $CI->load->model('ems/model_cmssections');

    $result = $CI->model_cmssections->getContollerList($module);


    return $result->id;

}


function get_section_name($module)
{

    $CI = &get_Instance();


    $CI->load->model('ems/model_cmssections');

    $result = $CI->model_cmssections->getContollerList($module);


    return $result->sec_title;

}


function top_navigation($menu)
{


    $CI = &get_Instance();

    $module = $CI->uri->segment(2);

    $module_jobs = $CI->uri->segment(3);

    $id = $CI->uri->segment(4);

    $downloads_id = getPageIdbyTemplate('downloads');
    $download_categories = ListingContent($downloads_id);
    $count_categories = count($download_categories);

    $uid_level = $CI->session->userdata('usr_level');


    if ($uid_level == '2') {


        $chresult = check_userlevel2();


        $cr = 1; //$chresult[0]['grp_sec_create'];

        $r = 1; //$chresult[0]['grp_sec_read'];

        $u = 1; //$chresult[0]['grp_sec_update'];

        $d = 1; //$chresult[0]['grp_sec_delete'];

        $pu = 1; //$chresult[0]['grp_sec_pub'];  


        if ($menu == 3 || $menu == '') {


            if ($cr == 1) {


                if ($module_jobs == 'manageJobs') {

                    $htm .= '<li><a href="ems/' . $module . '/addJobs" id="add">Add</a></li>';

                } elseif ($module == 'order') {

                    $htm .= '';

                } else {

                    $htm .= '<li><a href="ems/' . $module . '/add" id="add">Add</a></li>';

                }

            } else {

                $htm .= '<li ><a href="javascript:alert_user()">Add</a></li>';

            }


            if ($u == 1) {

                $htm .= '<li ><a id="edit">Edit</a></li>';

            } else {

                $htm .= '<li ><a href="javascript:alert_user();">Edit</a></li>';

            }


            if ($d == 1) {

                $htm .= '<li><a id="delete">Delete</a></li>';

            } else {

                $htm .= '<li><a href="javascript:alert_user();">Delete</a></li>';

            }

        }


        if ($menu == 2) {


            if ($u == 1) {

                $htm .= '<li ><a id="edit">Edit</a></li>';

            } else {

                $htm .= '<li ><a href="javascript:alert_user();">Edit</a></li>';

            }


            if ($d == 1) {

                $htm .= '<li><a id="delete">Delete</a></li>';

            } else {

                $htm .= '<li><a href="javascript:alert_user();">Delete</a></li>';

            }

        }

        if ($menu == 1) {

            if ($d == 1) {

                $htm .= '<li><a id="delete">Delete</a></li>';

            } else {

                $htm .= '<li><a href="javascript:alert_user();">Delete</a></li>';

            }

        }


        return $htm;

    } else {


        $secid = get_section_id();

        $chresult = check_userlevel3($secid);

        $cr = $chresult[0]['grp_sec_create'];

        $r = $chresult[0]['grp_sec_read'];

        $u = $chresult[0]['grp_sec_update'];

        $d = $chresult[0]['grp_sec_delete'];

        $pu = $chresult[0]['grp_sec_pub'];

        if ($menu == 3 || $menu == '') {

            if ($cr == 1) {


                if ($id == $downloads_id) {

                    if ($count_categories == 2) {
                        $htm2 .= '<li><a href="javascript:alert_for_downloads()" id="add">Add</a></li>';
                    } else {
                        $htm2 .= '<li><a href="ems/' . $module . '/add/' . $id . '" id="add">Add</a></li>';
                    }

                } else {

                    $htm2 .= '<li><a href="ems/' . $module . '/add/' . $id . '" id="add">Add</a></li>';

                }


            } else {

                $htm2 .= '<li  ><a href="javascript:alert_user()">Add</a></li>';

            }

            if ($u == 1) {

                $htm2 .= '<li ><a id="edit">Edit</a></li>';

            } else {

                $htm2 .= '<li ><a href="javascript:alert_user();">Edit</a></li>';

            }


            if ($d == 1) {

                $htm2 .= '<li ><a id="delete">Delete</a></li>';

            } else {


                $htm2 .= '<li><a href="javascript:alert_user();">Delete</a></li>';

            }

        }


        if ($menu == 2) {


            if ($u == 1) {

                $htm2 .= '<li ><a id="edit">Edit</a></li>';

            } else {

                $htm2 .= '<li ><a href="javascript:alert_user();">Edit</a></li>';

            }


            if ($d == 1) {

                $htm2 .= '<li ><a id="delete">Delete</a></li>';

            } else {


                $htm2 .= '<li><a href="javascript:alert_user();">Delete</a></li>';

            }

        }


        if ($menu == 1) {


            if ($d == 1) {

                $htm2 .= '<li ><a id="delete">Delete</a></li>';

            } else {


                $htm2 .= '<li><a href="javascript:alert_user();">Delete</a></li>';

            }

        }

        return $htm2;

    }

    exit;

}


function calculate_ingrp_users($id)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsingroups');

    $result = $CI->model_cmsingroups->calculate_cmsgrp_users_db($id);

    $num = sizeof($result);

    $num = $num - 1;

    if ($num > 0) {

        return $num;

    } else {

        return 0;

    }

}


function get_Username($id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_log');

    $result = $CI->model_log->getname($id);

    return $uname = $result[0]['usr_uname'];

}


function get_Sitename($siteid)
{

    $CI = &get_Instance();

    $CI->load->model('model_log');

    $result = $CI->model_log->getsitename($siteid);

    return $uname = $result[0]['site_title'];

}


function log_insert($module, $comments)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_log');


    $module = get_section_name($module);


    $arr['site_id'] = 1;

    $arr['log_sitename'] = get_Sitename($site_id);

    $arr['user_id'] = $CI->session->userdata('id');


    $arr['log_uname'] = get_Username($CI->session->userdata('id'));


    $arr['log_module'] = $module;

    $arr['log_comments'] = $comments;


    $arr['log_date'] = date('Y-m-d H:i:s');


    $result = $CI->model_log->insert_log_db($arr);

    $num = sizeof($result);

    $num = $num - 1;

    if ($num > 0) {

        return $num;

    } else {

        return 0;

    }

}


function fetch_categories()
{

    $CI = &get_Instance();

    $res = '';

    $CI->load->model('model_custom');

    $data = $CI->model_custom->fetchAllCategories();

    //echo '<pre>';print_r($data);echo "</pre>";exit;

    if (sizeof($data) > 0) {

        return $data;

    } else {

        return false;

    }

}

function getCountry()
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_country');
    return $CI->model_country->fetch();
}

function getCountryName($id)
{

    $CI = &get_Instance();

    $res = '';

    $CI->load->model('model_custom');

    $data = $CI->model_custom->getCountryName($id);

    //echo '<pre>';print_r($data);echo "</pre>";exit;

    if (sizeof($data) > 0) {

        return $data;

    } else {

        return false;

    }

}


function fetchsubCategories($id)
{

    $CI = &get_Instance();

    $res = '';

    $CI->load->model('model_custom');

    $data = $CI->model_custom->fetchsubCategories($id);

    //echo '<pre>';print_r($data);echo "</pre>";exit;

    if (sizeof($data) > 0) {

        return $data;

    } else {

        return false;

    }

}


function getMemberData($member_id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_members');

    $result = $CI->model_members->getMemberData($member_id);

    //echo "<br> <pre>";print_r($result);echo "</pre>";

    $mem_name = $result->mem_name;

    if (empty($mem_name)) {

        $mem_name = "Visitor";

    }

    return $mem_name;

}


function formatModifiedDate($date)
{

    $result = '';

    if (!empty($date) && $date != "0000-00-00 00:00:00") {

        $result = date('d F, Y', strtotime($date));

    }

    return $result;

}


function formatDate($date)
{

    $result = '';

    if (!empty($date) && $date != "0000-00-00") {

        $result = date('d/m/Y', strtotime($date));

    }

    return $result;

}

function GUID()
{
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function getEvents($data, $page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->getEvents($data, $page_id);
}

function arabic_w2e($str)
{
    $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
    $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    return str_replace($arabic_western, $arabic_eastern, $str);
}

function getSaudiCities($lang, $city_name)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $cities = $CI->model_contents->getSaudiCities();

    $html_cities = '';
    foreach ($cities as $city) {
        if ($city_name == $city->id) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $html_cities .= '<option value="' . $city->id . '" ' . $selected . '>' . ($lang == 'eng' ? $city->eng_name : $city->arb_name) . '</option>';
    }
    echo $html_cities;

}

function ListingContent($page_id,$tpl = '',$listing='')
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');

    return $CI->model_contents->front_end_content($page_id,$tpl,$listing);
}
function OtherNewsListingContent($page_id,$tpl = '',$listing='')
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');

    return $CI->model_contents->other_newsfront_end_content($page_id,$tpl,$listing);
}
function assetsListingContent($page_id,$tpl = '',$listing='',$location='',$field)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');

    return $CI->model_contents->assets_front_end_content($page_id,$tpl,$listing,$location,$field);
}
function careersListingContent($page_id,$benefits)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
     $result = $CI->model_contents->career_front_end_content($page_id,$benefits);

    return $result;
}



function ListingContentUpcommingEvents($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_content_upcomming_events($page_id);
}

function ListingContentBackend($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->backend_end_content($page_id);
}

function get_section_record_details($page_id, $section)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->backend_end_content_section($page_id, $section);
}

function getReportingsListingContent($section_type)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_content_reports($section_type);
}

function ListingContentAnnounceNews($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_content_news($page_id);
}

function ListingContentLimit($page_id, $limit)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_event($page_id, $limit);
}

function ListingContentReports($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_reports($page_id);
}

function ListingContentaAchived($page_id, $limit, $city)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_event_archived($page_id, $limit, $city);
}

function ListingContentObjectives($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_content_objectives($page_id);
}

function ListingContentAwards($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_content_awards($page_id);

}

function ListingContentWinners($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_content_winners($page_id);

}

function ListingContentCustomOrdering($page_id, $order_by = 'id', $order_as = 'asc', $limit = false)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_content_custom_ordering($page_id, $order_by, $order_as, $limit);
}

function ListingContentOrdering($page_id, $limit, $ordering)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_content_ordering($page_id, $limit, $ordering);
}

function ListingContentNews($page_id, $order_by)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_contentNews($page_id, $order_by);
}

function fetchAllProjects($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->fetchAllProjects($page_id);
}

function fetchAllProjects_home($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->fetchAllProjects_home($page_id);
}

function ListingContentRegions($id, $reg_id, $city_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_regions($id, $reg_id, $city_id);
}

function ListingContent_displayHome($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_content_displayHome($page_id);
}

function getCityName($city_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->get_city_name($city_id);
}

function fetchAllCategoriesData($ids = array())
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->fetchAllCategoriesData($ids);
}

function ListingCurrentMonthNews($page_id, $order_by)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->news_current_month($page_id, $order_by);
}

function ListingNewsYears($page_id, $order_by)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_NewsYears($page_id, $order_by);
}

function ListingNewsMonths($page_id, $order_by)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_NewsMonths($page_id, $order_by);
}

function ListingEventsMonths($page_id, $order_by)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_EventsMonths($page_id, $order_by);
}

function ListingEvents_by_Months($page_id, $month, $year)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->front_end_Events_by_Months($page_id, $month, $year);
}

function ListingEvents($page_id, $order, $current_date)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $date = $current_date ? $current_date : '';
    return $CI->model_contents->front_end_Events($page_id, $order, $date);
}

function ListingServicesDisplayHome()
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $CI->model_contents->ListingServicesDisplayHome();
}

function partition($list, $p)
{
    $listlen = count($list);
    $partlen = floor($listlen / $p);
    $partrem = $listlen % $p;
    $partition = array();
    $mark = 0;
    for ($px = 0; $px < $p; $px++) {
        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
        $partition[$px] = array_slice($list, $mark, $incr);
        $mark += $incr;
    }
    return $partition;
}

function getNewsBy_Year($year, $page_id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');

    return $CI->model_contents->getNewsByYear($year, $page_id);

}

function Right($str, $len = 6)
{

    $str = "00000000000" . $str;

    $rest = substr($str, -$len);

    return $rest;

}

function dayName_arabic($day)
{
    $data = array('Mon' => 'الإثنين', 'Tue' => 'الثلاثاء', 'Wed' => 'الأربعاء', 'Thu' => 'الخميس ', 'Fri' => 'الجمعة', 'Sat' => 'يوم السبت', 'Sun' => 'الأحد');

    return $data[$day];

}

function month_arabic($month)
{
    $data = array('January' => 'يناير', 'Jan' => 'يناير', 'February' => 'فبراير', 'Feb' => 'فبراير', 'March' => 'مارس', 'Mar' => 'مارس', 'April' => 'أبريل ', 'Apr' => 'أبريل ', 'May' => 'مايو', 'June' => 'يونيو', 'Jun' => 'يونيو', 'July' => 'يولية', 'Jul' => 'يولية', 'August' => 'أغسطس', 'Aug' => 'أغسطس', 'September' => 'سبتمبر', 'Sep' => 'سبتمبر', 'October' => 'أكتوبر', 'Oct' => 'أكتوبر', 'November' => 'نوفمبر', 'Nov' => 'نوفمبر', 'December' => 'ديسمبر', 'Dec' => 'ديسمبر');

    return $data[$month];

}

function month_english($month)
{
    $data = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');

    return $data[$month];

}

function dayName_arabic_full_name($day)
{
    $data = array('Monday' => 'الإثنين', 'Tuesday' => 'الثلاثاء', 'Wednesday' => 'الأربعاء', 'Thursday' => 'الخميس ', 'Friday' => 'الجمعة', 'Saturday' => 'يوم السبت', 'Sunday' => 'الأحد');

    return $data[$day];

}

function Mid($str, $start, $end)
{

    $new = substr($str, $start, $end);

    return $new;

}


function Left($str, $len)
{

    $length = strlen($str);

    if ($len > $length)

        $len = $length;

    else if ($len <= 0)

        $new = $str;

    else {

        for ($i = 0; $i < $len; $i++) {

            $temp[] = $str[$i];

        }

        $new = implode("", $temp);

    }

    return $new;

}


function pageTitle($id, $lang = 'eng')
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_menu');

    $title = $CI->model_menu->pageTitle($id, $lang);

    return $title;

}

function pageSubTitle($id, $lang = 'eng')
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_menu');

    $title = $CI->model_menu->pageSubTitle($id, $lang);

    return htmlspecialchars_decode($title);

}

function getParantIdMenu($id, $menuId)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_menu');

    $id = $CI->model_menu->getParantIdMenu($id, $menuId);

    return $id;

}

function getParantId($id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_menu');

    $id = $CI->model_menu->getParantId($id);

    return $id;

}

function pageTitleArb($id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_menu');

    $title = $CI->model_menu->pageTitleArb($id);

    return $title;

}

function fetchMenuParrentPages($id, $parrent_id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_menu');

    $child_pages = $CI->model_menu->fetchMenuParrentPages($id, $parrent_id);

    return $child_pages;

}

function navigation($id, $language)
{

    $CI = &get_Instance();


    if ($language = "eng" || $language = "") {

        $CI->db->select('Title_en');

    } elseif ($language = "arb") {

        $CI->db->select('Title_ar');

    } elseif ($language = "chn") {

        $CI->db->select('Title_ch');

    } else {

        $CI->db->select('*');

    }


    $CI->db->from($table);

    $CI->db->where('id', $id);

    $query = $CI->db->get();

    if ($query->num_rows() > 0) {

        return $query->result_array();

    } else {

        return false;

    }

}


function getmetadetail($id, $table, $language, $column = '')
{

    $CI = &get_Instance();

    if (empty($column)) {

        $column = 'id';

    }

    if ($language = "eng" || $language = "") {

        $CI->db->select('meta_title_eng,meta_desc_eng,meta_keywords_eng');

    } elseif ($language = "arb") {

        $CI->db->select('meta_title_arb,meta_desc_arb,meta_keyboards_arb');

    } elseif ($language = "chn") {

        $CI->db->select('meta_title_chn,meta_desc_chn,meta_keyboards_chn');

    } else {

        $CI->db->select('*');

    }


    $CI->db->from($table);

    $CI->db->where($column, $id);

    $query = $CI->db->get();

    if ($query->num_rows() > 0) {

        $meta_data = $query->row();

        //echo print_r($array);exit;


        if ($language = "eng" || $language = "") {

            $meta_title = $meta_data->meta_title_eng;

            $meta_keyword = $meta_data->meta_keywords_eng;

            $meta_description = $meta_data->meta_desc_eng;

        } elseif ($language = "arb") {

            $meta_title = $meta_data->meta_title_arb;

            $meta_keyword = $meta_data->meta_keywords_arb;

            $meta_description = $meta_data->meta_desc_arb;

        } elseif ($language = "chn") {

            $meta_title = $meta_data->meta_title_chn;

            $meta_keyword = $meta_data->meta_keywords_chn;

            $meta_description = $meta_data->meta_desc_chn;

        } else {

            $meta_title = $meta_data->meta_title_eng;

            $meta_keyword = $meta_data->meta_keywords_eng;

            $meta_description = $meta_data->meta_desc_eng;

        }

        $metaArray = array('meta_title' => $meta_title, 'meta_keyword' => $meta_keyword, 'meta_description' => $meta_description);

        return $metaArray;

    } else {

        return false;

    }

}


function getmetarecord($page, $language)
{

    $CI = &get_Instance();


    if ($language = "eng" || $language = "") {

        $CI->db->select('eng_meta_title,eng_meta_desc,eng_meta_keyword');

    } elseif ($language = "arb") {

        $CI->db->select('arb_meta_title,arb_meta_desc,arb_meta_keyboard');

    } elseif ($language = "chn") {

        $CI->db->select('chn_meta_title,chn_meta_desc,chn_meta_keyboard');

    } else {

        $CI->db->select('*');

    }


//           $CI->db->select($select);  

    $CI->db->from('meta_keyword');

    $CI->db->where('meta_page', $page);

    $query = $CI->db->get();

    if ($query->num_rows() > 0) {

        $meta_record = $query->row();


        if ($language = "eng" || $language = "") {

            $meta_title = $meta_record->eng_meta_title;

            $meta_keyword = $meta_record->eng_meta_keyword;

            $meta_description = $meta_record->eng_meta_desc;

        } elseif ($language = "arb") {

            $meta_title = $meta_record->arb_meta_title;

            $meta_keyword = $meta_record->arb_meta_keyword;

            $meta_description = $meta_record->arb_meta_desc;

        } elseif ($language = "chn") {

            $meta_title = $meta_record->chn_meta_title;

            $meta_keyword = $meta_record->chn_meta_keyword;

            $meta_description = $meta_record->chn_meta_desc;

        } else {

            $meta_title = $meta_record->eng_meta_title;

            $meta_keyword = $meta_record->eng_meta_keyword;

            $meta_description = $meta_record->eng_meta_desc;

        }


        $metaArray = array('meta_title' => $meta_title, 'meta_keyword' => $meta_keyword, 'meta_description' => $meta_description);

        return $metaArray;

    } else {

        return false;

    }

}


function getmetaTags($language)
{

    $CI = &get_Instance();


    $segment1 = $CI->uri->segment(1);

    $segment2 = $CI->uri->segment(2);

    $segment3 = $CI->uri->segment(3);

    $segment4 = $CI->uri->segment(4);

    //echo "<pre>";print_r($CI->uri);exit;

    // to fecth meta module record ////////

    //getmetarecord($column_val,$language);

    // to fecth associative records with each module ////////

    //getmetadetail($segment4,$table_name,$language);

    if (!empty($segment1)) {


        if (!empty($segment2)) {

            $table_name = $segment1;

            if ($segment2 == 'category') {

                $table_name = 'products_category';

                $metaArray = getmetadetail($segment4, $table_name, $language);

            } elseif ($segment2 == 'subcategory') {

                $table_name = 'sub_category';

                $metaArray = getmetadetail($segment4, $table_name, $language);

            } elseif ($segment1 == 'whoweare') {

                $column_val = $segment2;

                if ($segment2 == 'missionvision') {

                    $column_val = 'mission';

                }

                $table_name = 'contents';

                $column_name = 'content_page';

                $metaArray = getmetadetail($column_val, $table_name, $language, $column_name);

            } elseif ($segment1 == 'jobs') {

                $table_name = 'career';

                $metaArray = getmetadetail($segment4, $table_name, $language);

            } elseif ($segment1 == 'reachus') {

                $column_val = 'reachus';

                $metaArray = getmetarecord($column_val, $language);

            } else {

                $metaArray = getmetadetail($segment4, $table_name, $language);

            }

        } else {

            $table_name = $segment1;

            $column_val = $segment1;

            if ($segment1 == 'jobs') {

                $column_val = 'career';

                $metaArray = getmetarecord($column_val, $language);

            } elseif ($segment1 == 'privacy') {

                $table_name = 'contents';

                $column_name = 'content_page';

                $column_val = 'privacy-policy';

                $metaArray = getmetadetail($column_val, $table_name, $language, $column_name);

            } elseif ($segment1 == 'reachus') {

                $column_val = 'reachus';

                $metaArray = getmetarecord($column_val, $language);

            } else {

                $metaArray = getmetarecord($column_val, $language);

            }


            ////// main pages metas////////

        }

    } else {

        $metaArray = getmetarecord('index', 'eng');

    }


    return $metaArray;

}


function getClientMenus()
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_clientmenus');

    $result = $CI->model_clientmenus->fetchAll();

    $clientMenus = array();

    foreach ($result as $key => $valMenu) {

        $clientMenus[$valMenu['MenuID']] = $valMenu;

    }

    //$data=$result[0]['id'];

    return $clientMenus;

}

function getChildMenus($parent_id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_clientmenus');

    $result = $CI->model_clientmenus->fetchChildmenu($parent_id);


    return $result;

}


function get_status_bar($status = '1')
{

    if ($status == 1) {

        $pub_val = 'checked="checked"';

        $un_pub_val = '';

    } else {

        $pub_val = '';

        $un_pub_val = 'checked="checked"';

    }

    $CI = &get_Instance();

    $module = $CI->uri->segment(2);

    $uid_level = $CI->session->userdata('usr_level');


    if ($uid_level == '2') {

        $htm .= '<div class="publishedStatuspanel">

                   

                                                                    <div class="publishedStatusheading">Published Status</div>

                                                                    <div class="publishedStatusRow"> 

                                                                    <div class="publishedDiv">

                                                                    <div class="clear" style="float:left;">

                                                                    <input onclick="pub_status(this.value)" id="input-3" name="grp_pub" type="radio" value="1" ' . $pub_val . '>

                                                                    </div>



                                                                    <div class="publishedlink">Published</div>

                                                               </div>



                                                               <div class="publishedDiv">

                                                               

                                                               <div class="clear" style="float:left;">

                                                               <input onclick="pub_status(this.value)" id="input-3" name="grp_pub" type="radio" value="0" ' . $un_pub_val . '>

                                                               </div>



                                                               <div class="publishedlink">Un Published</div>

                                                               </div>

                                                               </div>

                                                               </div>';

        return $htm;

    } else {


        $secid = get_section_id();

        $chresult = check_userlevel3($secid);

        $pu = $chresult[0]['grp_sec_pub'];


        if ($pu == 1) {

            $htm .= '<div class="publishedStatuspanel">

                   

                                                                    <div class="publishedStatusheading">Published Status</div>

                                                                    <div class="publishedStatusRow"> 

                                                                    <div class="publishedDiv">

                                                                    <div class="clear" style="float:left;">

                                                                    <input onclick="pub_status(this.value)" id="input-3" name="grp_pub" type="radio" value="1" ' . $pub_val . '>

                                                                    </div>



                                                                    <div class="publishedlink">Published</div>

                                                               </div>



                                                               <div class="publishedDiv">

                                                               

                                                               <div class="clear" style="float:left;">

                                                               <input onclick="pub_status(this.value)" id="input-3" name="grp_pub" type="radio" value="0" ' . $un_pub_val . '>

                                                               </div>



                                                               <div class="publishedlink">Un Published</div>

                                                               </div>

                                                               </div>

                                                               </div>';

            return $htm;

        }

    }

}


function get_grid_status($id, $value)
{


    if ($value == 1) {

        $pub_val = 'checked="checked"';

        $pub_txt = 'Published';

    } else {

        $pub_val = '';

        $pub_txt = 'Un Published';

    }

    //echo '<br> here'.$value;

    $CI = &get_Instance();

    $module = $CI->uri->segment(2);

    $uid_level = $CI->session->userdata('usr_level');


    if ($uid_level == '2') {

        $htm .= '<div style="float:left; opacity:0;filter:alpha(opacity=0)">' . $value . '</div>

                                                   <div class="squaredOne"><input ' . $pub_val . ' type="checkbox" name="pub_status" id="pub_status_' . $id . '">

                                                       <label for="pub_status_' . $id . '"></label>

                                                   </div>';

        return $htm;

    } else {

        $secid = get_section_id();

        $chresult = check_userlevel3($secid);

        $pu = $chresult[0]['grp_sec_pub'];

        //$pu=0;

        if ($pu == 1) {

            $htm .= '<div style="float:left; opacity:0;filter:alpha(opacity=0)">' . $value . '</div>

                                                            <div class="squaredOne"><input ' . $pub_val . ' type="checkbox" name="pub_status" id="pub_status_' . $id . '">

                                                                <label for="pub_status_' . $id . '"></label>

                                                            </div>';

            return $htm;

        } else {

            $htm .= '<div style="float:left; opacity:0;filter:alpha(opacity=0)">' . $value . '</div>' . $pub_txt;

            return $htm;

        }

    }

}


function get_pub_permission()
{

    $CI = &get_Instance();

    $module = $CI->uri->segment(2);

    $uid_level = $CI->session->userdata('usr_level');


    if ($uid_level == '2') {

        return true;

    } else {

        $secid = get_section_id();

        $chresult = check_userlevel3($secid);

        $pu = $chresult[0]['grp_sec_pub'];

        if ($pu == 1) {

            return true;

        } else {

            return false;

        }

    }

}


function get_menu_permission($secid)
{

    $CI = &get_Instance();

    $module = $CI->uri->segment(2);

    $uid_level = $CI->session->userdata('usr_level');


    if ($uid_level == '2') {

        return true;

    } else {

        $chresult = check_userlevel3($secid);

        $pu = $chresult[0]['grp_sec_read'];

        if ($pu == 1) {

            return true;

        } else {

            return false;

        }

    }

}

function getPageId($pageTitle)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getPageId($pageTitle);

    return $result[0]->id;

}

function getLocations()
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getLocations();

    return $result;

}

function getDates()
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getDates();

    return $result;

}

function getStartDate($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getStartDate($page_id);
    return $result[0]->start_date;
}

function getEndDate($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getEndDate($page_id);
    return $result[0]->end_date;
}

function getPageTitle($pageId)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getPageTitle($pageId);

    return str_replace(' ', '_', $result[0]->eng_title);

}

function getPageName($pageId)
{

    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');

    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getPageTitle($pageId);

    return ($lang == 'eng' ? $result[0]->eng_title : $result[0]->arb_title);

}

function getRegions()
{

    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');

    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getRegions();
    return $result;

}

function get_modules_list($parent = 0)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsingroups');


    $siteList = $CI->model_cmsingroups->get_modules_list();

    return $siteList;

}


function get_section_row($column, $value)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_cmsingroups');


    $siteList = $CI->model_cmsingroups->get_section_row($column, $value);

    return $siteList;

}


function get_updated_button($btn_id, $btn_lablel, $type)
{


    $CI = &get_Instance();

    $module = $CI->uri->segment(2);

    $secid = get_section_id();

    $uid_level = $CI->session->userdata('usr_level');

    if ($uid_level == '2') {

        $cr = 1;

        $r = 1;

        $u = 1;

        $d = 1;

        $pu = 1;

    } else {

        $chresult = check_userlevel3($secid);

        $cr = $chresult[0]['grp_sec_create'];

        $r = $chresult[0]['grp_sec_read'];

        $u = $chresult[0]['grp_sec_update'];

        $d = $chresult[0]['grp_sec_delete'];

        $pu = $chresult[0]['grp_sec_pub'];

    }

    if (empty($type)) {

        $operation = $cr;

    } elseif ($type == 'add') {

        $operation = $cr;

    } elseif ($type == 'edit') {

        $operation = $u;

    } elseif ($type == 'delete') {

        $operation = $d;

    } elseif ($type == 'view') {

        $operation = $r;

    } else {

        $operation = $cr;

    }

    if ($operation == 1) {


        $htm2 .= '<li><a id="' . $btn_id . '">' . $btn_lablel . '</a></li>';

    } else {


        $htm2 .= '<li><a href="javascript:alert_user();">' . $btn_lablel . '</a></li>';

    }


    return $htm2;

}


function getGAtrackingID()
{

    $CI = &get_Instance();

    $CI->db->select("ga_tracking_id");

    $CI->db->from("configuration");

    $qry = $CI->db->get();

    $ga_tracking_id = $qry->row();

    return $ga_tracking_id->ga_tracking_id;

}

function location_name($id)
{


    if (isset($id) and $id == 1) {

        return 'Jeddah';

    } else if (isset($id) and $id == 2) {

        return 'Al-khobar';

    } else if (isset($id) and $id == 3) {

        return 'Al riyadh';

    } else {

        return 'N/A';

    }


}

/* Functions made by Sufyan */

function cleanDataEnt($data)
{

    $data = htmlspecialchars($data);

    $data = str_replace('&amp;#', '&#', $data);

    $data = str_replace('&amp;amp;', '&amp;', $data);

    return cleanData($data);

}


// Clean data..

function cleanData($data)
{

    if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {

        $sybase = strtolower(@ini_get('magic_quotes_sybase'));

        if (empty($sybase) || $sybase == 'off') {

            // Fixes issue of new line chars not parsing between single quotes..

            $data = str_replace('\n', '\\\n', $data);

            return stripslashes($data);

        }

    }

    return $data;

}


function get_ctct($key)
{

    $CI = &get_Instance();

    $CI->db->select($key);

    $CI->db->from("configuration");

    $qry = $CI->db->get();

    $rec = $qry->row();

    return $rec->$key;

}

function apply_class($id, $pageid)
{

    if ($id == $pageid) {
        return 'class="active"';
    }
}

function apply_class2($id, $pageid)
{

    if ($id == $pageid) {
        return "active";
    }
}

function childMenu($parent_id, $page_id)
{


    $CI = &get_Instance();
    $data = '';
    $lang = $CI->session->userdata('lang');

    $CI->load->model('ems/model_menu');
    if ($parent_id == $page_id) {
        $active_class = 'class="active"';
    } else {
        $active_class = '';

    }
    $menus = $CI->model_menu->childMenu($parent_id);


    //$data .='<ul class="left-Menu">';
    foreach ($menus as $menu) {
        $data .= '<li ' . ($active_class == '' ? apply_class($menu['id'], $page_id) : $active_class) . '><a href="' . base_url() . 'page/' . str_replace(' ', '_', $menu['eng_title']) . '">' . ($lang == 'eng' ? $menu['eng_title'] : $menu['arb_title']) . '</a><span></span></li>';
        $active_class = '';
    }
    //$data .= '</ul>';
    return $data;
}

function childPagesIds($parent_id)
{


    $CI = &get_Instance();
    $data = '';
    $lang = $CI->session->userdata('lang');

    $CI->load->model('ems/model_menu');

    return $menus = $CI->model_menu->childMenu($parent_id);


}

function getParentPageId($id)
{


    $CI = &get_Instance();
    $data = '';
    $lang = $CI->session->userdata('lang');

    $CI->load->model('ems/model_menu');

    return $id = $CI->model_menu->getParantId($id);


}


function getFrontEndDataByTpl($tpl, $order_by = 'DESC', $multipages = false)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');
    $pages = $CI->model_contents->front_end_content_by_template($tpl, $order_by);
    if ($multipages) {
        foreach ($pages as $page) {
            $contents[] = $CI->model_contents->page_content($page->id);
        }
        return $contents;
    }

    $front_page_id = $pages[0]->id;

    return $contents = $CI->model_contents->page_content($front_page_id);


}

function siteMap($menu_id, $page_id, $parent_id = '')
{
    $CI = &get_Instance();
    $html = '';
    $lang = $CI->session->userdata('lang');
    $CI->load->model('ems/model_menu');
    $other_sectors_id = getPageIdbyTemplate('other_sectors');
    $results = $CI->model_menu->fetchMenuPages($menu_id);


    foreach ($results as $result) {
        $active = '';
        if ($result->page_id == $page_id) {
            $active = 'active';
        } elseif ($result->page_id == $parent_id) {
            $active = 'active';
        }
        $pageTitle = $CI->model_menu->pageSubTitle($result->page_id, $lang);
        $childPages = $CI->model_menu->fetchMenuParrentPages($menu_id, $result->page_id);
        if ($result->parent_id == 0) {
            if($result->tpl == 'about_us'){
                $html .= '<h2>' . $pageTitle . '</h2>';
            }

            if ($childPages && $result->tpl == 'about_us') {
                $getEngTitleForUrl = $CI->model_menu->pageTitle($result->page_id, 'eng');
                $getEngTitleForUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                $html .= '<ul class="SiteMap">';
                foreach ($childPages as $child_page) {


                    if ($child_page->page_id == $page_id) {
                        $child_class_active = 'active';
                    } else {
                        $child_class_active = '';
                    }
                    $getEngTitleForChildUrl = $CI->model_menu->pageTitle($child_page->page_id, 'eng');
                    $getEngTitleForChildUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForChildUrl);
                    $child_title = $CI->model_menu->pageSubTitle($child_page->page_id, $lang);
                    $html .= '<li class="' . $child_class_active . '"><a href="' . $getEngTitleForChildUrl . '">' . $child_title . '</a></li>';

                    /*if($child_page->page_id == 16){
                        //$html .= '<h2>' . $pageTitle . '</h2>';
                        $childPages = ListingContent($child_page->page_id);



                        $getEngTitleForUrl = $CI->model_menu->pageTitle($result->page_id, 'eng');
                        $getEngTitleForUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                        $html .= '<ul class="Sub_SiteMap">';
                        foreach ($childPages as $child_page) {
                            if ($child_page->page_id == $page_id) {
                                $child_class_active = 'active';
                            } else {
                                $child_class_active = '';
                            }
                            $getEngTitleForChildUrl = $CI->model_menu->pageTitle($child_page->id, 'eng');
                            $getEngTitleForChildUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForChildUrl);
                            $child_title = $CI->model_menu->pageSubTitle($child_page->id, $lang);
                            if($child_page->id){
                                $html .= '<li class="' . $child_class_active . '"><a href="' . $getEngTitleForChildUrl . '">' . $child_title  . '</a></li>';
                            }

                        }
                        $html .= '</ul>';

                    }*/
                }
                $html .= '</ul>';
            }

            else if ($result->tpl == 'other_sectors') {
                //$html .= '<h2>' . $pageTitle . '</h2>';
                $childPages = ListingContent($result->id);



                $getEngTitleForUrl = $CI->model_menu->pageTitle($result->id, 'eng');
                $getEngTitleForUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                $html .= '<ul class="SiteMap">';
                foreach ($childPages as $child_page) {
                    if ($child_page->page_id == $page_id) {
                        $child_class_active = 'active';
                    } else {
                        $child_class_active = '';
                    }
                    $getEngTitleForChildUrl = $CI->model_menu->pageTitle($other_sectors_id, 'eng');
                    $getEngTitleForChildUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForChildUrl).'/'.$child_page->id;
                    $child_title = $CI->model_menu->pageSubTitle($child_page->id, $lang);
                    if($child_page->id){
                        $html .= '<li class="' . $child_class_active . '"><a href="' . $getEngTitleForChildUrl . '">' . $child_title  . '</a></li>';


                        if($child_page->id){

                            //$html .= '<h2>' . $pageTitle . '</h2>';
                            $childPages = ListingContent($child_page->id);

                            //$getEngTitleForUrl = $CI->model_menu->pageTitle($result->id, 'eng');
                            //$getEngTitleForUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                            $html .= '<ul class="Sub_SiteMap">';
                            foreach ($childPages as $child_page) {
                                if ($child_page->page_id == $page_id) {
                                    $child_class_active = 'active';
                                } else {
                                    $child_class_active = '';
                                }
                                $getEngTitleForChildUrl = $CI->model_menu->pageTitle($child_page->id, 'eng');
                                $getEngTitleForChildUrl = lang_base_url() . 'page/other_sector_detail/' . $child_page->id;
                                $child_title = $CI->model_menu->pageSubTitle($child_page->id, $lang);
                                if($child_page->id){
                                    $html .= '<li class="' . $child_class_active . '"><a href="' . $getEngTitleForChildUrl . '">' . $child_title  . '</a></li>';
                                }

                            }
                            $html .= '</ul>';

                        }
                    }

                }
                $html .= '</ul>';
            }
            else {
                if($result->tpl == 'our_sectors'){
                    $getEngTitleForUrl = $CI->model_menu->pageTitle($other_sectors_id, 'eng');
                }else{
                    $getEngTitleForUrl = $CI->model_menu->pageTitle($result->page_id, 'eng');
                }

                if ($result->tpl == 'services' || $result->tpl == 'asset_management') {

                    $getEngTitleForUrl = $CI->model_menu->pageTitle($result->id, 'eng');
                    $getEngTitleForUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                    $html .= '<div class="column col1"><ul>';
                    $html .= '<li class="heading" ' . $active . '">' . $pageTitle . '</li>';
                    $childPages = ListingContent($result->id);
                    foreach ($childPages as $child_page) {
                        if ($child_page->page_id == $page_id) {
                            $child_class_active = 'active';
                        } else {
                            $child_class_active = '';
                        }
                        $getEngTitleForChildUrl = $CI->model_menu->pageTitle($child_page->id, 'eng');
                        $getEngTitleForChildUrl = lang_base_url() . 'page/   sub_service/' . $child_page->id;
                        $child_title = $CI->model_menu->pageSubTitle($child_page->id, $lang);
                        $html .= '<li class="' . $child_class_active . '"><a href="' . $getEngTitleForChildUrl . '">' . $child_title . '</a></li>';
                    }
                    $html .= '</ul></div>';
                } elseif ($result->tpl == 'who_we_are') {
                    $sectors_page_url = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                    $html .= '<div class="column col1"><ul><li class="heading" ' . $active . '">' . $pageTitle . '</li>' . header_menu('4', $child_page->id) . '</ul></div>';
                } elseif ($result->tpl == 'programs') {
                    $programs_page_url = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                    $html .= '<ul>
                                    <li class="heading">' . $pageTitle . '</li>
                                    <li><a href="' . $programs_page_url . '">' . pageSubTitle($result->page_id, $lang) . '</a></li>
                                    <li><a href="' . $programs_page_url . '?vision">' . content_detail($lang . '_vision_title', $result->page_id) . '</a></li>
                                    <!--<li><a href="' . $programs_page_url . '?initiative">' . content_detail($lang . '_initiative_title', $result->page_id) . '</a></li>-->
                                </ul>';
                }elseif ($result->tpl == 'career') {
                    $html .= '<ul><li class="heading ' . $active . '"><a target="_blank" href="' . content_detail('eng_external_page', $result->page_id) . '">' . $pageTitle . '</a></li></ul>';
                } else {
                    $html .= '<h2><a href="' . lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl) . '">' . $pageTitle . '</a></h2>';
                }
            }
        }
    }

    return $html;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

//$
function footer_menu_new($menu_id, $page_id, $parent_id = '')
{
    $CI = &get_Instance();
    $html = '';
    $lang = $CI->session->userdata('lang');
    $CI->load->model('ems/model_menu');
    $results = $CI->model_menu->fetchMenuPages($menu_id);

    foreach ($results as $result) {
        $active = '';
        if ($result->page_id == $page_id) {
            $active = 'active';
        } elseif ($result->page_id == $parent_id) {
            $active = 'active';
        }
        $pageTitle = $CI->model_menu->pageTitle($result->page_id, $lang);
        $childPages = $CI->model_menu->fetchMenuParrentPages($menu_id, $result->page_id);
        if ($result->parent_id == 0) {
            if ($childPages) {
                $getEngTitleForUrl = $CI->model_menu->pageTitle($result->page_id, 'eng');
                $getEngTitleForUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                $html .= '<ul>';
                $html .= '<li  ' . $active . '"><a href="' . $getEngTitleForUrl . '">' . $pageTitle . '</a></li>
				';
                foreach ($childPages as $child_page) {

                    if ($child_page->page_id == $page_id) {
                        $child_class_active = 'active';
                    } else {
                        $child_class_active = '';
                    }
                    $getEngTitleForChildUrl = $CI->model_menu->pageTitle($child_page->page_id, 'eng');
                    $getEngTitleForChildUrl = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForChildUrl);
                    $child_title = $CI->model_menu->pageTitle($child_page->page_id, $lang);
                    if ($child_page->page_id == 176) {

                        $html .= '<li class="' . $child_class_active . '"><a href="#" data-toggle="modal" data-target="#Newsletter">' . $child_title . '</a></li>';
                    } else {
                        $html .= '<li class="' . $child_class_active . '"><a href="' . $getEngTitleForChildUrl . '">' . $child_title . '</a></li>';
                    }

                }
                $html .= '</ul>';
            } else {
                $getEngTitleForUrl = $CI->model_menu->pageTitle($result->page_id, 'eng');
                if ($result->tpl == 'about') {
                    $about_page_url = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                    $html .= '<ul>
                                    <li >' . $pageTitle . '</li>
                                    <li><a href="' . $about_page_url . '">' . pageTitle($result->page_id, $lang) . '</a></li>
                                    <li><a href="' . $about_page_url . '#history">' . content_detail($lang . '_history_title', $result->page_id) . '</a></li>
                                    <li><a href="' . $about_page_url . '#tealab">' . content_detail($lang . '_teaLab_title', $result->page_id) . '</a></li>
                                    <li><a href="' . $about_page_url . '#quality">' . content_detail($lang . '_quality_title', $result->page_id) . '</a></li>
                                </ul>';
                } elseif ($result->tpl == 'products') {
                    $products_page_url = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                    $html .= '<ul>
                                    <li >' . $pageTitle . '</li>
                                    <li><a href="' . $products_page_url . '">' . pageTitle($result->page_id, $lang) . '</a></li>
                                    <li><a href="' . $products_page_url . '#fproducts">' . content_detail($lang . '_featured_title', $result->page_id) . '</a></li>
                                </ul>';
                } elseif ($result->tpl == 'recipes') {
                    $recipes_page_url = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                    $html .= '<ul>
                                    <li >' . $pageTitle . '</li>
                                    <li><a href="' . $recipes_page_url . '">' . pageTitle($result->page_id, $lang) . '</a></li>
                                </ul>';
                } elseif ($result->tpl == 'campaigns') {
                    $campaigns_page_url = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                    $html .= '<ul>
                                    <li >' . $pageTitle . '</li>
                                    <li><a href="' . $campaigns_page_url . '">' . pageTitle($result->page_id, $lang) . '</a></li>
                                </ul>';
                } elseif ($result->tpl == 'contact') {
                    $contact_page_url = lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl);
                    $html .= '<ul>
                                    <li >' . $pageTitle . '</li>
                                    <li><a href="' . $contact_page_url . '">' . pageTitle($result->page_id, $lang) . '</a></li>
                                </ul>';
                } else {
                    $html .= '<ul><li class="heading ' . $active . '"><a href="' . lang_base_url() . 'page/' . str_replace(' ', '_', $getEngTitleForUrl) . '">' . $pageTitle . '</a></li></ul>';
                }
            }
        }
    }

    return $html;
}

//$
function header_menu($menu_id, $page_id)
{
    $CI = &get_Instance();
    $html = '';
    $lang = $CI->session->userdata('lang');

    $CI->load->model('ems/model_menu');
    $results1 = $CI->model_menu->fetchMenuPages($menu_id);
    $results = $CI->model_menu->fetchMenuPages($menu_id);
    $blog_id = getPageIdbyTemplate('blog');
    $get_parentId = getParentCategoryId($page_id);
    $html .= '';
    $about_us_id = getPageIdbyTemplate('about_us');
    $profile_id = getPageIdbyTemplate('profile');
    $landmarks_id = getPageIdbyTemplate('landmarks');
    $bod_id = getPageIdbyTemplate('bod');
    $our_sectors_id = getPageIdbyTemplate('our_sectors');
    $other_sectors_id = getPageIdbyTemplate('other_sectors');
    $al_murjan_holding_id = getPageIdbyTemplate('murjan_holding');
    $ceo_message_id = getPageIdbyTemplate('ceo_message');
    $events_id = getPageIdbyTemplate('events');
    $other_sector_detail_id = getPageIdbyTemplate('other_sector_detail');


    foreach ($results1 as $resultOuter) {

        $othersector_parentId = getParentCategoryId($get_parentId);
        //var_dump($page_id,$get_parentId);die;

        $aboutus_child = array($ceo_message_id,$bod_id,$profile_id);
        $sector_child = array($al_murjan_holding_id,$other_sectors_id);
        $medai_center_active_methods = array($other_sectors_id);
        if ($resultOuter->page_id == $page_id ) {
            $parent_class_active = 'active';
        } else {
            $parent_class_active = '';
        }
        if ($get_parentId == $events_id && $resultOuter->tpl=='events') {
            $mediaparent_class_active = 'active';
        } else {
            $mediaparent_class_active = '';
        }
        if ( ($page_id == $other_sectors_id && $resultOuter->tpl=='our_sectors')|| ($othersector_parentId == $other_sectors_id && $resultOuter->tpl=='our_sectors')) {
            $sectorparent_class_active = 'active';
        } else {
            $sectorparent_class_active = '';
        }
        ///ahmed remaining this
        if ( (in_array($get_parentId,$aboutus_child) && $resultOuter->tpl=='about_us') || (in_array($page_id,$aboutus_child) && $resultOuter->tpl=='about_us') ) {
            $aboutgparent_class_active = 'active';
        } else {
            $aboutgparent_class_active = '';
        }
        if ( (in_array($get_parentId,$sector_child) && $resultOuter->tpl=='our_sectors') || (in_array($page_id,$sector_child) && $resultOuter->tpl=='our_sectors') ) {
            $oursectorparent_class_active = 'active';
        } else {
            $oursectorparent_class_active = '';
        }

        $pageTitle = $CI->model_menu->pageTitle($resultOuter->page_id, $lang);
        if ($resultOuter->tpl == 'media_center') {
            $news_id = getPageIdbyTemplate('news');
            $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($news_id, 'eng'));
        } else {
            if($resultOuter->tpl == 'our_sectors'){
                $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($other_sectors_id, 'eng'));
            }else{
                $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($resultOuter->page_id, 'eng'));
            }
        }

        if ($resultOuter->parent_id == 0) {

           /* $pageTitle = $CI->model_menu->pageTitle($resultOuter->id, $lang);*/
            $pageFullTitle = $original_page_title = $CI->model_menu->pageTitle($resultOuter->id, $lang);
            $pageTitle = explode(' ',$pageFullTitle);
            $childPagesCheck = $CI->model_menu->fetchMenuParrentPages($menu_id, $resultOuter->id);

            if (false && $childPagesCheck && !empty($childPagesCheck)) {
                $ul_start = '<div  class="dropdown-menu">';
                $ul_end = '</div>';
                $html .= '<li  class="' . $aboutgparent_class_active . '' . $oursectorparent_class_active . ' ' . $sectorparent_class_active . '' . $mediaparent_class_active .'' . $parent_class_active . ' nav-item">
                           <a href="#" class=" ' . $oursectorparent_class_active . ' ' . $sectorparent_class_active . '' . $mediaparent_class_active .''. $parent_class_active . ' nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <hgroup class="MainTopMenuLinks">
                                        <h5>'.$pageTitle[0].'</h5>
                                        <h6>'.$pageTitle[1].'</h6>
                                    </hgroup>
                           </a>' . $ul_start;
                foreach ($results as $result) {

                    if ($resultOuter->id == $result->parent_id) {
                        $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng'));
                        if ($result->page_id == $page_id || $result->page_id == $get_parentId || $result->page_id == $othersector_parentId ) {
                            $active = 'active';
                        } else {
                            $active = '';
                        }

                        $childPages = array();
                        if($result->page_id==$ceo_message_id){
                            $pageTitle = $CI->model_menu->pageSubTitle($result->page_id, $lang);
                        }else{
                            $pageTitle = $CI->model_menu->pageTitle($result->page_id, $lang);
                        }
                        if($result->page_id==1293){
                            $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng')).'?id='.$result->page_id;
                        }else{
                            $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng'));
                        }
                        $html .= '<a href="' . $eng_URL . '" class="dropdown-item ' . $active . '">' . $pageTitle . '</a>';

                    }
                }
                $html .= $ul_end . '</li>';
            }else{
                if($resultOuter->page_id == 7 || $resultOuter->page_id == 26){
                    $html .= '<li  class=" ' . $mediaparent_class_active .' ' . $parent_class_active . '">
                           <a href="' . $eng_URL . '">'.$original_page_title.'</a>
                           </li>';
                }else{
                    if($resultOuter->page_id == 102 || $resultOuter->page_id == 89){
                        $html .= '<li  class=" ' . $parent_class_active . '">
                           <a href="' . $eng_URL . '">'.$original_page_title.'</a>
                           </li>';
                    }else{
                        $html .= '<li  class=" ' . $parent_class_active . ''.$oursectorparent_class_active.' '.$sectorparent_class_active.'">
                           <a href="' . $eng_URL . '">'.$original_page_title.'</a>
                            </li>';
                    }
                }
            }
        }
    }

    $html .= '';
    return $html;
}//$

//mobile menu
function mobile_header_menu($menu_id, $page_id)
{
    $CI = &get_Instance();
    $html = '';
    $lang = $CI->session->userdata('lang');

    $CI->load->model('ems/model_menu');
    $results1 = $CI->model_menu->fetchMenuPages($menu_id);
    $results = $CI->model_menu->fetchMenuPages($menu_id);
    $blog_id = getPageIdbyTemplate('blog');
    $get_parentId = getParentCategoryId($page_id);
    $html .= '';
    $about_us_id = getPageIdbyTemplate('about_us');
    $profile_id = getPageIdbyTemplate('profile');
    $landmarks_id = getPageIdbyTemplate('landmarks');
    $bod_id = getPageIdbyTemplate('bod');
    $our_sectors_id = getPageIdbyTemplate('our_sectors');
    $other_sectors_id = getPageIdbyTemplate('other_sectors');
    $al_murjan_holding_id = getPageIdbyTemplate('murjan_holding');
    $ceo_message_id = getPageIdbyTemplate('ceo_message');
    $events_id = getPageIdbyTemplate('events');
    $ones = array(
        1 => "one",2 => "two",3 => "three",4 => "four", 5 => "five",6 => "six",7 => "seven",8 => "eight",9 => "nine",10 => "ten",11 => "eleven",12 => "twelve",13 => "thirteen",14 => "fourteen",);
    $i=1;
    foreach ($results1 as $resultOuter) {
        $head_number= $ones[$i];

        if ($resultOuter->page_id == $page_id) {
            $parent_class_active = 'active';
        } else {
            $parent_class_active = '';
        }

        $pageTitle = $CI->model_menu->pageTitle($resultOuter->page_id, $lang);
        if ($resultOuter->tpl == 'media_center') {
            $news_id = getPageIdbyTemplate('news');
            $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($news_id, 'eng'));
        } else {
            if($resultOuter->tpl == 'our_sectors'){
                $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($other_sectors_id, 'eng'));
            }else{
                $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($resultOuter->page_id, 'eng'));
            }
        }

        if ($resultOuter->parent_id == 0) {

            /* $pageTitle = $CI->model_menu->pageTitle($resultOuter->id, $lang);*/
            $pageFullTitle = $CI->model_menu->pageSubTitle($resultOuter->id, $lang);
            $pageTitle = explode(' ',$pageFullTitle);
            $childPagesCheck = $CI->model_menu->fetchMenuParrentPages($menu_id, $resultOuter->id);

            if ($childPagesCheck && !empty($childPagesCheck)) {
                $ul_start = '<div id="collapse'.$head_number.'" class="collapse" aria-labelledby="heading'.$head_number.'>" data-parent="#accordion">
                                    <div class="card-body SideBlueNavUL">
                                        <ul>';
                $ul_end = '            </ul>
                                    </div>
                            </div>';
                $html .= '<div class="card SideBlueNav">
                                <div class="card-header" id="heading'.$head_number.'">
                                <h2 class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse'.$head_number.'" aria-expanded="false" aria-controls="collapse'.$head_number.'">
                                            '.$pageFullTitle.'
                                            <span class="fa-stack fa-sm">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </button>
                                    </h2>
                                </div>' . $ul_start;
                foreach ($results as $result) {

                    if ($resultOuter->id == $result->parent_id) {
                        $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng'));
                        $medai_center_active_methods = array('news_detail', 'media_center_event_details');
                        if ($result->page_id == $page_id || $result->page_id == $get_parentId || ($result->tpl == 'media_center' && in_array($CI->router->fetch_method(), $medai_center_active_methods))) {
                            $active = 'active';
                        } else {
                            $active = '';
                        }

                        if ($result->page_id == $page_id) {
                            $active = 'active';
                        } else {
                            $active = '';
                        }

                        $childPages = array();
                        $pageTitle = $CI->model_menu->pageTitle($result->page_id, $lang);
                        if ($result->page_id == $profile_id) {
                            $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($about_us_id, 'eng'));
                        }else {
                            if ($result->page_id == $our_sectors_id) {
                                $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($other_sectors_id, 'eng'));
                            }else{
                                if($result->page_id==1293){
                                    $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng')).'?id='.$result->page_id;
                                }else{
                                    $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng'));
                                }
                                //$eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng'));
                            }

                        }
                        $html .= '<li><a href="' . $eng_URL . '" class="dropdown-item ' . $active . '">' . $pageTitle . '</a></li>';

                    }
                }
                $html .= $ul_end . '</div>';
            }else{
                    $html .= '<div class="card SideBlueNav">
                                <div class="card-header" >
                                    <h2 class="mb-0">
                                        <button onclick="window.location=\'' . $eng_URL . '\'"  class="d-flex align-items-center justify-content-between btn btn-link collapsed" >
                                            '.$pageFullTitle.'
                                        </button>
                                    </h2>
                                </div>
                               </div>';

            }
        }
        $i++;
    }

    $html .= '';
    return $html;
}//$

function footer_menu($menu_id, $page_id)
{
    $CI = &get_Instance();
    $html = '';
    $lang = $CI->session->userdata('lang');

    $CI->load->model('ems/model_menu');
    $results = $CI->model_menu->fetchMenuPages($menu_id);
    $blog_id = getPageIdbyTemplate('blog');
    $careers_id = getPageIdbyTemplate('careers');
    $self_application_id = getPageIdbyTemplate('self');
    $oursector_id = getPageIdbyTemplate('our_sectors');
    $other_sectors_id = getPageIdbyTemplate('other_sectors');
    $get_parentId = getParentCategoryId($page_id);

    foreach ($results as $result) {
        if($result->page_id == $oursector_id){
            $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($other_sectors_id, 'eng'));
        }elseif($result->page_id == $careers_id){
            $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($self_application_id, 'eng'));
        }else{
            $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng'));
        }
        if ($result->page_id == $page_id || $result->page_id == $get_parentId) {
            $active = 'active';
        } else {
            $active = '';
        }

        $childPages = array();
        $pageTitle = $CI->model_menu->pageTitle($result->page_id, $lang);
        $childPages = $CI->model_menu->fetchMenuParrentPages($menu_id, $result->page_id);

        if ($result->parent_id == 0) {

            $page_ids = array();
            $parent_ids = array();
            foreach ($childPages as $child) {
                $page_ids[] = $child->page_id;
                $parent_ids[] = $child->parent_id;
            }

            if (in_array($page_id, $page_ids) || in_array($page_id, $parent_ids)) {
                $active1 = 'active';
            } else {
                $active1 = '';
            }
//            $html .= '<li class="mr-4" ><a class="' . $active . '" href="' . $eng_URL . '">' . $pageTitle . '</a></li>';
            // $html .= '<a class="pb-1 w-50 Width100991 list-group-item-action ' . $active . '" href="' . $eng_URL . '">' . $pageTitle . '</a>';
            $html .= '<li><a href="' . $eng_URL . '">' . $pageTitle . '</a></li>';

        }
    }

    return $html;
}

function footer_second_menu($menu_id, $page_id)
{
    $CI = &get_Instance();
    $html = '';
    $lang = $CI->session->userdata('lang');

    $CI->load->model('ems/model_menu');
    $results = $CI->model_menu->fetchMenuPages($menu_id);
    $blog_id = getPageIdbyTemplate('blog');
    $get_parentId = getParentCategoryId($page_id);
    $html .= '<ul>';
    foreach ($results as $result) {
        $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng'));
        if ($result->page_id == $page_id || $result->page_id == $get_parentId) {
            $active = 'active';
        } else {
            $active = '';
        }

        $childPages = array();
        $pageTitle = $CI->model_menu->pageTitle($result->page_id, $lang);
        $childPages = $CI->model_menu->fetchMenuParrentPages($menu_id, $result->page_id);

        if ($result->parent_id == 0) {

            $page_ids = array();
            $parent_ids = array();
            foreach ($childPages as $child) {
                $page_ids[] = $child->page_id;
                $parent_ids[] = $child->parent_id;
            }

            if (in_array($page_id, $page_ids) || in_array($page_id, $parent_ids)) {
                $active1 = 'active';
            } else {
                $active1 = '';
            }
            $html .= '<li><a class="' . $active . '" href="' . $eng_URL . '">' . $pageTitle . '</a></li>';

        }
    }
    $html .= '</ul>';
    return $html;
}

function about_menu($menu_id, $page_id)
{
    $CI = &get_Instance();
    $html = '';
    $lang = $CI->session->userdata('lang');

    $active = '';
    $CI->load->model('ems/model_menu');
    $results = $CI->model_menu->fetchMenuPages($menu_id);
    $parent1_id = getParentCategoryId($page_id);

    $html .= '<ul class="tabset">';
    foreach ($results as $result) {
        $eng_URL = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($result->page_id, 'eng'));
        if ($result->page_id == $page_id || $result->page_id == $parent1_id) {
            $active = 'active';
        } else {
            $active = '';
        }

        $pageTitle = $CI->model_menu->pageTitle($result->page_id, $lang);

        $html .= '<li class="' . $active . '"><a href="' . $eng_URL . '">' . $pageTitle . '</a></li>';

    }

    $html .= '</ul>';

    return $html;
}

function getClass($numberOfRecords)
{

    if ($numberOfRecords <= 3) {
        return 'three';
    } elseif ($numberOfRecords == 4) {
        return 'four';
    } elseif ($numberOfRecords == 5) {
        return 'five';
    }


}

function fetchLastupdated($table, $col)
{
    $result = '';
    $CI = &get_Instance();
    $CI->load->model('model_custom');
    $lastupdated = $CI->model_custom->fetchLastupdated($table, $col);
    $date = $lastupdated[0]['max(' . $col . ')'];
    if (!empty($date) && $date != "0000-00-00 00:00:00") {
        $result = date('d F, Y', strtotime($date));
    }
    return $result;

}

function fetchContctSubject()
{
    $CI = &get_Instance();
    $CI->load->model('model_custom');
    $result = $CI->model_custom->fetchContactSubject();
    return $result;

}

function getAllData($item_name = '')
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getAllData($item_name);

    return $result;

}

function getAllEvents($category_id, $city_id, $event_date)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getAllEvents($category_id, $city_id, $event_date);

    return $result;

}

function getProductMainCategoryId($sub_sub_cat_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $sub_cat_id = $CI->model_contents->getParentId($sub_sub_cat_id);

    return $cat_id = $CI->model_contents->getParentId($sub_cat_id);
}

function getProductMainCategoryId2($sub_sub_cat_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $sub_cat_id = $CI->model_contents->getParentId($sub_sub_cat_id);
    $cat_id = $CI->model_contents->getParentId($sub_cat_id);
    return $cat_id = $CI->model_contents->getParentId($cat_id);
}

function getParentCategoryId($id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    return $parent_id = $CI->model_contents->getParentId($id);

}

function remove_space($item)
{

    return str_replace(' ', '_', $item);
    //return strtolower(str_replace(' ','_',strtolower($item)));
}


function listing_view($page_id)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $html = '';
    $i = 1;


    foreach ($CI->model_contents->front_end_content($page_id) as $val) {
        $title = ($CI->session->userdata('lang') == 'eng' ? $val->eng_title : $val->arb_title);

        $html .= '<div class="panel panel-default">';
        $html .= '<div class="panel-heading" role="tab" id="heading' . $i . '">';
        $html .= '<h4 class="panel-title">';
        $html .= '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' . $i . '" aria-expanded="true" aria-controls="collapse' . $i . '">';
        $html .= '' . $title . '';
        if ($i == 1) {
            $class = 'in';
        } else {
            $class = '';
        }
        $html .= '</a></h4></div><div id="collapse' . $i . '" class="panel-collapse collapse ' . $class . '" role="tabpanel" aria-labelledby="heading' . $i . '">';
        $html .= '<div class="panel-body">';
        $html .= ($CI->session->userdata('lang') == 'eng' ? content_detail('eng_content', $val->id) : content_detail('arb_content', $val->id));
        $html .= '</div></div></div>';

        $i++;
    }


    return $html;


}

function listing_view_services($ids)
{
    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');
    $data;
    foreach ($ids as $val) {
        $title = ($CI->session->userdata('lang') == 'eng' ? $val['eng_title'] : $val['arb_title']);

        $data .= '<li><div class="img-frame full-third-short"><a href="' . base_url() . 'page/detail/' . str_replace(' ', '_', $val['eng_title']) . '"><img src="' . base_url() . 'assets/script/' . content_detail('bg', $val['id']) . '" alt="Service" width="280" height="124"/></a></div>';
        $data .= '<h4>' . $title . '</h4>';
        $data .= ' <p>' . ($CI->session->userdata('lang') == 'eng' ? content_detail('eng_content', $val['id']) : content_detail('arb_content', $val['id'])) . '</p>';
        $data .= '</li>';
    }


    return $data;


}

function listing_view_projects_search($result)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');
    $data;
    foreach ($result as $val) {
        $title = ($CI->session->userdata('lang') == 'eng' ? $val->eng_title : $val->arb_title);

        $data .= '<li><a href="' . base_url() . 'page/detail/' . str_replace(' ', '_', $val->eng_title) . '"><h1>' . $title . '</h1></a>';
        $data .= '<div class="thumbs_div">';
        $data .= '<a href="#">';
        $data .= '<img src="' . base_url() . 'assets/script/' . content_detail('bg', $val->id) . '" width="150" height="113" alt="' . $title . '"></a>';
        $data .= '</div>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Date' : '?????') . ':&nbsp;</strong>';
        $data .= $val->pro_date . '<br>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Contract No:' : '??? ???:') . '&nbsp;</strong>';
        $data .= content_detail('pro_contract_no', $val->id) . '<br>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Location:' : '??????:') . '&nbsp;</strong>';
        $data .= ($CI->session->userdata('lang') == 'eng' ? $val->eng_location : $val->arb_location) . '<br>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Description:' : '?????:') . '&nbsp;</strong> ';
        $data .= ($CI->session->userdata('lang') == 'eng' ? content_detail('eng_content', $val->id) : $content_detail('arb_content', $val->id)) . '<br>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Major Equipment:' : '???????? ???????:') . '&nbsp;</strong>';
        $data .= ($CI->session->userdata('lang') == 'eng' ? content_detail('eng_major_equip', $val->id) : $content_detail('arb_major_equip', $val->id)) . '<br>';
        $data .= '</li>';
    }


    return $data;


}


function countProjects($page_id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');
    //	$data ;
    return count($CI->model_contents->front_end_content($page_id));


}

function listing_view_projects($page_id)
{

    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');
    $data;
    foreach ($CI->model_contents->front_end_content($page_id) as $val) {
        $title = ($CI->session->userdata('lang') == 'eng' ? $val->eng_title : $val->arb_title);

        $data .= '<li><a href="' . base_url() . 'page/detail/' . str_replace(' ', '_', $val->eng_title) . '"><h1>' . $title . '</h1></a>';
        $data .= '<div class="thumbs_div">';
        $data .= '<a href="#">';
        $data .= '<img src="' . base_url() . 'assets/script/' . content_detail('bg', $val->id) . '" width="150" height="113" alt="' . $title . '"></a>';
        $data .= '</div>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Date' : '?????') . ':&nbsp;</strong>';
        $data .= $val->pro_date . '<br>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Contract No:' : '??? ???:') . '&nbsp;</strong>';
        $data .= content_detail('pro_contract_no', $val->id) . '<br>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Location:' : '??????:') . '&nbsp;</strong>';
        $data .= ($CI->session->userdata('lang') == 'eng' ? $val->eng_location : $val->arb_location) . '<br>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Description:' : '?????:') . '&nbsp;</strong> ';
        $data .= ($CI->session->userdata('lang') == 'eng' ? content_detail('eng_content', $val->id) : $content_detail('arb_content', $val->id)) . '<br>';
        $data .= '<strong style="font-size:14px; font-weight:bold;">' . ($CI->session->userdata('lang') == 'eng' ? 'Major Equipment:' : '???????? ???????:') . '&nbsp;</strong>';
        $data .= ($CI->session->userdata('lang') == 'eng' ? content_detail('eng_major_equip', $val->id) : $content_detail('arb_major_equip', $val->id)) . '<br>';
        $data .= '</li>';
    }


    return $data;


}


function listing_view_clients($page_id)
{


    $CI = &get_Instance();

    $CI->load->model('ems/model_contents');


    $data = '';
    foreach ($CI->model_contents->front_end_content($page_id) as $val) {
        $data .= '<li>';
        $title = ($CI->session->userdata('lang') == 'eng' ? $val->eng_title : $val->arb_title);
        $data .= '<div class="img-frame full-third-short"><img src="' . base_url() . 'assets/script/' . content_detail("bg", $val->id) . '" alt="Client" width="280" height="124"/></div>';
        $data .= '<h4>' . $title . '</h4></li>';

    }


    return $data;


}

function contact_form($page_title = '')
{
    $CI = &get_Instance();
    return '<form enctype="multipart/form-data" action="' . $CI->config->item('base_url') . 'page/submit" method="post" class="cform contact">
               <input type="hidden" name="page_title" value="' . $page_title . '">
			    <ol class="cf-ol">
                    <li id="li--1" class="">
                        <label for="cf_field_1"><span>' . ($CI->session->userdata('lang') == 'eng' ? 'Your Name (required)' : '????') . '</span></label>
                        <input required type="text" name="name" id="cf_field_1" class="single fldrequired" value="" onfocus="clearField(this)" onblur="setField(this)" />
                        <span class="reqtxt">(required)</span></li>
                    <li id="li--2" class="">
                        <label for="cf_field_2"><span>' . ($CI->session->userdata('lang') == 'eng' ? 'Your Email (required)' : '?????? ?????????? ????? ??') . '</span></label>
                        <input required type="email" name="email" id="cf_field_2" class="single fldemail fldrequired" value=""/>
                        <span class="emailreqtxt">(valid email required)</span></li>
                    <li id="li--3" class="">
                        <label for="subject"><span>' . ($CI->session->userdata('lang') == 'eng' ? 'Subject' : '?????') . '</span></label>
                        <input type="text" name="subject" id="cf_field_3" class="single" value=""/>
                    </li>
                    <li id="li--4" class="">
                        <label for="message"><span>' . ($CI->session->userdata('lang') == 'eng' ? 'Your Message' : '??????') . '</span></label>
                        <textarea cols="30" rows="8" name="message" id="cf_field_4" class="area"></textarea>
                    </li>
                </ol>                
                <p class="cf-sb ar-work">
                    <input type="submit" name="sendbutton" id="sendbutton" class="sendbutton" value="' . ($CI->session->userdata('lang') == 'eng' ? 'Send' : '?????') . '" onclick="return cforms_validate("", false)"/>
                </p>
                <input type="hidden" id="lang_contact" value="<?php echo $lang; ?>">
            </form>';

}

function map($lat, $lng, $loc)
{
    $zoom_level = 8;
    if (!empty($lat) and !empty($lng)) {


        $loc = preg_replace('/\s+/', '', $loc);

        ?>
        <div id="ShowMap">
            <script type="text/javascript">


                var locations = [


                    [

                        '<?php echo $loc;?>',

                        <?php echo $lat;?>, <?php echo $lng;?>,

                        1,

                    ]
                ]

                var map = new google.maps.Map(document.getElementById('ShowMap'), {

                    zoom: <?php echo $zoom_level;?>,

                    center: new google.maps.LatLng(<?php echo $lat;?>, <?php echo $lng;?>),

                    mapTypeId: google.maps.MapTypeId.ROADMAP

                });

                var marker, i;

                var markers = new Array();

                for (i = 0; i < locations.length; i++) {


                    var infowindow = new google.maps.InfoWindow();

                    marker = new google.maps.Marker({

                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),

                        map: map

                    });

                    markers.push(marker);

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {

                        return function () {

                            infowindow.setContent(locations[i][0], locations[i][0]);

                            infowindow.open(map, marker);

                        }

                    })(marker, i));

                }

                function showInfo(id) {
                    google.maps.event.trigger(markers[id], 'click');
                    location.hash = "mapLoc";
                }

                /*setInterval(function () {

                 var i = Math.floor(Math.random() * markers.length);

                 google.maps.event.trigger(markers[i], 'click');

                 }, 3000);*/

            </script>
            <a name="mapLoc">&nbsp;</a>
            <div id="loc_map" style="width:100%;height:426px;"></div>

        </div>


        <?php
    }
}

function usre_data($uid)
{

    $CI = &get_Instance();
    $CI->db->select('usr_uname');
    $CI->db->from('cms_users');
    $CI->db->where("id", $uid);
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return 0;
    }

}


function content_detail_AwardsEng($key, $id)
{
    $CI = &get_Instance();
    $CI->db->select('*');
    $CI->db->from('awards');
    $CI->db->where("id", $id);
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        return $query->row()->eng_desc;
    } else {
        return false;
    }

}

function content_detail_objectivesEng($key, $id)
{
    $CI = &get_Instance();
    $CI->db->select('*');
    $CI->db->from('compitition_objectives');
    $CI->db->where("id", $id);
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        return $query->row()->eng_desc;
    } else {
        return false;
    }

}

function content_detail_image($id)
{
    $CI = &get_Instance();
    $CI->db->select('*');
    $CI->db->from('compitition_objectives');
    $CI->db->where("id", $id);
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        return $query->row()->logo_image;
    } else {
        return false;
    }

}

function content_detail_image_awards($id)
{
    $CI = &get_Instance();
    $CI->db->select('*');
    $CI->db->from('awards');
    $CI->db->where("id", $id);
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        return $query->row()->logo_image;
    } else {
        return false;
    }

}

function content_detail_objectivesArb($key, $id)
{

    $CI = &get_Instance();
    $CI->db->select('*');
    $CI->db->from('compitition_objectives');
    $CI->db->where("id", $id);
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        return $query->row()->arb_desc;
    } else {
        return false;
    }

}

function content_detail_AwardsArb($key, $id)
{

    $CI = &get_Instance();
    $CI->db->select('*');
    $CI->db->from('awards');
    $CI->db->where("id", $id);
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        return $query->row()->arb_desc;
    } else {
        return false;
    }

}

function content_detail($key, $id)
{

    $CI = &get_Instance();
    $CI->db->select('*');
    $CI->db->from('content_detail');
    $CI->db->where("meta_key", $key);
    $CI->db->where("post_id", $id);
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {

        return html_entity_decode($query->row()->meta_value);
    } else {
        return false;
    }

}
function companies_content_detail($key, $id)
{
    $data=array();
    $CI = &get_Instance();
    $CI->db->select('*');
    $CI->db->from('companies_map');
    $CI->db->where("id", $id);
    $CI->db->where("post_id", $id);
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        return $query->row($key);
    } else {
        return false;
    }

}

function file_upload($file, $id)
{

    foreach ($file as $key => $val) {
        if (!empty($val['name'])) {
            $file = str_replace($val['name'], $key . '_' . $id . '.jpg', $val['name']);

            $target_path = './uploads/bg/';
            $target_path = $target_path . basename($file);

            if (move_uploaded_file($val['tmp_name'], $target_path)) {
                return true;
            } else {
                return false;
            }
        }
    }

}

function img_explode($arr, $url = true)
{
    preg_match_all('/<img[^>]+>/i', $arr, $result);

    if (!empty($result[0])) {
        $search = array('../../../../', '<p>', '</p>');
        $replace = array(base_url(), '', '');
        $arr = str_replace($search, $replace, $arr);

        $rec = explode('<img', $arr);
        $li = '';
        foreach ($rec as $val) {
            if (!empty($val)) {
                preg_match('/(src=["\'](.*?)["\'])/', $val, $match);  //find src="X" or src='X'

                $li .= '<li>';
                if ($url == true) {
                    $li .= '<a href="' . $match[2] . '" data-group="set1" class="html5lightbox">';
                }
                $li .= '<img ' . $val . '';

                $li .= '</li>';


            }
        }
        return $li;
    }

}

function check_active_compitition()
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');
    $compitition = $CI->model_contents->getActiveCompitition($month, $page_id);
    return $compitition;
}

function getEventsByMonth($month, $page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $lang = $CI->session->userdata('lang');

    $dataEvents = $CI->model_contents->fetchEventsByMonth_front($month, $page_id);

    return $dataEvents;
}

function getAnnouncement()
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_announcement');
    $lang = $CI->session->userdata('lang');

    //$dataAnnouncement = array();


    $dataAnnouncement = $CI->model_announcement->fetchAllAnnouncement_front('announcement');

    return $dataAnnouncement;
}

function get_events($id = NULL)
{

    $CI = &get_Instance();

    $CI->db->select('*');

    $CI->db->from('contents');

    $CI->db->where('id', $id);


    $query = $CI->db->get();

    if ($query->num_rows() > 0) {


        return $query->result();

    } else {

        return false;

    }

}

function getCities($country_code = '', $lang)

{

    $CI = &get_Instance();

    $CI->load->model('model_custom');

    $cities = $CI->model_custom->getCities($country_code);
    //return $cities;
    //Munim Changes
    $selected = "";
    $html_cities = '';
    foreach ($cities as $city) {
        /* if($country_code == $city->countrycode){
            $selected = 'selected';
        }
        else{
            $selected = '';
        } */
        $html_cities .= '<option value="' . ($lang == 'eng' ? $city->eng_name : $city->arb_name) . '" ' . $selected . '>' . ($lang == 'eng' ? $city->eng_name : $city->arb_name) . '</option>';
    }
    echo $html_cities;
}

function getAllCountries()
{
    $CI = &get_Instance();
    $CI->load->model('model_custom');
    $result = $CI->model_custom->getAllCountries();
    return $result;
}

function getCitiesForCountry($country)
{
    $CI = &get_Instance();
    $CI->load->model('model_custom');
    $result = $CI->model_custom->getCitiesForCountry($country);
    return $result;
}

function getCountryNameByCode_cms($id)
{
    $CI = &get_Instance();
    $CI->load->model('model_custom');
    $data = $CI->model_custom->getCountryNameById($id);
    return $data;
}

function getCityNameById_cms($id)
{
    $CI = &get_Instance();
    $CI->load->model('model_custom');
    $data = $CI->model_custom->getCityNameById($id);
    return $data;
}

function getCountryNameById($id, $lang)
{
    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');
    $CI->load->model('model_custom');
    $data = $CI->model_custom->getCountryNameById($id);
    if ($lang == 'eng') {
        $country = $data[0]->eng_title;
    } else {
        $country = $data[0]->arb_title;
    }
    return $country;
}

function getCityNameById($id, $lang)
{
    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');
    $CI->load->model('model_custom');
    $data = $CI->model_custom->getCityNameById($id);
    if ($lang == 'eng') {
        $city = $data[0]->eng_title;
    } else {
        $city = $data[0]->arb_title;
    }
    return $city;
}

function getCategories()
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_formlist');
    $result = $CI->model_formlist->fetchRow();
    return $result;
}

function getPublishedProfiles($cat_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getPublishedProfiles($cat_id);
    return $result;
}

function getCategoryProfiles($cat_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_inquries');
    $result = $CI->model_inquries->getCategoryProfiles($cat_id);
    return $result;
}

function getPublishedProfilesHighestVoting($cat_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getPublishedProfilesHighestVoting($cat_id);
    return $result;
}

function intPart($float)
{
    if ($float < -0.0000001)
        return ceil($float - 0.0000001);
    else
        return floor($float + 0.0000001);
}

function Greg2Hijri($day, $month, $year, $string = false)
{
    $day = (int)$day;
    $month = (int)$month;
    $year = (int)$year;

    if (($year > 1582) or (($year == 1582) and ($month > 10)) or (($year == 1582) and ($month == 10) and ($day > 14))) {
        $jd = intPart((1461 * ($year + 4800 + intPart(($month - 14) / 12))) / 4) + intPart((367 * ($month - 2 - 12 * (intPart(($month - 14) / 12)))) / 12) -
            intPart((3 * (intPart(($year + 4900 + intPart(($month - 14) / 12)) / 100))) / 4) + $day - 32075;
    } else {
        $jd = 367 * $year - intPart((7 * ($year + 5001 + intPart(($month - 9) / 7))) / 4) + intPart((275 * $month) / 9) + $day + 1729777;
    }

    $l = $jd - 1948440 + 10632;
    $n = intPart(($l - 1) / 10631);
    $l = $l - 10631 * $n + 354;
    $j = (intPart((10985 - $l) / 5316)) * (intPart((50 * $l) / 17719)) + (intPart($l / 5670)) * (intPart((43 * $l) / 15238));
    $l = $l - (intPart((30 - $j) / 15)) * (intPart((17719 * $j) / 50)) - (intPart($j / 16)) * (intPart((15238 * $j) / 43)) + 29;

    $month = intPart((24 * $l) / 709);
    $day = $l - intPart((709 * $month) / 24);
    $year = 30 * $n + $j - 30;

    $date = array();
    $date['year'] = $year;
    $date['month'] = $month;
    $date['day'] = $day;

    if (!$string)
        return $date;
    else
        return "{$year}-{$month}-{$day}";
}

function islamic_month($month)
{
    $data = array("Muharram", "Safar", "(Rabi al-Awwal", "Rabi ath-Thani ", "Jumada al-Ula", "Jumada ath-Thaniya", "Rajab", "Sha'ban", "Ramadan", "Shawwal", "Dhu al-Qa'da", "Dhu al-Hijjah");

    return $data[$month];

}

function getProgramImages($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_programs');
    $result = $CI->model_programs->fetchAllImages($page_id, 'programs');
    return $result;
}

function getProgramVideos($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_programs');
    $result = $CI->model_programs->fetchAllVideos($page_id, 'programs');
    return $result;
}

function getProgramFiles($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_programs');
    $result = $CI->model_programs->fetchAllFiles($page_id, 'programs');
    return $result;
}

function getLocation($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getLocation($id);
    return $result;

}

function getAllCountriesEms()
{
    $CI = &get_Instance();
    $CI->load->model('model_frontend_content');
    $result = $CI->model_frontend_content->getAllCountriesEms();
    return $result;
}

function getCitiesEms($country_code)
{
    $CI = &get_Instance();
    $CI->load->model('model_frontend_content');
    $result = $CI->model_frontend_content->getCitiesEms($country_code);
    return $result;
}

function getCitiesForCountryEms($cid)
{
    $CI = &get_Instance();
    $CI->load->model('model_frontend_content');
    $result = $CI->model_frontend_content->getCitiesForCountryEms($cid);
    return $result;
}

function getSingleCityData($id)
{
    $CI = &get_Instance();
    $CI->load->model('model_frontend_content');
    $result = $CI->model_frontend_content->getSingleCityData($id);
    return $result[0];
}

function getSingleCountryData($id)
{
    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');
    $CI->load->model('model_frontend_content');
    $result = $CI->model_frontend_content->getSingleCountryData($id);
    return $result[0];
}

function getCityTitle($id)
{
    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');
    $CI->load->model('model_frontend_content');
    $result = $CI->model_frontend_content->getSingleCityData($id);
    if ($lang == 'eng') {
        $title = $result[0]->eng_title;
    } else {
        $title = $result[0]->arb_title;
    }
    return $title;
}

function getCountryTitle($id)
{
    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');
    $CI->load->model('model_frontend_content');
    $result = $CI->model_frontend_content->getSingleCountryData($id);
    if ($lang == 'eng') {
        $title = $result[0]->eng_title;
    } else {
        $title = $result[0]->arb_title;
    }
    return $title;
}

function getUserData($id)
{
    $CI = &get_Instance();
    $CI->load->model('model_custom');
    $result = $CI->model_custom->getUserData($id);
    return $result[0];
}

function getAllPropertyCities()
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_properties');
    $result = $CI->model_properties->fetchCities();
    return $result;
}

function getAllProperties()
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_properties');
    $result = $CI->model_properties->fetchAll('properties');
    return $result;
}

function getPropertyCity_eng($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_properties');
    $result = $CI->model_properties->fetchCity_eng($id);
    return $result[0]->city;
}

function getPropertyCity_arb($id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_properties');
    $result = $CI->model_properties->fetchCity_arb($id);
    return $result[0]->city_arb;
}

function get_careers($title)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_user');
    $result = $CI->model_user->get_careers($title);
    return $result;
}

function getAllServices()
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->get_all_services();
    return $result;
}

function get_property_types()
{
    $CI = &get_Instance();
    $lang = $CI->session->userdata('lang');
    $CI->load->model('ems/model_properties');
    $result = $CI->model_properties->fetchAll('properties');
    $types_arr = array();
    foreach ($result as $val) {
        if ($lang == 'eng') {
            $types_arr[] = $val->property_type;
        } else {
            $types_arr[] = $val->property_type_arb;
        }
    }
    return $types_arr;
}

function check_captcha($str)
{
    $CI = &get_Instance();
    $word = $CI->session->userdata('captchaWord');
    if (strcmp(strtoupper($str), strtoupper($word)) == 0) {
        return true;
    } else {
        /*$this->form_validation->set_message('check_captcha', 'Please enter correct words!');*/
        return false;
    }
}

function search_events($year, $month, $page_id)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getListingByYearnMonth($year, $month, $page_id);
    return $result;
}

function genPdfThumbnail($source, $target)
{
    $target = dirname($source) . DIRECTORY_SEPARATOR . $target;
    exec("convert \"{$source}[0]\" -colorspace RGB -geometry 200 $target");

    /*$im->setimageformat("jpeg");
    $im->thumbnailimage(160, 120); // width and height
    $im->writeimage($target);
    $im->clear();
    $im->destroy();*/
}

/*New Functions*/
function check_admin_lang()
{
    $CI = &get_Instance();
    $data = array();

    if (!$CI->session->userdata('admin_lang')) {
        $CI->session->set_userdata(array('admin_lang' => 'eng'));
        $admin_lang = $CI->session->userdata('admin_lang');
    } else {
        $admin_lang = $CI->session->userdata('admin_lang');
    }

    $CI->lang->load('ems', $admin_lang);
    $data['label'] = $CI->lang->line('label');
    $data['admin_lang'] = $admin_lang;
    return $data;
}

function parentId($id)
{

    $CI = &get_Instance();
    $CI->db->select('parant_id');
    $CI->db->from('contents');
    $CI->db->where('id', $id);
    $query = $CI->db->get();
    if ($query->num_rows() == 1) {
        $array = $query->result();
        return $array[0]->parant_id;
    } else {
        return 0;
    }
}

function get_pages()
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $move_to_trash = 0;
    $pages = $CI->model_contents->fetchAll($move_to_trash);
    if ($pages) {
        return $pages;
    } else {
        return false;
    }
}

function getuserGrpId($id)
{

    $CI = &get_Instance();

    $CI->db->select("*");

    $CI->db->from("cms_users");

    $CI->db->where("id", $id);

    $qry = $CI->db->get();

    $section = $qry->row();

    return $section->usr_grp_id;

}

function getSectionNameByID($id)
{

    $CI = &get_Instance();

    $CI->db->select("*");

    $CI->db->from("sections");

    $CI->db->where("id", $id);

    $qry = $CI->db->get();

    $section = $qry->row();

    return $section->sec_title;

}

function section_arabic($sec_title)
{
    $sec_title = trim(strip_tags(ucfirst($sec_title)));

    $data = array('Careers' => 'وظائف', 'Configuration' => 'الإعدادات', 'Pages' => 'صفحات المحتوى', 'Menu' => 'القائمة', 'CMS Users' => 'مستخدمين النظام', 'CMS Groups' => 'مجموعات النظام ', 'Log' => 'السجل', 'Statistics' => 'الإحصائيات', 'Social Links' => 'روابط التواصل الإجتماعي', 'Newsletter' => 'النشرة الإخبارية', 'Contact Inquiries' => 'بيانات التواصل', 'Media' => 'Media', 'Newsletter Subscribers' => 'النشرة الإخبارية', 'Category' => 'الفئة', 'Reservations' => 'الحجوزات', 'Contacts' => 'جهات الاتصال', 'Users Applications' => 'تطبيقات المستخدم', 'Users Profiles' => 'ملفات تعريف المستخدم', 'Request Quotation' => 'طلب سعر', 'Self Applications' => 'تطبيقات ذاتية');

    return $data[$sec_title];

}

function translate($lang, $eng, $arb)
{
    $translation = '';
    if ($lang == 'eng') {
        $translation = $eng;
    } else {
        $translation = $arb;
    }
    return $translation;
}
function check_user_grp_id(){
    $CI = &get_Instance();
    $id=$CI->session->userdata('id');
    $CI->db->select("*");
    $CI->db->from("cms_users");
    $CI->db->where('id',$id);
    $query1 = $CI->db->get();
    $result1 = $query1->result_array();
    $grid=$result1[0]['usr_grp_id'];

    return $grid;
}
function check_permission($section_id, $type, $show_error = true)
{
    $CI = &get_Instance();
    $CI->load->model('ems/model_cmsgroups');
    $result = $CI->model_cmsgroups->sec_rights($section_id);
    if ($result) {
        $result = $result[0];
        $is_allowed = false;
        if ($type === 'create') {
            $is_allowed = $result['grp_sec_create'] == 1;
        } elseif ($type === 'read') {
            $is_allowed = $result['grp_sec_read'] == 1;
        } elseif ($type === 'update') {
            $is_allowed = $result['grp_sec_update'] == 1;
        } elseif ($type === 'delete') {
            $is_allowed = $result['grp_sec_delete'] == 1;
        } elseif ($type === 'publish') {
            $is_allowed = $result['grp_sec_pub'] == 1;
        } elseif ($type === 'all') {
            $is_allowed = ($result['grp_sec_create'] == 1 && $result['grp_sec_read'] == 1 && $result['grp_sec_update'] == 1 && $result['grp_sec_delete'] == 1 && $result['grp_sec_pub'] == 1);
        } elseif ($type === 'any') {
            $is_allowed = ($result['grp_sec_create'] == 1 || $result['grp_sec_read'] == 1 || $result['grp_sec_update'] == 1 || $result['grp_sec_delete'] == 1  || $result['grp_sec_pub'] == 1);
        }
    } else {
        $is_allowed = false;
    }

    if ($show_error && !$is_allowed)
    {
        show_error('The action you have requested is not allowed.');
    } else {
        return $is_allowed;
    }
}
function check_page_permission($page_id, $type, $show_error = true)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_cmsgroups');
    $result = $CI->model_cmsgroups->page_rights($page_id);
    if ($result) {
        $result = $result[0];
        $is_allowed = false;
        if ($type === 'allow') {
            $is_allowed = $result['allow'] == 1;
        }
    } else {
        $is_allowed = false;
    }

    if ($show_error && !$is_allowed)
    {
        show_error('The action you have requested is not allowed.');
    } else {
        return $is_allowed;
    }
}
function check_menu_page_permission($page_id, $type, $show_error = true)
{

    $CI = &get_Instance();
    $CI->load->model('ems/model_cmsgroups');
    $result = $CI->model_cmsgroups->page_rights($page_id);
    if ($result) {
        $result = $result[0];
        $is_allowed = false;
        if ($type === 'allow') {
            $is_allowed = $result['allow'] == 1;
        }
    } else {
        $is_allowed = false;
    }

    if ($show_error && !$is_allowed)
    {
        return $is_allowed;
    } else {
        return $is_allowed;
    }
}
function getCategoriesNameAndClass(){
    if($key == 1){
        $category_name =  ($lang == 'eng' ? 'Board of Directors' : 'أعضاء مجلس الإدارة');
        if($page_id!=16){
            $sectionOneClass='fiveInRow';
        }else{
            $sectionOneClass='fourInRow';
        }
    }
    if($key == 2){
        $category_name =  ($lang == 'eng' ? 'Executive Committee' : 'اللجنة التنفيذية');
         if($page_id!=16){
            $sectionTwoClass='';
        }else{
            $sectionTwoClass='fourInRow';
        }
        
    }
    if($key == 3){
        $category_name =  ($lang == 'eng' ? 'Leadership Team' : 'فريق القيادة');
         if($page_id!=16){
            $sectionThreeClass='fiveInRow';
        }else{
            $sectionThreeClass='fourInRow';
        }
        
    }
    if($key == 4){
        $category_name =  ($lang == 'eng' ? 'Investment Advisory Board' : 'المجلس الاستشاري للاستثمار');
        if($page_id!=16){
            $sectionFourClass='fiveInRow';
        }else{
            $sectionFourClass='';
        }
    
    }
    if($key == 5){
        $category_name =  ($lang == 'eng' ? 'Management Team' : 'فريق الإدارة');
        if($page_id!=16){
            $sectionFiveClass='fiveInRow';
        }else{
            $sectionFiveClass='';
        }
    
    }   
}
function check_page_title_repeatation($title,$id){
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->checkPageTitleRepeat($title,$id);
    return $result;
}
function isFlagValue($id){
    $CI = &get_Instance();
    $CI->load->model('ems/model_contents');
    $result = $CI->model_contents->getIsFlagValue($id);
    //echo $this->db->last_query();
    return $result;
}