<?php
error_reporting(0);
if (!defined('BASEPATH'))

    exit('No direct script access allowed');

//<label>Short Brief <br> <small>Words Limit: <span style="color:green">Only first 12 words will appear on Front.</span></small></label>


function upload_video($fieldName, $name, $pre_class, $edit = false, $id='')
{

    $CI = &get_Instance();
    $CI->load->helper('custom');
    $CI->load->library('session');
    $admin_lang = check_admin_lang();
    $video_name = getVideo_file($id);
	
    if ($edit) {
        $remove = '';
        if ($video_name->$name != '') {
            $file_path = base_url() . 'uploads/videos/' . $video_name->$name;
            $pdf_icon = base_url() . 'assets/images/video_icon.png';
            $remove = '<img src="' . base_url() . 'assets/images/delete_forever48dp.png" width="25" height="25" id="remove_' . $name . '"  style="margin-left: -12px; margin-top: 66px; cursor: pointer;">';
            $pdf_text = '';
            $cursor_style = '';
            $width = '90px';
			$height = '90px';
        } else {
			
            $file_path = 'javascript:void(0);';
            $pdf_icon = base_url() . 'assets/admin/assets/images/no-video-icon.png';
            $pdf_text = '<b style="color: red;">' . $admin_lang['label']['file_not_uploaded'] . '.</b>';
            $cursor_style = 'default';
			$width = '110px';
			$height = '100px';
        }
    }

    echo
        '<div class="uk-width-large-1-1 ' . $pre_class . ' ' . ($pre_class ? 'hide' : '') . ' uk-grid-margin uk-row-first">
  ' . ($edit ?
            '<div class="uk-width-medium-1-2" id="filediv_' . $name . '">
    <div class="uk-form-row" id="user_pic_panel">
     <label>' . $fieldName . '</label>
      <a target= "_blank" href="' . $file_path . '">
      <img style="margin-top: 30px; margin-bottom: 30px; width: '.$width.'; max-width: 100%; height: '.$height.'; cursor:'.$cursor_style.';" src="' . $pdf_icon . '">
      </a>
      ' . $remove . '
    </div>
    <span>' . $pdf_text . '</span>
   </div>' : '') . '
   <h3 class="heading_a">' . $fieldName . '</h3>
   <div class="uk-grid">
    <div class="uk-width-1-1">
     <div id="file_upload-drop" class="uk-file-upload">
      <button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$pre_class.'_'.$name.'_btn">' . $admin_lang['label']['content_choose_video'] . '</button>
      <input multiple type="file" name="' . $name . '" id="'.$pre_class.'_'.$name.'_id"  style="display: none;">
      <input type="hidden" value="' . ($edit ? $video_name->$name : '') . '" id="' . $name . '_hdn" name="' . $name . '_hdn">
     </div>
     <div id="file_upload-progressbar" class="uk-progress uk-hidden">
      <div class="uk-progress-bar" style="width:0">0%</div>
     </div>
    </div>
   </div>    
  </div>';

    echo
        '<script>
 $(document).ready(function () {
  $("#'.$pre_class.'_'.$name.'_btn").click(function () {
                $("#'.$pre_class.'_'.$name.'_id").click();
        });
  
  $("#remove_' . $name . '").click(function () {

            if (confirm("' . $admin_lang['label']['delete_file'] . '")) {

                $("#filediv_' . $name . '").html("");

                $("#' . $name . '_hdn").val("");

            }
        });
 
 });
 </script>';
}


function create_textarea($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){

	$CI = &get_Instance();
	$CI->load->helper('custom');
	$CI->load->library('session');
	$admin_lang = check_admin_lang();
	
	if($lang == 'eng'){
		$class = 'class="md-input eng_text"';
	}else{
		$class = 'class="md-input arb_text"';
	}
 	$exp_fieldName = explode(',',$filedName);
	
	echo 
	'<div class="uk-width-medium-1-2 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.($exp_fieldName[1] != ''?'
		<label>'.$exp_fieldName[0].'<br> <small>'.$admin_lang['label']['words_limit'].': <span style="color:green">'.$admin_lang['label']['only_first'].' '.$exp_fieldName[1].' '.$admin_lang['label']['words_appear'].'</span></small></label>
		':'<label>'.$filedName.'</label>').'
		<div class="uk-form-row">
		<textarea id="'.$lang.'_'.$name.'_'.$pre_class.'" name="'.$lang.'_'.$name.'" '.$class.'>'.($edit ? content_detail($lang.'_'.$name,$id) : '').'</textarea>
		</div>
	</div>';
}

function create_companies_textarea($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){

    $CI = &get_Instance();
    $CI->load->helper('custom');
    $CI->load->library('session');
    $admin_lang = check_admin_lang();

    if($lang == 'eng'){
        $class = 'class="md-input eng_text"';
    }else{
        $class = 'class="md-input arb_text"';
    }
    $exp_fieldName = explode(',',$filedName);

    echo
        '<div class="uk-width-medium-1-2 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.($exp_fieldName[1] != ''?'
		<label>'.$exp_fieldName[0].'<br> <small>'.$admin_lang['label']['words_limit'].': <span style="color:green">'.$admin_lang['label']['only_first'].' '.$exp_fieldName[1].' '.$admin_lang['label']['words_appear'].'</span></small></label>
		':'<label>'.$filedName.'</label>').'
		<div class="uk-form-row">
		<textarea id="'.$lang.'_'.$name.'_'.$pre_class.'" name="'.$lang.'_'.$name.'" '.$class.'>'.($edit ? companies_content_detail($lang.'_'.$name,$id) : '').'</textarea>
		</div>
	</div>';
}

function create_datepicker($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	/*var_dump($id,$edit);die;ahmed*/
	if($lang == 'eng'){
		$class = 'class="md-input valid required"';
	}else{
		$class = 'class="md-input valid"';
	}
	if($edit){
	    $saved_date = content_detail($name,$id);
    }else{
        $saved_date='';
    }
	echo '<div class="uk-form-row">
		<label for="kUI_datetimepicker_range_end" class="uk-form-label">'.$filedName.'</label>
		<input class="datetimepickerNew" name="'.$name.'"  value="'.$saved_date.'" autocomplete="off"  />
	</div>';
	
}
function create_datepicker_start_date($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	if($lang == 'eng'){
		$class = 'class="md-input valid required"';
	}else{
		$class = 'class="md-input valid"';
	}
	echo '<div class="uk-form-row">
		<label for="kUI_datetimepicker_range_start" class="uk-form-label">'.$filedName.'</label>
		<input id="start_date_sel" class="kUI_datetimepicker_basic" name="'.$name.'"  value="'.($edit ? get_Date($id, $name) : '').'" autocomplete="off"  />
	</div>';
	
}
function create_datepicker_end_date($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	if($lang == 'eng'){
		$class = 'class="md-input valid required"';
	}else{
		$class = 'class="md-input valid"';
	}
	echo '<div class="uk-form-row">
		<label for="kUI_datetimepicker_range_end" class="uk-form-label">'.$filedName.'</label>
		<input id="end_date_sel" class="kUI_datetimepicker_basic" name="'.$name.'"  value="'.($edit ? get_Date($id, $name) : '').'" autocomplete="off"  />
	</div>';
	
}
function create_datepicker_edit($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	if($lang == 'eng'){
		$class = 'class="md-input valid required"';
	}else{
		$class = 'class="md-input valid"';
	}
	echo '<div class="uk-form-row">
		<label for="kUI_datetimepicker_range_end" class="uk-form-label">'.$filedName.'</label>
		<input id="" name="'.$name.'"  value="'.($edit ? get_Date($id) : '').'"/>
	</div>';
	
}

function create_datepicker_specified_time($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	$date = content_detail($name, $id);
	$CI = &get_Instance();

	if($lang == 'eng'){
		$class = 'class="md-input valid required"';
	}else{
		$class = 'class="md-input valid"';
	}
	
	echo 
	'<div class="uk-width-large-1-2  '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		<label>'.$filedName.'</label>
		<div class="uk-form-row">
		<input type="date" id="eng_'.$name.'" name="'.$name.'" '.$class.' value="'.($edit ? $date : '').'" >
		</div>
	</div>';
	
}

function create_textfield($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	if($lang == 'eng'){
		$class = 'class="md-input required"';
	}else{
		$class = 'class="md-input"';
	}
	
	echo 
	'<div class="uk-width-medium-1-2 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		<label>'.$filedName.'*</label>
			<div class="uk-form-row">
				<input type="text" name="'.$lang.'_'.$name.'" '.$class.' value="'.($edit ? content_detail($lang.'_'.$name,$id) : '').'" >
			</div>
	</div>';
}
function create_datetime($filedName, $name, $lang = 'eng', $pre_class = 'specified_date_time', $edit=false,  $id=''){
	
	if($lang == 'eng'){
		$class = '';
	}else{
		$class = '';
	}
	
	echo 
	'<div class="uk-width-medium-1-2 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		<label>'.$filedName.'</label>
			<div class="uk-form-row">
				<input type="text" id="datepicker" name="'.$lang.'_'.$name.'" '.$class.' value="'.($edit ? content_detail($lang.'_'.$name,$id) : '').'" >
			</div>
	</div>';
}

function create_textfield_not_required($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id='',$sort_value=''){
	if($lang == 'eng'){
		$class = 'class="md-input"';
	}else{
		$class = 'class="md-input"';
	}
	echo 
	'<div class="uk-width-medium-1-2 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		<label">'.$filedName.'</label>
			<div class="uk-form-row">
				<input type="text" name="'.$lang.'_'.$name.'" '.$class.' value="'.($edit ? ($sort_value?$sort_value:content_detail($lang.'_'.$name,$id)) : '').'" >
			</div>
	</div>';
}

function create_videolink($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	$CI = &get_Instance();
	$CI->load->helper('custom');
	$CI->load->library('session');
	$admin_lang = check_admin_lang();
	
	if($lang == 'eng'){
		$class = 'class="md-input"';
	}else{
		$class = 'class="md-input"';
	}
	
	echo 
	'<div class="uk-width-medium-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		<label>'.$filedName.'</label>
		<div class="uk-form-row">
		<input type="text" name="'.$lang.'_'.$name.'" '.$class.' value="'.($edit ? content_detail($lang.'_'.$name,$id) : '').'" >
		<small>'.$admin_lang['label']['content_sample'].':https://www.youtube.com/watch?v=JLDVjN-Dfr0</small>
		</div>
	</div>';
}

function create_externallink($filedName, $name, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	$CI = &get_Instance();
	$CI->load->helper('custom');
	$CI->load->library('session');
	$admin_lang = check_admin_lang();
	
	if($lang == 'eng'){
		$class = 'class="md-input"';
	}else{
		$class = 'class="md-input"';
	}
	
	echo 
	'<div class="uk-width-medium-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		<label>'.$filedName.'</label>
		<div class="uk-form-row">
		<input type="text" name="'.$lang.'_'.$name.'" '.$class.' value="'.($edit ? content_detail($lang.'_'.$name,$id) : '').'" >
		<small>'.$admin_lang['label']['content_sample'].':http://www.example-domain.com</small>
		</div>
	</div>';
}

function create_location($fieldName, $name, $pre_class, $edit = false,  $id=''){

 echo

 '<div class="uk-width-medium-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">

  <label>'.$fieldName.'</label>
  
  <div class="maptip">
	<a href="javascript:void(0);" id="load_map">
		<img src="'.base_url().'assets/images/mapTip.png" width="14" height="23" border="0" alt="" />
	</a>
   </div>

  <div class="uk-form-row">

   <input name="'.$name.'" value="'.($edit ? content_detail($name, $id) : '').'" id="'.$name.'" type="text" class="md-input"/> 

  </div>


 </div>
 
 ';

}

function upload_file($fieldName, $name, $pre_class, $edit=false,  $id=''){
	
    $CI = &get_Instance();
    $CI->load->helper('custom');
    $CI->load->library('session');
    $admin_lang = check_admin_lang();
	
	if($edit){
        $remove = '';
        if(getPdf_file($id) != ''){
			$file_path = base_url().'uploads/pdf/'.getPdf_file($id);
			$ext = pathinfo($file_path, PATHINFO_EXTENSION);
			if($ext == 'pdf'){
				$pdf_icon = base_url().'assets/images/pdf-icon.png';
			}
			elseif($ext == 'doc' || $ext == 'docx'){
				$pdf_icon = base_url().'assets/images/doc-icon.png';
			}
			else{
				$pdf_icon = base_url().'assets/images/doc-icon.png';
			}
			$remove = '<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="25" id="remove_'.$name.'"  style="margin-left: -22px; margin-top: 66px; cursor: pointer;">';
			$pdf_text = '<b style="color: green;">'.$admin_lang['label']['click_to_view_file'].'.</b>';
        }else{
			$file_path = 'javascript:void(0);';
            $pdf_icon = base_url().'assets/images/no-pdf-icon.png';
			$pdf_text = '<b style="color: red;">'.$admin_lang['label']['file_not_uploaded'].'.</b>';
        }
    }

    echo
        '<div class="uk-width-large-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.($edit ?
            '<div class="uk-width-medium-1-2" id="filediv_'.$name.'">
				<div class="uk-form-row" id="user_pic_panel">
					<label>'.$fieldName.'</label>
						<a target= "_blank" href="'.$file_path.'">
						<img style="margin-top: 30px; margin-bottom: 30px;width: 110px; max-width: 100%; height: 100px;" src="'.$pdf_icon.'">
						</a>
						'.$remove.'
						<span>'.$pdf_text.'</span>
				</div>        
			</div>' : '').'
			<h3 class="heading_a">'.$fieldName.'</h3>
			<span style="color:red">'.$admin_lang['label']['eng_file'].'</span>
			<div class="uk-grid">
				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$name.'_btn">'.$admin_lang['label']['choose_file'].'</button>
						<input type="file" name="'.$name.'" id="'.$name.'_id"   style="display: none;">
						<input type="hidden" value="'.($edit ? getPdf_file($id) : '').'" id="'.$name.'_hdn" name="'.$name.'_hdn">
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
			</div>		  
		</div>';
		
	echo
	'<script>
	$(document).ready(function () {
		$("#'.$name.'_btn").click(function () {
                $("#'.$name.'_id").click();
        });
		
		$("#remove_'.$name.'").click(function () {

            if (confirm("'.$admin_lang['label']['delete_file'].'")) {

                $("#filediv_'.$name.'").html("");

                $("#'.$name.'_hdn").val("");

            }
        });
	
	});
	</script>';
}


function upload_file_half_div($fieldName, $name, $pre_class, $edit=false,  $id=''){
	
    $CI = &get_Instance();
    $CI->load->helper('custom');
    $CI->load->library('session');
    $admin_lang = check_admin_lang();
	
	if($edit){
        $remove = '';
        if(getPdf_file($id,$name) != '' && getPdf_file($id,$name) != 0){
			$file_path = base_url().'uploads/pdf/'.getPdf_file($id,$name);
			$ext = pathinfo($file_path, PATHINFO_EXTENSION);
			if($ext == 'pdf'){
				$pdf_icon = base_url().'assets/images/pdf-icon.png';
			}
			elseif($ext == 'doc' || $ext == 'docx'){
				$pdf_icon = base_url().'assets/images/doc-icon.png';
			}
			elseif($ext == ''){
				$pdf_icon = base_url().'assets/images/pdf-icon.png';
				$name = '';
			}
			$remove = '<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="25" id="remove_'.$name.'"  style="margin-left: -22px; margin-top: 66px; cursor: pointer;">';
			$pdf_text = '<b style="color: green;">'.$admin_lang['label']['click_to_view_file'].'.</b>';
        }else{
			$file_path = 'javascript:void(0);';
            $pdf_icon = base_url().'assets/images/no-pdf-icon.png';
			$pdf_text = '<b style="color: red;">'.$admin_lang['label']['file_not_uploaded'].'.</b>';
        }
    }

    echo
        '<div class="uk-width-large-1-2 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.($edit ?
            '<div class="uk-width-medium-1-2" id="filediv_'.$name.'">
				<div class="uk-form-row" id="user_pic_panel">
					<label>'.$fieldName.'</label>
						<a target= "_blank" href="'.$file_path.'">
						<img style="margin-top: 30px; margin-bottom: 30px;width: 110px; max-width: 100%; height: 100px;" src="'.$pdf_icon.'">
						</a>
						'.$remove.'
						<p style="height:22px;" class="'.$name.'_btn">'.$pdf_text.'</p>
				</div>        
			</div>' : '').'
			<h3 class="heading_a">'.$fieldName.'</h3>
			<span style="color:red">'.$admin_lang['label']['eng_file'].'</span>
			<div class="uk-grid">
				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$name.'_btn">'.$admin_lang['label']['choose_file'].'</button>
						<input type="file" name="'.$name.'" id="'.$name.'_id"   style="display: none;" accept=".pdf">
						<input type="hidden" value="'.($edit ? getPdf_file($id,$name) : '').'" id="'.$name.'_hdn" name="'.$name.'_hdn">
						<p style="color:#2196f3; margin-top: 5px; background-color: #adff2f9c;" id="'.$name.'_name_id"></p>
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
			</div>		  
		</div>';
		
	echo
	'<script>
	$(document).ready(function () {
		$("#'.$name.'_btn").click(function () {
            $("#'.$name.'_id").click();
        });
		
		$("#remove_'.$name.'").click(function () {

            if (confirm("'.$admin_lang['label']['delete_file'].'")) {

                $("#filediv_'.$name.'").html("");

                $("#'.$name.'_hdn").val("");

            }
        });
		
		$("#'.$name.'_id").change(function(e){
            var fileName = e.target.files[0].name;
            $("#'.$name.'_name_id").html(fileName);
			$(".'.$name.'_btn").html("");
        });
	
	});
	</script>';
}

function upload_file_arb($fieldName, $name, $pre_class, $edit=false,  $id=''){

    $CI = &get_Instance();
    $CI->load->helper('custom');
    $CI->load->library('session');
    $admin_lang = check_admin_lang();

    if($edit){
        $remove = '';
        if(getPdf_file_arb($id) != ''){
			$file_path_arb = base_url().'uploads/pdf/'.getPdf_file_arb($id);
			$ext = pathinfo($file_path_arb, PATHINFO_EXTENSION);
			if($ext == 'pdf'){
				$pdf_icon = base_url().'assets/images/pdf-icon.png';
			}
			elseif($ext == 'doc' || $ext == 'docx'){
				$pdf_icon = base_url().'assets/images/doc-icon.png';
			}
			else{
				$pdf_icon = base_url().'assets/images/doc-icon.png';
			}
			
			$remove = '<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="25" id="remove_'.$name.'"  style="margin-left: -22px; margin-top: 66px; cursor: pointer;">';
			$pdf_text = '<b style="color: green;">'.$admin_lang['label']['click_to_view_file'].'.</b>';
        }else{
			$file_path_arb = 'javascript:void(0);';
            $pdf_icon = base_url().'assets/images/no-pdf-icon.png';
			$pdf_text = '<b style="color: red;">'.$admin_lang['label']['file_not_uploaded'].'.</b>';
        }
    }

    echo
        '<div class="uk-width-large-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.($edit ?
            '<div class="uk-width-medium-1-2" id="filediv_'.$name.'">
				<div class="uk-form-row" id="user_pic_panel">
					<label>'.$fieldName.'</label>
						<a target= "_blank" href="'.$file_path_arb.'">
						<img style="margin-top: 30px; margin-bottom: 30px;width: 110px; max-width: 100%; height: 100px;" src="'.$pdf_icon.'">
						</a>
						'.$remove.'
						<span>'.$pdf_text.'</span>
				</div>        
			</div>' : '').'
			<h3 class="heading_a">'.$fieldName.'</h3>
			<span style="color:red">'.$admin_lang['label']['arb_file'].'</span>
			<div class="uk-grid">
				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$name.'_btn">'.$admin_lang['label']['choose_file'].'</button>
						<input type="file" name="'.$name.'" id="'.$name.'_id"   style="display: none;">
						<input type="hidden" value="'.($edit ? getPdf_file_arb($id) : '').'" id="'.$name.'_hdn" name="'.$name.'_hdn">
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
			</div>		  
		</div>';
		
	echo
	'<script>
	$(document).ready(function () {
		$("#'.$name.'_btn").click(function () {
                $("#'.$name.'_id").click();
        });
		
		$("#remove_'.$name.'").click(function () {

            if (confirm("'.$admin_lang['label']['delete_file'].'")) {

                $("#filediv_'.$name.'").html("");

                $("#'.$name.'_hdn").val("");

            }
        });
	
	});
	</script>';
}

function upload_file_arb_half_div($fieldName, $name, $pre_class, $edit=false,  $id=''){

    $CI = &get_Instance();
    $CI->load->helper('custom');
    $CI->load->library('session');
    $admin_lang = check_admin_lang();

    if($edit){
        $remove = '';
        if(getPdf_file($id,$name) != ''){
			$file_path_arb = base_url().'uploads/pdf/'.getPdf_file($id,$name);
			$ext = pathinfo($file_path_arb, PATHINFO_EXTENSION);
			if($ext == 'pdf'){
				$pdf_icon = base_url().'assets/images/pdf-icon.png';
			}
			elseif($ext == 'doc' || $ext == 'docx'){
				$pdf_icon = base_url().'assets/images/doc-icon.png';
			}
			elseif($ext == ''){
				$pdf_icon = base_url().'assets/images/pdf-icon.png';
				$name = '';
			}
			
			$remove = '<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="25" id="remove_'.$name.'"  style="margin-left: -22px; margin-top: 66px; cursor: pointer;">';
			$pdf_text = '<b style="color: green;">'.$admin_lang['label']['click_to_view_file'].'.</b>';
        }else{
			$file_path_arb = 'javascript:void(0);';
            $pdf_icon = base_url().'assets/images/no-pdf-icon.png';
			$pdf_text = '<b style="color: red;">'.$admin_lang['label']['file_not_uploaded'].'.</b>';
        }
    }

    echo
        '<div class="uk-width-large-1-2 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.($edit ?
            '<div class="uk-width-medium-1-2" id="filediv_'.$name.'">
				<div class="uk-form-row" id="user_pic_panel">
					<label>'.$fieldName.'</label>
						<a target= "_blank" href="'.$file_path_arb.'">
						<img style="margin-top: 30px; margin-bottom: 30px;width: 110px; max-width: 100%; height: 100px;" src="'.$pdf_icon.'">
						</a>
						'.$remove.'
						<p style="height:22px;" class="'.$name.'_btn">'.$pdf_text.'</p>
				</div>        
			</div>' : '').'
			<h3 class="heading_a">'.$fieldName.'</h3>
			<span style="color:red">'.$admin_lang['label']['arb_file'].'</span>
			<div class="uk-grid">
				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$name.'_btn">'.$admin_lang['label']['choose_file'].'</button>
						<input type="file" name="'.$name.'" id="'.$name.'_id"   style="display: none;"  accept=".pdf">
						<input type="hidden" value="'.($edit ? getPdf_file($id,$name) : '').'" id="'.$name.'_hdn" name="'.$name.'_hdn">
						<p style="color:#2196f3; margin-top: 5px; background-color: #adff2f9c;" id="'.$name.'_name_id"></p>
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
			</div>		  
		</div>';
		
	echo
	'<script>
	$(document).ready(function () {
		$("#'.$name.'_btn").click(function () {
                $("#'.$name.'_id").click();
        });
		
		$("#remove_'.$name.'").click(function () {

            if (confirm("'.$admin_lang['label']['delete_file'].'")) {

                $("#filediv_'.$name.'").html("");

                $("#'.$name.'_hdn").val("");

            }
        });
		
		$("#'.$name.'_id").change(function(e){
            var fileName = e.target.files[0].name;
            $("#'.$name.'_name_id").html(fileName);
			$(".'.$name.'_btn").html("");
        });
	
	});
	</script>';
}

function create_image($fieldName, $name, $dimensions, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	$CI = &get_Instance();
	$CI->load->helper('custom');
	$CI->load->library('session');
	$admin_lang = check_admin_lang();
	
	if($edit){
		$scr = '';
		$remove = '';
		if(content_detail($lang.'_'.$name.'_mkey_hdn',$id) != ''){
			$scr = base_url().'assets/script/'.content_detail($lang.'_'.$name.'_mkey_hdn',$id);
			$remove = '<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="25" onclick="removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')"  style="margin-left: -6px; margin-top: 80px; cursor: pointer;">';
			$imgClass = 'img_thumb';
		}else{
			$scr = base_url().'assets/images/noImage.jpg';
			$imgClass = 'no_img';
		}
		
		$addImage = '';	
	}
	else{
		$addImage = '<div style="margin-top:10px;" class="showSelectImage_'.$lang.'_'.$name.'"><img class="img_thumb " width="200" height="200" src="'.base_url().'assets/images/noImage.jpg'.'"></img></div>';
	}

	echo '<div id="returned_'.$lang.'_'.$name.'" style="display:none;"></div>';
	
	echo
	'<div class="uk-width-large-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.$addImage.'
		'.($edit ? 
			'<div class="uk-width-medium-1-2" id="thumbdiv_'.$lang.'_'.$name.'">
				<div class="uk-form-row" id="user_pic_panel">
					<label>'.$fieldName.'</label>
						<img class="'.$imgClass.'" style="margin-top: 30px; margin-bottom: 30px; background: #f5f5f5;" src="'.$scr.'" width="100">
						'.$remove.'
				</div>        
			</div>' : '').'
			<h3 class="heading_a">'.$fieldName.'</h3>
			<span style="color:red">'.$dimensions.'</span>
			<div class="uk-grid">
				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$lang.'_'.$name.'_btn">'.$admin_lang['label']['content_choose_image'].'</button>
						<input type="hidden" value="'.($edit ? base_url().'assets/script/'.content_detail($lang.'_'.$name.'_mkey_hdn',$id) : '').'" id="'.$lang.'_'.$name.'_mkey_hdn" name="'.$lang.'_'.$name.'_mkey_hdn">
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
			</div>		  
		</div>';
	
	echo
	'<script>
	$("#'.$lang.'_'.$name.'_btn").mlibready({returnto:\'#returned_'.$lang.'_'.$name.'\', maxselect:1});
	$("#returned_'.$lang.'_'.$name.'").bind("DOMSubtreeModified",function(){
			 
			var new_val = $("#returned_'.$lang.'_'.$name.'").html();
			  if( $("#thumbdiv_'.$lang.'_'.$name.'").length != 0 ) {				
	
			$( "#thumbdiv_'.$lang.'_'.$name.'" ).html("<div class=\"uk-form-row\" id=\"user_pic_panel\"><label>'.$fieldName.'</label><img class=\"img_thumb\" src=\""+new_val+"\"><img onclick=\"removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')\" src=\"'.base_url().'assets/images/delete_forever48dp.png\" width=\"25\" height=\"20\"  style=\"margin-left: -6px; margin-top: 80px; cursor: pointer;\"></div>");
		}
						
		$("#'.$lang.'_'.$name.'_mkey_hdn").val(new_val);
		if(new_val){
			$(".showSelectImage_'.$lang.'_'.$name.' img").attr("src", new_val);			   			   
		}
		 //alert(new_val)  
		});
	</script>';
}

function create_image_half_div($fieldName, $name, $dimensions, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	$CI = &get_Instance();
	$CI->load->helper('custom');
	$CI->load->library('session');
	$admin_lang = check_admin_lang();
	
	if($edit){
		$scr = '';
		$remove = '';
		if(content_detail($lang.'_'.$name.'_mkey_hdn',$id) != ''){
			$scr = base_url().'assets/script/'.content_detail($lang.'_'.$name.'_mkey_hdn',$id);
			$remove = '<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="25" onclick="removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')"  style="margin-left: -6px; margin-top: 80px; cursor: pointer;">';
			$imgClass = 'img_thumb';
		}else{
			$scr = base_url().'assets/images/noImage.jpg';
			$imgClass = 'no_img';
		}
	}
	echo '<div id="returned_'.$lang.'_'.$name.'" style="display:none;"></div>';
	
	echo
	'<div class="uk-width-large-1-2 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.($edit ? 
			'<div class="uk-width-medium-1-2" id="thumbdiv_'.$lang.'_'.$name.'">
				<div class="uk-form-row" id="user_pic_panel">
					<label>'.$fieldName.'</label>
						<img class="'.$imgClass.'" style="margin-top: 30px; margin-bottom: 30px; background: #f5f5f5;" src="'.$scr.'" width="100">
						'.$remove.'
				</div>        
			</div>' : '').'
			<h3 class="heading_a">'.$fieldName.'</h3>
			<span style="color:red">'.$dimensions.'</span>
			<div class="uk-grid">
				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$lang.'_'.$name.'_btn">'.$admin_lang['label']['content_choose_image'].'</button>
						<input type="hidden" value="'.($edit ? base_url().'assets/script/'.content_detail($lang.'_'.$name.'_mkey_hdn',$id) : '').'" id="'.$lang.'_'.$name.'_mkey_hdn" name="'.$lang.'_'.$name.'_mkey_hdn">
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
			</div>		  
		</div>';
	
	echo
	'<script>
	$("#'.$lang.'_'.$name.'_btn").mlibready({returnto:\'#returned_'.$lang.'_'.$name.'\', maxselect:1});
	$("#returned_'.$lang.'_'.$name.'").bind("DOMSubtreeModified",function(){
			 
			var new_val = $("#returned_'.$lang.'_'.$name.'").html();
			  if( $("#thumbdiv_'.$lang.'_'.$name.'").length != 0 ) {				
	
			$( "#thumbdiv_'.$lang.'_'.$name.'" ).html("<div class=\"uk-form-row\" id=\"user_pic_panel\"><label>'.$fieldName.'</label><img class=\"img_thumb\" src=\""+new_val+"\"><img onclick=\"removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')\" src=\"'.base_url().'assets/images/delete_forever48dp.png\" width=\"25\" height=\"20\"  style=\"margin-left: -6px; margin-top: 80px; cursor: pointer;\"></div>");
		}
						
		$("#'.$lang.'_'.$name.'_mkey_hdn").val(new_val);			   			   
		   
		});
	</script>';
}

function create_image_objectives($fieldName, $name, $dimensions, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	$CI = &get_Instance();
	$CI->load->helper('custom');
	$CI->load->library('session');
	$admin_lang = check_admin_lang();
	
	if($edit){
		$scr = '';
		$remove = '';
		if(content_detail_image($id) != ''){
			
			$scr = content_detail_image($id);
			$remove = '<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="25" onclick="removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')"  style="margin-left: -6px; margin-top: 80px; cursor: pointer;">';
			$imgClass = 'img_thumb';
		}else{
			$scr = base_url().'assets/images/noImage.jpg';
			$imgClass = 'no_img';
		}
	}
	echo '<div id="returned_'.$lang.'_'.$name.'" style="display:none;"></div>';
	
	echo
	'<div class="uk-width-large-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.($edit ? 
			'<div class="uk-width-medium-1-2" id="thumbdiv_'.$lang.'_'.$name.'">
				<div class="uk-form-row" id="user_pic_panel">
					<label>'.$fieldName.'</label>
						<img class="'.$imgClass.'" style="margin-top: 30px; margin-bottom: 30px; background: #f5f5f5;" src="'.$scr.'" width="100">
						'.$remove.'
				</div>        
			</div>' : '').'
			<h3 class="heading_a">'.$fieldName.'</h3>
			<span style="color:red">'.$dimensions.'</span>
			<div class="uk-grid">
				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$lang.'_'.$name.'_btn">'.$admin_lang['label']['content_choose_image'].'</button>
						<input type="hidden" value="'.($edit ? content_detail_image($id) : '').'" id="'.$lang.'_'.$name.'_mkey_hdn" name="'.$lang.'_'.$name.'_mkey_hdn">
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
			</div>		  
		</div>';
	
	echo
	'<script>
	$("#'.$lang.'_'.$name.'_btn").mlibready({returnto:\'#returned_'.$lang.'_'.$name.'\', maxselect:1});
	$("#returned_'.$lang.'_'.$name.'").bind("DOMSubtreeModified",function(){
			 
			var new_val = $("#returned_'.$lang.'_'.$name.'").html();
			  if( $("#thumbdiv_'.$lang.'_'.$name.'").length != 0 ) {				
	
			$( "#thumbdiv_'.$lang.'_'.$name.'" ).html("<div class=\"uk-form-row\" id=\"user_pic_panel\"><label>'.$fieldName.'</label><img class=\"img_thumb\" src=\""+new_val+"\"><img onclick=\"removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')\" src=\"'.base_url().'assets/images/delete_forever48dp.png\" width=\"25\" height=\"20\"  style=\"margin-left: -6px; margin-top: 80px; cursor: pointer;\"></div>");
		}
						
		$("#'.$lang.'_'.$name.'_mkey_hdn").val(new_val);			   			   
		   
		});
	</script>';
}

function create_image_awards($fieldName, $name, $dimensions, $lang = 'eng', $pre_class, $edit=false,  $id=''){
	
	$CI = &get_Instance();
	$CI->load->helper('custom');
	$CI->load->library('session');
	$admin_lang = check_admin_lang();
	
	if($edit){
		$scr = '';
		$remove = '';
		if(content_detail_image_awards($id) != ''){
			
			$scr = content_detail_image_awards($id);
			$remove = '<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="25" onclick="removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')"  style="margin-left: -6px; margin-top: 80px; cursor: pointer;">';
			$imgClass = 'img_thumb';
		}else{
			$scr = base_url().'assets/images/noImage.jpg';
			$imgClass = 'no_img';
		}
	}
	echo '<div id="returned_'.$lang.'_'.$name.'" style="display:none;"></div>';
	
	echo
	'<div class="uk-width-large-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
		'.($edit ? 
			'<div class="uk-width-medium-1-2" id="thumbdiv_'.$lang.'_'.$name.'">
				<div class="uk-form-row" id="user_pic_panel">
					<label>'.$fieldName.'</label>
						<img class="'.$imgClass.'" style="margin-top: 30px; margin-bottom: 30px; background: #f5f5f5;" src="'.$scr.'" width="100">
						'.$remove.'
				</div>        
			</div>' : '').'
			<h3 class="heading_a">'.$fieldName.'</h3>
			<span style="color:red">'.$dimensions.'</span>
			<div class="uk-grid">
				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$lang.'_'.$name.'_btn">'.$admin_lang['label']['content_choose_image'].'</button>
						<input type="hidden" value="'.($edit ? content_detail_image_awards($id) : '').'" id="'.$lang.'_'.$name.'_mkey_hdn" name="'.$lang.'_'.$name.'_mkey_hdn">
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
			</div>		  
		</div>';
	
	echo
	'<script>
	$("#'.$lang.'_'.$name.'_btn").mlibready({returnto:\'#returned_'.$lang.'_'.$name.'\', maxselect:1});
	$("#returned_'.$lang.'_'.$name.'").bind("DOMSubtreeModified",function(){
			 
			var new_val = $("#returned_'.$lang.'_'.$name.'").html();
			  if( $("#thumbdiv_'.$lang.'_'.$name.'").length != 0 ) {				
	
			$( "#thumbdiv_'.$lang.'_'.$name.'" ).html("<div class=\"uk-form-row\" id=\"user_pic_panel\"><label>'.$fieldName.'</label><img class=\"img_thumb\" src=\""+new_val+"\"><img onclick=\"removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')\" src=\"'.base_url().'assets/images/delete_forever48dp.png\" width=\"25\" height=\"20\"  style=\"margin-left: -6px; margin-top: 80px; cursor: pointer;\"></div>");
		}
						
		$("#'.$lang.'_'.$name.'_mkey_hdn").val(new_val);			   			   
		   
		});
	</script>';
}


function create_gallery($fieldName, $name, $dimensions, $lang = 'eng', $pre_class, $edit = false,  $id=''){

    $CI = &get_Instance();
    $CI->load->helper('custom');
    $CI->load->library('session');
    $admin_lang = check_admin_lang();

    echo '<div id="returned_'.$lang.'_'.$name.'" style="display:none;"></div>';

    if(!$edit){
        $style = 'style="width: 100%"; display:contents; ';
        echo
            '<div class="uk-width-large-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
				<h3 class="heading_a">'.$fieldName.'</h3>           
				<span style="color:red">('.$dimensions.' '.$admin_lang['label']['pixels'].')</span>
				<div class="uk-grid">
				<div class="galleryImages_'.$lang.'_'.$name.'" style="width: 100%"></div>
				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$lang.'_'.$name.'_btn">'.$admin_lang['label']['choose_images'].'</button>
						<input type="hidden" value="'.$lang.'_'.$name.'" id="'.$lang.'_'.$name.'" name="'.$lang.'_'.$name.'">
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
				</div>
			</div>';

    }else{
        $style = 'style="display:contents"';
        echo

            '<div class="uk-width-large-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">

		<h3 class="heading_a">'.$fieldName.' <a class="md-btn md-btn-primary autoTooltip page_order_images" href="'.base_url().'ems/content/gallerySorting/'.$lang.'_'.$name.'/'.$id.'" title="'.$admin_lang['label']['tooltip_ordering'].'">
		'.$admin_lang['label']['sorting_click'].'
		</a></h3> 
		
		<span style="color:red">'.$dimensions.'</span>		
		
		<div class="uk-form-row gallery-images-eng gallery_video_block">';



        $photos =  content_detail($lang.'_'.$name,$id);
        $photos = str_replace('eng_'.$name.',','',$photos);
        $photos_array = explode(',',$photos);

        if(!empty($photos_array)){

            foreach ($photos_array as $imageData) {

                if(!empty($imageData) && $imageData !='eng_event_slider_images' && $imageData !='eng_news_images' && $imageData !='eng_client_gallery' && $imageData !='eng_gallery_images' && $imageData !='eng_project_gallery' && $imageData !='eng_media_gallery_images' && $imageData !='eng_project_gallery_images' && $imageData !='eng_services_gallery' && $imageData !='eng_gallery_media'  && $imageData !='eng_slider_event'){

                    $image = explode('||',$imageData)[0];
                    if(strpos($image, '___') !== false){

                        $imageFinal = explode('___',$image)[1];
                    } else{
                        $imageFinal = $image;
                    }

                    $video = explode('||',$imageData)[1];
                    if($imageFinal != ''){
                        echo
                            '<div class="row_data bgImg" style="display: inline-block; margin-right: 8px; vertical-align: top;">
							<div class="user_pic_panel" id="user_pic_panel">
								<div class="user_pic_wrap_">
									<img class="img_thumb" src="'.base_url().'assets/script/'.$imageFinal.'" alt="Slider Image">
								</div>
							</div>
							<div class="close_button">
								<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="20" style="margin-'.($admin_lang['admin_lang'] == 'eng'?'left':'right').': 107px; margin-top: -40px; cursor: pointer;"/>
							</div>
						</div>';
                    }

                }

            }
            echo '<div class="galleryImages_'.$lang.'_'.$name.'" '.$style.'></div>';
        }



        echo
            '<div class="uk-grid">
			<div class="uk-width-1-1">
			<div id="file_upload-drop" class="uk-file-upload">
			<button id="'.$lang.'_'.$name.'_btn" class="uk-form-file md-btn md-btn-primary" type="button" id="user_file">'.$admin_lang['label']['choose_images'].'</button>
			<input type="hidden" value="'.$photos.'" id="'.$lang.'_'.$name.'" name="'.$lang.'_'.$name.'">
			</div>
			<div id="file_upload-progressbar" class="uk-progress uk-hidden">
				<div class="uk-progress-bar" style="width:0">0%</div>
			</div>
			<div class="gallery_image_previews" style="display: none;"></div>
			</div>
			</div>
			
			</div></div>';

    }



    echo

        '<script>

	$("#'.$lang.'_'.$name.'_btn").mlibready({returnto:"#returned_'.$lang.'_'.$name.'", maxselect:99});

	

	var count_image_thumb = 0;

	$("#returned_'.$lang.'_'.$name.'").bind("DOMSubtreeModified",function(){

	 

	  var vaule = new Array();

	  var val = $("#'.$lang.'_'.$name.'").val();

	  var new_val = $("#returned_'.$lang.'_'.$name.'").html();

	  new_val = new_val.split(",");

	  for (i = 0; i < new_val.length; i++) { 

			text = new_val[i];

			new_text =  text.split("script/");

			vaule[i] =  new_text[1];
			//alert(vaule[i]);
			if( $("#thumbdiv_'.$lang.'_'.$name.'").length != 0 ) {
				//alert("if");
				$("#thumbdiv_'.$lang.'_'.$name.'").append( "<div class=\"row_data bgImg\"><div class=\"user_pic_panel\" id=\"user_pic_panel\"><div class=\"user_pic_wrap_\"><img class=\"img_thumb\" src=\"'.base_url().'assets/script/"+vaule[i]+"\"></div></div><div class=\"close_button\"><img onclick=\"removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')\" src=\"'.base_url().'assets/images/delete_forever48dp.png\" width=\"19\" height=\"17\"></div></div>" );

			}
			else{
				//alert("else");
				if(vaule[i]){
					$(".galleryImages_'.$lang.'_'.$name.'").append( "<div class=\"row_data bgImg\" style=\"margin: 0px 10px 0px 0px; display:inline-block\"><div class=\"user_pic_panel\" id=\"user_pic_panel\"><div class=\"user_pic_wrap_\"><img class=\"img_thumb\" src=\"'.base_url().'assets/script/"+vaule[i]+"\"></div></div><div class=\"close_button\"><img class=\"removeImageAdded\" style=\"margin-left: 107px; margin-top: -40px; cursor: pointer; \" data-id="+vaule[i]+"  data-id-field=\"'.$lang.'_'.$name.'\" src=\"'.base_url().'assets/images/delete_forever48dp.png\" width=\"25\" height=\"20\"></div></div>" );
				}
			}

		} 

		  new_val =  vaule.join(); 

		  

	  if(val != \'\'){

		  val = val + \',\'+  new_val ;  

		  }else{

		   val = new_val ;	  

			  }

	  

	   $("#'.$lang.'_'.$name.'").val(val);
	   //$("#'.$lang.'_'.$name.'").parent().parent().find(".gallery_image_previews").html(val).show();



	   count_image_thumb++;

	});

	

	$(".uk-form-row").on("click",".close_button img", function() {

				

		if(confirm("'.$admin_lang['label']['delete_image'].'")){ 

		$(this).parents(".row_data").remove();

		var imgsrcs = "";

		$(".gallery-images-eng img.img_thumb").each(function (index, element){

			var value = $(this).attr(\'src\');

			value = value.split("script/");

			new_value = value[1];

			if(imgsrcs!=="") imgsrcs += ",";

			imgsrcs += new_value;
			//alert(imgsrcs);

		});
		$("#'.$lang.'_'.$name.'").val(imgsrcs);
		//alert(imgsrcs);

		}

	});

	

	</script>';

}


function create_gallery_new($fieldName, $name, $dimensions, $lang = 'eng', $pre_class, $edit = false,  $id=''){

	$CI = &get_Instance();
	$CI->load->helper('custom');
	$CI->load->library('session');
	$admin_lang = check_admin_lang();

	echo '<div id="returned_'.$lang.'_'.$name.'" style="display:none;"></div>';

	if(!$edit){

		echo
			'<div class="uk-width-large-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">
				<h3 class="heading_a">'.$fieldName.'</h3>           
				<span style="color:red">('.$dimensions.' '.$admin_lang['label']['pixels'].')</span>
				<div class="uk-grid">

				<div class="uk-width-1-1">
					<div id="file_upload-drop" class="uk-file-upload">
						<button class="uk-form-file md-btn md-btn-primary" type="button" id="'.$lang.'_'.$name.'_btn">'.$admin_lang['label']['choose_images'].'</button>
						<input type="hidden" value="'.$lang.'_'.$name.'" id="'.$lang.'_'.$name.'" name="'.$lang.'_'.$name.'">
					</div>
					<div id="file_upload-progressbar" class="uk-progress uk-hidden">
						<div class="uk-progress-bar" style="width:0">0%</div>
					</div>
				</div>
				</div>
			</div>';

	}else{

		echo

		'<div class="uk-width-large-1-1 '.$pre_class.' '.($pre_class?'hide':'').' uk-grid-margin uk-row-first">

		<h3 class="heading_a">'.$fieldName.' <a class="md-btn md-btn-primary autoTooltip page_order_images" href="'.base_url().'content/gallerySorting/'.$lang.'_'.$name.'/'.$id.'" title="'.$admin_lang['label']['tooltip_ordering'].'">
		'.$admin_lang['label']['sorting_click'].'
		</a></h3>    
		<span style="color:red">'.$dimensions.'</span>		
		
		<div class="uk-form-row gallery-images-eng gallery_video_block">
		<table class="uk-table uk-table-align-vertical">
		
		<tbody id="" class="" data-field-name="'.$lang.'_'.$name.'">
		';
		
		

		$photos =  content_detail($lang.'_'.$name,$id);

		$photos_array = explode(',',$photos);

		if(!empty($photos_array)){

			foreach ($photos_array as $imageData) {

				if(!empty($imageData) && $imageData !='eng_event_slider_images' && $imageData !='eng_news_images' && $imageData !='eng_client_gallery' && $imageData !='eng_gallery_images' && $imageData !='eng_project_gallery' && $imageData !='eng_media_gallery_images' && $imageData !='eng_project_gallery_images' && $imageData !='eng_services_gallery'){

					$image = explode('||',$imageData)[0];

					$video = explode('||',$imageData)[1];
					
					echo
					'
					<tr id="'.$image.'">
						<td>
							<div class="row_data bgImg" style="display: inline-block; margin-right: 8px; vertical-align: top;">
								<div class="user_pic_panel" id="user_pic_panel">
									<div class="user_pic_wrap_">
										<img class="img_thumb" src="'.base_url().'assets/script/'.$image.'" alt="Slider Image">
									</div>
								</div>
								<div class="close_button">
									<img src="'.base_url().'assets/images/delete_forever48dp.png" width="25" height="20" style="margin-'.($admin_lang['admin_lang'] == 'eng'?'left':'right').': 107px; margin-top: -40px; cursor: pointer;"/>
								</div>
							</div>
						</td>
					</tr>
					';

				}

			}
			
		}

		

		echo
		'
		
		</tbody>
		</table>
		
		<div class="uk-grid">
			<div class="uk-width-1-1">
			<div id="file_upload-drop" class="uk-file-upload">
			<button id="'.$lang.'_'.$name.'_btn" class="uk-form-file md-btn md-btn-primary" type="button" id="user_file">'.$admin_lang['label']['choose_images'].'</button>
			<input type="hidden" value="'.$photos.'" id="'.$lang.'_'.$name.'" name="'.$lang.'_'.$name.'">
			</div>
			<div id="file_upload-progressbar" class="uk-progress uk-hidden">
				<div class="uk-progress-bar" style="width:0">0%</div>
			</div>
			</div>
			</div>
			
			</div></div>';

	}

	

	echo

	'<script>

	$("#'.$lang.'_'.$name.'_btn").mlibready({returnto:"#returned_'.$lang.'_'.$name.'", maxselect:99});

	

	var count_image_thumb = 0;

	$("#returned_'.$lang.'_'.$name.'").bind("DOMSubtreeModified",function(){

	 

	  var vaule = new Array();

	  var val = $("#'.$lang.'_'.$name.'").val();

	  var new_val = $("#returned_'.$lang.'_'.$name.'").html();

	  new_val = new_val.split(",");

	  for (i = 0; i < new_val.length; i++) { 

			text = new_val[i];

			new_text =  text.split("script/");

			vaule[i] =  new_text[1];

			if( $("#thumbdiv_'.$lang.'_'.$name.'").length != 0 ) {

				$("#thumbdiv_'.$lang.'_'.$name.'").append( "<div class=\"row_data bgImg\"><div class=\"user_pic_panel\" id=\"user_pic_panel\"><div class=\"user_pic_wrap_\"><img class=\"img_thumb\" src=\"'.base_url().'assets/script/"+vaule[i]+"\"></div></div><div class=\"close_button\"><img onclick=\"removeImage(\'thumbdiv_'.$lang.'_'.$name.'\')\" src=\"'.base_url().'assets/images/delete_forever48dp.png\" width=\"19\" height=\"17\"></div></div>" );
				
				

			}

		} 

		  new_val =  vaule.join(); 

		  

	  if(val != \'\'){

		  val = val + \',\'+  new_val ;  

		  }else{

		   val = new_val ;	  

			  }

	  

	   $("#'.$lang.'_'.$name.'").val(val);



	   count_image_thumb++;

	});

	

	$(".uk-form-row").on("click",".close_button img", function() {

				

		if(confirm("'.$admin_lang['label']['delete_image'].'")){ 

		$(this).parents(".row_data").remove();

		var imgsrcs = "";

		$(".gallery-images-eng img.img_thumb").each(function (index, element){

			var value = $(this).attr(\'src\');

			value = value.split("script/");

			new_value = value[1];

			if(imgsrcs!=="") imgsrcs += ",";

			imgsrcs += new_value;

		});

		$("#'.$lang.'_'.$name.'").val(imgsrcs);

		}

	});

	

	</script>';

}

function getAllCategories()
{
    $CI = &get_Instance();
    $CI->load->model('model_contents');
    $result = $CI->model_contents->getAllCategories();
    return $result;

}


?>