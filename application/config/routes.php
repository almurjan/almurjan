<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There enea two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['ems'] = "ems/admin_login/index";
$route['ems/admin_login/login'] = "ems/admin_login/login";
$route['ems/admin_login/forgot_password'] = "ems/admin_login/forgot_password";
$route['ems/admin-login/forgot_password'] = "ems/admin_login/forgot_password";
$route['ems/admin_login/logout'] = "ems/admin_login/logout";
$route['ems/admin_login'] = "ems/admin_login/test";
$route['default_controller'] = "page/index";
$route['(:num)'] = 'page/index';
$route['404_override'] = '';
$route['med'] = "page/med";
$route['page/med_submit'] = "page/med_submit";
$route['page/saveContactData'] = "page/saveContactData";
$route['page/loadMoreUsers'] = "page/loadMoreUsers";
$route['page/loadMoreReports'] = "page/loadMoreReports";
$route['page/newsletter'] = "page/newsletter";
$route['page/sendToFriend'] = "page/sendToFriend";
$route['page/submitCv'] = "page/submitCv";
$route['page/applyJob'] = "page/applyJob";
$route['page/job_application_form'] = "page/job_application_form";
$route['page/job_openings'] = "page/job_openings";
$route['page/submit_self_application'] = "page/submit_self_application";
$route['page/search'] = "page/search";
$route['page/getFrontendCities'] = "page/getFrontendCities";
$route['page/subscribe'] = "page/newsletter_old";
$route['page/check_captcha'] = "page/check_captcha";
$route['page/login'] = "page/login";
$route['logout'] = "logout/index";
$route['page/service/(:num)'] = "page/service/$1";
$route['page/reportings/(:num)'] = "page/reportings/$1";
$route['page/sub_service/(:num)'] = "page/sub_service/$1";
$route['page/sub_sub_service/(:num)'] = "page/sub_sub_service/$1";
$route['page/sub_sub_sub_service/(:num)'] = "page/sub_sub_sub_service/$1";
$route['ajax/save'] = "ajax/save";
$route['page/bod_details/(:num)'] = "page/bod_details/$1";
$route['page/event_details/(:num)'] = "page/event_details/$1";
$route['page/single_album/(:num)'] = "page/single_album/$1";
$route['page/detail_news/(:num)'] = "page/detail_news/$1";
$route['page/service_detail/(:num)'] = "page/service_detail/$1";
$route['page/media_center_event_details/(:num)'] = "page/media_center_event_details/$1";
$route['page/service_request/(:num)'] = "page/service_request/$1";
$route['page/detail/(:num)'] = "page/detail/$1";
$route['page/job_detail/(:num)'] = "page/job_detail/$1";
$route['page/awareness_detail/(:num)'] = "page/awareness_detail/$1";
$route['page/project/(:num)'] = "page/project/$1";
$route['page/event/(:any)'] = "page/event/$1";
$route['page/loadMoreNews'] = "page/loadMoreNews";
$route['page/getRegionCities'] = "page/getRegionCities";
$route['ems'] = "ems/index";
$route['page/submit_contact'] = "page/submit_contact";
$route['page/other_sector_detail/(:num)'] = "page/other_sector_detail/$1";
$route['page/almurjan_holding_detail/(:num)'] = "page/almurjan_holding_detail/$1";
$route['page/other_sector_project_detail/(:num)'] = "page/other_sector_project_detail/$1";
$route['page/newsLetter'] = "page/newsLetter";
$route['page/submit_ceneer'] = "page/submit_ceneer";
$route['page/detail/(:num)'] = "page/detail/$1";
$route['page/detail_service/(:num)'] = "page/detail_service/$1";
$route['page/saveForm'] = "page/saveForm";
$route['page/NewsRoomLogin'] = "page/NewsRoomLogin";
$route['page/NewsRoomLogout'] = "page/NewsRoomLogout";
$route['page/news_detail/(:num)'] = "page/news_detail/$1";
$route['page/news_room_detail/(:num)'] = "page/news_room_detail/$1";
$route['page/getCitiesDtc'] = "page/getCitiesDtc";
$route['page/vacancies_detail/(:num)'] = "page/vacancies_detail/$1";
$route['page/vacancy_application/(:num)'] = "page/vacancy_application/$1";

$route['page/project_details/(:num)'] = "page/project_details/$1";
$route['page/company_details/(:num)'] = "page/company_details/$1";
$route['page/(:any)'] = "page/index";


$route['page/contactUsRegistrationForm'] = "page/contactUsRegistrationForm";
$route['ar/page/contactUsRegistrationForm'] = "page/contactUsRegistrationForm";


$route['ar'] = 'page/index';
$route['ar/(:num)'] = 'page/index';
$route['ar/page/newsletter'] = "page/newsletter";
$route['ar/page/getFrontendCities'] = "page/getFrontendCities";
$route['ar/page/saveContactData'] = "page/saveContactData";
$route['ar/page/loadMoreUsers'] = "page/loadMoreUsers";
$route['ar/page/loadMoreReports'] = "page/loadMoreReports";
$route['ar/page/submitCv'] = "page/submitCv";
$route['ar/page/applyJob'] = "page/applyJob";
$route['page/job_application_form'] = "page/job_application_form";
$route['ar/page/job_openings'] = "page/job_openings";
$route['ar/page/search'] = "page/search";
$route['ar/page/subscribe'] = "page/newsletter_old";
$route['ar/page/other_sector_detail/(:num)'] = "page/other_sector_detail/$1";
$route['ar/page/almurjan_holding_detail/(:num)'] = "page/almurjan_holding_detail/$1";
$route['ar/page/other_sector_project_detail/(:num)'] = "page/other_sector_project_detail/$1";
$route['ar/ajax/save'] = "ajax/save";
$route['ar/page/service/(:num)'] = "page/service/$1";
$route['ar/page/reportings/(:num)'] = "page/reportings/$1";
$route['ar/page/sub_service/(:num)'] = "page/sub_service/$1";
$route['ar/page/sub_sub_service/(:num)'] = "page/sub_sub_service/$1";
$route['ar/page/sub_sub_sub_service/(:num)'] = "page/sub_sub_sub_service/$1";
$route['ar/page/book/(:num)'] = "page/book/$1";
$route['ar/logout'] = "logout/index";
$route['ar/page/bod_details/(:num)'] = "page/bod_details/$1";
$route['ar/page/event_details/(:num)'] = "page/event_details/$1";
$route['ar/page/single_album/(:num)'] = "page/single_album/$1";
$route['ar/page/detail_news/(:num)'] = "page/detail_news/$1";
$route['ar/page/service_detail/(:num)'] = "page/service_detail/$1";
$route['ar/page/media_center_event_details/(:num)'] = "page/media_center_event_details/$1";
$route['ar/page/service_request/(:num)'] = "page/service_request/$1";
$route['ar/page/detail/(:num)'] = "page/detail/$1";
$route['ar/page/job_detail/(:num)'] = "page/job_detail/$1";
$route['ar/page/awareness_detail/(:num)'] = "page/awareness_detail/$1";
$route['ar/page/project/(:num)'] = "page/project/$1";
$route['ar/page/event/(:any)'] = "page/event/$1";
$route['ar/page/loadMoreNews'] = "page/loadMoreNews";
$route['ar/page/getRegionCities'] = "page/getRegionCities";
$route['ar/page/preview/(:num)'] = "page/preview/$1";
$route['ar/ems'] = "ems/index";
$route['ar/page/saveForm'] = "page/saveForm";
$route['ar/page/NewsRoomLogin'] = "page/NewsRoomLogin";
$route['ar/page/NewsRoomLogout'] = "page/NewsRoomLogout";
$route['ar/page/submit_contact'] = "page/submit_contact";
$route['ar/page/newsLetter'] = "page/newsLetter";
$route['ar/page/detail/(:num)'] = "page/detail/$1";
$route['ar/page/detail_service/(:num)'] = "page/detail_service/$1";
$route['ar/page/news_detail/(:num)'] = "page/news_detail/$1";
$route['ar/med'] = "page/med";
$route['ar/page/med_submit'] = "page/med_submit";
$route['ar/page/vacancies_detail/(:num)'] = "page/vacancies_detail/$1";
$route['ar/page/vacancy_application/(:num)'] = "page/vacancy_application/$1";


$route['ar/page/news_room_detail/(:num)'] = "page/news_room_detail/$1";
$route['ar/page/project_details/(:num)'] = "page/project_details/$1";
$route['ar/page/company_details/(:num)'] = "page/company_details/$1";
$route['ar/page/(:any)'] = "page/index";

/* End of file routes.php */
/* Location: ./application/config/routes.php */