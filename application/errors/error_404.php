<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<style type="text/css">
.py-4{
	display:none;
}
img.atnlogo {
	max-width: 130px;
}
.errorpage h3 {
	font-size: 36px;
	text-transform: uppercase;
	color: #003B71;
	margin: 0;
}
.errorpage h4 {
	font-size: 30px;
	text-transform: uppercase;
	font-weight: bold;
	color: #676767;
}
.errorpage p {
	font-size: 18px;
}
.text-center img, .text-center h3, .text-center h4, .text-center p{
	margin-top: 25px;
}
.btn{
	padding: 11px 22px 14px 25px;
}
label.error {
	color: red;
	position:relative;
	top:-15px;
}
#success-msg{
	color: #1a4686;
    font-size: initial;
    /* text-align: center; */
    display: block;
    padding-top: 12px;
    font-size: 23px;
}
</style>
</head>
<body>
<?php 
	$lang = 'eng';
	$logo = '';
	$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
	//$home_id = getPageIdbyTemplate('home');
	//$logo = base_url() . 'assets/script/' . content_detail('eng_logo_image_mkey_hdn', $home_id);
	
?>
<section class="content errorpage">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center" style="text-align: center !important;">
				<img src="<?php echo $logo;?>" class="atnlogo">
				<h3><?php echo ($lang == 'eng' ? 'Sorry' : 'عذرا،'); ?></h3>
				<h4><?php echo ($lang == 'eng' ? 'Page not Found' : 'لم يتم العثور على الصفحة'); ?></h4>
				<p><?php echo ($lang == 'eng' ? 'The webpage you were trying to reach could not be found on the server,or that you typed in the URL incorrectly' : 'لم يتم العثور على صفحة الويب التي تحاول الوصول إليها على الخادم ، أو التي تم كتابتها في عنوان الURL  بشكل غير صحيح.'); ?></p>
				<a style="color: #fff; background-color: #007bff; border-color: #007bff; text-decoration: none;" href="<?php echo str_replace('index.php','',$actual_link); ?>" class="btn btn-primary"><?php echo ($lang == 'eng' ? 'Go To Home' : 'الرجوع إلى الصفحة الرئيسية'); ?></a>
			</div>
		</div>
	</div>
</section>
</body>
</html>