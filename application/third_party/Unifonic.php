<?php
require('Unifonic/Autoload.php');

use \Unifonic\API\Client;

class Unifonic
{
	private $sender;
	
	private $phone_no;
	
	private $sms_text;
	
	public function __construct(array $params = NULL)
	{
		$this->sender = (isset($params['sender']) ? $params['sender'] : 'eDesign');
		
		$this->phone_no = $params['phone_no'];
		
		$this->sms_text = $params['sms_text'];
	}
	
	public function sendSMS()
	{

		$client = new Client();	
		$returnStatus = false;
		try {
			$result = $client->Messages->Send($this->phone_no, $this->sms_text, $this->sender);

			if (is_object($result)) {

				if (isset($result->MessageID)) {
					$returnStatus = true;
				} else {
					//if there would be any error of sms it will fall in this false
					$returnStatus = false;
				}
			} else {
				$returnStatus = false;
			}
			
		} catch (Exception $e) {
			// do nothing
			return false;
		}

	}
}