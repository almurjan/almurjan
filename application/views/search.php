<style>
    .SearchBoxes{
        border-bottom: 1px solid #c4a34e;
        padding: 20px 0px;
    }
    .mb-0 a , .mb-0{
        color: #00447C;
        font-weight: 700;
    }
    .BlogContentArea{
        padding: 125px;
    }
    p.mt-3{
        color: #000 !important;
        font-weight: 500 !important;
    }
</style>
<div class="ContentArea BlogContentArea">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="BigHeader">
                <h2><?php echo($lang == 'eng' ? 'Search Results' : 'البحث المتقدم') ?></h2>

                <br/>
                <label for="keyword"><?php echo($lang == 'eng' ? 'Enter keywords you need to search for' : 'أدخل الكلمات التي تحتاج  البحث عنها') ?></label>
                <form method="get" class="" action="<?php echo lang_base_url() . 'page/search' ?>">
                    <div class="ContactForm">
                        <div class="form-group">
                            <input type="text" class="form-control rounded-0 item_title" name="item_title"
                                   value="<?php echo $item_title; ?>" class="form-control"
                                   placeholder="<?php echo($lang == 'eng' ? 'Search' : 'بحث') ?>" required
                                   autocomplete="off">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="terms">

                <?php
                $i = 1;
                $rowsCount = 0;

                if ($items) {

                    foreach ($items as $item) {

                        $rowsCount = 1;

                        $parent_page_details = get_content_data($item->parant_id);
                        // $url = '#';
                        /*if ($item->tpl == 'home' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'profile' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($parent_page_details->tpl == 'home' && $item->parant_id != 0){
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->parant_id, 'eng'));
                        }
                        else if (($item->tpl == 'history' && $item->parant_id == 0) OR ($item->tpl == 'mission_vision' && $item->parant_id == 0)) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'history' && $item->parant_id  != 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->parant_id, 'eng'));
                        }
                        else if ($item->tpl == 'ceo_message') {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'bod' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'bod' && $item->parant_id != 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->parant_id, 'eng')).'#bodPopUp_'.$item->id;
                        }
                        else if ($item->tpl == 'events' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'other_sectors' && $item->parant_id != 0) {
                            $url = lang_base_url() . 'page/other_sector_detail/' . $item->id;
                        }
                        else if ($item->tpl == 'events' && $item->parant_id != 0) {
                            $url = lang_base_url() . 'page/event_details/' . $item->id;
                        }else if ($item->tpl == 'self' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'csr' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'news' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'news' && $item->parant_id != 0) {
                            $url = lang_base_url() . 'page/news_detail/' . $item->id;
                        }
                        else if ($item->tpl == 'companies_map' && $item->parant_id == 0) {
                            $url = lang_base_url();
                        }else if ($item->tpl == 'companies_map' && $item->parant_id != 0) {
                            $url = $url = lang_base_url();
                        }
                        else if ($item->tpl == 'landmarks' && $item->parant_id != 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->parant_id, 'eng')).'#landmarkPopUp_'.$item->id;
                        }
                        else if ($item->tpl == 'contact_us' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'terms' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'sitemap' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        } else if ($item->tpl == 'privacy_policy' && $item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        }
                        else if ($item->tpl == 'insurance_category_new_table') {

                            $item->eng_title = $item->eng_name;
                            $item->arb_title = $item->arb_name;

                            $url = lang_base_url() . 'page/insurance_categories_detail/' .$item->insurance_category_id;
                        }
                        else if ($item->tpl == 'insurance_company_new_table') {

                            $item->eng_title = ($item->eng_name ? $item->eng_name : $item->section_name_eng);
                            $item->arb_title = ($item->arb_name ? $item->arb_name : $item->section_name_arb);

                            $url = lang_base_url() . 'page/insurance_company_detail/' .$item->comp_id;
                        }*/

                        $url = '#';
                        if ($item->parant_id == 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->id, 'eng'));
                        } elseif ($item->parant_id > 0) {
                            $url = lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($item->parant_id, 'eng'));
                        }
                        ?>

                        <div class="SearchBoxes">
                            <div class="row">
                                <div class="col-auto">
                                    <h6 class="mb-0"><?php echo str_pad(($lang == 'eng' ? $i : convertToArabic($i)), 2, '0', STR_PAD_LEFT); ?></h6>
                                </div>
                                <div class="col">
                                    <h6 class="mb-0"><a target="_blank"
                                                        href="<?php echo $url; ?>">
                                            <?php echo($lang == 'eng' ? str_replace(' |','',$item->eng_title) : str_replace(' |','',$item->arb_title)); ?></a>
                                    </h6>
                                    <p class="mt-3 mb-0">
                                        <?php
                                        //echo $item->id . ' - ' . fetchTemplate($item->parant_id) . '<br>';

                                        $replace = array('<p>', '</p>', '<h1>', '</h1>', '<h2>', '</h2>', '<blockquote>', '</blockquote>', '<div class="col-sm-6">', '</div>');
                                        $replace_by = array('', '', '', '', '', '', '', '', '', '');

                                        if ($item->tpl == 'home' && $item->parant_id == 0) {
                                            $desc = content_detail($lang . '_desc_menu_title', $item->id);
                                        }
                                        else if ($parent_page_details->tpl == 'home' && $item->parant_id != 0){
                                            $desc = content_detail($lang . '_home_sliderText', $item->id);
                                        }
                                        else if ($item->tpl == 'profile' && $item->parant_id == 0) {
                                            $desc = content_detail($lang . '_desc_pro', $item->id);
                                        }

                                        else if ($item->tpl == 'other_sectors' && $item->parant_id == 0) {
                                            $desc = content_detail($lang . '_home_content_description', $item->id);
                                        }
                                        else if ($item->tpl == 'other_sectors' && $item->parant_id  != 0) {
                                            $desc = content_detail($lang . '_other_sectors_dec', $item->id);
                                        }
                                        else if ($item->tpl == 'companies_map' && $item->parant_id == 0) {
                                            $desc = content_detail($lang . '_descs_miss', $item->id);
                                        }
                                        else if ($item->tpl == 'mission_vision' && $item->parant_id  != 0) {
                                            $desc = content_detail($lang . '_desc', $item->id);
                                        }
                                        else if ($item->tpl == 'ceo_message') {
                                            $desc = content_detail($lang . '_desc_our_mes', $item->id);
                                        }
                                        else if ($item->tpl == 'bod' && $item->parant_id  == 0) {
                                            $desc = content_detail($lang . '_dob_dec', $item->id);
                                        }
                                        else if ($item->tpl == 'bod' && $item->parant_id  != 0) {
                                            $desc = content_detail($lang . '_dob_dec', $item->id);
                                        }
                                        else if ($item->tpl == 'csr' && $item->parant_id  == 0) {
                                            $desc = content_detail($lang . '_csr_desc', $item->id);
                                        }
                                        else if ($item->tpl == 'landmarks' && $item->parant_id  != 0) {
                                            $desc = content_detail($lang . '_landmarks_dec', $item->id);
                                        }
                                        else if ($item->tpl == 'careers' && $item->parant_id  == 0) {
                                            $desc = content_detail($lang . '_vacancy_desc', $item->id);
                                        }
                                        else if ($item->tpl == 'events' && $item->parant_id  == 0) {
                                            $desc = content_detail($lang . '_desc', $item->id);
                                        }
                                        else if ($item->tpl == 'events' && $item->parant_id  != 0) {
                                            $desc = content_detail($lang . '_desc', $item->id);
                                        }else if ($item->tpl == 'news' && $item->parant_id != 0) {
                                            $desc = content_detail($lang . '_desc', $item->id);
                                        }else if ($item->tpl == 'news' && $item->parant_id == 0) {
                                            $desc = content_detail($lang . '_desc', $item->id);
                                        }
                                        else if ($item->tpl == 'terms' && $item->parant_id == 0) {
                                            $desc = content_detail($lang . '_desc_terms', $item->id);
                                        }
                                        if ($desc != '') {
                                            echo get_x_words(strip_tags(str_replace($replace, $replace_by, $desc), '50')) . ' ...';
                                        }
                                        //echo '<br/>' . $item->id;

                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <?php
                        $i++;
                    }
                }
                if($rowsCount == 0){
                    echo '<p class="noRecords">'.($lang == 'eng' ? 'No Records Found' : 'لم يتم العثور على نتائج').'</p>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
