<section class="other-sector-detail">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                <div class="sector-content">
                    <h2><?php echo pageTitle($page_id, $lang); ?></h2>
                    <?php echo content_detail($lang.'_other_sectors_dec',$page_id); ?>
                    <?php if (content_detail('eng_other_sectors_link', $page_id)) { ?>
                        <span class="sector-link"><a href="<?php echo content_detail('eng_other_sectors_link', $page_id); ?>" target="_blank">VISIT THE DIRECT WEBSITE</a></span>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="sector-logo-image">
                    <img class="img-fluid" src="<?php echo base_url() . 'assets/script/' . content_detail('eng_other_sectors_logo_mkey_hdn', $page_id); ?>" alt="">
                </div>
            </div>
        </div>
        <div class="col-12">
            <?php
            $projects = ListingContent($page_id);
            if ($projects) { ?>
                <div class="row space-between-boxes">
                    <?php foreach ($projects as $project) {
                        if (content_detail('eng_other_sectors_project_logo_mkey_hdn', $project->id)) { ?>
                            <div class="col-xxl-auto col-xl-auto col-lg-3 col-md-3 col-sm-3 col-6 pe-sm-0">
                                <a href="javascript:void(0)" data-bs-toggle="modal"
                                   data-bs-target="#staticBackdrop-project-<?php echo $project->id; ?>">
                                    <div class="sector-logo-box">
                                        <img class="img-fluid"
                                             src="<?php echo base_url('assets/script/' . content_detail('eng_other_sectors_project_logo_mkey_hdn', $project->id)); ?>"
                                             alt="">
                                    </div>
                                </a>
                            </div>

                            <div class="modal fade"
                                 id="staticBackdrop-project-<?php echo $project->id; ?>" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel-project-<?php echo $project->id; ?>" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-body position-relative">
                                            <button type="button"
                                                    class="btn-close close-modal"
                                                    data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            <div class="sector-modal-logo">
                                                <img class="img-fluid"
                                                     src="<?php echo base_url('assets/script/' . content_detail('eng_other_sectors_project_logo_mkey_hdn', $project->id)); ?>"
                                                     alt="">
                                            </div>

                                            <?php
                                            $slider_images = content_detail('eng_other_sectors_slider', $project->id);
                                            if ($slider_images) {
                                                ?>
                                                <div class="events-popup-slider owl-carousel owl-theme mt-4">
                                                    <?php
                                                    $slider_images_list = explode(',', $slider_images);
                                                    foreach ($slider_images_list as $slider_image) {
                                                        if ($slider_image == 'eng_other_sectors_slider') continue;
                                                        ?>
                                                        <div class="item">
                                                            <div class="px-5">
                                                                <img src="<?php echo base_url() . 'assets/script/' . $slider_image ?>"
                                                                     class="img-fluid" alt="">
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>

                                            <?php if (content_detail('eng_other_sectors_proj_video', $project->id) != '') {
                                                $video_id = explode('v=', content_detail('eng_other_sectors_proj_video', $project->id));
                                                ?>
                                                <div class="sector-modal-video position-relative">
                                                    <iframe width="766" height="315"
                                                            src=""
                                                            title="YouTube video player"
                                                            frameborder="0"
                                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                            allowfullscreen></iframe>
                                                </div>

                                                <script>
                                                    $("#staticBackdrop-project-<?php echo $project->id; ?>").on('hidden.bs.modal', function (e) {
                                                        $("#staticBackdrop-project-<?php echo $project->id; ?> iframe").attr("src", '');
                                                    });
                                                    $("#staticBackdrop-project-<?php echo $project->id; ?>").on('shown.bs.modal', function (e) {
                                                        $("#staticBackdrop-project-<?php echo $project->id; ?> iframe").attr("src", 'https://www.youtube.com/embed/<?php echo $video_id[1]; ?>');
                                                    });
                                                </script>
                                            <?php } ?>
                                            <div class="row pt-4 align-items-center">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="modal-sector-content">
                                                        <?php echo content_detail($lang . '_other_sectors_proj_dec', $project->id); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        ?>
                    <?php }
                    ?>
                </div>
            <?php } ?>
        </div>






    </div>
</section>