<?php
$home = getPageIdbyTemplate('home');
$profile = getPageIdbyTemplate('profile');
$about_us = getPageIdbyTemplate('about_us');
$ceo_message = getPageIdbyTemplate('ceo_message');
$murjan_group = getPageIdbyTemplate('murjan_group');
$landmarks = getPageIdbyTemplate('landmarks');
$bod = getPageIdbyTemplate('bod');
$bod = 3948;
$bod_listings = ListingContent($bod);
$profile_listings = ListingContent($profile,'profile','0');
$profile_companies_listings = ListingContent($profile,'profile',1);
/*$value_image = imageSetDimenssion(content_detail('eng_value_image_mkey_hdn', $profile), 540, 417, 1);*/
$value_image = base_url('assets/script/').'/'.content_detail('eng_profile_image_mkey_hdn', $profile);
$Pagetitle = content_detail($lang.'_page_title_profile', $profile);
?>
<style>
    .profileContainerBg {
        background: url(<?php echo $value_image;?>) no-repeat;
        background-size: cover;
    }
    footer{
        margin-top: 0px !important;
    }
</style>
<div class="inner-banner-main-box">
    <section class="about-banner" style="background-image: url('<?php echo base_url() . 'assets/script/' . content_detail('eng_about_us_banner_image_mkey_hdn', $page_id); ?>')">
        <div class="inner-banner-inner-box">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="white-banner-content">
                            <?php
                            $exploded_page_sub_title = explode(' ', pageSubTitle($page_id, $lang));
                            $page_sub_title = implode('<br>', $exploded_page_sub_title);
                            ?>
                            <h1><?php echo $page_sub_title; ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<section class="short-intro about-short-message">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="short-intro-content">
                    <?php echo content_detail($lang.'_desc_pro',$profile);?>
                </div>
                <div class="short-intro-readmore">
                    <a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(getPageIdbyTemplate('history'), 'eng')); ?>" class="btn btn-outline-light intro-readmore"><?php echo translate($lang, 'Our History', 'اكتشف تاريخنا'); ?></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="aboutpage-short-intro">
                    <div class="aboutpage-icon-box">
                        <div class="box-icon">
                            <img class="img-fluid" src="<?php echo base_url('assets/frontend/images/strategy.svg'); ?>" alt="">
                        </div>
                        <h2><?php echo content_detail($lang.'_strategy_title',$profile); ?></h2>
                        <p><?php echo content_detail($lang.'_strategy_value',$profile); ?></p>
                    </div>
                </div>

            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="aboutpage-short-intro">
                    <div class="aboutpage-icon-box">
                        <div class="box-icon">
                            <img class="img-fluid" src="<?php echo base_url('assets/frontend/images/vision.svg'); ?>" alt="">
                        </div>
                        <h2><?php echo content_detail($lang.'_vission_title',$profile); ?></h2>
                        <p><?php echo content_detail($lang.'_vission_value',$profile); ?></p>
                    </div>
                </div>

            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="aboutpage-short-intro">
                    <div class="aboutpage-icon-box">
                        <div class="box-icon">
                            <img class="img-fluid" src="<?php echo base_url('assets/frontend/images/mission.svg'); ?>" alt="">
                        </div>
                        <h2><?php echo content_detail($lang.'_mission_title',$profile); ?></h2>
                        <p><?php echo content_detail($lang.'_mission_desc_value',$profile); ?></p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="chairman-message">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="message-heading has-second-color">
                    <!--<h2><?php /*echo translate($lang, "Owner's <span>Message</span>", "رسالة <span>المُلاك</span>") */?></h2>-->
                    <h2><?php echo pageSubTitle($ceo_message, $lang); ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="chairman-picture">
                    <img class="img-fluid w-100" src="<?php echo imageSetDimenssion(content_detail('eng_ceo_image_mkey_hdn', $ceo_message), 350, 546, 1); ?>" alt="">
                </div>
                <div class="chairman-title-box mb-5">
                    <h4><?php echo strtoupper(content_detail($lang.'_ceo_name',$ceo_message));?></h4>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 order-3 order-lg-2">
                <div class="chairman-content" style="max-width: 100%;">
                    <?php echo content_detail($lang.'_desc_our_mes',$ceo_message);?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-12 order-2 order-lg-3">
                <div class="chairman-picture">
                    <img class="img-fluid w-100" src="<?php echo imageSetDimenssion(content_detail('eng_ceo_2_image_mkey_hdn', $ceo_message), 350, 546, 1); ?>" alt="">
                </div>
                <div class="chairman-title-box mb-5">
                    <h4><?php echo strtoupper(content_detail($lang.'_ceo_2_name',$ceo_message));?></h4>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="new-board-of-directors">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="short-intro-heading has-second-color">
                    <h2><?php echo translate($lang, 'BOARD <span>OF DIRECTORS</span>', 'أعضاء <span>مجلس الإدارة</span>'); ?></h2>
                </div>
            </div>
        </div>
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <?php
                foreach ($bod_listings as $bod_listing) { ?>
                    <div class="col mb-4">
                        <a href="javascript:void(0);" class="board-of-directors-box text-center" data-bs-toggle="modal" data-bs-target="#board-of-director-<?php echo $bod_listing->id; ?>">
                            <div class="board-of-directors-image">
                                <img src="<?php echo base_url() . 'assets/script/' . content_detail('eng_dob_image_mkey_hdn', $bod_listing->id); ?>"
                                     alt="" class="w-100">
                            </div>
                            <div class="board-of-directors-title">
                                <h2><?php echo pageTitle($bod_listing->id, $lang) ?></h2>
                            </div>
                            <span class="board-of-directors-position">
                                <h3><?php echo content_detail($lang . '_designation', $bod_listing->id) ?></h3>
                            </span>
                        </a>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade board-of-directors-modal" id="board-of-director-<?php echo $bod_listing->id; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-close-btn text-end">
                                    <img src="<?php echo base_url('assets/frontend/images/x-circle.svg'); ?>" alt="" data-bs-dismiss="modal" aria-label="Close" class="cm-fat-cursor">
                                </div>
                                <div class="">
                                    <div class="board-of-directors-modal-heading">
                                        <h2><?php echo pageTitle($bod_listing->id, $lang) ?></h2>
                                        <h3><?php echo content_detail($lang . '_designation', $bod_listing->id) ?></h3>
                                    </div>
                                    <div class="board-of-directors-modal-body">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="board-of-directors-image mb-4">
                                                    <img src="<?php echo base_url() . 'assets/script/' . content_detail('eng_dob_image_mkey_hdn', $bod_listing->id); ?>" alt="" class="w-100">
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="bord-of-director-modal-content">
                                                    <?php echo content_detail($lang . '_dob_dec', $bod_listing->id) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
            ?>
        </div>
    </div>
</section>
<section class="our-values">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="values-heading has-second-color">
                    <h2><?php echo content_detail($lang.'_value_title',$profile);?></h2>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <?php

            foreach($profile_listings as $profile_listing){
                if(content_detail('eng_value_image_mkey_hdn', $profile_listing->id)){
                    //$value_thumb = imageSetDimenssion(content_detail('eng_value_image_mkey_hdn', $profile_listing->id), 40, 40, 1);
                    $value_icon = base_url('assets/script/').'/'.content_detail('eng_value_image_mkey_hdn', $profile_listing->id);
                    $col_class_value = 'col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10';
                }else{
                    //$no_img=imageSetDimenssion('no_image.png', 40, 40, 1);
                    //$col_class_value = 'col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12';
                    $col_class_value = 'col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10';
                }
                ?>
                <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="value-content">
                        <div class="value-icon">
                            <img class="img-fluid" src="<?php echo $value_icon; ?>" alt="">
                        </div>
                        <h2><?php echo pageSubTitle($profile_listing->id,$lang);?></h2>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div>
</section>