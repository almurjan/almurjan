<?php

$home_id = getPageIdbyTemplate('home');

$home_listings = ListingContent($home_id);

$about_us_id = getPageIdbyTemplate('about_us');
$profile_id = getPageIdbyTemplate('profile');

$murjan_holding = getPageIdbyTemplate('murjan_holding');

$other_sectors = getPageIdbyTemplate('other_sectors');

$other_sectors_home_listings = ListingContent($other_sectors);

foreach($other_sectors_home_listings as $other_sectors_home_listing){

    $other_sectors_home_page_child_listings[] = ListingContent($other_sectors_home_listing->id,'homepage_sector_sorting');
}

$other_sectors_listings = ListingContent($other_sectors);

foreach($other_sectors_listings as $other_sectors_listing){

    $other_sectors_child_listings[] = ListingContent($other_sectors_listing->id);

}

$murjan_holding_listings = ListingContent($murjan_holding);

$logo_slider_listings = array_merge($murjan_holding_listings,$other_sectors_child_listings);

$slider_counter=0;

foreach($murjan_holding_listings as $logo_slider_listing){



    if(content_detail('is_show_home', $logo_slider_listing->id) == 1){

        $slider_counter++;

    }

}

$holding_logo_slider=$slider_counter;

if($holding_logo_slider >= 5){

    $margin_left = '-200px';

    $display='block';

    $width = '';

}

else{

    $margin_left = '0px';

    $display='none';

    $width = '280px';

}

$i = 0;

?>
<style>
    /*
        .owl-carousel

        {

            -ms-touch-action: pan-y;

            touch-action: pan-y;

        }
     */


    <?php

    if($lang == 'eng'){?>

    .owl-stage {

        left: <?php echo $margin_left;?>;

    }

    <?php }else{ ?>

    .owl-stage {

        right: <?php echo $margin_left;?>;

    }

    <?php } ?>

    .owlMyNav{

        display: <?php echo $display; ?>;

    }

    .hold_logo_slider .owl-item{

        width: <?php echo $width;?> !important;
        */
</style>

<div class="bg-video-wrap">
    <video src="<?php echo base_url('assets/frontend/almurjan video-6th Draft.mp4'); ?>" loop muted autoplay></video>
    <div class="overlay"></div>
    <div class="video-content">
        <div class="container">
            <div class="home-banner-content">
                <h1><?php echo pageSubTitle($page_id, $lang);?></h1>
            </div>
        </div>
    </div>
</div>

<!--<section class="homepage-banner" style="background: url('<?php /*echo base_url() . 'assets/script/' . content_detail('eng_banner_image_mkey_hdn', $page_id); */?>') no-repeat top right, url('<?php /*echo base_url('assets/frontend/images/home-banner-bg.jpg'); */?>') repeat-x">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="home-banner-content has-second-color">
                    <h1><?php /*echo pageSubTitle($page_id, $lang);*/?></h1>
                </div>
            </div>
        </div>
    </div>
</section>-->
<section class="short-intro">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="short-intro-heading has-second-color">
                    <h2><?php echo content_detail($lang.'_about_heading',$home_id); ?></h2>
                </div>
                <div class="short-intro-content">
                    <?php echo content_detail($lang.'_about_desc',$home_id); ?>
                </div>
                <div class="short-intro-readmore">
                    <a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($about_us_id, 'eng')); ?>" class="btn btn-outline-light intro-readmore"><?php echo translate($lang, 'Read More', 'اقرأ أكثر') ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="explore-sectors">
    <div class="container">
        <div class="row">
            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="sector-short-info">
                    <?php echo content_detail($lang.'_other_sector_home_desc',$other_sectors); ?>
                    <a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($other_sectors, 'eng')); ?>" class="btn btn-outline-light sector-short-info-readmore"><?php echo ($lang == 'eng' ? 'Explore Sectors  ' : 'استكشف القطاعات '); ?></a>
                </div>
            </div>
            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="our-sectors">
                    <div class="row">
                        <?php
                        foreach($other_sectors_listings as $other_sectors_listing){
                            $title = pageSubTitle($other_sectors_listing->id,$lang);
                            //$cat_icon = imageSetDimenssion(content_detail('eng_other_sectors_cat_icon_mkey_hdn', $other_sectors_listing->id), 112, 97, 1);
                            $cat_icon = base_url().'assets/script/'.content_detail('eng_other_sectors_cat_icon_mkey_hdn', $other_sectors_listing->id);
                            if(content_detail('eng_other_sectors_cat_icon_mkey_hdn', $other_sectors_listing->id)){
                                $cat_icon = $cat_icon;


                            }else{
                                $no_img=imageSetDimenssion('logo_noimage.png', 112, 97, 1);
                                $cat_icon=$no_img;

                            }
                            $cat_hover_icon = base_url().'assets/script/'.content_detail('eng_other_sectors_cat_hover_icon_mkey_hdn', $other_sectors_listing->id);
                            if(content_detail('eng_other_sectors_cat_hover_icon_mkey_hdn', $other_sectors_listing->id))
                            {
                                $cat_hover_icon = $cat_hover_icon;
                            }
                            else
                            {
                                $cat_hover_icon = $cat_icon;
                            }
                            ?>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-3 col-3">
                                <a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($other_sectors, 'eng')).'?s='.$other_sectors_listing->id; ?>">
                                    <div class="sector-icon">
                                        <img class="img-fluid" src="<?php echo $cat_hover_icon;?>" alt="">
                                        <h2><?php echo $title; ?></h2>
                                    </div>
                                </a>
                            </div>

                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pride-numbers" style="background-image: url('<?php echo base_url('assets/frontend/images/pride-numbers-bg.jpg'); ?>')">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="pride-number-heading has-second-color">
                    <h2><?php echo content_detail($lang.'_achievements_heading',$home_id); ?></h2>
                </div>
            </div>
        </div>
        <div class="row numbers-spacing">
            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                <div class="p-numbers">
                    <h2><span class="counter"><?php echo content_detail('eng_achievements_sectors',$home_id); ?></span>+</h2>
                    <h3><?php echo ($lang == 'eng' ? 'Sectors' : 'قطاعات');?></h3>
                </div>
            </div>
            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                <div class="p-numbers">
                    <h2><span class="counter"><?php echo content_detail('eng_achievements_experience',$home_id); ?></span>+</h2>
                    <h3><?php echo ($lang == 'eng' ? 'Years of experience' : 'عامًا من الخبرة');?></h3>
                </div>
            </div>
            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                <div class="p-numbers">
                    <h2><span class="counter"><?php echo content_detail('eng_achievements_companies',$home_id); ?></span>+</h2>
                    <h3><?php echo ($lang == 'eng' ? 'Companies' : 'شركة');?></h3>
                </div>
            </div>
            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                <div class="p-numbers">
                    <h2><span class="counter"><?php echo content_detail('eng_achievements_employee',$home_id); ?></span>+</h2>
                    <h3><?php echo ($lang == 'eng' ? 'Employees' : 'موظف');?></h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="our-companies">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="short-intro-heading has-second-color">
                    <h2><?php echo content_detail($lang . '_companies_heading',$home_id); ?></h2>
                </div>
                <div class="companies-slider">
                    <div class="owl-carousel-companies owl-carousel owl-theme">

                        <?php
                        $sectors_logo_sorting = [];
                        $home_page_sorted_companies = [];
                        foreach ($other_sectors_home_page_child_listings as  $other_sectors_home_page_child_listing)
                        {

                            foreach ($other_sectors_home_page_child_listing as $sector_child_listing)
                            {

                                if(content_detail('is_show_home', $sector_child_listing->id) == 1)
                                {
                                    $home_page_sorted_companies[] = $sector_child_listing;
                                    $sectors_logo_sorting[] = $sector_child_listing->sectors_logo_sorting;
                                }
                            }
                        }
                        /*sorting*/
                        array_multisort($sectors_logo_sorting, SORT_ASC, $home_page_sorted_companies);
                        foreach ($home_page_sorted_companies as $sector_child_listing)
                        {

                            $sector_img = base_url().'assets/script/'.content_detail('eng_other_sectors_logo_for_home_mkey_hdn', $sector_child_listing->id);
                            if(content_detail('eng_other_sectors_logo_for_home_mkey_hdn', $sector_child_listing->id))
                            {
                                ?>
                                <a href="<?php echo getPageUrl($other_sectors) . '?s=' . $sector_child_listing->parant_id . '&q=' . $sector_child_listing->id; ?>">
                                    <div class="item">
                                        <img class="img-fluid" src="<?php echo $sector_img; ?>" alt="">
                                    </div>
                                </a>

                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>