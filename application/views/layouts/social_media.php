<?php
$social_link = social_links();
if($social_link->soc_fb != '' && $social_link->soc_you != '' && $social_link->soc_snapchat != '' && $social_link->soc_tw != '' && $social_link->soc_insta != ''){
?>
<strong class="title"><?php echo ($lang=='eng' ? 'Please Follow us on' : 'يرجى متابعتنا على'); ?></strong>
<?php } ?>
<ul class="social-networks">
	<?php if($social_link->soc_fb != ''){?>
    <li>
        <a href="<?php echo ($social_link->soc_fb != '' ? $social_link->soc_fb : "javascript: void(0);"); ?>" target="_blank" class="facebook"></a>
    </li>
	<?php } ?>
	<?php if($social_link->soc_you != ''){?>
    <li>
        <a href="<?php echo ($social_link->soc_you != '' ? $social_link->soc_you : "javascript: void(0);"); ?>" target="_blank" class="youtube"></a>
    </li>
	<?php } ?>
    <?php if($social_link->soc_snapchat != ''){?>
        <li>
            <a href="<?php echo ($social_link->soc_snapchat != '' ? $social_link->soc_snapchat : "javascript: void(0);"); ?>" target="_blank" class="tumbler"></a>
        </li>
    <?php } ?>
	<?php if($social_link->soc_tw != ''){?>
    <li>
        <a href="<?php echo ($social_link->soc_tw != '' ? $social_link->soc_tw : "javascript: void(0);"); ?>" target="_blank" class="twitter"></a>
    </li>
	<?php } ?>
	<?php if($social_link->soc_insta != ''){?>
    <li>
        <a href="<?php echo ($social_link->soc_insta != '' ? $social_link->soc_insta : "javascript: void(0);"); ?>" target="_blank" class="instagram"></a>
    </li>
	<?php } ?>

</ul>