<main id="main" role="main" class="contact">
    <div class="banner-holder">
        <div class="container">
            <span class="page-title"><?php echo pageTitle($page_id,$lang); ?></span>
        </div>
    </div>
    <div class="container">
        <div class="content">
            <form class="form-contact" action="<?php echo base_url().'page/saveContactData'?>" method="post" name="contactForm" id="contactUs_id" onsubmit="return false;">
                <?php echo content_detail($lang.'_page_desc', $page_id);?>
                <div class="holder">
                    <input type="text" id="name_id" name="name" placeholder="Name *" class="input-normal">
                    <input type="text" id="email_id" name="email" placeholder="Email *" class="input-normal">
                    <input type="text" id="telephone_id" name="telephone" placeholder="Telephone *" class="input-normal">
                    <input type="text" id="activity_id" name="activity" placeholder="ct Business Activity *" class="input-normal">
                    <?php echo $get_row = getCategories();
                    if($lang == 'eng'){
                        $sources = explode(',', $get_row->eng_source);
                    }else{
                        $sources = explode(',', $get_row->arb_source);
                    }
                    ?>
                    <select name="source" id="source_id">

                        <option selected disabled>Select Source *</option>
                        <?php foreach ($sources as $source){
                            if($source != ''){?>
                                <option value="<?php echo $source?>"><?php echo $source?></option>
                            <?php } } ?>
                    </select>
                    <input type="text" name="message" id="message_id" placeholder="Your Message *" class="input-normal">
                    <div class="captcha-holder text-left">
                        <input type="hidden" id="captcha_id">
                        <div class="g-recaptcha" data-sitekey="6LfElCIUAAAAAPb0jfiWxhFfs2DD-sYEX7U_qABK"></div>
                        <!--<img src="<?php //echo base_url().'assets/frontend/motoon/'?>images/img-72.jpg" alt="captcha">-->				
                    </div>
                    <div class="text-right">
                        <input type="submit" class="btn-normal white" name="submit" id="submit_id" value="<?php echo ($lang == 'eng'?"Send Form":"Send Form"); ?>">
                    </div>
                </div>
            </form>
            <div class="loading" style="display: none">
                <img src="<?php echo base_url().'assets/images/loading_icon.png'?>" alt="">
            </div>
            <div id="success-message" class="submit_message"></div>
        </div>
    </div>
	<?php 
	$locations = ListingContent($page_id);	
	/*echo '<pre>';
	print_r($locations);*/
	foreach($locations as $value){	
	$marker_array[] = array($value->eng_location);			
	}		
	$marker_j = json_encode($marker_array);
	$logo_j = json_encode($logo_array);	
	//echo $marker_j;
	?>
    <div class="map-contact" id="map_canvas">
    </div><?php if($locations != ''){	?>		
	<script type="text/javascript">
	var markericons = [];
function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
	
    // Multiple Markers
    var markers = [
		<?php $i=0; 
		foreach($marker_array as $marker_loc) { ?>
         ['', <?php echo $marker_loc[0]; ?>,'<?php echo $logo_array[$i][0] ;?>'],
		<?php $i++; } ?>
    ];
                        
    // Info Window Content
    var infoWindowContent = [
	<?php 
	$i=0;
	foreach($locations as $value) { ?>
        ['<div class="info_content" style="color:black;">' +
        '<div class="CAN_logo"><img height="40" width="40" src="<?php echo base_url().'assets/script/'.content_detail('eng_construction_icon_mkey_hdn',$value->id);?>"><h3><?php echo pageTitle($value->id,$lang);?></h3></div>'+
		'<div class="map_desc"><?php echo content_detail($lang.'_construction_detail',$value->id);?></div>' +
        '</div>'],
	<?php $i++; } ?>
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    // Loop through our array of markers & place each one on the map 
	
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
			icon: url+'assets/frontend/motoon/images/blackMapLocation.png',	
            title: markers[i][0]
        });
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function(){
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
				for (var j = 0; j < markericons.length; j++) {
				  markericons[j].setIcon(url+'assets/frontend/motoon/images/blackMapLocation.png');
				}
				//Change the marker icon
				marker.setIcon(url+'assets/frontend/motoon/images/purpleMapLocation.png');
            }
			
        })(marker, i));
		markericons.push(marker);
        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);	
    }
	 
    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(8);
        google.maps.event.removeListener(boundsListener);
    });
}
	</script>
	<?php } ?>
</main>