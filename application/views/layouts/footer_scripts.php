<script>

    var base_url = '<?php echo base_url(); ?>';
    var lang_base_url = '<?php echo lang_base_url(); ?>';
    var tpl = '<?php echo $tpl_name?>';

    var lang = '<?php echo $lang; ?>'; var lang = '<?php echo $lang; ?>';

    var murjan_holding_total = '<?php echo $holding_logo_slider; ?>';

    var holding_slider_loop;

    if(murjan_holding_total >= 5){

        murjan_holding_slider = 4;

        holding_slider_loop=true;

    }
    else{

        murjan_holding_slider = murjan_holding_total;

        holding_slider_loop=false;

    }
    console.log(murjan_holding_total);
    console.log(holding_slider_loop);

</script>


<script>
    // Click
    $(document).on( "click", ".btn-default", function() {
        setTimeout(function () {
            window.location.reload();
        }, 1000);
    });

    $(".subscribe_newsletter").click(function (e) {
        //alert('here');
        var base_url  = '<?php echo base_url().'page/newsLetter';?>';
        var email = $('#news_letter').val();
        if(email == ''){
            $('#news_letter_msg').html('Please fill the field');
            return false;
        }
        else{
            $('#news_letter_msg').html('');
        }
        var lang = $('#lang').val();

        if(email == ''){
            $("#email").addClass('errorClass');
        }

        else if(validateEmail(email) == true){
            $(".page_loader_newsletter").fadeIn("slow");
            $(".subscribe_newsletter").attr("disabled", true);
            $.ajax({
                type: "POST"
                , url: base_url
                , data: {
                    email: email,
                    lang: lang
                }
                , success: function (response) {
                    $(".subscribe_newsletter").attr("disabled", false);
                    $(".page_loader_newsletter").fadeOut("slow");
                    var json = jQuery.parseJSON(response);
                    $("#newsletter_message").html(json.message);
                    //$(".open_mymodel_quote").click();
                    setTimeout(function() {
                        $("#newsletter_message").html('');
                        //location.reload();
                    }, 3000);

                }
            });
            //$(".page_loader").fadeOut("slow");
            e.preventDefault();
        }
        else{
            $('#news_letter_msg').html('Please fill the field');
            return false;
        }
    });
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
    function in_array(array, val){
        for(var i = 0; i < array.length; i++){
            if(val == array[i]){
                return true;
            }
        }
        return false;
    }
</script>
<script>

    function openNav() {

        document.getElementById("mySidenav").style.width = "330px";

        document.getElementById("main").style.marginLeft = "330px";

        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";

    }



    function closeNav() {

        document.getElementById("mySidenav").style.width = "0";

        document.getElementById("main").style.marginLeft= "0";

        document.body.style.backgroundColor = "white";

    }

</script>

<script>

    $('#langSwitch').click(function () {

        $('.page_loader').show();

        var language = lang === 'eng' ? 'arb' : 'eng';

        redirect(language);

    });



    function getAbsolutePath() {

        var loc = window.location;

        var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);

        return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));

    }

    function getUrlVars()

    {

        var vars = [], hash;

        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

        for(var i = 0; i < hashes.length; i++)

        {

            hash = hashes[i].split('?');

            vars.push(hash[0]);

            vars[hash[0]] = hash[1];

        }

        return vars;

    }

    function redirect(lang) {

        var concat = getUrlVars();

        if (/=/i.test(concat)){

            var mark = '?';

            //alert('yes');

        }

        else{

            //alert('no');

            concat = '';

            mark = '';

        }

        $('.page_loader').show();

        <?php $url = ''; ?>

        if (lang == 'arb') {

            <?php

            $uri_string = explode('/', uri_string());

            for ($i = 1; $i < sizeof($uri_string); $i++) {

                if ($uri_string[$i] != '') {

                    $url .= $uri_string[$i] . '/';

                }

            }

            ?>

            var redirection = '<?php  echo base_url() . 'ar/' . uri_string(); ?>';

            window.location.href = redirection+mark+concat;

        } else {

            var redirection = '<?php  echo base_url() . $url; ?>';

            window.location.href = redirection+mark+concat;

        }

    }



    $(document).ready(function () {
        /******************************
         BOTTOM SCROLL TOP BUTTON
         ******************************/

            // declare variable
        var scrollTop = $(".scrollTop");

        $(window).scroll(function() {
            // declare variable
            var topPos = $(this).scrollTop();

            // if user scrolls down - show scroll to top button
            if (topPos > 100) {
                $(scrollTop).css("opacity", "1");

            } else {
                $(scrollTop).css("opacity", "0");
            }

        }); // scroll END
        //Click event to scroll to top
        $(scrollTop).click(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 1500);
            return false;

        }); // click() scroll top EMD


        // Animate loader off screen

        $(".page_loader").fadeOut("slow");
        function other_sector_tab_active(){
            //window.stop();
            var val = '';
            if(lang=='eng'){
                val = '<?php echo $this->uri->segment(3) ?>';
            }else{
                val = '<?php echo $this->uri->segment(4) ?>';
            }
            $(".filter-button").not(this).removeClass('active_cat');
            //alert(val);
            $("#"+val).addClass("active_cat");
            if ($( ".filter" ).hasClass( val )) {
                $(".filter").hide();
                $("."+val).show();
            }
        }
        var val_id='';
        if(lang=='eng'){
            val_id = '<?php echo $this->uri->segment(3) ?>';
        }else{
            val_id = '<?php echo $this->uri->segment(4) ?>';
        }

        if(val_id){
            other_sector_tab_active();
        }

    });



    var myVideo = document.getElementById("myVideo");



    function playPause() {

        if (myVideo.paused)

            myVideo.play();

        else

            myVideo.pause();

    }

</script>
<script>

    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
    $(document).ready(function() {
        var url = '<?php echo current_url(); ?>';
        <?php
        if(strpos(current_url(),'Our_Sectors/')==false){?>
        $('.isFlag').each(function(){
            if($(this).data('id')>0){
                $(this).hide();
            }
        });
        <?php }?>

        setTimeout(function(){ topFunction(); }, 1000);
        var lang = '<?php echo $lang; ?>';
        var ln='';
        var dr='';
        var msg='';
        if(lang == 'arb'){
            ln='ar';
            dr='rtl';

        }else{
            ln='en';
            dr='ltr';
        }

        $('.js-rs-single').select2({
            language: {
                noResults: function () {
                    if(ln == 'ar'){
                        msg = 'لم يتم العثور على النتيجة';
                    }else{
                        msg = 'No Result Found';
                    }
                    return msg;
                }
            },
            dir: dr
        });
        $('.js-crs-single').select2({
            language: {
                noResults: function () {
                    if(ln == 'ar'){
                        msg = 'لم يتم العثور على النتيجة';
                    }else{
                        msg = 'No Result Found';
                    }
                    return msg;
                }
            },
            dir: dr
        });
        $('.js-ns-single').select2({
            language: {
                noResults: function () {
                    if(ln == 'ar'){
                        msg = 'لم يتم العثور على النتيجة';
                    }else{
                        msg = 'No Result Found';
                    }
                    return msg;
                }
            },
            dir: dr
        });
        var atLeastOneIsChecked = $('.education_level_other:checked').length > 0;
        $(document).on( "change", ".education_level_other", function() {
            if ($('.education_level_other').attr('value')=='Other') {

                $('#show_edu').show();
                $('#show-me').attr('required',true);


            } else {
                $('#show-me').attr('required',false);
                $('#show_edu').hide();

            }
        } );
        /*$('#med_form').change(function() {
            if ($('.education_level_other').attr('checked')) {
                alert('here');
                $('#show-me').attr('required',true);

            } else {
                $('#show-me').attr('required',false);

            }
        });*/
        $('.skip_to_section').click(function(){
            $('.home_landing_page').css("display","block");
            $('.home_landing_page_body').css("overflow","auto");
            $('html, body').animate({
                scrollTop: $("#About-Al-Murjan").offset().top
            }, 100);
        });
    });
</script>

<script type="text/javascript">

    /*jQuery(document).ready(function () {
        var type = window.location.hash.substr(1);
        console.log(type);
        $('#'+type).modal('show');
        //assets page
    });*/

    $(document).ready(function () {
        var size_li = $(".filtersdatanews").length;
        x=6;
        $('.filtersdatanews:lt('+x+')').show();
        $('#loadMore').click(function () {

            $('.page_loader').show();


            var next = $('.filtersdatanews:not([style*="display: block"])').length;
            if(next <=6){
                $('#loadMore').hide('slow');
            }
            x= (x+6 <= size_li) ? x+6 : size_li;
            $('.filtersdatanews:lt('+x+')').show();

            setTimeout(
                function()
                {
                    $('.page_loader').hide();
                }, 1500);

        });
    });

    $( document ).ready(function () {
        $(".filtersdata").slice(0, 12).show();
        $('.filtersdata:gt(11)').hide();
        if ($(".filtersdata:hidden").length > 12) {
            $("#load_more").show();
        }else{
            $("#load_more").hide();
        }
        $("#load_more").on('click', function (e) {
            e.preventDefault();
            $('.page_loader').show();
            $('html').css('overflow-y','hidden');
            if($(".filter-button").hasClass('active_cat')){
                var cat_id = $(".filter-button.active_cat ").data('filter');
                if(cat_id==='all'){
                    $(".filtersdata:hidden").slice(0, 12).slideDown();
                    if ($(".filtersdata:hidden").length === 0) {
                        $("#load_more").fadeOut('slow');
                    }
                    $('html,body').animate({
                        scrollTop: $(this).offset().top
                    }, 1500);
                    setTimeout(
                        function()
                        {
                            $('html').css('overflow-y','scroll');
                            $('.page_loader').hide();
                        }, 1500);
                }else{
                    $("."+cat_id+":hidden").slice(0, 12).slideDown();
                    if ($("."+cat_id+":hidden").length === 0) {
                        $("#load_more").fadeOut('slow');
                    }
                    $('html,body').animate({
                        scrollTop: $(this).offset().top
                    }, 1500);
                    setTimeout(
                        function()
                        {
                            $('html').css('overflow-y','scroll');
                            $('.page_loader').hide();
                        }, 1500);
                }
            }
        });

        $(".filtersdatanews").slice(0, 6).show();
        $('.filtersdatanews:gt(6)').hide();
        if ($(".filtersdatanews:hidden").length > 0) {
            $("#load_more_news").show();
        }else{
            $("#load_more_news").hide();
        }
        $("#load_more_news").on('click', function (e) {
            e.preventDefault();
            $('.page_loader').show();
            $('html').css('overflow-y','hidden');
            $(".filtersdatanews:hidden").slice(0, 6).slideDown();
            if ($(".filtersdatanews:hidden").length == 0) {
                $("#load_more_news").fadeOut('slow');
            }
            $('html,body').animate({
                scrollTop: $(this).offset().top
            }, 1500);
            setTimeout(
                function()
                {
                    $('html').css('overflow-y','scroll');
                    $('.page_loader').hide();
                }, 1500);
        });
    });
</script>


<!--<script src="<?php //echo base_url('assets/frontend/js/calendar.js'); ?>"></script>-->



<div class="page_loader">

    <div class="lds-spinner" style="100%;height:100%">

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

    </div>

</div>
<script src="<?php echo base_url('assets/frontend/bootstrap-5.0.2/js/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/bootstrap-5.0.2/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/owl-carousel/owl.carousel.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/frontend/jquery-validation-1.19.1/dist/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/jquery-validation-1.19.1/dist/additional-methods.min.js'); ?>"></script>
<?php if($lang != 'eng'){?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/localization/messages_ar.min.js"></script>
<?php } ?>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="<?php echo base_url('assets/frontend/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/jquery.counterup.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/main.js?v=' . rand()); ?>"></script>
<script src="<?php echo base_url('assets/frontend/custom.js?v=' . rand()); ?>"></script>

<?php if ($tpl_name == 'history') { ?>
    <script src="<?php echo base_url('assets/frontend/jquery-timeline/jquery.timelify.js'); ?>"></script>
    <script>
        $('.timeline').timelify({
            animLeft: "fadeInLeft",
            animCenter: "fadeInUp",
            animRight: "fadeInRight",
            animSpeed: 600,
            offset: 150
        });
    </script>
<?php } ?>

<script type="text/javascript">
    $('.owl-carousel-companies').owlCarousel({
        loop:true,
        margin:25,
        navText: ["<img src='<?php echo base_url('assets/frontend/images/arrow-left.svg'); ?>'>","<img src='<?php echo base_url('assets/frontend/images/arrow-right.svg'); ?>'>"],
        nav: true,
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        dots: false,
        autoplay: true,
        rtl: (lang == 'eng' ? false : true),
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            992:{
                items:3
            },
            1024: {
                items:4
            }
        }
    });
    $('.owl-carousel-news').owlCarousel({
        loop: true,
        margin:10,
        navText: ["<img src='<?php echo base_url('assets/frontend/images/arrow-left.svg'); ?>'>","<img src='<?php echo base_url('assets/frontend/images/arrow-right.svg'); ?>'>"],
        nav: true,
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        dots: false,
        autoplay: true,
        items: 2,
        rtl: (lang == 'eng' ? false : true),
        responsive:{
            0:{
                items:1
            },
            530:{
                items:2
            }

        }

    });

    $('.owl-carousel-sectors').owlCarousel({
        loop:true,
        margin:-20,
        navText: ["<img src='<?php echo base_url('assets/frontend/images/arrow-left-sector.svg'); ?>'>","<img src='<?php echo base_url('assets/frontend/images/arrow-right-sector.svg'); ?>'>"],
        nav: true,
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        dots: false,
        autoplay: true,
        rtl: (lang == 'eng' ? false : true),

        responsive:{
            0:{
                items:2
            },
            400:{
                items:3
            },
            600:{
                items:4
            },
            992:{
                items:6
            }
        }
    });

    $(document).on('click', '.sectors_nav', function() {
        $('.sectors_nav').removeClass('active');
        $(this).addClass('active');

        $(".sectors-section").removeClass('real-estate-bg');
        if ($(this).data('show-border') == 1) {
            $(".sectors-section").addClass('real-estate-bg');
        }
    });

    var owl_carousel_sectors_nav = $('.owl-carousel-sectors-nav');
    owl_carousel_sectors_nav.owlCarousel({
        // loop:true,
        // margin:-20,
        navText: ["<img src='<?php echo base_url('assets/frontend/images/arrow-left-sector.svg'); ?>'>","<img src='<?php echo base_url('assets/frontend/images/arrow-right-sector.svg'); ?>'>"],
        nav: true,
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        dots: false,
        // autoplay: true,
        rtl: (lang == 'eng' ? false : true),
        responsive:{
            0:{
                items:2
            },
            400:{
                items:3
            },
            600:{
                items:4
            },
            992:{
                items:6
            }
        }
    });

    $('.events-popup-slider').owlCarousel({
        loop:true,
        navText: ["<img src='<?php echo base_url('assets/frontend/images/arrow-left-sector.svg'); ?>'>","<img src='<?php echo base_url('assets/frontend/images/arrow-right-sector.svg'); ?>'>"],
        nav: true,
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        dots: false,
        autoplay: true,
        items: 1,
        rtl: (lang == 'eng' ? false : true),
    });

    window.onscroll = function() {myFunction()};

    var header = document.getElementById("site-header");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }

    $(document).ready(function($) {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });


</script>



</body>
</html>