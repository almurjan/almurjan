<?php
$home_id = getPageIdbyTemplate('home');
$logo = base_url() . 'assets/script/' . content_detail('eng_logo_image_mkey_hdn', $home_id);
$logo_url_link = lang_base_url();
if ($lang == 'eng') {
    $segmentMed = $this->uri->segment(1);
}
else {
    $segmentMed = $this->uri->segment(2);
}
if($segmentMed == 'med'){
    $logo_url_link = '#';
}
$selfapp_image = base_url() . 'assets/frontend/images/med2.jpg';
$social_links = social_links();
?>
<header id="site-header" class="<?php echo ($page_id == 54 ? 'position-absolute' : ''); ?> header-z-index w-100">
    <?php if($segmentMed != 'med'){ ?>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xxl-2 col-xl-2 col-lg-2 col-md-2 col-sm-3 col-3">
                <!--<img class="img-fluid murjan-logo" src="<?php /*echo base_url('assets/frontend/images/logo.png'); */?>" alt="">-->
                <a href="<?php echo $logo_url_link;?>"><img class="img-fluid murjan-logo" src="<?php echo base_url('assets/frontend/images/logo.png'); ?>" alt=""></a>
            </div>
            <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-7 col-sm-5 col-5">
                <ul class="main-menu">
                    <?php

                    if($segmentMed != 'med'){
                        echo header_menu(1, $page_id);
                    }
                    ?>
                </ul>
            </div>
            <div class="col-xxl-2 col-xl-2 col-lg-2 col-md-3 col-sm-4 col-4">
                <ul class="social-icons">
                    <li><a href="<?php echo $social_links->soc_tw; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="<?php echo $social_links->soc_insta; ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                    <?php if($lang == 'eng'){ ?>
                        <li><a href="javascript:void(0);" onClick="redirect('arb');">عربي <!--<img src="<?php /*echo base_url('assets/frontend/images/lang-ar.svg'); */?>" alt="">--></a></li>
                    <?php }else{?>
                        <li><a href="javascript:void(0);" onClick="redirect('eng');"> EN <!-- <img src="<?php echo base_url('assets/frontend') ?>/images/english1.png" alt=""> --></a></li>
                    <?php } ?>

                    <li><a href="javascript:void(0);" class="searcIcon"><i class="fas fa-search"></i></a></li>
                    <form action="<?php echo lang_base_url().'page/search'; ?>" class="searchBox"  method="get">
                        <div class="form-group">
                            <input name="item_title" type="text" class="form-control" placeholder="<?php echo translate($lang, 'Type here to search', 'اكتب هنا للبحث'); ?>..." />
                            <i class="fas fa-times crosIcon"></i>
                        </div>
                    </form>
                    <li><a class="hide-me-on768" data-bs-toggle="offcanvas" href="#offcanvas" role="button" aria-controls="offcanvas"><i class="fa fa-bars"></i></a></li>
                </ul>
                <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvas" aria-labelledby="offcanvasExampleLabel">
                    <div class="offcanvas-header">
                        <h5 class="offcanvas-title" id="offcanvasExampleLabel"><?php echo translate($lang, 'Navigation', 'الرجاء التحديد'); ?></h5>
                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                        <ul class="mobile-menu">
                            <!--<li><a href="#">Home</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Sectors</a></li>
                            <li><a href="#">Events & News</a></li>
                            <li><a href="#">CSR</a></li>
                            <li><a href="#">Contact Us</a></li>-->
                            <?php

                            if($segmentMed != 'med'){
                                echo header_menu(1, $page_id);
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }else{ ?>
    <div class="container">
        <div class="row HeaderPadding align-items-center">
            <div class="col-12 col-sm-9 col-md-9 col-lg-6 col-xl-6 AlmurjanLogo text-left">
                <a href="<?php echo $logo_url_link;?>"><img class="img-fluid Mainlogosize" src="<?php echo base_url('assets/frontend/images/logo.png'); ?>" alt=""></a>

                <a href="<?php echo $logo_url_link;?>"><img class="logosize" src="<?php echo $selfapp_image; ?>" alt=""></a>
            </div>
            <div class="col-12 col-sm-3 col-md-3 col-lg-6 col-xl-6 MainTopMenu text-right ">
                <div class="EN_ARB d-none">
                    <?php if($lang == 'eng'){ ?>
                        <a href="#" onClick="redirect('arb');"><img src="<?php echo base_url('assets/frontend') ?>/images/arabic.png" alt=""></a>
                    <?php }else{?>
                        <a href="#" onClick="redirect('eng');"> EN <!-- <img src="<?php echo base_url('assets/frontend') ?>/images/english1.png" alt=""> --></a>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>

<?php } ?>

</header>