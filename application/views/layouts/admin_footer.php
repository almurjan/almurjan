<?php $admin_lang = check_admin_lang();
if($admin_lang['admin_lang'] == 'eng'){
    $rtl = '';
}else{
    $rtl = 'rtl/';
}?>
<div class="uk-width-medium-1-3">
    <button class="md-btn alert-message-button" data-uk-modal="{target:'#modal_default', bgclose:false}" style="display:none;">Open</button>
	
	<button class="md-btn validation-message-button" data-uk-modal="{target:'#modal_validation'}" style="display:none;">Open</button>
	
	<div class="uk-modal" id="modal_default">   
		 <div class="uk-modal-dialog">         
		 <button type="button" class="uk-modal-close uk-close admin-modal"></button>      
		 <h2 id="alert-message-heading"></h2> 
		 <div id="alert-message"></div>   
		 </div>   
	</div>
	
	<div class="uk-modal" id="modal_validation">   
		 <div class="uk-modal-dialog">         
		 <button type="button" class="uk-modal-close uk-close"></button>      
		 <h2 id="validation-message-heading"></h2> 
		 <div id="validation-message"></div>   
		 </div>   
	</div>

</div>

<!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
	<!-- Chosen Jquery plugin for Multiple select options -->

	<script src="<?php echo base_url('assets/frontend/chosen_v1.8.7/chosen.jquery.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/frontend/chosen_v1.8.7/docsupport/prism.js'); ?>" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo base_url('assets/frontend/chosen_v1.8.7/docsupport/init.js'); ?>" type="text/javascript" charset="utf-8"></script>
    <!-- common functions -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/common.min.js"></script>

    <!-- uikit functions -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/altair_admin_common.js?v=1.7"></script>

    <!-- page specific plugins -->
    <!-- d3 -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/d3/d3.min.js"></script>
    <!-- peity (small charts) -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/peity/jquery.peity.min.js"></script>
    <!-- easy-pie-chart (circular statistics) -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
    <!-- countUp -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/countUp.js/dist/countUp.min.js"></script>
    <!-- handlebars.js -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/handlebars/handlebars.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/js/custom/handlebars_helpers.min.js"></script>
    <!-- CLNDR -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/clndr/clndr.min.js"></script>
    <!-- fitvids -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/fitvids/jquery.fitvids.js"></script>

    <!--kendoui Js-->
    <!-- kendo UI -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/js/kendoui_custom.min.js"></script>

    <!--  kendoui functions -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/js/pages/kendoui.min.js"></script>

    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/js/pages/kendoui.min.js"></script>

    <link href="https://cdn.datatables.net/rowreorder/1.0.0/css/rowReorder.dataTables.min.css" type="text/css" rel="stylesheet">

    <?php if($this->uri->segment('2') !== 'dashboard'){?>
        <!-- datatables -->
        <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/datatables/media/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/js/custom/datatables_uikit.min.js"></script>
        <script src="<?php echo base_url();?>assets/admin/assets/js/script.js?v=1.2"></script>
        <!-- datatables colVis-->
    <?php } ?>
    
    <!--  datatables functions -->
    <script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/js/pages/plugins_datatables.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/js/pages/ecommerce_product_edit.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/jquery.cookie.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
	
	 <?php if($admin_lang['admin_lang'] != 'eng'){?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/localization/messages_ar.min.js"></script>
	 <?php } ?>
	<!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mjs.nestedSortable.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/admin/tooltipster/dist/js/tooltipster.bundle.min.js"></script>
	
	<!-- ratings -->
	<script type='text/javascript' src='<?php echo base_url().'assets/'?>js-front/jquery.ratingb2ab.js' defer></script>
	<script type='text/javascript' src='<?php echo base_url().'assets/'?>js-front/jquery.wpcf7-starrating7ef2.js' defer></script>

	<!-- ckeditor -->
    
	<script src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>assets/tinymce/tinymce.min.js"></script>
	<script>
		tinymce.init({
			selector: '.arb_text',
			plugins: "lists, advlist, code, link",
			toolbar: "numlist bullist code",
			directionality : "rtl",
			height: 250
		});
		tinymce.init({
			selector: '.eng_text',
			plugins: "lists, advlist, code, link",
			toolbar: "numlist bullist code",
			height: 250
		});
	</script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script>
	$(document).on('click','.removeImageAdded',function (e){
		var img = $(this).attr('data-id');
		var field = $(this).attr('data-id-field');
		var vaule = new Array();
		var new_val = $("#"+field).val();
		//new_val = new_val.split(",");
		
		new_val = new_val.replace(img, '')
		var final_value = new_val.replace(/,\s*$/, "");
		$("#"+field).val(final_value);
		$(this).parent().parent().remove();

	});
        function initCk() {
            /* $('.eng_text').each(function (e) {

                if(this.id === 'eng_quote_text_home_div' || this.id === 'eng_club_text_' || this.id === 'eng_cm_quote_text_comedians_div' || this.id === 'eng_address_' || this.id === 'eng_subscribe_text_blog_div') {
                    CKEDITOR.replace(this.id, {toolbar: 'Full', width: '95%', height: '200px',enterMode : CKEDITOR.ENTER_BR, shiftEnterMode: CKEDITOR.ENTER_P});
                }else{
                    CKEDITOR.replace(this.id, {toolbar: 'Full', width: '95%', height: '200px'});
                }
                CKEDITOR.config.allowedContent = true;

            });

            $('.arb_text').each(function (e) {

                if(this.id === 'arb_quote_text_home_div' || this.id === 'arb_club_text_' || this.id === 'arb_cm_quote_text_comedians_div' || this.id === 'arb_address_' || this.id === 'arb_subscribe_text_blog_div') {
                    CKEDITOR.replace(this.id, {language: 'ar', toolbar: 'Full', width: '95%', height: '200px',enterMode : CKEDITOR.ENTER_BR, shiftEnterMode: CKEDITOR.ENTER_P});
                }else{
                    CKEDITOR.replace(this.id, {language: 'ar', toolbar: 'Full', width: '95%', height: '200px'});
                }
                CKEDITOR.config.allowedContent = true;

            }); */
        }

        function removeCk() {
            $('.eng_text').each(function (e) {
                var instance_eng = CKEDITOR.instances[this.id];
                if (instance_eng) {
                    instance_eng.destroy(true);
                }
            });
            $('.arb_text').each(function (e) {
                var instance_arb = CKEDITOR.instances[this.id];
                if (instance_arb) {
                    instance_arb.destroy(true);
                }
            });

        }
        $(function() {
            if(isHighDensity) {
                // enable hires images
                altair_helpers.retina_images();
            }
            if(Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });
        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
    </script>
	
	<script>
		/* $( function() {
			  $( "#datepicker" ).datetimepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2013',
				showMillisec: null
			  });
			} ); */
	</script>

    <div id="style_switcher">
        <div id="style_switcher_toggle"><i class="material-icons autoTooltip" title="<?php echo $admin_lang['label']['tooltip_settings_icon']; ?>">&#xE8B8;</i></div>
        <div class="uk-margin-medium-bottom">
            <h4 class="heading_c uk-margin-bottom"><?php echo $admin_lang['label']['theme_colors']; ?></h4>
            <ul class="switcher_app_themes" id="theme_switcher">
                <li class="app_style_default active_theme" data-app-theme="">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_a" data-app-theme="app_theme_a">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_b" data-app-theme="app_theme_b">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_c" data-app-theme="app_theme_c">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_d" data-app-theme="app_theme_d">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_e" data-app-theme="app_theme_e">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_f" data-app-theme="app_theme_f">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_g" data-app-theme="app_theme_g">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_h" data-app-theme="app_theme_h">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_i" data-app-theme="app_theme_i">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_dark" data-app-theme="app_theme_dark">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
            </ul>
        </div>
        <div class="uk-visible-large uk-margin-medium-bottom">
            <h4 class="heading_c"><?php echo $admin_lang['label']['theme_sidebar']; ?></h4>
            <p>
                <input type="checkbox" name="style_sidebar_mini" id="style_sidebar_mini" data-md-icheck />
                <label for="style_sidebar_mini" class="inline-label"><?php echo $admin_lang['label']['theme_mini_sidebar']; ?></label>
            </p>
            <p>
                <input type="checkbox" name="style_sidebar_slim" id="style_sidebar_slim" data-md-icheck />
                <label for="style_sidebar_slim" class="inline-label"><?php echo $admin_lang['label']['theme_slim_sidebar']; ?></label>
            </p>
        </div>
        <div class="uk-visible-large uk-margin-medium-bottom">
            <h4 class="heading_c"><?php echo $admin_lang['label']['theme_layout']; ?></h4>
            <p>
                <input type="checkbox" name="style_layout_boxed" id="style_layout_boxed" data-md-icheck />
                <label for="style_layout_boxed" class="inline-label"><?php echo $admin_lang['label']['theme_boxed_layout']; ?></label>
            </p>
        </div>
        <div class="uk-visible-large">
            <h4 class="heading_c"><?php echo $admin_lang['label']['theme_menu_accordion']; ?></h4>
            <p>
                <input type="checkbox" name="accordion_mode_main_menu" id="accordion_mode_main_menu" data-md-icheck />
                <label for="accordion_mode_main_menu" class="inline-label"><?php echo $admin_lang['label']['theme_accordion_mode']; ?></label>
            </p>
        </div>
    </div>

<!--these variables for page sepecify script-->
<?php
    $path = $this->uri->segment(2);
    $full_path = $this->uri->segment(2).'/'.$this->uri->segment(3);
?>


    <script>
        $(function() {
            var $switcher = $('#style_switcher'),
                $switcher_toggle = $('#style_switcher_toggle'),
                $theme_switcher = $('#theme_switcher'),
                $mini_sidebar_toggle = $('#style_sidebar_mini'),
                $slim_sidebar_toggle = $('#style_sidebar_slim'),
                $boxed_layout_toggle = $('#style_layout_boxed'),
                $accordion_mode_toggle = $('#accordion_mode_main_menu'),
                $html = $('html'),
                $body = $('body');

            
            $switcher_toggle.click(function(e) {
                e.preventDefault();
                $switcher.toggleClass('switcher_active');
            });

            $theme_switcher.children('li').click(function(e) {
                e.preventDefault();
                var $this = $(this),
                    this_theme = $this.attr('data-app-theme');

                $theme_switcher.children('li').removeClass('active_theme');
                $(this).addClass('active_theme');
                $html
                    .removeClass('app_theme_a app_theme_b app_theme_c app_theme_d app_theme_e app_theme_f app_theme_g app_theme_h app_theme_i app_theme_dark')
                    .addClass(this_theme);

                if(this_theme == '') {
                    localStorage.removeItem('altair_theme');
                } else {
                    localStorage.setItem("altair_theme", this_theme);
                    if(this_theme == 'app_theme_dark') {
                        $('#kendoCSS').attr('href','bower_components/kendo-ui/styles/kendo.materialblack.min.css')
                    }
                }

            });

            // hide style switcher
            $document.on('click keyup', function(e) {
                if( $switcher.hasClass('switcher_active') ) {
                    if (
                        ( !$(e.target).closest($switcher).length )
                        || ( e.keyCode == 27 )
                    ) {
                        $switcher.removeClass('switcher_active');
                    }
                }
            });

            // get theme from local storage
            if(localStorage.getItem("altair_theme") !== null) {
                $theme_switcher.children('li[data-app-theme='+localStorage.getItem("altair_theme")+']').click();
            }


        // toggle mini sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_mini") !== null && localStorage.getItem("altair_sidebar_mini") == '1') || $body.hasClass('sidebar_mini')) {
                $mini_sidebar_toggle.iCheck('check');
            }

            $mini_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_mini", '1');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                });

        // toggle slim sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_slim") !== null && localStorage.getItem("altair_sidebar_slim") == '1') || $body.hasClass('sidebar_slim')) {
                $slim_sidebar_toggle.iCheck('check');
            }

            $slim_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_slim", '1');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                });

        // toggle boxed layout

            if((localStorage.getItem("altair_layout") !== null && localStorage.getItem("altair_layout") == 'boxed') || $body.hasClass('boxed_layout')) {
                $boxed_layout_toggle.iCheck('check');
                $body.addClass('boxed_layout');
                $(window).resize();
            }

            $boxed_layout_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_layout", 'boxed');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_layout');
                    location.reload(true);
                });

        // main menu accordion mode
            if($sidebar_main.hasClass('accordion_mode')) {
                $accordion_mode_toggle.iCheck('check');
            }

            $accordion_mode_toggle
                .on('ifChecked', function(){
                    $sidebar_main.addClass('accordion_mode');
                })
                .on('ifUnchecked', function(){
                    $sidebar_main.removeClass('accordion_mode');
                });


        });

    $().ready(function() {
        // validate the comment form when it is submitted
        $('#submit').click(function(e) {
            e.preventDefault();

            $("#site_form").submit();
            $("#siteup_form").submit();
        });
        $("#site_form").validate();
        $("#siteup_form").validate();
    });

    function get_help(){
        src='https://www.google.com';
        $.modal('<iframe src="' + src + '" height="450" width="830" style="border:0">', {
            closeHTML:"",
            containerCss:{
                backgroundColor:"#fff",
                borderColor:"#fff",
                height:450,
                padding:0,
                width:830
            },
            overlayClose:true
        });

    }

    $(document).ready(function(){


        $("#select_all1").click(function(){
            var checked_status = this.checked;
            $("input[id='select']").each(function(){
                this.checked = checked_status;
            });

        });

        $("#select_all2").click(function(){
            var checked_status = this.checked;
            $("input[id='select2']").each(function(){
                this.checked = checked_status;
            });

        });

        $("#select_all3").click(function(){
            var checked_status = this.checked;
            $("input[id='select3']").each(function(){
                this.checked = checked_status;
            });

        });
        $("#select_all4").click(function(){
            var checked_status = this.checked;
            $("input[id='select4']").each(function(){
                this.checked = checked_status;
            });

        });
        $("#select_all5").click(function(){
            var checked_status = this.checked;
            $("input[id='select5']").each(function(){
                this.checked = checked_status;
            });

        });
        $("#select_all6").click(function(){
            var checked_status = this.checked;
            $("input[id='select6']").each(function(){
                this.checked = checked_status;
            });

        });


    });

    function pub_status(vals){
        //alert(vals);
        $("#pub_val").val(vals);
    }

    function randomPassword() {
        var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$_+?%^&)";
        var size = 10;
        var i = 1;
        var ret = ""
        while ( i <= size ) {
            $max = chars.length-1;
            $num = Math.floor(Math.random()*$max);
            $temp = chars.substr($num, 1);
            ret += $temp;
            i++;
        }
        $('#usr_pass').attr('type', 'text');
        $("#usr_pass").val(ret);
    }

    function hidemaster(i,vals){

        var cr = site_form.elements["create_"+vals];
        var re = site_form.elements["read_"+vals];
        var up = site_form.elements["upd_"+vals];
        var de = site_form.elements["del_"+vals];
        var pu = site_form.elements["pub_"+vals];

        if (cr.checked == true && re.checked==true && up.checked==true && de.checked==true && pu.checked==true)
        {
            $("input[name='select["+i+"]']").prop('checked', true);
            $("input[name='select["+i+"]']").css("opacity","");
        }
        else
        {
            $("input[name='select["+i+"]']").prop('checked', true);
            $("input[name='select["+i+"]']").fadeTo('slow', 0.5);
        }

    }

</script>
<script>
    <?php
    $id  = $this->uri->segment(4)? $this->uri->segment(4):1;
    ?>
    $(document).ready(function(){
        var ns = $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div',
            helper:	'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 2,
            isTree: true,
            expandOnHover: 700,
            startCollapsed: false,
            update: function () {

                var arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
                $.post("<?php echo base_url();?>ems/menu/menu_pos_update/",
                    {sortable:arraied,id: <?php echo $id;?>},
                    function (data) { location.reload();}
                );
            }

        });

        $('.expandEditor').attr('title','Click to show/hide item editor');
        $('.disclose').attr('title','Click to show/hide children');
        $('.deleteMenu').attr('title', 'Click to delete item.');

        $('.disclose').on('click', function() {
            $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            $(this).toggleClass('ui-icon-plusthick').toggleClass('ui-icon-minusthick');
        });

        $('.expandEditor, .itemTitle').click(function(){
            var id = $(this).attr('data-id');
            $('#menuEdit'+id).toggle();
            $(this).toggleClass('ui-icon-triangle-1-n').toggleClass('ui-icon-triangle-1-s');
        });

        $('.deleteMenu').click(function(){
            var id = $(this).attr('data-id');
            $('#menuItem_'+id).remove();
        });

    });
$(function($) {
    // Asynchronously Load the map API
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyAZfo9eInMuIGx4s4qBI6ewc2NVkd4gHOQ&sensor=false&callback=initialize";
    document.body.appendChild(script);

});
var myOptions = '';
var gmarkers = [];

function initialize() {

    var map;
    var bounds = new google.maps.LatLngBounds();
    var myLatlng = new google.maps.LatLng(24.720403782761128,46.678762435913086);
    var myOptions = {
        zoom: 11,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControlOptions: {position: google.maps.ControlPosition.CENTRE}
    };
    map = new google.maps.Map(document.getElementById("location_map"), myOptions);
    google.maps.event.addListener(map, "click", function(event) {
        removeMarkers();
        var lat = event.latLng.lat();
        var lng = event.latLng.lng();

        placeMarker(event.latLng);
         $("#maps_loc").val(lat + "," + lng);
        $("#maps_loc").focus();
    });

    function placeMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        gmarkers.push(marker);
    }

    function removeMarkers() {
        for (i = 0; i < gmarkers.length; i++) {
            gmarkers[i].setMap(null);
        }
    }
}
    $(document).on("click","#load_map",function(){
        $('#btn_location').click();
        initialize();
    });

    $(document).on("click","#close_popup",function(){
        $('#modal_location .uk-modal-close').click();
    });

    $(document).on("change","#lang_switcher", function(){
        $(".page_loader").show();
        var get_val = $(this).val();
        if(get_val === 'gb'){
            admin_lang = 'eng';
            }else{
            admin_lang = 'arb';
        }
        $.ajax({
            url : '<?php echo base_url()?>ems/admin_login/set_admin_lang',
            type: "post",
            async: false,
            data : {admin_lang:admin_lang},
            success: function(data){
                return true;
            }
        });
        location.reload();
    });

    var ttstoids = [];
    $(document).on("click","#help_icon",function(){

        for (tti = 0; tti < ttstoids.length; tti++) {
            clearTimeout(ttstoids[tti]);
            $('.autoTooltip').trigger('mouseleave');
        }

        var titeration = 0;
        var tdelay = 3000;
        $('.autoTooltip').each(function(){
            var tthisref = this;
            ttstoids[titeration] = setTimeout(function(){
                $('.autoTooltip').trigger('mouseleave');
                $(tthisref).trigger('mouseenter');
            }, tdelay*titeration);
            titeration++;
        });

        setTimeout(function(){
            $('.autoTooltip').trigger('mouseleave');
        }, tdelay*titeration);

    });
	
	
</script>

<script src="<?php echo base_url();?>assets/admin/assets/js/kendoui_custom.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/assets/js/pages/kendoui.min.js"></script>
 
 <script>
	 $(document).ready(function () {
		 
		// create DateTimePicker from input HTML element
		//value: new Date(), Commenting this because its picking up the current date and time
		$(".datetimepickerNew").kendoDatePicker({
			dateInput: true,
			
		});
		
		$(".kUI_datetimepicker_basic").kendoDateTimePicker({
		  dateInput: true
		});
	});
 </script>
 
<!-- JS Cookie Library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>

<script>
	$(document).on('change', '#start_date_sel', function() {
		var start_date = $(this).val();
		var end_date = $('#end_date_sel').val();
		if (new Date(start_date) > new Date(end_date)) {
			alert('start date must be before end date');
			$('#saveAbout').css({"pointer-events": "none",
			"cursor": "default"});
			$('#err_msg_dates').html("<p>Select valid start & end dates</p>");
		} else {
			$('#saveAbout').css({"pointer-events": "",
			"cursor": ""});
			$('#err_msg_dates').html("<p></p>");
		}	
	});
	
	$(document).on('change', '#end_date_sel', function() {
		var end_date = $(this).val();
		var start_date = $('#start_date_sel').val();
		if (new Date(start_date) > new Date(end_date)) {
			alert('start date must be before end date');
			$('#saveAbout').css({"pointer-events": "none",
			"cursor": "default"});
			$('#err_msg_dates').html("<p>Select valid start & end dates</p>");
		} else {
			$('#saveAbout').css({"pointer-events": "",
			"cursor": ""});
			$('#err_msg_dates').html("<p></p>");
		}	
	});
	
	$(document).on('change', '.contactDate', function(){
		var val = $(this).val();
		if(val != ''){
			Cookies.set('date_contact', val, {path: '/'});
			location.reload();
		}
	});
	
	$(document).on('change', '.date_request', function(){
		var val = $(this).val();
		if(val != ''){
			Cookies.set('date_request', val, {path: '/'});
			location.reload();
		}
	});
	
	$(document).on('change', '.country', function(){
		var val = $(this).val();
		if(val != ''){
			Cookies.set('country', val, {path: '/'});
			location.reload();
		}
	});
	
	$(document).on('change', '.city', function(){
		var val = $(this).val();
		if(val != ''){
			Cookies.set('city', val, {path: '/'});
			location.reload();
		}
	});
	
	$(document).on('change', '.services', function(){
		var val = $(this).val();
		if(val != ''){
			Cookies.set('services', val, {path: '/'});
			location.reload();
		}
	});
	
	$(document).on('change', '.job_title', function(){
		var val = $(this).val();
		if(val != ''){
			Cookies.set('job_id', val, {path: '/'});
			location.reload();
		}
	});
	$(document).on('change', '.education', function(){
		var val = $(this).val();
		if(val != ''){
			Cookies.set('education', val, {path: '/'});
			location.reload();
		}
	});
	$(document).on('change', '.gender', function(){
		var val = $(this).val();
		if(val != ''){
			Cookies.set('gender', val, {path: '/'});
			location.reload();
		}
	});
	
	$(window).load(function() {
	  // When the page has loaded
		$('#saveAbout').css('display','block');
		$('#saveSocial').css('display','block');
	});
	<?php if(isset($_GET['show_all']) && $_GET['show_all'] = 'clear'){ ?>
			$(document).ready(function(){
				var uri = window.location.toString();
				if (uri.indexOf("?") > 0) {
					var clean_uri = uri.substring(0, uri.indexOf("?"));
					window.history.replaceState({}, document.title, clean_uri);
				}
			});
	<?php } ?>
	
</script>

<script type="text/javascript">
    $(document).ready(function(){
        // Adding CSRF to AJAX
        $.ajaxSetup({
            beforeSend:function(jqXHR, Obj){
                var value = "; " + document.cookie;
                var parts = value.split("; csrf_cookie_name=");
                if(parts.length == 2)
                    Obj.data += '&csrf_test_name='+parts.pop().split(";").shift();
            }
        });
    });
</script>
 
<button class="md-btn" id="btn_location" data-uk-modal="{target:'#modal_location'}" style="display:none;">Open</button>
<div class="uk-modal" id="modal_location">
    <div class="uk-modal-dialog">
        <button type="button" class="uk-modal-close uk-close"></button>
        <h2 id="location-message-heading"></h2>
        <div id="location-message">
            <div id="location_map" style="height: 320px; position: relative; overflow: hidden;"></div>
        </div>

        <button style="margin-left: 38%; margin-top: 20px;" id="close_popup" type="button" class=" md-btn md-btn-primary"><?php echo $admin_lang['label']['theme_choose']; ?></button>

    </div>
</div>
<button class="md-btn" id="btn_notifications" data-uk-modal="{target:'#modal_notifications'}" style="display:none;">Open</button>
<div class="uk-modal" id="modal_notifications">
    <div class="uk-modal-dialog">
        <button type="button" class="uk-modal-close uk-close"></button>
        <h2 id="message-heading"></h2>
        <div id="modal-message">
            Record saved successfully.
        </div>
    </div>
</div>

<div class="page_loader"><div class="lds-spinner" style="100%;height:100%"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
</body>
</html>