<style>
    .border{
        border: 2px solid red !important;
    }
    .NewsLetters > .border{
        border: 2px solid red !important;
    }
</style>
<?php

$home_id = getPageIdbyTemplate('home');
$sitemap = getPageIdbyTemplate('sitemap');

$config_settings = getConfigurationSetting();

$contactUsId = getPageIdByTemplate('contact_us');
$bodId = getPageIdByTemplate('bod');

$social_links = social_links();

$privacy_policy = getPageIdbyTemplate('privacy_policy');

$terms = getPageIdbyTemplate('terms');

$sitemap = getPageIdbyTemplate('sitemap');

$murjan_holding = getPageIdbyTemplate('murjan_holding');

$murjan_holding_listings = ListingContent($murjan_holding);
$slider_counter=0;
foreach($murjan_holding_listings as $murjan_holding_listing){

    if(content_detail('is_show_home', $murjan_holding_listing->id) == 1){
        $slider_counter++;
    }
}
$holding_logo_slider=$slider_counter;
$footerlogo = base_url() . 'assets/script/' . content_detail('eng_footer_logo_image_mkey_hdn', $home_id);

$companies_map = getPageIdbyTemplate('companies_map');

$mapPinsListing = ListingContent($companies_map);
?>
<?php
if ($lang == 'eng') {
    $segmentMed = $this->uri->segment(1);
}
else {
    $segmentMed = $this->uri->segment(2);
}

?>
<?php
if ($lang == 'eng') {
    $segmentMed = $this->uri->segment(1);
}
else {
    $segmentMed = $this->uri->segment(2);
}
if($segmentMed != 'med'){
?>
<footer class="blue-footer">
    <div class="container">
        <div class="row">
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                <div class="get-in-touch has-second-color">
                    <h2><?php echo ($lang == 'eng' ? 'Get in touch' : 'ابقى على تواصل');?></h2>
                    <p><?php echo content_detail($lang.'_desc_contact',$contactUsId); ?></p>
                    <ul>
                        <li><span><?php echo ($lang == 'eng' ? 'Phone:' : 'رقم الهاتف:');?></span> <span class="footer-phone"><?php echo content_detail('eng_phone',$contactUsId); ?></span></li>
                        <li><span><?php echo ($lang == 'eng' ? 'E-Mail:' : 'البريد الإلكتروني:');?></span> <?php echo content_detail('eng_mail',$contactUsId); ?></li>
                    </ul>
                </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="footer-menu">
                    <ul>
                        <?php echo footer_menu(2,$page_id); ?>
                    </ul>
                </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-3 col-sm-12 col-12 d-flex justify-content-end align-items-end styled-on-767">
                <div class="social-links-footer">
                    <ul>
                        <li><a href="<?php echo $social_links->soc_tw; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="<?php echo $social_links->soc_lin; ?>" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        <li><a href="<?php echo $social_links->soc_insta; ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="<?php echo $social_links->soc_you; ?>" target="_blank"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php }else{ ?>



    <style>
        .newsLetterDiv{
            display: none;
        }
    </style>
    <footer style="margin-top: 30px; padding-bottom: 20px; background: url('<?php echo base_url('assets/frontend') ?>/images/footer_shape.png') repeat-x center top, #00447c; background-size: cover;">

        <div class="container">

            <div class="row PaddingTop60">

                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 text-md-center text-lg-center text-xl-center">

                </div>

                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 FooterCol">

                </div>

                <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5 FooterCol">

                    <h2><?php echo ($lang == 'eng' ? 'Get in touch' : 'ابقى على تواصل');?></h2>

                    <p><?php echo content_detail($lang.'_desc_contact',$contactUsId); ?></p>

                    <p><?php echo ($lang == 'eng' ? 'Phone:' : 'رقم الهاتف:');?> <span style=" <?php echo ($lang != 'eng')?'display: inline-block;direction: ltr':'' ?>"><?php echo content_detail('eng_phone',$contactUsId); ?></span></p>

                    <p><?php echo content_detail('eng_officeEmail',$contactUsId); ?></p>

                </div>

            </div>

            <hr class="Footer">

            <div class="row">

                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">

                    <ul class="FooterLastNav">

                        <li><a href="#">
                                <?php
                                if($lang == 'eng'){
                                    $text = 'All rights reserved 2020.';

                                }else{
                                    $text = 'جميع الحقوق محفوظة جمعية الوداد الخيرية ٢٠١٨';

                                }
                                $yearFinal = str_replace('2020',date('Y'),$text);
                                $yearFinal = str_replace('٢٠١٨',date('Y'),$yearFinal);
                                echo ($lang=='arb' ? convertToArabic($yearFinal) : $yearFinal);
                                ?>
                            </a></li>
                    </ul>

                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 socialLinks">



                    <?php if($social_links->soc_fb != ''){ ?>

                        <a href="<?php echo $social_links->soc_fb; ?>" target="_blank"><i class="fab fa-facebook-f" ></i></a>

                    <?php } ?>



                    <?php if($social_links->soc_tw != ''){ ?>

                        <a href="<?php echo $social_links->soc_tw; ?>" target="_blank"><i class="fab fa-twitter"></i></a>

                    <?php } ?>



                    <?php if($social_links->soc_lin != ''){ ?>

                        <a href="<?php echo $social_links->soc_lin; ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>

                    <?php } ?>



                    <?php if($social_links->soc_you != ''){ ?>

                        <a href="<?php echo $social_links->soc_you; ?>" target="_blank"><i class="fab fa-youtube"></i></a>

                    <?php } ?>



                    <?php if($social_links->soc_insta != ''){ ?>

                        <a href="<?php echo $social_links->soc_insta; ?>" target="_blank"><i class="fab fa-instagram"></i></a>

                    <?php } ?>

                </div>

            </div>

        </div>

    </footer>
<?php } ?>