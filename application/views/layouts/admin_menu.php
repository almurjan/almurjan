 <!-- main sidebar -->
 <?php
    $admin_lang = check_admin_lang();
    $module = $this->uri->segment(2);

    $access = $this->uri->segment(5);
    $access_index = $this->uri->segment(4);
    $gal_module = $this->session->userdata('gal_module');
    $this->session->unset_userdata('gal_module');
    if ($access == 'access') {
        $this->session->set_userdata(array('access' => '1', 'usr_level' => '2', 'site_id' => '1'));
    }

    $uid_level = $this->session->userdata('usr_level');
    $admin_access = $this->session->userdata('access');

    $hidden_menu_ids = [17, 18, 19, 20, 21];

    ?>
 <aside id="sidebar_main">

     <div class="sidebar_main_header">

         <div class="sidebar_logo">

             <a href="<?php echo base_url(); ?>" class="sSidebar_hide sidebar_logo_large" target="_blank">

                 <img class="logo_regular" src="<?php echo base_url(); ?>assets/frontend/images/logo.png" alt="" height="120" width="85" style="margin-<?php echo ($admin_lang['admin_lang'] == 'eng' ? 'left' : 'right'); ?>: 62px; margin-top: -30px;position: absolute;" />

                 <img class="logo_light" src="<?php echo base_url(); ?>assets/frontend/images/logo.png" alt="" height="120" width="85" />

             </a>

             <a href="<?php echo base_url(); ?>" class="sSidebar_show sidebar_logo_small" target="_blank">

                 <img class="logo_regular" src="<?php echo base_url(); ?>assets/admin/assets/img/logo_main_small.png" alt="" height="32" width="32" />

                 <img class="logo_light" src="<?php echo base_url(); ?>assets/admin/assets/img/logo_main_small_light.png" alt="" height="32" width="32" />

             </a>
             <?php
                $id = $this->session->userdata('site_id');
                //echo getsiteTitle($id);
                $admin_lang = $this->session->userdata('admin_lang');
                ?>
         </div>

         <div class="sidebar_actions">
             <select id="lang_switcher" name="lang_switcher">
                 <option value="gb" <?php echo ($admin_lang == 'eng' ? 'selected' : ''); ?>>English</option>
                 <option value="sa" <?php echo ($admin_lang == 'arb' ? 'selected' : ''); ?>>Arabic</option>
             </select>
         </div>
     </div>

     <?php $url = $this->uri->segment(3);
        if ($url == '' && $module == 'dashboard') { ?>
         <!--side_left-->
     <?php } else {
            $admin_lang = check_admin_lang();
            ?>
         <div class="menu_section">

             <ul>
                 <?php
                 $grp=check_user_grp_id();

                 if($grp!='6') {
                     ?>
                     <li class="<?php if ($module == 'dashboard') {
                         echo 'current_section';
                     } ?> tooltip" title="<?php echo $admin_lang['label']['section_dashboard']; ?>">

                         <a href="<?php echo base_url(); ?>ems/dashboard/manage">

                             <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>

                             <span class="menu_title"><?php echo $admin_lang['label']['section_dashboard']; ?></span>

                         </a>

                     </li>
                     <?php
                 }
                        $pages = get_pages();
                        $modules_data = get_modules_list();
                        $mod_counter = 1;
                        $content_id = $this->uri->segment(4);
                        $content_controller = $module . '/manage/' . $content_id;
                        foreach ($modules_data as $val) {
                            $module_id = $val['id'];
                            if (in_array($module_id, $hidden_menu_ids)) {
                                continue;
                            }
                            $controller_name = $val['controller_name'];
                            $module_title =  $admin_lang['admin_lang'] == 'eng' ? $val['sec_title'] : section_arabic($val['sec_title']);

                            ////// fetch parent of current node //////
                            $get_child_row = get_section_row('controller_name', $module);


                            $parent_id = $get_child_row->parent;
                            $get_parent_row = get_section_row('id', $parent_id);
                            $parent_controller = $get_parent_row->controller_name;
                            if ($controller_name == 'configuration') {
                                $material_icon = '&#xE87B;';
                            } else if ($controller_name == 'menu') {
                                $material_icon = '&#xE241;';
                            } else if ($controller_name == 'cmsInUsers') {
                                $material_icon = '&#xE87C;';
                            } else if ($controller_name == 'Inlog') {
                                $material_icon = '&#xE8D2;';
                            } else if ($controller_name == 'newsletteryears') {
                                $material_icon = '&#xE158;';
                            } else if ($controller_name == 'cmsInnGroups') {
                                $material_icon = '&#xE1BD;';
                            } else if ($controller_name == 'social') {
                                $material_icon = '&#xE8C0;';
                            } else if ($controller_name == 'inquries') {
                                $material_icon = '&#xE0D0;';
                            } else if ($controller_name == 'Booking') {
                                $material_icon = '&#xE0CF;';
                            } else if ($controller_name == 'Users') {
                                $material_icon = '&#xE0BA;';
                            } else {
                                $material_icon = '&#xE871;';
                            }
                            //echo $gal_module;
                            //////// checking menu permissions ////////
                            if (get_menu_permission($module_id) || $uid_level == 2) {

                                if ($controller_name != 'list_content' && $controller_name != 'countries' && $controller_name != 'industries' && $controller_name != 'statistics') {
                                    ?>
                             <?php if ($controller_name == 'content') { ?>
                                 <li <?php if ($module == $controller_name or $controller_name == $parent_controller or $controller_name == $content_controller) { ?> class="current_section tooltip" <?php } else { ?> class="tooltip" <?php } ?> title="<?php echo $module_title; ?>" data-cont="<?php echo $controller_name; ?>">
                                     <a href="#">
                                         <span class="menu_icon"><i class="material-icons">&#xE24D;</i></span>
                                         <span class="menu_title"><?php echo $admin_lang['label']['section_content_pages']; ?></span>
                                     </a>
                                     <ul>
                                         <li class="<?php echo ($url == 'addNewPage' ? "act_item" : "") ?> tooltip" title="<?php echo $module_title; ?>" data-cont="<?php echo $controller_name; ?>">
                                             <a href="<?php echo base_url() . 'ems/content/addNewPage'; ?>">
                                                 <?php echo $admin_lang['label']['content_add_page']; ?>
                                             </a>
                                         </li>
                                         <?php

                                                                $i = 0;
                                                                foreach ($pages as $result => $val) {
                                                                    if(check_menu_page_permission($val->id, 'allow')==1){
                                                                    $page_details2 = get_content_data($val->id);
                                                                    $page_details = get_content_data($page_id);
                                                                    if ($page_details->tpl == 'projects' && $page_details2->tpl == 'projects') {
                                                                        if (($page_details->category == 1 || $page_details->category == 2 || $page_details->category == 3) || ($page_details->category == '' && $page_details->tpl == 'projects')) {
                                                                            $class_cat = 'act_item';
                                                                        } else {
                                                                            $class_cat = '';
                                                                        }
                                                                        ?>

                                                 <li class="<?php echo $class_cat; ?> tooltip" id="<?php echo $val->id; ?>">
                                                     <a href="<?php echo base_url() . 'ems/content/edit/id/' . $val->id; ?>">
                                                         <?php echo ($admin_lang['admin_lang'] == 'eng' ? $val->eng_title : $val->arb_title); ?>
                                                     </a>
                                                 </li>

                                             <?php } else {
                                                        if(stripos(current_url(),'list_content/index')!=false || stripos(current_url(),'list_content/add')!=false || stripos(current_url(),'list_content/edit')!=false){
                                                            //var_dump(parentId($access),$val->id,$access_index,$access);die;
                                                            $check_condition = $access_index;
                                                            if(stripos(current_url(),'list_content/edit')!=false || stripos(current_url(),'list_content/add')!=false){
                                                                if($access_index=='id' && parentId($access_index)!=0){
                                                                    $page_parent = parentId(parentId($access_index));
                                                                }elseif($access_index=='id' && parentId($access_index)==0){
                                                                    if(parentId(parentId($access))==0){
                                                                        $page_parent = parentId($access);
                                                                    }elseif(parentId(parentId(parentId($access)))!=0){
                                                                        $page_parent = parentId(parentId(parentId($access)));
                                                                    }else{
                                                                        $page_parent = parentId(parentId($access));
                                                                    }
                                                                }else{
                                                                    if(parentId(parentId($access_index)) != 0){
                                                                        $page_parent = parentId(parentId($access_index));
                                                                    }else{
                                                                        $page_parent = parentId($access_index);
                                                                    }
                                                                }
                                                            }else{
                                                                if(parentId(parentId($access_index)) != 0){
                                                                    $page_parent = parentId(parentId($access_index));
                                                                }else{
                                                                    $page_parent = parentId($access_index);
                                                                }
                                                            }
                                                        }else{
                                                            $check_condition = $access;
                                                            $page_parent = parentId($access);
                                                        }
                                                        //echo $this->db->last_query();
                                                          //var_dump($page_parent,$access_index);die;
                                                                        ?>
                                                 <li class="<?php echo (($page_parent == $val->id /*|| $access == $val->id*/ || $check_condition == $val->id) && ($module == 'content' || $module == 'list_content') ? "act_item" : "") ?> tooltip" id="<?php echo $val->id; ?>">
                                                     <a href="<?php echo base_url() . 'ems/content/edit/id/' . $val->id; ?>">
                                                         <?php echo ($admin_lang['admin_lang'] == 'eng' ? $val->eng_title : $val->arb_title); ?>
                                                     </a>
                                                 </li>

                                         <?php  }
                                                                    $i++;
                                                                } } ?>
                                     </ul>
                                 </li>
                             <?php } else if ($controller_name == 'statistics') { ?>
                                 <li <?php if ($module == $controller_name or $controller_name == $parent_controller or $controller_name == $content_controller) { ?> class="current_section tooltip" <?php } ?> title="<?php echo $module_title; ?>" data-cont="<?php echo $controller_name; ?>">
                                     <a href="#">
                                         <span class="menu_icon"><i class="material-icons">&#xE85C;</i></span>
                                         <span class="menu_title"><?php echo $admin_lang['label']['statistics']; ?></span>
                                     </a>
                                     <ul>
                                         <li class="<?php echo ($url == 'traffic' ? "act_item" : "") ?> tooltip" title="<?php echo $module_title; ?>" data-cont="<?php echo $controller_name; ?>">
                                             <a href="<?php echo base_url() . 'ems/statistics/traffic'; ?>">
                                                 <?php echo $admin_lang['label']['dashboard_traffic']; ?>
                                             </a>
                                         </li>
                                         <li class="<?php echo ($url == 'siteusage' ? "act_item" : "") ?>" title="<?php echo $module_title; ?>" data-cont="<?php echo $controller_name; ?>">
                                             <a href="<?php echo base_url() . 'ems/statistics/siteusage'; ?>">
                                                 <?php echo $admin_lang['label']['dashboard_site']; ?>
                                             </a>
                                         </li>
                                         <li class="<?php echo ($url == 'mapoverview' ? "act_item" : "") ?>" title="<?php echo $module_title; ?>" data-cont="<?php echo $controller_name; ?>">
                                             <a href="<?php echo base_url() . 'ems/statistics/mapoverview'; ?>">
                                                 <?php echo $admin_lang['label']['dashboard_map_overview']; ?>
                                             </a>
                                         </li>
                                         <li class="<?php echo ($url == 'trafficoverview' ? "act_item" : "") ?>" title="<?php echo $module_title; ?>" data-cont="<?php echo $controller_name; ?>">
                                             <a href="<?php echo base_url() . 'ems/statistics/trafficoverview'; ?>">
                                                 <?php echo $admin_lang['label']['dashboard_sources']; ?>
                                             </a>
                                         </li>
                                     </ul>
                                 </li>
                             <?php } else {  ?>
                                 <li <?php if ($module == $controller_name or $controller_name == $parent_controller or $controller_name == $content_controller) { ?> class="current_section tooltip" <?php } else { ?> class="tooltip" <?php } ?> title="<?php echo $module_title; ?>" data-cont="<?php echo $controller_name; ?>">
                                     <a href="<?php echo base_url(); ?>ems/<?php echo ($controller_name == 'contacts' ? $controller_name.'?show_all=clear' : ($controller_name == 'request_quotation' ? $controller_name.'?show_all=clear' : ($controller_name == 'careers' ? $controller_name.'?show_all=clear' : $controller_name))); ?>">
                                         <span class="menu_icon">
                                             <i class="material-icons"><?php echo $material_icon; ?></i>
                                         </span>
                                         <span class="menu_title"><?php echo $module_title; ?></span>
                                     </a>
                                 </li>
                 <?php }
                                }
                            }
                            $mod_counter++;
                        } // foreach ends
                        ?>

             </ul>

         </div>
     <?php } ?>

 </aside><!-- main sidebar end -->
 {content}