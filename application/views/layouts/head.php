<!DOCTYPE html>
<html lang="en" style="direction: <?php echo $lang == 'arb' ? 'rtl' : 'ltr'; ?>;">
<head>
    <title>Al Murjan Group</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--<link rel="icon" type="image/png" href="<?php /*echo base_url('assets/frontend/images/favicon.png'); */?>" />-->
    <link rel="shortcut icon" href="<?php echo base_url('assets/frontend/images/favicon.ico'); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('assets/frontend/images/favicon.ico'); ?>" type="image/x-icon">
    <?php if ($lang == 'arb') { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/bootstrap-5.0.2/css/bootstrap.rtl.min.css'); ?>">
    <?php } else { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/bootstrap-5.0.2/css/bootstrap.min.css'); ?>">
    <?php } ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/owl-carousel/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend/icofont/icofont.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/fontawesome/css/all.css'); ?>">

    <?php if ($tpl_name == 'history') { ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.4.0/animate.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/jquery-timeline/timelify.css'); ?>">
    <?php } ?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/css/styles.css?v='.rand()); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/css/new-style.css?v='.rand()); ?>">
    <?php if ($lang == 'arb') { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/css/styles.rtl.css?v='.rand()); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/css/new-styles.rtl.css?v='.rand()); ?>">
    <?php } ?>
    <script src="<?php echo base_url('assets/frontend/jquery/jquery-3.6.0.min.js'); ?>"></script>
    <script src='https://www.google.com/recaptcha/api.js<?php echo($lang == 'arb' ? '?hl=ar' : ''); ?>'></script>
    <style>
        body.eng .searchBox{
            display: none;
            position: absolute;
        }
        body.eng.searchBox .form-group{
            position: relative;
        }
        body.eng.searchBox  .form-control{
            padding: 12px 32px 12px 20px !important;
        }

        body.arb .searchBox{
            display: none;
            position: absolute;
        }
        body.arb .searchBox .form-group{
            position: relative;
        }
        body.arb .searchBox  .form-control{
            padding: 12px 20px 12px 32px !important;
        }

        .SearchBoxes{
            border-bottom: 1px solid #c4a34e;
            padding: 20px 0px;
        }
    </style>
</head>
<body class="<?php echo $tpl_name; ?> <?php echo $lang; ?>">