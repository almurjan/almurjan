<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- If you delete this tag, the sky will fall on your head -->
	<meta name="viewport" content="width=device-width" />

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Almurjan Group</title>
<!--<base href="http://ioud.ed.sa/html/email_temp/" target="_blank">-->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
	<style type="text/css">
		* {
			margin:0;
			padding:0;
		}
		* { font-family: "Roboto Condensed", sans-serif;}

		img {
			max-width: 100%;
		}
		body {
			-webkit-font-smoothing:antialiased;
			-webkit-text-size-adjust:none;
			width: 100%!important;
			height: 100%;
		}


		/* -------------------------------------------
                PHONE
                For clients that support media queries.
                Nothing fancy.
        -------------------------------------------- */
		@media only screen and (max-width: 600px) {

			a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

			div[class="column"] { width: auto!important; float:none!important;}

			table.social div[class="column"] {
				width:auto!important;
			}

		}
	</style>
</head>

<body link="blue" bgcolor="#FFFFFF" style="background-color: #fff; font-family: Roboto Condensed, sans-serif;">

<table class="head-wrap" style="background-color: #fff;
								border: 0 none;
								margin: 0 auto;
								max-width: 660px;
                                border-bottom:5px solid #337ab7;
								width: 660px;" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; " border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td colspan="2" style="padding: 8px 0;">
								<a href="javascript:void(0);" style="float:left; height:100%;">
									<img src="<?php echo base_url();?>assets/frontend/images/logo.png" alt="logo" height="80" width="80" />
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Header END -->


<table class="Textarea" style="background-color: #fff;
								border: 0 none;
								margin: 0 auto;
								max-width: 660px;
								width: 660px" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; "  border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>

							<td align="center" style="
													color: #21215f;
													font-size: 25px;
													font-weight: 300;
													min-height: 82px;
													padding: 24px 0;
													width: 50%;
													">
								<?php
                                if(isset($title)){
                                echo $title;
                                } ?>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>

</table><!-- Order Status END -->


<table class="whiteBox" style="background-color: #fff;
								border: 0 none;
								margin: 0 auto;
								max-width: 660px;
								padding-bottom:34px;
								width: 660px" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 11px;"></td>
			<td style="width: 11px;"></td>
		</tr>
		<tr>
			<td style="width: 11px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<table style="
						background-color: #fff;
						background-image: url(images/bgShadow.png);
						background-position: left top;
						background-repeat: repeat-y;
						background-size: 100% 1px;
						padding: 30px 20px 13px;
						width: 100%;
							  " class="divWBox" border="0" cellpadding="0" cellspacing="0">
					<tbody>

                        <tr>
							<td colspan="2" align="left">

								<?php echo $message; ?>
								<br />

							</td>
						</tr>


						<tr>
							<td colspan="2">
								<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
									<thead>
										<tr>
											<th colspan="4" align="right" style="
												color: #9f9277;
												font-size: 16px;
												font-weight: 300;
												padding: 0 10px 15px;
											">&nbsp;</th>
										</tr>
									</thead>
									<tbody>

                                   <?php
								   //echo $info['product_service']; exit;
								   if($info['job_id'] != ''){									   
										$info['job_id'] = ucwords(pageSubTitle($info['job_id'], $info['lang']));
								   }
								   if($info['dob'] != ''){									   
										$info['dob'] = date('d, M Y', strtotime($info['dob']));
								   }								   
								   unset($info['lang']);
								   unset($info['terms']);
								   	
								   if(count($info) > 0 ) {
                                       foreach ($info as $name => $value) { 
										if($name == 'f_name'){
											$name = 'first_name';
										}
										else if($name == 'm_name'){
											$name = 'middle_name';
										}
										else if($name == 'l_name'){
											$name = 'last_name';
										}
										else if($name == 'c_residence'){
											$name = 'country_residence';
										}
										else if($name == 'ct_residence'){
											$name = 'city_residence';
										}
										else if($name == 'eng_proof'){
											$name = 'english_proficiency';
										}
										else if($name == 'saudi_comm'){
											$name = 'saudi_verified';
										}
										
									   $name = str_replace('_id','',$name);											
									   $name = str_replace('_',' ',$name);	
									   $name = str_replace('contactpurpose','Contact Purpose',$name);
										if(ucwords($name) == 'Comments'){											
											$name = 'Message';
										}
										if(ucwords($name) == 'Product Service'){											
											$name = 'Requested Service';
										}
									   ?>
                                           <tr>
                                               <td style="border-top: 1px solid #d0d0d0; padding: 10px; width: 40px;">&nbsp;
                                                   <?php  echo ucwords($name);?>
                                               </td>
                                               <td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px;
												width:190px;
											">
												<?php 												
												if($name == 'C V' || $name == 'c v' || $name == 'C v' || $name == 'C_V' || $name == 'c_v'){													
												$url = base_url().'uploads/cvs/'.$value;													
													echo '<a href="'.$url.'" download><img height="70" width="65" src="'.base_url().'assets/images/download.png'.'"></img></a>';																									}												
												else{													
													echo ucwords($value);												
												}												
												?>
											</td>
                                               
                                           </tr>
                                       <?php }
                                   }?>


									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 11px;"></td>
		</tr>
		<tr>
			<td style="width: 11px;"></td>

			<td style="width: 11px;"></td>
		</tr>
	</tbody>
</table><!-- Main White Box END -->


<table class="footer-wrap" style="background-color: #337ab7;
								border: 0 none;
								margin: 0 auto;
								max-width: 660px;
								width: 660px" bgcolor="#343434" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; " border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<?php if($hideSocialLinks != 1){ ?>
						<tr>
							<td style="padding: 10px 0 0; width: 50%;">
							<ul style="padding:0; text-align: center">
                                 <?php
									$social_links = social_links();		
									
										if($social_links->soc_fb != ''){
											$http = '';	
											if (strpos($social_links->soc_fb, 'http') !== false) {
												$http = '';
											}else{
												$http = 'http://';
											}		
									?>
										 <li style="display:inline-block;"><a href="<?php echo $http.$social_links->soc_fb; ?>" target="_blank"><img style="width: 20px;" src="<?php echo base_url('assets/frontend/images/fbs.png'); ?>"></a></li>
									<?php } ?>
									
									<?php 
										if($social_links->soc_tw != ''){ 
											$http = '';	
											if (strpos($social_links->soc_tw, 'http') !== false) {
												$http = '';
											}else{
												$http = 'http://';
											}
									?>
									 <li style="display:inline-block;"><a href="<?php echo $http.$social_links->soc_tw; ?>" target="_blank"><img style="width: 20px;" src="<?php echo base_url('assets/frontend/images/tws.png'); ?>"></a></li>
									<?php } ?>
									
									<?php if($social_links->soc_insta != ''){ 
											$http = '';	
											if (strpos($social_links->soc_insta, 'http') !== false) {
												$http = '';
											}else{
												$http = 'http://';
											}
									?>
									<li style="display:inline-block;"><a href="<?php echo $http.$social_links->soc_insta; ?>" target="_blank"><img style="width: 20px;" src="<?php echo base_url('assets/frontend/images/ins.png'); ?>"></a></li>
									<?php } ?>
									
								</ul>	
							</td>
						</tr>
						
						<?php } ?>
						<tr>	
							<td align="center" style="
													 color: #fff;
													 font-size: 13px;
													 padding:0 0 10px;
													 width: 50%;">
                               Copyrights protected, all rights reserved,  <?php echo date("Y") ?>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Footer END -->

</body>
</html>