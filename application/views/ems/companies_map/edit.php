<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h2 id="product_edit_name"><?php echo 'Companies'; ?></h2>
        <span class="uk-text-muted uk-text-upper uk-text-small"></span>
        <div id="success_msg" style="text-align:center;color:#7cb342;">
            <?php if(validation_errors()){ echo _erMsg(validation_errors());} if($this->session->flashdata('message')) {echo $this->session->flashdata('message');} ?>
        </div>
    </div>
    <div id="page_content_inner">
        <?php echo form_open_multipart('ems/companies_map/saveCompanies',array('method'=>'post','id'=>'socialLinks')); ?>
        <input type="hidden" value="<?php echo $res->id; ?>" id="id" name="id">
        <div class="md-card">
            <div class="md-card-content"><br>
                <div class="uk-grid" data-uk-grid-margin>
                    <?php
                    create_companies_textarea('Description','companyone','eng','',true,$res->id);
                    create_companies_textarea('Description','companyone','arb','',true,$res->id);
                    create_companies_textarea('Description','companytwo','eng','',true,$res->id);
                    create_companies_textarea('Description','companytwo','arb','',true,$res->id);
                    create_companies_textarea('Description','companythree','eng','',true,$res->id);
                    create_companies_textarea('Description','companythree','arb','',true,$res->id);
                    create_companies_textarea('Description','companyfour','eng','',true,$res->id);
                    create_companies_textarea('Description','companyfour','arb','',true,$res->id);
                    create_companies_textarea('Description','companyfive','eng','',true,$res->id);
                    create_companies_textarea('Description','companyfive','arb','',true,$res->id);
                    create_companies_textarea('Description','companysix','eng','',true,$res->id);
                    create_companies_textarea('Description','companysix','arb','',true,$res->id);
                    create_companies_textarea('Description','companyseven','eng','',true,$res->id);
                    create_companies_textarea('Description','companyseven','arb','',true,$res->id);
                    create_companies_textarea('Description','companyeight','eng','',true,$res->id);
                    create_companies_textarea('Description','companyeight','arb','',true,$res->id);
                    ?>
                    <div class="uk-form-row"></div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_update_content']; ?>" href="javascript:void(0);" id="saveSocial">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function () {

        $('#saveSocial').click(function(){
            $(".page_loader").show();
            $('#socialLinks').submit();
        });
        $("#socialLinks").validate();
        setInterval(function(){$('#show_error').html(''); }, 1500);
    });

</script>
<!--End New HTML-->