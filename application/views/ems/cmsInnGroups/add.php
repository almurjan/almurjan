<script type="text/javascript">
    $(document).ready(function () {
        //$("#select").trigger('click');
        $("div.cmsUserContentRow #select").click( function( e ) {
            var i=$(this).val();
            if ($(this).attr('checked')){
                $("input[name=create_" + i + "]").prop('checked', true);
                $("input[name=read_" + i + "]").prop('checked', true);
                $("input[name=upd_" + i + "]").prop('checked', true);
                $("input[name=del_" + i + "]").prop('checked', true);
                $("input[name=pub_" + i + "]").prop('checked', true);
            }else{
                $("input[name=create_" + i + "]").prop('checked', false);
                $("input[name=read_" + i + "]").prop('checked', false);
                $("input[name=upd_" + i + "]").prop('checked', false);
                $("input[name=del_" + i + "]").prop('checked', false);
                $("input[name=pub_" + i + "]").prop('checked', false);
            }
            //alert($(this).val());
        });
        //$("div.cmsUserContentRow ")
    });
</script>
<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
    <?php echo form_open('ems/cmsInnGroups/insert_grp',array('method'=>'post','id'=>'site_form'));?>
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['cms_groups']; ?> - <?php echo $admin_lang['label']['add']; ?></h1>
    </div>
    <div id="page_content_inner">
        <a href="<?php echo base_url().'ems/cmsInnGroups';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"> <?php echo $admin_lang['label']['back']; ?></a>
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['cms_groups_title']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required" id="eng_web_title" name="eng_web_title" value="" />
                                </div>
                            </div>

                            <div class="uk-width-medium-1-1">
                                <label><?php echo $admin_lang['label']['cms_users_role']; ?></label>
                                <div class="uk-overflow-container cmsUserContentRow">
                                    <table class="uk-table">
                                        <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" name="checkbox12" id="select_all1" />
                                                <label for="select_all1"></label>
                                                <?php echo $admin_lang['label']['cms_groups_edit_web']; ?>
                                            </th>
                                            <th>
                                                <input type="checkbox" name="checkbox12" id="select_all2" />
                                                <label for="select_all2"></label>
                                                <?php echo $admin_lang['label']['cms_users_create']; ?>
                                            </th>
                                            <th>
                                                <input type="checkbox" name="checkbox12" id="select_all4" />
                                                <label for="select_all4"></label>
                                                <?php echo $admin_lang['label']['cms_users_update']; ?>
                                            </th>
                                            <th>
                                                <input type="checkbox" name="checkbox12" id="select_all5" />
                                                <label for="select_all5"></label>
                                                <?php echo $admin_lang['label']['cms_users_delete']; ?>
                                            </th>
                                            <th>
                                                <input type="checkbox" name="checkbox12" id="select_all6" />
                                                <label for="select_all6"></label>
                                                <?php echo $admin_lang['label']['cms_users_publish']; ?>
                                            </th>
                                            <th>
                                                <input type="checkbox" name="checkbox12" id="select_all3" />
                                                <label for="select_all3"></label>
                                                <?php echo $admin_lang['label']['cms_groups_edit_menu']; ?>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $count=1;
                                        foreach($siteList as $result=>$val){
                                        ?>
                                        <tr>
                                            <td id="select_<?php echo $val['id']?>">
                                                <input style="visibility:visible" value="<?php echo $val['id']?>" type="checkbox"  name="select[<?php echo $count?>]" id="select" />
                                                <label for="select"></label><?php echo $val['sec_title']?>
                                            </td>
                                            <td>
                                                <input onclick="hidemaster(1,'<?php echo $val['id']?>');" style="visibility:visible" type="checkbox" name="create_<?php echo $val['id']?>" id="select2" value="1"/>
                                                <label for="select2"></label>
                                            </td>
                                            <td>
                                                <input style="visibility:visible" type="checkbox"  onclick="hidemaster(1,'<?php echo $val['id']?>');" name="upd_<?php echo $val['id']?>" id="select4" value="1"/>
                                                <label for="select4"></label>
                                            </td>
                                            <td>
                                                <input style="visibility:visible" type="checkbox"  onclick="hidemaster(1,'<?php echo $val['id']?>');"  name="del_<?php echo $val['id']?>" id="select5" value="1"/>
                                                <label for="select5"></label>
                                            </td>
                                            <td>
                                                <input style="visibility:visible" type="checkbox" onclick="hidemaster(1,'<?php echo $val['id']?>');" name="pub_<?php echo $val['id']?>" id="select6" value="1"/>
                                                <label for="select6"></label>
                                            </td>
                                            <td>
                                                <input  style="visibility:visible" type="checkbox" name="read_<?php echo $val['id']?>" onclick="hidemaster(1,'<?php echo $val['id']?>');" id="select3" value="1"/>

                                            </td>
                                        </tr>
                                        <?php $count++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php /*echo form_close(); */?><!--
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary md-btn-primary autoTooltip" title="<?php /*echo $admin_lang['label']['tooltip_save_content']; */?>" href="javascript:void(0);" id="submit">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>-->
    </div>

    <div id="page_content_inner">
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-medium-1-2 uk-row-first">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo 'Pages Name'; ?></label>
                                <div class="uk-form-row">
                                    <!--<input type="text" class="md-input required" id="title" name="grp_id" value="<?php /*echo $result->title;*/?>" disabled="disabled"/>-->
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <div class="uk-overflow-container cmsPagesContentRow">
                                    <table class="uk-table">
                                        <thead>
                                        <tr>
                                            <th><!--<input type="checkbox" name="selectallpages" id="selectallpages" />
                                                    <label for="selectallpages"></label>-->Allow</th>
                                            <th>Page</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $k = 0;
                                        $pages = get_pages();
                                        foreach($pages as $page){
                                            $checkTitle = ($admin_lang['admin_lang'] == 'eng'?$page->eng_title:$page->arb_title);
                                            if($checkTitle != '' && $page->id != 993){

                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="uk-width-large-1-4 uk-width-medium-1-2">
                                                            <div class="uk-input-group">
                                                                <span class="uk-input-group-addon"><input type="checkbox" name="page_id[]" value="<?php echo $page->id;?>" data-md-icheck/></span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?php echo ($admin_lang['admin_lang'] == 'eng'?$page->eng_title:$page->arb_title); ?>
                                                    </td>
                                                </tr>
                                                <!--<input type="hidden" name="parant_id[]" value="<?php /*echo $menu_pages[$i]->parant_id;  */?>" />
                                            <input type="hidden" name="position[]" value="<?php /*echo $menu_pages[$i]->position;  */?>" />-->
                                                <?php $k++; } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary md-btn-primary" href="javascript:void(0);" id="submit">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
    </div>
</div>
<script language="javascript">
    $(document).ready(function () {
        // validate the comment form when it is submitted
        $('#submit').click(function(e) {
            $(".page_loader").show();
            e.preventDefault();
            $("#site_form").submit();
            $("#siteup_form").submit();
        });
        $("#site_form").validate();
        $("#siteup_form").validate();
    });
</script>
<!--End New HTML-->
