<div class="content_row">
    <div class="light_shadow_bar"></div>
    <div class="black_bar_main">
        <div class="inner_left_wrap">
            <div class="inner_left_heading"><?php echo getsiteTitle(1); ?></div>
            <div class="inner_left_headingsmalll">Website</div>
        </div>
        <div class="inner_mid_heading">Add Property Details </div>
        <div class="get_help"><a  id="pop">Get Help</a><span style="padding-top:20px; padding-left:8px; float:right"><a  id="pop2"><img src="assets/images/hepl_icon.png" width="20" height="20" /></a></span></div>
        <div class="side_bar_wrap" style="border-left:1px solid #fff;"><span><img src="assets/images/sidebar_icon.png" width="15" height="15" /></span>Sidebar</div>
    </div>
    <div class="black_shadow"></div>
</div>  

<!--gray_panel-->
<div class="inner_gray_panel" >
    <!--side_right-->
    <div class="side_right">
        <div class="seprator_horizontal"></div>
        <!-- status bar - start-->
        <?php echo get_status_bar(); ?>
        <!-- status bar - end-->
    </div><!--side_right-->

    <!--middle_content_areainner-->
    <div class="middle_content_areainner" style="padding-top:0px; min-height:1100px;">
        <!--inner_menu_bar-->
        <div class="inner_menu_bar" style="width:22%;">
            <ul>
                <li style="z-index:99; width:53%;"><a id="saveProperty" name="submit" >Save</a></li>
                <li style="z-index:100; left:45%; width:53%;"><a onclick="javascript:history.back();">Cancel</a></li>
            </ul>
        </div><!--inner_menu_bar-->
        <div class="admin_manage_panel" style="padding-top:90px;">
            <div align="center"><?php
                if ($this->session->flashdata('message')) {
                    echo $this->session->flashdata('message');
                }
                ?> </div>
            <div class="table_content_row">
                <?php echo form_open_multipart('ems/Properties/saveProperty', array('method' => 'post', 'id' => 'addProp'))
                ; ?>
                <input type="hidden" value="" id="pub_val" name="pub_val">
                <input type="hidden" value="<?php echo $page_id;?>" id="page_id" name="page_id">
                <div class="table_content_row">
                    <table width="100%" border="0" class="edit_table">
                        <tbody>
                            <tr class="blue_row">
                                <td width="2%">&nbsp;</td>
                                <td width="98%">Property Details (English)</td>
                            </tr>
                            <tr class="gray_row">
                                <td>&nbsp;</td>
            <td>
                <!--edit_data_table-->

                <div class="edit_data_table">

                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Property Title</div>
                        <div class="edit_data_table_col2">
                            <input name="property_title" id="property_title" type="text" class="input_field required " />
                        </div>
                    </div>
                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Property Type</div>
                        <div class="edit_data_table_col2">
                            <input name="property_type" id="property_type" type="text" class="input_field required " />
                        </div>
                    </div>
                    <!--<div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> City</div>
                        <div class="edit_data_table_col2">
                            <input name="city" id="city" type="text" class="input_field required " />
                        </div>
                    </div>-->					<div class="edit_data_table_row">                        <div class="edit_data_table_col1"> City</div>                        <div class="edit_data_table_col2">						<select name="city" id="city" class="required ">						<option disabled selected>-- Select City --</option>					<?php 					$saudi_cities = getCitiesForCountryEms('12');					foreach ($saudi_cities as $saudi_city){					?>					<option value="<?php echo $saudi_city->id; ?>"><?php echo $saudi_city->eng_title; ?></option>					<?php } ?>					</select>					</div>					</div>									
                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Area</div>
                        <div class="edit_data_table_col2">
                            <input name="area" id="property_area" type="text" class="input_field " />
                        </div>
                    </div>
                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Access</div>
                        <div class="edit_data_table_col2">
                            <input name="access" id="access" type="text" class="input_field " />
                            <span><strong>For Multiple:</strong> item1,item2,item3, etc........</span>
                        </div>
                    </div>
                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Space</div>
                        <div class="edit_data_table_col2">
                            <input name="space" id="space" type="text" class="input_field " />
                            <span><strong>For Multiple:</strong> item1,item2,item3, etc........</span>
                        </div>
                    </div>
                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Facilities</div>
                        <div class="edit_data_table_col2">
                            <input name="facilities" id="facilities" type="text" class="input_field " />
                            <span><strong>For Multiple:</strong> item1,item2,item3, etc........</span>
                        </div>
                    </div>

                </div><!--edit_data_table-->

            </td>
                        </tbody></table>
                </div>

<div class="table_content_row">
    <table width="100%" border="0" class="edit_table">
        <tbody>
        <tr class="blue_row">
            <td width="2%">&nbsp;</td>
            <td width="98%">Property Details (Arabic)</td>
        </tr>
        <tr class="gray_row">
            <td>&nbsp;</td>
            <td>
                <!--edit_data_table-->

                <div class="edit_data_table">

                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Property Title</div>
                        <div class="edit_data_table_col2">
                            <input name="property_title_arb" id="property_title" type="text" class="input_field required " />
                        </div>
                    </div>
                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Property Type</div>
                        <div class="edit_data_table_col2">
                            <input name="property_type_arb" id="property_type" type="text" class="input_field required " />
                        </div>
                    </div>
                    <!--<div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> City</div>
                        <div class="edit_data_table_col2">
                            <input name="city_arb" id="city" type="text" class="input_field required " />
                        </div>
                    </div>-->
                    <!--<div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Area</div>
                        <div class="edit_data_table_col2">
                            <input name="area_arb" id="property_area" type="text" class="input_field " />
                        </div>
                    </div>-->
                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Access</div>
                        <div class="edit_data_table_col2">
                            <input name="access_arb" id="access" type="text" class="input_field " />
                            <span><strong>For Multiple:</strong> item1,item2,item3, etc........</span>
                        </div>
                    </div>
                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Space</div>
                        <div class="edit_data_table_col2">
                            <input name="space_arb" id="space" type="text" class="input_field " />
                            <span><strong>For Multiple:</strong> item1,item2,item3, etc........</span>
                        </div>
                    </div>
                    <div class="edit_data_table_row">
                        <div class="edit_data_table_col1"> Facilities</div>
                        <div class="edit_data_table_col2">
                            <input name="facilities_arb" id="facilities" type="text" class="input_field " />
                            <span><strong>For Multiple:</strong> item1,item2,item3, etc........</span>
                        </div>
                    </div>



                </div><!--edit_data_table-->

            </td>


        </tbody></table>
</div>

                <?php echo form_close(); ?> 

            </div>
        </div>

    </div><!--middle_content_areainner-->
<script language="javascript">
    $().ready(function () {
        $('#saveProperty').click(function () {
          $("#addProp").attr("action", "<?php echo base_url();?>ems/Properties/saveProperty/");
          $("#addProp").attr("target", "");
          $('#addProp').submit();
        });
        $("#addProp").validate();
    });
</script>