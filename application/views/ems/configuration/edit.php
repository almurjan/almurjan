<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
    <h2 id="product_edit_name"><?php echo $admin_lang['label']['section_configuration']; ?></h2>
    <span class="uk-text-muted uk-text-upper uk-text-small"></span>
    <div id="success_msg" style="text-align:center;color:#7cb342;">
        <?php if(validation_errors()){ echo _erMsg(validation_errors());} if($this->session->flashdata('message')) {echo $this->session->flashdata('message');} ?>
    </div>
</div>
<div id="page_content_inner">
    <?php echo form_open_multipart('ems/configuration/save',array('method'=>'post','id'=>'socialLinks')); ?>
    <input type="hidden" value="<?php echo $res->pub_status; ?>" id="pub_val" name="pub_val">
    <div class="md-card">
        <div class="md-card-content"><h3 class="heading_a autoTooltip" title="<?php echo $admin_lang['label']['tooltip_general_settings']; ?>"><b><?php echo $admin_lang['label']['config_general_settings']; ?></b></h3><br>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['config_project_name']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="project_name" name="project_name" value="<?php echo $res->project_name;?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['config_project_name']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="project_name_arb" name="project_name_arb" value="<?php echo $res->project_name_arb;?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php echo 'Newsletter'; ?></label>
                    <?php
                    if($res->newsletter_section==1) {
                        $checked1='selected';
                        $checked2='';
                    }else{
                        $checked1='';
                        $checked2='selected';
                    }
                    ?>
                    <div class="uk-form-row">
                        <select class="md-input" name="newsletter_section">
                            <option>select</option>
                            <option <?php echo $checked1; ?> value="1">ON</option>
                            <option <?php echo $checked2; ?> value="0">OFF</option>
                        </select>
                    </div>
                </div>
				<?php
				//echo create_datepicker($admin_lang['label']['date'],'date','eng','','','');
				?>
            </div>
        </div>
    </div>
    <div class="md-card">
        <div class="md-card-content"><h3 class="heading_a autoTooltip" title="<?php echo $admin_lang['label']['tooltip_email_settings']; ?>"><b><?php echo $admin_lang['label']['config_email_settings']; ?></b></h3><br>
            <div class="uk-grid" data-uk-grid-margin>
                <!--<div class="uk-width-medium-1-2"><label><?php //echo $admin_lang['label']['config_email_sent_from']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input" id="from_email" name="from_email" value="<?php //echo $res->from_email;?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php //echo $admin_lang['label']['config_email_reach_us']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input" id="reachus_email" name="reachus_email" value="<?php //echo $res->reachus_email;?>">
                    </div>
                </div>-->
				<!--
              
                <div class="uk-width-medium-1-2"><label><?php //echo $admin_lang['label']['phone']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input" id="phone" name="phone" value="<?php //echo $res->phone;?>">
                    </div>
                </div>-->
				
				<!--<div class="uk-width-medium-1-2"><label><?php /*echo $admin_lang['label']['news_room_admin_email']; */?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="sendinquiry_email" name="sendinquiry_email" value="<?php /*echo $res->sendinquiry_email;*/?>">
                    </div>
                </div>-->
				<div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['email_contact']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input" id="contact_email" name="contact_email" value="<?php echo $res->contact_email;?>">
                    </div>
                </div>
				
				<div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['career_email']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input" id="career_email" name="career_email" value="<?php echo $res->career_email;?>">
                    </div>
                </div>

				<!--<div class="uk-width-medium-1-2"><label><?php /*echo $admin_lang['label']['request_email']; */?></label>
                    <div class="uk-form-row">
                        <input class="md-input" id="request_email" name="request_email" value="<?php /*echo $res->request_email;*/?>">
                    </div>
                </div>-->

            </div>
        </div>
    </div>
    <div class="md-card">
        <div class="md-card-content"><h3 class="heading_a autoTooltip" title="<?php echo $admin_lang['label']['tooltip_google_analytics']; ?>"><b><?php echo $admin_lang['label']['config_google_analytics']; ?></b></h3><br>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['config_user_name']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input" id="ga_user" name="ga_user" value="<?php echo $res->ga_user;?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['config_user_password']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input" id="ga_password" name="ga_password" type="text" value="<?php //echo $res->ga_password;?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['config_tracking_id']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="ga_tracking_id" name="ga_tracking_id" value="<?php echo $res->ga_tracking_id;?>">
                        <small><?php echo $admin_lang['label']['config_tracking_id_help_text']; ?></small>
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['config_view_id']; ?></label>
                    <div class="uk-form-row">
                        <input class="md-input" id="ga_view_id" name="ga_view_id" value="<?php echo $res->ga_view_id;?>">
                        <small><?php echo $admin_lang['label']['config_view_id_help_text']; ?></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="md-card">
        <div class="md-card-content"><h3 class="heading_a autoTooltip" title="SMTP Settings"><b><?php echo($admin_lang['admin_lang'] == 'eng' ? 'SMTP Settings' : 'SMTP إعدادات'); ?></b></h3><br>
            <div class="uk-grid" data-uk-grid-margin>

                <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <label class="autoTooltip"><?php echo($admin_lang['admin_lang'] == 'eng' ? 'SMTP Encryption' : 'تشفير'); ?></label>
                        <select class="" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}"
                                title="SMTP Encryption" name="smtp_crypto" id="smtp_crypto">
                            <option value="ssl" selected>SSL</option>
                            <option value="tls">TLS</option>
                        </select>
                    </div>
                </div>
                <div class="uk-width-medium-1-2"></div>
                <div class="uk-width-medium-1-2"><label><?php echo($admin_lang['admin_lang'] == 'eng' ? 'SMTP Host' : 'ضيافة'); ?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="smtp_host" name="smtp_host" value="<?php echo $res->smtp_host;?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php echo($admin_lang['admin_lang'] == 'eng' ? 'SMTP Email' : 'بريد الالكتروني'); ?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="smtp_email" name="smtp_email" value="<?php echo $res->smtp_email;?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php echo($admin_lang['admin_lang'] == 'eng' ? 'SMTP Password' : 'الرقم السري'); ?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="smtp_password" name="smtp_password" value="<?php echo $res->smtp_password;?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php echo($admin_lang['admin_lang'] == 'eng' ? 'SMTP Port' : 'منفذ'); ?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="smtp_port" name="smtp_port" value="<?php echo $res->smtp_port;?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="md-card">
        <div class="md-card-content"><h3 class="heading_a autoTooltip" title="SMTP Settings"><b><?php echo($admin_lang['admin_lang'] == 'eng' ? 'Mail Chimp Settings' : 'Mail chimp إعدادات'); ?></b></h3><br>
            <div class="uk-grid" data-uk-grid-margin>

                <div class="uk-width-medium-1-2"><label><?php echo($admin_lang['admin_lang'] == 'eng' ? 'Mail Chimp Api key' : 'مفتاح واجهة برمجة تطبيقات'); ?> </label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="mailChimp_api_key" name="mailChimp_api_key" value="<?php echo $res->mailChimp_api_key;?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php echo($admin_lang['admin_lang'] == 'eng' ? 'Mail Chimp List Id' : ' رقم المعرف'); ?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="mailChimp_list_id" name="mailChimp_list_id" value="<?php echo $res->mailChimp_list_id;?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- News Room -->
	<!--<div class="md-card">
        <div class="md-card-content"><h3 class="heading_a autoTooltip" title="News Room Credentials"><b><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'News Room Credentials' : 'هوية غرفة الاخبار'); */?> </b></h3><br>
            <div class="uk-grid" data-uk-grid-margin>

                <div class="uk-width-medium-1-2"><label><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Username' : 'اسم المستخدم'); */?> </label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="username_news" name="username_news" value="<?php /*echo $res->username_news;*/?>">
                    </div>
                </div>
                <div class="uk-width-medium-1-2"><label><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Password' : ' الرقم السري'); */?></label>
                    <div class="uk-form-row">
                        <input class="md-input required" id="password_news" name="password_news" value="<?php /*echo $res->password_news;*/?>">
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <?php echo form_close(); ?>
    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_update_content']; ?>" href="javascript:void(0);" id="saveSocial">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
</div>
</div>

<script language="javascript">
    $(document).ready(function () {

        $('#saveSocial').click(function(){
            $(".page_loader").show();
            $('#socialLinks').submit();
        });
        $("#socialLinks").validate();
        setInterval(function(){$('#show_error').html(''); }, 1500);
    });

</script>
<!--End New HTML-->