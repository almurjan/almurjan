<?php $admin_lang = check_admin_lang(); ?>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){

        $(document).on('click','a.delete',function (){
            if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {
                $(".page_loader").show();
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?php echo base_url(); ?>' + 'ems/content/delete',
                    data: {id: id},
                    success: function (result) {
                    }
                });
                location.reload();
            }
        });

        $(document).on('click','a.publish',function (){
            $(".page_loader").show();
            var str = $(this).attr('data-status');
            split_str = str.split('-');
            var status = split_str[0];
            var id = split_str[1];
            $.ajax({
                type: "POST",
                async:false,
                url: '<?php echo base_url(); ?>'+'ems/list_content/publish',
                data: {id:id,pub_status:status},
                success: function (result) {
                }
            });
            location.reload();
        });

        $('a.publish_compitition').click(function (){
            $(".page_loader").show();
            var str = $(this).attr('data-status');
            split_str = str.split('-');
            var status = split_str[0];
            var id = split_str[1];
            $.ajax({
                type: "POST",
                async:false,
                url: '<?php echo base_url(); ?>'+'ems/list_content/publish_compitition',
                data: {id:id,pub_status:status},
                success: function (result) {
                }
            });
            location.reload();
        });

        $('#add').click(function (){
            <?php if($tpl_name == 'supporters'){ ?>
            var count_rows = $('.dt_default tbody tr.supporter').length;

            if(count_rows === 6){
                alert("<?php echo $admin_lang['label']['list_content_logo_limit']; ?>");
                return false;
            }else{
                return true;
            }
            <?php }else if($tpl_name == 'blog'){ ?>
            var count_rows = $('.dt_default tbody tr.blog').length;

            if(count_rows === 2){
                alert("<?php echo $admin_lang['label']['list_content_categories_limit']; ?>");
                return false;
            }else{
                return true;
            }
            <?php } ?>

        });

        $(".page_order").click(function(){
            $("#sortme").sortable({
                update : function () {
                    serial = $('#sortme').sortable('serialize');
                    $.ajax({
                        type: "post",
                        url: "<?php echo base_url(); ?>ems/content/sortable",
                        data: serial,
                        success: function(data){
                            // location.reload();
                        },
                        error: function(){
                            alert("theres an error with AJAX");
                        }
                    });
                }
            });
            $(".PageSizeContainer div:last-child").click();
        });

    });
</script>
<?php $template = fetchTemplate($parent_id); ?>
<!--New HTML-->
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo pageTitle($parent_id,$admin_lang['admin_lang']); ?> - <?php echo $admin_lang['label']['gallery_sorting']; ?></h1>

        <div id="success_msg" style="text-align:center;color:#7cb342;">
            <?php
            if (validation_errors()) {
                echo _erMsg(validation_errors());
            } if ($this->session->flashdata('message')) {
                echo $this->session->flashdata('message');
            }
            ?>
        </div>

    </div>
    <div id="page_content_inner">

        <?php if($this->uri->segment(4) == 'eng_landmarks_image' || $this->uri->segment(4) == 'eng_other_sectors_slider'){ ?>
            <a href="<?php echo base_url().'ems/list_content/edit/id/'.$this->uri->segment(5); ?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"> <?php echo $admin_lang['label']['back']; ?></a>
        <?php }else{ ?>
            <a href="<?php echo base_url().'ems/content/edit/id/'.$this->uri->segment(5); ?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"> <?php echo $admin_lang['label']['back']; ?></a>
        <?php } ?>

        &nbsp <span style="color:red; font-size: 17px;"> <?php echo $admin_lang['label']['drag_and_drop']; ?></span>

        <a href="javascript:void(0);" class="md-btn md-btn-primary autoTooltip save_titles" style="float: right; margin-left: 10px; position: fixed; z-index: 9999;" title="<?php echo $admin_lang['label']['save_titles']; ?>"> <?php echo $admin_lang['label']['save_titles']; ?></a>

        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical">
                                <thead>
                                <tr>
                                    <th><?php echo $admin_lang['label']['Sr']; ?></th>
                                    <th><?php echo $admin_lang['label']['image']; ?></th>
                                    <th><?php echo $admin_lang['label']['title']; ?></th>
                                </tr>
                                </thead>
                                <tbody class="sortmeGallery" data-field-name="<?php echo $this->uri->segment(4); ?>">
                                <?php
                                if($tpl_name == 'gallery')
                                {
                                    if($list_content){
                                        $i=0;
                                        foreach ($list_content as $result => $val) {
                                            if($val->category == 0 && $val->tpl != ''){
                                                $list_content2[] = $val;
                                            }

                                        }
                                    }
                                    $list_content = $list_content2;
                                }

                                if($list_content){
                                    $i=0;
                                    $j = 0;
                                    foreach ($list_content as $result => $val) {
                                        $image = base_url() . 'assets/script/' . $val;

                                        if (str_replace("=",'',$val) != '') {

                                            if($i == 0){
                                                $tclass = 'autoTooltip';
                                            }else{
                                                $tclass = 'tooltip';
                                            }
                                            if($val->tpl == 'supporters'){
                                                $tpl_class = 'supporter';
                                            }else if($val->tpl == 'blog_listing'){
                                                $tpl_class = 'blog';
                                            }else{
                                                $tpl_class = '';
                                            }

                                            ?>
                                            <tr class="" id="<?php echo $val; ?>">
                                                <td><?php echo ++$i;?></td>
                                                <td>
                                                    <img class="align-self-center img-fluid" src="<?php echo $image ?>" alt="placeholder image" height="65" width="70">
                                                </td>

                                                <td>
                                                    <div class="uk-form-row">
                                                        <input type="text" class="md-input" id="title_<?php echo $j ?>" name="title_<?php echo $j ?>" value="<?php echo $titles[$j]; ?>" style="border: 1px solid #ccc;" placeholder="<?php echo $admin_lang['label']['please_type_title']; ?>" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php  $j++; } } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- light box for image -->
<button class="md-btn" id="btn_thumb" data-uk-modal="{target:'#modal_lightbox'}" style="display:none;">Open</button>
<div class="uk-modal" id="modal_lightbox">
    <div class="uk-modal-dialog uk-modal-dialog-lightbox">
        <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
        <img id="img_url" src="" alt="" style="background: #f5f5f5;"/>
    </div>
</div>
<!-- end light box for image -->
<!--End New HTML-->

<script type="text/javascript" charset="utf-8">

    $('.img-fluid').click(function () {
        $('#img_url').attr("src","");
        $('#btn_thumb').click();
        var img_src = $(this).attr("src");
        $('#img_url').attr("src",img_src);
    });

    function goBack() {
        window.history.back();
    }
    $(document).ready(function(){
        $(".sortmeGallery").sortable({
            update : function () {
                serial = $(this).closest('tbody.sortmeGallery').sortable('serialize');
                //alert(serial);


                var i = 1;
                var field_values = '';
                $('.uk-table tr').each(function() {
                    var field_value = $(this).find('input').val();
                    //alert(field_value);
                    //field_value = '';
                    if(!field_value){
                        field_value = '';
                    }
                    field_values += field_value+'___'+this.id+'|';
                    i = i+1;
                });
                var field_name = $(this).closest('tbody.sortmeGallery').attr("data-field-name");

                //alert(field_name);

                var newOrdering = field_values.replace("|", ",");
                var colum  = '<?php echo $this->uri->segment(4); ?>';
                //alert(newOrdering);

                $.ajax({
                    type: "post",
                    dataType: "JSON",
                    url: "<?php echo base_url(); ?>ems/content/sortableImageGallery?field="+colum+'&id=<?php echo $this->uri->segment(5); ?>',
                    data: {field_values:field_values},
                    success: function(data){
                        //alert(data.field_values)
                        $('#'+field_name).val(data.field_values);
                        //alert('');
                        setTimeout(function(){ location.reload(); }, 1000);
                    },
                    error: function(){
                        alert("theres an error with AJAX");
                    }
                });
            }
        });

        $('.save_titles').click(function () {
            var field_values = '';
            var i = 0;
            $('.uk-table tr').each(function() {
                var field_value = $('#title_'+i).val();
                //alert(field_value);
                if(!field_value){
                    field_value = '';
                }
                field_values += this.id+'|'+field_value+'___';
                i = i+1;
            });

            //alert(field_values);
            var colum  = '<?php echo $this->uri->segment(4); ?>';
            var field_name = field_values.replace(/___\s*$/, "");;

            //field_name = field_name.replace(/|\s*$/, "");;

            //alert(field_name);
            $(".page_loader").show();
            $.ajax({
                type: "post",
                dataType: "JSON",
                url: "<?php echo base_url(); ?>ems/content/sortableImageGallery?field="+colum+'&id=<?php echo $this->uri->segment(5); ?>',
                data: {field_values:field_name},
                success: function(data){
                    //alert(data.field_values)
                    //$('#'+field_name).val(data.field_values);
                    //$('#success_msg').val(data.message_ajax);
                    location.reload();
                },
                error: function(){
                    alert("theres an error with AJAX");
                }
            });
        });

    });
</script>