<?php $admin_lang = check_admin_lang(); ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){

    $('a.delete').click(function (){
        if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {
            $(".page_loader").show();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url(); ?>' + 'ems/content/delete',
                data: {id: id},
                success: function (result) {
                }
            });
            location.reload();
        }
    });

});
</script>
<!--New HTML-->

<!-- Content Wrapper. Contains page content -->
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['version_page']; ?></h1>
        <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
    </div>
    <div id="page_content_inner">
        <a href="<?php echo base_url().'ems/content/edit/id/'.$page_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th><?php echo $admin_lang['label']['version']; ?></th>
                                    <th><?php echo $admin_lang['label']['version_title']; ?></th>
                                    <th><?php echo $admin_lang['label']['version_date']; ?></th>
                                    <th><?php echo $admin_lang['label']['version_created']; ?></th>
                                    <th class="nosort"><?php echo $admin_lang['label']['action']; ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($versions){
                                    $i=0;
                                    foreach ($versions as $result => $val) {
                                        if($i == 0){
                                            $tclass = 'autoTooltip';
                                        }else{
                                            $tclass = 'tooltip';
                                        }
                                        $i++;
                                        $user_info = usre_data($val->content_updated_by);
                                        ?>
                                        <tr class="<?php echo $val->id;?>">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $val->eng_title; ?></td>
                                            <td><?php echo $val->content_updated_at; ?></td>
                                            <td><?php echo $user_info->usr_uname; ?></td>
                                            <td>
                                                <a href="<?php echo base_url().'ems/content/edit/id/'.$page_id.'/version_id/'.$val->id;?>" class="edit <?php echo $tclass; ?>" title="<?php echo $admin_lang['label']['version_view']; ?>">
                                                    <i class="md-icon material-icons">visibility</i>
                                                </a>
                                                <a href="javascript:void(0);" class="uk-margin-left delete <?php echo $tclass; ?>" data-id="<?php echo $val->id;?>" title="<?php echo $admin_lang['label']['tooltip_delete']; ?>">
                                                    <i class="material-icons md-24 delete">&#xE872;</i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php  } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- /.content-wrapper -->

<!--End New HTML-->


