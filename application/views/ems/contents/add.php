<?php $admin_lang = check_admin_lang(); ?>

<style>
	.customDivs{
		display: inline-block;
		width: 48%;
	}
	.showMetaDivs > .uk-width-medium-1-2{
		display: inline-block !important;
	}
	.showMetaDivs{
		width: 100%;
		display: inline-table;
		/* padding: 45px; */
		/* margin: 30px; */
		margin: inherit;
		margin-left: 4px;
	}
</style>

<div id="page_content">
<?php echo form_open_multipart('ems/content/savePage', array('method' => 'post', 'id' => 'updateAbout','onsubmit' => 'return checkUniquePageTitle(0)')); ?>
<input type="hidden" value="1" id="pub_status" name="pub_status">
<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
	<h1 id="product_edit_name"><?php echo $admin_lang['label']['content_add_page']; ?></h1>
	<div id="success_msg" style="text-align:center;color:#7cb342;">
		<?php
		if (validation_errors()) {
			echo _erMsg(validation_errors());
		} if ($this->session->flashdata('message')) {
			echo $this->session->flashdata('message');
		}
		?>
	</div>
	<div class="uk-width-medium-1-2 customDivs">
			<label class="autoTooltip" title="<?php echo $admin_lang['label']['content_select_template']; ?>"><?php echo $admin_lang['label']['content_page_template']; ?></label>
			<select class="required" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="<?php echo $admin_lang['label']['content_select_template']; ?>" name="tpl" id="tpl" onchange="show_div(this.value);">
				<option selected value="">------------------------------------<?php echo $admin_lang['label']['content_select_template']; ?>------------------------------------</option>


                <option value="home">Home</option>
                <option value="about_us">About Us</option>
                <option value="companies_map">Companies Map</option>
                <option value="profile">Profile</option>
                <option value="ceo_message">Chairman Message</option>
                <option value="bod">Board of Directors</option>
                <option value="events">Events</option>
                <option value="landmarks">Landmarks & Assets</option>
                <option value="our_sectors">Our Sectors</option>
                <option value="other_sectors">Other Sectors</option>
                <option value="murjan_holding">AL Murjan Holding</option>
                <option value="murjan_group">AL Murjan Groups</option>
                <option value="history">History</option>
                <option value="careers">Careers</option>
                <option value="privacy_policy">Privacy Policy</option>
                <option value="terms">Terms & Conditions</option>
                <option value="contact_us">Contact Us</option>
                <option value="csr">Corporate Social Responsibility</option>
                <option value="self">Self Application</option>
                <option value="sitemap">Sitemap</option>
<!--				<option value="home">Home</option>-->
<!--				<option value="about_us">About Us</option>-->
<!--				<option value="profile">Profile</option>-->
<!--				<option value="history">History</option>-->
<!--				<option value="mission_vision">Mission, Vision &amp; Values</option>-->
<!--				<option value="why_fa">Why Us</option>-->
<!--				<option value="ceo_message">Ceo Message</option>-->
<!--				<option value="ceo_clients">Our Clients</option>-->
<!--				<option value="our_services">Products & Services</option>-->
<!--				<option value="media_center">Media Center</option>-->
<!--				<option value="events">Events</option>-->
<!--				<option value="news">Latest News</option>-->
<!--				<option value="careers">Careers</option>-->
<!--				<option value="faqs">FAQs</option>-->
<!--				<option value="privacy_policy">Privacy Policy</option>-->
<!--				<option value="terms">Terms & Conditions</option>-->
<!--				<option value="contact_us">Contact Us</option>-->
<!--				<option value="introduction">Introduction</option>-->
<!--				<option value="security_printing">Security Printing</option>-->
<!--				<option value="security_features">Security Features</option>-->
<!--				<option value="our_approach">Our Approach</option>-->
<!--				<option value="request_now">Request Now</option>-->
<!--				<option value="news_room">News Room</option>-->
				<!--
					<option value="awards">Awards</option>
					<option value="bod">Board of Directors</option>
					<option value="projects">Project Categories</option>
					<option value="services_category">Services Categories</option>
					
				-->
				
			</select>
	</div>
	
	<div class="uk-width-medium-1-2 customDivs">
		<label class="autoTooltip" title="<?php echo $admin_lang['label']['content_select_meta_title_options']; ?>"><?php echo $admin_lang['label']['content_select_meta_title_options']; ?></label>
		<select class="show_tags" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="<?php echo $admin_lang['label']['content_select_meta_title_options']; ?>" name="show_tags" id="show_tags">
			<option value="1"><?php echo $admin_lang['label']['show']; ?></option>
			<option value="0"><?php echo $admin_lang['label']['hide']; ?></option>
		</select>
	</div>
</div>
<div id="page_content_inner">
<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
<div class="uk-width-xLarge-10-10  uk-width-large-10-10">
    <div class="md-card">
        <div class="md-card-toolbar">
            <h3 class="md-card-toolbar-heading-text">
                <?php echo $admin_lang['label']['content_page_content']; ?>
            </h3>
        </div>
        <div class="md-card-content">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2">
                    <label><?php echo $admin_lang['label']['content_eng_title']; ?>*</label>
                    <div class="uk-form-row">
                        <input type="text" class="md-input required" id="eng_title" name="eng_title" value="" />
                    </div>
                </div>
                <div class="uk-width-medium-1-2">
                    <label><?php echo $admin_lang['label']['content_arb_title']; ?>* </label>
                    <div class="uk-form-row">
                        <input class="md-input required" name="arb_title" id="arb_title" value="" />
                    </div>
                </div>
                <div class="uk-width-medium-1-2">
                    <label><?php echo $admin_lang['label']['content_eng_sub_title']; ?>*</label>
                    <div class="uk-form-row">
                        <input type="text" class="md-input required" id="eng_sub_title" name="eng_sub_title" value="" />
                    </div>
                </div>
                <div class="uk-width-medium-1-2">
                    <label><?php echo $admin_lang['label']['content_arb_sub_title']; ?>* </label>
                    <div class="uk-form-row">
                        <input class="md-input required" name="arb_sub_title" id="arb_sub_title" value="" />
                    </div>
                </div>
                <!--Start Content Divs-->
                <!--Last parameter after lang is for parent class which is using for hide/show divs from show_div() JS function-->
                <!--For textarea field there is words limit digit after comma which is optional-->
                <?php
                //other_sectors
                create_textarea($admin_lang['label']['home_content_description'], 'other_sector_home_desc', 'eng','other_sectors_div');
                create_textarea($admin_lang['label']['home_content_description_ar'], 'other_sector_home_desc', 'arb','other_sectors_div');
                create_textarea($admin_lang['label']['page_content_description'], 'other_sector_page_desc', 'eng','other_sectors_div');
                create_textarea($admin_lang['label']['page_content_description_ar'], 'other_sector_page_desc', 'arb','other_sectors_div');
                //csr
                create_textfield_not_required($admin_lang['label']['page_content_title'], 'csr_title', 'eng','csr_div');
                create_textfield_not_required($admin_lang['label']['page_content_title_ar'], 'csr_title', 'arb','csr_div');
                create_textarea($admin_lang['label']['page_content_description'], 'csr_desc', 'eng','csr_div');
                create_textarea($admin_lang['label']['page_content_description_ar'], 'csr_desc', 'arb','csr_div');

               //even and news
               /* create_textfield_not_required($admin_lang['label']['page_content_title'], 'news_page_title', 'eng','events_div');
                create_textfield_not_required($admin_lang['label']['page_content_title_ar'], 'news_page_title', 'arb','events_div');*/
                create_textarea($admin_lang['label']['page_content_description'], 'news_desc', 'eng','events_div');
                create_textarea($admin_lang['label']['page_content_description_ar'], 'news_desc', 'arb','events_div');

                //landmarks
                create_textfield_not_required($admin_lang['label']['page_content_title'], 'landmark_page_title', 'eng','landmark_div');
                create_textfield_not_required($admin_lang['label']['page_content_title_ar'], 'landmark_page_title', 'arb','landmark_div');
                create_textarea($admin_lang['label']['page_content_description'], 'landmark_desc', 'eng','landmark_div');
                create_textarea($admin_lang['label']['page_content_description_ar'], 'landmark_desc', 'arb','landmark_div');

                //bod
                create_image('Logo Image', 'bod_logo_image', '225&times;200', 'eng','bod_div');
                create_textarea("Description", 'bod_desc', 'eng','bod_div');
                create_textarea('Description', 'bod_desc', 'arb','bod_div');


                // Profile
                create_image('Background Image', 'profile_image', '2030&times;370', 'eng','profile_div');
                create_image('Thumb Image', 'value_image', '900&times;900', 'eng','profile_div');

                create_textarea("Page Title", 'page_title_profile', 'eng','profile_div');
                create_textarea("Page Title Arabic", 'page_title_profile', 'arb','profile_div');

                create_textarea("Profile Description", 'desc_pro', 'eng','profile_div');
                create_textarea('Profile Description', 'desc_pro', 'arb','profile_div');

                create_image('Mission Section Image', 'mission_image', '900&times;900', 'eng','profile_div');

                create_textfield_not_required("Vision Title", 'vission_title', 'eng','profile_div');
                create_textfield_not_required("Vision Title", 'vission_title', 'arb','profile_div');
                create_textarea('Vision Description', 'vission_value', 'eng','profile_div');
                create_textarea('Vision Description', 'vission_value', 'arb','profile_div');

                create_textfield_not_required("Mission Title", 'mission_title', 'eng','profile_div');
                create_textfield_not_required("Mission Title", 'mission_title', 'arb','profile_div');
                create_textarea('Mission Description', 'mission_desc_value', 'eng','profile_div');
                create_textarea('Mission Description', 'mission_desc_value', 'arb','profile_div');

                create_textfield_not_required("Strategy Title", 'strategy_title', 'eng','profile_div');
                create_textfield_not_required("Strategy Title", 'strategy_title', 'arb','profile_div');
                create_textarea('Strategy Description', 'strategy_value', 'eng','profile_div');
                create_textarea('Strategy Description', 'strategy_value', 'arb','profile_div');
                create_textfield_not_required("Value Title", 'value_title', 'eng','profile_div');
                create_textfield_not_required("Value Title", 'value_title', 'arb','profile_div');


               /* create_image('Founder Image', 'founder_image', '2030&times;370', 'eng','profile_div');
                create_textarea('Founder Description', 'desc_founder', 'eng','profile_div');
                create_textarea('Founder Description', 'desc_founder', 'arb','profile_div'
                );
                create_textfield_not_required('Founder Name','founder_name','eng','profile_div');
                create_textfield_not_required('Founder Name','founder_name','arb','profile_div');
                create_textfield_not_required('Position','founder_position','eng','profile_div');
                create_textfield_not_required('Position','founder_position','arb','profile_div');*/

					//Introduction
					create_image($admin_lang['label']['content_banner_image'], 'image_intro', '2030&times;370', 'eng','introduction_div');
					create_gallery($admin_lang['label']['gallery_intro'],'gallery_intro','400&times;300','eng','introduction_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_intro', 'eng','introduction_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_intro', 'arb','introduction_div');
					
					//Security Printing
					create_image($admin_lang['label']['content_banner_image'], 'image_printing', '2030&times;370', 'eng','security_printing_div');
					create_image($admin_lang['label']['security_logo'], 'image_printing_logo', '2030&times;370', 'eng','security_printing_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_printing', 'eng','security_printing_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_printing', 'arb','security_printing_div');
					
					//Security Features
					create_textarea($admin_lang['label']['content_description'], 'desc_security', 'eng','security_features_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_security', 'arb','security_features_div');
					
					//Media Center
					create_gallery('Gallery','gallery_media','400&times;300','eng','media_center_div');
							
							
					//Our Approach
					create_textarea($admin_lang['label']['approach_desc'], 'desc_approach', 'eng','our_approach_div');
					create_textarea($admin_lang['label']['approach_desc'], 'desc_approach', 'arb','our_approach_div');
					
					//History
                    create_image($admin_lang['label']['content_banner_image'], 'history_banner_image', '2030&times;370', 'eng','history_div');
					create_textarea('Banner Description', 'history_banner_desc', 'eng','history_div');
					create_textarea('Banner Description', 'history_banner_desc', 'arb','history_div');
                    create_textfield('Family Values Title', 'history_family_values_title', 'eng','history_div');
                    create_textfield('Family Values Title', 'history_family_values_title', 'arb','history_div');
                    create_textarea('Family Values Description', 'history_family_values_desc', 'eng','history_div');
                    create_textarea('Family Values Description', 'history_family_values_desc', 'arb','history_div');

					// Mission, Vision &amp; Values
					create_textarea($admin_lang['label']['mission_desc'], 'desc_miss', 'eng','mission_vision_div');
					create_textarea($admin_lang['label']['mission_desc'], 'desc_miss', 'arb','mission_vision_div');
					
					create_textarea($admin_lang['label']['values_desc'], 'desc_val', 'eng','mission_vision_div');
					create_textarea($admin_lang['label']['values_desc'], 'desc_val', 'arb','mission_vision_div');
					
					create_textarea($admin_lang['label']['vision_desc'], 'desc_vis', 'eng','mission_vision_div');
					create_textarea($admin_lang['label']['vision_desc'], 'desc_vis', 'arb','mission_vision_div');
					
					// Why Us
					create_textarea($admin_lang['label']['content_description'], 'desc_why_us', 'eng','why_fa_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_why_us', 'arb','why_fa_div');
					
					// Ceo Message
					create_image($admin_lang['label']['image'], 'ceo_image', '2030&times;370', 'eng','ceo_message_div');
                create_textfield('CEO Name','ceo_name','eng','ceo_message_div');
                create_textfield('CEO Name','ceo_name','arb','ceo_message_div');
                create_textfield('Title','ceo_title','eng','ceo_message_div');
                create_textfield('Title','ceo_title','arb','ceo_message_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_our_mes', 'eng','ceo_message_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_our_mes', 'arb','ceo_message_div');

                // holding
                create_image('Image', 'holding_logo', '200&times;370', 'eng','holding_div');
                create_textarea($admin_lang['label']['content_description'], 'desc_holding', 'eng','holding_div');
                create_textarea($admin_lang['label']['content_description'], 'desc_holding', 'arb','holding_div');

					// Careers
					create_image('Image', 'careers_image', '200&times;370', 'eng','careers_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_career', 'eng','careers_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_career', 'arb','careers_div');
					create_textfield_not_required('Benefits Section Heading', 'benefits_heading', 'eng','careers_div');
					create_textfield_not_required('Benefits Section Heading', 'benefits_heading', 'arb','careers_div');
                create_textarea($admin_lang['label']['content_description'], 'desc_benefits', 'eng','careers_div');
                create_textarea($admin_lang['label']['content_description'], 'desc_benefits', 'arb','careers_div');
					create_textfield_not_required('Positions Section Heading', 'positions_heading', 'eng','careers_div');
					create_textfield_not_required('Positions Section Heading', 'positions_heading', 'arb','careers_div');
                create_textarea($admin_lang['label']['content_description'], 'desc_positions', 'eng','careers_div');
                create_textarea($admin_lang['label']['content_description'], 'desc_positions', 'arb','careers_div');

                // self Application
                create_image('Image', 'selfapp_image', '200&times;370', 'eng','self_div');


					// Privacy Policy
					/*create_image($admin_lang['label']['content_banner_image'], 'image_privacy', '2030&times;370', 'eng','privacy_policy_div');*/
					create_textarea($admin_lang['label']['content_description'], 'desc_privacy', 'eng','privacy_policy_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_privacy', 'arb','privacy_policy_div');
					
					// Terms & Conditions
					/*create_image($admin_lang['label']['content_banner_image'], 'image_term', '2030&times;370', 'eng','terms_div');*/
					create_textarea($admin_lang['label']['content_description'], 'desc_terms', 'eng','terms_div');
					create_textarea($admin_lang['label']['content_description'], 'desc_terms', 'arb','terms_div');
					
					
					// Contact Us
					create_textarea($admin_lang['label']['content_address'], 'desc_contact', 'eng', 'contact_us_div','','');
					create_textarea($admin_lang['label']['content_address'], 'desc_contact', 'arb', 'contact_us_div','','');  
					create_location($admin_lang['label']['content_location'], 'maps_loc', 'contact_us_div', '', '');
					create_textfield($admin_lang['label']['phone'], 'phone','eng', 'contact_us_div', '', '');
					echo '<div class="uk-width-medium-1-2"></div>';
					create_textfield($admin_lang['label']['office_email'], 'mail','eng' , 'contact_us_div', '', '');
					echo '<div class="uk-width-medium-1-2"></div>';
					
					
				?>
                <!--End Content Divs-->
				<div class="showMetaDivs">	
					<div class="uk-width-medium-1-2">
						<label><?php echo $admin_lang['label']['content_eng_meta_title']; ?></label>
						<div class="uk-form-row">
							<input type="text" class="md-input" id="meta_title_eng" name="meta_title_eng" value="" />
						</div>
					</div>
					<div class="uk-width-medium-1-2">
						<label><?php echo $admin_lang['label']['content_arb_meta_title']; ?></label>
						<div class="uk-form-row">
							<input class="md-input" name="meta_title_arb" id="meta_title_arb" value="" />
						</div>
					</div>
					<div class="uk-width-medium-1-2">
						<label><?php echo $admin_lang['label']['content_eng_meta_desc']; ?></label>
						<div class="uk-form-row">
							<textarea class="md-input" id="meta_desc_eng" name="meta_desc_eng"></textarea>
						</div>
					</div>
					<div class="uk-width-medium-1-2">
						<label><?php echo $admin_lang['label']['content_arb_meta_desc']; ?></label>
						<div class="uk-form-row">
							<textarea class="md-input" id="meta_desc_arb" name="meta_desc_arb"></textarea>
						</div>
					</div>
					<div class="uk-width-medium-1-2">
						<label><?php echo $admin_lang['label']['content_eng_meta_keys']; ?></label>
						<div class="uk-form-row">
							<textarea class="md-input" id="meta_keywords_eng" name="meta_keywords_eng"></textarea>
							<p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
						</div>
					</div>
					<div class="uk-width-medium-1-2">
						<label><?php echo $admin_lang['label']['content_arb_meta_keys']; ?></label>
						<div class="uk-form-row">
							<textarea class="md-input" id="meta_keywords_arb" name="meta_keywords_arb"></textarea>
							<p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
</div>
<?php echo form_close(); ?>
<div class="md-fab-wrapper">
    <a class="md-fab md-fab-primary md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_save_content']; ?>" href="javascript:void(0);" id="saveAbout">
        <i class="material-icons">&#xE161;</i>
    </a>
</div>
</div>
</div>
<script language="javascript">
/* $(document).on('click','.removeImageAdded',function (){
	if(confirm("Are you sure to delete image? \nNote: After deleting you should need to save form!")){
		var img = $(this).attr('data-id');
		var field = $(this).attr('data-id-field');
		var vaule = new Array();
		var new_val = $("#"+field).val();
		//new_val = new_val.split(",");
		
		new_val = new_val.replace(img, '')
		var final_value = new_val.replace(/,\s*$/, "");
		$("#"+field).val(final_value);
		$(this).parent().parent().remove();
    }

}); */

$(document).ready(function () {
	
	$('#saveAbout').click(function () {
        $(".page_loader").show();
        $("#updateAbout").attr("action", "<?php echo base_url();?>ems/content/savePage/");
        $("#updateAbout").attr("target", "");
        $('#updateAbout').submit();
        $(".required").each(function(){
            if($(this).val() == ''){
                setTimeout(function(){
                    $(".page_loader").hide();
                },500);
            }
        });
    });
    $("#updateAbout").validate();
	
	
	$('.show_tags').change(function () {
       var value = $(this).val();
	   if(value == 1){
		   $('.showMetaDivs').fadeIn('slow');
	   }
	   else{
		   $('.showMetaDivs').fadeOut('slow');
	   }
    });
	
});
setInterval(function(){$('#success_msg').html(''); }, 3000);
</script>