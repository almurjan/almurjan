<?php $admin_lang = check_admin_lang(); ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){

    $(document).on('click','a.delete',function (){
        if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {
            $(".page_loader").show();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url(); ?>' + 'ems/content/delete',
                data: {id: id},
                success: function (result) {
                }
            });
            location.reload();
        }
    });
	
	$(document).on('click','a.publish',function (){
        $(".page_loader").show();
        var str = $(this).attr('data-status');
        split_str = str.split('-');
        var status = split_str[0];
        var id = split_str[1];
        $.ajax({
            type: "POST",
            async:false,
            url: '<?php echo base_url(); ?>'+'ems/list_content/publish',
            data: {id:id,pub_status:status},
            success: function (result) {
            }
        });
        location.reload();
    });

    $('a.publish_compitition').click(function (){
        $(".page_loader").show();
        var str = $(this).attr('data-status');
        split_str = str.split('-');
        var status = split_str[0];
        var id = split_str[1];
        $.ajax({
            type: "POST",
            async:false,
            url: '<?php echo base_url(); ?>'+'ems/list_content/publish_compitition',
            data: {id:id,pub_status:status},
            success: function (result) {
            }
        });
        location.reload();
    });

    $('#add').click(function (){
        <?php if($tpl_name == 'supporters'){ ?>
            var count_rows = $('.dt_default tbody tr.supporter').length;

            if(count_rows === 6){
                alert("<?php echo $admin_lang['label']['list_content_logo_limit']; ?>");
                return false;
            }else{
                return true;
            }
        <?php }else if($tpl_name == 'blog'){ ?>
            var count_rows = $('.dt_default tbody tr.blog').length;

            if(count_rows === 2){
                alert("<?php echo $admin_lang['label']['list_content_categories_limit']; ?>");
                return false;
            }else{
                return true;
            }
        <?php } ?>

    });
	
	$(".page_order").click(function(){
        $("#sortme").sortable({
            update : function () {
                serial = $('#sortme').sortable('serialize');
                $.ajax({
                    type: "post",
                    url: "<?php echo base_url(); ?>ems/content/sortable",
                    data: serial,
                    success: function(data){
                        // location.reload();
                    },
                    error: function(){
                        alert("theres an error with AJAX");
                    }
                });
            }
        });
        $(".PageSizeContainer div:last-child").click();
    });
	
});
</script>
<?php $template = fetchTemplate($parent_id); ?>
<!--New HTML-->
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo pageTitle($parent_id,$admin_lang['admin_lang']); ?> - <?php echo $admin_lang['label']['list_content_listing']; ?></h1>
    </div>
    <div id="page_content_inner">
        <?php if($template == 'our_services' || $template == 'our_approach'){ ?>
            <a href="<?php echo base_url().'ems/list_content/edit/id/'.$parent_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"> <?php echo $admin_lang['label']['back']; ?></a>
        <?php }else{ ?>
            <a href="<?php echo base_url().'ems/content/edit/id/'.$parent_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"> <?php echo $admin_lang['label']['back']; ?></a>
        <?php } ?>
		
        <a id="add" href="<?php echo base_url().'ems/list_content/add/'.$parent_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_add']; ?>"> <?php echo $admin_lang['label']['add']; ?></a>
		<a class="md-btn md-btn-primary autoTooltip page_order" title="<?php echo $admin_lang['label']['tooltip_ordering']; ?>"> <?php echo $admin_lang['label']['ordering']; ?></a>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th><?php echo $admin_lang['label']['Sr']; ?></th>
                                    <th><?php echo $admin_lang['label']['title']; ?></th>
									
									<?php if($template == 'home'){ ?>
										<th><?php echo $admin_lang['label']['category']; ?></th>
									<?php } ?>
									
									<?php if($template == 'periodic_reports'){ ?>
										<th><?php echo $admin_lang['label']['list_content_report_type']; ?></th>
									<?php } ?>
									
                                    <th><?php echo $admin_lang['label']['active']; ?></th>
                                    <th class="nosort"><?php echo $admin_lang['label']['action']; ?></th>
                                </tr>
                                </thead>
                                <tbody id="sortme">
                                <?php
								if($tpl_name == 'gallery')
								{
									if($list_content){
										$i=0;
										foreach ($list_content as $result => $val) {
											if($val->category == 0 && $val->tpl != ''){
												$list_content2[] = $val;
											}
											
										}
									}
									$list_content = $list_content2;
								}	
								
                                if($list_content){
                                    $i=0;
                                    foreach ($list_content as $result => $val) {
										
                                        if($i == 0){
                                            $tclass = 'autoTooltip';
                                        }else{
                                            $tclass = 'tooltip';
                                        }
                                        if($val->tpl == 'supporters'){
                                            $tpl_class = 'supporter';
                                        }else if($val->tpl == 'blog_listing'){
                                            $tpl_class = 'blog';
                                        }else{
                                            $tpl_class = '';
                                        }
										
                                        ?>
                                        <tr class="<?php echo $val->id.' '.$tpl_class;?>" id="id_<?php echo $val->id; ?>">
                                            <td><?php echo ++$i;?></td>
                                            <td><?php echo ($admin_lang['admin_lang'] == 'eng'?$val->eng_title:$val->arb_title); ?></td>
                                        
											<?php if($template == 'home'){ ?>
												
												<td>
													<?php
													if($val->selectSlideOptions != ''){
														if($val->selectSlideOptions == 1){
															$category_name =  ($admin_lang['admin_lang'] == 'eng' ? 'Slider Image' : 'Slider Image');
														}
														if($val->selectSlideOptions == 2){
															$category_name =  ($admin_lang['admin_lang'] == 'eng' ? 'Slider Video' : 'Slider Video');
														}
														echo $category_name;
													}	
													?>
												</td>
												
											<?php } ?>
											
											
											
											
											<?php if($template == 'periodic_reports'){ ?>
												
												<td>
													<?php
													if($val->report_type != ''){
														if($val->report_type == 1){
															$category_name =  ($admin_lang['admin_lang'] == 'eng' ? 'Daily Report' : 'Daily Report');
														}
														if($val->report_type == 2){
															$category_name =  ($admin_lang['admin_lang'] == 'eng' ? 'Monthly Report' : 'Monthly Report');
														}
														if($val->report_type == 3){
															$category_name =  ($admin_lang['admin_lang'] == 'eng' ? 'Quaterly Report' : 'Quaterly Report');
														}
														echo $category_name;
													}	
													?>
												</td>
												
											<?php } ?>
											
											
											 <td>
                                                <?php if($val->pub_status == 1){;?>
                                                    <a href="javascript:void(0);" class="publish <?php echo $tclass; ?>" id="status" data-status="<?php echo '0-'.$val->id;?>" title="<?php echo $admin_lang['label']['tooltip_publish']; ?>">
                                                        <i class="md-icon material-icons green">check_circle</i>
                                                    </a>
                                                <?php }else{ ?>
                                                    <a href="javascript:void(0);" class="publish <?php echo $tclass; ?>" id="status" data-status="<?php echo '1-'.$val->id;?>" title="<?php echo $admin_lang['label']['tooltip_publish']; ?>">
                                                        <i class="md-icon material-icons black">check_circle</i>
                                                    </a>
                                                <?php } ?>
                                            </td>
										
                                            <td>
                                                <a class="edit <?php echo $tclass; ?>" href="<?php echo base_url().'ems/list_content/edit/id/'.$val->id;?>" title="<?php echo $admin_lang['label']['tooltip_edit']; ?>">
                                                    <i class="md-icon material-icons">&#xE254;</i>
                                                </a>
                                                <?php
                                                $blog_id = getPageIdbyTemplate('blog');
                                                if($val->parant_id !== $blog_id){?>
                                                <a href="javascript:void(0);" class="uk-margin-left delete <?php echo $tclass; ?>" data-id="<?php echo $val->id;?>" title="<?php echo $admin_lang['label']['tooltip_delete']; ?>">
                                                    <i class="material-icons md-24 delete">&#xE872;</i>
                                                </a>
                                                <?php } ?>
                                            </td>
											
                                        </tr>
                                    <?php  } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--End New HTML-->


