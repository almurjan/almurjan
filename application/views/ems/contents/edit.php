<style>
	#sortmeGallery tr{
		display: inline-block;
	}
</style>

<script>
	$(document).on('click','a.ActiveInactiveHomeSlider',function (){
        $(".page_loader").show();
        var str = $(this).attr('data-status');
        split_str = str.split('-');
        var status = split_str[0];
        var id = split_str[1];
        $.ajax({
            type: "POST",
            async:false,
            url: '<?php echo base_url(); ?>'+'ems/list_content/activeSlider',
            data: {id:id,pub_status:status},
            success: function (result) {
				//alert(result);
            }
        });
        location.reload();
    });
</script>
<div id="page_content">
    <?php $admin_lang = check_admin_lang(); ?>
<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
    <h1 id="product_edit_name">
	<?php if($result->parant_id == 0 ) { ?>
	
	<?php echo getPagePath($result->id,$admin_lang['admin_lang']). ' - ' . $admin_lang['label']['edit']; ?>
	
	<?php }else{ echo 'Version';} ?></h1>
    <!--<div class="modifiedDate">Last modified <?php /*echo date("F j, Y, g:i a",strtotime($result->content_updated_at)); */?> </div>-->
    <?php if($result->parant_id === 2000 ) { ?>
    <p><b>Total Versions : </b> <?php echo $total_versions; ?><p>
    <?php } ?>
    <div id="success_msg" style="text-align:center;color:#7cb342;">
        <?php
        if (validation_errors()) {
            echo _erMsg(validation_errors());
        } if ($this->session->flashdata('message')) {
            echo $this->session->flashdata('message');
        }
        ?>
    </div>
	
</div>
<div id="returned_page_image" style="display:none;"></div>
<div id="page_content_inner">
    <?php 
	if(/*$result->tpl == 'home' || */$result->tpl == 'history' || $result->tpl == 'awards' || $result->tpl == 'ceo_clients' || $result->tpl == 'faqs' || $result->tpl == 'our_services'  || $result->tpl == 'events' || $result->tpl == 'bod' || $result->tpl == 'our_approach'|| $result->tpl == 'landmarks' || $result->tpl == 'murjan_holding' || $result->tpl == 'profile' || $result->tpl == 'murjan_group' || $result->tpl == 'companies_map' || $result->tpl == 'csr'){ ?>
        <a href="<?php echo base_url();?>ems/list_content/index/<?php echo $result->id; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>">
            <?php
            /*if ($result->tpl == 'home')
                echo $admin_lang['label']['slider_listing'];*/
            if ($result->tpl == 'history' || $result->tpl == 'ceo_clients' || $result->tpl == 'events')
                echo $admin_lang['label']['list_content_listing'];
            if ($result->tpl == 'media_center')
                echo $admin_lang['label']['list_content_listing_albums'];
			if ($result->tpl == 'security_features')
				echo 'Features Listing';
			if ($result->tpl == 'our_approach')
				echo 'Approach\'s Categories';
			if ($result->tpl == 'our_services')
				echo 'Categories';
			if ($result->tpl == 'murjan_group')
				echo 'Listing';
			if ($result->tpl == 'companies_map')
				echo 'Map Pins Listing';
            if ($result->tpl == 'csr')
                echo 'Listing';
            if ($result->tpl == 'bod')
                echo 'Listing';
			if ($result->tpl == 'profile')
				echo 'Values Listing';
			if ($result->tpl == 'landmarks')
				echo 'Listing';
			if ($result->tpl == 'murjan_holding')
				echo 'Companies Listing';
			
            ?>
		</a> 
		
    <?php } ?>

    <?php if($result->tpl == 'landmarks'){ ?>
        <a href="<?php echo base_url();?>ems/list_content/category_index/<?php echo $result->id; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>">
            <?php
            if ($result->tpl == 'landmarks')
                echo 'Category Listing';
            ?>
        </a>
    <?php } ?>

    <?php /*if($result->tpl == 'profile'){ */?><!--
        <a href="<?php /*echo base_url();*/?>ems/list_content/index/<?php /*echo $result->id.'/1'; */?>" class="md-btn autoTooltip md-btn-primary" title="<?php /*echo $admin_lang['label']['tooltip_listing']; */?>">
            <?php
/*            if ($result->tpl == 'profile')
                echo 'Companies Listing';
            */?>
        </a>
    --><?php /*} */?>

    <?php if($result->tpl == 'careers'){ ?>
        <a href="<?php echo base_url();?>ems/list_content/index/<?php echo $result->id.'/0'; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>">
            <?php
            if ($result->tpl == 'careers')
                echo 'Vacancies Listing';
            ?>
        </a>
    <?php } ?>

    <?php if($result->tpl == 'careers'){ ?>
        <a href="<?php echo base_url();?>ems/list_content/index/<?php echo $result->id.'/1'; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>">
            <?php
            if ($result->tpl == 'careers')
                echo 'Benefits Listing';
            ?>
        </a>
    <?php } ?>


    <?php if($result->tpl == 'other_sectors'){ ?>
        <a href="<?php echo base_url();?>ems/list_content/index/<?php echo $result->id.'/0'; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>">
            <?php
            if ($result->tpl == 'other_sectors')
                echo 'Category Listing';
            ?>
        </a>
    <?php } ?>

	<?php if($result->tpl == 'media_center'){ ?>
        <a href="<?php echo base_url();?>ems/list_content/index_video/<?php echo $result->id; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>">
            <?php
            if ($result->tpl == 'media_center')
                echo 'Videos';
            ?>
		</a>	
    <?php } ?>
	
	
	
    <?php if($this->uri->segment(6) != ''){?>
        <a href="<?php echo base_url().'ems/content/versions/'.$page_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
    <?php } ?>
	
    <?php if($result->parant_id == 0){ ?>
        <a href="<?php echo base_url();?>ems/content/versions/<?php echo $result->id; ?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_versions']; ?>"><?php echo $admin_lang['label']['content_versions']; ?></a>
		<?php if ($result->tpl != 'home'){ ?>
        <a id="delete" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_delete']; ?>"><?php echo $admin_lang['label']['delete']; ?></a>
    <?php } } ?>
	
	<!--  <a href="javascript:void(0);" id="preview" class="md-btn md-btn-primary autoTooltip" title="<?php /*echo $admin_lang['label']['tooltip_preview']; */?>"><?php /*echo $admin_lang['label']['content_preview']; */?></a>-->
	
    <input type="hidden" value="<?php echo $result->eng_title; ?>" id="page_title_before_change" name="page_title_before_change">
    <?php echo form_open_multipart('ems/content/update/id/' . $result->id, array('method' => 'post', 'id' => 'updateAbout','onsubmit' => 'return checkUniquePageTitleEdit(1)')); ?>
    <input type="hidden" value="<?php echo $result->pub_status; ?>" id="pub_status" name="pub_status">
    <input type="hidden" value="<?php echo $result->id; ?>" id="gid" name="gid">
    <input type="hidden" value="<?php echo $page_id; ?>" id="page_id" name="page_id">
    <input type="hidden" value="<?php echo $result->tpl; ?>" id="tpl" name="tpl">
    <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
        <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
            <div class="md-card">
                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text">
                        <?php echo ($admin_lang['admin_lang'] == 'eng'?$result->eng_title:$result->arb_title); ?> <?php echo $admin_lang['label']['content_page_detail']; ?>
                    </h3>
                </div>
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_eng_title']; ?>*</label>
                            <div class="uk-form-row">
                                <input type="text" cols="30" rows="4" class="md-input required" id="eng_title" name="eng_title" value="<?php echo $result->eng_title; ?>" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_arb_title']; ?>* </label>
                            <div class="uk-form-row">
                                <input cols="30" rows="4" class="md-input required" name="arb_title" id="arb_title" value="<?php echo $result->arb_title; ?>" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_eng_sub_title']; ?>*</label>
                            <div class="uk-form-row">
                                <input type="text" class="md-input required" id="eng_sub_title" name="eng_sub_title" value="<?php echo $result->eng_sub_title; ?>" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_arb_sub_title']; ?>* </label>
                            <div class="uk-form-row">
                                <input class="md-input required" name="arb_sub_title" id="arb_sub_title" value="<?php echo $result->arb_sub_title; ?>" />
                            </div>
                        </div>
						
                        <!--Start Content Divs-->
                        <!--Last parameter after lang is for parent class which is using for hide & show content-->
                        <!--For textarea field there is words limit digit after comma which is optional-->
                        
						<?php
							
							// Home
							// create_image($admin_lang['label']['logo'], 'logo_image', '115&times;122', 'eng','home_div',true,$result->id);
                            // create_image('Footer Logo', 'footer_logo_image', '115&times;122', 'eng','home_div',true,$result->id);
                            // create_image('Watermark Image', 'watermark_image', '588&times;1013', 'eng','home_div',true,$result->id);
                            create_image('Banner Image', 'banner_image', '1920&times;741', 'eng','home_div',true,$result->id);
                            // create_textfield('Main Slider Heading','slider_heading','eng','home_div',true,$result->id);
                            // create_textfield('Main Slider Heading','slider_heading','arb','home_div',true,$result->id);
                            create_textfield('About Section Heading','about_heading','eng','home_div',true,$result->id);
                            create_textfield('About Section Heading','about_heading','arb','home_div',true,$result->id);
                            create_textarea('About Section Description','about_desc','eng','home_div',true,$result->id);
                            create_textarea('About Section Description','about_desc','arb','home_div',true,$result->id);
                            // create_image('Companies Section Image', 'achievements_image', '705&times;524', 'eng','home_div',true,$result->id);
                            create_textfield('Achievements Section Heading','achievements_heading','eng','home_div',true,$result->id);
                            create_textfield('Achievements Section Heading','achievements_heading','arb','home_div',true,$result->id);
                            create_textfield('Sectors','achievements_sectors','eng','home_div',true,$result->id);
                            create_textfield('Year of Experience','achievements_experience','eng','home_div',true,$result->id);
                            create_textfield('Companies','achievements_companies','eng','home_div',true,$result->id);
                            create_textfield('Employee','achievements_employee','eng','home_div',true,$result->id);
                            // create_textarea('Achievements Section Description','achievements_desc','eng','home_div',true,$result->id);
                            // create_textarea('Achievements Section Description','achievements_desc','arb','home_div',true,$result->id);
                        create_textfield('Companies Section Heading','companies_heading','eng','home_div',true,$result->id);
                        create_textfield('Companies Section Heading','companies_heading','arb','home_div',true,$result->id);

                            // about us
                        create_image('Banner Image', 'about_us_banner_image', '1920&times;623', 'eng','about_us_div',true,$result->id);


                        //csr
                        create_image('Banner Image', 'csr_banner_image', '1920&times;623', 'eng','csr_div',true,$result->id);
                        // create_textfield_not_required($admin_lang['label']['page_content_title'], 'csr_title', 'eng','csr_div',true,$result->id);
                        // create_textfield_not_required($admin_lang['label']['page_content_title_ar'], 'csr_title', 'arb','csr_div',true,$result->id);
                        // create_textarea("Description", 'csr_desc', 'eng','csr_div',true,$result->id);
                        // create_textarea('Description', 'csr_desc', 'arb','csr_div',true,$result->id);

                        //even and news
                        /*create_textfield_not_required($admin_lang['label']['page_content_title'], 'news_page_title', 'eng','events_div',true,$result->id);
                        create_textfield_not_required($admin_lang['label']['page_content_title_ar'], 'news_page_title', 'arb','events_div',true,$result->id);*/
                        create_image('Banner Image', 'news_banner_image', '1920&times;623', 'eng','events_div',true,$result->id);
                        create_textarea($admin_lang['label']['page_content_description'], 'news_desc', 'eng','events_div',true,$result->id);
                        create_textarea($admin_lang['label']['page_content_description_ar'], 'news_desc', 'arb','events_div',true,$result->id);

                        //landmarks
                        create_textfield_not_required($admin_lang['label']['page_content_title'], 'landmark_page_title', 'eng','landmark_div',true,$result->id);
                        create_textfield_not_required($admin_lang['label']['page_content_title_ar'], 'landmark_page_title', 'arb','landmark_div',true,$result->id);
                        create_textarea($admin_lang['label']['page_content_description'], 'landmark_desc', 'eng','landmark_div',true,$result->id);
                        create_textarea($admin_lang['label']['page_content_description_ar'], 'landmark_desc', 'arb','landmark_div',true,$result->id);

                        //other_sectors
                        // create_image('Banner Image', 'other_sector_banner_image', '1920&times;623', 'eng','other_sectors_div',true,$result->id);
                        create_gallery('Banner Images','other_sector_banner_images_slider','1920&times;623','eng','other_sectors_div',true, $result->id);
                        create_textarea($admin_lang['label']['home_content_description'], 'other_sector_home_desc', 'eng','other_sectors_div',true,$result->id);
                        create_textarea($admin_lang['label']['home_content_description_ar'], 'other_sector_home_desc', 'arb','other_sectors_div',true,$result->id);
                        // create_textarea($admin_lang['label']['page_content_description'], 'other_sector_page_desc', 'eng','other_sectors_div',true,$result->id);
                        // create_textarea($admin_lang['label']['page_content_description_ar'], 'other_sector_page_desc', 'arb','other_sectors_div',true,$result->id);

                        //bod
                        create_image('Logo Image', 'bod_logo_image', '225&times;200', 'eng','bod_div',true,$result->id);
                        create_textarea("Description", 'bod_desc', 'eng','bod_div',true,$result->id); 
                        create_textarea('Description', 'bod_desc', 'arb','bod_div',true,$result->id);

                        // Profile
                        // create_image('Background Image', 'profile_image', '555&times;375', 'eng','profile_div',true,$result->id);
                        // create_image('Thumb Image', 'value_image', '900&times;900', 'eng','profile_div',true,$result->id);

                        // create_textarea("Page Title", 'page_title_profile', 'eng','profile_div',true,$result->id);
                        // create_textarea("Page Title Arabic", 'page_title_profile', 'arb','profile_div',true,$result->id);

                        create_textarea("Profile Description", 'desc_pro', 'eng','profile_div',true,$result->id);
                        create_textarea('Profile Description', 'desc_pro', 'arb','profile_div',true,$result->id);

                        // create_image('Mission Section Image', 'mission_image', '900&times;900', 'eng','profile_div',true,$result->id);

                        create_textfield_not_required("Vision Title", 'vission_title', 'eng','profile_div',true,$result->id);
                        create_textfield_not_required("Vision Title", 'vission_title', 'arb','profile_div',true,$result->id);
                        create_textarea('Vision Description', 'vission_value', 'eng','profile_div',true,$result->id);
                        create_textarea('Vision Description', 'vission_value', 'arb','profile_div',true,$result->id);

                        create_textfield_not_required("Mission Title", 'mission_title', 'eng','profile_div',true,$result->id);
                        create_textfield_not_required("Mission Title", 'mission_title', 'arb','profile_div',true,$result->id);
                        create_textarea('Mission Description', 'mission_desc_value', 'eng','profile_div',true,$result->id);
                        create_textarea('Mission Description', 'mission_desc_value', 'arb','profile_div',true,$result->id);

                        create_textfield_not_required("Strategy Title", 'strategy_title', 'eng','profile_div',true,$result->id);
                        create_textfield_not_required("Strategy Title", 'strategy_title', 'arb','profile_div',true,$result->id);
                        create_textarea('Strategy Description', 'strategy_value', 'eng','profile_div',true,$result->id);
                        create_textarea('Strategy Description', 'strategy_value', 'arb','profile_div',true,$result->id);
                        create_textfield_not_required("Value Title", 'value_title', 'eng','profile_div',true,$result->id);
                        create_textfield_not_required("Value Title", 'value_title', 'arb','profile_div',true,$result->id);


							/*create_image('Founder Image', 'founder_image', '457&times;642', 'eng','profile_div',true,$result->id);
							create_textarea('Founder Description', 'desc_founder', 'eng','profile_div',true,$result->id);
							create_textarea('Founder Description', 'desc_founder', 'arb','profile_div',true,$result->id);
                        create_textfield_not_required('Founder Name','founder_name','eng','profile_div',true,$result->id);
                        create_textfield_not_required('Founder Name','founder_name','arb','profile_div',true,$result->id);
                        create_textfield_not_required('Position','founder_position','eng','profile_div',true,$result->id);
                        create_textfield_not_required('Position','founder_position','arb','profile_div',true,$result->id);
						*/
							// INTRODUCTION
							create_image($admin_lang['label']['content_banner_image'], 'image_intro', '2030&times;370', 'eng','introduction_div',true,$result->id);
							create_gallery($admin_lang['label']['gallery_intro'],'gallery_intro','400&times;300','eng','introduction_div',true, $result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_intro', 'eng','introduction_div',true,$result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_intro', 'arb','introduction_div',true,$result->id);
							
							
							//Security Printing
							create_image($admin_lang['label']['content_banner_image'], 'image_printing', '2030&times;370', 'eng','security_printing_div',true, $result->id);
							create_image($admin_lang['label']['security_logo'], 'image_printing_logo', '2030&times;370', 'eng','security_printing_div',true, $result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_printing', 'eng','security_printing_div',true, $result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_printing', 'arb','security_printing_div',true, $result->id);
							
							
							//Security Features
							create_textarea($admin_lang['label']['content_description'], 'desc_security', 'eng','security_features_div',true, $result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_security', 'arb','security_features_div',true, $result->id);
					
							//Our Approach
							create_textarea($admin_lang['label']['approach_desc'], 'desc_approach', 'eng','our_approach_div',true, $result->id);
							create_textarea($admin_lang['label']['approach_desc'], 'desc_approach', 'arb','our_approach_div',true, $result->id);
							
							//Media Center
							create_gallery('Gallery','gallery_media','400&times;300','eng','media_center_div',true, $result->id);
										
							//History
                            create_image($admin_lang['label']['content_banner_image'], 'history_banner_image', '2030&times;370', 'eng','history_div',true, $result->id);
                            create_textarea('Banner Description', 'history_banner_desc', 'eng','history_div',true, $result->id);
                            create_textarea('Banner Description', 'history_banner_desc', 'arb','history_div',true, $result->id);
                            create_textfield('Banner Description Sub-Heading', 'history_banner_desc_sub_heading', 'eng','history_div',true, $result->id);
                        create_textfield('Banner Description Sub-Heading', 'history_banner_desc_sub_heading', 'arb','history_div',true, $result->id);
                            create_textfield('Family Values Title', 'history_family_values_title', 'eng','history_div',true, $result->id);
                            create_textfield('Family Values Title', 'history_family_values_title', 'arb','history_div',true, $result->id);
                            create_textarea('Family Values Description', 'history_family_values_desc', 'eng','history_div',true, $result->id);
                            create_textarea('Family Values Description', 'history_family_values_desc', 'arb','history_div',true, $result->id);
							
							// Mission, Vision &amp; Values
							create_textarea($admin_lang['label']['mission_desc'], 'desc_miss', 'eng','mission_vision_div',true, $result->id);
							create_textarea($admin_lang['label']['mission_desc'], 'desc_miss', 'arb','mission_vision_div',true, $result->id);
							
							create_textarea($admin_lang['label']['values_desc'], 'desc_val', 'eng','mission_vision_div',true, $result->id);
							create_textarea($admin_lang['label']['values_desc'], 'desc_val', 'arb','mission_vision_div',true, $result->id);
							
							create_textarea($admin_lang['label']['vision_desc'], 'desc_vis', 'eng','mission_vision_div',true, $result->id);
							create_textarea($admin_lang['label']['vision_desc'], 'desc_vis', 'arb','mission_vision_div',true, $result->id);
					
							
							// Why Us
							create_textarea($admin_lang['label']['content_description'], 'desc_why_us', 'eng','why_fa_div',true,$result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_why_us', 'arb','why_fa_div',true,$result->id);

							// CEO Message
							create_image($admin_lang['label']['image'], 'ceo_image', '356&times;555', 'eng','ceo_message_div',true, $result->id);
                            create_textfield('CEO 1 Name','ceo_name','eng','ceo_message_div',true,$result->id);
                            create_textfield('CEO 1 Name','ceo_name','arb','ceo_message_div',true,$result->id);

                        create_image($admin_lang['label']['image'], 'ceo_2_image', '356&times;555', 'eng','ceo_message_div',true, $result->id);
                        create_textfield('CEO 2 Name','ceo_2_name','eng','ceo_message_div',true,$result->id);
                        create_textfield('CEO 2 Name','ceo_2_name','arb','ceo_message_div',true,$result->id);

                            // create_textfield('Title','ceo_title','eng','ceo_message_div',true,$result->id);
                            // create_textfield('Title','ceo_title','arb','ceo_message_div',true,$result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_our_mes', 'eng','ceo_message_div',true,$result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_our_mes', 'arb','ceo_message_div',true,$result->id);

                        // holding
                        create_image('Image', 'holding_logo', '271&times;216', 'eng','holding_div',true,$result->id);
                        create_textarea($admin_lang['label']['content_description'], 'desc_holding', 'eng','holding_div',true,$result->id);
                        create_textarea($admin_lang['label']['content_description'], 'desc_holding', 'arb','holding_div',true,$result->id);

							// Careers
							create_image('Image', 'careers_image', '556&times;511', 'eng','careers_div',true,$result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_career', 'eng','careers_div',true,$result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_career', 'arb','careers_div',true,$result->id);
                        create_textfield_not_required('Benefits Section Heading', 'benefits_heading', 'eng','careers_div',true,$result->id);
                        create_textfield_not_required('Benefits Section Heading', 'benefits_heading', 'arb','careers_div',true,$result->id);
                        create_textarea($admin_lang['label']['content_description'], 'desc_benefits', 'eng','careers_div',true,$result->id);
                        create_textarea($admin_lang['label']['content_description'], 'desc_benefits', 'arb','careers_div',true,$result->id);
                        create_textfield_not_required('Positions Section Heading', 'positions_heading', 'eng','careers_div',true,$result->id);
                        create_textfield_not_required('Positions Section Heading', 'positions_heading', 'arb','careers_div',true,$result->id);
                        create_textarea($admin_lang['label']['content_description'], 'desc_positions', 'eng','careers_div',true,$result->id);
                        create_textarea($admin_lang['label']['content_description'], 'desc_positions', 'arb','careers_div',true,$result->id);

                        // self Application
                        create_image('Image', 'selfapp_image', '510&times;552', 'eng','self_div',true,$result->id);

							// Privacy Policy
							/*create_image($admin_lang['label']['content_banner_image'], 'image_privacy', '2030&times;370', 'eng','privacy_policy_div',true,$result->id);*/
							create_textarea($admin_lang['label']['content_description'], 'desc_privacy', 'eng','privacy_policy_div',true,$result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_privacy', 'arb','privacy_policy_div',true,$result->id);
							
							// Terms & Conditions
							/*create_image($admin_lang['label']['content_banner_image'], 'image_term', '2030&times;370', 'eng','terms_div',true,$result->id);*/
							create_textarea($admin_lang['label']['content_description'], 'desc_terms', 'eng','terms_div',true,$result->id);
							create_textarea($admin_lang['label']['content_description'], 'desc_terms', 'arb','terms_div',true,$result->id);
							
							// Contact Us
                        create_image('Banner Image', 'contact_us_banner_image', '1920&times;623', 'eng','contact_us_div',true,$result->id);
							create_textarea($admin_lang['label']['content_address'], 'desc_contact', 'eng','contact_us_div',true,$result->id);
                            create_textarea($admin_lang['label']['content_address'], 'desc_contact', 'arb','contact_us_div',true,$result->id);  
                            create_location($admin_lang['label']['content_location'], 'maps_loc', 'contact_us_div', true,$result->id);
                            create_textfield($admin_lang['label']['phone'], 'phone','eng', 'contact_us_div', true,$result->id);
                            echo '<div class="uk-width-medium-1-2"></div>';
                            create_textfield($admin_lang['label']['office_email'], 'mail','eng', 'contact_us_div', true,$result->id);
                            echo '<div class="uk-width-medium-1-2"></div>';
							
						?>
						
                        <!--End Content Divs-->
						
					<?php 
					if($result->show_tags == 1){ ?>	
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_eng_meta_title']; ?></label>
                            <div class="uk-form-row">
                                <input type="text" cols="30" rows="4" class="md-input" id="meta_title_eng" name="meta_title_eng" value="<?php echo $result->meta_title_eng; ?>" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_arb_meta_title']; ?> </label>
                            <div class="uk-form-row">
                                <input cols="30" rows="4" class="md-input" name="meta_title_arb" id="meta_title_arb" value="<?php echo $result->meta_title_arb; ?>" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_eng_meta_desc']; ?></label>
                            <div class="uk-form-row">
                                <textarea class="md-input" id="meta_desc_eng" name="meta_desc_eng"><?php echo $result->meta_desc_eng;?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_arb_meta_desc']; ?></label>
                            <div class="uk-form-row">
                                <textarea class="md-input" id="meta_desc_arb" name="meta_desc_arb"><?php echo $result->meta_desc_arb;?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_eng_meta_keys']; ?></label>
                            <div class="uk-form-row">
                                <textarea class="md-input" id="meta_keywords_eng" name="meta_keywords_eng"><?php echo $result->meta_keywords_eng;?></textarea>
                                <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label><?php echo $admin_lang['label']['content_arb_meta_keys']; ?></label>
                            <div class="uk-form-row">
                                <textarea class="md-input" id="meta_keywords_arb" name="meta_keywords_arb"><?php echo $result->meta_keywords_arb;?></textarea>
                                <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                            </div>
                        </div>
					<?php } ?>	
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>
<?php if($this->uri->segment(6) != 'version_id'){ ?>
<div class="md-fab-wrapper">
    <a class="md-fab md-fab-primary md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_update_content']; ?>" href="javascript:void(0);" id="saveAbout">
        <i class="material-icons">&#xE161;</i>
    </a>
</div>
<?php } ?>
<!-- light box for image -->
<button class="md-btn" id="btn_thumb" data-uk-modal="{target:'#modal_lightbox'}" style="display:none;">Open</button>
<div class="uk-modal" id="modal_lightbox">
    <div class="uk-modal-dialog uk-modal-dialog-lightbox">
        <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
        <img id="img_url" src="" alt="" style="background: #f5f5f5;"/>
    </div>
</div>
<!-- end light box for image -->
</div>
</div>
<script language="javascript">
$(document).ready(function () {
	
	
	$(".sortmeGallery").sortable({ 
		update : function () {
			serial = $(this).closest('tbody.sortmeGallery').sortable('serialize');
			//alert(serial);
			
			var field_values = '';
			$('.uk-table tr').each(function() {
				field_values += this.id+'|';
			});
			var field_name = $(this).closest('tbody.sortmeGallery').attr("data-field-name");
			
			//alert(field_name);
			
			var newOrdering = field_values.replace("|", ",");
			
			//alert(newOrdering);
			
			//alert(field_values);
			
			$.ajax({
				type: "post",
				dataType: "JSON",
				url: "<?php echo base_url(); ?>ems/content/sortableImageGallery?field="+field_name+'&id=<?php echo $result->id; ?>',
				data: {field_values:field_values},
				success: function(data){
					//alert(data.field_values)
					$('#'+field_name).val(data.field_values);
				},
				error: function(){
					alert("theres an error with AJAX");
				}
			});
		}
	});
		//$(".PageSizeContainer div:last-child").click();
		
    $('#delete').click(function () {
        if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {
            var id = <?php echo $result->id; ?>;
            url = '<?php echo base_url(); ?>ems/content/delete';
            $.post(url, {id: id}, function (data) {
              return true;
            });
            $(location).attr('href', '<?php echo base_url(); ?>' + 'ems/content/addNewPage');
        }
    });
    $('#saveAbout').click(function () {
        $(".page_loader").show();
		$("#updateAbout").attr("action", "<?php echo base_url();?>ems/content/update/id/<?php echo $result->id; ?>");
		$("#updateAbout").attr("target", "");
		$('#updateAbout').submit();
        $(".required").each(function(){
            if($(this).val() == ''){
                setTimeout(function(){
                    $(".page_loader").hide();
                },500);
            }
        });
		
	});
	
    $("#updateAbout").validate();
    setInterval(function(){$('#success_msg').html(''); }, 3000);
    
	/* 
		$('.img_thumb').click(function () {
			$('#img_url').attr("src","");
			$('#btn_thumb').click();
			var img_src = $(this).attr("src");
			$('#img_url').attr("src",img_src);
		});
	*/
	
	function check_required_images(){
		
	}

});
</script>

<script type="text/javascript">
	/* $(document).on('click','.removeImageAdded',function (){
		if(confirm("Are you sure to delete image? \nNote: After deleting you should need to save form!")){
			var img = $(this).attr('data-id');
			var field = $(this).attr('data-id-field');
			var vaule = new Array();
			var new_val = $("#"+field).val();
			//new_val = new_val.split(",");
			
			new_val = new_val.replace(img, '')
			var final_value = new_val.replace(/,\s*$/, "");
			$("#"+field).val(final_value);
			$(this).parent().parent().remove();
		}

	}); */
</script>