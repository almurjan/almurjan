<script>
// JS API Code

(function(w,d,s,g,js,fs)
{
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));
</script>

<script>

gapi.analytics.ready(function() {

  /**
   * Authorize the user immediately if the user has already granted access.
   * If no access has been created, render an authorize button inside the
   * element with the ID "embed-api-auth-container".
   */
  gapi.analytics.auth.authorize({
    container: 'embed-api-auth-container',
    clientid: '116311478161629329447',
	serverAuth: {
    access_token: '<?php echo $analytics;?>',

  }
  });

    /**
   * Create a new ViewSelector instance to be rendered inside of an
   * element with the id "view-selector-container".
   */


  var dataChart2 = new gapi.analytics.googleCharts.DataChart({
    query: {
	  'ids': 'ga:<?php echo $ga_view_id; ?>',
      'metrics': 'ga:sessions',
      'dimensions': 'ga:country',
      'start-date': '30daysAgo',
      'end-date': 'yesterday',
      'max-results': 6,
      'sort': '-ga:sessions'
    },
    chart: {
      container: 'chart_divbConfig_geo',
      type: 'GEO',
      options: {
        width: '100%',
        pieHole: 4/9
      }
    }
  });
  dataChart2.execute();

  var dataChartTrafic = new gapi.analytics.googleCharts.DataChart({
    query: {
       'ids': 'ga:<?php echo $ga_view_id; ?>',
	  'metrics': 'ga:sessions',
      'dimensions': 'ga:date',
      'start-date': '30daysAgo',
      'end-date': 'yesterday'
    },
    chart: {
      container: 'chart-containerTrafic',
      type: 'LINE',
      options: {
        width: '100%'

      }
    }
  });

 dataChartTrafic.execute();


    var dataChartBar = new gapi.analytics.googleCharts.DataChart({
  reportType: 'ga',
  query: {
    'ids': 'ga:<?php echo $ga_view_id; ?>',
	'dimensions': 'ga:date',
    'metrics': 'ga:sessions',
    'start-date': '30daysAgo',
    'end-date': 'yesterday',
  },
  chart: {
   container:'chart_divcConfig_bar',
    type: 'COLUMN',
	options: {
        width: '100%'
      }

  }
});
dataChartBar.execute();


    var dataChartTraficSourceTable = new gapi.analytics.googleCharts.DataChart({
  reportType: 'ga',
  query: {
    'ids': 'ga:<?php echo $ga_view_id; ?>',
	'dimensions': 'ga:source',
    'metrics': 'ga:sessions',
    'start-date': '30daysAgo',
    'end-date': 'yesterday',
  },
  chart: {
   container:'chart_divdConfig_table',
    type: 'TABLE',
	options: {
        width: '92%'

      }

  }
});

   dataChartTraficSourceTable.execute();


  /**
   * Render the dataChart on the page whenever a new view is selected.
   */


});
</script>

<div id="page_content">
    <div id="page_content_inner">
        <div id="embed-api-auth-container">
            <?php $admin_lang = check_admin_lang(); ?>
        </div>
        <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
            <div class="uk-width-medium-1-1">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a uk-margin-bottom autoTooltip" title="<?php echo $admin_lang['label']['tooltip_traffic']; ?>"><?php echo $admin_lang['label']['dashboard_traffic']; ?></h3>
                        <div class="uk-overflow-container">
                            <div class="trafficMap" id="chart_divaConfig"> </div>

                            <div id="chart-containerTrafic">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a uk-margin-bottom autoTooltip" title="<?php echo $admin_lang['label']['tooltip_bar']; ?>"><?php echo $admin_lang['label']['dashboard_bar']; ?></h3>
                        <div id="view-selector-bar"></div>
                        <div class="mapOverview" id="chart_divcConfig_bar"> </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a uk-margin-bottom autoTooltip" title="<?php echo $admin_lang['label']['tooltip_map_overview']; ?>"><?php echo $admin_lang['label']['dashboard_map_overview']; ?></h3>
                        <div id="view-selector-container"></div>
                        <div class="mapOverview" id="chart_divbConfig_geo"> </div>
                     </div>
                </div>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
            <div class="uk-width-medium-1-1">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a uk-margin-bottom autoTooltip" title="<?php echo $admin_lang['label']['tooltip_src_overview']; ?>"><?php echo $admin_lang['label']['dashboard_sources_overview']; ?></h3>
                        <div class="uk-overflow-container">
                            <div id="view-selector-traficsource"></div>
                            <div class="trafficOverview" id="chart_divdConfig_table">
                            </div>
                         </div>
                    </div>
                 </div>
             </div>
        </div>
        <?php if (check_permission(5, 'read', false)) { ?>
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-content">
                            <h3 class="heading_a uk-margin-bottom autoTooltip" title="<?php echo $admin_lang['label']['tooltip_log']; ?>"><?php echo $admin_lang['label']['dashboard_log']; ?></h3>
                            <div class="uk-overflow-container">
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <tbody>
                                    <?php  $i=0;  foreach($data as $result=>$val) {  ?>
                                        <tr class="uk-table-middle">
                                            <td class="uk-width-2-10 uk-text-nowrap">
                                                <!--GrayRow-->
                                                <span class="uk-badge">
                                              <?php echo $val['log_uname'].' '.$val['log_comments'].' <strong>'.$val['log_module'].'</strong>'.' '.date('Dd M, Y  H:i:s',strtotime($val['log_date'])); ?>
                                            </span>
                                            </td>
                                        </tr>
                                        <?php $i++;}?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>
</div>


<!--middle_content_areainner-->