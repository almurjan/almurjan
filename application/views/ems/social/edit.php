<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
    <!--<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h2 id="product_edit_name">Social</h2>
        <span class="uk-text-muted uk-text-upper uk-text-small"></span>
    </div>-->
    <div id="page_content_inner">
        <?php echo form_open_multipart('ems/social/save',array('method'=>'post','id'=>'socialLinks'));?>
        <input type="hidden" value="<?php echo $res->pub_status; ?>" id="pub_val" name="pub_val">
        <div class="md-card">
            <div class="md-card-content"><h3 class="heading_a"><b><?php echo $admin_lang['label']['social_settings']; ?></b></h3><br>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2" style="display: none;"><label><?php echo $admin_lang['label']['social_facebook']; ?></label>
                        <div class="uk-form-row">
                            <input class="md-input" id="soc_fb" name="soc_fb" value="<?php echo $res->soc_fb;?>">
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['social_youtube']; ?></label>
                        <div class="uk-form-row">
                            <input class="md-input" id="soc_you" name="soc_you" value="<?php echo $res->soc_you; ?>">
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2" style="display: none;"><label><?php echo $admin_lang['label']['social_snapchat']; ?></label>
                        <div class="uk-form-row">
                            <input class="md-input" id="soc_snapchat" name="soc_snapchat" value="<?php echo $res->soc_snapchat;?>">
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['social_twitter']; ?></label>
                        <div class="uk-form-row">
                            <input class="md-input" id="soc_tw" name="soc_tw" value="<?php echo $res->soc_tw; ?>">
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['social_instagram']; ?></label>
                        <div class="uk-form-row">
                            <input class="md-input" id="soc_insta" name="soc_insta" value="<?php echo $res->soc_insta; ?>">
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2" style="display: none;"><label><?php echo $admin_lang['label']['social_google_plus']; ?></label>
                        <div class="uk-form-row">
                            <input class="md-input" id="soc_google" name="soc_google" value="<?php echo $res->soc_google; ?>">
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2"><label><?php echo $admin_lang['label']['social_linkedin']; ?></label>
                        <div class="uk-form-row">
                            <input class="md-input" id="soc_lin" name="soc_lin" value="<?php echo $res->soc_lin; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_update_content']; ?>" href="javascript:void(0);" id="saveSocial">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function () {

        $('#saveSocial').click(function(){
            $(".page_loader").show();
            $('#socialLinks').submit();
        });
        $("#socialLinks").validate();
        setInterval(function(){$('#show_error').html(''); }, 1500);
    });

</script>
<!--End New HTML-->