<?php $admin_lang = check_admin_lang(); ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){

    $('a.delete').click(function (){
        if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {
            $(".page_loader").show();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url(); ?>' + 'ems/reservations/delete',
                data: {id: id},
                success: function (result) {
                }
            });
            location.reload();
        }
    });

    $('a.export').click(function (){
        var id = [];
        var input_fields = '';
        id = $(this).attr('data-id');
        input_fields = input_fields + '<input type="hidden" name="id[]" value="'+id+'">';
        input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="1" />';
        $("#excel_report").html(input_fields);
        $("#excel_report").submit();
    });

    $('#export_to_excel_All').click(function () {
        var input_fields = '';
        input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="0" />'
        $("#excel_report").submit();

    });

});
</script>
<!--New HTML-->
<form id="excel_report" method="post" action="<?php echo base_url(); ?>ems/users/export_excel">
</form>
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['users']; ?></h1>
        <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
    </div>
    <div id="page_content_inner">
        <a href="javascript:void(0);" id="export_to_excel_All" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_export_all']; ?>"><?php echo $admin_lang['label']['export_all']; ?></a>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th><?php echo $admin_lang['label']['Sr']; ?></th>
                                    <th><?php echo $admin_lang['label']['full_name']; ?></th>
                                    <th><?php echo $admin_lang['label']['email']; ?></th>
                                    <th><?php echo $admin_lang['label']['mobile']; ?></th>
                                    <th><?php echo $admin_lang['label']['interest']; ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($res){
                                    $i = 1;
                                    foreach ($res as $result => $val) {
                                        if($i == 1){
                                            $tclass = 'autoTooltip';
                                        }else{
                                            $tclass = 'tooltip';
                                        }
										
										?>
                                        <tr class="<?php echo $val['id'] ?>">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $val['user_name']; ?></td>
                                            <td><?php echo $val['email']; ?></td>
                                            <td><?php echo $val['mobile_no']; ?></td>
                                            <td>
											<?php 
												$lang = $admin_lang['admin_lang'];
												if($val['role'] == 1){
													$interests = getUserInterests($val['user_id']);
													$interest_name_concat = '';
													foreach($interests as $interest_id){
														$interest_row = getCategoryName($interest_id->interest_id);
														$interest_name_concat.= ($lang == 'eng' ? $interest_row->eng_name : $interest_row->arb_name) . ', ';
													}
													echo trim(trim($interest_name_concat),',');
												}
												else{
													echo '-';
												}
											?>
											</td>
                                            <!--<td><?php //echo date("Y-m-d", strtotime($val['created_at']));?></td>-->
                                            
                                        </tr>
                                    <?php $i++; } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--End New HTML-->


