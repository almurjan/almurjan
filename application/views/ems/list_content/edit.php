<style>
	.imageDiv > .uk-width-medium-1-2{
		display: inline-block !important;
	}
	#sortmeGallery tr{
		display: inline-block;
	}
</style>
<?php 

$admin_lang = check_admin_lang(); 
$tpl_id = $result->id;
$page_details = get_content_data($tpl_id);

$listing = $this->uri->segment(6);
/*if ($listing > 1) {
    $listing = $listing - 1;
}*/

?>

<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo getPagePath($result->id,$admin_lang['admin_lang']) .' - '. $admin_lang['label']['edit']; ?></h1>
        <!--<div class="modifiedDate">Last modified <?php /*echo date("F j, Y, g:i a",strtotime($result->content_updated_at)); */?> </div>-->
        <div id="success_msg" style="text-align:center;color:#7cb342;">
            <?php
            if (validation_errors()) {
                echo _erMsg(validation_errors());
            } if ($this->session->flashdata('message')) {
                echo $this->session->flashdata('message');
            }
            ?>
        </div>
		<div id="err_msg_dates" style="text-align:center;color:red;"></div>
    </div>
	
    <div id="page_content_inner">
        <?php
        if($result->tpl == 'other_sectors' && $result->listing_level == 1){ ?>

        <a href="<?php echo base_url();?>ems/list_content/index/<?php echo $result->id.'/2'; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>"> <?php echo 'Sectors Listing'; ?> </a>

        <?php }elseif($result->tpl == 'other_sectors' && $result->listing_level == 2){?>
            <a href="<?php echo base_url();?>ems/list_content/index/<?php echo $result->id.'/3'; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>"> <?php echo 'Projects Listing'; ?> </a>

        <?php }

			if($result->tpl == 'our_approach' && $result->listing_level == 1){ ?>
				
				<a href="<?php echo base_url();?>ems/list_content/index/<?php echo $result->id; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>"> <?php echo 'Approach listing'; ?> </a>
				
			<?php }
		?>
		<?php 
			if($result->tpl == 'our_services' && $result->listing_level == 1){ ?>
				
				<a href="<?php echo base_url();?>ems/list_content/index/<?php echo $result->id; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>"> <?php echo 'Products And Services'; ?> </a>
				
			<?php }
		?>
	
	<?php if($result->tpl == 'media_center'){ ?>
        <a href="<?php echo base_url();?>ems/list_content/index_video/<?php echo $result->id; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>">
            <?php
            if ($result->tpl == 'media_center')
                echo 'Album Videos';
            ?>
		</a>	
    <?php } ?>
	
        <?php
        $blog_id = getPageIdbyTemplate('blog');
        if($result->parant_id == $blog_id){ ?>
            <a href="<?php echo base_url();?>ems/list_content/index/<?php echo $result->id; ?>" class="md-btn autoTooltip md-btn-primary" title="<?php echo $admin_lang['label']['tooltip_listing']; ?>">
                <?php
                if ($result->parant_id == $blog_id)
                    echo $admin_lang['label']['list_content_blogs'];
                ?>
            </a>
        <?php } ?>
        <?php if($this->uri->segment(6) != '' && $result->tpl != 'home' && $result->tpl != 'careers' && $result->tpl != 'murjan_holding' && $result->tpl != 'other_sectors' && $result->tpl != 'bod' && $result->tpl !='murjan_group' && $result->tpl !='profile' && $result->tpl !='history'){ ?>
            <a href="<?php echo base_url().'ems/content/versionsListing/'.$page_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_delete']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <?php }else{ ?>
            <a href="<?php echo base_url();?>ems/content/versionsListing/<?php echo $result->id; ?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_versions']; ?>"><?php echo $admin_lang['label']['content_versions']; ?></a>
            <?php
            if($result->tpl == 'other_sectors' && $page_details->listing_level == '1'){?>
                <a href="<?php echo base_url().'ems/list_content/index/'.$result->parant_id.'/0';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
            <?php }elseif($result->tpl == 'other_sectors' && $page_details->listing_level == '2'){?>
                <a href="<?php echo base_url().'ems/list_content/index/'.$result->parant_id.'/2';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
            <?php }elseif($result->tpl == 'other_sectors' && $page_details->listing_level == '3'){?>
                <a href="<?php echo base_url().'ems/list_content/index/'.$result->parant_id.'/3';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
            <?php }
            elseif($result->tpl == 'careers' && $listing == '1'){?>
                <a href="<?php echo base_url().'ems/list_content/index/'.$result->parant_id.'/1';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
            <?php }elseif($result->tpl == 'careers' && $listing == '0'){?>
                <a href="<?php echo base_url().'ems/list_content/index/'.$result->parant_id.'/0';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
            <?php }elseif($result->tpl == 'profile' && $listing == '1'){?>
                <a href="<?php echo base_url().'ems/list_content/index/'.$result->parant_id.'/1';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
            <?php }else{?>
                <a href="<?php echo base_url().'ems/list_content/index/'.$result->parant_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
            <?php } ?>

        <?php } ?>
		
		
		
		
		<!--<a href="javascript:void(0);" id="preview" class="md-btn autoTooltip" title="<?php /*echo $admin_lang['label']['tooltip_preview']; */?>"><?php /*echo $admin_lang['label']['content_preview']; */?></a>-->
        <?php
        if($result->tpl == 'other_sectors'){?>
            <?php echo form_open_multipart('ems/content/update/id/' . $result->id, array('method' => 'post', 'id' => 'updateAbout')); ?>
        <?php }else{?>
            <?php echo form_open_multipart('ems/content/update/id/' . $result->id, array('method' => 'post', 'id' => 'updateAbout','onsubmit' => 'return checkUniquePageTitle(1)')); ?>
        <?php } ?>
        <input type="hidden" value="<?php echo $result->eng_title; ?>" id="page_title_before_change" name="page_title_before_change">
        <input type="hidden" value="<?php echo $result->pub_status; ?>" id="pub_status" name="pub_status">
        <input type="hidden" value="<?php echo $result->id; ?>" id="gid" name="gid">
        <input type="hidden" value="<?php echo $page_id; ?>" id="page_id" name="page_id">
        <input type="hidden" value="<?php echo $template; ?>" id="tpl" name="tpl">
        <input type="hidden" value="<?php echo $result->parant_id; ?>" id="tpl_id" name="tpl_id">
		
		<?php if($template == 'other_sectors' || $template == 'our_services' || ($template == 'profile' && $listing > 0))
		{

			$val = ($page_details->listing_level > 1 ?$page_details->listing_level: 1);
			echo '<input type="hidden" value="'.$val.'" id="listing_level" name="listing_level">';
		}
		if($template=='home'){
		    $sub_title_req='';
		    $req='';
        }else{
            $sub_title_req= 'required';
            $req='*';
        }
		?>
		
        <input type="hidden" value="<?php echo $template;?>" id="tpl_name" name="tpl_name">
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                <div class="md-card">
                    <div class="md-card-toolbar">
                    </div>
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_title']; ?>*</label>
                                <div class="uk-form-row">
                                    <input type="text" cols="30" rows="4" class="md-input required" id="eng_title" name="eng_title" value="<?php echo $result->eng_title; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_title']; ?>*</label>
                                <div class="uk-form-row">
                                    <input cols="30" rows="4" class="md-input required" name="arb_title" id="arb_title" value="<?php echo $result->arb_title; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_sub_title'].' '.$req; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input <?php echo $sub_title_req ?>" id="eng_sub_title" name="eng_sub_title" value="<?php echo $result->eng_sub_title; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_sub_title'].' '.$req; ?></label>
                                <div class="uk-form-row">
                                    <input class="md-input <?php echo $sub_title_req ?>" name="arb_sub_title" id="arb_sub_title" value="<?php echo $result->arb_sub_title; ?>" />
                                </div>
                            </div>
							
                            <!--Start Content Divs-->
                            <!--Last parameter after lang is for parent class which is using for hide & show content
                            (for list_content leave this empty)-->
                            <!--For textarea field there is words limit digit after comma which is optional-->
							
							 <?php
								
								//Home
								if ($template == 'home') {
									if ($page_details->selectSlideOptions == '2') {
										$image_selected = '';

										$video_selected = 'selected = selected';
									}
									if ($page_details->selectSlideOptions == '1') {
										$image_selected = 'selected = selected';
										$video_selected = '';
									}
									echo '
									<div class="uk-width-medium-1-2">
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="selectSlideOptions" class="">
											<option value= "1" ' . $image_selected . '>Slider Image</option>
										<option value= "2" ' . $video_selected . '>Slider Video</option>                                
									</select>
									</div>
									<div class="uk-width-medium-1-2"></div>
									';

									echo "<div class='uk-width-medium-1-1 imageDiv'> ";
									create_image($admin_lang['label']['thumb_image'], 'home_thumbImage', '1300&times;1300', 'eng','',true,$result->id);
									create_textarea($admin_lang['label']['image_select'], 'home_sliderText', 'eng', '',true,$result->id);
									create_textarea($admin_lang['label']['image_select'], 'home_sliderText', 'arb', '',true,$result->id);
									echo '</div>';
									/*echo '<div class="uk-width-medium-1-2"></div>';*/
								
									echo "<div class='uk-width-medium-1-1 videoDiv'> ";
									upload_video($admin_lang['label']['content_choose_video'], 'file', '', true,$result->id);	
									//create_image($admin_lang['label']['video_thumb'], 'video_thumb', '200&times;200', 'eng','',true,$result->id);
									//create_externallink($admin_lang['label']['video'], 'home_videoLink1', 'eng', '',true,$result->id);
									echo "</div> ";
                                    create_externallink($admin_lang['label']['content_external_link'], 'home_sliderLink', 'eng', '', true,$result->id);
								}
								
								//History
								if($template == 'history') {
                                    create_image($admin_lang['label']['thumb_image'],'history_listing_thumb','198&times;228','eng','',true,$result->id);
                                    create_textfield('History Title','history_listing_title','eng','',true,$result->id);
                                    create_textfield('History Title','history_listing_title','arb','',true,$result->id);
                                    create_textarea($admin_lang['label']['content_description'],'history_listing_desc','eng','',true,$result->id);
                                    create_textarea($admin_lang['label']['content_description'],'history_listing_desc','arb','',true,$result->id);
									/*for($i=1990; $i<=date('Y'); $i++){
										if($page_details->year == $i){
											$selected = 'selected = selected';
										}
										else{
											$selected = '';
										}
										$all_options .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
									}
									
									echo' <div class="uk-width-medium-1-2">
									<label>'.'Select Year'.'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="year" class="">
										<option value="">'.$admin_lang['label']['list_content_select_option'].'</option>
										'.$all_options.'
									</select>
									</div>'; */
									/* create_datepicker($admin_lang['label']['date'],'date','eng','',true,$result->id); */
									// echo '<div class="uk-width-medium-1-2"></div>';
							
								}
								
								//Security Features
								if($template == 'security_features') {
									create_image($admin_lang['label']['thumb_image'],'thumb_security','200&times;200','eng','',true, $result->id);
								}

                             //companies map pin
                             if($template == 'companies_map') {
                                 create_textfield($admin_lang['label']['companies_pin_location_x'], 'compnies_pin_xposition', 'eng', '',true, $result->id);
                                 create_textfield($admin_lang['label']['companies_pin_location_y'], 'compnies_pin_yposition', 'eng', '',true, $result->id);

                             }

								// Our Approach
								
								
								if($template == 'our_approach' && $result->listing_level == 1) {
									create_image($admin_lang['label']['category_logo'],'approach_category_logo','200&times;200','eng','',true, $result->id);
								}
								//FAQS
								if($template == 'faqs') {
									
									// Final PDF code with name showing.
									//upload_file_half_div($admin_lang['label']['choose_file'],'file','',true, $result->id);
									//upload_file_arb_half_div($admin_lang['label']['choose_file'],'file_arb','',true, $result->id);
									create_textarea($admin_lang['label']['faq_ques'],'question','eng','',true, $result->id);
									create_textarea($admin_lang['label']['faq_ques'],'question','arb','',true, $result->id);									
									create_textarea($admin_lang['label']['faq_ans'],'answer','eng','',true, $result->id);
									create_textarea($admin_lang['label']['faq_ans'],'answer','arb','',true, $result->id);
								}
								
								if($template == 'ceo_clients') {
									create_image($admin_lang['label']['thumb_image'],'thumb','200&times;200','eng','',true,$result->id);
								}

                             #todo::profile section new added
                             if($template == 'profile' && $result->companies_listing == 1){
                                 create_image($admin_lang['label']['image'],'company_logo','130&times;130','eng','',true,$result->id);
                                 create_textarea($admin_lang['label']['content_description'],'company_desc','eng','',true,$result->id);
                                 create_textarea($admin_lang['label']['content_description'],'company_desc','arb','',true,$result->id);
                             }elseif($template == 'profile' && $result->companies_listing == 0) {
                                 create_image($admin_lang['label']['image'],'value_image','55&times;55','eng','',true,$result->id);
                                 create_textarea($admin_lang['label']['content_description'],'value_desc','eng','',true,$result->id);
                                 create_textarea($admin_lang['label']['content_description'],'value_desc','arb','',true,$result->id);
                             }

                             if($template == 'csr'){
                                 //csr
                                 create_image($admin_lang['label']['thumb_image'], 'csr_thumb_image', '507&times;507', 'eng','',true,$result->id);
                                 create_textarea($admin_lang['label']['content_description'], 'csr_desc', 'eng','',true,$result->id);
                                 create_textarea($admin_lang['label']['content_description'], 'csr_desc', 'arb','',true,$result->id);
                             }

                             if($template == 'bod') {
                                 $selected_cat= $result->category;
                                 if($selected_cat==1){
                                     $selected_cat_class1='selected = selected';
                                     $selected_cat_class2='';
                                     $selected_cat_class3='';
                                     $selected_cat_class4='';
                                     $selected_cat_class5='';
                                     $selected_cat_class6='';
                                 }elseif($selected_cat==2){
                                     $selected_cat_class2='selected = selected';
                                     $selected_cat_class1='';
                                     $selected_cat_class3='';
                                     $selected_cat_class4='';
                                     $selected_cat_class5='';
                                     $selected_cat_class6='';
                                 }elseif($selected_cat==3){
                                     $selected_cat_class3='selected = selected';
                                     $selected_cat_class1='';
                                     $selected_cat_class2='';
                                     $selected_cat_class4='';
                                     $selected_cat_class5='';
                                     $selected_cat_class6='';
                                 }elseif($selected_cat==4){
                                     $selected_cat_class4='selected = selected';
                                     $selected_cat_class1='';
                                     $selected_cat_class2='';
                                     $selected_cat_class3='';
                                     $selected_cat_class5='';
                                     $selected_cat_class6='';
                                 }elseif($selected_cat==5){
                                     $selected_cat_class5='selected = selected';
                                     $selected_cat_class1='';
                                     $selected_cat_class2='';
                                     $selected_cat_class3='';
                                     $selected_cat_class4='';
                                     $selected_cat_class6='';
                                 }elseif($selected_cat==6){
                                     $selected_cat_class6='selected = selected';
                                     $selected_cat_class1='';
                                     $selected_cat_class2='';
                                     $selected_cat_class3='';
                                     $selected_cat_class4='';
                                     $selected_cat_class5='';
                                 }

                                 $category_name1 =  ($admin_lang['admin_lang'] == 'eng' ? 'Board of Directors' : 'أعضاء مجلس الإدارة');
                                 $category_name2 =  ($admin_lang['admin_lang'] == 'eng' ? 'Executive Committee' : 'اللجنة التنفيذية');
                                 $category_name3 =  ($admin_lang['admin_lang'] == 'eng' ? 'Leadership Team' : 'فريق القيادة');
                                 $category_name4 =  ($admin_lang['admin_lang'] == 'eng' ? 'Investment Advisory Board' : 'المجلس الاستشاري للاستثمار');
                                 $category_name5 =  ($admin_lang['admin_lang'] == 'eng' ? 'Management Team' : 'فريق الإدارة');
                                 $category_name6 =  ($admin_lang['admin_lang'] == 'eng' ? 'Executive Team' : 'الفريق التنفيذي');
                                 echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['categories'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="category" class="">
										<option value="" >Select Category</option>
										<option value="1" '.$selected_cat_class1.'>'.$category_name1.'</option>
										<option value="2" '.$selected_cat_class2.'>'.$category_name2.'</option>
										<option value="3" '.$selected_cat_class3.'>'.$category_name3.'</option>
										<option value="4" '.$selected_cat_class4.'>'.$category_name4.'</option>
										<option value="5" '.$selected_cat_class5.'>'.$category_name5.'</option>
										<option value="6" '.$selected_cat_class6.'>'.$category_name6.'</option>
									</select>
									</div>';
                                 create_image($admin_lang['label']['image'],'dob_image','263&times;400','eng','',true,$result->id);
                                 create_textfield('Designation','designation','eng','',true,$result->id);
                                 create_textfield('Designation','designation','arb','',true,$result->id);
                                 create_textarea('Description','dob_dec','eng','',true,$result->id);
                                 create_textarea('Description','dob_dec','arb','',true,$result->id);
                             }
                             if($template == 'murjan_group') {
                                 create_image($admin_lang['label']['image'],'group_image','150&times;150','eng','',true,$result->id);
                                 create_textarea('Description','group_dec','eng','',true,$result->id);
                                 create_textarea('Description','group_dec','arb','',true,$result->id);
                             }
                             if($template == 'landmarks') {

                                 echo '<div class="uk-width-medium-1-2 uk-grid-margin">
                                            <label">'.$admin_lang['label']['country_name'].'
                                                <div class="uk-form-row">
                                                    <div class="md-input-wrapper md-input-filled">
                                                     <input type="text" name="country_name" class="md-input valid" value="'.$page_details->country_name.'" aria-invalid="false">
                                                     <span class="md-input-bar "></span>
                                                    </div>
                                                </div>
                                            </label">
                                        </div>';
                                 echo '<div class="uk-width-medium-1-2 uk-grid-margin">
                                            <label">'.$admin_lang['label']['country_name_ar'].'
                                                <div class="uk-form-row">
                                                    <div class="md-input-wrapper md-input-filled">
                                                     <input type="text" name="country_name_ar" class="md-input valid" value="'.$page_details->country_name_ar.'" aria-invalid="false">
                                                     <span class="md-input-bar "></span>
                                                    </div>
                                                </div>
                                            </label">
                                        </div>';

                                 echo' <div class="uk-width-medium-1-2"></div>';
                                 ?>
                                 <div class="uk-width-medium-1-2">
                                     <label><?php echo $admin_lang['label']['select_category']?></label>
                                     <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="category_id" class="">
                                         <option value="" >Select Category</option>
                                         <?php
                                         $categories_data = get_catogries_data(0);
                                         $category_html = '';
                                         foreach($categories_data as $category){
                                             if($page_details->category_id==$category->category_id){
                                                 $cat_selected = 'selected';
                                             }else{
                                                 $cat_selected = '';
                                             }
                                             if($admin_lang['admin_lang']=='eng'){
                                                 $title_cat = $category->category_title;
                                             }else{
                                                 $title_cat = $category->category_title_ar;
                                             }?>
                                             <option <?php echo $cat_selected; ?> value="<?php echo $category->category_id;?>" ><?php echo  $title_cat;?></option>
                                         <?php } ?>
                                     </select>
                                 </div>
                                 <?php
                                 create_image('Logo','landmarks_logo','570&times;420','eng','',true,$result->id);
                                 create_image($admin_lang['label']['thumb_image'],'landmarks_thumb','600&times;600','eng','',true,$result->id);
                                 create_gallery($admin_lang['label']['image'],'landmarks_image','600&times;600','eng','',true,$result->id);
                                 create_textarea('Description','landmarks_dec','eng','',true,$result->id);
                                 create_textarea('Description','landmarks_dec','arb','',true,$result->id);
                             }
                             if($template == 'murjan_holding') {
                                 create_image('Logo','holding_logo','170&times;170','eng','',true,$result->id);
                                 create_gallery($admin_lang['label']['gallary'],'holding_slider','325&times;118','eng','',true,$result->id);
                                 $is_show= content_detail('is_show_home',$result->id);
                                 if($is_show == 1){
                                     $is_show_home0 = '';
                                     $is_show_home1 = 'selected = selected';
                                 }
                                 if($is_show == 0){
                                     $is_show_home0 = 'selected = selected';
                                     $is_show_home1 = '';
                                 }

                                 echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_show_home" class="">
										<option value="0" '.$is_show_home0.'>No</option>
										<option value="1" '.$is_show_home1.'>yes</option>
									</select>
									</div>';
                                 create_externallink('Link','holding_link','eng','',true,$result->id);//ahmed
                                 create_textarea('Description','holding_dec','eng','',true,$result->id);
                                 create_textarea('Description','holding_dec','arb','',true,$result->id);
                             }
                             if($template == 'other_sectors' && $val == 1) {
                                 create_textfield_not_required('Short Description','other_sectors_cat_desc_1','eng','',true,$result->id);
                                 create_textfield_not_required('Short Description','other_sectors_cat_desc_1','arb','',true,$result->id);
                                 create_textarea('Description 2','other_sectors_cat_desc_2','eng','',true,$result->id);
                                 create_textarea('Description 2','other_sectors_cat_desc_2','arb','',true,$result->id);
                                 create_image('Icon (For Sectors Page)','other_sectors_cat_icon','50&times;50','eng','',true,$result->id);

                                 create_image('Icon (For Home Page)','other_sectors_cat_hover_icon','50&times;50','eng','',true,$result->id);
                             }
                             if($template == 'other_sectors' && $val == 2) {
                                 create_image($admin_lang['label']['logo'],'other_sectors_logo','185&times;185','eng','',true,$result->id);
                                  $is_show= content_detail('is_show_home',$result->id);
                                 if($is_show == 1){
                                     $is_show_home0 = '';
                                     $is_show_home1 = 'selected = selected';
                                 }
                                 if($is_show == 0){
                                     $is_show_home0 = 'selected = selected';
                                     $is_show_home1 = '';
                                 }

                                 echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_show_home" class="">
										<option value="0" '.$is_show_home0.'>No</option>
										<option value="1" '.$is_show_home1.'>yes</option>
									</select> 
									</div>';
                                 $sortingon_home = $page_details->sectors_logo_sorting;
                                 create_textfield_not_required($admin_lang['label']['sorting_sector_logo_for_home'],'sectors_logo_sorting','eng','',true,'',$sortingon_home);
                                  create_image($admin_lang['label']['sector_logo_for_home'],'other_sectors_logo_for_home','125&times;80','eng','',true,$result->id);
                                 create_gallery($admin_lang['label']['gallery_images'],'other_sectors_discover_more_slider','626&times;530','eng','',true,$result->id);
                                 create_image($admin_lang['label']['slider_image'],'other_sectors_slider_image','950&times;950','eng','',true,$result->id);
                                 /*$slider_number= content_detail('slider_number',$result->id);
                                 if($slider_number == 1){
                                     $slider1 = 'selected = selected';
                                     $slider2 = '';
                                     $slider3 = '';
                                 }
                                 elseif($slider_number == 2){
                                     $slider2 = 'selected = selected';
                                     $slider1 = '';
                                     $slider3 = '';
                                 }elseif($slider_number == 3){
                                     $slider3 = 'selected = selected';
                                     $slider1 = '';
                                     $slider2 = '';
                                 }else{
                                     $slider2 = '';
                                     $slider1 = '';
                                     $slider3 = '';
                                 }

                                 echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['slider_number'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="slider_number" class="">
										<option '.$slider1.' value="1" >1</option>
										<option '.$slider2.' value="2" >2</option>
										<option '.$slider3.' value="3" >3</option>
									</select> 
									</div>';*/
                                 create_externallink('Link','other_sectors_link','eng','',true,$result->id);//ahmed
                                 create_textarea('Description','other_sectors_dec','eng','',true,$result->id);
                                 create_textarea('Description','other_sectors_dec','arb','',true,$result->id);
                                 create_textarea('Company Profile','other_sectors_company_profile_desc','eng','',true,$result->id);
                                 create_textarea('Company Profile','other_sectors_company_profile_desc','arb','',true,$result->id);
                                 create_textfield_not_required($admin_lang['label']['ceo_section_heading'],'other_sectors_ceo_heading','eng','',true,$result->id);
                                 create_textfield_not_required($admin_lang['label']['ceo_section_heading'],'other_sectors_ceo_heading','arb','',true,$result->id);
                                 create_image($admin_lang['label']['ceo_image'],'other_sectors_ceo_img','104&times;100','eng','',true,$result->id);
                                 create_textfield_not_required($admin_lang['label']['ceo_name'],'other_sectors_ceo_name','eng','',true,$result->id);
                                 create_textfield_not_required($admin_lang['label']['ceo_name'],'other_sectors_ceo_name','arb','',true,$result->id);
                                 create_textfield_not_required($admin_lang['label']['position'],'other_sectors_ceo_position','eng','',true,$result->id);
                                 create_textfield_not_required($admin_lang['label']['position'],'other_sectors_ceo_position','arb','',true,$result->id);
                                 create_textarea($admin_lang['label']['ceo_dec'],'other_sectors_ceo_dec','eng','',true,$result->id);
                                 create_textarea($admin_lang['label']['ceo_dec'],'other_sectors_ceo_dec','arb','',true,$result->id);
                             }
                             if($template == 'other_sectors' && $val == 3) {
                                 create_image('Project Logo','other_sectors_project_logo','90&times;85','eng','',true,$result->id);
                                 create_gallery($admin_lang['label']['gallery_images'],'other_sectors_slider','1440&times;636','eng','',true,$result->id);
                                 create_textfield_not_required('Project Video (YouTube Link)','other_sectors_proj_video','eng','',true,$result->id);
                                 echo '<div class="uk-width-medium-1-2"></div>';
                                 create_textarea($admin_lang['label']['project_dec'],'other_sectors_proj_dec','eng','',true,$result->id);
                                 create_textarea($admin_lang['label']['project_dec'],'other_sectors_proj_dec','arb','',true,$result->id);
                             }
								
								// Our Services
								if($template == 'our_services' && $page_details->listing_level == 2) {
									
									$products = getMultipleProductCategory();
								
									if($page_details->display_to_home == 1){
										$is_show_home0 = '';
										$is_show_home1 = 'selected = selected';
									}
									if($page_details->display_to_home == 0){
										$is_show_home0 = 'selected = selected';
										$is_show_home1 = '';
									}
									
									echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="display_to_home" class="">
										<option value="0" '.$is_show_home0.'>No</option>
										<option value="1" '.$is_show_home1.'>yes</option>
									</select>
									</div>';
									$dbIds = $page_details->multi_category;
									$dbId = explode(",", $dbIds);
									
									foreach($products as $product){
										
										$selected = "";
										$title_eng = $product->eng_title;
										$title_arb = $product->arb_title;
										$product_id = $product->id;
										if(in_array($product_id, $dbId))
										{
											$selected = 'selected = selected';
										}
										
										$html .= '<option value='.$product_id.' '.$selected.'>'.($admin_lang['admin_lang'] == 'eng' ? $title_eng : $title_arb ).'</option>';
									}
									
									echo '<div class="uk-width-medium-1-2"></div>';
									echo '<div class="uk-width-medium-1-2">
										<p>Multiple Category Selection</p>
										<select data-placeholder="Select Products & Services Categories" name="multiple_select[]" multiple class="chosen-select-width" tabindex="16">
											'.$html.'
										</select>							
									</div>';
									create_image($admin_lang['label']['thumb_image'],'services_thumb','274&times;272','eng','',true,$result->id);
									create_gallery($admin_lang['label']['product_gal'],'services_gallery','636&times;822','eng','',true, $result->id);
									create_textarea($admin_lang['label']['content_page_desc'],'desc','eng','',true,$result->id);
									create_textarea($admin_lang['label']['content_page_desc'],'desc','arb','',true,$result->id);
								}
								if($template == 'media_center') {
									create_gallery($admin_lang['label']['gallery_images'],'media_gallery_images','373&times;350','eng','',true,$result->id);
								}
								if($template == 'news') {
									
									/* if($page_details->is_show_home == 1){
										$is_show_home0 = '';
										$is_show_home1 = 'selected = selected';
									}
									if($page_details->is_show_home == 0){
										$is_show_home0 = 'selected = selected';
										$is_show_home1 = '';
									} */
									create_image($admin_lang['label']['page_image'],'page','1142&times;350','eng','',true,$result->id);
									create_image($admin_lang['label']['thumb_image'],'thumb','120&times;120','eng','',true,$result->id);
									create_datepicker($admin_lang['label']['date'],'date','eng','',true,$result->id);
									
									/* echo '<div class="uk-width-medium-1-2"></div>';
									echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_show_home" class="">
										<option value="1" '.$is_show_home1.'>yes</option>
										<option value="0" '.$is_show_home0.'>No</option>
									</select>
									</div>'; */
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textarea($admin_lang['label']['content_description'], 'desc', 'eng','',true,$result->id);
									create_textarea($admin_lang['label']['content_description'], 'desc', 'arb','',true,$result->id);
								}
								if($template == 'news_room') {
									
									/* if($page_details->is_show_home == 1){
										$is_show_home0 = '';
										$is_show_home1 = 'selected = selected';
									}
									if($page_details->is_show_home == 0){
										$is_show_home0 = 'selected = selected';
										$is_show_home1 = '';
									} */
									create_image($admin_lang['label']['page_image'],'page','1152&times;392','eng','',true,$result->id);
									create_image($admin_lang['label']['thumb_image'],'thumb','200&times;200','eng','',true,$result->id);
									create_datepicker($admin_lang['label']['date'],'date','eng','',true,$result->id);
									
									/* echo '<div class="uk-width-medium-1-2"></div>';
									echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_show_home" class="">
										<option value="1" '.$is_show_home1.'>yes</option>
										<option value="0" '.$is_show_home0.'>No</option>
									</select>
									</div>'; */
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textarea($admin_lang['label']['content_description'], 'desc', 'eng','',true,$result->id);
									create_textarea($admin_lang['label']['content_description'], 'desc', 'arb','',true,$result->id);
								}
								
								if($template == 'events') {

                                    $is_show_home0 = '';
                                    $is_show_home1 = '';
									 if($page_details->show_other_news == 1){
										$is_show_home0 = '';
										$is_show_home1 = 'selected = selected';
									}
									if($page_details->show_other_news == 0){
										$is_show_home0 = 'selected = selected';
										$is_show_home1 = '';
									}


									create_image($admin_lang['label']['thumb_image'],'thumb_event','485&times;485','eng','',true,$result->id);
									//create_image('Banner Image','banner_event','1152&times;392','eng','',true,$result->id);
                                    create_gallery('Detail Page Slider Images','slider_event','1210&times;1210','eng','',true,$result->id);

									create_datepicker($admin_lang['label']['date'],'event_date','eng','',true,$result->id);
                                    /*echo '<div class="uk-width-medium-1-2"></div>';
                                    echo' <div class="uk-width-medium-1-2">
                                    <label>'.$admin_lang['label']['show_other_news'].'</label>
                                    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="show_other_news" class="">
                                        <option '.$is_show_home1.' value="1">yes</option>
                                        <option '.$is_show_home0.' value="0">No</option>
                                    </select>
                                    </div>';*/
//									create_datepicker_end_date($admin_lang['label']['end_date'],'end_date','eng','',true,$result->id);
									
									/*$country_name = content_detail('country', $result->id);
									echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['list_content_country'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="select country" name="country" onchange="changeCountry(this)" id="country">
										<option value="">Select option</option>';
										getAllCountriesNew($admin_lang['admin_lang'],$country_name);
									echo '</select></div>';
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textfield_not_required($admin_lang['label']['num_exhibitors'], 'num_exhibitors', 'eng','',true, $result->id);*/
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textarea($admin_lang['label']['content_description'], 'desc', 'eng','',true,$result->id);
									create_textarea($admin_lang['label']['content_description'], 'desc', 'arb','',true,$result->id);
								}
								if($template == 'careers'  && $listing ==0) {
									/*create_image($admin_lang['label']['thumb_image'],'thumb_vacancy','200&times;200','eng','',true,$result->id);*/
                                    ?><input type="hidden" value="0" id="is_benefits" name="is_benefits"><?php
									create_textfield('Type', 'vac_type', 'eng','',true,$result->id);
									create_textfield('Type', 'vac_type', 'arb','',true,$result->id);
									create_textfield('Area', 'vac_area', 'eng','',true,$result->id);
									create_textfield('Area', 'vac_area', 'arb','',true,$result->id);

									/* create_location($admin_lang['label']['location'],'location','',true,$result->id); */
									create_textarea($admin_lang['label']['content_description'], 'vacancy_desc', 'eng','',true,$result->id);
									create_textarea($admin_lang['label']['content_description'], 'vacancy_desc', 'arb','',true,$result->id);
								}
                             if($template == 'careers' && $listing == 1) {
                                 ?>
                                 <input type="hidden" value="1" id="is_benefits" name="is_benefits">
                                 <?php
                                 create_textarea($admin_lang['label']['content_description'], 'benefits_desc', 'eng','',true,$result->id);
                                 create_textarea($admin_lang['label']['content_description'], 'benefits_desc', 'arb','',true,$result->id);
                             }
								/* if($template == 'faqs') {
									create_textarea($admin_lang['label']['question'], 'question', 'eng','',true,$result->id);
									create_textarea($admin_lang['label']['question'], 'question', 'arb','',true,$result->id);
									create_textarea($admin_lang['label']['answer'], 'answer', 'eng','',true,$result->id);
									create_textarea($admin_lang['label']['answer'], 'answer', 'arb','',true,$result->id);
								} */
								if($template == 'contact_us') {
									/* create_location($admin_lang['label']['location'],'location','',true,$result->id);
									create_textfield_not_required($admin_lang['label']['list_content_address'], 'address', 'eng','',true,$result->id);
									create_textfield_not_required($admin_lang['label']['list_content_address'], 'address', 'arb','',true,$result->id);
									create_textfield_not_required($admin_lang['label']['list_content_phone'], 'phone', 'eng','',true,$result->id);
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textfield_not_required($admin_lang['label']['list_content_fax'], 'fax', 'eng','',true,$result->id);
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textfield_not_required($admin_lang['label']['list_content_po_box'], 'po_box', 'eng','',true,$result->id);
									create_textfield_not_required($admin_lang['label']['list_content_po_box'], 'po_box', 'arb','',true,$result->id); */
								}
                            ?>
						
							
                            <!--End Content Divs-->
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_title']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" cols="30" rows="4" class="md-input" id="meta_title_eng" name="meta_title_eng" value="<?php echo $result->meta_title_eng; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_title']; ?> </label>
                                <div class="uk-form-row">
                                    <input cols="30" rows="4" class="md-input" name="meta_title_arb" id="meta_title_arb" value="<?php echo $result->meta_title_arb; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_desc']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_desc_eng" name="meta_desc_eng"><?php echo $result->meta_desc_eng;?></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_desc']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_desc_arb" name="meta_desc_arb"><?php echo $result->meta_desc_arb;?></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_keys']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_keywords_eng" name="meta_keywords_eng"><?php echo $result->meta_keywords_eng;?></textarea>
                                    <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_keys']; ?> </label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_keywords_arb" name="meta_keywords_arb"><?php echo $result->meta_keywords_arb;?></textarea>
                                    <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_update_content']; ?>" href="javascript:void(0);" id="saveAbout">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
        <!-- light box for image -->
        <button class="md-btn" id="btn_thumb" data-uk-modal="{target:'#modal_lightbox'}" style="display:none;">Open</button>
        <div class="uk-modal" id="modal_lightbox">
            <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
                <img id="img_url" src="" alt="" style="background: #f5f5f5;"/>
            </div>
        </div>
        <!-- end light box for image -->

    </div>

	<!--
	
		<div class="field_wrapper">
			<div>
				<input type="text" name="field_name[]" value=""/>
				<a href="javascript:void(0);" class="add_button" title="Add field"><img src="add-icon.png"/></a>
			</div>
		</div> 
	
	-->

</div>

<script language="javascript">

    $(document).ready(function () {
		
		
		if(<?php echo ($page_details->selectSlideOptions ?: 0); ?> == 1) {
			$('.imageDiv').show();
			$('.videoDiv').hide();
		} else {
			$('.videoDiv').show();
			$('.imageDiv').hide();
		}
			
		 $('[name = "selectSlideOptions"]').on('change',function(){
			var item = $(this);
			if(item.val()==1) {
				$('.imageDiv').show();
				$('.videoDiv').hide();
			} else {
				
				$('.videoDiv').show();
				$('.imageDiv').hide();
			}
		});
		
		$(".serviceTypeSelectBox").hide();
		<?php if($result->tpl == 'investment_centers'){ ?>
			loadCountry(<?php echo $page_details->region_id; ?>);
		<?php } ?>
        $('#saveAbout').click(function () {
            $(".page_loader").show();
            $("#updateAbout").attr("action", "<?php echo base_url();?>ems/content/update/id/<?php echo $result->id; ?>");
            $("#updateAbout").attr("target", "");
            $('#updateAbout').submit();
            $(".required").each(function(){
                if($(this).val() == ''){
                    setTimeout(function(){
                        $(".page_loader").hide();
                    },500);
                }
            });
        });
        $("#updateAbout").validate();
        setInterval(function(){$('#success_msg').html(''); }, 3000);

        $('.img_thumb').click(function () {
            $('#img_url').attr("src","");
            $('#btn_thumb').click();
            var img_src = $(this).attr("src");
            $('#img_url').attr("src",img_src);
        });
		
		$('.SliderCategory').on('change', function(e){ // Home slider category change
			e.preventDefault();
			if($(this).val() == 1){
				$('.homeHideShowDiv').fadeOut('slow');
			}
			else{
				$('.homeHideShowDiv').fadeIn('slow');
			}
		});
		
    });
	
	
function changeCountry(item){
	var id = item.value;
	//alert(id);
	//var hidden_branch_id = $(this).val();
	var $select = $('select.city').selectize();

	var selectize = $select[0].selectize;

	$.ajax({
		type: "POST",
		url: '<?php echo base_url().'ems/content/getCities'?>',
		data: {'id': id},
		dataType: "json",
		cache: false,
		//async:false,
		success: function (result) {
			selectize.clearOptions();
			var option = result.cities.split(',');
			var opt;
			for (var i = 0; i < option.length; i++) {
				opt = option[i].split('|');
				selectize.addOption({
					value: opt[0],
					text: opt[1]
				});
			}
			
			//$("#select.cityBranches option:selected").val(hidden_branch_id)
			selectize.refreshOptions();
		}
	});
};
	
function loadCountry(id){
	
	var $select = $('select.city').selectize();
	var selectize = $select[0].selectize;
	var hidden_branch_id = '<?php echo $page_details->city_id; ?>';
	//alert(hidden_branch_id);
	$.ajax({
		type: "POST",
		url: '<?php echo base_url().'ems/content/getCitiesSelected'; ?>',
		data: {'id': id},
		dataType: "json",
		cache: false,
		//async:false,
		success: function (result) {
			//alert(result.cities);
			selectize.clearOptions();
			var option = result.cities.split(',');
			var opt;
			for (var i = 0; i < option.length; i++) {
				opt = option[i].split('|');
				selectize.addOption({
					value: opt[0],
					text: opt[1]
				});
			}
			
			$(".city option:selected").val(hidden_branch_id)
			$('.city').data('selectize').setValue(hidden_branch_id);
			selectize.refreshOptions();
			
		}
	});
};

</script>

<script type="text/javascript">
$(document).ready(function(){
	
	$(".sortmeGallery").sortable({ 
		update : function () {
			serial = $(this).closest('tbody.sortmeGallery').sortable('serialize');
			//alert(serial);
			
			var field_values = '';
			$('.uk-table tr').each(function() {
				field_values += this.id+'|';
			});
			var field_name = $(this).closest('tbody.sortmeGallery').attr("data-field-name");
			
			//alert(field_name);
			
			var newOrdering = field_values.replace("|", ",");
			
			//alert(newOrdering);
			
			//alert(field_values);
			
			$.ajax({
				type: "post",
				dataType: "JSON",
				url: "<?php echo base_url(); ?>ems/content/sortableImageGallery?field="+field_name+'&id=<?php echo $result->id; ?>',
				data: {field_values:field_values},
				success: function(data){
					//alert(data.field_values)
					$('#'+field_name).val(data.field_values);
				},
				error: function(){
					alert("theres an error with AJAX");
				}
			});
		}
	});
	
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    
	var fieldHTML = '<div><label for="kUI_datetimepicker_range_end" class="uk-form-label">Event Date </label><input type="text" class="datetimepicker" value="" name="date[]"/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img style="margin-left: 12px; height: 25px;" src="<?php echo base_url('assets/admin/assets/img/');?>/remove-icon.png"/></a></div>'; 
    var x = 1; //Initial field counter is 1
	
	
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
			
			$(".datetimepicker").kendoDateTimePicker({
				dateInput: true
			});
			
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

<script>
	 $(document).ready(function () {
		// create DateTimePicker from input HTML element
		//value: new Date(), Commenting this because its picking up the current date and time
		$(".datetimepicker").kendoDateTimePicker({
			dateInput: true
		});
	});
</script>