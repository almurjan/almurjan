<!--New HTML-->
<style>
	.imageDiv > .uk-width-medium-1-2{
		display: inline-block !important;
	}
</style>

<?php 
$admin_lang = check_admin_lang();
$page_details = get_content_data($tpl_id);

$listing = $this->uri->segment(5);
if($listing >= 1 && $template == 'other_sectors'){
    $listing = $listing;
}elseif ($listing > 1) {
    $listing = $listing - 1;
}


?>
<div id="page_content">

    <div id="returned_page_image" style="display:none;"></div>
    <div id="returned_news_image" style="display:none;"></div>
    <div id="returned_news_image_desc" style="display:none;"></div>
    <?php echo form_open_multipart('ems/content/savePage', array('method' => 'post', 'id' => 'updateAbout')); ?>
    <input type="hidden" value="1" id="pub_status" name="pub_status">
    <input type="hidden" value="<?php echo $tpl_id; ?>" id="tpl_id" name="tpl_id">
    <input type="hidden" value="<?php echo $template; ?>" id="tpl_name" name="tpl_name">
    <input type="hidden" value="<?php echo $template; ?>" id="tpl" name="tpl">
	<?php if($template == 'other_sectors' || $template == 'our_services' || ($template == 'profile' && $listing!=''))
		{
			$val = ($page_details->listing_level == "" ? 1 : $listing);
			echo '<input type="hidden" value="'.$val.'" id="listing_level" name="listing_level">';
		}
	?>
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo getPagePath($page_details->id,$admin_lang['admin_lang']) . ' - ' .$admin_lang['label']['list_content_add']; ?></h1>
        <div id="success_msg" style="text-align:center;color:#7cb342;">
            <?php if (validation_errors()) {
                echo _erMsg(validation_errors());
            } if ($this->session->flashdata('message')) {
                echo $this->session->flashdata('message');
            } ?>
        </div>
		<div id="err_msg_dates" style="text-align:center;color:red;"></div>
    </div>
	
    <div id="page_content_inner">
        <?php
        if($template == 'other_sectors' && $val == '1'){?>
            <a href="<?php echo base_url().'ems/list_content/index/'.$tpl_id.'/0';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <?php }
        elseif($template == 'other_sectors' && $val == '2'){?>
            <a href="<?php echo base_url().'ems/list_content/index/'.$tpl_id.'/2';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <?php }elseif($template == 'other_sectors' && $val == '3'){?>
            <a href="<?php echo base_url().'ems/list_content/index/'.$tpl_id.'/3';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <?php }
        elseif($template == 'careers' && $listing == '0'){?>
            <a href="<?php echo base_url().'ems/list_content/index/'.$tpl_id.'/0';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <?php }
        elseif($template == 'careers' && $listing == '1'){?>
            <a href="<?php echo base_url().'ems/list_content/index/'.$tpl_id.'/1';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <?php }elseif($template == 'profile' && $listing == '1'){?>
            <a href="<?php echo base_url().'ems/list_content/index/'.$tpl_id.'/1';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <?php }else{?>
            <a href="<?php echo base_url().'ems/list_content/index/'.$tpl_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <?php }
        if($template=='home'){
            $sub_title_req='';
            $req='';
        }else{
            $sub_title_req= 'required';
            $req='*';
        }
        ?>

        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <h3 class="md-card-toolbar-heading-text">
                            <?php echo $admin_lang['label']['content_page_content']; ?>
                        </h3>
                    </div>
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_title']; ?>*</label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required" id="eng_title" name="eng_title" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_title']; ?>*</label>
                                <div class="uk-form-row">
                                    <input class="md-input required" name="arb_title" id="arb_title" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_sub_title'].' '.$req; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input <?php echo $sub_title_req ?>" id="eng_sub_title" name="eng_sub_title" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_sub_title'].' '.$req; ?></label>
                                <div class="uk-form-row">
                                    <input class="md-input <?php echo $sub_title_req ?>" name="arb_sub_title" id="arb_sub_title" value="" />
                                </div>
                            </div>

                            <!--Start Content Divs-->

                            <!--Last parameter after lang is for parent class which is using for hide & show content (for
                            list_content leave this empty)-->
                            <!--For textarea field there is words limit digit after comma which is optional-->

                            <?php
								
								
								//HOME 
								 if ($template == 'home') {
									echo '
									<div class="uk-width-medium-1-2">
										<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="selectSlideOptions" class="">
											
											<option value= "1">Slider Image</option>
											<option value= "2">Slider Video</option>                                
										</select>
										</div>
										<div class="uk-width-medium-1-2"></div>
										';

									echo "<div class='uk-width-medium-1-1 imageDiv'> ";
									create_image($admin_lang['label']['thumb_image'], 'home_thumbImage', '1300&times;1300', 'eng','', '', '');
									create_textarea($admin_lang['label']['image_select'], 'home_sliderText', 'eng', '','', '');
									create_textarea($admin_lang['label']['image_select'], 'home_sliderText', 'arb', '','', '');
									echo '</div>';
									echo '';

									echo "<div class='uk-width-medium-1-1 videoDiv'> ";
										upload_video($admin_lang['label']['content_choose_video'], 'file', '', '', '');
										//create_image($admin_lang['label']['video_thumb'], 'video_thumb', '200&times;200', 'eng','', '', '');
										//create_externallink($admin_lang['label']['video'], 'home_videoLink1', 'eng', '','', '');
									echo "</div>";
                                     create_externallink($admin_lang['label']['content_external_link'], 'home_sliderLink', 'eng', '','', '');
								}
								//Secuirty Features
								if($template == 'security_features') {
									create_image($admin_lang['label']['thumb_image'],'thumb_security','200&times;200','eng','');	
								
								}
								//companies map pin
								if($template == 'companies_map') {
                                    create_textfield($admin_lang['label']['companies_pin_location_x'], 'compnies_pin_xposition', 'eng', '','', '');
                                    create_textfield($admin_lang['label']['companies_pin_location_y'], 'compnies_pin_yposition', 'eng', '','', '');

								}
								//FAQS
								if($template == 'faqs') {
									
									// Final PDF code with name showing.
									//upload_file_half_div($admin_lang['label']['choose_file'],'file','','profile_div');
									//upload_file_arb_half_div($admin_lang['label']['choose_file'],'file_arb','','profile_div');
									create_textarea($admin_lang['label']['faq_ques'],'question','eng','');
									create_textarea($admin_lang['label']['faq_ques'],'question','arb','');									
									create_textarea($admin_lang['label']['faq_ans'],'answer','eng','');
									create_textarea($admin_lang['label']['faq_ans'],'answer','arb','');
								}
								
								// Our Approach
								if($template == 'our_approach' && $page_details->listing_level == "") {
									create_image($admin_lang['label']['category_logo'],'approach_category_logo','200&times;200','eng','');	
										
								}
								
								//HISTORY
								if($template == 'history') {
									create_image($admin_lang['label']['thumb_image'],'history_listing_thumb','200&times;200','eng','');
                                    create_textfield('History Title','history_listing_title','eng','');
                                    create_textfield('History Title','history_listing_title','arb','');
                                    create_textarea($admin_lang['label']['content_description'],'history_listing_desc','eng','');
                                    create_textarea($admin_lang['label']['content_description'],'history_listing_desc','arb','');
									
									/*for ($i = 1990; $i <= date('Y'); $i++) {
                                    $all_options .= '<option value="' . $i . '">' . $i . '</option>';
									}

                                echo ' <div class="uk-width-medium-1-2">
									<label>' . 'Select Year' . '</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="year" class="">
										<option value="">' . $admin_lang['label']['list_content_select_option'] . '</option>
										' . $all_options . '
									</select>
									</div>';
									echo '<div class="uk-width-medium-1-2"></div>';*/
									
								}
								if($template == 'ceo_clients') {
									create_image($admin_lang['label']['thumb_image'],'thumb','200&times;200','eng','');
								}
								#todo::profile section new added
                                if($template == 'profile' && $listing > 0) {
                                    create_image($admin_lang['label']['image'],'company_logo','130&times;130','eng','');
                                    create_textarea($admin_lang['label']['content_description'],'company_desc','eng','');
                                    create_textarea($admin_lang['label']['content_description'],'company_desc','arb','');
                                }elseif($template == 'profile') {
                                    create_image($admin_lang['label']['image'],'value_image','150&times;150','eng','');
                                    create_textarea($admin_lang['label']['content_description'],'value_desc','eng','');
                                    create_textarea($admin_lang['label']['content_description'],'value_desc','arb','');
                                }

                                if($template == 'csr'){
                                    //csr
                                    create_image($admin_lang['label']['thumb_image'], 'csr_thumb_image', '507&times;507', 'eng','','','');
                                    create_textarea($admin_lang['label']['content_description'], 'csr_desc', 'eng','','','');
                                    create_textarea($admin_lang['label']['content_description'], 'csr_desc', 'arb','','','');
                                }

                                if($template == 'bod') {
                                    $category_name1 =  ($admin_lang['admin_lang'] == 'eng' ? 'Board of Directors' : 'أعضاء مجلس الإدارة');
                                    $category_name2 =  ($admin_lang['admin_lang'] == 'eng' ? 'Executive Committee' : 'اللجنة التنفيذية');
                                    $category_name3 =  ($admin_lang['admin_lang'] == 'eng' ? 'Leadership Team' : 'فريق القيادة');
                                    $category_name4 =  ($admin_lang['admin_lang'] == 'eng' ? 'Investment Advisory Board' : 'المجلس الاستشاري للاستثمار');
                                    $category_name5 =  ($admin_lang['admin_lang'] == 'eng' ? 'Management Team' : 'فريق الإدارة');
                                    $category_name6 =  ($admin_lang['admin_lang'] == 'eng' ? 'Executive Team' : 'الفريق التنفيذي');
                                    echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['categories'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="category" class="">
										<option value="" >Select Category</option>
										<option value="1" >'.$category_name1.'</option>
										<option value="2" >'.$category_name2.'</option>
										<option value="3" >'.$category_name3.'</option>
										<option value="4" >'.$category_name4.'</option>
										<option value="5" >'.$category_name5.'</option>
										<option value="6" >'.$category_name6.'</option>
									</select>
									</div>';
                                    create_image($admin_lang['label']['image'],'dob_image','263&times;400','eng','');
                                    create_textfield('Designation','designation','eng','','');
                                    create_textfield('Designation','designation','arb','','');
                                    create_textarea('Description','dob_dec','eng','','');
                                    create_textarea('Description','dob_dec','arb','','');
                                }
                            if($template == 'murjan_group') {
                                create_image($admin_lang['label']['image'],'group_image','150&times;150','eng','');
                                create_textarea('Description','group_dec','eng','','');
                                create_textarea('Description','group_dec','arb','','');
                            }
                                if($template == 'landmarks') {
                                    echo '<div class="uk-width-medium-1-2 uk-grid-margin">
                                            <label">'.$admin_lang['label']['country_name'].'
                                                <div class="uk-form-row">
                                                    <div class="md-input-wrapper md-input-filled">
                                                     <input type="text" name="country_name" class="md-input valid" value="" aria-invalid="false">
                                                     <span class="md-input-bar "></span>
                                                    </div>
                                                </div>
                                            </label">
                                        </div>';
                                    echo '<div class="uk-width-medium-1-2 uk-grid-margin">
                                            <label">'.$admin_lang['label']['country_name_ar'].'
                                                <div class="uk-form-row">
                                                    <div class="md-input-wrapper md-input-filled">
                                                     <input type="text" name="country_name_ar" class="md-input valid" value="" aria-invalid="false">
                                                     <span class="md-input-bar "></span>
                                                    </div>
                                                </div>
                                            </label">
                                        </div>';

                                    echo' <div class="uk-width-medium-1-2"></div>';
                                    //ahmed
                                    ?>
                                    <div class="uk-width-medium-1-2">
									<label><?php echo $admin_lang['label']['select_category']?></label>
                                        <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="category_id" class="">
                                            <option value="" >Select Category</option>
                                           <?php
                                           $categories_data = get_catogries_data(0);
                                           $category_html = '';
                                           foreach($categories_data as $category){
                                               if($admin_lang['admin_lang']=='eng'){
                                                   $title_cat = $category->category_title;
                                               }else{
                                                   $title_cat = $category->category_title_ar;
                                               }?>
                                               <option value="<?php echo $category->category_id;?>" ><?php echo  $title_cat;?></option>
                                          <?php } ?>
                                        </select>
									</div>
									<?php
                                    create_image('Logo','landmarks_logo','570&times;420','eng','');
                                    create_image($admin_lang['label']['thumb_image'],'landmarks_thumb','600&times;600','eng','');
                                    create_gallery($admin_lang['label']['image'],'landmarks_image','600&times;600','eng','');
                                    create_textarea('Description','landmarks_dec','eng','','');
                                    create_textarea('Description','landmarks_dec','arb','','');
                                }
                                if($template == 'murjan_holding') {
                                    create_image('Logo','holding_logo','170&times;170','eng','');
                                    create_gallery($admin_lang['label']['gallary'],'holding_slider','325&times;118','eng','');
                                    echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_show_home" class="">
										<option value="0" >No</option>
										<option value="1" >yes</option>
									</select>
									</div>';
                                    create_externallink('Link','holding_link','eng','');//ahmed
                                    create_textarea('Description','holding_dec','eng','','');
                                    create_textarea('Description','holding_dec','arb','','');
                                }
                                if($template == 'other_sectors' && $val == 1) {
                                    create_textfield_not_required('Short Description','other_sectors_cat_desc_1','eng','','');
                                    create_textfield_not_required('Short Description','other_sectors_cat_desc_1','arb','','');
                                    create_textarea('Description 2','other_sectors_cat_desc_2','eng','','');
                                    create_textarea('Description 2','other_sectors_cat_desc_2','arb','','');
                                    create_image($admin_lang['label']['category_icon'],'other_sectors_cat_icon','112&times;97','eng','');
                                    create_image($admin_lang['label']['category_hover_icon'],'other_sectors_cat_hover_icon','112&times;97','eng','');
                                }
                                if($template == 'other_sectors' && $val == 2) {
                                    create_image($admin_lang['label']['logo'],'other_sectors_logo','104&times;100','eng','');
                                    echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_show_home" class="">
										<option value="0" >No</option>
										<option value="1" >yes</option>
									</select> 
									</div>';
                                    create_textfield_not_required($admin_lang['label']['sorting_sector_logo_for_home'],'sectors_logo_sorting','eng','');
                                    create_image($admin_lang['label']['sector_logo_for_home'],'other_sectors_logo_for_home','125&times;80','eng','');
                                    create_gallery($admin_lang['label']['gallery_images'],'other_sectors_discover_more_slider','626&times;530','eng','');
                                    create_image($admin_lang['label']['slider_image'],'other_sectors_slider_image','950&times;950','eng','');
                                    /*echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['slider_number'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="slider_number" class="">
										<option value="1" >1</option>
										<option value="2" >2</option>
										<option value="3" >3</option>
									</select> 
									</div>';*/
                                    create_externallink('Link','other_sectors_link','eng','');//ahmed
                                    create_textarea('Description','other_sectors_dec','eng','','');
                                    create_textarea('Description','other_sectors_dec','arb','','');
                                    create_textarea('Company Profile','other_sectors_company_profile_desc','eng','','');
                                    create_textarea('Company Profile','other_sectors_company_profile_desc','arb','','');
                                    create_textfield_not_required($admin_lang['label']['ceo_section_heading'],'other_sectors_ceo_heading','eng','');
                                    create_textfield_not_required($admin_lang['label']['ceo_section_heading'],'other_sectors_ceo_heading','arb','');
                                    create_image($admin_lang['label']['ceo_image'],'other_sectors_ceo_img','104&times;100','eng','');
                                    create_textfield_not_required($admin_lang['label']['ceo_name'],'other_sectors_ceo_name','eng','');
                                    create_textfield_not_required($admin_lang['label']['ceo_name'],'other_sectors_ceo_name','arb','');
                                    create_textfield_not_required($admin_lang['label']['position'],'other_sectors_ceo_position','eng','');
                                    create_textfield_not_required($admin_lang['label']['position'],'other_sectors_ceo_position','arb','');
                                    create_textarea($admin_lang['label']['ceo_dec'],'other_sectors_ceo_dec','eng','','');
                                    create_textarea($admin_lang['label']['ceo_dec'],'other_sectors_ceo_dec','arb','','');
                                }
                            if($template == 'other_sectors' && $val == 3) {
                                create_image('Project Logo','other_sectors_project_logo','90&times;85','eng','');
                                create_gallery($admin_lang['label']['gallery_images'],'other_sectors_slider','1440&times;636','eng','');
                                create_textfield_not_required('Project Video (YouTube Link)','other_sectors_proj_video','eng','','');
                                echo '<div class="uk-width-medium-1-2"></div>';
                                create_textarea($admin_lang['label']['project_dec'],'other_sectors_proj_dec','eng','','');
                                create_textarea($admin_lang['label']['project_dec'],'other_sectors_proj_dec','arb','','');
                            }

								if($template == 'our_services' && $page_details->listing_level == 1) {
									echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="display_to_home" class="">
										<option value="0">No</option>
										<option value="1">yes</option>
									</select>
									</div>';
									
									$products = getMultipleProductCategory();
									foreach($products as $product){
										$title_eng = $product->eng_title;
										$title_arb = $product->arb_title;
										$product_id = $product->id;
										$selected = "";
										if($product_id == $tpl_id){
											$selected = 'selected';
										}
										$html .= '<option value='.$product_id.' '.$selected.'>'.($admin_lang['admin_lang'] == 'eng' ? $title_eng : $title_arb ).'</option>';
									}
									
									echo '<div class="uk-width-medium-1-2"></div>';
									echo '<div class="uk-width-medium-1-2">
										<p>Multiple Category Selection</p>
										<select data-placeholder="Select Products & Services Categories" name="multiple_select[]" multiple class="chosen-select-width" tabindex="16">
											'.$html.'
										</select>							
									</div>';
									
									create_image($admin_lang['label']['thumb_image'],'services_thumb','274&times;272','eng','');
									create_gallery($admin_lang['label']['product_gal'],'services_gallery','636&times;822','eng','');
									create_textarea($admin_lang['label']['content_page_desc'],'desc','eng','');
									create_textarea($admin_lang['label']['content_page_desc'],'desc','arb','');
								}
								if($template == 'media_center') {
									create_gallery($admin_lang['label']['gallery_images'],'media_gallery_images','373&times;350','eng','');
								}
								if($template == 'news') {
									create_image($admin_lang['label']['page_image'],'page','1142&times;350','eng','');
									create_image($admin_lang['label']['thumb_image'],'thumb','120&times;120','eng','');
									create_datepicker($admin_lang['label']['date'],'date','eng','','','');
									/* 
									echo '<div class="uk-width-medium-1-2"></div>';
									echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_show_home" class="">
										<option value="1">yes</option>
										<option value="0">No</option>
									</select>
									</div>';
									 */
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textarea($admin_lang['label']['content_description'], 'desc', 'eng','');
									create_textarea($admin_lang['label']['content_description'], 'desc', 'arb','');
								}
								if($template == 'news_room') {
									create_image($admin_lang['label']['page_image'],'page','1152&times;392','eng','');
									create_image($admin_lang['label']['thumb_image'],'thumb','200&times;200','eng','');
									create_datepicker($admin_lang['label']['date'],'date','eng','','','');
									/* 
									echo '<div class="uk-width-medium-1-2"></div>';
									echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_show_home" class="">
										<option value="1">yes</option>
										<option value="0">No</option>
									</select>
									</div>';
									 */
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textarea($admin_lang['label']['content_description'], 'desc', 'eng','');
									create_textarea($admin_lang['label']['content_description'], 'desc', 'arb','');
								}
								
								if($template == 'events') {
									create_image($admin_lang['label']['thumb_image'],'thumb_event','485&times;485','eng','','','');
                                    //create_image('Banner Image','banner_event','1152&times;392','eng','','','');
                                    create_gallery('Detail Page Slider Images','slider_event','1210&times;1210','eng','','','');

									create_datepicker($admin_lang['label']['date'],'event_date','eng','','','');
                                    /*echo '<div class="uk-width-medium-1-2"></div>';
                                    echo' <div class="uk-width-medium-1-2">
                                    <label>'.$admin_lang['label']['show_other_news'].'</label>
                                    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="show_other_news" class="">
                                        <option value="1">yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    </div>';*/

									/*create_datepicker_end_date($admin_lang['label']['end_date'],'end_date','eng','','','');*/
									
									/*echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['list_content_country'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="select country" name="country" onchange="(this)" id="country">
										<option value="">Select option</option>';
										getAllCountriesNew($admin_lang['admin_lang']);
									echo '</select></div>';
									echo '<div class="uk-width-medium-1-2"></div>';*/
									
									/* echo' <div class="uk-width-medium-1-2">
									<label>'.$admin_lang['label']['show_home'].'</label>
									<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_show_home" class="">
										<option value="1">yes</option>
										<option value="0">No</option>
									</select>
									</div>';
										
									echo '<div class="uk-width-medium-1-2"></div>';
									*/
									/*create_textfield_not_required($admin_lang['label']['num_exhibitors'], 'num_exhibitors', 'eng','');*/
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textarea($admin_lang['label']['content_description'], 'desc', 'eng','');
									create_textarea($admin_lang['label']['content_description'], 'desc', 'arb','');
								}
								if($template == 'careers' && $listing ==0) {
									/*create_image($admin_lang['label']['thumb_image'],'thumb_vacancy','200&times;200','eng','','','');*/
                                    create_textfield('Type', 'vac_type', 'eng','');
                                    create_textfield('Type', 'vac_type', 'arb','');
                                    create_textfield('Area', 'vac_area', 'eng','');
                                    create_textfield('Area', 'vac_area', 'arb','');?>
                                    <input type="hidden" value="0" id="is_benefits" name="is_benefits">
                                        <?php
									create_textarea($admin_lang['label']['content_description'], 'vacancy_desc', 'eng','');
									create_textarea($admin_lang['label']['content_description'], 'vacancy_desc', 'arb','');
								}
                                if($template == 'careers' && $listing == 1) {
                                    ?>
                                    <input type="hidden" value="1" id="is_benefits" name="is_benefits">
                                    <?php
                                    create_textarea($admin_lang['label']['content_description'], 'benefits_desc', 'eng','');
                                    create_textarea($admin_lang['label']['content_description'], 'benefits_desc', 'arb','');
                                }
								/* if($template == 'faqs') {
									create_textarea($admin_lang['label']['question'], 'question', 'eng','');
									create_textarea($admin_lang['label']['question'], 'question', 'arb','');
									create_textarea($admin_lang['label']['answer'], 'answer', 'eng','');
									create_textarea($admin_lang['label']['answer'], 'answer', 'arb','');
								} */
								if($template == 'contact_us') {
									/* create_location($admin_lang['label']['location'],'location','','','');
									create_textfield_not_required($admin_lang['label']['list_content_address'], 'address', 'eng','');
									create_textfield_not_required($admin_lang['label']['list_content_address'], 'address', 'arb','');
									create_textfield_not_required($admin_lang['label']['list_content_phone'], 'phone', 'eng','');
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textfield_not_required($admin_lang['label']['list_content_fax'], 'fax', 'eng','');
									echo '<div class="uk-width-medium-1-2"></div>';
									create_textfield_not_required($admin_lang['label']['list_content_po_box'], 'po_box', 'eng','');
									create_textfield_not_required($admin_lang['label']['list_content_po_box'], 'po_box', 'arb',''); */
								}
								
                            ?>
							
                            <!--End Content Divs-->

                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_title']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input" id="meta_title_eng" name="meta_title_eng" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_title']; ?> </label>
                                <div class="uk-form-row">
                                    <input class="md-input" name="meta_title_arb" id="meta_title_arb" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_desc']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_desc_eng" name="meta_desc_eng"></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_desc']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_desc_arb" name="meta_desc_arb"></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_keys']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_keywords_eng" name="meta_keywords_eng"></textarea>
                                    <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_keys']; ?> </label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_keywords_arb" name="meta_keywords_arb"></textarea>
                                    <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_save_content']; ?>" href="javascript:void(0);" id="saveAbout">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
    </div>
</div>

<script language="javascript">

    $(document).ready(function () {
		$('.videoDiv').hide();
		$('[name = "selectSlideOptions"]').on('change',function(){ 
			var item = $(this);
			if(item.val()==1) {
				$('.imageDiv').show();
				$('.videoDiv').hide();
			} else {
				$('.videoDiv').show();
				$('.imageDiv').hide();
			}
		});

        $('#saveAbout').click(function () {
			$(".page_loader").show();
			$("#updateAbout").attr("action", "<?php echo base_url();?>ems/content/savePage/");
			$("#updateAbout").attr("target", "");
			$('#updateAbout').submit();
            $(".required").each(function(){
                if($(this).val() == ''){
                    setTimeout(function(){
                        $(".page_loader").hide();
                    },500);
                }
            });
        });
		$("#updateAbout").validate();
        

    });
    setInterval(function(){$('#success_msg').html(''); }, 3000);
</script>



<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    
	var fieldHTML = '<label for="kUI_datetimepicker_range_end" class="uk-form-label">Event Date </label><input type="text" class="datetimepicker" value="" name="date[]"/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img style="margin-left: 12px; height: 25px;" src="<?php echo base_url('assets/admin/assets/img/');?>/remove-icon.png"/></a>'; 
    var x = 1; //Initial field counter is 1
	
	
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
			
			$(".datetimepicker").kendoDateTimePicker({
				dateInput: true
			});
			
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
	
	$('.SliderCategory').on('change', function(e){ // Home slider category change
		e.preventDefault();
		if($(this).val() == 1){
			$('.homeHideShowDiv').fadeOut('slow');
		}
		else{
			$('.homeHideShowDiv').fadeIn('slow');
		}
    });
	
});

function changeCountry(item){
		var id = item.value;
		//var hidden_branch_id = $(this).val();
		var $select = $('select.city').selectize();

		var selectize = $select[0].selectize;

		$.ajax({
			type: "POST",
				url: '<?php echo base_url().'ems/content/getCities'?>',
			data: {'id': id},
			dataType: "json",
			cache: false,
			//async:false,
			success: function (result) {
				selectize.clearOptions();
				var option = result.cities.split(',');
				var opt;
				for (var i = 0; i < option.length; i++) {
					opt = option[i].split('|');
					selectize.addOption({
						value: opt[0],
						text: opt[1]
					});
				}
				
				//$("#select.cityBranches option:selected").val(hidden_branch_id)
				selectize.refreshOptions();
			}
		});
	};

</script>

<script>
	 $(document).ready(function () {
		// create DateTimePicker from input HTML element
		//value: new Date(), Commenting this because its picking up the current date and time
		$(".datetimepicker").kendoDateTimePicker({
			dateInput: true
		});
	});
</script>

<!--End New HTML-->
