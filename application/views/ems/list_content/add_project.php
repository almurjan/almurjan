<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
    <div id="returned_page_image" style="display:none;"></div>
    <div id="returned_news_image" style="display:none;"></div>
    <div id="returned_news_image_desc" style="display:none;"></div>
    <?php echo form_open_multipart('ems/content/savePageProject', array('method' => 'post', 'id' => 'updateAbout','onsubmit' => 'return checkUniquePageTitle(0)')); ?>
    <input type="hidden" value="1" id="pub_status" name="pub_status">
    <input type="hidden" value="<?php echo $tpl_id; ?>" id="tpl_id" name="tpl_id">
    <input type="hidden" value="<?php echo $template; ?>" id="tpl_name" name="tpl_name">
    <input type="hidden" value="<?php echo $template; ?>" id="tpl" name="tpl">
    <input type="hidden" value="1" id="is_project" name="is_project">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['list_content_add']; ?></h1>
        <div id="success_msg" style="text-align:center;color:#7cb342;">
            <?php if (validation_errors()) {
                echo _erMsg(validation_errors());
            } if ($this->session->flashdata('message')) {
                echo $this->session->flashdata('message');
            } ?>
        </div>
    </div>
    <div id="page_content_inner">
        <a href="<?php echo base_url().'ems/list_content/list_content_projects/'.$tpl_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <h3 class="md-card-toolbar-heading-text">
                            <?php echo $admin_lang['label']['content_page_content']; ?>
                        </h3>
                    </div>
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_title']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required" id="eng_title" name="eng_title" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_title']; ?> </label>
                                <div class="uk-form-row">
                                    <input class="md-input required" name="arb_title" id="arb_title" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_sub_title']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required" id="eng_sub_title" name="eng_sub_title" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_sub_title']; ?> </label>
                                <div class="uk-form-row">
                                    <input class="md-input required" name="arb_sub_title" id="arb_sub_title" value="" />
                                </div>
                            </div>

                            <!--Start Content Divs-->

                            <!--Last parameter after lang is for parent class which is using for hide & show content (for
                            list_content leave this empty)-->
                            <!--For textarea field there is words limit digit after comma which is optional-->

                            <?php
							
							
                            if($template == 'projects') {
								
								create_textfield('Category Name', 'program_title', 'eng','');
								create_textfield('Category Name', 'program_title', 'arb','');
								create_datepicker_specified_time('Start ' .$admin_lang['label']['date'],'start_date','eng','','','');
								echo '<div class="uk-width-medium-1-2"></div>';
								create_datepicker_specified_time('Completion ' .$admin_lang['label']['date'],'end_date','eng','','','');
								echo '<div class="uk-width-medium-1-2"></div>';
								create_location($admin_lang['label']['location'],'location','','','');
								create_image($admin_lang['label']['thumb_image'],'gallery_thumb','300&times;300','eng','');
								create_gallery($admin_lang['label']['gallery_images'],'project_gallery_images','600&times;470','eng','');
								create_textarea('Overview', 'overview', 'eng','');
								create_textarea('Overview', 'overview', 'arb','');
								create_textarea('Service Provided', 'service_provided', 'eng','');
								create_textarea('Service Provided', 'service_provided', 'arb','');
                            }
                            if($template == 'services_category') {
								
								create_textfield('Category Name', 'program_title', 'eng','');
								create_textfield('Category Name', 'program_title', 'arb','');
								echo' <div class="uk-width-medium-1-2">
								<label>'.$admin_lang['label']['category'].'</label>
								<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="is_feature" class="required">
									<option value="">'.$admin_lang['label']['list_content_select_option'].'</option>
									<option value="0">Normal</option>
									<option value="1">Featured</option>
								</select>
								</div>';
								echo '<div class="uk-width-medium-1-2"></div>';
								create_image($admin_lang['label']['thumb_image'],'gallery_thumb','300&times;300','eng','');
								create_textarea('Overview', 'overview', 'eng','');
								create_textarea('Overview', 'overview', 'arb','');
								create_textarea('Description', 'desc', 'eng','');
								create_textarea('Description', 'desc', 'arb','');
                            }
							 
                            ?>
							
							
                            <!--End Content Divs-->

                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_title']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required" id="meta_title_eng" name="meta_title_eng" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_title']; ?> </label>
                                <div class="uk-form-row">
                                    <input class="md-input" name="meta_title_arb" id="meta_title_arb" value="" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_desc']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input required" id="meta_desc_eng" name="meta_desc_eng"></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_desc']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_desc_arb" name="meta_desc_arb"></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_keys']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input required" id="meta_keywords_eng" name="meta_keywords_eng"></textarea>
                                    <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_keys']; ?> </label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_keywords_arb" name="meta_keywords_arb"></textarea>
                                    <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_save_content']; ?>" href="javascript:void(0);" id="saveAbout">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function () {
        $('#saveAbout').click(function () {
			$(".page_loader").show();
			$("#updateAbout").attr("action", "<?php echo base_url();?>ems/content/savePageProject/");
			$("#updateAbout").attr("target", "");
			$('#updateAbout').submit();
            $(".required").each(function(){
                if($(this).val() == ''){
                    setTimeout(function(){
                        $(".page_loader").hide();
                    },500);
                }
            });
        });
		$("#updateAbout").validate();
        

    });
    setInterval(function(){$('#success_msg').html(''); }, 3000);
</script>



<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    
	var fieldHTML = '<label for="kUI_datetimepicker_range_end" class="uk-form-label">Event Date </label><input type="text" class="datetimepicker" value="" name="date[]"/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img style="margin-left: 12px; height: 25px;" src="<?php echo base_url('assets/admin/assets/img/');?>/remove-icon.png"/></a>'; 
    var x = 1; //Initial field counter is 1
	
	
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
			
			$(".datetimepicker").kendoDateTimePicker({
				dateInput: true
			});
			
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

<script>
	 $(document).ready(function () {
		// create DateTimePicker from input HTML element
		//value: new Date(), Commenting this because its picking up the current date and time
		$(".datetimepicker").kendoDateTimePicker({
			dateInput: true
		});
	});
	
	$(document).ready(function () {
		$("input[name='eng_seats']").val("0");
	});
 </script>


<!--End New HTML-->
