
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo ($admin_lang['admin_lang'] == 'eng'?$result->eng_title:$result->arb_title); ?> - <?php echo $admin_lang['label']['edit']; ?></h1>
        <!--<div class="modifiedDate">Last modified <?php /*echo date("F j, Y, g:i a",strtotime($result->content_updated_at)); */?> </div>-->
        <div id="success_msg" style="text-align:center;color:#7cb342;">
            <?php
            if (validation_errors()) {
                echo _erMsg(validation_errors());
            } if ($this->session->flashdata('message')) {
                echo $this->session->flashdata('message');
            }
            ?>
        </div>
    </div>
	
    <div id="page_content_inner">
	<a href="<?php echo base_url().'ems/list_content/index_video/'.$result->parant_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?>
	
        <?php if($this->uri->segment(6) != ''){ ?>
            <a href="<?php echo base_url().'ems/content/versionsListing/'.$page_id;?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_delete']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <?php }else{ ?>
            <a href="<?php echo base_url();?>ems/content/versionsListing/<?php echo $result->id; ?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_versions']; ?>"><?php echo $admin_lang['label']['content_versions']; ?></a>
            
        <?php } ?>
<!--<a href="javascript:void(0);" id="preview" class="md-btn autoTooltip" title="<?php /*echo $admin_lang['label']['tooltip_preview']; */?>"><?php /*echo $admin_lang['label']['content_preview']; */?></a>-->
        <input type="hidden" value="<?php echo $result->eng_title; ?>" id="page_title_before_change" name="page_title_before_change">
        <?php echo form_open_multipart('ems/content/update/id/' . $result->id, array('method' => 'post', 'id' => 'updateAbout','onsubmit' => 'return checkUniquePageTitle(1)')); ?>
        <input type="hidden" value="<?php echo $result->pub_status; ?>" id="pub_status" name="pub_status">
        <input type="hidden" value="<?php echo $result->id; ?>" id="gid" name="gid">
        <input type="hidden" value="<?php echo $page_id; ?>" id="page_id" name="page_id">
        <input type="hidden" value="<?php echo $template; ?>" id="tpl" name="tpl">
        <input type="hidden" value="<?php echo $result->parant_id; ?>" id="tpl_id" name="tpl_id">
        <input type="hidden" value="<?php echo $template;?>" id="tpl_name" name="tpl_name">
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                <div class="md-card">
                    <div class="md-card-toolbar">
                    </div>
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_title']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" cols="30" rows="4" class="md-input required" id="eng_title" name="eng_title" value="<?php echo $result->eng_title; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_title']; ?> </label>
                                <div class="uk-form-row">
                                    <input cols="30" rows="4" class="md-input required" name="arb_title" id="arb_title" value="<?php echo $result->arb_title; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_sub_title']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required" id="eng_sub_title" name="eng_sub_title" value="<?php echo $result->eng_sub_title; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['list_content_sub_title']; ?> </label>
                                <div class="uk-form-row">
                                    <input class="md-input required" name="arb_sub_title" id="arb_sub_title" value="<?php echo $result->arb_sub_title; ?>" />
                                </div>
                            </div>
                            <!--Start Content Divs-->
                            <!--Last parameter after lang is for parent class which is using for hide & show content
                            (for list_content leave this empty)-->
                            <!--For textarea field there is words limit digit after comma which is optional-->
							
                        <?php
						
						if($template == 'media_center') {
							create_image($admin_lang['label']['thumb_image'],'gallery_thumb','300&times;300','eng','',true,$result->id);
							//create_datepicker($admin_lang['label']['date'],'date','eng','',true,$result->id);
                            //echo '<div class="uk-width-medium-1-2"></div>';
							create_videolink($admin_lang['label']['content_page_desc_video'],'video_links','eng','',true,$result->id);
							
							//create_gallery($admin_lang['label']['slider_images'],'gallery_images','300&times;300','eng','',true,$result->id);
							echo' <div class="uk-width-medium-1-2">
							<label>Category</label>
							<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="category">
								<option value="1">Videos</option>
							</select>
							</div>';
							echo '<div class="uk-width-medium-1-2"></div>';	
						}
					?>
								

                            <!--End Content Divs-->
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_title']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" cols="30" rows="4" class="md-input required" id="meta_title_eng" name="meta_title_eng" value="<?php echo $result->meta_title_eng; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_title']; ?> </label>
                                <div class="uk-form-row">
                                    <input cols="30" rows="4" class="md-input" name="meta_title_arb" id="meta_title_arb" value="<?php echo $result->meta_title_arb; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_desc']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input required" id="meta_desc_eng" name="meta_desc_eng"><?php echo $result->meta_desc_eng;?></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_desc']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_desc_arb" name="meta_desc_arb"><?php echo $result->meta_desc_arb;?></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_eng_meta_keys']; ?></label>
                                <div class="uk-form-row">
                                    <textarea class="md-input required" id="meta_keywords_eng" name="meta_keywords_eng"><?php echo $result->meta_keywords_eng;?></textarea>
                                    <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['content_arb_meta_keys']; ?> </label>
                                <div class="uk-form-row">
                                    <textarea class="md-input" id="meta_keywords_arb" name="meta_keywords_arb"><?php echo $result->meta_keywords_arb;?></textarea>
                                    <p><?php echo $admin_lang['label']['content_meta_help_text']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_update_content']; ?>" href="javascript:void(0);" id="saveAbout">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
        <!-- light box for image -->
        <button class="md-btn" id="btn_thumb" data-uk-modal="{target:'#modal_lightbox'}" style="display:none;">Open</button>
        <div class="uk-modal" id="modal_lightbox">
            <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
                <img id="img_url" src="" alt="" style="background: #f5f5f5;"/>
            </div>
        </div>
        <!-- end light box for image -->

    </div>

	<!--
	
		<div class="field_wrapper">
			<div>
				<input type="text" name="field_name[]" value=""/>
				<a href="javascript:void(0);" class="add_button" title="Add field"><img src="add-icon.png"/></a>
			</div>
		</div> 
	
	-->

</div>

<script language="javascript">
    $(document).ready(function () {

        $('#saveAbout').click(function () {
            $(".page_loader").show();
            $("#updateAbout").attr("action", "<?php echo base_url();?>ems/content/update/id/<?php echo $result->id; ?>");
            $("#updateAbout").attr("target", "");
            $('#updateAbout').submit();
            $(".required").each(function(){
                if($(this).val() == ''){
                    setTimeout(function(){
                        $(".page_loader").hide();
                    },500);
                }
            });
        });
        $("#updateAbout").validate();
        setInterval(function(){$('#success_msg').html(''); }, 3000);

        $('.img_thumb').click(function () {
            $('#img_url').attr("src","");
            $('#btn_thumb').click();
            var img_src = $(this).attr("src");
            $('#img_url').attr("src",img_src);
        });
    });
</script>

<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    
	var fieldHTML = '<div><label for="kUI_datetimepicker_range_end" class="uk-form-label">Event Date </label><input type="text" class="datetimepicker" value="" name="date[]"/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img style="margin-left: 12px; height: 25px;" src="<?php echo base_url('assets/admin/assets/img/');?>/remove-icon.png"/></a></div>'; 
    var x = 1; //Initial field counter is 1
	
	
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
			
			$(".datetimepicker").kendoDateTimePicker({
				dateInput: true
			});
			
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

<script>
	 $(document).ready(function () {
		// create DateTimePicker from input HTML element
		//value: new Date(), Commenting this because its picking up the current date and time
		$(".datetimepicker").kendoDateTimePicker({
			dateInput: true
		});
	});
	$(document).ready(function () {
		var seats = $("input[name='eng_seats']").val();
		if(seats == ''){
			$("input[name='eng_seats']").val('0');
		}
	});
 </script>
