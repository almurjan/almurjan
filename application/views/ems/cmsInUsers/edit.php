<script language="javascript">
function getGroup(gid) {

    $('#groupdiv').html('<div><img src="<?php echo base_url();?>assets/images/loader.gif" height="50"></div>');

    $.ajax({

        type: "POST",

        url: "ems/cmsInUsers/getGroupTable",

        data: {gid: gid}

    }).done(function(data) {
		$("#groupdiv").html(data);
    });

}

$(document).ready(function() {
    $("#pass").click(function() {
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = 8;
        var randomstring = '';
        var charCount = 0;
        var numCount = 0;

        for (var i = 0; i < string_length; i++) {
            // If random bit is 0, there are less than 3 digits already saved, and there are not already 5 characters saved, generate a numeric value.
            if ((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
                var rnum = Math.floor(Math.random() * 10);
                randomstring += rnum;
                numCount += 1;
            } else {
                // If any of the above criteria fail, go ahead and generate an alpha character from the chars string
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum, rnum + 1);
                charCount += 1;
            }
        }
        $('#usr_pass').val(randomstring);

    //document.getElementById('usr_pass').Value = "randomstring";
    //alert(randomstring);
    });
});

</script>
<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
    <?php echo form_open('ems/cmsInUsers/upd_user_rec', array('method' => 'post', 'id' => 'site_form')); ?>
    <input type="hidden" value="" id="pub_val" name="pub_val">
    <input type="hidden" value="<?php echo $results[0]['id']; ?>" id="id" name="id">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['cms_users']; ?> - <?php echo $admin_lang['label']['edit']; ?></h1>
    </div>
    <div id="page_content_inner">
        <a href="<?php echo base_url().'ems/cmsInUsers/';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['cms_users_username']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required" id="usr_uname" name="usr_uname" value="<?php echo $results[0]['usr_uname']; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['phone_no']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required digits" id="usr_phone" name="usr_phone" value="<?php echo $results[0]['usr_phone']; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['email']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required email" id="usr_email" name="usr_email" value="<?php echo $results[0]['usr_email']; ?>" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['password']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input inputFieldForVoucher" id="usr_pass" name="usr_pass" value=""/>
                                    <img id="pass" src="<?php echo base_url();?>assets/images/generate_password.png" width="108" height="23"border="0" alt="" />
                                </div>
                            </div>
							
							<!-- 
								onchange="javacsript:getGroup(this.value)"
								commenting this because its making issue.
							-->
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row" >
                                    <label><?php echo $admin_lang['label']['cms_users_group']; ?></label>
                                    <select class="required" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="<?php echo $admin_lang['label']['cms_users_select_group']; ?>" name="usr_grp_id" id="usr_grp_id" >
                                        <option value="" disabled><?php echo $admin_lang['label']['select']; ?></option>
                                        <?php
										$selected = $results[0]['usr_grp_id'];
                                        foreach($groups as $result=>$val){
                                            foreach($val as $value){ ?>
                                                <option value="<?php echo $value['id']; ?>"  <?php $opt = $value['id'];
                                                if ($opt == $selected) { ?>selected="selected"<?php } ?> ><?php echo $value['eng_grp_title']; ?></option>
                                            <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <label><?php echo $admin_lang['label']['cms_users_role']; ?></label>
                                <div class="uk-overflow-container groupdiv" id="groupdiv">
                                    <table class="uk-table">
                                        <thead>
                                        <tr>
                                            <th>
                                                <input value="none" type="checkbox" name="website" disabled/>
                                                <label for="website"></label>
                                                <?php echo $admin_lang['label']['cms_users_sections']; ?>
                                            </th>
                                            <th>
                                                <input value="none" type="checkbox" name="create" disabled/>
                                                <label for="create"></label>
                                                <?php echo $admin_lang['label']['cms_users_create']; ?>
                                            </th>
                                            <th>
                                                <input value="none" type="checkbox" name="read" disabled/>
                                                <label for="read"></label>
                                                <?php echo $admin_lang['label']['cms_users_read']; ?>
                                            </th>
                                            <th>
                                                <input value="none" type="checkbox" name="update" disabled/>
                                                <label for="update"></label>
                                                <?php echo $admin_lang['label']['cms_users_update']; ?>
                                            </th>
                                            <th>
                                                <input value="none" type="checkbox" name="delete" disabled/>
                                                <label for="delete"></label>
                                                <?php echo $admin_lang['label']['cms_users_delete']; ?>
                                            </th>
                                            <th>
                                                <input value="none" type="checkbox" name="publish" disabled/>
                                                <label for="publish"></label>
                                                <?php echo $admin_lang['label']['cms_users_publish']; ?>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $selected = $results[0]['usr_grp_id'];
                                        //echo '<script> getGroup(' . $selected . ');</script>';
                                        $count=1;
                                        /*echo '<pre>';
                                        print_r($siteList);
                                        exit;*/
                                        foreach($siteList as $result=>$val){
                                        $name_of_page = getSectionNameByID($val['secn_id']);
										if($name_of_page != ''){
										?>
                                        <tr>
                                            <td id="select_<?php echo $val['id']?>">
                                                <input style="visibility:visible" value="<?php echo $val['id']; ?>" type="checkbox" name="select[<?php echo $count?>]" id="select[<?php echo $count?>]" checked disabled/>
                                                <label for="select[<?php echo $count; ?>]"></label>
                                                <?php echo $name_of_page; ?>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="create_<?php echo $val['id']?>" <?php echo ($val['grp_sec_create'] == 1?'checked':'');?> id="create_<?php echo $val['id']?>" value="1" disabled/>
                                                <label for="create_<?php echo $val['id']?>"></label>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="read_<?php echo $val['id']?>" <?php echo ($val['grp_sec_read'] == 1?'checked':'');?> id="read_<?php echo $val['id']?>" value="1" disabled/>
                                                <label for="read_<?php echo  $val['id']?>"></label>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="upd_<?php echo $val['id']?>" <?php echo ($val['grp_sec_update'] == 1?'checked':'');?> id="upd_<?php echo $val['id']?>" value="1" disabled/>
                                                <label for="upd_<?php echo $val['id']?>"></label>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="del_<?php echo $val['id']?>" <?php echo ($val['grp_sec_delete'] == 1?'checked':'');?> id="del_<?php echo $val['id']?>" value="1" disabled/>
                                                <label for="del_<?php echo $val['id']?>"></label>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="pub_<?php echo $val['id']?>" <?php echo ($val['grp_sec_pub'] == 1?'checked':'');?> id="pub_<?php echo $val['id']?>" value="1" disabled/>
                                                <label for="pub_<?php echo $val['id']?>"></label>
                                            </td>
                                        </tr>
                                        <?php $count++; } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_update_content']; ?>" href="javascript:void(0);" id="submit">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function () 
		
        // validate the comment form when it is submitted
        $('#submit').click(function(e) {
            $(".page_loader").show();
            e.preventDefault();
            $("#site_form").submit();
            $("#siteup_form").submit();
        });
        $("#site_form").validate();
        $("#siteup_form").validate();
    });
</script>
<!--End New HTML-->