<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="shortcut icon" href="<?php echo base_url();?>assets/frontend/images/favicon.png" type="image/ico" />
    <title>Admin | Login</title>

    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/login_page.css" media="all">
    <script> var base_url =  '<?php echo base_url();?>';</script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/html5.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
    <!-- altair admin -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/main.min.css" media="all">
    <!-- uikit -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
	<style>
		.warningerror{
			text-align: center;
			color: red;
		}
	</style>
	
    <script type="text/javascript">

        $(document).ready(function(){

            $('#submit_form').click(function(){

                $('#overlay_form').submit();
            });
            //$('#overlay_form').validate();

//open popup
            $("#pop").click(function(){
                $("#overlay_form").fadeIn(1000);
                positionPopup();

            });

            $("#pop2").click(function(){
                $("#overlay_form").fadeIn(1000);
                positionPopup();
            });

//close popup
            $("#close").click(function(){
                $("#overlay_form").fadeOut(500);
            });
        });

        //position the popup at the center of the page
        function positionPopup(){
            if(!$("#overlay_form").is(':visible')){
                return;
            }
            $("#overlay_form").css({
                left: ($(window).width() - $('#overlay_form').width()) / 2,
                top: ($(window).height() - $('#overlay_form').height()) / 2,
                height:170,
                width:400,
                position:'absolute'
            });
        }

        //maintain the popup at center of the page when browser resized
        $(window).bind('resize',positionPopup);
        $("#overlay_form").focus();

    </script>
</head>

<body class="login_page">
<?php $config_settings = getConfigurationSetting(); ?>
<div class="page_loader"></div>
<div class="login_page_wrapper">
    <div class="md-card" id="login_card">

        <div class="md-card-content large-padding" id="login_form">
            <div class="login_heading">
                <h2><?php echo ($lang == 'eng' ? $config_settings->project_name : $config_settings->project_name_arb); ?> Admin Panel</h2>
            </div>
            <?php echo form_open('ems/admin_login/login',array('method'=>'post','id'=>'login_form'));?>
            <div class="login_msg"></div>
            <div class="uk-form-row">
                <label for="login_email">Email</label>
                <input class="md-input" name="username" id="username" type="text" autocomplete="off" value=""  required/>
            </div>
            <div class="uk-form-row">
                <label for="login_password">Password</label>
                <input class="md-input" type="password" id="pass" name="password"  autocomplete="off" value=""  required/>
            </div>
            <div class="uk-form-row">
                <a href="javascript:void(0)" data-uk-modal="{target:'#modal_forgotpassword'}">Forgot Password?</a>
            </div>
            <div class="uk-margin-medium-top">
                <button type="submit" id="btn_login" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</button>
            </div>
            <div class="uk-margin-top">
            </div>
            <?php if(validation_errors()){ echo _erMsg2(validation_errors());}
            if($this->session->flashdata('message')) {echo $this->session->flashdata('message');} ?>
            </form>
        </div>

    </div>

</div>



<button class="md-btn" id="btn_forgotpassword" data-uk-modal="{target:'#modal_forgotpassword'}" style="display:none;">Open</button>
<div class="uk-width-medium-1-2">
    <div class="uk-modal" id="modal_forgotpassword" style="display: none;">
        <div class="uk-modal-dialog">
            <button type="button" class="uk-modal-close uk-close"></button>
            <h2 id="message-heading">Forgot Password</h2>
            <form action="<?php echo base_url().'ems/admin-login/forgot_password';?>" method="post" novalidate="novalidate">
                <div class="uk-form-row">
                    <label for="login_email">Username</label>
                    <input class="md-input required" name="name" id="name" type="text" autocomplete="off" value=""/>
                </div>
                <div class="uk-form-row">
                    <label for="login_password">Email</label>
                    <input class="md-input required" type="text" id="email" name="email" autocomplete="off" value=""/>
                </div>
                <div class="uk-form-row">
                    <input class="md-btn md-btn-primary" type="submit" id="submit" name="submit" value="Submit"/>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
<script src="<?php echo base_url();?>assets/admin/assets/js/common.min.js"></script>
<!-- uikit functions -->
<script src="<?php echo base_url();?>assets/admin/assets/js/uikit_custom.min.js"></script>
<!-- altair common functions/helpers -->
<script src="<?php echo base_url();?>assets/admin/assets/js/altair_admin_common.min.js"></script>

<!-- altair login page functions -->
<script src="<?php echo base_url();?>assets/admin/assets/js/pages/login.min.js"></script>
</html>
<script language="javascript">
    $(document).ready(function() {
        // validate the comment form when it is submitted
        //$("#login_form").validate();

        $('#btn_password').click(function () {
            $("#btn_forgotpassword").click();
        });
    });
</script>

<!------------------------------------------- End New Theme ------------------------------------------------->
