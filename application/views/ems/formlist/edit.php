<div class="content_row">

    <div class="light_shadow_bar"></div>

    <div class="black_bar_main">

        <div class="inner_left_wrap">

            <div class="inner_left_heading"><?php echo getsiteTitle(1); ?></div>

            <div class="inner_left_headingsmalll">Website</div>

        </div>

        <div class="inner_mid_heading">Manage Dropdown Values</div>

        <div class="get_help"><a id="pop">Get Help</a><span style="padding-top:20px; padding-left:8px; float:right"><a
                    id="pop2"><img src="assets/images/hepl_icon.png" width="20" height="20"/></a></span></div>

        <div class="side_bar_wrap" style="border-left:1px solid #fff;"><span><img src="assets/images/sidebar_icon.png"
                                                                                  width="15" height="15"/></span>Sidebar
        </div>

    </div>

    <div class="black_shadow"></div>

</div> 
 <!--gray_panel-->
	<div class="inner_gray_panel" >

               <!--side_right-->
               <div class="side_right">
                 <div class="seprator_horizontal"></div>                              
                 <!-- status bar - start-->  
                 <?php // $pub = $res->pub_status;?>
                 <?php // echo get_status_bar($pub);?>               
                 <!-- status bar - end-->               

               </div><!--side_right-->
				<!--middle_content_areainner-->
                       <div class="middle_content_areainner" style="padding-top:0px;">

                     <!--inner_menu_bar-->

                      <div class="inner_menu_bar" style="width:22%;">          

                     <ul>

                     <?php //echo get_updated_button("saveSocial","Save","edit");?>

					 <li><a id="listopt">Save</a></li>

                       <li><a href="ems/dashboard/manage">Cancel</a></li>

                     </ul>
                     </div><!--inner_menu_bar-->
<div style="padding-top:90px;" class="admin_manage_panel">
 <?php echo form_open_multipart('ems/formlist/save',array('method'=>'post','id'=>'listform'));?>
		 <input type="hidden" value="<?php echo $res->pub_status; ?>" id="pub_val" name="pub_val">      
		   <div class="table_content_row">
               

<table width="100%" border="0" class="edit_table" id="occupation">
  <tbody>
          <tr><td align="center" width="40%" colspan="3">&nbsp;  </td></tr>
  <tr class="blue_row">
    <td width="2%" colspan="3" style="padding:5px;"><h3>Career Level Dropdown for Recruitment</h3></td>
  </tr>
  <tr class="gray_row">
    <td style="padding:10px;">English</td>
    <td style="padding:10px;">Arabic</td>
     <td style="padding:10px;">&nbsp;</td>
    </tr>
     <?php

     $eng_category = explode(',',$res->eng_category);

    $arb_category = explode(',',$res->arb_category);

    $n = 0;

    foreach($eng_category as $key => $ocu){
        if(!empty($ocu)){
     $n++;
     ?>
    <tr class="gray_row" id="category_-<?php echo $n; ?>">
    <td style="padding:10px;">
    <input type="text" class="required" name="eng_category[]" id="eng_category" value="<?php echo $ocu; ?>"></td>
    <td style="padding:10px;">
     <input type="text" class="required" name="arb_category[]" id="arb_category" value="<?php echo
     $arb_category[$key]; ?>" style="direction: rtl;">
</td>
<td style="padding:10px;">
        <button type="button" onclick="remove_row('category','-<?php echo $n; ?>')" >Remove Row</button>
</td>
</tr>

    <?php } } ?>
<tr class="gray_row" id="category_1">
   <td style="padding:10px;">
    <input type="text" class="required" name="eng_category[]" id="eng_category" value=""></td>
    <td style="padding:10px;">
     <input type="text" class="required" name="arb_category[]" id="arb_category" value="" style="direction: rtl;">
</td>
<td style="padding:10px;">
        <button type="button" name="add_category" id="add_category">Add Row</button>
</td>
</tr>
</tbody></table>


 </div>
	 <?php echo form_close();?>                           
 </div>
</div><!--middle_content_areainner-->
<script language="javascript">
$().ready(function() {
	$('#listopt').click(function(){
		$('#listform').submit();
		});
	$("#listopt").validate();

	var x=1;
    $('#add_cities').click(function () {
			   x++;
			   var label = "'cities'";
		   var row ='<tr class="gray_row" id="cities_'+x+'"><td style="padding:10px;"><input type="text" class="required" name="city_en[]" id="city_en" value=""></td><td style="padding:10px;"><input type="text" class="required" name="city_ar[]" id="city_ar" value="" style="direction: rtl;"></td><td style="padding:10px;"><button type="button" onclick="remove_row('+label+','+x+')" >Remove Row</button> </td></tr>';
		   $('#cities tbody').append(row);
		  });
		  	var c=1;
    $('#add_category').click(function () {
			   c++;
			   var label = "'category'";
		   var row ='<tr class="gray_row" id="category_'+c+'"> <td style="padding:10px;"><input type="text" class="required" name="eng_category[]" id="eng_category" value=""></td><td style="padding:10px;"><input type="text" class="required" name="arb_category[]" id="arb_category" value="" style="direction: rtl;"></td><td style="padding:10px;"><button type="button" onclick="remove_row('+label+','+c+')" >Remove Row</button> </td></tr>';
		   $('#occupation tbody').append(row);
		  });
});
	 function remove_row(a,b){

		$('#'+a+'_'+b).remove();  
  }
</script>