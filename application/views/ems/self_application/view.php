<!--New HTML-->
<?php $admin_lang = check_admin_lang();

?>
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo 'Self Application'; ?> - <?php echo $admin_lang['label']['view']; ?></h1>
    </div>
    <div id="page_content_inner">
        <a href="<?php echo base_url();?>ems/self_application" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-xMedium-10-10  uk-width-large-10-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                <label><?php echo 'Title'; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input" name="" value="<?php echo $res->title; ?>" readonly/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <label><?php echo $admin_lang['label']['full_name']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input" name="" value="<?php echo $res->name; ?>" readonly/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <label><?php echo $admin_lang['label']['email']; ?> </label>
                                <div class="uk-form-row">
                                    <input class="md-input" name="" value="<?php echo $res->email; ?>" readonly/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <label><?php echo $admin_lang['label']['work_experience']; ?> </label>
                                <div class="uk-form-row">
                                    <input class="md-input" name="" value="<?php echo $res->experience; ?>" readonly/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <label><?php echo 'Link'; ?> </label>
                                <div class="uk-form-row">
                                    <input class="md-input" name="" value="<?php echo $res->link; ?>" readonly/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <label><?php echo $admin_lang['label']['c_v']; ?></label>
                                <div class="uk-form-row">
                                    <a href="<?php echo base_url().'uploads/cvs/'.$res->c_v; ?>" download>
                                        <img height="25" width="40" src="<?php echo base_url().'assets/images/download.png';?>"></img>
                                    </a>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">

                            </div>
                            <div class="uk-width-medium-1-1">
                                <label><?php echo $admin_lang['label']['submit_date']; ?> </label>
                                <div class="uk-form-row">
                                    <input class="md-input" name="" value="<?php echo date("Y-m-d", strtotime($res->created_at)); ?>" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End New HTML-->
