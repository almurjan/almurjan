<?php
$admin_lang = check_admin_lang();

$self_application = getPageIdbyTemplate('self_application');

?>

<style>
    .k-datepicker{
        width: 100%;
        margin-top: 6px;
    }
    .uk-grid{
        margin-top: 10px;
    }
</style>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){

        $('a.delete').click(function (){
            if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {
                $(".page_loader").show();
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?php echo base_url(); ?>' + 'ems/self_application/deleteQuote',
                    data: {id: id},
                    success: function (result) {
                    }
                });
                location.reload();
            }
        });

        $('a.export').click(function (){
            var id = [];
            var input_fields = '';
            id = $(this).attr('data-id');
            input_fields = input_fields + '<input type="hidden" name="id[]" value="'+id+'">';
            input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="1" />';
            $("#excel_report").html(input_fields);
            $("#excel_report").submit();
        });

        $('#export_to_excel_All').click(function () {
            var input_fields = '';
            input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="0" />'
            $("#excel_report").submit();

        });

    });
</script>
<!--New HTML-->
<form id="excel_report" method="post" action="<?php echo base_url(); ?>ems/mediclinic/export_excel">
</form>
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <!--<h1 id="product_edit_name"><?php /*echo $admin_lang['label']['careers']; */?></h1>-->
        <h1 id="product_edit_name"><?php echo 'Mediclinic Export'; ?></h1>
        <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
    </div>
    <div id="page_content_inner">
        
        <div class="uk-grid" data-uk-grid-margin>
          
            <div class="uk-width-1-4">

                <!--<select id="select_demo_5" data-md-selectize data-md-selectize-bottom title="job title" name="job_title" id="job_title" autocomplete="off" class="job_title">
                    <option value=""><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Select Job Title' : 'اختر المسمى الوظيفي'); */?> </option>
                    <?php /*foreach($careers_listings as $career_listing){
                        if($job_id == $career_listing->id){
                            echo "here";
                            $selected = "selected";
                        }
                        else{
                            $selected = "";
                        }
                        echo "<option value='".$career_listing->id."'"." ".$selected.">".pageSubTitle($career_listing->id, $admin_lang['admin_lang'])."</option>";
                    }  */?>
                </select>-->
            </div>

            <div class="uk-width-1-4">

               <!-- --><?php
/*                $selected_hi = "";
                $selected_med = "";
                $selected_grad = "";
                $selected_mas = "";
                if(isset($_COOKIE['education'])){
                    if($_COOKIE['education'] == 'High School' || $_COOKIE['education'] == 'المدرسة الثنوية'){
                        $selected_hi = "selected";
                    }else if($_COOKIE['education'] == 'Diploma' || $_COOKIE['education'] == 'دبلوم') {
                        $selected_hi = "";
                        $selected_med = "selected";
                    }else if($_COOKIE['education'] == 'Bachelor' || $_COOKIE['education'] == 'بكالوريوس') {
                        $selected_hi = "";
                        $selected_med = "";
                        $selected_grad = "selected";
                    }else if($_COOKIE['education'] == 'Master' || $_COOKIE['education'] == 'ماجستير') {
                        $selected_hi = "";
                        $selected_med = "";
                        $selected_grad = "";
                        $selected_mas = "selected";
                    }
                }

                */?>
                <!--<select id="select_demo_5" data-md-selectize data-md-selectize-bottom title="education" name="education" id="education" autocomplete="off" class="education">
					<option value=""><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Select Education' : 'اختر التعليم'); */?></option>
					<option value="<?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'High School' : 'المدرسة الثنوية'); */?>" <?php /*echo $selected_hi; */?> ><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'High School' : 'المدرسة الثنوية'); */?></option>
					<option value="<?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Diploma' : 'دبلوم'); */?>" <?php /*echo $selected_med; */?> ><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Diploma' : 'دبلوم'); */?></option>
					<option value="<?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Bachelor' : 'بكالوريوس'); */?>" <?php /*echo $selected_grad; */?> ><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Bachelor' : 'بكالوريوس'); */?></option>
					<option value="<?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Master' : 'ماجستير'); */?>" <?php /*echo $selected_mas; */?> ><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Master' : 'ماجستير'); */?></option>
				</select>-->
            </div>

            <div class="uk-width-1-4">
              
                <!--<select id="select_demo_5" data-md-selectize data-md-selectize-bottom title="gender" name="gender" id="gender" autocomplete="off" class="gender">
					<option value=""><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Select Gender' : 'اختر نوع'); */?></option>
					<option value="<?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'male' : 'male'); */?>" <?php /*echo $selected_ma; */?> ><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Male' : 'ذكر'); */?></option>
					<option value="<?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'female' : 'female'); */?>" <?php /*echo $selected_fe; */?> ><?php /*echo($admin_lang['admin_lang'] == 'eng' ? 'Female' : 'انثى'); */?></option>
				</select>-->
            </div>
        </div>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <a href="javascript:void(0);" id="export_to_excel_All" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_export_all']; ?>"><?php echo $admin_lang['label']['export_all']; ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--End New HTML-->


