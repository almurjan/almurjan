<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
    <?php echo form_open_multipart('ems/menu/update/id/' . $result->id, array('method' => 'post', 'id' => 'updateAbout')); ?>
    <input type="hidden" value="<?php echo $result->pub_status; ?>" id="pub_status" name="pub_status">
    <input type="hidden" value="<?php echo $result->id; ?>" id="gid" name="gid">
    <input type="hidden" value="1" id="pub_status" name="pub_status">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['menu_edit_name']; ?></h1>
    </div>
    <div id="page_content_inner">
        <a href="<?php echo base_url().'ems/menu';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"> <?php echo $admin_lang['label']['back']; ?></a>
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-medium-1-2 uk-row-first">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                <label><?php echo $admin_lang['label']['menu_add_name']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required" id="title" name="title" value="<?php echo html_escape($result->title); ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary md-btn-primary autoTooltip" href="javascript:void(0);" id="saveAbout" title="<?php echo $admin_lang['label']['tooltip_update_content']; ?>">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function () {
        $('#saveAbout').click(function(e) {
            $(".page_loader").show();
            $('#updateAbout').submit();
        });
        $("#updateAbout").validate();
    });
</script>
<!--End New HTML-->
