<style type="text/css">
    html {
        background-color: #eee;
    }

    /*body {
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
        color: #444;
        background-color: #fff;
        font-size: 13px;
        font-family: Freesans, sans-serif;
        padding: 2em 4em;
        width: 860px;
        margin: 15px auto;
        box-shadow: 1px 1px 8px #444;
        -mox-box-shadow: 1px 1px 8px #444;
        */-webkit-box-shadow: 1px -1px 8px #444;
    }

    a,a:visited {
        color: #4183C4;
        text-decoration: none;
    }

    a:hover {
        text-decoration: underline;
    }

    pre,code {
        font-size: 12px;
    }

    pre {
        width: 100%;
        overflow: auto;
    }

    small {
        font-size: 90%;
    }

    small code {
        font-size: 11px;
    }

    .placeholder {
        outline: 1px dashed #4183C4;
    }

    .mjs-nestedSortable-error {
        background: #fbe3e4;
        border-color: transparent;
    }

    #tree {
        width: 550px;
        margin: 0;
    }

    ol {
        max-width: 450px;
        padding-left: 25px;
    }

    ol.sortable,ol.sortable ol {
        list-style-type: none;
    }

    .sortable li div {
        border: 1px solid #d4d4d4;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        cursor: move;
        border-color: #D4D4D4 #D4D4D4 #BCBCBC;
        margin: 0;
        padding: 3px;
    }

    li.mjs-nestedSortable-collapsed.mjs-nestedSortable-hovering div {
        border-color: #999;
    }

    .disclose, .expandEditor {
        cursor: pointer;
        width: 20px;
        display: none;
    }

    .sortable li.mjs-nestedSortable-collapsed > ol {
        display: none;
    }

    .sortable li.mjs-nestedSortable-branch > div > .disclose {
        display: inline-block;
    }

    .sortable span.ui-icon {
        display: inline-block;
        margin: 0;
        padding: 0;
    }

    .menuDiv {
        background: #EBEBEB;
    }

    .menuEdit {
        background: #FFF;
    }

    .itemTitle {
        vertical-align: middle;
        cursor: pointer;
    }

    .deleteMenu {
        float: right;
        cursor: pointer;
    }

    h1 {
        font-size: 2em;
        margin-bottom: 0;
    }

    h2 {
        font-size: 1.2em;
        font-weight: 400;
        font-style: italic;
        margin-top: .2em;
        margin-bottom: 1.5em;
    }

    h3 {
        font-size: 1em;
        margin: 1em 0 .3em;
    }

    p,ol,ul,pre,form {
        margin-top: 0;
        margin-bottom: 1em;
    }

    dl {
        margin: 0;
    }

    dd {
        margin: 0;
        padding: 0 0 0 1.5em;
    }

    code {
        background: #e5e5e5;
    }

    input {
        vertical-align: text-bottom;
    }

    .notice {
        color: #c33;
    }
    .clearED {clear:both;}
    section.manageMenuEd {padding:90px 20px 0 30px;}
    .manageMenuEd h2 {
        background: #909090 none repeat scroll 0 0;
        border-bottom: 1px solid #000000;
        color: #ffffff;
        font-size: 14px;
        font-style: normal;
        font-weight: normal;
        line-height: 2.5;
        margin: 0;
        padding: 10px 27px 4px;
    }
    .manageMenuEd ol {
        background-color: #939393;
        border-bottom: 1px solid #000000;
        float: left;
        max-width: inherit;
        padding: 0%;
        width: 100%;
    }
    .manageMenuEd ol li {
        background: rgba(0, 0, 0, 0) url("http://blogswizards.com/edesign_wp/assets/images/black_bar.png") no-repeat scroll 0 0;
        border-right: 1px solid #000000;
        padding-left: 8px;
    }
    .manageMenuEd ol li div {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        border-radius: 0;
        font-size: 14px;
        line-height: 1.5;
        padding: 0;
    }
    .manageMenuEd ol li div.menuEdit {background-color: #cccccc; border-left:1px solid #fff; padding: 13px 15px;}
    .manageMenuEd ol li:nth-child(2n+2) div.menuEdit {background-color:#eeeeee ;}
    .manageMenuEd ol li p {margin:0;}
</style>
<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
<?php echo form_open_multipart('ems/menu/update/id/' . $result->id, array('method' => 'post', 'id' => 'updateAbout')); ?>
<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
    <h1 id="product_edit_name"><?php echo $admin_lang['label']['menu_manage']; ?></h1>
</div>
<div id="page_content_inner">
    <a href="<?php echo base_url().'ems/menu';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"> <?php echo $admin_lang['label']['back']; ?></a>
    <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
        <div class="uk-width-medium-1-2 uk-row-first">
            <div class="md-card">
                <div class="md-card-content">
                    <h4 class="heading_a"><?php echo $admin_lang['label']['menu_page_titles']; ?></h4>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <!--<ul class="uk-nestable" data-uk-nestable>
                                <?php
/*                                $i = 0;
                                foreach ($menu_pages as $result => $val) {
                                $page_title = pageTitle($val->page_id);
                                */?>
                                <li class="uk-nestable-item">
                                    <div class="uk-nestable-panel">
                                        <div class="uk-nestable-toggle" data-nestable-action="toggle"></div>
                                        <?php /*echo $page_title.' '.$val->page_id;*/?>
                                    </div>
                                </li>
                                <?php /*} */?>
                                <?php /*$child_pages = fetchMenuParrentPages($menu_id,$val->page_id);
                                if(!empty($child_pages)){ */?>
                                <li class="uk-nestable-item">
                                    <div class="uk-nestable-panel">
                                        <div class="uk-nestable-toggle" data-nestable-action="toggle"></div>
                                        <?php /*echo $page_title.' '.$val->page_id;*/?>
                                    </div>
                                    <ul>
                                        <?php /*foreach($child_pages as $child_page){
                                        $child_page_title = pageTitle($child_page->page_id);
                                        */?>
                                        <li class="uk-nestable-item">
                                            <div class="uk-nestable-panel">
                                                <div class="uk-nestable-toggle" data-nestable-action="toggle"></div>
                                                <?php /*echo $child_page_title; */?>
                                            </div>
                                        </li>
                                        <?php /*} */?>
                                    </ul>
                                </li>
                                <?php /*} */?>
                            </ul>-->
                            <ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">
                                <?php
                                $i = 0;
                                foreach ($menu_pages as $result => $val) {
                                    $page_title = pageTitle($val->page_id, $admin_lang['admin_lang']);
									if($page_title != ''){
                                    ?>
                                    <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_<?php echo $val->page_id;?>" data-foo="bar">
                                        <div class="menuDiv">
                                            <div id="menuEdit<?php echo $val->page_id;?>" class="menuEdit hidden">
                                                <p>
                                                    <?php echo $page_title.' '.$val->page_id;?>
                                                </p>
                                            </div>
                                        </div>
                                        <?php $child_pages = fetchMenuParrentPages($menu_id,$val->page_id);
                                        if(!empty($child_pages)){ ?>
                                            <ol>
                                                <?php foreach($child_pages as $child_page){
                                                    $child_page_title = pageTitle($child_page->page_id, $admin_lang['admin_lang']);
                                                    ?>
                                                    <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_<?php echo $child_page->page_id;?>" data-foo="baz">
                                                        <div class="menuDiv">
                                                            <div id="menuEdit4" class="menuEdit hidden">
                                                                <p>
                                                                    <?php echo $child_page_title; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ol>
                                        <?php } ?>
                                    </li>
                                <?php } } ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
</div>

<!--End New HTML-->