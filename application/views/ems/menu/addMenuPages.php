<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
    <?php echo form_open_multipart('ems/menu/saveMenuPages', array('method' => 'post', 'id' => 'updateAbout')); ?>
    <input type="hidden" value="1" id="pub_status" name="pub_status">
    <input type="hidden" class="input_field required"  value="<?php echo $result->id;?>" name="menu_id" id="title">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['menu_add_pages_to']; ?> <?php echo $result->title ;?></h1>
    </div>
    <div id="page_content_inner">
        <a href="<?php echo base_url().'ems/menu';?>" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"> <?php echo $admin_lang['label']['back']; ?></a>
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
            <div class="uk-width-medium-1-2 uk-row-first">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label><?php echo $admin_lang['label']['menu_add_name']; ?></label>
                                <div class="uk-form-row">
                                    <input type="text" class="md-input required" id="title" name="title" value="<?php echo $result->title;?>" disabled="disabled"/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <div class="uk-overflow-container cmsUserContentRow">
                                    <table class="uk-table">
                                        <tbody>
                                        <?php
                                        $i = 0;
                                        foreach($pages as $page){
											$checkTitle = ($admin_lang['admin_lang'] == 'eng'?$page->eng_title:$page->arb_title);
											if($checkTitle != '' && $page->id != 993){
                                            if(in_array($page->id,$menu_ids)){
                                                $checked = "checked";
                                            }else{
                                                $checked = "";
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    <div class="uk-width-large-1-4 uk-width-medium-1-2">
                                                        <div class="uk-input-group">
                                                            <span class="uk-input-group-addon"><input type="checkbox" name="page_id[]" value="<?php echo $page->id;?>" <?php echo $checked;?> data-md-icheck/></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php echo ($admin_lang['admin_lang'] == 'eng'?$page->eng_title:$page->arb_title); ?>
                                                </td>
                                            </tr>
                                            <input type="hidden" name="parant_id[]" value="<?php echo $menu_pages[$i]->parant_id;  ?>" />
                                            <input type="hidden" name="position[]" value="<?php echo $menu_pages[$i]->position;  ?>" />
                                            <?php $i++; } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary md-btn-primary" href="javascript:void(0);" id="saveAbout">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function () {
        // validate the comment form when it is submitted
        $('#saveAbout').click(function(e) {
            $(".page_loader").show();
            $('#updateAbout').submit();
        });
        $("#updateAbout").validate();
    });
</script>
<!--End New HTML-->
