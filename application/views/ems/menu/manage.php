<?php $admin_lang = check_admin_lang(); ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){

    $('a.delete').click(function (){
        if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {
            $(".page_loader").show();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url(); ?>'+'ems/menu/delete',
                data: {id: id},
                success: function (result) {
                }
            });
            location.reload();
        }
    });

    $('a.publish').click(function (){
        $(".page_loader").show();
        var str = $(this).attr('data-status');
        split_str = str.split('-');
        var status = split_str[0];
        var id = split_str[1];
        $.ajax({
            type: "POST",
            async:false,
            url: '<?php echo base_url(); ?>'+'ems/menu/publish',
            data: {id:id,pub_status:status},
            success: function (result) {
            }
        });
        location.reload();
    });
});
</script>
<!--New HTML-->
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['menu_manage']; ?></h1>
    </div>
    <div id="page_content_inner">
        <a title="<?php echo $admin_lang['label']['tooltip_menu_add']; ?>" href="<?php echo base_url().'ems/menu/addMenu';?>" class="md-btn md-btn-primary autoTooltip"> <?php echo $admin_lang['label']['add']; ?></a>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th><?php echo $admin_lang['label']['Sr']; ?></th>
                                    <th><?php echo $admin_lang['label']['menu_names']; ?></th>
                                    <th><?php echo $admin_lang['label']['active']; ?></th>
                                    <th class="nosort"><?php echo $admin_lang['label']['action']; ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($menus){
                                    $i=0;
                                    foreach($menus as $result=>$val){
                                        if($i == 0){
                                            $tclass = 'autoTooltip';
                                        }else{
                                            $tclass = 'tooltip';
                                        }
                                        ?>
                                        <tr class="<?php echo $val->id; ?>">
                                            <td><?php echo ++$i;?></td>
                                            <td><?php echo $val->title; ?></td>
                                            <td>
                                                <?php if($val->pub_status == 1){;?>
                                                    <a href="javascript:void(0);" class="publish <?php echo $tclass; ?>" id="status" data-status="<?php echo '0-'.$val->id;?>" title="<?php echo $admin_lang['label']['tooltip_publish']; ?>">
                                                        <i class="md-icon material-icons green">check_circle</i>
                                                    </a>
                                                <?php }else{ ?>
                                                    <a href="javascript:void(0);" class="publish <?php echo $tclass; ?>" id="status" data-status="<?php echo '1-'.$val->id;?>" title="<?php echo $admin_lang['label']['tooltip_publish']; ?>">
                                                        <i class="md-icon material-icons black">check_circle</i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <a class=" <?php echo $tclass; ?>" href="<?php echo base_url().'ems/menu/edit/id/'.$val->id;?>" title="<?php echo $admin_lang['label']['tooltip_edit']; ?>">
                                                    <i class="md-icon material-icons">&#xE254;</i>
                                                </a>
                                                <a href="javascript:void(0);" class="uk-margin-left delete <?php echo $tclass; ?>" data-id="<?php echo $val->id;?>" title="<?php echo $admin_lang['label']['tooltip_delete']; ?>">
                                                    <i class="material-icons md-24 delete">&#xE872;</i>
                                                </a>
                                                <a href="<?php echo base_url().'ems/menu/addMenuPages/id/'.$val->id;?>" class="uk-margin-left <?php echo $tclass; ?>" title="<?php echo $admin_lang['label']['tooltip_menu_pages']; ?>">
                                                    <?php echo $admin_lang['label']['menu_add_pages']; ?>
                                                </a>
                                                <a href="<?php echo base_url().'ems/menu/manageMenuPages/'.$val->id;?>" class="uk-margin-left <?php echo $tclass; ?>" title="<?php echo $admin_lang['label']['tooltip_menu_order']; ?>">
                                                    <?php echo $admin_lang['label']['menu_order_pages']; ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php  } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--End New HTML-->