<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
    <!--<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h2 id="product_edit_name">Log</h2>
        <span class="uk-text-muted uk-text-upper uk-text-small"></span>
    </div>-->
    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content"><h3 class="heading_a"><b><?php echo $admin_lang['label']['dashboard_log']; ?></b></h3><br>
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-align-vertical listing dt_default">
                        <tbody>
                        <?php  $i=0;  foreach($data as $result=>$val) {  ?>
                            <tr class="uk-table-middle">
                                <td class="uk-width-2-10 uk-text-nowrap">

                                    <!--GrayRow-->
                                    <span class="uk-badge">
                                    <?php echo $val['log_uname'].' '.$val['log_comments'].' <strong>'.$val['log_module'].'</strong>'.' '.date('Dd M, Y  H:i:s',strtotime($val['log_date'])); ?>
                                </span>

                                </td>
                            </tr>
                            <?php $i++;}?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End New HTML-->