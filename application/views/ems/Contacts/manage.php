<?php $admin_lang = check_admin_lang(); ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){

    $('a.delete').click(function (){
        if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {
            $(".page_loader").show();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url(); ?>' + 'ems/users/deleteContact',
                data: {id: id},
                success: function (result) {
                }
            });
            location.reload();
        }
    });

    $('a.export').click(function (){
        var id = [];
        var input_fields = '';
        id = $(this).attr('data-id');
        input_fields = input_fields + '<input type="hidden" name="id[]" value="'+id+'">';
        input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="1" />';
        $("#excel_report").html(input_fields);
        $("#excel_report").submit();
    });

    $('#export_to_excel_All').click(function () {
        var input_fields = '';
        input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="0" />'
        $("#excel_report").submit();

    });

});
</script>
<style>
    .dataTables_wrapper .uk-overflow-container td, .dataTables_wrapper .uk-overflow-container th {
        white-space: normal !important;
    }
</style>
<!--New HTML-->
<form id="excel_report" method="post" action="<?php echo base_url(); ?>ems/contacts/export_excel">
</form>
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['contacts']; ?></h1>
        <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
    </div>
    <div id="page_content_inner">
		<a href="javascript:void(0);" id="export_to_excel_All" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_export_all']; ?>"><?php echo $admin_lang['label']['export_all']; ?></a>
		
		<a href="<?php echo base_url(); ?>ems/contacts?show_all=clear"  class="md-btn md-btn-primary autoTooltip" title=""><?php echo $admin_lang['label']['show_all']; ?></a>
		
		<div class="" style="float:right; padding-right: 40px;">
			<input class="datetimepickerNew contactDate" name="date_contact"  value="<?php echo (isset($_COOKIE['date_contact']) ? $_COOKIE['date_contact'] : ''); ?>" placeholder="<?php echo($admin_lang['admin_lang'] == 'eng' ? 'Date' : 'التاريخ'); ?>" autocomplete="off" />
		</div>

        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th><?php echo $admin_lang['label']['Sr']; ?></th>
                                    <th><?php echo $admin_lang['label']['full_name']; ?></th>
                                    <th><?php echo $admin_lang['label']['email']; ?></th>
                                    <th><?php echo $admin_lang['label']['mobile']; ?></th>
                                    <th width="300px" style="white-space: normal;"><?php echo $admin_lang['label']['message']; ?></th>
                                    <th><?php echo 'Subject'; ?></th>
									<th><?php echo $admin_lang['label']['created_at']; ?></th>
									<th><?php echo $admin_lang['label']['action']; ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($users){
                                    $i = 1;
                                    foreach ($users as $user) {
                                        if($i == 1){
                                            $tclass = 'autoTooltip';
                                        }else{
                                            $tclass = 'tooltip';
                                        }
                                        ?>
                                        <tr class="<?php echo $user->id ?>">
                                            <td><?php echo $i; ?></td>
											<td><?php echo $user['name']; ?></td>
                                            <td><?php echo $user['email']; ?></td>
											<td><?php echo $user['mobile']; ?></td>
                                            <td><?php echo $user['message'];?></td>
                                            <td><?php echo $user['subject'];?></td>
                                            <td><?php echo date('d M Y h:i',strtotime($user['created_at']));?></td>
                                           <td>
											 <a href="javascript:void(0);" class="uk-margin-left delete autoTooltip tooltipstered" data-id="<?php echo $user['id']; ?>">
												<i class="material-icons md-24 delete">&#xE872;</i>
											</a>
											</td> 
                                        </tr>
                                    <?php $i++; } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<span id="some-id"></span>
    </div>
</div>
<!--End New HTML-->


