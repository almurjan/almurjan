<?php $admin_lang = check_admin_lang(); ?>
<style>
	.k-datepicker{
		width: 100%;
		margin-top: 6px;
	}
	.uk-grid{
		margin-top: 10px;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){



    $('a.delete').click(function (){

        if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {

            $(".page_loader").show();

            var id = $(this).attr('data-id');

            $.ajax({

                type: "POST",

                async: false,

                url: '<?php echo base_url(); ?>' + 'ems/users/deleteRequest',

                data: {id: id},

                success: function (result) {

                }

            });

            location.reload();

        }

    });

    $('a.export').click(function (){
        var id = [];
        var input_fields = '';
        id = $(this).attr('data-id');
        input_fields = input_fields + '<input type="hidden" name="id[]" value="'+id+'">';
        input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="1" />';
        $("#excel_report").html(input_fields);
        $("#excel_report").submit();
    });

    $('#export_to_excel_All').click(function () {
        var input_fields = '';
        input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="0" />'
        $("#excel_report").submit();

    });

});
</script>
<!--New HTML-->
<form id="excel_report" method="post" action="<?php echo base_url(); ?>ems/request_quotation/export_excel">
</form>
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo ($admin_lang['admin_lang'] == 'eng' ? 'Request Quotation' : 'طلب سعر' ); ?></h1>
        <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
    </div>
    <div id="page_content_inner">
		<a href="javascript:void(0);" id="export_to_excel_All" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_export_all']; ?>"><?php echo $admin_lang['label']['export_all']; ?></a>
		
		<a href="<?php echo base_url(); ?>ems/request_quotation?show_all=clear"  class="md-btn md-btn-primary autoTooltip" title=""><?php echo $admin_lang['label']['show_all']; ?></a>
		
		<div class="uk-grid" data-uk-grid-margin>
			<div class="uk-width-1-4">
				<input class="datetimepickerNew date_request" name="date_request"  value="<?php echo (isset($_COOKIE['date_request']) ? $_COOKIE['date_request'] : ''); ?>" placeholder="<?php echo($admin_lang['admin_lang'] == 'eng' ? 'Date' : 'التاريخ'); ?>" autocomplete="off" />
			</div>
			<?php 
				if(isset($_COOKIE['country'])){
					$country = $_COOKIE['country'];
				}
			?>
			<div class="uk-width-1-4">		
				<select id="select_demo_5" data-md-selectize data-md-selectize-bottom title="select country" name="country" id="country" autocomplete="off" class="country">
					<option value=""><?php echo($admin_lang['admin_lang'] == 'eng' ? 'Select Country' : 'اختر بلد'); ?></option>
					<?php getAllCountriesFilter($admin_lang['admin_lang'],$country)?>                               
				</select>
			</div>	
			
			<div class="uk-width-1-4">		
				<select id="select_demo_5" data-md-selectize data-md-selectize-bottom title="select city" name="city" id="city" autocomplete="off" class="city">
					<option value=""><?php echo($admin_lang['admin_lang'] == 'eng' ? 'Select City' : 'اختر مدينة'); ?></option>
					 <?php 
						
						if(isset($_COOKIE['city'])){
							$cityS = $_COOKIE['city'];
						}
		
						foreach($cities as $city){
							if($cityS == $city->city){
								$selected_city = 'selected=selected';
							}
							else{
								$selected_city = '';
							}
							echo '<option value="'.$city->city.'" '.$selected_city.'>'.$city->city.'</option>';
						}
					 ?>
				</select>
			</div>	
			
			
			<div class="uk-width-1-4">		
				<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:"top"}" title="Select with tooltip" name="selectSlideOptions" autocomplete="off" class="services">
				<option value=""><?php echo($admin_lang['admin_lang'] == 'eng' ? 'Choose Product or Service' : 'اختر منتج او خدمة'); ?></option>
				<?php 
					$services_id = getPageIdbyTemplate('our_services');
					$services_listings = ListingContent($services_id);
					
					if(isset($_COOKIE['services'])){
						$services = $_COOKIE['services'];
					}
					
					foreach($services_listings as $listing) {  
					$products[] = ListingContent($listing->id);
					}
					foreach($products as $singleProduct) {
						foreach($singleProduct as $product){
							
							$selected = "";
							if(($lang == 'eng' ? $product->eng_title : $product->arb_title) == $services)
							{
								$selected = 'selected=selected';
								
							}
							$val = ($lang == 'eng' ? $product->eng_title : $product->arb_title);
							echo "<option value='".$val."' $selected>".$val."</option>";
						}
					}
				?>
				
				</select>
			</div>	
			
			
		</div>	
	
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th><?php echo $admin_lang['label']['Sr']; ?></th>
                                    <th><?php echo $admin_lang['label']['org_name']; ?></th>
									<th><?php echo $admin_lang['label']['name']; ?></th>
									<th><?php echo $admin_lang['label']['city']; ?></th>
									<th><?php echo $admin_lang['label']['coumtry']; ?></th>
									<th><?php echo $admin_lang['label']['req_ser']; ?></th>
									<th><?php echo $admin_lang['label']['created_at']; ?></th>
									<th><?php echo $admin_lang['label']['action']; ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($users){
                                    $i = 1;
                                    foreach ($users as $user) {
                                        if($i == 1){
                                            $tclass = 'autoTooltip';
                                        }else{
                                            $tclass = 'tooltip';
                                        }
                                        ?>
                                        <tr class="<?php echo $user->id ?>">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $user['organization_name']; ?></td>
                                            <td><?php echo $user['first_name'].' '.$user['last_name']; ?></td>
											<td><?php echo $user['city']; ?></td>
											<td><?php echo $user['country']; ?></td>
											<td><?php echo $user['product_service']; ?></td>  
											<td><?php echo date('d M Y h:i',strtotime($user['created_at']));?></td>
											<td>
												
											 <a class="edit <?php echo $tclass; ?>" href="<?php echo base_url().'ems/request_quotation/view/id/'.$user['id'];?>" title="<?php echo $admin_lang['label']['tooltip_edit']; ?>">
                                                    <i class="md-icon material-icons">visibility</i>
                                              </a>
											 <a href="javascript:void(0);" class="uk-margin-left delete autoTooltip tooltipstered" data-id="<?php echo $user['id']; ?>">
												<i class="material-icons md-24 delete">&#xE872;</i>
											</a>
											</td>                                     
                                        </tr>
                                    <?php $i++; } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--End New HTML-->


