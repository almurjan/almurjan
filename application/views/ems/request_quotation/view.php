<!--New HTML-->
<?php $admin_lang = check_admin_lang(); ?>
<div id="page_content">
<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
    <h1 id="product_edit_name"><?php echo 'Request Quotation View'; ?></h1>
</div>
<div id="page_content_inner">
<a href="<?php echo base_url();?>ems/request_quotation" class="md-btn md-btn-primary autoTooltip" title="<?php echo $admin_lang['label']['tooltip_go_back']; ?>"><?php echo $admin_lang['label']['back']; ?></a>
<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
    <div class="uk-width-xMedium-10-10  uk-width-large-10-10">
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['org_name']; ?></label>
                        <div class="uk-form-row">
                            <input type="text" class="md-input" name="" value="<?php echo $res->organization_name; ?>" readonly/>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['req_ser']; ?></label>
                        <div class="uk-form-row">
                            <input type="text" class="md-input" name="" value="<?php echo $res->product_service; ?>" readonly/>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['job_title']; ?> </label>
                        <div class="uk-form-row">
                            <input class="md-input" name="" value="<?php echo $res->job_title; ?>" readonly/>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['first_name']; ?> </label>
                        <div class="uk-form-row">
                            <input class="md-input" name="" value="<?php echo $res->first_name; ?>" readonly/>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['last_name']; ?> </label>
                        <div class="uk-form-row">
                            <input class="md-input" name="" value="<?php echo $res->last_name; ?>" readonly/>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['email']; ?> </label>
                        <div class="uk-form-row">
                            <input class="md-input" name="" value="<?php echo $res->email; ?>" readonly/>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['phone']; ?> </label>
                        <div class="uk-form-row">
                            <input class="md-input" name="" value="<?php echo $res->phone; ?>" readonly/>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['city']; ?> </label>
                        <div class="uk-form-row">
                            <input class="md-input" name="" value="<?php echo $res->city; ?>" readonly/>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['country']; ?> </label>
                        <div class="uk-form-row">
                            <input class="md-input" name="" value="<?php echo $res->country; ?>" readonly/>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['message']; ?></label>
                        <div class="uk-form-row">
                            <textarea class="md-input" name="" readonly><?php echo $res->message; ?></textarea>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label><?php echo $admin_lang['label']['submit_date']; ?> </label>
                        <div class="uk-form-row">
                            <input class="md-input" name="" value="<?php echo date("Y-m-d", strtotime($res->created_at)); ?>" readonly/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!--End New HTML-->
