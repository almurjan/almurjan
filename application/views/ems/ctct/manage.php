<?php $admin_lang = check_admin_lang(); ?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){

    $('a.delete').click(function (){
        if (confirm("<?php echo $admin_lang['label']['delete_text']; ?>")) {
            $(".page_loader").show();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url(); ?>' + 'ems/Users/delete',
                data: {id: id},
                success: function (result) {
                }
            });
            location.reload();
        }
    });

    $('a.export').click(function (){
        var id = [];
        var input_fields = '';
        id = $(this).attr('data-id');
        input_fields = input_fields + '<input type="hidden" name="id[]" value="'+id+'">';
        input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="1" />';
        $("#excel_report").html(input_fields);
        $("#excel_report").submit();
    });

    $('#export_to_excel_All').click(function () {
        var input_fields = '';
        input_fields = input_fields + '<input type="hidden" name="excel_type" id="excel_type" value="0" />'
        $("#excel_report").submit();

    });

});
</script>
<!--New HTML-->
<form id="excel_report" method="post" action="<?php echo base_url(); ?>ems/Users/export_excel">
</form>
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name"><?php echo $admin_lang['label']['newsletter_subscribers']; ?></h1>
        <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
    </div>
    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th><?php echo $admin_lang['label']['Sr']; ?></th>
                                    <th><?php echo $admin_lang['label']['email']; ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
								if($users){
                                    $i = 1;
                                    foreach ($users as $user) {
										
                                        if($i == 1){
                                            $tclass = 'autoTooltip';
                                        }else{
                                            $tclass = 'tooltip';
                                        }
                                        ?>
                                        <tr class="">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $user; ?></td>
                                            
                                        </tr>
                                    <?php $i++; } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--End New HTML-->


