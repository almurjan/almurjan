<?php
$home = getPageIdByTemplate('home');
$contactUsId = getPageIdByTemplate('contact_us');
$privacy_policy = getPageIdbyTemplate('privacy_policy');
$contact_purpose_listings = ListingContent($contactUsId);
$cordinates = content_detail('maps_loc',$page_id);
$cordinates_exploded = explode(',',$cordinates);
$social_links = social_links();
?>
<div class="inner-banner-main-box">
    <section class="about-banner" style="background-image: url('<?php echo base_url() . 'assets/script/' . content_detail('eng_contact_us_banner_image_mkey_hdn', $page_id); ?>')">
        <div class="inner-banner-inner-box">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="white-banner-content contact-heading-padding">
                            <?php
                            $exploded_page_sub_title = explode(' ', pageSubTitle($page_id, $lang));
                            $page_sub_title = implode('<br>', $exploded_page_sub_title);
                            ?>
                            <h1><?php echo $page_sub_title; ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="contact-heading text-center contact-small-box">
                    <!--<h2><?php /*echo pageSubTitle($page_id, $lang); */?></h2>-->
                    <h2><?php echo translate($lang, 'Get  in touch', 'ابقى على تواصل'); ?></h2>
                    <h3><?php echo translate($lang, 'Need a help?', 'بحاجة الى مساعدة؟'); ?></h3> <h3><?php echo translate($lang, 'Here’s how to reach us.', 'إليك كيفية الوصول إلينا.'); ?></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="contact-form contact-small-box">
                    <form action="<?php echo base_url('page/submit_contact'); ?>" class="contact_us_form validate" method="post" onsubmit="return false;">
                        <div class="row">
                            <div class="col-12 col-md-6 pb-3">
                                <div class="input-wrap">
                                    <label for=""><?php echo ($lang == 'eng' ? 'Full Name*' : '*الأسم الكامل');?></label>
                                    <input required type="text" class="form-control inputTextOnly" name="name" placeholder="" id="name">
                                    <input type="hidden"  name="lang" id="lang" value="<?php echo $lang ?>" >
                                </div>
                            </div>
                            <div class="col-12 col-md-6 pb-3">
                                <div class="input-wrap">
                                    <label for=""><?php echo ($lang == 'eng' ? 'Phone Number*' : '*رقم الهاتف');?></label>
                                    <input required type="text" class="form-control numberOnly" name="mobile" placeholder="" id="number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6 pb-3">
                                <div class="input-wrap">
                                    <label for=""><?php echo ($lang == 'eng' ? 'Email*' : 'البريد الإلكتروني*');?></label>
                                    <input required type="email" class="form-control" name="email" placeholder="" id="u_email">
                                </div>
                            </div>
                            <div class="col-12 col-md-6 pb-3">
                                <div class="input-wrap">
                                    <label for=""><?php echo ($lang == 'eng' ? 'Subject*' : 'الموضوع*');?></label>
                                    <input required type="text" class="form-control" name="subject" placeholder="" id="subject">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 pb-3">
                                <div class="input-wrap">
                                    <label for=""><?php echo ($lang == 'eng' ? 'Type Your Message Here....*' : '*الرسالة');?></label>
                                    <textarea required class="form-control"  name="message" placeholder="" rows="14" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <div class="g-recaptcha" data-sitekey="6LcI0rUkAAAAAMVWOpuE4mV3Y4R7ua2sjwopF4lt"></div>
                                <p style="color:red; font-weight:700;" id="captcha_message"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <button class="btn btn-outline-light intro-readmore submitbtn" role="button" aria-pressed="true"><?php echo ($lang == 'eng' ? 'Submit' : 'إرسال');?> </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="contact-small-box">
            <div class="row row-cols-1 row-cols-md-3 row-cols-lg-3">
                <div class="col mb-3">
                    <div class="contact-details has-second-color text-center contact-us-icons">
                        <i class="fas fa-map-marker-alt"></i>
                        <h2><?php echo ($lang == 'eng' ? 'Address' : 'العنوان');?></h2>
                        <p><?php echo content_detail($lang.'_desc_contact',$page_id); ?> </p>
                    </div>
                </div>
                <div class="col mb-3">
                    <div class="contact-details has-second-color text-center contact-us-icons contact-us-address">
                        <i class="far fa-envelope"></i>
                        <h2><?php echo ($lang == 'eng' ? 'Address' : 'العنوان');?></h2>
                        <p><?php echo content_detail('eng_phone',$page_id); ?></p>
                        <p><a href="mailto:<?php echo content_detail('eng_mail',$page_id); ?>"><?php echo content_detail('eng_mail',$page_id); ?></a></p>
                    </div>
                </div>
                <div class="col mb-3">
                    <div class="location-map text-center contact-us-icons">
                        <i class="fas fa-map"></i>
                        <div class="google-map">
                            <div id="map" style="height:200px;width: 100%;border:0;"></div>
                        </div>
                    </div>
                </div>
                <!--<div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="contact-social-icons">
                    <a href="<?php /*echo $social_links->soc_tw; */?>" target="_blank"><i class="fab fa-twitter fa-2x"></i></a>
                    <a href="<?php /*echo $social_links->soc_lin; */?>" target="_blank"><i class="fab fa-linkedin fa-2x"></i></a>
                    <a href="<?php /*echo $social_links->soc_you; */?>" target="_blank"><i class="fab fa-youtube fa-2x"></i></a>
                    <a href="<?php /*echo $social_links->soc_insta; */?>" target="_blank"><i class="fab fa-instagram fa-2x"></i></a>
                </div>
            </div>-->
            </div>
        </div>
    </div>
</section>

<script>
    function load_map()
    {
        var uluru = { lat: <?php echo $cordinates_exploded[0]; ?>, lng: <?php echo $cordinates_exploded[1]; ?> };
        /*var uluru_old = { lat: 24.720403782761128, lng: 46.678762435913086 };*/
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: uluru,
            mapTypeId: 'roadmap'
        });
        // var image = "<?php echo base_url('assets/images/pinmap2.png') ?>";
        /*var marker = new google.maps.Marker({
            position: uluru,
            map: map,
            //label: {text: "ALMURJAN", color: "red"},
            icon: image,
            url: "https://www.google.com/maps/place/24%C2%B042'37.5%22N+46%C2%B040'36.1%22E/@24.7104238,46.6745138,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d24.7104238!4d46.6767025"
        });*/
        /*google.maps.event.addListener(marker, 'click', function() {
              window.open(this.url);
          });*/
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYw6WtdLYGmzbjj_rMaFIQYX7K-uOBJ7k&callback=load_map" async defer></script>