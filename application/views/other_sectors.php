<?php
$home = getPageIdbyTemplate('home');
$other_sectors = getPageIdbyTemplate('other_sectors');
$other_sectors_listings = ListingContent($other_sectors);
$slider_one = array();
$slider_two = array();
$slider_three = array();
foreach ($other_sectors_listings as $other_sectors_listing) {
    $category_listings = ListingContent($other_sectors_listing->id);
    foreach ($category_listings as $category_listing) {
        if (content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id)) {
            $slider_one[$category_listing->id] = content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id);
        }
        /*$slider_no = content_detail('slider_number', $category_listing->id);
        if($slider_no==1 && content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id)){
            $slider_one[$category_listing->id] = content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id);
        }elseif($slider_no==2 && content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id)){
            $slider_two[$category_listing->id] = content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id);
        }elseif($slider_no==3 && content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id)){
            $slider_three[$category_listing->id] = content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id);
        }*/
    }
}
?>
<style>
    /*.sector_category .active_cat{
       background-color: #00447C;
       color: #C8A443;
    }*/
</style>
<div class="inner-banner-main-box">
    <!--<section class="about-banner" style="background-image: url('<?php /*echo base_url() . 'assets/script/' . content_detail('eng_other_sector_banner_image_mkey_hdn', $page_id); */ ?>')">
        <div class="inner-banner-inner-box">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="white-banner-content has-blue-color">
                            <?php
    /*                            $exploded_page_sub_title = explode(' ', pageSubTitle($page_id, $lang));
                                $page_sub_title = implode('<br>', $exploded_page_sub_title);
                                */ ?>
                            <h1><?php /*echo $page_sub_title; */ ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <div class="other-sectors-slider">

        <?php
        $slider_images = content_detail('eng_other_sector_banner_images_slider', $page_id);
        if ($slider_images) {
            ?>
            <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <?php
                    $i = 0;
                    $slider_images_list = explode(',', $slider_images);
                    foreach ($slider_images_list as $slider_image) {
                        if ($slider_image == 'eng_other_sector_banner_images_slider') continue;
                        ?>
                        <button type="button" data-bs-target="#carouselExampleIndicators"
                                data-bs-slide-to="<?php echo $i; ?>" class="<?php echo($i == 0 ? 'active' : ''); ?>"
                                aria-current="true" aria-label="Slide <?php echo $i + 1; ?>"></button>
                        <?php $i++;
                    } ?>
                </div>
                <div class="carousel-inner">
                    <?php
                    $i = 0;
                    $slider_images_list = explode(',', $slider_images);
                    foreach ($slider_images_list as $slider_image) {
                        if ($slider_image == 'eng_other_sector_banner_images_slider') continue;
                        ?>
                        <div class="carousel-item <?php echo($i == 0 ? 'active' : ''); ?>">
                            <div class="slider-main-box">
                                <div class="slider-image-main-box">
                                    <div class="slider-image" style="background-image: url('<?php echo base_url() . 'assets/script/' . $slider_image ?>');min-height: 623px;">

                                    </div>
                                </div>
                                <div class="other-sector-banner-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="white-banner-content has-blue-color csr-banner-heading">
                                                    <h1><?php echo pageSubTitle($page_id, $lang); ?></h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $i++;
                    } ?>
                </div>
            </div>
        <?php } ?>

    </div>
</div>
<section class="sectors-section">
    <nav class="sectors-tab-nav">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="nav nav-tabs owl-carousel-sectors-nav owl-carousel owl-theme" id="nav-tab" role="tablist">
                        <?php
                        if (!empty($other_sectors_listings)) {
                            ?>
                            <?php
                            $i = 0;
                            foreach ($other_sectors_listings as $other_sectors_listing) {
                                if (content_detail($lang . '_other_sectors_cat_desc_1', $other_sectors_listing->id) != '') {
                                    $show_blue_border = true;
                                } else {
                                    $show_blue_border = false;
                                }
                                ?>
                                <button class="item nav-link sectors_nav <?php echo($i == 0 ? 'active' : ''); ?>"
                                        id="nav-realestate-tab-<?php echo $other_sectors_listing->id; ?>"
                                        data-bs-toggle="tab"
                                        data-bs-target="#nav-realestate-<?php echo $other_sectors_listing->id; ?>"
                                        type="button" role="tab"
                                        aria-controls="nav-realestate-<?php echo $other_sectors_listing->id; ?>"
                                        aria-selected="true"
                                        data-show-border="<?php echo($show_blue_border ? 1 : 0); ?>"
                                        data-index="<?php echo $i; ?>"
                                        data-desc="<?php echo content_detail($lang . '_other_sectors_cat_desc_1', $other_sectors_listing->id); ?>">
                                    <?php echo pageSubTitle($other_sectors_listing->id, $lang); ?>
                                </button>
                                <?php $i++;
                            } ?>
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="tab-content" id="nav-tabContent">
                    <?php
                    if (!empty($other_sectors_listings)) {
                        ?>
                        <?php
                        $i = 0;
                        foreach ($other_sectors_listings as $other_sectors_listing) { ?>
                            <div class="tab-pane fade <?php echo($i == 0 ? 'active show' : ''); ?>"
                                 id="nav-realestate-<?php echo $other_sectors_listing->id; ?>" role="tabpanel"
                                 aria-labelledby="nav-realestate-tab-<?php echo $other_sectors_listing->id; ?>">
                                <?php if (content_detail($lang . '_other_sectors_cat_desc_1', $other_sectors_listing->id) != '' && content_detail($lang . '_other_sectors_cat_desc_2', $other_sectors_listing->id) != '') { ?>
                                    <div class="row">
                                        <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="content-within-border has-second-color">
                                                <h1><?php echo content_detail($lang . '_other_sectors_cat_desc_1', $other_sectors_listing->id); ?></h1>
                                                <!--<h1>EMPOWERING <br>THE <br><span>ECONOMY</span> THROUGH YEARS OF HARD
                                                    WORK.</h1>-->
                                            </div>
                                        </div>
                                        <div class="col-xxl-8 col-xl-8 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="border-detailed-content">
                                                <?php echo content_detail($lang . '_other_sectors_cat_desc_2', $other_sectors_listing->id); ?>
                                                <!--<p>OUR ETHICAL INVESTMENT APPROACH COMBINES ECONOMIC GAINS WITH SICIETAL
                                                    BENEFIT TO CREATE ENDURING ASSETS AND INCOME GENERATING PROJECTS
                                                    THAT OUTPERFORM THE MARKET.</p>
                                                <p>OUR FOCUS IS ON THE KEY SECTORS OF REAL ESTATE, MEDICINE AND HEALTH,
                                                    FINANCIAL SERVICES, HOSPITALITY, INDUSTRY AND MANUFACTURING,
                                                    TECHNOLOGY, RETAIL AND CINSTRUCTION.</p>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row top-of-page-margin">
                                        <!--<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <a href="javascript:void(0);" onclick="topFunction();">
                                                <div class="top-of-page">
                                                    <p>Top of Page</p>
                                                    <span><img src="<?php /*echo base_url('assets/frontend/images/mouse.svg') */ ?>"
                                                               alt=""></span>
                                                </div>
                                            </a>
                                        </div>-->
                                    </div>
                                <?php } ?>
                                <?php
                                $j = 0;
                                $category_listings = ListingContent($other_sectors_listing->id);
                                foreach ($category_listings as $category_listing) {
                                    $flag = isFlagValue($category_listing->id);
                                    if ($flag->is_flag == 1) {
                                        $repeated_class = 'isFlag';
                                        $repeated_íd = 1;
                                    } else {
                                        $repeated_class = '';
                                        $repeated_íd = 0;
                                    }
                                    if (content_detail('eng_other_sectors_logo_mkey_hdn', $category_listing->id) != '') {

                                        //$category_listing_image = imageSetDimenssion(content_detail('eng_other_sectors_logo_mkey_hdn', $category_listing->id), 170, 170, 1);

                                        $category_listing_image = base_url() . 'assets/script/' . content_detail('eng_other_sectors_logo_mkey_hdn', $category_listing->id);

                                        $style = 'max-width: 100%';
                                    } else {
                                        $no_img = base_url() . 'assets/images/no_image.png';
                                        $category_listing_image = $no_img;
                                        $style = 'max-width:170px;max-height:170px';
                                    }
                                    ?>
                                    <!--<a href="<?php /*echo lang_base_url() . 'page/other_sector_detail/' . $category_listing->id; */ ?>">-->
                                    <div class="row align-items-center <?php echo($j == 0 ? 'mt-130' : 'mt-130'); ?>" id="company-<?php echo $category_listing->id; ?>">
                                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                            <div class="sector-logo-image">
                                                <img class="img-fluid"
                                                     src="<?php echo $category_listing_image; ?>"
                                                     alt="">
                                            </div>
                                        </div>
                                        <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                                            <div class="sector-content">
                                                <h2><?php echo pageTitle($category_listing->id, $lang); ?></h2>
                                                <?php echo content_detail($lang . '_other_sectors_dec', $category_listing->id); ?>
                                            </div>
                                            <div class="sector-actions text-end">
                                                <?php if (content_detail('eng_other_sectors_link', $category_listing->id)) { ?>
                                                    <a class="visit_us"
                                                       href="<?php echo content_detail('eng_other_sectors_link', $category_listing->id); ?>"
                                                       target="_blank"><?php echo($lang == 'eng' ? 'Visit us' : 'قم بزيارة الصفحة'); ?></a>
                                                <?php } ?>

                                                <?php
                                                $company_description = content_detail($lang . '_other_sectors_company_profile_desc', $category_listing->id);
                                                $slider_images = content_detail('eng_other_sectors_discover_more_slider', $category_listing->id);
                                                if ($company_description || str_replace('eng_other_sectors_discover_more_slider,', '', $slider_images)) {
                                                    ?>
                                                    <a class="discover_more" href="javascript:void(0);"
                                                       data-bs-toggle="modal"
                                                       data-bs-target="#staticBackdrop-sector-discover-<?php echo $category_listing->id; ?>">
                                                        <?php echo($lang == 'eng' ? 'Discover More' : 'إكتشف المزيد'); ?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!--</a>-->

                                    <div class="modal fade"
                                         id="staticBackdrop-sector-discover-<?php echo $category_listing->id; ?>"
                                         data-bs-keyboard="false" tabindex="-1"
                                         aria-labelledby="staticBackdropLabel-sector-discover-<?php echo $category_listing->id; ?>"
                                         aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body position-relative">
                                                    <button type="button" class="btn-close close-modal"
                                                            data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    <div class="news-detail-modal">
                                                        <h2><?php echo pageSubTitle($category_listing->id, $lang); ?></h2>
                                                        <hr>

                                                        <?php
                                                        $slider_images = content_detail('eng_other_sectors_discover_more_slider', $category_listing->id);
                                                        if ($slider_images) {
                                                            ?>
                                                            <div class="events-popup-slider owl-carousel owl-theme">
                                                                <?php
                                                                $slider_images_list = explode(',', $slider_images);
                                                                foreach ($slider_images_list as $slider_image) {
                                                                    if ($slider_image == 'eng_other_sectors_discover_more_slider') continue;
                                                                    ?>
                                                                    <div class="item">
                                                                        <div class="px-1 px-md-5">
                                                                            <img src="<?php echo base_url() . 'assets/script/' . $slider_image ?>"
                                                                                 class="img-fluid" alt="">
                                                                        </div>
                                                                    </div>
                                                                <?php }
                                                                ?>
                                                            </div>
                                                        <?php } ?>

                                                        <div class="news-detail-modal-desc">
                                                            <p><?php echo html_entity_decode(content_detail($lang . '_other_sectors_company_profile_desc', $category_listing->id)); ?></p>
                                                        </div>
                                                    </div>
                                                    <!--<div class="sector-modal-video position-relative">
                                                        asdsadsa
                                                    </div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12"></div>
                                        <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                                            <?php
                                            $projects = ListingContent($category_listing->id);
                                            if ($projects) { ?>
                                                <div class="row pt-5 pb-3 row-cols-2 row-cols-sm-3 row-cols-md-4 row-cols-lg-5">
                                                    <?php foreach ($projects as $project) {
                                                        if (content_detail('eng_other_sectors_project_logo_mkey_hdn', $project->id)) { ?>
                                                            <div class="col">
                                                                <a href="javascript:void(0)" data-bs-toggle="modal"
                                                                   data-bs-target="#staticBackdrop-project-<?php echo $project->id; ?>">
                                                                    <div class="sector-logo-box">
                                                                        <img class="img-fluid"
                                                                             src="<?php echo base_url('assets/script/' . content_detail('eng_other_sectors_project_logo_mkey_hdn', $project->id)); ?>"
                                                                             alt="">
                                                                    </div>
                                                                </a>
                                                            </div>

                                                            <div class="modal fade"
                                                                 id="staticBackdrop-project-<?php echo $project->id; ?>"
                                                                 data-bs-keyboard="false" tabindex="-1"
                                                                 aria-labelledby="staticBackdropLabel-project-<?php echo $project->id; ?>"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-body position-relative">
                                                                            <button type="button"
                                                                                    class="btn-close close-modal"
                                                                                    data-bs-dismiss="modal"
                                                                                    aria-label="Close"></button>
                                                                            <div class="sector-modal-logo">
                                                                                <img class="img-fluid"
                                                                                     src="<?php echo base_url('assets/script/' . content_detail('eng_other_sectors_project_logo_mkey_hdn', $project->id)); ?>"
                                                                                     alt="">
                                                                            </div>

                                                                            <?php
                                                                            $slider_images = content_detail('eng_other_sectors_slider', $project->id);
                                                                            if ($slider_images) {
                                                                                ?>
                                                                                <div class="events-popup-slider owl-carousel owl-theme mt-4">
                                                                                    <?php
                                                                                    $slider_images_list = explode(',', $slider_images);
                                                                                    foreach ($slider_images_list as $slider_image) {
                                                                                        if ($slider_image == 'eng_other_sectors_slider') continue;
                                                                                        ?>
                                                                                        <div class="item">
                                                                                            <div class="px-1 px-md-5">
                                                                                                <img src="<?php echo base_url() . 'assets/script/' . $slider_image ?>"
                                                                                                     class="img-fluid" alt="">
                                                                                            </div>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            <?php } ?>

                                                                            <?php if (content_detail('eng_other_sectors_proj_video', $project->id) != '') {
                                                                                $video_id = explode('v=', content_detail('eng_other_sectors_proj_video', $project->id));
                                                                                ?>
                                                                                <div class="sector-modal-video position-relative">
                                                                                    <iframe width="766" height="315"
                                                                                            src=""
                                                                                            title="YouTube video player"
                                                                                            frameborder="0"
                                                                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                                                            allowfullscreen></iframe>
                                                                                </div>

                                                                                <script>
                                                                                    $("#staticBackdrop-project-<?php echo $project->id; ?>").on('hidden.bs.modal', function (e) {
                                                                                        $("#staticBackdrop-project-<?php echo $project->id; ?> iframe").attr("src", '');
                                                                                    });
                                                                                    $("#staticBackdrop-project-<?php echo $project->id; ?>").on('shown.bs.modal', function (e) {
                                                                                        $("#staticBackdrop-project-<?php echo $project->id; ?> iframe").attr("src", 'https://www.youtube.com/embed/<?php echo $video_id[1]; ?>');
                                                                                    });
                                                                                </script>
                                                                            <?php } ?>
                                                                            <div class="row pt-4 align-items-center">
                                                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                                    <div class="modal-sector-content">
                                                                                        <?php echo content_detail($lang . '_other_sectors_proj_dec', $project->id); ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        <?php }
                                                        ?>
                                                    <?php }
                                                    ?>
                                                </div>
                                            <?php }
                                            ?>
                                        </div>
                                    </div>


                                    <?php $j++;
                                }
                                ?>
                            </div>
                            <?php $i++;
                        } ?>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
if (!empty($other_sectors_listings)) {
    ?>
    <section class="sectors-footer-slider">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="sectors-slider-heading has-second-color">
                        <h2><?php echo pageSubTitle($page_id, $lang); ?></h2>
                    </div>
                    <div class="sectors-slider">
                        <div class="owl-carousel-sectors owl-carousel owl-theme">
                            <?php
                            $i = 0;
                            foreach ($other_sectors_listings as $other_sectors_listing) {
                                if (content_detail($lang . '_other_sectors_cat_desc_1', $other_sectors_listing->id) != '') {
                                    $show_blue_border = true;
                                } else {
                                    $show_blue_border = false;
                                }
                                ?>
                                <div class="item">
                                    <div class="sector-slide"
                                         data-show-border="<?php echo($show_blue_border ? 1 : 0); ?>"
                                         onclick="activate_tab_and_scroll(<?php echo $other_sectors_listing->id; ?>);">
                                        <img class="img-fluid"
                                             src="<?php echo base_url() . 'assets/script/' . content_detail('eng_other_sectors_cat_icon_mkey_hdn', $other_sectors_listing->id); ?>"
                                             alt="">
                                    </div>
                                    <h2><?php echo pageTitle($other_sectors_listing->id, $lang); ?></h2>
                                </div>
                                <?php $i++;
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>

<script>
    $(document).ready(function () {
        <?php if (isset($_GET['s']) && $_GET['s'] > 0) { ?>
            activate_tab(<?php echo $_GET['s']; ?>);
        <?php } else { ?>
            if ($('.owl-carousel-sectors-nav').find('.sectors_nav').length) {
                $('.owl-carousel-sectors-nav').find('.sectors_nav:first').trigger('click');
            }
        <?php } ?>

        <?php if (isset($_GET['q']) && $_GET['q'] > 0) { ?>
            setTimeout(function() {
                $('html, body').animate({
                    scrollTop: $("#company-<?php echo $_GET['q']; ?>").offset().top - $("#company-<?php echo $_GET['q']; ?>").height() - 100
                }, 500);
            }, 1000);
        <?php } ?>
    });
</script>

<script>
    function activate_tab(tab_id) {
        setTimeout(function () {
            var someTabTriggerEl = document.querySelector('#nav-realestate-tab-' + tab_id);
            var tab = new bootstrap.Tab(someTabTriggerEl);
            tab.show();
            setTimeout(function () {
                var owl = $('.owl-carousel-sectors-nav');
                owl.find('#nav-realestate-tab-' + tab_id).trigger('click');
                owl.trigger('to.owl.carousel', [owl.find('#nav-realestate-tab-' + tab_id).data('index'), 300]);
            }, 500);
        }, 500);
    }
</script>
