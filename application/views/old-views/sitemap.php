<section class=" MinHeight Site_Bg SpecialClass">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2>Site Map</h2>
            </div>
        </div>
        <div class="row pt-5 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 BlockStyling">
                
                <?php 
                    echo siteMap(3,$page_id);
                ?>
                <!--
                <h2>About Al-Murjan</h2>
                <ul class="SiteMap">
                    <li><a href="profile.php">Company Profile</a></li>
                    <li><a href="chairman_message.php">Chairman Message</a></li>
                    <li>
                        <a href="board_of_directors.php">Board of Directors</a>
                        <ul class="Sub_SiteMap">
                            <li><a href="#">Sheikh Abdulrahman Khalid Binmahfouz</a></li>
                            <li><a href="#">Sheikh Sultan Khalid Binmahfouz</a></li>
                            <li><a href="#">Eng. Ammar Farouq Zahran</a></li>
                            <li><a href="#">Mr. Shahid Hanif Shaikh</a></li>
                            <li><a href="#">Mr. Shahid Hanif Shaikh</a></li>
                            <li><a href="#">Mr. Abdullah Abdu Mukred</a></li>
                            <li><a href="#">Mr. Hani Othman Baothman</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Landmark & Assets</a></li>
                </ul>
                <h2>Our Sectors</h2>
                <ul class="SiteMap">
                    <li>
                        <a href="profile.php">Al-Murjan Holding</a>
                        <ul class="Sub_SiteMap">
                            <li><a href="#">Company 1</a></li>
                            <li><a href="#">Company 2</a></li>
                            <li><a href="#">Company 3</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="chairman_message.php">Other Sectors</a>
                        <ul class="Sub_SiteMap">
                            <li>
                                <a href="#">Real Estate</a>
                                <ul class="ThirdLevel">
                                    <li><a href="#">Company 1</a></li>
                                    <li><a href="#">Company 2</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Investment</a>
                                <ul class="ThirdLevel">
                                    <li><a href="#">Company 1</a></li>
                                    <li><a href="#">Company 2</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <h2><a href="NewsAndEvents.php">Events & News</a></h2>
                <h2><a href="contact_us.php">Contact Us</a></h2>
                <h2><a href="Careers.php">Careers</a></h2>
                -->
            </div>
        </div>
    </div>
</section>
</body>
</html>