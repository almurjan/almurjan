<?php 
$home = getPageIdbyTemplate('home');

$listings = ListingContent($page_id);
?>
<main id="main">
	<div class="container">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb justify-content-end my-3">
				<li class="breadcrumb-item"><a href="<?php echo lang_base_url().'page/'.str_replace(' ', '_', pageTitle($home, 'eng')); ?>"><?php echo pageSubTitle($home, $lang); ?></a></li>
				<li class="breadcrumb-item active" aria-current="page"><?php echo pageSubTitle($page_id, $lang); ?></li>
			</ol>
		</nav>
	</div>

	<section id="ProductServices" class="watermark">
		<div class="container text-center mb-5">
			<!-- Dont remove id filters -->
			<div id="filters" class="button-group mb-5 mt-4">
				<button class="btn btn-outline-primary px-4 py-2 mb-2 clickable_all active" data-filter="*"><?php echo ($lang == 'eng' ? 'All' : 'جميع');?></button>
				<?php $k = 0;
				   foreach($listings as $listing) {  
				   $products[] = ListingContent($listing->id);
				   ?>
					<button class="btn btn-outline-primary px-4 py-2 mb-2 clickable_product" data-filter="<?php echo '.'.str_replace(' ','',pageSubTitle($listing->id, $lang)); ?>"><?php echo pageSubTitle($listing->id, $lang); ?></button>
				<?php $k++; } ?>	
			</div>

            <div id="isotopBox" class="form-row">
				  <?php $j = 0;
						foreach($products as $singleProduct) { 
							foreach($singleProduct as $product) { 
							$image = '';
							if(content_detail('eng_services_thumb_mkey_hdn',$product->id) != ''){
								$image = imageSetDimenssion(content_detail('eng_services_thumb_mkey_hdn', $product->id),274,272,1);
							}
							else{
								$image = base_url().'assets/script/noImageNew2.png';
							}
							
							$classes = '';
							$multi_category = $product->multi_category;
							$pro_cats = explode(",",$multi_category);
							foreach($pro_cats as $singleCategory){
								$classes .= str_replace(' ','',pageSubTitle($singleCategory, $lang)) . ' ';
							} ?>
                            <div class="item col-sm-6 col-md-4 col-lg-3 <?php echo $classes; ?>">
                                <div class="item-wrap">
                                    <a href="<?php echo lang_base_url().'page/service_detail/'.$product->id; ?>">
                                        <figure class="figure position-relative d-block rounded">
                                            <img src="<?php echo $image; ?>" class="w-100 rounded" alt="<?php echo pageSubTitle($product->id, $lang); ?>" />
                                            <figcaption class="figure-caption text-center px-3"><?php echo pageSubTitle($product->id, $lang); ?></figcaption>
                                        </figure>
                                    </a>
                                </div>
                            </div>
					<?php } } ?>
				</div>
		</div>
	</section>
</main>