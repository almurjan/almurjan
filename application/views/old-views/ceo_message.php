<?php
$home = getPageIdbyTemplate('home');
$profile = getPageIdbyTemplate('profile');
$about_us = getPageIdbyTemplate('about_us');
$ceo_message = getPageIdbyTemplate('ceo_message');
$murjan_group = getPageIdbyTemplate('murjan_group');
$landmarks = getPageIdbyTemplate('landmarks');
$bod = getPageIdbyTemplate('bod');
$bod_listings = ListingContent($bod);
?>
<section class="MinHeight Site_Bg site_bgNone SpecialClass">
    <div class="container">
        <!--<div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2><?php /*echo pageSubTitle($about_us, $lang); */?></h2>
            </div>
        </div>-->
        <section class="about_custom_nav mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="newdesign_border">
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($profile, 'eng')); ?>" > <?php echo $ar_space.pageSubTitle($profile, $lang).$ar_space; ?></a></li>
                            <li><a class="active" href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($ceo_message, 'eng')); ?>"> <?php echo $ar_space.pageSubTitle($ceo_message, $lang).$ar_space; ?></a></li>
                            <li><a  href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(16, 'eng')); ?>" <?php echo ($page_id == '16' ? 'active' : ''); ?>> <?php echo $ar_space.pageSubTitle(16, $lang).$ar_space; ?></a></li>
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(1293, 'eng')); ?>" <?php echo ($page_id == '1293' ? 'active' : ''); ?>> <?php echo $ar_space.pageSubTitle(1293, $lang).$ar_space; ?></a></li>
                            <!--<li><a href="<?php /*echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(1305, 'eng')); */?>" <?php /*echo ($page_id == '1305' ? 'active' : ''); */?>> <?php /*echo $ar_space.pageSubTitle(1305, $lang).$ar_space; */?></a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <div class="row pt-5 pb-4">
            <div class="col-12 col-sm-5 col-md-4 col-lg-3 col-xl-3 BlockStyling">
                <div class="position-relative ABCXYZ">
                    <?php

                    if (content_detail('eng_ceo_image_mkey_hdn', $ceo_message) != '') {
                        $ceo_image = imageSetDimenssion(content_detail('eng_ceo_image_mkey_hdn', $ceo_message), 350, 546, 1);
                    }else{
                        $no_img=base_url() . 'assets/images/no_image.png';
                        $ceo_image = $no_img;
                    }
                    ?>
                    <img class="w-100" src="<?php echo $ceo_image;?>" alt="">
                    <div class="Picture-Caption ">
                        <h2 style="font-size:16px;"><?php echo content_detail($lang.'_ceo_name',$ceo_message);?></h2>
                        <p class="BoardMember"><?php echo content_detail($lang.'_ceo_title',$ceo_message);?></p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-7 col-md-8 col-lg-9 col-xl-9 BlockStyling">
                <?php echo content_detail($lang.'_desc_our_mes',$ceo_message);?>
            </div>
        </div>
    </div>
</section>

