<?php

if (isset($lang) && $lang == 'arb') {
    $lang = 'arb';
} else {
    $lang = 'eng';
}

$home_id = getPageIdbyTemplate('home');

$config_settings = getConfigurationSetting();

$watermark_image = base_url() . 'assets/script/' . content_detail('eng_watermark_image_mkey_hdn', $home_id);


?>

<!DOCTYPE html>

<html lang="en" dir="<?php echo $lang == 'eng' ? 'ltr' : 'rtl'; ?>">

<head>

    <meta charset="UTF-8">

    <title><?php echo($lang == 'eng' ? $config_settings->project_name : $config_settings->project_name_arb); ?></title> 

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@200;300;400;500;700;800;900&display=swap" rel="stylesheet"> -->
    <link href="<?php echo base_url(); ?>assets/frontend/images/favicon.png" rel="shortcut icon" type="image/ico"/>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $lang == 'eng' ? base_url('assets/frontend') . '/bootstrap/css/bootstrap.css' : base_url('assets/frontend') . '/bootstrap_rtl/bootstrap.min.css'; ?>">

    

    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('assets/frontend') ?>/owlcarousel/css/owl.carousel.min.css">

    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('assets/frontend') ?>/owlcarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('assets/frontend/'); ?>/custom_css/jquery-confirm.min.css">
    <link href="<?php echo base_url('assets/frontend/'); ?>/loader.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/frontend') ?>/mapsvg/css/mapsvg.css?v=1" rel="stylesheet">
    <link href="<?php echo base_url('assets/frontend') ?>/mapsvg/css/nanoscroller.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend') ?>/<?php echo $lang == 'eng' ? 'custom_css/Almurjan_All.css?v=1.3' : 'custom_css_rtl/Almurjan_All.css?v=1.3'; ?>"> 
    <script src='https://www.google.com/recaptcha/api.js<?php echo($lang == 'arb' ? '?hl=ar' : ''); ?>'></script>

    <style>

        /*#My_Achievements_image:before {

            background: url(<?php echo $image; ?>) no-repeat right top !important;

            background-size: contain !important;

        }*/

        #HomePageSlider:after {

            background: url(<?php echo $watermark_image;?>) no-repeat right !important;

            background-size: contain !important;
            display: none;

        }

        .border {

            border: 2px solid red !important;

        }

        .customStyleMed {
            float: right;
            margin-top: 67px;
        }

        /*.AboutNav .nav-link {
            padding: 10px 27px !important;
        }

        .AboutNav .nav-link {
            font-size: 20px;
        }*/

        .BornHS__Modal__Content {
            /*display: none !important;*/
        }

        /*.arb .counter {
            font-size: 73px !important;
        }*/

    </style>

</head>
<body class="<?php /*echo ($tpl_name == 'home') ? 'home_landing_page_body' : '' */?> <?php echo $lang; ?>">