<?php 
    $services =  $listing_row;
    //echo $services->id;
    $sub_services = ListingContent($services->id);
    // echo '<pre>';
    // print_r($sub_services);
    // exit;
    $sub_sub_services = array();
    foreach($sub_services as $sub_service){
        $sub_sub_services[] = ListingContent($sub_service->id);
    }
    // echo '<pre>';
    // print_r($sub_sub_services);
    // exit;
?>
<div class="investor-report sub-service">
        <div class="container">
            <?php
            if(content_detail('eng_bg_image_mkey_hdn', $services->id) != "") {
                $image = base_url() . 'assets/script/' . content_detail('eng_bg_image_mkey_hdn', $services->id); }
            ?>
            <div class="sub-service-bg" style="background-image: url(<?php echo $image; ?>);">
                <?php echo content_detail($lang . '_quote_text', $services->id); ?>
            </div>
            <nav class="inner-tabs sub-tabs">
                <div class="nav nav-tabs nav-fill align-items-center" id="nav-tab" role="tablist">
                    <?php 
                    $active = 0;
                    $tab_no = 1;
                    foreach($sub_services as $sub_service) { ?>
                        <a class="nav-item nav-link <?php echo($active == 0 ? 'active' :''); ?> call-1"  data-toggle="tab" href="#nav-<?php echo $tab_no; ?>" role="tab" aria-controls="nav-Financial" aria-selected="true"><?php echo pageSubTitle($sub_service->id, $lang); ?></a>
                    <?php $active++; $tab_no++; 
                    }?>
                    <a class="nav-item nav-link call-1" id="Capital" data-toggle="tab" href="#nav-Capital" role="tab" aria-controls="nav-Capital" aria-selected="false">Funds</a>
                </div>
            </nav>
        </div>
        <div class="tab-content px-3 px-sm-0" id="nav-tabContent">
             <?php 
                $active = 0;
                $tab_no = 1;
                foreach($sub_services as $sub_service) {  
                ?>
                <div class="tab-pane <?php echo($active == 0 ? 'show active' :'fade'); ?>" id="nav-<?php echo $tab_no; ?>" role="tabpanel" aria-labelledby="Financial">
                    <div class="container sub-tab-1">
                        <?php echo content_detail($lang. '_asset_desc',$sub_service->id); ?>
                    </div>
                    <?php foreach($sub_sub_services as $sub_sub_service){ ?>
                    <div class="sub-tab-bg">
                        <div class="container">
                           <?php foreach($sub_sub_service as $sub_sub_sub_service){ 
                               if($sub_sub_sub_service->is_image == 2 ){ ?>
                                <div class="row justify-content-center">
                                    <div class="col-md-5">
                                        <div class="sub-tab-detail">
                                            <h4><?php echo pageSubTitle($sub_sub_sub_service->id, $lang); ?></h4>
                                            <?php echo content_detail($lang. '_asset_desc', $sub_sub_sub_service->id); ?>
                                        </div>
                                    </div>
                                </div>
                           <?php } else{ ?>
                            <div class="row justify-content-center">
                            <div class="col-md-5 text-md-right">
                                <img src="images/resurch.png" alt="">
                            </div>
                            <div class="col-md-7">
                                <div class="sub-tab-detail tab-detail-bg">
                                    <h4>RESEARCH</h4>
                                    <p>At SKFH, we provide important research and analysis through a strategic partnership with KFH Research who are leaders in the banking sector for specialized research and analysis. Through our partnership with KFH, we employ top professional analysts and economists to produce market-leading research and reports that include various asset classes, sectors, industries, and geographical regions. We also publish reports and periodic analysis of regional financial markets and companies in order to provide our client base with up-to-date and timely information.</p>
                                </div>
                            </div>
                        </div>
                         <?php  
                                } 
                        } ?>
                        </div>
                    </div>        
                </div>
                    <?php }
                $active++; $tab_no++; 
                } ?>
        </div>
</div>