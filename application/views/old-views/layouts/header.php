<?php

include 'head.php';

$logo = base_url() . 'assets/script/' . content_detail('eng_logo_image_mkey_hdn', $home_id);
$logo_url_link = lang_base_url();
if ($lang == 'eng') {
    $segmentMed = $this->uri->segment(1);
}
else {
    $segmentMed = $this->uri->segment(2);
}
if($segmentMed == 'med'){
    $logo_url_link = '#';
}
$selfapp_image = base_url() . 'assets/frontend/images/med2.jpg';
$social_links = social_links();
?>
<header>
<?php if($segmentMed != 'med'){ ?>
    <div class="container-fluid">
        <div class="row HeaderPadding align-items-center">
            <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 AlmurjanLogo">
                <a href="<?php echo $logo_url_link;?>"><img src="<?php echo $logo; ?>" alt=""></a>
            </div>
            <div class="col-6 col-sm-6 col-md-9 col-lg-9 col-xl-9 MainTopMenu ">
                <nav class="Menu-d-none navbar navbar-expand-md navbar-light">
                    <div class="collapse navbar-collapse " id="navbarSupportedContent">
                        <ul class="navbar-nav py-4 py-md-0 ">
                            <?php 

							if($segmentMed != 'med'){
								echo header_menu(1, $page_id);
							} 
							?>
                        </ul>
                    </div>
                    <div class="EN_ARB">
                        <?php if($lang == 'eng'){ ?>
                            <a href="#" onClick="redirect('arb');"><img src="<?php echo base_url('assets/frontend') ?>/images/arabic.png" alt=""></a>
                        <?php }else{?>
                            <a href="#" onClick="redirect('eng');"> EN <!-- <img src="<?php echo base_url('assets/frontend') ?>/images/english1.png" alt=""> --></a>
                        <?php } ?>
                    </div>
                    <div class="header_sociallinks socialLinks">


                        <a class="searcIcon" href="javascript:void(0);"><i class="fa fa-search" aria-hidden="true"></i></a>
                        <form action="<?php echo lang_base_url().'page/search'; ?>" class="searchBox"  method="get">
                            <div class="form-group">
                                <input name="item_title" type="text" class="form-control" placeholder="Type here to search..." />
                                <i class="fas fa-times crosIcon"></i>
                            </div>
                        </form>
                        <?php if($social_links->soc_fb != ''){ ?>

                            <a href="<?php echo $social_links->soc_fb; ?>" target="_blank"><i class="fab fa-facebook-f" ></i></a>

                        <?php } ?>



                        <?php if($social_links->soc_tw != ''){ ?>

                            <a href="<?php echo $social_links->soc_tw; ?>" target="_blank"><i class="fab fa-twitter"></i></a>

                        <?php } ?>



                        <!-- <?php if($social_links->soc_lin != ''){ ?>

                            <a href="<?php echo $social_links->soc_lin; ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>

                        <?php } ?> -->



                        <!-- <?php if($social_links->soc_you != ''){ ?>

                            <a href="<?php echo $social_links->soc_you; ?>" target="_blank"><i class="fab fa-youtube"></i></a>

                        <?php } ?> -->



                        <?php if($social_links->soc_insta != ''){ ?>

                            <a href="<?php echo $social_links->soc_insta; ?>" target="_blank"><i class="fab fa-instagram"></i></a>

                        <?php } ?>

                    </div>
                </nav>
                <div class="MainSideMenu d-md-none d-lg-none d-xl-none">
                    <div class="header_sociallinks socialLinks">


                        <a class="searcIcon" href="javascript:void(0);"><i class="fa fa-search" aria-hidden="true"></i></a>
                        <form action="<?php echo lang_base_url().'page/search'; ?>" class="searchBox"  method="get">
                            <div class="form-group">
                                <input name="item_title" type="text" class="form-control" placeholder="Type here to search..." />
                                <i class="fas fa-times crosIcon"></i>
                            </div>
                        </form>
                        <?php if($social_links->soc_fb != ''){ ?>

                            <a href="<?php echo $social_links->soc_fb; ?>" target="_blank"><i class="fab fa-facebook-f" ></i></a>

                        <?php } ?>



                        <?php if($social_links->soc_tw != ''){ ?>

                            <a href="<?php echo $social_links->soc_tw; ?>" target="_blank"><i class="fab fa-twitter"></i></a>

                        <?php } ?>



                        <!-- <?php if($social_links->soc_lin != ''){ ?>

                            <a href="<?php echo $social_links->soc_lin; ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>

                        <?php } ?> -->



                        <!-- <?php if($social_links->soc_you != ''){ ?>

                            <a href="<?php echo $social_links->soc_you; ?>" target="_blank"><i class="fab fa-youtube"></i></a>

                        <?php } ?> -->



                        <?php if($social_links->soc_insta != ''){ ?>

                            <a href="<?php echo $social_links->soc_insta; ?>" target="_blank"><i class="fab fa-instagram"></i></a>

                        <?php } ?>

                    </div>
                    <span class="EN_ARB">
                        <?php if($lang == 'eng'){ ?>
                            <a href="#" onClick="redirect('arb');"><img src="<?php echo base_url('assets/frontend') ?>/images/arabic.png" alt=""></a>
                        <?php }else{?>
                            <a href="#" onClick="redirect('eng');"> EN <!-- <img src="<?php echo base_url('assets/frontend') ?>/images/english1.png" alt=""> --></a>
                        <?php } ?>
                    </span>
                    <?php if($segmentMed != 'med'){?>
                        <div id="mySidenav" class="sidenav">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                            <div id="accordion" class="myaccordion">
                                <?php echo mobile_header_menu(1, $page_id); ?>
                            </div>
                        </div>
                        <span style="font-size:20px;cursor:pointer; padding-<?php echo ($lang=='eng'?'left':'right');?>: 10px; color: #c8a443;" onclick="openNav()"><i class="fas fa-bars"></i></span>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php }else{ ?>
	 <div class="container">
        <div class="row HeaderPadding align-items-center">
            <div class="col-12 col-sm-9 col-md-9 col-lg-6 col-xl-6 AlmurjanLogo text-left">
                <a href="<?php echo $logo_url_link;?>"><img class="img-fluid Mainlogosize" src="<?php echo $logo; ?>" alt=""></a>
                
				<a href="<?php echo $logo_url_link;?>"><img class="logosize" src="<?php echo $selfapp_image; ?>" alt=""></a>
            </div>
            <div class="col-12 col-sm-3 col-md-3 col-lg-6 col-xl-6 MainTopMenu text-right ">
                <div class="EN_ARB d-none">
                    <?php if($lang == 'eng'){ ?>
                        <a href="#" onClick="redirect('arb');"><img src="<?php echo base_url('assets/frontend') ?>/images/arabic.png" alt=""></a>
                    <?php }else{?>
                        <a href="#" onClick="redirect('eng');"> EN <!-- <img src="<?php echo base_url('assets/frontend') ?>/images/english1.png" alt=""> --></a>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>

<?php } ?>

</header>
