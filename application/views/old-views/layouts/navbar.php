<?php
$lang = $this->session->userdata('lang');
$center_id = getPageIdbyTemplate('investment_centers');
$online_services_id = getPageIdbyTemplate('online_services');
$home_id = getPageIdbyTemplate('home');

if(content_detail($lang.'_logo_image_mkey_hdn',$home_id) != ''){ 
	$home_image = base_url().'assets/script/'.content_detail($lang.'_logo_image_mkey_hdn',$home_id);
}
?>
		
	<div class="quicklinks">
        <ul>
            <li><a href="<?php echo lang_base_url().'page/';?><?php echo str_replace(' ', '_', pageTitle($online_services_id, 'eng'));?>" target="_blank"><i class="fa fa-user" aria-hidden="true"></i></a></li>
            <li><a href="<?php echo lang_base_url().'page/';?><?php echo str_replace(' ', '_', pageTitle($center_id, 'eng'));?>"><i class="fa fa-volume-control-phone" aria-hidden="true"></i></a></li>
        </ul>
    </div>
    <span class="targetdiv" id="MainNavbar"></span>
    <nav class="navbar main-nav inner <?php echo (strpos($_SERVER['PHP_SELF'], 'index.php') !== false) ? "home-nav" : ""?>">
        <div class="container">
            <div class="logo">
				<?php if($home_image != ''){ ?>
					<a href="<?php echo lang_base_url(); ?>"><img src="<?php echo $home_image?>" alt=""></a>
				<?php } ?>
            </div>
			
            <div class="header-top">
			<?php 
			
			 if($lang=='eng') { ?>
				<a href="javascript:void(0);" class="lang" id="langSwitch2" >عربي</a>
			<?php } else { ?>
				<a href="javascript:void(0);" class="lang" id="langSwitch2" >Eng</a>
			<?php } 
						$online_services_id 	= getPageIdbyTemplate('online_services');
					?>
					
					
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?php echo pageSubTitle($online_services_id, $lang);?> <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                             <?php
								$listings = ListingContent($online_services_id);
								foreach($listings as $list){ 
								if($list->category == 0){
								?>
									<li><a href="<?php  echo content_detail($lang.'_link', $list->id); ?>" target="_blank">
										<?php echo pageTitle($list->id,$lang); ?>
									</a></li>	
							<?php }
								else{ ?>
									<li><a href="<?php  echo lang_base_url().'page/service_detail/'.$list->id; ?>">
										<?php echo pageTitle($list->id,$lang); ?>
									</a></li>
							<?php	}
							} ?>
                        </ul>
                    </div>
            </div>
            <div class="navbar-header">
                <button type="button" class="hamburger hamburger--elastic" data-toggle="collapse" data-target="#myNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
				<?php echo header_menu('1', $page_id); ?>
				<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu mega-dropdown search-drodown">
                        <li>
                            <form method="post" action="<?php echo lang_base_url().'page/searchResults' ?>">
                                <input type="text" name="item_title" value="<?php echo ($_POST['item_title'] != '' ? $_POST['item_title'] : '')?>" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'Type Word' : 'كتابة الكلمات') ?>" required>
                                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
            </div>
        </div>
    </nav>
