<html lang="en" <?php if ($lang=='rtl') { echo 'dir="rtl" '; } ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
</head>
<body>
	<table style="width: 600px;margin: 0 auto;direction: rtl;border: none;border-collapse: collapse;border: 1px solid #999999;">
		<thead>
			<tr style="background-color: #000000;">
				<td style="padding: 10px 0;border: none;" align="center">
					<img height="130" src="<?php echo base_url();?>assets/frontend/images/logo_email.png?v=1.1" style="margin: 0 125px 0 0;">
				</td>
				<?php 
					$social_links = social_links();
				?>
				
				<td style="padding: 10px 0;border: none;" align="center">
					<?php if($social_links->soc_fb != ''){ ?>
						<a style="margin: 0 0 0 5px;" href="<?php echo $social_links->soc_fb; ?>" target="_blank"><img src="<?php echo base_url();?>assets/frontend/images/fb.png"></a>
					<?php } ?>
					
					<?php if($social_links->soc_tw != ''){ ?>
						<a href="<?php echo $social_links->soc_tw; ?>" target="_blank"><img src="<?php echo base_url();?>assets/frontend/images/tw.png"></a>
					<?php } ?>
					<?php if($social_links->soc_insta != ''){ ?>
						<a href="<?php echo $social_links->soc_insta; ?>" target="_blank"><img src="<?php echo base_url();?>assets/frontend/images/insta.png" style="margin-right: 6px;"></a>
					<?php } ?>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="border: none;" colspan="2"><img src="<?php echo base_url();?>assets/frontend/images/img8.png?v=1.2"></td>
			</tr>
			<tr>
				<td style="border: none; padding: 35px;text-align: center;" colspan="2">
					<h3 style="color: #777777"><?php echo $message; ?></h3>
				</td>
			</tr>
			<tr>
				<td  style="border: none; padding: 25px 15px;text-align: center;background: #dddddd" colspan="2"><p style="font-size: 13px;margin: 0;">حقوق الحفظ والنشر | هي مرسيدس</p></td>
			</tr>
			
		</tbody>
	</table>
</body>
</html>