<style>
.border{
	border: 2px solid red !important;
}
.NewsLetters > .border{
	border: 2px solid red !important; 
}
</style>
<?php

$home_id = getPageIdbyTemplate('home');
$sitemap = getPageIdbyTemplate('sitemap');

$config_settings = getConfigurationSetting();

$contactUsId = getPageIdByTemplate('contact_us');
$bodId = getPageIdByTemplate('bod');

$social_links = social_links();

$privacy_policy = getPageIdbyTemplate('privacy_policy');

$terms = getPageIdbyTemplate('terms');

$sitemap = getPageIdbyTemplate('sitemap');

$murjan_holding = getPageIdbyTemplate('murjan_holding');

$murjan_holding_listings = ListingContent($murjan_holding);
$slider_counter=0;
foreach($murjan_holding_listings as $murjan_holding_listing){

    if(content_detail('is_show_home', $murjan_holding_listing->id) == 1){
        $slider_counter++;
    }
}
$holding_logo_slider=$slider_counter;
$footerlogo = base_url() . 'assets/script/' . content_detail('eng_footer_logo_image_mkey_hdn', $home_id);

$companies_map = getPageIdbyTemplate('companies_map');

$mapPinsListing = ListingContent($companies_map);
?>
<?php 
	if ($lang == 'eng') {
		$segmentMed = $this->uri->segment(1);
	} 
	else {
		$segmentMed = $this->uri->segment(2);
	}

?>
<section class="<?php /*echo ($tpl_name == 'home')?'home_landing_page':'' */?>">
    <?php
    $config=getConfigurationSetting();
    if($config->newsletter_section ==1 && $segmentMed != 'med'){?>
    <div class="container">

        <div class="row">

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <hr>

            </div>

        </div>

        <div class="row pt-2">

            <div class="offset-lg-1 offset-xl-2 col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3 LatestUpdate">

                <h2><?php echo ($lang == 'eng' ? 'SUBSCRIBE TO GET OUR LATEST UPDATES' : 'اشترك للحصول على آخر أخبارنا'); ?></h2>

            </div>

            <div class="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-5 NewsLetters">

               <!-- <form action="<?php /*echo base_url('page/subscribe') */?>" id="subcribs_form" onsubmit="return false;">-->

                <form action="<?php echo base_url().'page/newsLetter';?>" method="post" onsubmit="return false;" id="subcribs_form_1">

                    <div class="input-group mb-3">

                        <input type="text" name="news_letter" id="news_letter" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'Your Email Address' : 'عنوان البريد الإلكتروني');?>">
						<input type="hidden" name="lang" id="lang" class="form-control" value="<?php echo $lang; ?>">
                        <div class="input-group-append">

                            <button class="btn btn-success BtnBlue subscribe_newsletter" type="submit"><?php echo ($lang == 'eng' ? 'Subscribe' : 'الإشتراك'); ?></button>

                        </div>

                    </div>
                    <div style=" margin-bottom: 10px; ">
                        <p id="news_letter_msg" style="color: red;"></p>
                        <p id="newsletter_message" style="color: green;"></p>
                    </div>
                    
                </form>

                <p><?php echo ($lang == 'eng' ? 'By subscribing, you agree to the ' : 'بالاشتراك في النشرة الاخبارية، أنت توافق على'); ?> <span style="text-decoration: underline"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($privacy_policy, 'eng')); ?>"><?php echo pageSubTitle($privacy_policy, $lang); ?></a></span></p>

            </div>

        </div>

    </div>
    <?php } ?>
</section>

<?php 
	if ($lang == 'eng') {
		$segmentMed = $this->uri->segment(1);
	} 
	else {
		$segmentMed = $this->uri->segment(2);
	}
	if($segmentMed != 'med'){
?>
	
<footer class="<?php /*echo ($tpl_name == 'home')?'home_landing_page':'' */?>" style="margin-top: 30px; padding-bottom: 20px; background: url('<?php echo base_url('assets/frontend') ?>/images/footer_shape.png') repeat-x center top, #00447c; background-size: cover;">

    <div class="container">

        <div class="row PaddingTop60">

            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 text-md-center text-lg-center text-xl-center">

                <a href="<?php echo lang_base_url()?>"><img src="<?php echo $footerlogo; ?>" alt=""></a>

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 FooterCol">

                <h2><?php echo ($lang == 'eng' ? $config_settings->project_name : $config_settings->project_name_arb); ?></h2>

                <div class="d-flex flex-row flex-wrap FooterNavList">

                    <?php echo footer_menu(2,$page_id); ?>

                </div>

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5 FooterCol">

                <h2><?php echo ($lang == 'eng' ? 'Get in touch' : 'ابقى على تواصل');?></h2>

                <p><?php echo content_detail($lang.'_desc_contact',$contactUsId); ?></p>

                <p><?php echo ($lang == 'eng' ? 'Phone:' : 'رقم الهاتف:');?> <span style=" <?php echo ($lang != 'eng')?'display: inline-block;direction: ltr':'' ?>"><?php echo content_detail('eng_phone',$contactUsId); ?></span></p>

                <p><?php echo content_detail('eng_officeEmail',$contactUsId); ?></p>

            </div>

        </div>

        <hr class="Footer">

        <div class="row">

            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">

                <ul class="FooterLastNav">

                    <li><a href="#">
					<?php 
						if($lang == 'eng'){ 
							$text = 'All rights reserved 2020.';
							
						}else{ 
							$text = ' جميع الحقوق محفوظة مجموعة المرجان القابضة' . ' ٢٠١٨';
							
						} 
						$yearFinal = str_replace('2020',date('Y'),$text);
						$yearFinal = str_replace('٢٠١٨',date('Y'),$yearFinal);
						echo ($lang=='arb' ? convertToArabic($yearFinal) : $yearFinal);
					?>
					</a></li>

                    <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($terms, 'eng')); ?>"><?php echo pageSubTitle($terms, $lang); ?></a></li>

                    <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($sitemap, 'eng')); ?>"><?php echo pageSubTitle($sitemap, $lang); ?></a></li>

                    <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($privacy_policy, 'eng')); ?>"><?php echo pageSubTitle($privacy_policy, $lang); ?></a></li>

                </ul>

            </div>

            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 socialLinks">



                <?php if($social_links->soc_fb != ''){ ?>

                    <a href="<?php echo $social_links->soc_fb; ?>" target="_blank"><i class="fab fa-facebook-f" ></i></a>

                <?php } ?>



                <?php if($social_links->soc_tw != ''){ ?>

                    <a href="<?php echo $social_links->soc_tw; ?>" target="_blank"><i class="fab fa-twitter"></i></a>

                <?php } ?>



                <?php if($social_links->soc_lin != ''){ ?>

                    <a href="<?php echo $social_links->soc_lin; ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>

                <?php } ?>



                <?php if($social_links->soc_you != ''){ ?>

                   <a href="<?php echo $social_links->soc_you; ?>" target="_blank"><i class="fab fa-youtube"></i></a>

                <?php } ?>



                <?php if($social_links->soc_insta != ''){ ?>

                    <a href="<?php echo $social_links->soc_insta; ?>" target="_blank"><i class="fab fa-instagram"></i></a>

                <?php } ?>

            </div>

        </div>

    </div>

</footer>
<?php }else{ ?>



<style>
	.newsLetterDiv{
		display: none;
	}
</style>
<footer style="margin-top: 30px; padding-bottom: 20px; background: url('<?php echo base_url('assets/frontend') ?>/images/footer_shape.png') repeat-x center top, #00447c; background-size: cover;">

    <div class="container">

        <div class="row PaddingTop60">

            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 text-md-center text-lg-center text-xl-center">

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 FooterCol">

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5 FooterCol">

                <h2><?php echo ($lang == 'eng' ? 'Get in touch' : 'ابقى على تواصل');?></h2>

                <p><?php echo content_detail($lang.'_desc_contact',$contactUsId); ?></p>

                <p><?php echo ($lang == 'eng' ? 'Phone:' : 'رقم الهاتف:');?> <span style=" <?php echo ($lang != 'eng')?'display: inline-block;direction: ltr':'' ?>"><?php echo content_detail('eng_phone',$contactUsId); ?></span></p>

                <p><?php echo content_detail('eng_officeEmail',$contactUsId); ?></p>

            </div>

        </div>

        <hr class="Footer">

        <div class="row">

            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">

                <ul class="FooterLastNav">

                    <li><a href="#">
					<?php 
						if($lang == 'eng'){ 
							$text = 'All rights reserved 2020.';
							
						}else{ 
							$text = 'جميع الحقوق محفوظة جمعية الوداد الخيرية ٢٠١٨';
							
						} 
						$yearFinal = str_replace('2020',date('Y'),$text);
						$yearFinal = str_replace('٢٠١٨',date('Y'),$yearFinal);
						echo ($lang=='arb' ? convertToArabic($yearFinal) : $yearFinal);
					?>
					</a></li>
                </ul>

            </div>

            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 socialLinks">



                <?php if($social_links->soc_fb != ''){ ?>

                    <a href="<?php echo $social_links->soc_fb; ?>" target="_blank"><i class="fab fa-facebook-f" ></i></a>

                <?php } ?>



                <?php if($social_links->soc_tw != ''){ ?>

                    <a href="<?php echo $social_links->soc_tw; ?>" target="_blank"><i class="fab fa-twitter"></i></a>

                <?php } ?>



                <?php if($social_links->soc_lin != ''){ ?>

                    <a href="<?php echo $social_links->soc_lin; ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>

                <?php } ?>



                <?php if($social_links->soc_you != ''){ ?>

                   <a href="<?php echo $social_links->soc_you; ?>" target="_blank"><i class="fab fa-youtube"></i></a>

                <?php } ?>



                <?php if($social_links->soc_insta != ''){ ?>

                    <a href="<?php echo $social_links->soc_insta; ?>" target="_blank"><i class="fab fa-instagram"></i></a>

                <?php } ?>

            </div>

        </div>

    </div>

</footer>
<?php } ?>

<div id="stop" class="scrollTop">
    <img src="<?php echo base_url().'assets/frontend/images/scroll-to-top.png'; ?>" alt="">
</div>
<script>

    var base_url = '<?php echo base_url(); ?>';
    var lang_base_url = '<?php echo lang_base_url(); ?>';
    var tpl = '<?php echo $tpl_name?>';

    var lang = '<?php echo $lang; ?>'; var lang = '<?php echo $lang; ?>';

    var murjan_holding_total = '<?php echo $holding_logo_slider; ?>';

    var holding_slider_loop;

    if(murjan_holding_total >= 5){

        murjan_holding_slider = 4;

        holding_slider_loop=true;

    }
    else{

        murjan_holding_slider = murjan_holding_total;

        holding_slider_loop=false;

    }
    console.log(murjan_holding_total);
    console.log(holding_slider_loop);

</script>

<script src="<?php echo base_url('assets/frontend') ?>/custom_js/jQuery-v3.3.1.js"></script>



<script src="<?php echo base_url('assets/frontend') ?>/jquery-validation-1.19.1/dist/additional-methods.min.js"></script>

<script src="<?php echo base_url('assets/frontend') ?>/jquery-validation-1.19.1/dist/jquery.validate.min.js"></script>

<?php if($lang != 'eng'){?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/localization/messages_ar.min.js"></script>
<?php } ?>

<script src="<?php echo base_url('assets/frontend') ?>/custom_js/jquery-confirm.min.js"></script>

<script src="<?php echo base_url('assets/frontend') ?>/custom_js/custom.js?v=1.3"></script>

<script src="<?php echo base_url('assets/frontend') ?>/bootstrap/js/popper.min.js"></script>

<script src="<?php echo base_url('assets/frontend') ?>/<?php echo $lang == 'eng' ? 'bootstrap/js/bootstrap.min.js' : 'bootstrap_rtl/bootstrap.min.js'; ?>"></script>

<script src="<?php echo base_url('assets/frontend') ?>/owlcarousel/js/owl.carousel.js"></script>

<script src="<?php echo base_url('assets/frontend') ?>/<?php echo $lang == 'eng' ? 'custom_js/main.js' : 'custom_js_rtl/main.js'; ?>"></script>

<script src="<?php echo base_url('assets/frontend') ?>/custom_js/fontawesome.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script src="<?php echo base_url('assets/frontend') ?>/mapsvg/js/jquery.mousewheel.min.js"></script>
<script src="<?php echo base_url('assets/frontend') ?>/mapsvg/js/jquery.nanoscroller.min.js"></script>
<script src="<?php echo base_url('assets/frontend') ?>/mapsvg/js/mapsvg.min.js?v=1"></script>


<script>
// Click
$(document).on( "click", ".btn-default", function() {	
	setTimeout(function () {
		window.location.reload();
	}, 1000);
});

$(".subscribe_newsletter").click(function (e) {
	//alert('here');
	var base_url  = '<?php echo base_url().'page/newsLetter';?>';
	var email = $('#news_letter').val();
	if(email == ''){
		$('#news_letter_msg').html('Please fill the field');
		return false;
	}
	else{
		$('#news_letter_msg').html('');
	}
	var lang = $('#lang').val();
	
	if(email == ''){
		 $("#email").addClass('errorClass');
	}
	
	else if(validateEmail(email) == true){
		$(".page_loader_newsletter").fadeIn("slow");
		$(".subscribe_newsletter").attr("disabled", true);
		$.ajax({
			type: "POST"
			, url: base_url
			, data: {
				email: email,
				lang: lang
			}
			, success: function (response) {
				$(".subscribe_newsletter").attr("disabled", false);
				$(".page_loader_newsletter").fadeOut("slow");
				var json = jQuery.parseJSON(response);
					$("#newsletter_message").html(json.message);
					//$(".open_mymodel_quote").click();
				setTimeout(function() {
					$("#newsletter_message").html('');
				   //location.reload(); 
				}, 3000);
				
			}
		});
		//$(".page_loader").fadeOut("slow");
		e.preventDefault();	
	}
	else{
        $('#news_letter_msg').html('Please fill the field');
        return false;
	}
});
function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}	
function in_array(array, val){
	for(var i = 0; i < array.length; i++){
		if(val == array[i]){
			return true;
		}
	}
	return false;
}
</script>
<script>

    function openNav() {

        document.getElementById("mySidenav").style.width = "330px";

        document.getElementById("main").style.marginLeft = "330px";

        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";

    }



    function closeNav() {

        document.getElementById("mySidenav").style.width = "0";

        document.getElementById("main").style.marginLeft= "0";

        document.body.style.backgroundColor = "white";

    }

</script>

<script>

    $('#langSwitch').click(function () {

        $('.page_loader').show();

        var language = lang === 'eng' ? 'arb' : 'eng';

        redirect(language);

    });



    function getAbsolutePath() {

        var loc = window.location;

        var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);

        return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));

    }

    function getUrlVars()

    {

        var vars = [], hash;

        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

        for(var i = 0; i < hashes.length; i++)

        {

            hash = hashes[i].split('?');

            vars.push(hash[0]);

            vars[hash[0]] = hash[1];

        }

        return vars;

    }

    function redirect(lang) {

        var concat = getUrlVars();

        if (/=/i.test(concat)){

            var mark = '?';

            //alert('yes');

        }

        else{

            //alert('no');

            concat = '';

            mark = '';

        }

        $('.page_loader').show();

        <?php $url = ''; ?>

        if (lang == 'arb') {

            <?php

            $uri_string = explode('/', uri_string());

            for ($i = 1; $i < sizeof($uri_string); $i++) {

                if ($uri_string[$i] != '') {

                    $url .= $uri_string[$i] . '/';

                }

            }

            ?>

            var redirection = '<?php  echo base_url() . 'ar/' . uri_string(); ?>';

            window.location.href = redirection+mark+concat;

        } else {

            var redirection = '<?php  echo base_url() . $url; ?>';

            window.location.href = redirection+mark+concat;

        }

    }



    $(document).ready(function () {
    /******************************
        BOTTOM SCROLL TOP BUTTON
    ******************************/

        // declare variable
        var scrollTop = $(".scrollTop");

        $(window).scroll(function() {
            // declare variable
            var topPos = $(this).scrollTop();

            // if user scrolls down - show scroll to top button
            if (topPos > 100) {
                $(scrollTop).css("opacity", "1");

            } else {
                $(scrollTop).css("opacity", "0");
            }

        }); // scroll END
        //Click event to scroll to top
        $(scrollTop).click(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 1500);
            return false;

        }); // click() scroll top EMD


        // Animate loader off screen

        $(".page_loader").fadeOut("slow");
        function other_sector_tab_active(){
            //window.stop();
            var val = '';
            if(lang=='eng'){
                val = '<?php echo $this->uri->segment(3) ?>';
            }else{
                val = '<?php echo $this->uri->segment(4) ?>';
            }
            $(".filter-button").not(this).removeClass('active_cat');
            //alert(val);
            $("#"+val).addClass("active_cat");
            if ($( ".filter" ).hasClass( val )) {
                $(".filter").hide();
                $("."+val).show();
            }
        }
        var val_id='';
        if(lang=='eng'){
            val_id = '<?php echo $this->uri->segment(3) ?>';
        }else{
            val_id = '<?php echo $this->uri->segment(4) ?>';
        }

        if(val_id){
            other_sector_tab_active();
        }

    });



    var myVideo = document.getElementById("myVideo");



    function playPause() {

        if (myVideo.paused)

            myVideo.play();

        else

            myVideo.pause();

    }

</script>
<script>
    
    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
    $(document).ready(function() {
        var url = '<?php echo current_url(); ?>';
       <?php
        if(strpos(current_url(),'Our_Sectors/')==false){?>
            $('.isFlag').each(function(){
                if($(this).data('id')>0){
                    $(this).hide();
                }
            });
        <?php }?>

        setTimeout(function(){ topFunction(); }, 1000);
        var lang = '<?php echo $lang; ?>';
        var ln='';
        var dr='';
        var msg='';
        if(lang == 'arb'){
            ln='ar';
            dr='rtl';

        }else{
            ln='en';
            dr='ltr';
        }

        $('.js-rs-single').select2({
            language: {
                noResults: function () {
                    if(ln == 'ar'){
                        msg = 'لم يتم العثور على النتيجة';
                    }else{
                        msg = 'No Result Found';
                    }
                    return msg;
                }
            },
            dir: dr
        });
        $('.js-crs-single').select2({
            language: {
                noResults: function () {
                    if(ln == 'ar'){
                        msg = 'لم يتم العثور على النتيجة';
                    }else{
                        msg = 'No Result Found';
                    }
                    return msg;
                }
            },
            dir: dr
        });
        $('.js-ns-single').select2({
            language: {
                noResults: function () {
                    if(ln == 'ar'){
                        msg = 'لم يتم العثور على النتيجة';
                    }else{
                        msg = 'No Result Found';
                    }
                    return msg;
                }
            },
            dir: dr
        });
        var atLeastOneIsChecked = $('.education_level_other:checked').length > 0;
        $(document).on( "change", ".education_level_other", function() {
            if ($('.education_level_other').attr('value')=='Other') {

                $('#show_edu').show();
                $('#show-me').attr('required',true);


            } else {
                $('#show-me').attr('required',false);
                $('#show_edu').hide();

            }
        } );
        /*$('#med_form').change(function() {
            if ($('.education_level_other').attr('checked')) {
                alert('here');
                $('#show-me').attr('required',true);

            } else {
                $('#show-me').attr('required',false);

            }
        });*/
        $('.skip_to_section').click(function(){
            $('.home_landing_page').css("display","block");
            $('.home_landing_page_body').css("overflow","auto");
            $('html, body').animate({
                scrollTop: $("#About-Al-Murjan").offset().top
            }, 100);
        });
    });
</script>

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery("#mapsvg").mapSvg({
        markerLastID: 2,
        viewBox: [0, 0, 300, 100],
        colors: {
            baseDefault: "#000000",
            background: "#ffffff",
            selected: 40,
            hover: 20,
            directory: "#fafafa",
            status: {}
            },
        gauge: {
            on: false,
            labels: {
                low: "low",
                high: "high"
            },
            colors: {
                lowRGB: {
                    r: 85,
                    g: 0,
                    b: 0,
                    a: 1
                },
                highRGB: {
                    r: 238,
                    g: 0,
                    b: 0,
                    a: 1
                },
                low: "#550000",
                high: "#ee0000",
                diffRGB: {
                    r: 153,
                    g: 0,
                    b: 0,
                    a: 0
                }
            },
            min: 0,
            max: false
        },
        source: "<?php echo base_url('assets/frontend') ?>/mapsvg/map.svg",
        title: "World_map_with_points",
        markers: [
            <?php $i=1; foreach($mapPinsListing as $mapPin){?>
            {
                id: "marker_<?php echo $i; ?>",
                attached: true,
                isLink: false,
                popover: "<h3><?php echo pageSubTitle($mapPin->id,$lang); ?></h3>",
                data: {},
                src: "<?php echo base_url('assets/frontend') ?>/mapsvg/markers/pin1_red.png",
                width: 20,
                height: 19,
                x: <?php echo content_detail('eng_compnies_pin_xposition',$mapPin->id); ?>,
                y: <?php echo content_detail('eng_compnies_pin_yposition',$mapPin->id); ?>
            },
            <?php $i++;} ?>
        ],
        responsive: true
    });
});

jQuery(document).ready(function () {
    var type = window.location.hash.substr(1);
    console.log(type);
    $('#'+type).modal('show');
    //assets page
});

$(document).ready(function () {
    var size_li = $(".filtersdatanews").length;
    x=6;
    $('.filtersdatanews:lt('+x+')').show();
    $('#loadMore').click(function () {
        
        $('.page_loader').show();
        
        
        var next = $('.filtersdatanews:not([style*="display: block"])').length;
        if(next <=6){
            $('#loadMore').hide('slow');
        }
        x= (x+6 <= size_li) ? x+6 : size_li;
        $('.filtersdatanews:lt('+x+')').show();
        
        setTimeout(
        function()
        {
            $('.page_loader').hide();
        }, 1500);
            
    });
});

$( document ).ready(function () {
    $(".filtersdata").slice(0, 12).show();
    $('.filtersdata:gt(11)').hide();
    if ($(".filtersdata:hidden").length > 12) {
        $("#load_more").show();
    }else{
        $("#load_more").hide();
    }
    $("#load_more").on('click', function (e) {
        e.preventDefault();
        $('.page_loader').show();
        $('html').css('overflow-y','hidden');
        if($(".filter-button").hasClass('active_cat')){
           var cat_id = $(".filter-button.active_cat ").data('filter');
           if(cat_id==='all'){
               $(".filtersdata:hidden").slice(0, 12).slideDown();
               if ($(".filtersdata:hidden").length === 0) {
                   $("#load_more").fadeOut('slow');
               }
               $('html,body').animate({
                   scrollTop: $(this).offset().top
               }, 1500);
               setTimeout(
                   function()
                   {
                       $('html').css('overflow-y','scroll');
                       $('.page_loader').hide();
                   }, 1500);
           }else{
               $("."+cat_id+":hidden").slice(0, 12).slideDown();
               if ($("."+cat_id+":hidden").length === 0) {
                   $("#load_more").fadeOut('slow');
               }
               $('html,body').animate({
                   scrollTop: $(this).offset().top
               }, 1500);
               setTimeout(
                   function()
                   {
                       $('html').css('overflow-y','scroll');
                       $('.page_loader').hide();
                   }, 1500);
           }
        }
    });

    $(".filtersdatanews").slice(0, 6).show();
    $('.filtersdatanews:gt(6)').hide();
    if ($(".filtersdatanews:hidden").length > 0) {
        $("#load_more_news").show();
    }else{
        $("#load_more_news").hide();
    }
    $("#load_more_news").on('click', function (e) {
        e.preventDefault();
        $('.page_loader').show();
        $('html').css('overflow-y','hidden');
        $(".filtersdatanews:hidden").slice(0, 6).slideDown();
        if ($(".filtersdatanews:hidden").length == 0) {
            $("#load_more_news").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
        setTimeout(
            function()
            {
                $('html').css('overflow-y','scroll');
                $('.page_loader').hide();
            }, 1500);
    });
});
</script>


<!--<script src="<?php //echo base_url('assets/frontend/js/calendar.js'); ?>"></script>-->



<div class="page_loader">

    <div class="lds-spinner" style="100%;height:100%">

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

        <div></div>

    </div>

</div>

</body>

</html>