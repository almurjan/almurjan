<!doctype html>
<?php $admin_lang = check_admin_lang();
if($admin_lang['admin_lang'] == 'eng'){
    $dir = '';
    $rtl = '';
}else{
    $dir = 'rtl';
    $rtl = 'rtl/';
}?>
<?php $config_settings = getConfigurationSetting(); ?>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" dir="<?php echo $dir; ?>"> <!--<![endif]-->
<script>
    var base_url = '<?php echo base_url();?>';
    var light_box_images = '';
    var admin_lang = '<?php echo $admin_lang['admin_lang'];?>';
</script>
<head data-lang="<?php echo $admin_lang['admin_lang'];?>">

<style>
	.k-i-calendar {
	  width: 20px;
	  height: 20px;
	  background: url(<?php echo base_url().'assets/admin/assets/img/calendar.png';?>) !important;
	  background-repeat: no-repeat;
	 }
	.success{
	 text-align-center;
	}
	.success{
	 text-align-center;
	}
	.success p{
	 text-align-center;
	max-width:100% !important;
	}
	#saveAbout{
		display: none;
	}
	.showMetaDivs .uk-width-medium-1-2{
		width: 47%;
		margin: 15px 25px 0px 0px;
	}
	.error{
		color: red !important;
	}
	#saveSocial, #saveAbout{
		display: none; 
	}
</style>

    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <link href="<?php echo base_url();?>assets/frontend/images/favicon.png" rel="shortcut icon"  type="image/ico" />
    <title><?php echo ($lang == 'eng' ? $config_settings->project_name : $config_settings->project_name_arb); ?> <?php echo $admin_lang['label']['admin_panel']; ?></title>
    <link href="<?php echo base_url(); ?>assets/frontend/images/favicon.png" rel="shortcut icon" type="image/ico"/>
    <!-- additional styles for plugins -->
	<!-- Chosen Jquery plugin for Multiple select options -->
	<link rel="stylesheet" href="<?php echo base_url('assets/frontend/chosen_v1.8.7/docsupport/style.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/frontend/chosen_v1.8.7/docsupport/prism.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/frontend/chosen_v1.8.7/chosen.css'); ?>">
    <!-- weather icons -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/weather-icons/css/weather-icons.min.css" media="all">
    <!-- metrics graphics (charts) -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/metrics-graphics/dist/metricsgraphics.css">
    <!-- kendo UI -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/kendo-ui/styles/kendo.common-material.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/kendo-ui/styles/kendo.material.min.css" id="kendoCSS"/>
    <!-- uikit -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
	
	<link media="all" type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>/bower_components/kendo-ui/styles/kendo.common-material.min.css">
	<link media="all" type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>/bower_components/kendo-ui/styles/kendo.material.min.css">
	
    <!-- flag icons -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/icons/flags/flags.min.css" media="all">
    <!-- style switcher -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/css/main.min.css" media="all">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css" />
    
	<!-- Popup css 
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/magnific-popup.css" media="all">
	-->
    <!-- themes -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/css/themes/themes_combined.min.css" media="all">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/custom_admin.css" media="all">
	
	<!-- ratings -->
	<link rel='stylesheet' id='jquery-rating-style-css'  href='<?php echo base_url().'assets/'?>css-front/jquery.ratingb2ab.css' type='text/css' media='all' />
	<link rel='stylesheet' id='jquery-wpcf7-rating-style-css'  href='<?php echo base_url().'assets/'?>css-front/jquery.wpcf7-starrating7ef2.css' type='text/css' media='all' />
	
    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
        <link rel="stylesheet" href="assets/css/ie.css" media="all">
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/tooltipster/dist/css/tooltipster.bundle.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/admin/assets/js/jquery.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/assets/js/jquery-ui-timepicker-addon.js" ></script>
	
    <script type="text/javascript" src="https://jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
    <link rel="stylesheet" href="https://jonthornton.github.io/jquery-timepicker/jquery.timepicker.css" type="text/css" />
    <!-- for time picker end -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/html5.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/kendo.web.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/ajaxfileupload.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.tablednd.js"></script>
    <!--
	<script type="text/javascript" charset="utf-8" src="<?php echo base_url();?>assets/js/e aa"></script>
	-->
    <script type="text/javascript" charset="utf-8" src="<?php echo base_url();?>assets/js/jquery.dataTables.rowReordering.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.msgBox.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.selectbox-0.2.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/tinymce/tinymce.min.js" ></script>
  
    
    <script>
        var mlib_domain = '<?php echo base_url();?>assets/script/';
        var base_url = '<?php echo base_url();?>';
    </script>
    <script src="<?php echo base_url();?>assets/script/mlib-includes/js/init.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/script/mlib-includes/js/mce_mlib.js"></script>
    <script>
		
			
        $(document).ready(function () {
            $("#specified_date_time").datepicker({
                dateFormat: "yy-mm-dd hh:ii",
                minDate: 0,
                changeMonth: true,
                changeYear: true,
                onSelect: function (date) {
                    var date2 = $('#specified_date_time').datepicker('getDate');
                    date2.setDate(date2.getDate());
                    $('#specified_date_time').datepicker('option', 'minDate', date2);
                }
            });
			
            $(".start_date").datepicker({
                dateFormat: "yy-mm-dd",
                minDate: 0,
                changeMonth: true,
                changeYear: true,
                onSelect: function (date) {
                    var date2 = $('.start_date').datepicker('getDate');
                    date2.setDate(date2.getDate());
                    $('.end_date').datepicker('option', 'minDate', date2);
                }
            });
            $('.end_date').datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                onClose: function () {
                    var start_date = $('.start_date').datepicker('getDate');
                    var end_date = $('.end_date').datepicker('getDate');
                }
            });
        });
        var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
        function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }
                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
        var _validFile_Extensions = [".pdf", ".doc"];
        function ValidateSingleInput_file(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFile_Extensions.length; j++) {
                        var sCurExtension = _validFile_Extensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }
                    if (!blnValid) {
                        alert("<?php echo $admin_lang['label']['sorry']; ?>, " + sFileName + " <?php echo $admin_lang['label']['valid_extensions_msg']; ?> " + _validFile_Extensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
        function getCityForEms(cid, actionUrl) {
            $("#ems_cities").html('');
            if (cid == '') {
                $("#ems_cities").val('');
                return true;
            }
            $.ajax({
                type: "POST",
                url: base_url + actionUrl,
                data: {'id': cid},
                dataType: "json",
                cache: false,
                success: function (result) {
                    if (result.error != 'false') {
                        $("#mySmallModalLabel").html('Error');
                        $("#message").html(result.error);
                        document.getElementById("show_success_messge").click();
                    } else {
                        $("#ems_cities").html('');
                        $("#ems_cities").html(result.cities);
                    }
                }
            });
        }
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    jQuery('.imageBox').attr('src','');
                    jQuery('.imageBox')
                        .attr('src', e.target.result)
                        .width(112)
                        .height(108);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        var result =new Array();
        function checkUniquePageTitle(check_form_type) { // check_form_type 0 for add 1 for edit
            tpl = document.getElementById('tpl').value;
            if (tpl === '') {
                alert("<?php echo $admin_lang['label']['please_select_page_template']; ?>");
                $(".page_loader").hide();
                return false;
            } else {
                title = document.getElementById('eng_title').value;
				if (check_form_type == 1) {
                    page_title_before_change = document.getElementById('page_title_before_change').value;
                    if (title == page_title_before_change) {
                        return true;
                    }
                }
                $.ajax({
                    type: "post",
                    url: "<?php echo base_url();?>ems/content/checkUniquePageTitle",
                    async: false,
                    data: 'page_title=' + title,
                    success: function (data) {
                        result = data.split('@@');
                    }
                });
                if (result[0] == 1 && title != '') {
                    alert("<?php echo $admin_lang['label']['page_title_unique']; ?>");
                    $(".page_loader").hide();
                    return false;
                } else {
                    return true;
                }
             }
        }
        function checkUniquePageTitleEdit(check_form_type) { // check_form_type 0 for add 1 for edit
            tpl = document.getElementById('tpl').value;
            if (tpl === '') {
                alert("<?php echo $admin_lang['label']['please_select_page_template']; ?>");
                $(".page_loader").hide();
                return false;
            } else {
                title = document.getElementById('eng_title').value;
				if (check_form_type == 1) {
                    page_title_before_change = document.getElementById('page_title_before_change').value;
                    if (title == page_title_before_change) {
                        return true;
                    }
                }
                $.ajax({
                    type: "post",
                    url: "<?php echo base_url();?>ems/content/checkUniquePageTitleEdit/<?php echo $this->uri->segment(5);?>",
                    async: false,
                    data: 'page_title=' + title,
                    success: function (data) {
                        result = data.split('@@');
                    }
                });
                if (result[0] == 1 && title != '') {
                    alert("<?php echo $admin_lang['label']['page_title_unique']; ?>");
                    $(".page_loader").hide();
                    return false;
                } else {
                    return true;
                }
             }
        }
    </script>
    <script src="<?php echo base_url();?>assets/js/jquery.icheck.min.js?v=0.9"></script>
    <script>
        $(document).ready(function(){
            $('.tooltip').tooltipster({
                theme: 'tooltipster-borderless',
                contentCloning: false
            });
            $('.autoTooltip').tooltipster({
                theme: 'tooltipster-borderless',
                contentCloning: false
            });
			
            show_div('<?php echo $result->tpl;?>');
            $("#preview").click(function (){
                var id = $("#page_id").val();
                if($("#tpl_id").val() > 0){
                    $("#updateAbout").attr("action", "<?php echo base_url();?>page/preview/"+id);
                }else{
                    $("#updateAbout").attr("action", "<?php echo base_url();?>page/preview/"+id);
                }
                $("#updateAbout").attr("target", "_blank");
                $("#updateAbout").submit();
            });
        });
        function hide_all_divs(){
            $(".hide").hide(hideCallback);
        }
        function show_div(template_name){
			removeCk();
            hide_all_divs();
            if (template_name == "home") {
                $(".home_div").show(showCallback);
            }
			else if (template_name == "about_us") {
				$(".about_us_div").show(showCallback);
            }
			else if (template_name == "profile") {
				$(".profile_div").show(showCallback);
            }
			else if (template_name == "history") {
				$(".history_div").show(showCallback);
            }
			else if (template_name == "mission_vision") {
				$(".mission_vision_div").show(showCallback);
            }
			else if (template_name == "why_fa") {
				$(".why_fa_div").show(showCallback);
            }
			else if (template_name == "ceo_message") {
				$(".ceo_message_div").show(showCallback);
            }
			else if (template_name == "ceo_clients") {
				$(".ceo_clients_div").show(showCallback);
            }
			else if (template_name == "careers") {
				$(".careers_div").show(showCallback);
            }
			else if (template_name == "privacy_policy") {
				$(".privacy_policy_div").show(showCallback);
            }
			else if (template_name == "terms") {
				$(".terms_div").show(showCallback);
            }
			else if (template_name == "contact_us") {
				$(".contact_us_div").show(showCallback);
            }
			else if (template_name == "security_printing") {
				$(".security_printing_div").show(showCallback);
            }
			else if (template_name == "introduction") {
				$(".introduction_div").show(showCallback);
            }else if (template_name == "security_features") {
				$(".security_features_div").show(showCallback);
            }else if (template_name == "our_services") {
				$(".our_services_div").show(showCallback);
            }else if (template_name == "events") {
				$(".events_div").show(showCallback);
            }else if (template_name == "our_approach") {
				$(".our_approach_div").show(showCallback);
            }else if (template_name == "media_center") {
				$(".media_center_div").show(showCallback);
            }else if (template_name == "request_now") {
				$(".request_now_div").show(showCallback);
            }else if (template_name == "news_room") {
				$(".news_room_div").show(showCallback);
            }else if (template_name == "faqs") {
				$(".faqs_div").show(showCallback);
            }else if (template_name == "csr") {
				$(".csr_div").show(showCallback);
            }else if (template_name == "self") {
				$(".self_div").show(showCallback);
            }else if (template_name == "murjan_holding") {
				$(".holding_div").show(showCallback);
            }else if (template_name == "bod") {
				$(".bod_div").show(showCallback);
            }else if (template_name == "other_sectors") {
				$(".other_sectors_div").show(showCallback);
            }else if (template_name == "landmarks") {
				$(".landmark_div").show(showCallback);
            }
			else{
                removeCk();
                hide_all_divs();
            }
            $(document).ready(function() {
                setTimeout(function () {
                    initCk();
                }, 500);
            });
        }
		
		
        function showCallback(){
            $(this).find('input, textarea, button, select').prop('disabled', false);
        }
        function hideCallback(){
            $(this).find('input, textarea, button, select').prop('disabled', true);
        }
        $(document).ready(function(){
            var callbacks_list = $('.demo-callbacks ul');
            function callback_log(id, type) {
                callbacks_list.prepend('<li><span>#' + id + '</span> is ' + type.replace('if', '').toLowerCase() + '</li>');
            };
            $('.demo-list input').on('ifCreated ifClicked ifChanged ifChecked ifUnchecked ifDisabled ifEnabled ifDestroyed', function(event){
                $("#pub_val").val($('.demo-list input').val());
                callback_log(this.id, event.type);
            }).iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
            //================================================end===============================================================
        });
        function removeImage(id){
            id_div = id;
            if(confirm("<?php echo $admin_lang['label']['delete_image']; ?>")){
                //$( "div" ).remove( "#"+id_div );
                $( "#"+id_div ).html("");
                var id_of_hdn_value = id_div.split('thumbdiv_');
                id_of_hdn_value = id_of_hdn_value[1];
                id_of_hdn_value = id_of_hdn_value+'_mkey_hdn';
                $("#"+id_of_hdn_value).val('');
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#submit_form').click(function(){
                $('#overlay_form').submit();
            });
            $('#overlay_form').validate();
//open popup
            $("#pop").click(function(){
                $("#overlay_form").fadeIn(1000);
                positionPopup();
            });
            $("#pop2").click(function(){
                $("#overlay_form").fadeIn(1000);
                positionPopup();
            });
            //close popup
            $("#close").click(function(){
                $("#overlay_form").fadeOut(500);
            });
        });
        //position the popup at the center of the page
        function positionPopup(){
            if(!$("#overlay_form").is(':visible')){
                return;
            }
            $("#overlay_form").css({
                left: ($(window).width() - $('#overlay_form').width()) / 2,
                top: ($(window).height() - $('#overlay_form').height()) / 2,
                height:385,
                width:400,
                position:'absolute'
            });
        }
        //maintain the popup at center of the page when browser resized
        $(window).bind('resize',positionPopup);
        $("#overlay_form").focus();
    </script>
</head>
<body class=" sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                                
                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>
                
                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>
                
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li>
                            <a href="javascript:void(0)" id="help_icon" class="user_action_image tooltip" title="<?php echo $admin_lang['label']['tooltip_help_icon']; ?>">
                                <img class="md-user-image" src="<?php echo base_url();?>assets/admin/assets/images/blue_question.png" alt=""/>
                            </a>
                        </li>
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large tooltip" title="<?php echo $admin_lang['label']['tooltip_fullScreen_icon']; ?>"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image tooltip" title="<?php echo $admin_lang['label']['tooltip_logout_icon']; ?>">
							<img class="md-user-image" src="<?php echo base_url();?>assets/admin/<?php echo $rtl; ?>assets/img/avatars/avatar_11_tn.png" alt=""/></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    <li><a href="javascript:void(0);"><?php echo $this->session->userdata('usr_uname'); ?></a></li>
                                    <li><a href="<?php echo base_url();?>ems/admin_login/logout"><?php echo $admin_lang['label']['logout']; ?></a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <button id="btn_ck_init" style="display: none;"></button>
    </header><!-- main header end -->