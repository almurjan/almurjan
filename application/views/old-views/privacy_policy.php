<?php 
$privacy_policy = getPageIdbyTemplate('privacy_policy');
$home = getPageIdbyTemplate('home');
?>
<section class=" MinHeight Site_Bg SpecialClass">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2><?php echo pageSubTitle($privacy_policy, $lang); ?></h2>
            </div>
        </div>
        <div class="row pt-5 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 BlockStyling">
                <?php
                $desc = content_detail($lang.'_desc_privacy',$privacy_policy);
                echo $desc;
                ?>
            </div>
        </div>
    </div>
</section>
