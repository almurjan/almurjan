<style>
.error {
    color:red;
}
</style>

<?php
	$home = getPageIdByTemplate('home');
	$contactUsId = getPageIdByTemplate('contact_us');
    $privacy_policy = getPageIdbyTemplate('privacy_policy');
	$contact_purpose_listings = ListingContent($contactUsId);
	$cordinates = content_detail('maps_loc',$page_id);
	$cordinates_exploded = explode(',',$cordinates);
?>

<div class="Site_Bg SpecialClass">
    <section class="mt-4 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                    <h2><?php echo pageSubTitle($page_id, $lang); ?> </h2>
                </div>
            </div>
        </div>
    </section>

    <div class="google-map">
        <div id="map" style="height:366px;width: 100%;border:0;"></div>
    </div>
    <section>
        <div class="container">
            <div class="row pt-5 pb-4">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 BlockStyling contact_border_right contactUsHeadingStyle">
                    <h2><?php echo ($lang=='eng')?'Send Message':'تواصل معنا'; ?></h2>
                    <form action="<?php echo base_url('page/submit_contact'); ?>" class="contact_us_form validate" method="post" onsubmit="return false;">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="form-group">

                                    <input required type="text" class="form-control inputTextOnly" name="name" placeholder="<?php echo ($lang == 'eng' ? 'Name*' : '*الأسم الكامل');?>" id="name">
                                    <input type="hidden"  name="lang" id="lang" value="<?php echo $lang ?>" >
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="form-group">
                                    <input required type="text" class="form-control numberOnly" name="mobile" placeholder="<?php echo ($lang == 'eng' ? 'Phone Number*' : '*رقم الهاتف');?>" id="number">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-0 mt-md-3">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="form-group">
                                    <input required type="email" class="form-control" name="email" placeholder="<?php echo ($lang == 'eng' ? 'Email*' : 'البريد الإلكتروني*');?>" id="u_email">

                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="form-group">
                                    <input required type="text" class="form-control" name="subject" placeholder="<?php echo ($lang == 'eng' ? 'Subject*' : 'الموضوع*');?>" id="subject">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-0 mt-md-3">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <textarea required class="form-control"  name="message" placeholder="<?php echo ($lang == 'eng' ? 'Message*' : '*الرسالة');?>" rows="14" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-0 mt-md-3">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="g-recaptcha" data-sitekey="6LcAJ_IUAAAAANqk0SOKASp4ACYC7tjqMXnyaWOV"></div>
                                <p style="color:red; font-weight:700;" id="captcha_message"></p>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="row align-items-center">
                                <div class="col-md-4 col-xl-4 col-lg-4 col-sm-12 col-12">
                                    <button class="btn btn-primary btn-lg active submitBlueBtn" role="button" aria-pressed="true"><?php echo ($lang == 'eng' ? 'Send' : 'إرسال');?></button>
                                </div>
                                <div class="col-md-8 col-xl-8 col-lg-8 col-sm-12 col-12 text-left text-xl-right text-lg-right text-md-right">
                                    <!--<div class="SmallText"><?php /*echo ($lang == 'eng' ? 'You consent to our' : 'فإنك توافق على موقعنا');*/?> <u><a href="<?php /*echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($privacy_policy, 'eng')); */?>"><?php /*echo pageSubTitle($privacy_policy, $lang); */?></a></u></div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 BlockStyling contactUsHeadingStyle mt-5 mt-lg-0">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
                            <h2><?php echo ($lang=='eng')?'Contact Details':'معلومات التواصل'; ?></h2>
                        </div>
                    </div>
                    <h4><?php echo ($lang == 'eng' ? 'Address' : 'العنوان');?></h4>
                    <p><?php echo content_detail($lang.'_desc_contact',$page_id); ?> </p>
                    <h4><?php echo ($lang == 'eng' ? 'Email' : 'البريد الإلكتروني');?></h4>
                    <p><a href="mailto:<?php echo content_detail('eng_mail',$page_id); ?>"><?php echo content_detail('eng_mail',$page_id); ?></a></p>
                    <h4><?php echo ($lang == 'eng' ? 'Phone' : 'رقم الهاتف');?></h4>
                    <p style="<?php echo ($lang == 'arb' ? 'direction: ltr;': ''); ?>"><?php echo content_detail('eng_phone',$page_id); ?></p>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
function load_map()
{
  var uluru = { lat: <?php echo $cordinates_exploded[0]; ?>, lng: <?php echo $cordinates_exploded[1]; ?> };
  /*var uluru_old = { lat: 24.720403782761128, lng: 46.678762435913086 };*/
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 18,
    center: uluru,
    mapTypeId: 'roadmap'
  });
  var image = "<?php echo base_url('assets/images/pinmap2.png') ?>";
  var marker = new google.maps.Marker({
    position: uluru,
    map: map,
      //label: {text: "ALMURJAN", color: "red"},
	icon: image,
	url: "https://www.google.com/maps/place/24%C2%B042'37.5%22N+46%C2%B040'36.1%22E/@24.7104238,46.6745138,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d24.7104238!4d46.6767025"
  });
  /*google.maps.event.addListener(marker, 'click', function() {
        window.open(this.url);
    });*/
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZfo9eInMuIGx4s4qBI6ewc2NVkd4gHOQ&callback=load_map" async defer></script>