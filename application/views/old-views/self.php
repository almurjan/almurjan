<?php
$home = getPageIdbyTemplate('home');
$self = getPageIdbyTemplate('self');
$careers = getPageIdbyTemplate('careers');
    if (content_detail('eng_selfapp_image_mkey_hdn', $self) != '') {
                        $selfapp_image = imageSetDimenssion(content_detail('eng_selfapp_image_mkey_hdn', $self), 450, 480, 1);
                    }else{
                        $no_img=base_url() . 'assets/images/no_image.png';
        $selfapp_image = $no_img;
                    }
    ?>
<style>
    /*#Self_Application_image:before {
        background: url(<?php echo $selfapp_image; ?>) no-repeat right !important;
    }*/
</style>
<section id="Self_Application_image" class=" MinHeight SpecialClass">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($home, 'eng')); ?>"><?php echo pageSubTitle($home, $lang); ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo pageSubTitle($careers, $lang); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2><?php echo pageSubTitle($self,$lang);?></h2>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 BlockStyling">
                <h2><?php echo ($lang == 'eng' ? 'Apply Now' : 'التقديم على وظائف');?></h2>

                    <form id="self_application_form" enctype="multipart/form-data" class="validate" action="<?php echo base_url('page/submit_self_application'); ?>" method="POST" onsubmit="return false;">
                    <div class="row mt-3">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="text" name="name" class="form-control inputTextOnly" placeholder="<?php echo ($lang == 'eng' ? 'Name*' : '*الاسم');?>" id="name">
                                <input type="hidden" name="lang" value="<?php echo $lang; ?>" >
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								 <input required type="text" class="form-control" name="title" placeholder="<?php echo ($lang == 'eng' ? 'Job Title*' : '*المسمى الوظيفي');?>" id="title">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								 <input required type="text" class="form-control numberOnly" name="experience" placeholder="<?php echo ($lang == 'eng' ? 'Experience years*' : '*سنوات الخبرة');?>" id="experiance">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="email" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'E-mail*' : '*البريد الإلكتروني');?>" name="email" id="u_email">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="form-group">
								<input required name="link" type="text" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'linked in Link*' : 'رابط LinkedIn*');?>" id="linkedin">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="g-recaptcha" data-sitekey="6LcAJ_IUAAAAANqk0SOKASp4ACYC7tjqMXnyaWOV"></div>
                            <p style="color:red; font-weight:700;" id="captcha_message"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-right">
                            <button type="submit" class="btn btn-primary btn-lg active submitBlueBtn" ><?php echo ($lang == 'eng' ? 'Send' : 'إرسال');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
