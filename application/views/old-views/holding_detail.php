<?php
$home = getPageIdbyTemplate('home');
$our_sectors = getPageIdbyTemplate('our_sectors');
$murjan_holding = getPageIdbyTemplate('murjan_holding');
?>
<section class="mt-4 MinHeight Site_Bg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($home, 'eng')); ?>"><?php echo pageSubTitle($home,$lang) ?></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($our_sectors, 'eng')); ?>"><?php echo pageSubTitle($murjan_holding,$lang) ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo pageSubTitle($page_id,$lang); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SectorDetailPage">
                <h2><?php echo pageSubTitle($page_id,$lang) ?></h2>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mb-4">
                <div class="GroupCompany GrayBorder">
                    <?php
                    $holding_sliders =  explode(',',content_detail('eng_holding_slider', $page_id));
                  
                    ?>
                    <div style="position: in" id="carousel-1" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php
                            $no_img=base_url() . 'assets/images/no_image.png';
                            if(content_detail('eng_holding_slider', $page_id)){
                            for($i=0;$i<=sizeof($holding_sliders);$i++) {
                                if($i==0){
                                   $slider_active = 'active';
                                }else{
                                    $slider_active = '';
                                }
                                //$holding_slider_img = imageSetDimenssion($holding_sliders[$i], 325, 118, 0);
                                
                                $holding_slider_img = base_url() . 'assets/script/' . $holding_sliders[$i];
                                
                                if($holding_sliders[$i] != ''){
                                ?>
                                <div class="carousel-item <?php echo $slider_active; ?>">
                                    <img class="img-fluid" src="<?php echo $holding_slider_img; ?>" alt="">
                                </div>
                         <?php  } } } else{?>
                                <div class="carousel-item active">
                                    <img style="max-width: 325px;max-height: 118px;" class="img-fluid" src="<?php echo $no_img; ?>" alt="">
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7 DetailText">
                
                <?php if(content_detail('eng_holding_link',$page_id)){
                    $url = content_detail('eng_holding_link',$page_id);
                    if(stripos($url,'http')===false) {
                        $url = '//' . $url;
                    }?>
                <h2><a target="_blank" href="<?php echo $url ?>"><?php echo ($lang == 'eng' ? 'Visit' : 'زيارة');?> <?php echo pageSubTitle($page_id,$lang) ?> <?php echo ($lang == 'eng' ? 'Website' : 'موقع الكتروني');?></a></h2>
                <?php } ?>
                <h3><a target="_blank" href="<?php echo $url ?>"><?php echo content_detail('eng_holding_link',$page_id) ?></a></h3>
                <p><?php echo content_detail($lang.'_holding_dec',$page_id); ?></p>
                
            </div>
        </div>
    </div>
</section>
