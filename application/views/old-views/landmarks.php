<?php
$home = getPageIdbyTemplate('home');
$profile = getPageIdbyTemplate('profile');
$about_us = getPageIdbyTemplate('about_us');
$ceo_message = getPageIdbyTemplate('ceo_message');
$landmarks = getPageIdbyTemplate('landmarks');
$murjan_group = getPageIdbyTemplate('murjan_group');
$category_listing = get_catogries_data(0);

/*$landmarks_listings = ListingContentLimit($landmarks,4);*/
$bod = getPageIdbyTemplate('bod');
?>
<style>
    .LandmarkHeight{
        height: auto !important;
    }
    .carousel-control-prev-icon, .carousel-control-next-icon{
        height: 45px !important;
        width: 45px !important;
    }
    .carousel-control-prev, .carousel-control-next{
        opacity: 1 !important;
    }
    body.arb .borderBottom h2:before {
        bottom: -12px;
        right: 0px;
    }
</style>

<section class="MinHeight SpecialClass csr_custom_class">
    <?php if(content_detail($lang.'_landmark_page_title',$page_id) || content_detail($lang.'_landmark_desc',$page_id)){?>
        <div class="assets_banner">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 BlockStyling new_heading_style">
                        <div class="assets_content">
                            <h2><?php echo content_detail($lang.'_landmark_page_title',$page_id);?></h2>
                            <?php echo content_detail($lang.'_landmark_desc',$page_id);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="assets_custom_nav mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="newdesign_border btn_new_style">
                        <button class="active_cat btn btn-default filter-button" onclick="tab_ative(this.id)" id="0" data-filter="all"><?php echo ($lang == 'eng' ? 'All' : 'الكل');?></button>
                        <?php foreach($category_listing as $category){
                            if($lang=='eng'){
                                $title_cat = $category->category_title;
                                $cat_desc = $category->category_desc_eng;
                            }else{
                                $title_cat = $category->category_title_ar;
                                $cat_desc = $category->category_desc_arb;
                            }
                            ?>
                            <button class="not_active btn btn-default filter-button" id="<?php echo $category->category_id;?>" data-filter="<?php echo $category->category_id;?>" data-cat-title="<?php  echo $title_cat; ?>" data-cat-desc="<?php  echo $cat_desc; ?>"><?php echo $title_cat;?></button>
                        <?php } ?>
                        <div class="dropdown locationbtn">
						<?php 
						$location_lbl = ($lang=='eng'?'Location':'موقعك');
						if($search) {
							$location_lbl = $search;
						}
						?>
                            <a href="javascript:void(0);" class="btn"> <?php echo $location_lbl; ?></a>
                            <div class="dropdown-menu dropdown-menu-right locationbtnDropDown" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item <?php echo ((html_escape($this->input->get('location')) == 'All' || html_escape($this->input->get('location')) == 'الكل')?'active':''); ?>" href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($page_id, 'eng')).'?location=All'; ?>"><?php echo ($lang == 'eng' ? 'All' : 'الكل');?></a>
                                <?php foreach($location_listings as $location_listing){
                                        if($lang=='eng'){
                                            $title_location = $location_listing->country_name;
                                        }else{
                                            $title_location = $location_listing->country_name_ar;
                                        }
                                    ?>
                                        <a class="dropdown-item <?php echo ($search == $title_location?'active':''); ?>" href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($page_id, 'eng')).'?location='.$title_location; ?>"><?php echo $title_location; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="profileNewstyle HomeAboutAlMurjan sector_heading category_title_area" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="assets_banner mt-3 pt-4 pb-4 category_desc_area" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-12 assets_content BlockStyling"></div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row landmarkContainer">
            <?php
            $k=0;
            foreach($listings as $landmarks_listing){
            if(($k % 2) == 0){
                $landmarks_id_two='';
                $landmarks_id_one='';
                $left_arrow= base_url('assets/frontend').'/images/landmarg_arrow_left.png';
                $right_arrow= '';
                $left_arrow_class= 'LandmarkArrow';
                $right_arrow_class= '';
            }else{
                $landmarks_id_two='DivTwo';
                $landmarks_id_one='DivOne';
                $left_arrow_class= '';
                $left_arrow= '';
                $right_arrow_class= 'LandmarkArrow1';
                $right_arrow= base_url('assets/frontend').'/images/landmarg_arrow_right.png';
            }
            if($k==0){
                $first_landmark='mt-5';
            }else{
                $first_landmark='';
            }

            $landmarks_image = imageSetDimenssion(content_detail('eng_landmarks_thumb_mkey_hdn', $landmarks_listing->id), 400, 350, 2);
            $marks_listing = str_replace(',,',',',content_detail('eng_landmarks_image', $landmarks_listing->id));
            $landmarks_sliders =  explode(',',$marks_listing);
            $landmarks_logo = imageSetDimenssion(content_detail('eng_landmarks_logo_mkey_hdn', $landmarks_listing->id), 92, 92, 0);
            //var_dump(sizeof($landmarks_sliders));

                if(empty(content_detail('eng_landmarks_thumb_mkey_hdn', $landmarks_listing->id))){
                    $no_img=imageSetDimenssion('no_image.png', 400, 350, 1);
                    $landmarks_image = $no_img;
                }
                ?>
                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 filtersdata filter <?php echo $landmarks_listing->category_id; ?> text-center">
                    <a href="javascript:void(0);" class="assets_city_detail" data-toggle="modal" data-target="#landmarkPopUp_<?php echo $landmarks_listing->id;?>">
                        <div class="landmarkBox mb-0">
                            <div class="image">
                                <img src="<?php echo $landmarks_image; ?>" class="img-fluid">
                            </div>
                        </div>
                        <div class="assets_detail">
                            <h5><?php echo pageTitle($landmarks_listing->id,$lang); ?></h5>
                        </div>
                    </a>
                </div>
                <div class="modal fade" id="landmarkPopUp_<?php echo $landmarks_listing->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-right: 0px !important;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                                <div class="row align-items-center">
                                    <div class="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 pl-0 pr-md-3 pr-0">
                                        <div class="image text-center text-md-left">
                                            <div id="carousel-<?php echo $landmarks_listing->id; ?>" class="carousel slide" data-ride="carousel">
                                                <ol class="carousel-indicators">
                                                    <?php

                                                    if(content_detail('eng_landmarks_image', $landmarks_listing->id)){
                                                        if(sizeof($landmarks_sliders)>1){
                                                            for($i=0;$i<sizeof($landmarks_sliders);$i++) {
                                                                if($i==0){
                                                                    $slider_active = 'active';
                                                                }else{
                                                                    $slider_active = '';
                                                                }
                                                                ?>
                                                                <li style="border-radius: 50%;width: 10px;height: 10px;" data-target="#carousel-<?php echo $landmarks_listing->id; ?>" data-slide-to="<?php echo $i; ?>" class="<?php echo $slider_active?>"></li>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </ol>
                                                <div class="carousel-inner">
                                                    <?php
                                                    $no_img=imageSetDimenssion('no_image.png', 535, 562, 2);
                                                    if(content_detail('eng_landmarks_image', $landmarks_listing->id)!='eng_landmarks_image'){

                          $active = 0;
                                                        for($i=0;$i<sizeof($landmarks_sliders);$i++) {
                                                            if($i==0){
                                                                $slider_active = 'active';
                                                            }else{
                                                                $slider_active = '';
                                                            }
                                                            //$holding_slider_img = imageSetDimenssion($holding_sliders[$i], 325, 118, 0);

                                                            //$landmarks_slider_img = base_url() . 'assets/script/' . $landmarks_sliders[$i];
                                                            $landmarks_slider_img = imageSetDimenssion(str_replace('_','',$landmarks_sliders[$i]), 535, 562, 2);
                                                            if($landmarks_sliders[$i] != '' && strpos($landmarks_sliders[$i], 'mlib-uploads') !== false){
                                                                $active++;
                                                                ?>
                                                                <div class="carousel-item <?php echo ($active == 1 ? 'active' : ''); ?> LandmarkHeight">
                                                                    <img class="img-fluid" src="<?php echo $landmarks_slider_img;?>" alt="">
                                                                </div>
                                                            <?php  }elseif($landmarks_sliders[$i] == ''){?>
                                                                <div class="carousel-item active LandmarkHeight">
                                                                    <img class="w-100" src="<?php echo $no_img; ?>" alt="">
                                                                </div>
                                                            <?php } } } else{?>
                                                        <div class="carousel-item active LandmarkHeight">
                                                            <img class="w-100" src="<?php echo $no_img; ?>" alt="">
                                                        </div>
                                                    <?php } ?>
                                                </div>
												<?php if(sizeof($landmarks_sliders)>1){ ?>
                                                <!-- Left and right controls -->
                                                <a class="carousel-control-prev" href="#carousel-<?php echo $landmarks_listing->id; ?>" data-slide="prev">
                                                    <span class="carousel-control-prev-icon"></span>
                                                </a>
                                                <a class="carousel-control-next" href="#carousel-<?php echo $landmarks_listing->id; ?>" data-slide="next">
                                                    <span class="carousel-control-next-icon"></span>
                                                </a>
												<?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                                        <div class="bodModelContent">
                                            <h2><?php echo pageTitle($landmarks_listing->id,$lang); ?></h2>
                                            <div class="moderDiscription">
                                                <?php echo str_replace('<p>&nbsp;</p>','',content_detail($lang.'_landmarks_dec',$landmarks_listing->id));?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $k++; } ?>
        </div>
        <div class="row">
            <div class="col-12 text-right">
                <div class="load_more">
                    <button id="load_more" class="btn">+ &nbsp; <?php echo ($lang=='eng'?'Load More':'تحميل المزيد'); ?></button>
                </div>
            </div>
        </div>
    </div>
</section>
