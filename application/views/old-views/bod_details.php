<?php
$home = getPageIdbyTemplate('home');
$bod = getPageIdbyTemplate('bod');
$bod_listings = ListingContent($bod);
if(content_detail('eng_dob_image_mkey_hdn', $page_id)){
    $dob_image = imageSetDimenssion(content_detail('eng_dob_image_mkey_hdn', $page_id), 350, 546, 1);
}else{
    $no_img=base_url() . 'assets/images/no_image.png';
    $dob_image = $no_img;
}
?>


<section class="mt-4 MinHeight">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url(); ?>"><?php echo pageSubTitle($home,$lang) ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($parent_id, 'eng')); ?>"><?php echo pageSubTitle($parent_id,$lang) ?></a></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SectorDetailPage">
                <h2><?php echo pageSubTitle($page_id,$lang); ?></h2>
                <p style="font-size: 21px !important;" class="BoardMember"><?php echo content_detail($lang.'_designation',$page_id) ?></p>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 BlockStyling">
                <div class="position-relative">
                    <img class="w-100 mb-4" src="<?php echo $dob_image;?>" alt="">
                    <div class="Picture-Caption1">
                        <h2><?php echo pageSubTitle($page_id,$lang); ?></h2>
                        <p class="BoardMember"><?php echo content_detail($lang.'_designation',$page_id) ?></p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 BlockStyling">
                <?php echo content_detail($lang.'_dob_dec',$page_id) ?>
            </div>
        </div>
    </div>
</section>
