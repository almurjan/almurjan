<?php
$murjan_holding = getPageIdbyTemplate('murjan_holding');
$murjan_holding_listings = ListingContent($murjan_holding);
//$holding_logo = imageSetDimenssion(content_detail('eng_holding_logo_mkey_hdn', $murjan_holding), 200, 200, 1);

$holding_logo = base_url() . 'assets/script/' . content_detail('eng_holding_logo_mkey_hdn', $murjan_holding);
?>
<section class="MinHeight Site_Bg SpecialClass">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2><?php echo pageSubTitle($murjan_holding,$lang);?></h2>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 GroupText text-center">
                <?php if(content_detail('eng_holding_logo_mkey_hdn', $murjan_holding)){ ?>
                <div class="Holding-Logo"><img src="<?php echo $holding_logo;?>" alt=""></div>
                <?php } ?>
                <p><?php echo content_detail($lang.'_desc_holding',$murjan_holding);?></p>
            </div>
        </div>
        <div class="row pt-4">
            <?php
            $i=1;
            $count_holding = count($murjan_holding_listings);
            foreach($murjan_holding_listings as $murjan_holding_listing){

                if (content_detail('eng_holding_logo_mkey_hdn', $murjan_holding_listing->id) != '') {
                    /* $murjan_holding_image = imageSetDimenssion(content_detail('eng_holding_logo_mkey_hdn', $murjan_holding_listing->id), 190, 100, 1);
                    $style = ''; */

					//$murjan_holding_image = imageSetDimenssion(content_detail('eng_holding_logo_mkey_hdn', $murjan_holding_listing->id), 170, 170, 0);

                    $murjan_holding_image = base_url() . 'assets/script/' . content_detail('eng_holding_logo_mkey_hdn', $murjan_holding_listing->id);

                    $style = 'width: 170px';

                }else{
                    $no_img=base_url() . 'assets/images/no_image.png';
                    $murjan_holding_image = $no_img;
                    $style = 'max-width:170px;max-height:170px';
                }
                if($i==5 && $count_holding<6){
                    $extra_class='offset-lg-3 offset-xl-3';
                }else{
                    $extra_class='';
                }
                ?>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 <?php echo $extra_class;?> col-xl-3 mb-5 GroupText text-center">
                    <a href="<?php echo lang_base_url() . 'page/almurjan_holding_detail/'.$murjan_holding_listing->id;?>">
                        <div class="GroupCompany">
                            <img style="<?php echo $style; ?>" src="<?php echo $murjan_holding_image; ?>" alt="">
                        </div>
                        <h5><?php echo pageSubTitle($murjan_holding_listing->id,$lang); ?></h5>
                    </a>
                </div>
                <?php $i++; } ?>
        </div>
    </div>
</section>