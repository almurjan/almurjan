<?php 
$self = getPageIdbyTemplate('self');
$home = getPageIdbyTemplate('home');
$careers = getPageIdbyTemplate('careers');
$positions_listings = careersListingContent($careers,0);
$benefits_listings = careersListingContent($careers,1);

//var_dump($positions_listings);die;

?>


<section class="SpecialClass MinHeight Site_Bg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2><?php echo pageTitle($careers, $lang); ?></h2>
            </div>
        </div>
        <div class="row pt-5 pb-4 align-items-center">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mb-4 Careers_Content">
                <h2><?php echo pageSubTitle($careers, $lang); ?></h2>
                <?php
                $desc = content_detail($lang.'_desc_career',$careers);
                echo $desc;
                ?>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mb-4">
                <?php
                $image = imageSetDimenssion(content_detail('eng_careers_image_mkey_hdn',$careers),540,496);
                if(content_detail('eng_careers_image_mkey_hdn',$careers) != '') {
                    ?>
                    <img class="img-fluid" src="<?php echo $image;?>" alt="">
                <?php } ?>

            </div>
        </div>
        <div class="row pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4 Careers_Content">
                <h2><?php echo content_detail($lang.'_benefits_heading',$careers);?></h2>
                <?php
                $desc = content_detail($lang.'_desc_benefits',$careers);
                echo $desc;
                ?>
            </div>
        </div>
        <div class="row pb-4">
            <?php foreach($benefits_listings as $benefits_listing){ ?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mb-4 Careers_Content">
                <h3><?php echo pageSubTitle($benefits_listing->id,$lang);?></h3>
                <?php
                $desc = content_detail($lang.'_benefits_desc',$benefits_listing->id);
                echo $desc;
                ?>
            </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Careers_Content">
                <h2><?php echo content_detail($lang.'_positions_heading',$careers);?></h2>
                <?php
                $desc = content_detail($lang.'_desc_positions',$careers);
                echo $desc;
                ?>
            </div>
        </div>
        <div class="row mt-2">
            <?php foreach($positions_listings as $positions_listing){ ?>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mb-4 Careers_Content">
                <a href="<?php echo lang_base_url().'page/vacancies_detail/'.$positions_listing->id; ?>">
                    <div class="Position-Box">
                        <h4><?php echo pageSubTitle($positions_listing->id,$lang);?></h4>
                        <ul>
                            <li><?php echo content_detail($lang.'_vac_area',$positions_listing->id);?></li>
                            <li><?php echo content_detail($lang.'_vac_type',$positions_listing->id);?></li>
                        </ul>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>

        <div class="row mt-5 text-center">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4 Careers_Content">
                <h2><?php echo ($lang == 'eng' ? 'Nothing for you here?' : 'لا شيء لك هنا؟');?></h2>
                <nav aria-label="breadcrumb">
                    <ol style="display: inline-flex; margin-top: 0" class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url().'page/self_application'?>"><?php echo pageSubTitle($self,$lang); ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
