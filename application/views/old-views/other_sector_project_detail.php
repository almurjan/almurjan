<?php
$home = getPageIdbyTemplate('home');
$other_sectors = getPageIdbyTemplate('other_sectors');
$parent_id=getParentCategoryId($page_id);
$osdetail_listings = ListingContent($page_id);
//var_dump($page_id);die;
?>
<section class="Site_Bg MinHeight SpecialClass">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($home, 'eng')); ?>"><?php echo pageSubTitle($home,$lang) ?></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($other_sectors, 'eng')); ?>"><?php echo pageSubTitle($other_sectors,$lang) ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo lang_base_url() . 'page/other_sector_detail/'.$parent_id;?>"> <?php echo pageSubTitle($parent_id,$lang); ?></a></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-8 SectorDetailPage">
                <h2><?php echo pageSubTitle($page_id,$lang) ?></h2>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-xl-12 col-lg-12 col-sm-12 col-12">
                <div class="otherSectorDetail">
                    <h2><?php echo ($lang=='eng')?'About':'عن الشركة' ?></h2>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                            <?php
                            $image = content_detail('eng_other_sectors_logo_mkey_hdn', $page_id);
                            //var_dump($image);die;
                            ?>
                            <div class="GroupCompany GrayBorder mb-3">
                                <div style="position: in" id="carousel-1" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <?php

                                        $no_img=imageSetDimenssion('no_image.png', 255, 387, 1);

                                        $image_text = str_replace('','',content_detail('eng_other_sectors_slider', $page_id));
                                        $explodedValues = explode(',',trim($image_text, ','));

                                        $i = 1;
                                        foreach($explodedValues as $explodedValueSingle){
                                            if(strpos($explodedValueSingle, '___') !== false && strlen($explodedValueSingle) > 15){
                                                $explodedValueTitle = explode('___',$explodedValueSingle);

                                                if($explodedValueTitle[1] == ''){
                                                    $explodedValueTitle[1] = $explodedValues[$i];
                                                }
                                                $titles[] = $explodedValueTitle[0];
                                                $images[] = $explodedValueTitle[1];

                                            } else{
                                                $titles[] = '';
                                                $images[] = $explodedValueSingle;
                                            }

                                            $i++;
                                        }

                                        $newTitles = array();
                                        foreach($titles as $singleTitle){
                                            if($singleTitle){
                                                $newTitles[] = $singleTitle;
                                            }
                                            else{
                                                $newTitles[] = '';
                                            }
                                        }
                                        foreach($images as $imageNew){
                                            if($imageNew !='' && $imageNew != '___'){
                                                $imageNews[] = $imageNew;
                                            }
                                        }

                                        $other_sectors_sliders =  explode(',',$image_text);

                                        if($image_text != 'eng_other_sectors_slider' && !empty($image_text)){
                                            $slider_count=0;
                                            for($i=0;$i<=sizeof($imageNews);$i++) {
                                                if($i==0){
                                                    $slider_active = 'active';
                                                }else{
                                                    $slider_active = '';
                                                }

                                                //$other_sectors_sliders_img = imageSetDimenssion($other_sectors_sliders[$i], 325, 118, 1);
                                                $other_sectors_sliders_img=$imageNews[$i];

                                                if($imageNews[$i] && $imageNews[$i]!='eng_other_sectors_slider'){
                                                    $slider_count++;
                                                    if($slider_count==1){
                                                        $slider_active = 'active';
                                                    }else{
                                                        $slider_active = '';
                                                    }
                                                    ?>
                                                    <div class="carousel-item <?php echo $slider_active; ?>">
                                                        <img style="height: 185px;" class="img-fluid" src="<?php echo base_url('assets/script/').'/'.$other_sectors_sliders_img; ?>" alt="">
                                                    </div>
                                                <?php  } }}else{
                                            $no_img=imageSetDimenssion('no_image.png', 170, 170, 1);

                                            ?>
                                            <div class="carousel-item active">
                                                <img style="max-width: 325px;max-height: 118px;" class="img-fluid" src="<?php echo $no_img; ?>" alt="">
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                            <div class="detailContent">
                                <?php echo content_detail($lang.'_other_sectors_proj_dec',$page_id); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
