<?php
$home = getPageIdbyTemplate('home');
$profile = getPageIdbyTemplate('profile');
$about_us = getPageIdbyTemplate('about_us');
$ceo_message = getPageIdbyTemplate('ceo_message');
$murjan_group = getPageIdbyTemplate('murjan_group');
$landmarks = getPageIdbyTemplate('landmarks');
$bod = $page_id;
$bod_listings = ListingContent($page_id,$contents[0]->tpl);
$page_company_image = base_url().'assets/script/'.content_detail('eng_bod_logo_image_mkey_hdn', $page_id);
$page_company_desc = content_detail($lang.'_bod_desc', $page_id);
?>
<section class=" MinHeight Site_Bg site_bgNone SpecialClass">
    <div class="container">

        <section class="about_custom_nav mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="newdesign_border">
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($profile, 'eng')); ?>" > <?php echo $ar_space.pageSubTitle($profile, $lang).$ar_space; ?></a></li>
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($ceo_message, 'eng')); ?>"> <?php echo $ar_space.pageSubTitle($ceo_message, $lang).$ar_space; ?></a></li>
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(16, 'eng')); ?>" class="<?php echo ($page_id == '16' ? 'active' : ''); ?>"> <?php echo $ar_space.pageSubTitle(16, $lang).$ar_space; ?></a></li>
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(1293, 'eng')); ?>?id=1293" class="<?php echo ($page_id == '1293' ? 'active' : ''); ?>"> <?php echo $ar_space.pageSubTitle(1293, $lang).$ar_space; ?></a></li>
                            <!--<li><a href="<?php /*echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(1305, 'eng')); */?>" class="<?php /*echo ($page_id == '1305' ? 'active' : ''); */?>"> <?php /*echo $ar_space.pageSubTitle(1305, $lang).$ar_space; */?></a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>

        <!-- new box add -->
    <?php if(content_detail('eng_bod_logo_image_mkey_hdn', $page_id) || $page_company_desc){?>
        <div class="container-fluid newcontentadd">
            <div class="row justify-content-center">
                <div class="col-xl-3 col-lg-4 col-md-5 col-sm-12 col-12">
                    <div class="pageLogo text-center mb-4 mb-md-0 bod_page_logo">
                        <?php if(content_detail('eng_bod_logo_image_mkey_hdn', $page_id)){?>
                            <a href="javascript:void(0);"><img class="img-fluid" src="<?php echo $page_company_image; ?>"></a>
                       <?php } ?>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-7 col-md-7 col-sm-12 col-12">
                    <div class="TextDiscription BlockStyling">
                        <?php echo $page_company_desc;?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
        <div class="container">
        <div class="row pt-5 pb-4">

            <?php   $i=1;
            $newArray = array();

            $i = 1;
            foreach($bod_listings as $bod_listing) {
                if($bod_listing->category == 1){
                    $newArray['1'][] = $bod_listing;
                }
                if($bod_listing->category == 2){
                    $newArray['2'][] = $bod_listing;
                }
                if($bod_listing->category == 3){
                    $newArray['3'][] = $bod_listing;
                }
                if($bod_listing->category == 4){
                    $newArray['4'][] = $bod_listing;
                }
                if($bod_listing->category == 5){
                    $newArray['5'][] = $bod_listing;
                }
                if($bod_listing->category == 6){
                    $newArray['6'][] = $bod_listing;
                }
            }

            //echo '<pre>'; print_r($newArray);

            foreach($newArray as $key=>$cat_listing){
                if($cat_listing){

                    if($key == 1){
                        $category_name =  ($lang == 'eng' ? 'Board of Directors' : 'أعضاء مجلس الإدارة');
                        if($page_id!=16){
                            $sectionOneClass='fiveInRow';
                        }else{
                            $sectionOneClass='fiveInRow';
                        }
                    }
                    if($key == 2){
                        $category_name =  ($lang == 'eng' ? 'Executive Committee' : 'اللجنة التنفيذية');
                         if($page_id!=16){
                            $sectionTwoClass='';
                        }else{
                            $sectionTwoClass='fourInRow';
                            $sectionOneClass='';
                        }
                        
                    }
                    if($key == 3){
                        $category_name =  ($lang == 'eng' ? 'Leadership Team' : 'فريق القيادة');
                         if($page_id!=16){
                            $sectionThreeClass='';
                        }else{
                            $sectionThreeClass='fiveInRow';
                            $sectionOneClass='';
                            $sectionTwoClass='';
                        }
                        
                    }
                    if($key == 4){
                        $category_name =  ($lang == 'eng' ? 'Investment Advisory Board' : 'المجلس الاستشاري للاستثمار');
                        if($page_id!=16){
                            $sectionFourClass='fiveInRow';
                            $sectionThreeClass='';
                            $sectionOneClass='';
                            $sectionTwoClass='';
                        }else{
                            $sectionFourClass='';
                        }
                    
                    }
                    if($key == 5){
                        $category_name =  ($lang == 'eng' ? 'Management Team' : 'فريق الإدارة');
                        if($page_id!=16){
                            $sectionFiveClass='fourInRow';
                            $sectionFourClass='';
                            $sectionThreeClass='';
                            $sectionOneClass='';
                            $sectionTwoClass='';
                        }else{
                            $sectionFiveClass='';
                        }
                    
                    }
                    if($key == 6){
                        $category_name =  ($lang == 'eng' ? 'Executive Team' : 'الفريق التنفيذي');
                        if($page_id!=16){
                            $sectionSixClass='fiveInRow';
                            $sectionFiveClass='';
                            $sectionFourClass='';
                            $sectionThreeClass='';
                            $sectionOneClass='';
                            $sectionTwoClass='';
                        }else{
                            $sectionSixClass='fiveInRow';
                        }

                    }
                    echo '<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 innerheading SiteHeadings text-center">
                        <h2>'.$category_name.'</h2>
                    </div>';

                    echo '<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 bodCustomClass">
                            <ul class="BlockStyling pl-0 '.$sectionOneClass.''.$sectionTwoClass.''.$sectionThreeClass.''.$sectionFourClass.' '.$sectionFiveClass.' '.$sectionSixClass.'">';
                    foreach($cat_listing as $singleRecord) {

                        $dob_image = imageSetDimenssion(content_detail('eng_dob_image_mkey_hdn', $singleRecord->id), 350, 546, 1);
                        //$dob_image = base_url().'assets/script/'.content_detail('eng_dob_image_mkey_hdn', $singleRecord->id);
                        if(content_detail('eng_dob_image_mkey_hdn', $singleRecord->id)){
                            $dob_image=$dob_image;
                            $showBorderCls = '';
                        }else{
                            $no_img=imageSetDimenssion('logo_noimage.png', 150, 150, 1);
                            $dob_image=$no_img;
                            $showBorderCls = 'imgWithBorder';
                        }

                        ?>
                       
                        
                                <li data-toggle="modal" data-target="#bodPopUp_<?php echo $singleRecord->id;?>" class="<?php echo (content_detail($lang.'_dob_dec',$singleRecord->id) == '' ? 'no_cursor' : ''); ?>">
                                    <div class="bodMainBox <?php echo $showBorderCls; ?>" style="background: url('<?php echo $dob_image; ?>') no-repeat;">
                                    </div>
                                    <div class="contantBox">
                                        <h3><?php echo pageTitle($singleRecord->id,$lang);?></h3>
                                        <p class="BoardMember"><?php echo content_detail($lang.'_designation',$singleRecord->id);?></p>
                                    </div>
                                </li>
                            
                                <?php if (content_detail($lang.'_dob_dec',$singleRecord->id) != '') { ?>
                                 <div class="modal fade" id="bodPopUp_<?php echo $singleRecord->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-right: 0px !important;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row align-items-center">
                                                <div class="col-xl-5 col-lg-5 col-md-6 col-sm-6 col-12 pl-0 pr-sm-3 pr-0">
                                                    <div class="image text-center text-sm-left">
                                                        <img class="img-fluid" src="<?php echo $dob_image; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-xl-7 col-lg-7 col-md-6 col-sm-6 col-12">
                                                    <div class="bodModelContent">

                                                        <h2><?php echo pageTitle($singleRecord->id,$lang);?></h2>
                                                        <h6><?php echo content_detail($lang.'_designation',$singleRecord->id);?></h6>
                                                        <div class="moderDiscription">
                                                            <?php echo content_detail($lang.'_dob_dec',$singleRecord->id) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }
                }
                    echo '</ul>
                        </div>';

                } ?>



               <?php
            } ?>
        </div>
    </div>
</section>

