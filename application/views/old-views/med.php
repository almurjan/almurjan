<?php
$selfapp_image = base_url() . 'assets/frontend/images/med2.jpg';
?>

<style>
	<!-- #Self_Application_image:before {
		background: url('<?php //echo $selfapp_image; ?>') no-repeat right !important;
	} -->
    .form-check-inline {
        margin-right: 25px !important;
        padding-bottom: 10px !important;
    }
	.select2-container--default .select2-selection--single{
		height: 50px !important;
		border-radius: 0 !important;
		border: 1px solid #ced4da !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		color: #ced4da !important;
		line-height: 45px !important;
		padding-left: 20px !important;
		font-size: 18px !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow{
		height:45px !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow b{
		border-color: #ced4da transparent transparent transparent !important;
	}
    .select2{
        width: 100% !important;
    }

    @media screen and (max-width: 400px) {
        .logosize{
            max-width: 200px !important;
        }
        .Mainlogosize{
            max-width: 80px !important;
        }
    }
</style>

<section id="Self_Application_image" class="mt-4 MinHeight">
    <div class="container">
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2><?php echo pageSubTitle($self,$lang);?></h2>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 BlockStyling">
                <!--<h2><?php echo ($lang == 'eng' ? 'Apply Now' : 'التقديم على وظائف');?></h2>-->

                    <form id="med_form" enctype="multipart/form-data" class="validate" action="<?php echo base_url('page/med_submit'); ?>" method="POST" onsubmit="return false;">
                    <div class="row mt-3">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="text" name="f_name" class="form-control inputTextOnly" placeholder="<?php echo ($lang == 'eng' ? 'First Name*' : 'الاسم الاول*');?>" id="f_name">
                                <input type="hidden" name="lang" value="<?php echo $lang; ?>" >
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								 <input required type="text" class="form-control" name="m_name" placeholder="<?php echo ($lang == 'eng' ? 'Middle Name*' : 'اسم الاب*');?>" id="m_name">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								 <input required type="text" class="form-control" name="l_name" placeholder="<?php echo ($lang == 'eng' ? 'Last Name*' : 'اللقب*');?>" id="l_name">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="text" class="form-control numberOnly" placeholder="<?php echo ($lang == 'eng' ? 'Age*' : 'العمر*');?>" name="age" id="age">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								 <input required type="text" class="form-control numberOnly" name="mob_no" placeholder="<?php echo ($lang == 'eng' ? 'Mobile Number*' : 'رقم الجوال*');?>" id="mob_no">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="email" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'Email address*' : 'البريد الإلكتروني*');?>" name="email" id="email">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <select required id="nationality" name="nationality" class="form-control js-ns-single">
                                    <option value=""><?php echo ($lang == 'eng' ? 'Nationality*' : 'الجنسية*');?></option>
                                    <?php echo getAllCountriesNew($lang,'','country'); ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <select required id="c_residence" name="c_residence" class="form-control js-rs-single">
                                    <option value=""><?php echo ($lang == 'eng' ? 'Country of Residence*' : 'بلد الإقامة*');?></option>
                                    <?php echo getAllCountriesNew($lang,'','country'); ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <select class="form-control js-crs-single" id="ct_residence" name="ct_residence" required>
                                    <option value=""><?php echo ($lang == 'eng' ? 'City of Residence*' : 'مدينة الإقامة*');?></option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h3 style="min-height: unset !important; padding-bottom: 20px"><?php echo ($lang == 'eng' ? 'Education Level*' : '*المستوى التعليمي');?></h3>

                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="education_level" id="education_level" value="<?php echo ($lang == 'eng' ? 'High School Diploma' : 'الثانوية العامة');?>" checked>
                                <label class="form-check-label" for="exampleRadios1">
                                    <?php echo ($lang == 'eng' ? 'High School Diploma' : 'الثانوية العامة');?>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="education_level" id="education_level" value="<?php echo ($lang == 'eng' ? 'Diploma' : 'دبلوم');?>" >
                                <label class="form-check-label" for="exampleRadios1">
                                    <?php echo ($lang == 'eng' ? 'Diploma' : 'دبلوم');?>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="education_level" id="education_level" value="<?php echo ($lang == 'eng' ? 'Bachelor’s degree' : 'بكالوريوس');?>" >
                                <label class="form-check-label" for="exampleRadios1">
                                    <?php echo ($lang == 'eng' ? 'Bachelor’s degree' : 'بكالوريوس');?>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="education_level" id="education_level" value="<?php echo ($lang == 'eng' ? 'Master’s degree' : 'ماجيستير');?>" >
                                <label class="form-check-label" for="exampleRadios1">
                                    <?php echo ($lang == 'eng' ? 'Master’s degree' : 'ماجيستير');?>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="education_level" id="education_level" value="<?php echo ($lang == 'eng' ? 'Doctorate degree' : 'دكتوراة');?>" >
                                <label class="form-check-label" for="exampleRadios1">
                                    <?php echo ($lang == 'eng' ? 'Doctorate degree' : 'دكتوراة');?>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input education_level_other" type="radio" name="education_level" id="education_level" value="Other" >
                                <label class="form-check-label" for="exampleRadios1">
                                    <?php echo ($lang == 'eng' ? 'Other, please specify' : 'أخرى، يرجى التحديد');?>
                                </label>
                            </div>
                            <div style="display: none" id="show_edu" class="form-group">
                                <input placeholder="<?php echo ($lang == 'eng' ? 'Write Here' : 'الرجاء التحديد');?>"  class="form-control"  type="text" name="education_level_other" id="show-me">
                            </div>
                        </div>
                    </div>
					<div class="row mt-3">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								 <input required type="text" class="form-control" name="specialization" placeholder="<?php echo ($lang == 'eng' ? 'Specialization*' : 'التخصص*');?>" id="specialization">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="text" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'Institution Name*' : 'اسم الجامعة/المؤسسة التعليمية*');?>" name="institute_name" id="institute_name">
                            </div>
                        </div>
                    </div>
					<div class="row mt-2">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								 <input required type="text" class="form-control" name="last_job" placeholder="<?php echo ($lang == 'eng' ? 'Last job title*' : 'المسمى الوظيفي في اخر وظيفة*');?>" id="last_job">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="text" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'Total number of working experience*' : 'إجمالي سنوات الخبرة العملية*');?>" name="experience" id="experience">
                            </div>
                        </div>
                    </div>
					<div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								 <input required type="text" class="form-control" name="last_employees" placeholder="<?php echo ($lang == 'eng' ? 'Last Three Employees*' : 'اسم اخر ثلاثة شركات عملت بها*');?>" id="last_employees">
                            </div>
                        </div>
                    </div>
                        <div class="row mt-2">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <h3 style="min-height: unset !important; padding-bottom: 20px"><?php echo ($lang == 'eng' ? 'English Proficiency:' : '*مستوى اللغة الإنجليزية:');?></h3>
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="eng_prof" id="eng_prof" value="<?php echo ($lang == 'eng' ? 'Beginner:' : 'مبتدئي ');?>" >
                                        <label class="form-check-label" for="exampleRadios1">
                                            <?php echo ($lang == 'eng' ? 'Beginner' : 'مبتدئي ');?>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="eng_prof" id="eng_prof" value="<?php echo ($lang == 'eng' ? 'Intermediate' : 'متوسط');?>" >
                                        <label class="form-check-label" for="exampleRadios1">
                                            <?php echo ($lang == 'eng' ? 'Intermediate' : 'متوسط');?>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="eng_prof" id="eng_prof" value="<?php echo ($lang == 'eng' ? 'Advanced' : 'متقدم');?>" >
                                        <label class="form-check-label" for="exampleRadios1">
                                            <?php echo ($lang == 'eng' ? 'Advanced' : 'متقدم');?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
					<div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<textarea required  class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'Any other additional information' : 'أي معلومات اضافية أخرى');?>" name="additional_info" id="additional_info"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<h3><?php echo ($lang == 'eng' ? 'Other Data for Medical Positions' : 'بيانات تخص الوظائف الطبية');?></h3>
						</div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<textarea required class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'Do you possess any other certifications: Board, Fellowship, Specialist Training etc, please specify:' : 'هل تحمل شهادات مهنية أخرى: بورد، زمالة، تدريب متخصص الخ، يرجى تحديدها:');?>" name="any_other_Cert" id="any_other_Cert"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h3 style="min-height: unset !important; padding-bottom: 20px"><?php echo ($lang == 'eng' ? 'ARE YOU CERTIFIED BY SAUDI COMMISSION FOR HEALTH SPECIALTIES:' : ':هل انت مرخص من الهيئة السعودية للتخصصات الصحية');?></h3>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="saudi_comm" id="saudi_comm" value="<?php echo ($lang == 'eng' ? 'Yes' : 'نعم ');?>" >
                                    <label class="form-check-label" for="exampleRadios1">
                                        <?php echo ($lang == 'eng' ? 'Yes' : 'نعم ');?>
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="saudi_comm" id="saudi_comm" value="<?php echo ($lang == 'eng' ? 'No' : 'لا');?>" >
                                    <label class="form-check-label" for="exampleRadios1">
                                        <?php echo ($lang == 'eng' ? 'No' : 'لا');?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
					
					<div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="g-recaptcha" data-sitekey="6LcAJ_IUAAAAANqk0SOKASp4ACYC7tjqMXnyaWOV"></div>
                            <p style="color:red; font-weight:700;" id="captcha_message"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <button type="submit" class="btn btn-primary btn-lg active submitBlueBtn" ><?php echo ($lang == 'eng' ? 'Send' : 'إرسال');?></button>
                        </div>
                    </div>
					
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</section>
