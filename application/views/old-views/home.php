<?php

$home_id = getPageIdbyTemplate('home');

$home_listings = ListingContent($home_id);

$about_us_id = getPageIdbyTemplate('about_us');
$profile_id = getPageIdbyTemplate('profile');

$murjan_holding = getPageIdbyTemplate('murjan_holding');

$other_sectors = getPageIdbyTemplate('other_sectors');

$other_sectors_home_listings = ListingContent($other_sectors);

foreach($other_sectors_home_listings as $other_sectors_home_listing){

    $other_sectors_home_page_child_listings[] = ListingContent($other_sectors_home_listing->id,'homepage_sector_sorting');
}

$other_sectors_listings = ListingContent($other_sectors);

foreach($other_sectors_listings as $other_sectors_listing){

    $other_sectors_child_listings[] = ListingContent($other_sectors_listing->id);

}

$murjan_holding_listings = ListingContent($murjan_holding);

$logo_slider_listings = array_merge($murjan_holding_listings,$other_sectors_child_listings);

$slider_counter=0;

foreach($murjan_holding_listings as $logo_slider_listing){



    if(content_detail('is_show_home', $logo_slider_listing->id) == 1){

        $slider_counter++;

    }

}

$holding_logo_slider=$slider_counter;

if($holding_logo_slider >= 5){

    $margin_left = '-200px';

    $display='block';

	$width = '';

}

else{

    $margin_left = '0px';

    $display='none';

    $width = '280px';

}

$i = 0;

?>

<style>
/*
    .owl-carousel

    {

        -ms-touch-action: pan-y;

        touch-action: pan-y;

    }
 */


    <?php

    if($lang == 'eng'){?>

    .owl-stage {

        left: <?php echo $margin_left;?>;

    }

    <?php }else{ ?>

    .owl-stage {

        right: <?php echo $margin_left;?>;

    }

    <?php } ?>

    .owlMyNav{

        display: <?php echo $display; ?>;

    }

    .hold_logo_slider .owl-item{

        width: <?php echo $width;?> !important;
*/
</style>


<section class="sliderBox" id="HomePageSlider">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="HomeAboutAlMurjan sliderHeading customHeading">
                    <h2><?php echo content_detail($lang . '_slider_heading', $page_id);?></h2>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                <div class="customSlider">
                    <div id="mainslider" class="carousel slide homeSlider" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php
                            $i = 0;
                            foreach ($home_listings as $home_listing) {
                                if (count($home_listings) > 1) {
                                    if($i == 0){

                                        $slider_active = 'active';
                                    }else{
                                        $slider_active = '';
                                    }
                                    ?>
                                    <li data-target="#mainslider" data-slide-to="<?php echo $i; ?>" class="<?php echo $slider_active; ?>"></li>
                                <?php $i++;}
                            } ?>
                        </ol>
                        <div class="carousel-inner">
                            <?php
                            $i = 0;
                            foreach ($home_listings as $home_listing) {

                                if($i == 0){
                                    $slider_active = 'active';
                                }else{
                                    $slider_active = '';
                                }
                                #todo:: need to use it in slider (after zeeshan made new design)
                                $video_link = content_detail('eng_home_videoLink1', $home_listing->id);
                                $slider_link = content_detail('eng_home_sliderLink', $home_listing->id);
                                if($slider_link){
                                    if(stripos($slider_link,'http')===false) {
                                        $slider_link = '//' . $slider_link;
                                    }
                                    $class="";
                                }else{
                                    $slider_link = "javascript:void(0)";
                                    $class="noPointer";
                                }
                                $video_link_exploded = explode('=', $video_link);
                                $video = $video_link_exploded[1];
                                $image = '';
                                $desc = '';
                                if (content_detail('eng_home_thumbImage_mkey_hdn', $home_listing->id) != '') {
                                    $image = base_url() . 'assets/script/' . content_detail('eng_home_thumbImage_mkey_hdn', $home_listing->id);
                                    //$image = imageSetDimenssion(content_detail('eng_home_thumbImage_mkey_hdn', $home_listing->id), 1300, 1000, 1);
                                }else{
                                    $image = base_url() . 'assets/images/noImage.jpg';
                                }
                                if (content_detail('eng_video_thumb_mkey_hdn', $home_listing->id) != '') {
                                    $video_thumb = base_url() . 'assets/script/' . content_detail('eng_video_thumb_mkey_hdn', $home_listing->id);
                                }
                                $desc = content_detail($lang . '_home_sliderText', $home_listing->id);
                                $subtitle = pageSubTitle( $home_listing->id,$lang);
                                $title = pageTitle( $home_listing->id,$lang);
                                if ($home_listing->selectSlideOptions == 1) { ?>
                                    <div class="carousel-item <?php echo $slider_active; ?>">
                                        <a class="<?php echo $class; ?>" href="<?php echo $slider_link; ?>">
                                            <img src="<?php echo $image; ?>" class="img-fluid">
                                        </a>
                                    </div>
                                <?php $i++; }
                            } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="downarrow">
            <a class="d-inline-block skip_to_section" id="skip_to_section1" href="#About-Al-Murjan">
                <i class="fas fa-caret-down"></i>
            </a>
        </div>
    </div>
</section>


<section id="About-Al-Murjan" class="section2bg customBtnBg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 HomeAboutAlMurjan customHeading borderBottom">

                <h2><?php echo content_detail($lang.'_about_heading',$home_id); ?></h2>

                <p><?php echo content_detail($lang.'_about_desc',$home_id); ?></p>

                <a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($profile_id, 'eng')); ?>"><?php echo ($lang == 'eng' ? 'Discover More  ' : 'إكتشف المزيد '); ?> <!-- <img src="<?php echo base_url('assets/frontend');  ?>/images/more_arrow.png" alt=""> -->
                    <i class="fas fa-chevron-right"></i>
                </a>
            </div>
        </div>
    </div>
</section>
 
<section class="SectionPaddingTop customBtnBg pb-5">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="HomeAboutAlMurjan customHeading mb-5 sector_heading">
                    <?php echo content_detail($lang.'_other_sector_home_desc',$other_sectors); ?>
                    <a class="mt-4" href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($other_sectors, 'eng')); ?>"><?php echo ($lang == 'eng' ? 'Explore Sectors  ' : 'استكشف القطاعات '); ?><i class="fas fa-chevron-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 iconsSectionCompany">
                <div class="row">
                    <?php
                    foreach($other_sectors_listings as $other_sectors_listing){
                        $title = pageSubTitle($other_sectors_listing->id,$lang);
                        //$cat_icon = imageSetDimenssion(content_detail('eng_other_sectors_cat_icon_mkey_hdn', $other_sectors_listing->id), 112, 97, 1);
                        $cat_icon = base_url().'assets/script/'.content_detail('eng_other_sectors_cat_icon_mkey_hdn', $other_sectors_listing->id);
                        if(content_detail('eng_other_sectors_cat_icon_mkey_hdn', $other_sectors_listing->id)){
                            $cat_icon = $cat_icon;


                        }else{
                            $no_img=imageSetDimenssion('logo_noimage.png', 112, 97, 1);
                            $cat_icon=$no_img;
                            
                        }
                       $cat_hover_icon = base_url().'assets/script/'.content_detail('eng_other_sectors_cat_hover_icon_mkey_hdn', $other_sectors_listing->id);
                       if(content_detail('eng_other_sectors_cat_hover_icon_mkey_hdn', $other_sectors_listing->id))
                        {
                            $cat_hover_icon = $cat_hover_icon;
                        }
                        else
                        {
                            $cat_hover_icon = $cat_icon;
                        }
                        ?>
                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-6 text-center">
                            <a href="<?php echo lang_base_url() . 'page/Our_Sectors/'.$other_sectors_listing->id;?>" class="companyLogoContainer">
                                <div class="companyLogo">
                                    <img class="img-fluid  default_img" src="<?php echo $cat_icon; ?>">
                                    <img class="img-fluid  hover_img" src="<?php echo $cat_hover_icon;?>">
                                </div> 
                                <div class="HomeAboutAlMurjan">
                                    <h2><?php echo $title; ?></h2>
                                </div>
                            </a>
                        </div>
                    <?php }?>
                </div>
            </div>
            
        </div>
    </div>
</section>

<section class="mt-sm-5 mt-0">

    <div class="container">

        <div class="row">

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 HomeAboutAlMurjan customHeading borderBottom">

                <h2><?php echo content_detail($lang.'_companies_heading', $page_id) ?></h2>

            </div>

        </div>

    </div>

    <div class="container-fluid">

        <div class="row HotSpotBGImage">

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                <div id="mapsvg"></div>
            </div>

        </div>

    </div>

</section>

<section id="My_Achievements_image" class="SectionPaddingTop mt-5">

    <div class="container">

        <div class="row">

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <h2 class="d-none"><?php echo content_detail($lang.'_achievements_heading',$home_id); ?></h2>

                <div class="row">

                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">

                        <div class="sectors">

                            <span class="counter" data-count="<?php echo content_detail('eng_achievements_sectors',$home_id); ?>"></span><span class="plus">+</span>

                            <p><?php echo ($lang == 'eng' ? 'Sectors' : 'قطاعات');?></p>

                        </div>

                    </div>

                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">

                        <div class="sectors">

                            <span class="counter" data-count="<?php echo content_detail('eng_achievements_experience',$home_id); ?>"></span><span class="plus">+</span>

                            <p><?php echo ($lang == 'eng' ? 'Years of experience' : 'عامًا من الخبرة');?></p>

                        </div>

                    </div>

                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">

                        <div class="sectors">

                            <span class="counter" data-count="<?php echo content_detail('eng_achievements_companies',$home_id); ?>"></span><span class="plus">+</span>

                            <p><?php echo ($lang == 'eng' ? 'Companies' : 'شركة');?></p>

                        </div>

                    </div>

                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">

                        <div class="sectors">

                            <span class="counter" data-count="<?php echo content_detail('eng_achievements_employee',$home_id); ?>"></span><span class="plus">+</span>

                            <p><?php echo ($lang == 'eng' ? 'Employee' : 'موظف');?></p>

                        </div>

                    </div>

                </div>

                <div class="Achievement-Text">

                    <p><?php echo content_detail($lang.'_achievements_desc',$home_id); ?></p>

                </div>

            </div>

        </div>

    </div>

</section>

<section class="SectionPaddingTop Achievement">
    <?php
    $image = base_url() . 'assets/script/' . content_detail('eng_achievements_image_mkey_hdn', $home_id);
    ?>
    <div class="container">
        <div class="row">
            <?php
            $sectors_logo_sorting = [];
            $home_page_sorted_companies = [];
            foreach ($other_sectors_home_page_child_listings as  $other_sectors_home_page_child_listing)
            {

                foreach ($other_sectors_home_page_child_listing as $sector_child_listing)
                {

                    if(content_detail('is_show_home', $sector_child_listing->id) == 1)
                    {
                        $home_page_sorted_companies[] = $sector_child_listing;
                        $sectors_logo_sorting[] = $sector_child_listing->sectors_logo_sorting;
                    }
                }
            }
            /*sorting*/
                array_multisort($sectors_logo_sorting, SORT_ASC, $home_page_sorted_companies);
                foreach ($home_page_sorted_companies as $sector_child_listing)
                {

                    $sector_img = base_url().'assets/script/'.content_detail('eng_other_sectors_logo_for_home_mkey_hdn', $sector_child_listing->id);
                    if(content_detail('eng_other_sectors_logo_for_home_mkey_hdn', $sector_child_listing->id))
                    {
                        ?>
                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6">
                            <a href="javascript:void(0);">
                                <div class="companies_logo">
                                    <a href="<?php echo lang_base_url().'page/other_sector_detail/'.$sector_child_listing->id;?>">
                                        <img src="<?php echo $sector_img; ?>" alt="" class="img-fluid">
                                    </a>
                                </div>
                            </a>
                        </div>

                        <?php
                    }
                }
                ?>
            
        </div>
    </div>
</section>



