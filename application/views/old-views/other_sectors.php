<?php
$home = getPageIdbyTemplate('home');
$other_sectors = getPageIdbyTemplate('other_sectors');
$other_sectors_listings = ListingContent($other_sectors);
$slider_one = array();
$slider_two = array();
$slider_three = array();
foreach($other_sectors_listings as $other_sectors_listing){
    $category_listings = ListingContent($other_sectors_listing->id);
    foreach($category_listings as $category_listing){
        if(content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id)){
            $slider_one[$category_listing->id] = content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id);
        }
        /*$slider_no = content_detail('slider_number', $category_listing->id);
        if($slider_no==1 && content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id)){
            $slider_one[$category_listing->id] = content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id);
        }elseif($slider_no==2 && content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id)){
            $slider_two[$category_listing->id] = content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id);
        }elseif($slider_no==3 && content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id)){
            $slider_three[$category_listing->id] = content_detail('eng_other_sectors_slider_image_mkey_hdn', $category_listing->id);
        }*/
    }
}
?>
<style>
    /*.sector_category .active_cat{
       background-color: #00447C;
       color: #C8A443;
    }*/
</style>
<!--slider section start-->
<section class="sector_slider">
</section>
<!--slider section and-->

<section class="sector_desc">
    <div class="container">
        <div class="row">

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="sector_text">
                    <?php echo content_detail($lang.'_other_sector_page_desc',$page_id)?>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="sector_slider_container">
                    <div id="industrial_slider" class="carousel slide" data-ride="carousel" data-interval="4000">
                        <?php if(count($slider_one) > 1){ ?>
                            <ol class="carousel-indicators">
                                <?php
                                $i=0; foreach($slider_one as $key => $value){
                                    if($i==0){
                                        $active_class = 'active';
                                    }else{
                                        $active_class = '';
                                    }
                                    ?>
                                    <li data-target="#industrial_slider" data-slide-to="<?php echo $i; ?>" class="<?php echo $active_class; ?>"></li>
                                    <?php $i++; } ?>
                            </ol>
                        <?php } ?>
                        <div class="carousel-inner">
                            <?php
                            if(count($slider_one) > 0){
                                $i=0; foreach($slider_one as $key => $value){
                                    if($i==0){
                                        $active_class_slider = 'active';
                                    }else{
                                        $active_class_slider = '';
                                    }
                                    if(content_detail($lang.'_other_sectors_dec',$key)){
                                        $slider_one_link = lang_base_url() . 'page/other_sector_detail/'.$key;
                                    }else{
                                        $slider_one_link = "javascript:void(0)";
                                    }
                                    $slider_one_title = pageSubTitle($key,$lang);
                                    ?>
                                    <div class="carousel-item <?php echo $active_class_slider; ?>">
                                        <a href="<?php echo $slider_one_link; ?>">
                                            <img class="img-fluid" src="<?php echo base_url('assets/script/') .'/'. $value; ?>" alt="<?php echo $slider_one_title; ?>" title="<?php echo $slider_one_title; ?>">
                                        </a>
                                    </div>
                                    <?php
                                    $i++;
                                }
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Site_Bg site_bgNone MinHeight SpecialClass btn_new_style custompadding" id="otherSectorsPage">
    <div class="container">
        <!-- <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2> <?php // echo pageSubTitle($other_sectors,$lang);?></h2>
            </div>
        </div> -->
        <div class="row pt-4 pb-4">
            <div class="sector_category col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" align="center">
                <div class="newdesign_border">
                    <?php
                    if(!empty($other_sectors_listings)){?>

                        <?php $i=0; foreach($other_sectors_listings as $other_sectors_listing){
                            if($i==0){?>
                                <button class="active_cat btn btn-default filter-button" onclick="tab_ative(this.id)" id="0" data-filter="all"><?php echo ($lang == 'eng' ? 'All' : 'الكل');?></button>
                            <?php } ?>
                            <button class="not_active btn btn-default filter-button"  id="<?php echo $other_sectors_listing->id; ?>" data-filter="<?php echo $other_sectors_listing->id; ?>"><?php echo pageSubTitle($other_sectors_listing->id,$lang); ?></button>
                            <?php $i++;} ?>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row pt-3">
            <?php
            $repeated=false;
            foreach($other_sectors_listings as $other_sectors_listing){
                $category_listings = ListingContent($other_sectors_listing->id);
                foreach($category_listings as $category_listing){
                    $flag = isFlagValue($category_listing->id);
                    if($flag->is_flag==1){
                        $repeated_class = 'isFlag';
                        $repeated_íd = 1;
                    }else{
                        $repeated_class='';
                        $repeated_íd = 0;
                    }
                    if (content_detail('eng_other_sectors_logo_mkey_hdn', $category_listing->id) != '') {
                        
                        //$category_listing_image = imageSetDimenssion(content_detail('eng_other_sectors_logo_mkey_hdn', $category_listing->id), 170, 170, 1);
                        
                        $category_listing_image = base_url() . 'assets/script/' . content_detail('eng_other_sectors_logo_mkey_hdn', $category_listing->id);
                        
                        $style = 'max-width: 100%';
                    }else{
                        $no_img=base_url() . 'assets/images/no_image.png';
                        $category_listing_image = $no_img;
                        $style = 'max-width:170px;max-height:170px';
                    }
                ?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 mb-5 GroupText text-center gallery_product filter <?php echo $other_sectors_listing->id.' ' .$repeated_class; ?> " data-id="<?php echo $repeated_íd;?>">
                <a class="logos_newstyle" href="<?php echo lang_base_url() . 'page/other_sector_detail/'.$category_listing->id;?>">
                    <div class="GroupCompany">
                        <img style="<?php echo $style; ?>" src="<?php echo $category_listing_image; ?>" alt="">
                    </div>
                <!-- <h5><?php //echo //pageSubTitle($category_listing->id,$lang); ?></h5>-->
                </a>
            </div>
            <?php } } ?>
        </div>
    </div>
</section>
