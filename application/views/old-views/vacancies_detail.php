<?php
$careers = getPageIdbyTemplate('careers');
$careers_listings = ListingContent($careers);
$vacancy_name = pageSubTitle($page_id, $lang);
$home = getPageIdbyTemplate('home');
?>

<section class="mt-4 MinHeight">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($home, 'eng')); ?>"><?php echo pageSubTitle($home, $lang); ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo pageSubTitle($careers, $lang); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SectorDetailPage">
                <h2><?php echo pageSubTitle($page_id, $lang); ?></h2>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 BlockStyling">
                <?php echo content_detail($lang.'_vacancy_desc',$page_id)?>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 BlockStyling">
                <h2><?php echo($lang == 'eng' ? 'Apply Now' : 'تطبيق') ?></h2>
                <form id="job_application_form" class="validate" enctype="multipart/form-data" action="<?php echo base_url('page/job_application_form'); ?>" method="POST" onsubmit="return false;">
                    <div class="row mt-3">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="text" name="full_name" class="form-control inputTextOnly" placeholder="<?php echo ($lang == 'eng' ? 'Name*' : '*الاسم');?>" id="name">
                                <input type="hidden" name="lang" value="<?php echo $lang; ?>" >
                                <input type="hidden" name="job_id" value="<?php echo $page_id; ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="email" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'E-mail*' : 'البريد الإلكتروني*');?>" name="email" id="u_email">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required type="text" class="form-control numberOnly" name="experience" placeholder="<?php echo ($lang == 'eng' ? 'Experience years*' : '*سنوات الخبرة');?>" id="experiance">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
								<input required name="link" type="text" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'linked in Link*' : 'رابط LinkedIn*');?>" id="linkedin">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group file_div">
                                <label for="file" class="sr-only"><?php echo ($lang == 'eng' ? 'File*' : '*تحميل الملفات');?></label>
                                <div class="input-group">
                                    <input required type="text" name="filename" class="form-control" placeholder="<?php echo ($lang == 'eng' ? 'Your CV*' : '*السيرة الذاتية');?>" readonly>
                                    <span class="input-group-btn uploadfileBtn">
                                    <div class="btn btn-default  custom-file-uploader">
                                        <input type="file" name="c_v" onchange="this.form.filename.value = this.files.length ? this.files[0].name : ''" />
                                    <?php echo ($lang == 'eng' ? 'Upload' : 'تحميل');?>
                                    </div>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="g-recaptcha" data-sitekey="6LcAJ_IUAAAAANqk0SOKASp4ACYC7tjqMXnyaWOV"></div>
                            <p style="color:red; font-weight:700;" id="captcha_message"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <button type="submit" class="btn btn-primary btn-lg active submitBlueBtn w-100" aria-pressed="true"><?php echo ($lang == 'eng' ? 'Send' : 'إرسال');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
