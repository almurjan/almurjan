<?php
$home = getPageIdbyTemplate('home');
$other_sectors = getPageIdbyTemplate('other_sectors');
$parent_id=getParentCategoryId($page_id);
$osdetail_listings = ListingContent($page_id);

?>
<section class="Site_Bg MinHeight SpecialClass">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($home, 'eng')); ?>"><?php echo pageSubTitle($home,$lang) ?></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($other_sectors, 'eng')); ?>"><?php echo pageSubTitle($other_sectors,$lang) ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo pageSubTitle($parent_id,$lang); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-8 SectorDetailPage">
                <h2><?php echo pageSubTitle($page_id,$lang) ?></h2>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-4 text-right">
                <?php if(content_detail('eng_other_sectors_link',$page_id)){
                    $url = content_detail('eng_other_sectors_link',$page_id);
                    if(stripos($url,'http')===false) {
                        $url = '//' . $url;
                    }
                    ?>
                    <div class="linkarrow">
                        <h4><a target="_blank" href="<?php echo $url; ?>"><?php echo ($lang == 'eng' ? 'Visit' : 'زيارة');?> <?php echo pageSubTitle($page_id,$lang) ?> <?php echo ($lang == 'eng' ? 'Website' : 'موقع الكتروني');?></a></h4>

                        <span><a style="color: #7F7F7F" target="_blank" href="<?php echo $url; ?>"><?php echo content_detail('eng_other_sectors_link',$page_id) ?></a></span>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php
        if(content_detail($lang.'_other_sectors_ceo_name',$page_id) || content_detail($lang.'_other_sectors_ceo_position',$page_id) || content_detail($lang.'_other_sectors_ceo_dec',$page_id)){
            $cols_class='col-xl-9 col-lg-9 col-md-8';
        }else{
            $cols_class='col-xl-12 col-lg-12 col-md-12';
        }
        if(content_detail('eng_other_sectors_slider', $page_id)){
            $top_slider_col= 'col-xl-8 col-lg-8 col-md-7';
        }else{
            $top_slider_col= 'col-xl-12 col-lg-12 col-md-12';
        }
        ?>
        <div class="row pt-4 pb-4">
            <div class="<?php echo $cols_class; ?> col-sm-12 col-12">
                <div class="otherSectorDetail">
                    <h2><?php echo ($lang=='eng')?'About':'عن الشركة' ?></h2>
                    <div class="row">
                        <?php if(content_detail('eng_other_sectors_slider', $page_id)){ ?>
                        <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                            <?php
                            $image = imageSetDimenssion(content_detail('eng_other_sectors_slider_mkey_hdn', $page_id), 325, 118, 1);
                            ?>
                            <div class="GroupCompany GrayBorder mb-3">
                                <div style="position: in" id="carousel-1" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                <?php

                                $no_img=imageSetDimenssion('no_image.png', 255, 387, 1);

                                $image_text = str_replace('','',content_detail('eng_other_sectors_slider', $page_id));
                                $explodedValues = explode(',',trim($image_text, ','));

                                $i = 1;
                                foreach($explodedValues as $explodedValueSingle){
                                    if(strpos($explodedValueSingle, '___') !== false && strlen($explodedValueSingle) > 15){
                                        $explodedValueTitle = explode('___',$explodedValueSingle);

                                        if($explodedValueTitle[1] == ''){
                                            $explodedValueTitle[1] = $explodedValues[$i];
                                        }
                                        $titles[] = $explodedValueTitle[0];
                                        $images[] = $explodedValueTitle[1];

                                    } else{
                                        $titles[] = '';
                                        $images[] = $explodedValueSingle;
                                    }

                                    $i++;
                                }

                                $newTitles = array();
                                foreach($titles as $singleTitle){
                                    if($singleTitle){
                                        $newTitles[] = $singleTitle;
                                    }
                                    else{
                                        $newTitles[] = '';
                                    }
                                }
                                foreach($images as $imageNew){
                                    if($imageNew !='' && $imageNew != '___'){
                                        $imageNews[] = $imageNew;
                                    }
                                }

                                $other_sectors_sliders =  explode(',',$image_text);

                               if($image_text != 'eng_other_sectors_slider'){
                                   $slider_count=0;
                                    for($i=0;$i<=sizeof($imageNews);$i++) {
                                        if($i==0){
                                            $slider_active = 'active';
                                        }else{
                                            $slider_active = '';
                                        }

                                        //$other_sectors_sliders_img = imageSetDimenssion($other_sectors_sliders[$i], 325, 118, 1);
                                        $other_sectors_sliders_img=$imageNews[$i];

                                        if($imageNews[$i] && $imageNews[$i]!='eng_other_sectors_slider'){
                                            $slider_count++;
                                            if($slider_count==1){
                                                $slider_active = 'active';
                                            }else{
                                                $slider_active = '';
                                            }
                                        ?>
                                        <div class="carousel-item <?php echo $slider_active; ?>">
                                            <img style="height: 185px;" class="img-fluid" src="<?php echo base_url('assets/script/').'/'.$other_sectors_sliders_img; ?>" alt="">
                                        </div>
                                    <?php  }
                                    }
                               }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="<?php echo $top_slider_col;?> col-sm-12 col-12">
                            <div class="detailContent">
                                <?php echo content_detail($lang.'_other_sectors_dec',$page_id); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $ceo_image = imageSetDimenssion(content_detail('eng_other_sectors_ceo_img_mkey_hdn', $page_id), 255, 387, 1);
            if(empty(content_detail('eng_other_sectors_ceo_img_mkey_hdn', $page_id))){
                $ceo_image=$no_img;
            }

            if(content_detail($lang.'_other_sectors_ceo_name',$page_id) || content_detail($lang.'_other_sectors_ceo_position',$page_id) || content_detail($lang.'_other_sectors_ceo_dec',$page_id)){
            ?>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12 mt-4 mt-md-0">
                <a href="javascript:void(0);" data-toggle="modal" data-target="#bodPopUp">
                    <div class="otherSectorDetail">
                        <h2><?php echo content_detail($lang.'_other_sectors_ceo_heading',$page_id); ?></h2>
                        <div class="ceocontentBox">
                            <div class="iamge">
                                <img src="<?php echo $ceo_image; ?>" class="img-fluid">
                            </div>
                            <div class="ceoTitleBox">
                                <h3><?php echo content_detail($lang.'_other_sectors_ceo_name',$page_id); ?></h3>
                                <h4><?php echo ($lang=='eng')?'Know more':'تعرف أكثر' ?></h4>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
        <div class="row mt-3 mb-5 pb-4">

            <?php if($osdetail_listings){ ?>
                <div class="otherSectorDetail col-12">
                    <h2>
                        <?php
                        if($page_id!='262'){
                            echo ($lang=='eng')?'Projects':'المشاريع';
                        }else{
                            echo ($lang=='eng')?'Brands':'العلامات التجارية';
                        }
                        ?>
                    </h2>
                </div>
                <?php foreach($osdetail_listings as $osdetail_listing){
                    $project_detail_desc = content_detail($lang.'_other_sectors_proj_dec',$osdetail_listing->id);
                    $project_Detail_page_link = 'onclick="window.location.href= \''.lang_base_url() . "page/other_sector_project_detail/".$osdetail_listing->id.'\';" ';

                    ?>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 mb-4">

                        <div <?php echo ($project_detail_desc!='')?$project_Detail_page_link:''?> class="GroupCompany <?php echo ($project_detail_desc!='')?'project_slider':''?>">
                            <div style="position: in" id="carousel-1" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php

                                    $no_img=imageSetDimenssion('no_image.png', 255, 387, 1);

                                    $image_text = str_replace('','',content_detail('eng_other_sectors_slider', $osdetail_listing->id));
                                    $explodedValuess = explode(',',trim($image_text, ','));
                                    $titless = array();
                                    $imagess = array();
                                    $i = 1;
                                    foreach($explodedValuess as $explodedValueSingle){
                                        if(strpos($explodedValueSingle, '___') !== false && strlen($explodedValueSingle) > 15){
                                            $explodedValueTitle = explode('___',$explodedValueSingle);

                                            if($explodedValueTitle[1] == ''){
                                                $explodedValueTitle[1] = $explodedValues[$i];
                                            }
                                            $titless[] = $explodedValueTitle[0];
                                            $imagess[] = $explodedValueTitle[1];

                                        } else{
                                            $titless[] = '';
                                            $imagess[] = $explodedValueSingle;
                                        }

                                        $i++;
                                    }

                                    $newTitless = array();
                                    $imageNewss = array();
                                    foreach($titles as $singleTitle){
                                        if($singleTitle){
                                            $newTitless[] = $singleTitle;
                                        }
                                        else{
                                            $newTitless[] = '';
                                        }
                                    }
                                    foreach($imagess as $imageNeww){
                                        if($imageNeww !='' && $imageNeww != '___'){
                                            $imageNewss[] = $imageNeww;
                                        }
                                    }

                                    $other_sectors_sliderss =  explode(',',$image_text);

                                    if($image_text != 'eng_other_sectors_slider' && !empty($image_text)){
                                        $slider_count=0;
                                        for($i=0;$i<=sizeof($imageNewss);$i++) {
                                            if($i==0){
                                                $slider_active = 'active';
                                            }else{
                                                $slider_active = '';
                                            }

                                            //$other_sectors_sliders_img = imageSetDimenssion($other_sectors_sliders[$i], 325, 118, 1);
                                            $other_sectors_sliders_imgg=$imageNewss[$i];

                                            if($imageNewss[$i] && $imageNewss[$i]!='eng_other_sectors_slider'){
                                                $slider_count++;
                                                if($slider_count==1){
                                                    $slider_active = 'active';
                                                }else{
                                                    $slider_active = '';
                                                }
                                                ?>
                                                <div class="carousel-item <?php echo $slider_active; ?>">
                                                    <img style="height: 185px;" class="img-fluid" src="<?php echo base_url('assets/script/').'/'.$other_sectors_sliders_imgg; ?>" alt="">
                                                </div>
                                            <?php  } }}else{
                                        $no_img=imageSetDimenssion('no_image.png', 170, 170, 1);

                                        ?>
                                        <div class="carousel-item active">
                                            <img style="max-width: 325px;max-height: 118px;" class="img-fluid" src="<?php echo $no_img; ?>" alt="">
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php
        $ceo_image = imageSetDimenssion(content_detail('eng_other_sectors_ceo_img_mkey_hdn', $page_id), 350, 546, 1);
        if(empty(content_detail('eng_other_sectors_ceo_img_mkey_hdn', $page_id))){
            $no_img=imageSetDimenssion('no_image.png', 350, 546, 1);
            $ceo_image=$no_img;
        }
        ?>
        <div class="modal fade" id="bodPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-right: 0px !important;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-xl-5 col-lg-5 col-md-6 col-sm-6 col-12 pl-0 pr-sm-3 pr-0">
                                    <div class="image text-center text-sm-left">
                                        <img class="img-fluid" src="<?php echo $ceo_image; ?>">
                                    </div>
                                </div>
                                <div class="col-xl-7 col-lg-7 col-md-6 col-sm-6 col-12">
                                    <div class="bodModelContent">

                                        <h2><?php echo content_detail($lang.'_other_sectors_ceo_name',$page_id); ?></h2>
                                        <h6><?php echo content_detail($lang.'_other_sectors_ceo_position',$page_id); ?></h6>
                                        <div class="moderDiscription">
                                            <?php echo content_detail($lang.'_other_sectors_ceo_dec',$page_id); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
