<?php

$home = getPageIdbyTemplate('home');

$events_id = getPageIdbyTemplate('events');

$events_listings = ListingContent($events_id);
$other_events_listings = OtherNewsListingContent($events_id);

$events_image = imageSetDimenssion(content_detail('eng_banner_event_mkey_hdn', $page_id), 1152, 361, 1);

if(content_detail('eng_banner_event_mkey_hdn', $page_id)){

    $img_div_class = 'pt-4';

}else{

    $img_div_class = 'pt-2';

}

$data = content_detail('event_date', $page_id);



$month = date('M', strtotime($data));

$monthArabic = month_arabic($month);

?>

<section class="MinHeight SpecialClass">

    <div class="assets_banner news_detail_sect">

        <div class="container">

            <div class="row">



                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 BlockStyling new_heading_style">

                    <div class="sector_text">

                        <h2><?php echo pageSubTitle($page_id,$lang);?></h2>

                        <?php

                        if ($data != '') {

                            $date = ($lang == 'eng' ? date('j M Y', strtotime($data)) : convertToArabic((date('j M Y', strtotime($data)))));

                            if ($lang != 'eng') {

                                $date = str_replace($month, $monthArabic, $date);

                            }

                            ?>

                            <div class="date">

                                <p class="mb-0"><?php echo $date;?></p>

                            </div>

                       <?php } ?>

                    </div>

                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                    <div class="sector_slider_container">

                        <div id="industrial_slider" class="carousel slide" data-ride="carousel" data-interval="2000">

                            <?php

                            $image_text = str_replace('','',content_detail('eng_slider_event', $page_id));

                            $explodedValues = explode(',',trim($image_text, ','));

                            if(count($explodedValues)>1 && content_detail('eng_slider_event', $page_id)){?>

                                <ol class="carousel-indicators">

                                    <?php



                                    $no_img=imageSetDimenssion('no_image.png', 255, 387, 1);



                                    $image_text = str_replace('','',content_detail('eng_slider_event', $page_id));

                                    $explodedValues = explode(',',trim($image_text, ','));



                                    $i = 1;

                                    foreach($explodedValues as $explodedValueSingle){

                                        if(strpos($explodedValueSingle, '___') !== false && strlen($explodedValueSingle) > 15){

                                            $explodedValueTitle = explode('___',$explodedValueSingle);



                                            if($explodedValueTitle[1] == ''){

                                                $explodedValueTitle[1] = $explodedValues[$i];

                                            }

                                            $titles[] = $explodedValueTitle[0];

                                            $images[] = $explodedValueTitle[1];



                                        } else{

                                            $titles[] = '';

                                            $images[] = $explodedValueSingle;

                                        }



                                        $i++;

                                    }



                                    $newTitles = array();

                                    foreach($titles as $singleTitle){

                                        if($singleTitle){

                                            $newTitles[] = $singleTitle;

                                        }

                                        else{

                                            $newTitles[] = '';

                                        }

                                    }

                                    foreach($images as $imageNew){

                                        if($imageNew !='' && $imageNew != '___'){

                                            $imageNews[] = $imageNew;

                                        }

                                    }



                                    $other_sectors_sliders =  explode(',',$image_text);



                                    if($image_text != 'eng_slider_event'){

                                        $slider_count=0;

                                        for($i=0;$i<=sizeof($imageNews);$i++) {

                                            if($i==0){

                                                $slider_active = 'active';

                                            }else{

                                                $slider_active = '';

                                            }



                                            //$other_sectors_sliders_img = imageSetDimenssion($other_sectors_sliders[$i], 325, 118, 1);

                                            $other_sectors_sliders_img=$imageNews[$i];



                                            if($imageNews[$i] && $imageNews[$i]!='eng_other_sectors_slider'){

                                                $slider_count++;

                                                if($slider_count==1){

                                                    $slider_active = 'active';

                                                }else{

                                                    $slider_active = '';

                                                }

                                                ?>

                                                <li data-target="#industrial_slider" data-slide-to="<?php echo $i; ; ?>" class="<?php echo $slider_active; ?>"></li>

                                            <?php  }

                                        }

                                    }?>

                                </ol>

                             <?php } ?>

                            <div class="carousel-inner">

                                <?php



                                $no_img=imageSetDimenssion('no_image.png', 255, 387, 1);



                                if($image_text != 'eng_slider_event'){

                                    $slider_count=0;

                                    for($i=0;$i<=sizeof($imageNews);$i++) {

                                        if($i==0){

                                            $slider_active = 'active';

                                        }else{

                                            $slider_active = '';

                                        }



                                        //$other_sectors_sliders_img = imageSetDimenssion($other_sectors_sliders[$i], 325, 118, 1);

                                        $other_sectors_sliders_img=$imageNews[$i];



                                        if($imageNews[$i] && $imageNews[$i]!='eng_slider_event'){

                                            $slider_count++;

                                            if($slider_count==1){

                                                $slider_active = 'active';

                                            }else{

                                                $slider_active = '';

                                            }

                                            ?>

                                            <div class="carousel-item <?php echo $slider_active; ?>">

                                                <a href="javascript:void(0)">

                                                    <img class="img-fluid" src="<?php echo base_url('assets/script/').'/'.$other_sectors_sliders_img; ?>" alt="" title="" />

                                                </a>

                                            </div>

                                        <?php  }

                                    }

                                }?>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <div class="container">

        <!-- <div class="row">

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <nav aria-label="breadcrumb">

                    <ol class="breadcrumb">

                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($home, 'eng')); ?>"><?php echo pageSubTitle($home,$lang) ?></a></li>

                        <li class="breadcrumb-item active" aria-current="page"><?php echo pageSubTitle($events_id,$lang);?></li>

                    </ol>

                </nav>

            </div>

        </div> -->

        <!-- <div class="row pt-4 pb-4">

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SectorDetailPage">

                <h2><?php echo pageSubTitle($page_id,$lang);?></h2>

            </div>

        </div> -->

        <div class="row <?php echo $img_div_class; ?> pb-4 pt-5">

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 BlockStyling">

                <?php

                    if(content_detail('eng_banner_event_mkey_hdn', $page_id)){

                ?>

                <div class="News_Detail_Image d-none" ><img class="img-fluid" src="<?php echo $events_image;?>" alt=""></div>

                <?php } ?>

                <?php echo html_entity_decode(content_detail($lang.'_desc',$page_id));?>

            </div>

        </div>

        <?php if(count($other_events_listings) > 0){ ?>

        <div class="row pb-4 pt-5">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                <div class="other_news_head profileNewstyle new_heading_style BlockStyling">

                    <h2><?php echo ($lang=='eng'?'Other News':'أخبار أخرى'); ?></h2>

                </div>

            </div>

        </div>

        <div class="row pt-4">

            <?php foreach($other_events_listings as $newList){

                $events_image = content_detail('eng_thumb_event_mkey_hdn', $newList->id);

                ?>

            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 mb-5">

                <a href="<?php echo lang_base_url().'page/event_details/'.$newList->id;?>" class="other_news_detail BlockStyling">

                    <?php if($events_image){ ?>

                    <div class="other_news_img">

                        <img class="img-fluid" src="<?php echo base_url().'assets/script/'.$events_image?>" alt="" title="" />

                    </div>

                    <?php } ?>

                    <div class="news_detail_head">

                        <h2><?php echo pageSubTitle($newList->id,$lang);?></h2>

                    </div>

                    <?php

                    $data = content_detail('event_date',$newList->id);



                    $month = date('M',strtotime($data));

                    $monthArabic = month_arabic($month);



                    if($data!=''){

                        $date = ($lang == 'eng' ? date('j M Y',strtotime($data)) : convertToArabic((date('j M Y',strtotime($data)))));

                        if($lang != 'eng'){

                            $date = str_replace($month,$monthArabic,$date);

                        }



                        ?>

                        <div class="date">

                            <p><?php echo $date; ?></p>

                        </div>

                    <?php } ?>

                </a>

            </div>

            <?php } ?>

        </div>

        <?php } ?>

    </div>

</section>