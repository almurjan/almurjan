<?php
$home = getPageIdbyTemplate('home');
$profile = getPageIdbyTemplate('profile');
$about_us = getPageIdbyTemplate('about_us');
$ceo_message = getPageIdbyTemplate('ceo_message');
$murjan_group = getPageIdbyTemplate('murjan_group');
$landmarks = getPageIdbyTemplate('landmarks');
$murjan_group_page_data = $page_id;
$murjan_group_listings = ListingContent($murjan_group_page_data);

?>
<section class=" MinHeight Site_Bg SpecialClass">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2><?php echo pageSubTitle($about_us, $lang); ?></h2>
            </div>
        </div>
        <div class="row pt-4">

            <div class="owl-carousel owl-theme aboutUsnavSlider nav nav-tabs AboutNav d-flex justify-content-around" id="navSlider">
                <div class="item nav-item-links">
                    <a class="nav-link " href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($profile, 'eng')); ?>" ><?php echo pageSubTitle($profile, $lang); ?></a>
                </div>
                <div class="item">
                    <a class="nav-link " href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($ceo_message, 'eng')); ?>"><?php echo pageSubTitle($ceo_message, $lang); ?></a>
                </div>
                <div class="item active">
                    <a class="nav-link active" href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($murjan_group, 'eng')); ?>"><?php echo pageSubTitle($murjan_group, $lang); ?></a>
                </div>
                <div class="item">
                    <a class="nav-link <?php echo ($page_id == '16' ? 'active' : ''); ?>" href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(16, 'eng')); ?>"><?php echo pageSubTitle(16, $lang); ?></a>
                </div>
                <div class="item">
                    <a class="nav-link <?php echo ($page_id == '1293' ? 'active' : ''); ?>" href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(1293, 'eng')); ?>"><?php echo pageSubTitle(1293, $lang); ?></a>
                </div>
                <div class="item">
                    <a class="nav-link <?php echo ($page_id == '1305' ? 'active' : ''); ?>" href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(1305, 'eng')); ?>"><?php echo pageSubTitle(1305, $lang); ?></a>
                </div>
                <!--<div class="item">
                    <a class="nav-link <?php /*echo ($page_id == '884' ? 'active' : ''); */?>" href="<?php /*echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(884, 'eng')); */?>"><?php /*echo pageSubTitle(884, $lang); */?></a>
                </div>-->
                <!--<div class="item">
                    <a class="nav-link" href="<?php /*echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($landmarks, 'eng')); */?>"><?php /*echo pageSubTitle($landmarks, $lang); */?></a>
                </div>-->
            </div>
        </div>
        <div class="row pt-5 pb-5">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 innerheading SiteHeadings borderbotH">
                <h2><?php echo pageSubTitle($page_id, $lang); ?></h2>
            </div>
        </div>
        <?php
        foreach($murjan_group_listings as $murjan_group_listing){
            //$dob_image = imageSetDimenssion(content_detail('eng_group_image_mkey_hdn', $murjan_group_listing->id), 200, 200, 1);
            $dob_image = base_url('assets/script').'/'.content_detail('eng_group_image_mkey_hdn', $murjan_group_listing->id);
            if(content_detail('eng_group_image_mkey_hdn', $murjan_group_listing->id)){
                $dob_image=$dob_image;
            }else{
                $no_img=imageSetDimenssion('no_image.png', 100, 100, 1);
                $dob_image=$no_img;
            }
            ?>
        <div class="row align-items-center pb-4">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                <div class="almurjanGroupImg">
                    <img class="img-fluid" src="<?php echo $dob_image; ?>">
                </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-6 col-12 BlockStyling">
                <div class="almurjanGroupContent">
                    <h6><?php echo pageSubTitle($murjan_group_listing->id, $lang); ?></h6> <?php echo content_detail($lang.'_group_dec', $murjan_group_listing->id) ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>