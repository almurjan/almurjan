<?php
$home_id = getPageIdbyTemplate('home');
$csr_id = getPageIdbyTemplate('csr');
$csr_listings = ListingContent($csr_id);

?>
<section class="MinHeight SpecialClass csr_custom_class">
    <div class="assets_banner">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 BlockStyling new_heading_style">
                    <div class="assets_content">
                        <h2><?php echo content_detail($lang.'_csr_title',$csr_id);?></h2>
                        <?php echo content_detail($lang.'_csr_desc',$csr_id);?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $j=1; $i=1; foreach($csr_listings as $csr_listing){
            $csr_img = content_detail('eng_csr_thumb_image_mkey_hdn',$csr_listing->id);
            $csr_img_title = pageSubTitle($csr_listing->id,$lang);
            $csr_desc = content_detail($lang.'_csr_desc',$csr_listing->id);
            if($i==2){
                $csr_bg_class='';
                $csr_margin_class='';
                $third_img_class='mb_50_minus';
                $first_img_class='';
            }elseif($i==3){
                $csr_bg_class='row_bg';
                $csr_margin_class='';
                $third_img_class='mb_50_minus';
                $first_img_class='';
            }elseif($i==4){
                $first_img_class='';
                $csr_bg_class='';
                $third_img_class='';
                $csr_margin_class='custom_margin';
            }else{
                $csr_bg_class='';
                $csr_margin_class='';
                $first_img_class='';
                $third_img_class='';
            }
            if($j==1){
                $first_img_class='first_custom';
                $third_img_class='';
            }
            if($csr_img){
                $col_class = 'col-xl-6 col-lg-6';
            }else{
                $col_class = 'col-xl-12 col-lg-12';
            }
            if ($i % 2 != 0){?>
                <div class="BlockStyling <?php echo $csr_margin_class.' '.$csr_bg_class; ?>">
                    <div class="container">
                        <div class="row">
                            <?php if($csr_img){?>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 pr-3 pr-lg-5">
                                        <div class="image_content_box <?php echo $first_img_class.' '.$third_img_class;?>">
                                                <div class="img_box">
                                                    <img src="<?php echo base_url('assets/script').'/'.$csr_img;?>" CLASS="img-fluid">
                                                </div>
                                            <div class="head_box">
                                                <h3><?php echo $csr_img_title;?></h3>
                                            </div>
                                        </div>
                                </div>
                            <?php } ?>
                            <div class="<?php echo $col_class;?> col-md-12 col-sm-12 col-12 pl-3 pl-lg-5">
                                <div class="csr_content_box">
                                    <?php echo $csr_desc;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }else{ ?>
                <div class="BlockStyling <?php echo $csr_margin_class.' '.$csr_bg_class; ?>">
                    <div class="container">
                        <div class="row">
                            <div class="<?php echo $col_class;?> col-md-12 col-sm-12 col-12 pr-3 pr-lg-5 order-1 order-lg-0">
                                <div class="csr_content_box">
                                    <?php echo $csr_desc;?>
                                </div>
                            </div>
                            <?php if($csr_img){?>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 pl-3 pl-lg-5">
                                    <div class="image_content_box <?php echo $third_img_class;?>">
                                        <div class="img_box">
                                            <img src="<?php echo base_url('assets/script').'/'.$csr_img;?>" class="img-fluid">
                                        </div>
                                        <div class="head_box">
                                            <h3><?php echo $csr_img_title;?></h3>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php
            if($i==4){
                $i=0;
            }
            $i++;
        $j++;
        } ?>
    <!--<div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h2><?php /*echo pageSubTitle($csr_id,$lang); */?></h2>
            </div>
        </div>
        <div class="row pt-5 pb-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 BlockStyling">
                <?php /*echo content_detail($lang.'_csr_desc',$csr_id); */?>
            </div>
        </div>
    </div>-->
</section>
