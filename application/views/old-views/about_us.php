<?php
$home = getPageIdbyTemplate('home');
$profile = getPageIdbyTemplate('profile');
$about_us = getPageIdbyTemplate('about_us');
$ceo_message = getPageIdbyTemplate('ceo_message');
$murjan_group = getPageIdbyTemplate('murjan_group');
$landmarks = getPageIdbyTemplate('landmarks');
$bod = getPageIdbyTemplate('bod');
$bod_listings = ListingContent($bod);
$profile_listings = ListingContent($profile,'profile','0');
$profile_companies_listings = ListingContent($profile,'profile',1);
/*$value_image = imageSetDimenssion(content_detail('eng_value_image_mkey_hdn', $profile), 540, 417, 1);*/
$value_image = base_url('assets/script/').'/'.content_detail('eng_profile_image_mkey_hdn', $profile);
$Pagetitle = content_detail($lang.'_page_title_profile', $profile);
?>
<style>
    .profileContainerBg {
        background: url(<?php echo $value_image;?>) no-repeat;
        background-size: cover;
    }
    footer{
        margin-top: 0px !important;
    }
</style>
<section class=" MinHeight Site_Bg site_bgNone SpecialClass">
    <div class="container">
        <!--<div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 SiteHeadings">
                <h2><?php //echo pageSubTitle($about_us, $lang); ?></h2>
            </div>
        </div>-->


        <section class="about_custom_nav mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="newdesign_border">
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($profile, 'eng')); ?>" class="active"> <?php echo $ar_space.pageSubTitle($profile, $lang).$ar_space; ?></a></li>
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle($ceo_message, 'eng')); ?>"> <?php echo $ar_space.pageSubTitle($ceo_message, $lang).$ar_space; ?></a></li>
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(16, 'eng')); ?>" <?php echo ($page_id == '16' ? 'active' : ''); ?>> <?php echo $ar_space.pageSubTitle(16, $lang).$ar_space; ?></a></li>
                            <li><a href="<?php echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(1293, 'eng')); ?>?id=1293" class="<?php echo ($page_id == '1293' ? 'active' : ''); ?>"> <?php echo $ar_space.pageSubTitle(1293, $lang).$ar_space; ?></a></li>
                            <!--<li><a href="<?php /*echo lang_base_url() . 'page/' . str_replace(' ', '_', pageTitle(1305, 'eng')); */?>" <?php /*echo ($page_id == '1305' ? 'active' : ''); */?>> <?php /*echo $ar_space.pageSubTitle(1305, $lang).$ar_space; */?></a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div class="row BlockStyling new_heading_style custompadding_newstyle">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h2><?php// echo pageSubTitle($profile,$lang);?></h2>
                <?php echo $Pagetitle; ?>
            </div>
        </div>
    </div>
    <?php
    if(content_detail('eng_value_image_mkey_hdn', $profile)){
        //$profile_image = imageSetDimenssion(content_detail('eng_value_image_mkey_hdn', $profile), 214, 330, 1);
        $profile_image = base_url('assets/script/').'/'.content_detail('eng_value_image_mkey_hdn', $profile);
        $col_class = 'col-xl-6 col-lg-6 col-md-6';
    }else{
        $col_class = 'col-xl-12 col-lg-12 col-md-12';
    }
    ?>
    <section class="overview_desc">
        <div class="container">
            <div class="row">
                <div class="<?php echo $col_class; ?> col-sm-12 col-12 order-1 order-md-0">
                    <div class="overview_text BlockStyling">
                        <?php echo content_detail($lang.'_desc_pro',$profile);?>
                    </div>
                </div>
                <?php if(content_detail('eng_value_image_mkey_hdn', $profile)){?>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="desc_image">
                            <img class="img-fluid" src="<?php echo $profile_image; ?>">
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>

    <section class="new_logo_section">
        <div class="container">
            <div class="row">
                <?php
                foreach($profile_companies_listings as $profile_companies_listing){
                    $companies_description = content_detail($lang.'_company_desc',$profile_companies_listing->id);
                    if(content_detail('eng_company_logo_mkey_hdn',$profile_companies_listing->id)){
                        $companies_image = base_url('assets/script').'/'.content_detail('eng_company_logo_mkey_hdn',$profile_companies_listing->id);
                    }
                    ?>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 mt-5 mt-xl-4">
                        <div class="logo_box_container">
                            <?php if(content_detail('eng_company_logo_mkey_hdn',$profile_companies_listing->id)){ ?>
                                <div class="logo_box">
                                    <img class="img-fluid" src="<?php echo $companies_image; ?>">
                                </div>
                            <?php } ?>
                            <div class="logo_desc BlockStyling">
                                <?php echo $companies_description; ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <?php
    if(content_detail('eng_mission_image_mkey_hdn',$profile)){
        $mission_section_image = base_url('assets/script/').'/'.content_detail('eng_mission_image_mkey_hdn',$profile);
        $mission_col = 'col-xl-6';
    }else{
        $mission_col = 'col-xl-12';
    }
    $vision_title = content_detail($lang.'_vission_title',$profile);
    $vision_description = content_detail($lang.'_vission_value',$profile);
    $mission_title = content_detail($lang.'_mission_title',$profile);
    $mission_description = content_detail($lang.'_mission_desc_value',$profile);
    $strategy_title = content_detail($lang.'_strategy_title',$profile);
    $strategy_description = content_detail($lang.'_strategy_value',$profile);
    ?>
    <section class="mission_section">
        <div class="container">
            <div class="row">
                <?php if(content_detail('eng_mission_image_mkey_hdn',$profile)){ ?>
                    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 order-1 order-xl-0">
                        <div class="mission_img">
                            <img class="img-fluid" src="<?php echo $mission_section_image; ?>">
                        </div>
                    </div>
                <?php } ?>
                <div class="<?php echo $mission_col; ?> col-lg-12 col-md-12 col-sm-12 col-12 order-0 order-xl-1">
                    <div class="mission_desc">
                        <div class="mission_content_desc BlockStyling new_heading_style profileNewstyle">
                            <h2><?php echo $vision_title; ?></h2>
                            <?php echo $vision_description; ?>
                        </div>
                        <div class="mission_content_desc BlockStyling new_heading_style profileNewstyle">
                            <h2><?php echo $mission_title; ?></h2>
                            <?php echo $mission_description; ?>
                        </div>
                        <div class="mission_content_desc BlockStyling new_heading_style profileNewstyle">
                            <h2><?php echo $strategy_title; ?></h2>
                            <?php echo $strategy_description; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our_values">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="values_head  BlockStyling new_heading_style profileNewstyle views_head_padding">
                        <h2><?php echo content_detail($lang.'_value_title',$profile);?></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php

                foreach($profile_listings as $profile_listing){
                    if(content_detail('eng_value_image_mkey_hdn', $profile_listing->id)){
                        //$value_thumb = imageSetDimenssion(content_detail('eng_value_image_mkey_hdn', $profile_listing->id), 40, 40, 1);
                        $value_icon = base_url('assets/script/').'/'.content_detail('eng_value_image_mkey_hdn', $profile_listing->id);
                        $col_class_value = 'col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10';
                    }else{
                        //$no_img=imageSetDimenssion('no_image.png', 40, 40, 1);
                        //$col_class_value = 'col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12';
                        $col_class_value = 'col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10';
                    }
                    ?>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 values_icon_padd">
                        <div class="row">
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pr-0">
                                <?php if(content_detail('eng_value_image_mkey_hdn', $profile_listing->id)){ ?>
                                    <div class="values_icon">
                                        <img class="img-fluid" src="<?php echo $value_icon; ?>">
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="<?php echo $col_class_value; ?>">
                                <div class="values_desc BlockStyling">
                                    <h2><?php echo pageSubTitle($profile_listing->id,$lang);?>:</h2>
                                    <?php echo content_detail($lang.'_value_desc',$profile_listing->id); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
</section>







