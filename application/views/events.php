<?php
$home_id = getPageIdbyTemplate('home');
$events_id = getPageIdbyTemplate('events');
$events_listings = ListingContentCustomOrdering($events_id, 'id', 'desc');
$news_ids = [];
foreach ($events_listings as $events_listing) {
    $date = content_detail('event_date', $events_listing->id);
    $news_ids[$events_listing->id] = $date ? strtotime($date) : strtotime($events_listing->content_created_at);
}
arsort($news_ids);
?>
    <div class="inner-banner-main-box">
        <section class="about-banner"
                 style="background-image: url('<?php echo base_url() . 'assets/script/' . content_detail('eng_news_banner_image_mkey_hdn', $page_id); ?>')">
            <div class="inner-banner-inner-box">
                <div class="container">
                    <div class="row">
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="white-banner-content has-description">
                                <h1><?php echo pageSubTitle($events_id, $lang); ?></h1>
                                <?php if (content_detail($lang . '_news_desc', $events_id)) { ?>
                                    <p><?php echo strip_tags(content_detail($lang . '_news_desc', $events_id)); ?>
                                    </p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="latest-articles-search">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="article-heading">
                        <h2><?php echo translate($lang, 'LATEST ARTICLES', 'أحدث المقالات') ?></h2>
                    </div>
                </div>
            </div>
            <div class="row top-search">
                <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                    <div class="al-murjan-articles">
                        <div class="news-slider">
                            <div class="owl-carousel-news owl-carousel owl-theme">
                                <?php
                                $count = 1;
                                foreach ($news_ids as $key => $val) {
                                    if ($count > 4) break;
                                    $events_image = content_detail('eng_thumb_event_mkey_hdn', $key);
                                    $in = content_detail($lang . '_desc', $key);
                                    $description = strlen($in) > 50 ? substr($in, 0, 100) . "..." : $in;
                                    ?>

                                    <!--<div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">-->

                                    <div class="item">
                                        <a href="javascript:void(0)" data-bs-toggle="modal"
                                           data-bs-target="#staticBackdrop-latest_news-<?php echo $key; ?>">
                                            <div class="article-name">
                                                <p><?php
                                                    if ($lang == 'eng') {
                                                        echo pageSubTitle($key, $lang);
                                                    } else {
                                                        echo convertToArabic(pageSubTitle($key, $lang));
                                                    }

                                                    ?></p>
                                            </div>
                                        </a>
                                    </div>


                                    <!-- </div>-->


                                <?php $count++; } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                    <form>
                        <div class="al-murjan-search">
                            <div class="search-box">
                                <button class="search-btn-icon" type="submit"><i class="fa fa-search"></i></button>
                                <input type="text" class="form-control" name="search" placeholder="Search..."
                                       value="<?php echo(isset($_REQUEST['search']) ? $_REQUEST['search'] : ''); ?>">
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <div class="row pt-5">
                <?php
                $i = 0;
                foreach ($news_ids as $key => $val) {
                    $show_news = true;
                    if (isset($_REQUEST['search']) && $_REQUEST['search'] != '') {
                        if (stripos(pageSubTitle($key, $lang), $_REQUEST['search']) === false) {
                            $show_news = false;
                        }
                    }

                    if ($show_news) {
                        /*$events_image = imageSetDimenssion(content_detail('eng_thumb_event_mkey_hdn', $key), 111, 111, 1);*/
                        $events_image = content_detail('eng_thumb_event_mkey_hdn', $key);
                        $in = content_detail($lang . '_desc', $key);
                        $description = strlen($in) > 50 ? substr($in, 0, 100) . "..." : $in;
                        ?>
                        <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                            <a href="javascript:void(0)" data-bs-toggle="modal"
                               data-bs-target="#staticBackdrop-news-<?php echo $key; ?>">
                                <div class="news-thumb">
                                    <?php if ($events_image) { ?>
                                        <img src="<?php echo base_url() . 'assets/script/' . $events_image ?>"
                                             class="img-fluid" alt="">
                                    <?php } ?>
                                    <?php
                                    $data = content_detail('event_date', $key);
                                    $month = date('M', strtotime($data));
                                    $monthArabic = month_arabic($month);
                                    if ($data != '') {
                                        $date = ($lang == 'eng' ? date('j M Y', strtotime($data)) : convertToArabic((date('j M Y', strtotime($data)))));
                                        if ($lang != 'eng') {
                                            $date = str_replace($month, $monthArabic, $date);
                                        }
                                        ?>
                                        <h3><?php echo $date; ?></h3>
                                    <?php } ?>
                                    <h2><?php
                                        if ($lang == 'eng') {
                                            echo pageSubTitle($key, $lang);
                                        } else {
                                            echo convertToArabic(pageSubTitle($key, $lang));
                                        }

                                        ?></h2>
                                </div>
                            </a>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="staticBackdrop-news-<?php echo $key; ?>"
                             data-bs-keyboard="false" tabindex="-1"
                             aria-labelledby="staticBackdropLabel-news-<?php echo $key; ?>"
                             aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-body position-relative">
                                        <button type="button" class="btn-close close-modal" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        <div class="news-detail-modal">
                                            <h2><?php
                                                if ($lang == 'eng') {
                                                    echo pageSubTitle($key, $lang);
                                                } else {
                                                    echo convertToArabic(pageSubTitle($key, $lang));
                                                }

                                                ?></h2>
                                            <hr>

                                            <?php
                                            $slider_images = content_detail('eng_slider_event', $key);
                                            if ($slider_images) {
                                                ?>
                                                <div class="events-popup-slider owl-carousel owl-theme">
                                                    <?php
                                                    $slider_images_list = explode(',', $slider_images);
                                                    foreach ($slider_images_list as $slider_image) {
                                                        if ($slider_image == 'eng_slider_event') continue;
                                                        ?>
                                                        <div class="item">
                                                            <div class="px-1 px-md-5">
                                                                <img src="<?php echo base_url() . 'assets/script/' . $slider_image ?>"
                                                                     class="img-fluid" alt="">
                                                            </div>
                                                        </div>
                                                    <?php }
                                                    ?>
                                                </div>
                                            <?php } ?>

                                            <div class="news-detail-modal-desc">
                                                <p><?php echo html_entity_decode(content_detail($lang . '_desc', $key)); ?></p>
                                            </div>
                                        </div>
                                        <!--<div class="sector-modal-video position-relative">
                                            asdsadsa
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php $i++;
                    }
                } ?>
                <?php if ($i == 0) { ?>
                    <div class="col-12">
                        <strong><?php echo translate($lang, 'No records matching the search found!', 'لا توجد سجلات تطابق البحث وجدت!'); ?></strong>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>


<?php
$count = 1;
foreach ($news_ids as $key => $val) {
    if ($count > 4) break;
    $events_image = content_detail('eng_thumb_event_mkey_hdn', $key);
    $in = content_detail($lang . '_desc', $key);
    $description = strlen($in) > 50 ? substr($in, 0, 100) . "..." : $in;
    ?>
    <div class="modal fade" id="staticBackdrop-latest_news-<?php echo $key; ?>" data-bs-keyboard="false"
         tabindex="-1" aria-labelledby="staticBackdropLabel-latest_news-<?php echo $key; ?>"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body position-relative">
                    <button type="button" class="btn-close close-modal" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    <div class="news-detail-modal">
                        <h2><?php
                            if ($lang == 'eng') {
                                echo pageSubTitle($key, $lang);
                            } else {
                                echo convertToArabic(pageSubTitle($key, $lang));
                            }

                            ?></h2>
                        <hr>

                        <?php
                        $slider_images = content_detail('eng_slider_event', $key);
                        if ($slider_images) {
                            ?>
                            <div class="events-popup-slider owl-carousel owl-theme mt-4">
                                <?php
                                $slider_images_list = explode(',', $slider_images);
                                foreach ($slider_images_list as $slider_image) {
                                    if ($slider_image == 'eng_slider_event') continue;
                                    ?>
                                    <div class="item">
                                        <div class="px-1 px-md-5">
                                            <img src="<?php echo base_url() . 'assets/script/' . $slider_image ?>"
                                                 class="img-fluid" alt="">
                                        </div>
                                    </div>
                                <?php }
                                ?>
                            </div>
                        <?php } ?>

                        <div class="news-detail-modal-desc">
                            <p><?php echo html_entity_decode(content_detail($lang . '_desc', $key)); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $count++; } ?>