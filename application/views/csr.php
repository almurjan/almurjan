<?php
$home_id = getPageIdbyTemplate('home');
$csr_id = getPageIdbyTemplate('csr');
$csr_listings = ListingContent($csr_id);

?>
<div class="inner-banner-main-box">
    <section class="about-banner" style="background-image: url('<?php echo base_url() . 'assets/script/' . content_detail('eng_csr_banner_image_mkey_hdn', $page_id); ?>')">
        <div class="inner-banner-inner-box">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="white-banner-content has-blue-color csr-banner-heading">
                            <h1><?php echo pageSubTitle($page_id, $lang); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section class="csr-wrapper">
    <?php $j=1; $i=1; foreach($csr_listings as $csr_listing){
        $csr_img = content_detail('eng_csr_thumb_image_mkey_hdn',$csr_listing->id);
        $csr_img_title = pageSubTitle($csr_listing->id,$lang);
        $csr_desc = content_detail($lang.'_csr_desc',$csr_listing->id);
        if ($i % 2 != 0){?>
        <div class="container">
            <div class="row csr-block">
                <?php if($csr_img){?>
                <div class="col-xxl-4 col-xl-4 col-lg-5 col-md-6 col-sm-12 col-12">
                    <div class="image-heading has-second-color">
                        <h2><?php echo $csr_img_title;?></h2>
                        <img src="<?php echo base_url('assets/script').'/'.$csr_img;?>" class="img-fluid">
                    </div>
                </div>
                <?php } ?>
                <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12">
                    <div class="csr-description des-spacing-left mt-4">
                        <?php echo $csr_desc;?>
                    </div>
                </div>
            </div>
            <?php }else{ ?>
            <div class="row csr-block">
                <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-6 order-sm-down-2 col-sm-12 col-12">
                    <div class="csr-description des-spacing-right mt-4">
                        <?php echo $csr_desc;?>
                    </div>
                </div>
                <?php if($csr_img){?>
                <div class="col-xxl-4 col-xl-4 col-lg-5 col-md-6 order-sm-down-1 col-sm-12 col-12">
                    <div class="image-heading has-second-color">
                        <h2><?php echo $csr_img_title;?></h2>
                        <img src="<?php echo base_url('assets/script').'/'.$csr_img;?>" class="img-fluid">
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php
    if($i==4){
        $i=0;
    }
    $i++;
    $j++;
    } ?>
</section>