<style>
    .banner-quote, .white-banner-content{
        margin-right: auto;
        margin-left: 0;
    }
</style>
<?php $history_listings = ListingContent($page_id); ?>
<div class="inner-banner-main-box">
    <section class="about-banner" style="background-image: url('<?php echo base_url() . 'assets/script/' . content_detail('eng_history_banner_image_mkey_hdn', $page_id); ?>')">
        <div class="inner-banner-inner-box">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-8 col-sm-8 col-12">
                        <div class="white-banner-content banner-quote has-second-color">
                            <div class="quote-bg">
                                <?php echo content_detail($lang . '_history_banner_desc', $page_id); ?>
                                <p><span><?php echo content_detail($lang . '_history_banner_desc_sub_heading', $page_id); ?></span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-8 col-sm-8 col-12">
                        <div class="white-banner-content has-blue-color yellow-our">
                            <h1><?php echo pageSubTitle($page_id, $lang); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section class="csr-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <!-- Timeline start -->
                <div class="timeline">
                    <div class="">
                        <?php
                        $i = 0;
                        foreach ($history_listings as $history_listing) { ?>
                            <div class="time-line-content-one-box">
                                <div class="timeline-arrow-content">
                                    <h2><?php echo pageTitle($history_listing->id, $lang); ?></h2>
                                </div>
                                <ul class="timeline-items">
                                    <li class="is-hidden timeline-item <?php echo ($i%2 == 0 ? 'inverted' : ''); ?>">

                                        <?php if ($i%2 == 0) { ?>
                                            <div class="div-on-left">
                                                <div class="timeline-dot fb-bg"></div>
                                                <div class="timeline-picture">
                                                    <img class="img-fluid" src="<?php echo base_url() . 'assets/script/' . content_detail('eng_history_listing_thumb_mkey_hdn', $history_listing->id); ?>" alt="<?php echo pageTitle($history_listing->id, $lang); ?>">
                                                </div>
                                            </div>
                                            <div class="div-on-right">
                                                <div class="timeline-time">
                                                    <div class="">
                                                        <h2><?php echo content_detail($lang . '_history_listing_title', $history_listing->id); ?></h2>
                                                    </div>
                                                    <?php echo content_detail($lang . '_history_listing_desc', $history_listing->id); ?>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="div-on-left">
                                                <div class="timeline-time">
                                                    <div class="">
                                                        <h2><?php echo content_detail($lang . '_history_listing_title', $history_listing->id); ?></h2>
                                                    </div>
                                                    <?php echo content_detail($lang . '_history_listing_desc', $history_listing->id); ?>
                                                </div>
                                            </div>
                                            <div class="div-on-right">
                                                <div class="timeline-dot fb-bg"></div>
                                                <div class="timeline-picture">
                                                    <img class="img-fluid" src="<?php echo base_url() . 'assets/script/' . content_detail('eng_history_listing_thumb_mkey_hdn', $history_listing->id); ?>" alt="<?php echo pageTitle($history_listing->id, $lang); ?>">
                                                </div>
                                            </div>
                                        <?php } ?>

                                    </li>
                                </ul>
                            </div>
                        <?php $i++; } ?>
                    </div>
                </div>
                <!-- Timeline end -->
            </div>
        </div>

        <!-- Timeline 767px start -->

        <div class="row d-none-from-768">
            <?php foreach ($history_listings as $history_listing) { ?>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="mobile-timeline">
                    <div class="mobile-timeline-picture">
                        <img class="img-fluid w-100" src="<?php echo base_url() . 'assets/script/' . content_detail('eng_history_listing_thumb_mkey_hdn', $history_listing->id); ?>" alt="<?php echo pageTitle($history_listing->id, $lang); ?>">
                    </div>
                    <div class="mobile-timeline-head">
                        <h2><?php echo pageTitle($history_listing->id, $lang); ?></h2>
                    </div>
                    <div class="timeline-time">
                        <h2><?php echo content_detail($lang . '_history_listing_title', $history_listing->id); ?></h2>
                        <?php echo content_detail($lang . '_history_listing_desc', $history_listing->id); ?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <!-- Timeline 767px end -->
        <div class="row pt-5">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="family-values">
                    <h2><?php echo content_detail($lang . '_history_family_values_title', $page_id); ?></h2>
                    <?php echo content_detail($lang . '_history_family_values_desc', $page_id); ?>
                </div>
            </div>
        </div>
    </div>
</section>