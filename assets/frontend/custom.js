$(document).ready(function() {
    $("#subcribs_form").validate();
    $("#subcribs_form").submit(function (event) {
        if ($(this).valid()) {
            $('.page_loader').fadeIn('slow');
            $form = $(this);
            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: $form.serialize(),
                dataType: "json",
                cache: false,
                success: function (response) {
                    console.log(response);
                    console.log(response.status);
                    $('.page_loader').fadeOut('slow');
                    if (response.status === 1) {
                        $form[0].reset();
                        showSuccess(response.message);
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }else{
                        showError(response.message);
                    }
                }
            });
        }

    });
});
function showSuccess(message_text) {
    $.alert({
        title: "Success",
        content: message_text,
        type: 'green',
        backgroundDismiss: true, // this will just close the modal,
        rtl: (lang == 'arb'),
        theme: 'material' // 'material', 'bootstrap', 'supervan'
    });

}

function showError(message_text) {
    $.alert({
        title: "Error",
        content: message_text,
        type: 'red',
        backgroundDismiss: true, // this will just close the modal,
        rtl: (lang == 'arb'),
        theme: 'material' // 'material', 'bootstrap', 'supervan'
    });

}
$(document).ready(function(){

    //contact form
    $(".contact_us_form").validate();
    $(".contact_us_form").submit(function (event) {

        //alert('here');
        var email = $('#u_email').val();
        if(email !== '' && !validateEmail(email)) {
            //alert();
            $('.page_loader').fadeOut('slow');
            showError(lang == 'eng' ? 'Please provide a valid email address!' : 'الرجاء ادخال بريد الكتروني صحيح');
            return false;
        }
        if ($(this).valid()) {
            $('.page_loader').fadeIn('slow');
            //$('.page_loader').fadeIn('slow');
            $form = $(this);
            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: $form.serialize(),
                dataType: "json",
                cache: false,
                success: function (response) {
                    $('.page_loader').fadeOut('slow');
                    //alert(response.status);
                    //$(".page_loader").fadeOut("slow");
                    if (response.status === 1) {
                        $form[0].reset();
                        showSuccess(response.message);
                        setTimeout(function () {
                            window.location.reload();
                        }, 30000);
                        //document.forms['contactForm'].reset();
                    }
                    else if (response.status === 2)
                    {
                        $(".loader").fadeOut("slow");
                        $("#captcha_message").css( "display", "block" );
                        $("#captcha_message").html(response.error_captcha);
                        setTimeout(function(){
                            $( "#captcha_message" ).fadeOut("slow");
                        }, 40000);
                    }
                    else{
                        showError(response.message);
                    }
                }
            });
        }

    });

    //self_application
    //contact form
    $("#self_application_form").validate({
        errorPlacement: function(error, element) {
            if(element.attr("name") == 'filename') {
                error.insertAfter('.file_div');
            }else
                error.insertAfter(element);
        },
    });
    $('#c_residence').change(function () {
        $(".page_loader").fadeIn("slow");
        var country = $('#c_residence').val();
        $.ajax({
            type: "POST",
            url: lang_base_url+'page/getFrontendCities',
            data: {'country': country},
            dataType: "json",
            cache: false,
            success: function (response) {
                //alert('here');
                //alert(response.status);
                $(".page_loader").fadeOut("slow");
                if (response.status === 1) {
                    $("#ct_residence").html(response.html);
                    //document.forms['contactForm'].reset();
                }
            }
        });

    });
	$("#med_form").validate();
    $("#job_application_form").validate(
        {
            errorPlacement: function(error, element) {
                if(element.attr("name") == 'filename') {
                    error.insertAfter('.file_div');
                }else
                    error.insertAfter(element);
            },
        }
    );
    $("#job_application_form").submit(function (event) {

        //alert('here');
        var email = $('#u_email').val();
        if(email !== '' && !validateEmail(email)) {
            //alert();
            $('.page_loader').fadeOut('slow');
            showError(lang == 'eng' ? 'Please provide a valid email address!' : 'الرجاء ادخال بريد الكتروني صحيح');
            return false;
        }
        if ($(this).valid()) {
            $('.page_loader').fadeIn('slow');
            //$('.page_loader').fadeIn('slow');
            $form = $(this);
            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: new FormData($form[0]),
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.page_loader').fadeOut('slow');
                    //alert(response.status);
                    if (response.status === 1) {
                        $form[0].reset();
                        showSuccess(response.message);
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                        //document.forms['contactForm'].reset();
                    }
                    else if (response.status === 2)
                    {
                        $(".loader").fadeOut("slow");
                        $("#captcha_message").css( "display", "block" );
                        $("#captcha_message").html(response.error_captcha);
                        setTimeout(function(){
                            $( "#captcha_message" ).fadeOut("slow");
                        }, 4000);
                    }
                    else{
                        showError(response.message);
                    }
                }
            });
        }

    });
	
	$("#med_form").submit(function (event) {
		var email = $('#email').val();
        if(email !== '' && !validateEmail(email)) {
            //alert();
            $('.page_loader').fadeOut('slow');
            showError(lang == 'eng' ? 'Please provide a valid email address!' : 'الرجاء ادخال بريد الكتروني صحيح');
            return false;
        }
        if ($(this).valid()) {
            $('.page_loader').fadeIn('slow');
            //$('.page_loader').fadeIn('slow');
            $form = $(this);
            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: new FormData($form[0]),
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.page_loader').fadeOut('slow');
                    //alert(response.status);
                    if (response.status === 1) {
                        $form[0].reset();
                        showSuccess(response.message);
                        setTimeout(function () {
                            //window.location.reload();
                        }, 3000);
                        //document.forms['contactForm'].reset();
                    }
                    else if (response.status === 2)
                    {
                        $(".loader").fadeOut("slow");
                        $("#captcha_message").css( "display", "block" );
                        $("#captcha_message").html(response.error_captcha);
                        setTimeout(function(){
                            $( "#captcha_message" ).fadeOut("slow");
                        }, 4000);
                    }
                    else{
                        showError(response.message);
                    }
                }
            });
        }

    });
	
    $("#self_application_form").submit(function (event) {

        var email = $('#u_email').val();
        if(email !== '' && !validateEmail(email)) {
            //alert();
            $('.page_loader').fadeOut('slow');
            showError(lang == 'eng' ? 'Please provide a valid email address!' : 'الرجاء ادخال بريد الكتروني صحيح');
            return false;
        }
        if ($(this).valid()) {
            $('.page_loader').fadeIn('slow');
            //$('.page_loader').fadeIn('slow');
            $form = $(this);
            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: new FormData($form[0]),
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.page_loader').fadeOut('slow');
                    //alert(response.status);
                    if (response.status === 1) {
                        $form[0].reset();
                        showSuccess(response.message);
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                        //document.forms['contactForm'].reset();
                    }
                    else if (response.status === 2)
                    {
                        $(".loader").fadeOut("slow");
                        $("#captcha_message").css( "display", "block" );
                        $("#captcha_message").html(response.error_captcha);
                        setTimeout(function(){
                            $( "#captcha_message" ).fadeOut("slow");
                        }, 4000);
                    }
                    else{
                        showError(response.message);
                    }
                }
            });
        }

    });

    $(".numberOnly").keypress(function(e){
        var regx = /^[0-9]*$/;
        var string = event.key;
		
		if (string == undefined) return '';
		var str = $.trim(string.toString());
		if (str == "") return "";
		str = str.replace(/۰/g, '0');
		str = str.replace(/۱/g, '1');
		str = str.replace(/۲/g, '2');
		str = str.replace(/۳/g, '3');
		str = str.replace(/۴/g, '4');
		str = str.replace(/۵/g, '5');
		str = str.replace(/۶/g, '6');
		str = str.replace(/۷/g, '7');
		str = str.replace(/۸/g, '8');
		str = str.replace(/۹/g, '9');
		
		string = str;
		
		//alert(string);
		
        if (!regx.test(string))
        {
            return false;
        }
    });
    $(".inputTextOnly").keypress(function(event) {
        var character = String.fromCharCode(event.keyCode);
        return isValid(character,event);
    });


});
function isValid(str,e) {
    if(!$.isNumeric(str)){
        return !/[~`!@#$%\^&*()+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str);
    }
    else{
        e.preventDefault();
    }
}
function showSuccess(message_text) {
    $.alert({
        title: "",
        content: message_text,
        type: 'green',
        backgroundDismiss: true, // this will just close the modal,
        rtl: (lang == 'arb'),
        theme: 'material' // 'material', 'bootstrap', 'supervan'
    });

}

function showError(message_text) {
    $.alert({
        title: "Error",
        content: message_text,
        type: 'red',
        backgroundDismiss: true, // this will just close the modal,
        rtl: (lang == 'arb'),
        theme: 'material' // 'material', 'bootstrap', 'supervan'
    });

}
function tab_ative(val){
	//window.stop();
    $(".filter-button").not(this).removeClass('active_cat');
    $(".filtersdata").slice(0, 12).show();
    $('.filtersdata:gt(11)').hide();
    if($('.filter').length>12){
        $("#load_more").show();
    }
	//alert(val);
    $("#"+val).addClass("active_cat");
}
/*slider*/
/*
$('.owl-carousel').owlCarousel({
    stagePadding: 200,
    loop:holding_slider_loop,
    margin:30,
    nav:true,
    dots: true,
    dotsData: true,
    navText: false,
    navContainer:'.owlMyNav',
    dotsContainer: '.owl-dots-container',
    responsive:{
        0:{
            items:1,
            stagePadding: 0
        },
        450:{
            items:murjan_holding_slider,
            stagePadding: 0
        },
        600:{
            items:murjan_holding_slider,
            stagePadding: 0
        },
        1000:{
            items:murjan_holding_slider,
            stagePadding: 0
        },
        1199:{
            items:murjan_holding_slider,
            stagePadding: 0
        },
        1440:{
            items:murjan_holding_slider,
        },
        1920:{
            items:murjan_holding_slider,
        }
    }

})*/



$(".searcIcon").click(function(){
  $(".searchBox").fadeIn();
});

$(".crosIcon").click(function(){
  $(".searchBox").fadeOut();
});

function activate_tab_and_scroll(id) {

    $('html,body').animate({
            scrollTop: $(".sectors-section").offset().top
        }, 'fast');

    // $('#nav-realestate-tab-' + id).trigger('click');

    var someTabTriggerEl = document.querySelector('#nav-realestate-tab-' + id);
    var tab = new bootstrap.Tab(someTabTriggerEl);
    tab.show();

    $('.sectors_nav').removeClass('active');
    $('#nav-realestate-tab-' + id).addClass('active');

    $(".sectors-section").removeClass('real-estate-bg');
    if ($('#nav-realestate-tab-' + id).data('show-border') == 1) {
        $(".sectors-section").addClass('real-estate-bg');
    }

    owl_carousel_sectors_nav.trigger('to.owl.carousel', [$('#nav-realestate-tab-' + id).data('index'), 300]);
}

function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

$(document).ready(function() {
    if ($('.sectors_nav').length) {
        // $('.sectors_nav')[0].click();
    }
});