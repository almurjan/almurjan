// ************/TopNavigation/********
(function($) { "use strict";

    $(function() {
        var header = $(".start-style");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 10) {
                header.removeClass('start-style').addClass("scroll-on");
            } else {
                header.removeClass("scroll-on").addClass('start-style');
            }
        });
    });

    //Animation

    $(document).ready(function() {
        $('body.hero-anime').removeClass('hero-anime');
    });

    //Menu On Hover

    $('body').on('mouseenter mouseleave','.nav-item',function(e){
        if ($(window).width() > 750) {
            var _d=$(e.target).closest('.nav-item');_d.addClass('show');
            setTimeout(function(){
                _d[_d.is(':hover')?'addClass':'removeClass']('show');
            },1);
        }
    });

    //Menu On Hover

    $('body').on('mouseenter mouseleave','.locationbtn',function(e){
        if ($(window).width() > 320) {
            var _d=$(e.target).closest('.locationbtn');_d.addClass('DropDownShow');
            setTimeout(function(){
                _d[_d.is(':hover')?'addClass':'removeClass']('DropDownShow');
            },1);
        }
    });
    //Switch light/dark

    $("#switch").on('click', function () {
        if ($("body").hasClass("dark")) {
            $("body").removeClass("dark");
            $("#switch").removeClass("switched");
        }
        else {
            $("body").addClass("dark");
            $("#switch").addClass("switched");
        }
    });

})(jQuery);
///////////////////////////////////////////////////////////

$('.BornHS__Input').change(function(){
    if($(this).is(":checked")) {
        $('.BornHS__Wrapper').addClass("BornHS__Blur");
    } else {
        $('.BornHS__Wrapper').removeClass("BornHS__Blur");
    }
});

//////////////////////////////////////////////////////////
if(tpl!='home'){

 $('#navSlider').owlCarousel({
         nav:false,
         dots: false,
         autoWidth:true,
         rtl: true,
         margin: 33,
         responsive:{
            0:{
                //items:1
            },
            600:{
                //items:2
            },
            767:{
                //items:3
                nav:true,
            },
            1000:{
                //items:4
                nav:true,
            },
            1200:{
                //items:5
            },
            1500:{
                //items:5
            }
         }
    })

}else{
    var murjan_holding_slider = 3;
$('#hold_logo_slider_home').owlCarousel({
    stagePadding: 200,
    loop:holding_slider_loop,
    margin:30,
    nav:true,
    dots: true,
    dotsData: true,
    navText: false,
    navContainer:'.owlMyNav',
    dotsContainer: '.owl-dots-container',
    rtl: true,
    responsive:{
        0:{
            items:1,
            stagePadding: 0
        },
        450:{
            items:2,
            stagePadding: 0
        },
        600:{
            items:3,
            stagePadding: 0
        },
        1000:{
            items:murjan_holding_slider,
            stagePadding: 0
        },
        1199:{
            items:murjan_holding_slider,
            stagePadding: 0
        },
        1440:{
            items:murjan_holding_slider,
        },
        1920:{
            items:murjan_holding_slider,
        }
    }

})
}

//////////////////////////////////////////////////

$('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');

    $({
        countNum: $this.text()
    }).animate({
            countNum: countTo
        },

        {
            duration: 500,
            easing: 'linear',
            step: function() {
                $this.text(commaSeparateNumber(Math.floor(this.countNum)));
            },
            complete: function() {
                $this.text(commaSeparateNumber(this.countNum));
                //alert('finished');
            }
        }
    );

});

function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}
//////////////////////////////////
// scroll js top to bottom

$('#skip_to_section, #skip_to_section1').click(function() {
    $('html, body').animate({
        scrollTop: $('#About-Al-Murjan').offset().top
    }, 1000);
    return false;
});
///////////////////////////////////////////////
$(document).ready(function(){
	
    $(".filter-button").click(function(event){
		event.preventDefault();
        var value = $(this).attr('data-filter');
        var cat_title = $(this).attr('data-cat-title');
        var cat_desc = $(this).attr('data-cat-desc');

        $('.category_title_area').hide();
        $('.category_title_area').find('h2').text('');

        $('.category_desc_area').hide();
        $('.category_desc_area').find('.col-12').html('');

		//alert(value);
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
            $(".filtersdata").slice(0, 12).show();
            $('.filtersdata:gt(11)').hide();
            if($('.filter').length>12){
                $("#load_more").show();
            }
        }
        else
        {

            if (cat_desc !== '') {
                $('.category_title_area').find('h2').text(cat_title);
                $('.category_title_area').show();

                $('.category_desc_area').find('.col-12').html(cat_desc);
                $('.category_desc_area').show();
            }

//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            if($('.'+value).length>12){
                $("#load_more").show();
            }else{
                $("#load_more").hide();
            }

        }
        if ($(".filter-button").removeClass("active")) {
            $(this).removeClass("active");
        }
        $(this).addClass("active");
        $(".filter-button").not(this).removeClass('active_cat');
        //alert(val);
        $("#"+value).addClass("active_cat");
        return false;
    });


});
///////////////////////////////////////////////////////////
$('#carousel-1').carousel({
    // Amount of time to delay between cycling slide, If false, no cycle
    interval: 3000,

    // Pauses slide on mouse enter and resumes on mouseleave.
    pause: "hover",

    // Whether carousel should cycle continuously or have hard stops.
    wrap: true,

    // Whether the carousel should react to keyboard events.
    keyboard: true
});