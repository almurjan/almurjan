$(document).ready(function() {
	
	var theLanguage = $('html').attr('lang');

	if(theLanguage == 'ar'){
		$("input[type=\"file\"]").parent().parent().siblings().append('<i class="fa fa-exclamation-triangle" aria-hidden="true"><span>تنسيق غير معتمد.</span></i>');		
	}
	else{
	$("input[type=\"file\"]").parent().parent().siblings().append('<i class="fa fa-exclamation-triangle" aria-hidden="true"><span>Unsupported Format.</span></i>');
    }
    	
    $("input[type=\"file\"]").change(function(){
    	var parent = $(this).parent().parent().siblings();
    	var fileName = $(this).parent().parent().siblings().children().val($(this).val().replace(/C:\\fakepath\\/i, ''));
    	var fileExt = fileName.val();
    	var valid_extensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i; 
    	if(!valid_extensions.test(fileExt))
		{ 
			parent.find('.fa').css({'visibility': 'visible', 'opacity': 1});
		}
		else{
			parent.find('.fa').css({'visibility': 'hidden', 'opacity': 0});
		}

	});

	$('.search-icon').on('click', function(event) {
		$('.searh-form-header').toggleClass('active');
		setTimeout(function(){
      	$('.searh-form-header input.form-control').focus();
	  	}, 200);
	});

	$('form.searh-form-header').on('submit', function(){
		if(!$(this).find('input[name="s"]').val()){
			return false;
		}
	});

	/*matchHeight*/
	$('.solution-inner-wrap .solution-title + p').matchHeight();


	// initSiteMap: function () {
        $(".site-map .site-btn").click(function () {
            $(this).toggleClass('active');
            $('.site-info').toggleClass('active');
            $("#footerSitemap").slideToggle({
		      direction: "up"
		    }, 300);

			$('.site-info').ScrollTo({
				duration: 300
			});
            // $("html, body").animate({ scrollTop: $(document).height() }, "slow");
            // return false;

        });

        $( "#datepicker" ).datepicker();
    // },


	$('a[href="#medical-location"]').on('shown.bs.tab', function()
    {
        google.maps.event.trigger(map2, 'resize');
    });

	$('input[type="radio"]').addClass('rd-bx');

	$('span.check-bg-one').click(function() {
	   if($('.rd-bx').is(':checked')) { 
	   		$(this).addClass('diff');
	   		$('span.check-bg-two').removeClass('diff');
	    }
	});

	$('span.check-bg-two').click(function() {
	   if($('.rd-bx').is(':checked')) { 
	   		$(this).addClass('diff');
	   		$('span.check-bg-one').removeClass('diff');
	    }
	});

	
	$('footer .menu-item-214').on('click', function(){
		$('header').ScrollTo({
		    duration: 1000,
		    callback: function(){
		        $('#req_quote').trigger('click');
		    }
		});
	})

	$('footer .menu-item-213').on('click', function(){
		//$('.site-map.text-center a.btn.site-btn').trigger('click');
		$('.site-map').ScrollTo({
		    duration: 500,
		    callback: function(){
		        $('.site-map.text-center a.btn.site-btn').trigger('click');
		    }
		});
	});

	$('.faq-title a').click(function() {
		$(this).parent().siblings().slideToggle(300);
		$(this).toggleClass('active');
	});

	function isValidEmailAddress(email) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(email);
    };

	$('button.subscribe').on('click', function(eve){
		eve.preventDefault();
		alert = function(){};
		var email = $('input[type="email"]').val();
      	/*if(!isValidEmailAddress(email) || email == ''){
           $('input.subscribe-email').addClass('invalid-email');
        } */       
	})


	$('img:not([alt])').attr('alt', 'Fallback Alt Text');

	$('ul.sub-menu').wrapAll('carousel-inner');

	var items = $('#menu-item-61 ul.sub-menu > li');
      for (var i = 0; i < items.length; i += 4) {
        items.slice(i, i + 4).wrapAll('<div class="item"></div>');
  	};

	var indItems = $('#menu-item-65 ul.sub-menu > li');
      for (var i = 0; i < indItems.length; i += 4) {
        indItems.slice(i, i + 4).wrapAll('<div class="item"></div>');
  	};


  	$('ul.sub-menu > .item:first-of-type').addClass('active');
    
  	$('#menu-item-61 ul.sub-menu').wrapInner('<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false"></div>');

  	$('#menu-item-65 ul.sub-menu').wrapInner('<div id="carousel-nav-menu" class="carousel slide" data-ride="carousel" data-interval="false"></div>');

  	$('ul.sub-menu .carousel').wrapInner('<div class="carousel-inner" role="listbox"> </div>');

  	$('#menu-item-61 ul.sub-menu .carousel').append('<a class="carousel-control left" href="#carousel-example-generic" role="button" data-slide="prev"><span class="fa fa-angle-left" aria-hidden="true"></span></a><a class="carousel-control right" href="#carousel-example-generic" role="button" data-slide="next"><span class="fa fa-angle-right" aria-hidden="true"></span></a>');
	  
  	$('#menu-item-65 ul.sub-menu .carousel').append('<a class="carousel-control left" href="#carousel-nav-menu" role="button" data-slide="prev"><span class="fa fa-angle-left" aria-hidden="true"></span></a><a class="carousel-control right" href="#carousel-nav-menu" role="button" data-slide="next"><span class="fa fa-angle-right" aria-hidden="true"></span></a>');

	$('ul.sub-menu').append('<i class="fa fa-caret-down" aria-hidden="true"></i>');

	$li_length = $( "#menu-item-61 ul.sub-menu li" ).size();

	$li_length_ind = $( "#menu-item-65 ul.sub-menu li" ).size();

	if($li_length < 5){
		$('#menu-item-61 ul.sub-menu .carousel-control').addClass('hide');		
	}

	if($li_length_ind < 5){
		$('#menu-item-65 ul.sub-menu .carousel-control').addClass('hide');		
	}

	$(document).ready(function() {
		$(".fancybox").fancybox({
			openEffect	: 'none',
			closeEffect	: 'none'
		});
	});



	$('button[type="reset"]').on('click', function(){
		$('.star-rating-live').removeClass('star-rating-on');
	})


	$('.radiobox').on('click', function(){
	if($(".check-bg-two").hasClass("diff")) {
  		$('select#incursus').removeClass('not-shown');
  		$('select#cursus').addClass('not-shown');
	}
	if($(".check-bg-one").hasClass("diff")) {
  		$('select#cursus').removeClass('not-shown');
  		$('select#incursus').addClass('not-shown');
	}

	})

	/*zopim chat status message*/


	$zopim(function(){
	$zopim.livechat.setOnStatus(bubble);
	function bubble(status){
		if(status=='online')
		{
			if(theLanguage == 'ar'){
				$('div.fixed-side-menu ul li:last-of-type a span').html('نحن اون لاين');
			}
			else{
				$('div.fixed-side-menu ul li:last-of-type a span').html('We\'re Online');
			}

			$('.zopim').show();

		}
		else if(status=='away')
		{
			if(theLanguage == 'ar'){
				$('div.fixed-side-menu ul li:last-of-type a span').html('نحن اون لاين');
			}
			else{
				$('div.fixed-side-menu ul li:last-of-type a span').html('We\'re Online');
			}

			$('.zopim').show();

		}
		else if(status=='offline')
		{
			if(theLanguage == 'ar'){
				$('div.fixed-side-menu ul li:last-of-type a span').html('غير متصل حاليا');
			}
			else{
				$('div.fixed-side-menu ul li:last-of-type a span').html('We\'re Offline');
			}

			$('.zopim').hide();

			$('.overlay.jx_ui_Widget').on('click', function(){
				$('.zopim').hide();
			})
		}
	}
	});

	$('select#cursus').on('change', function(){
		$(this).removeClass('wpcf7-not-valid');
		var optVal = $('option:selected', this).attr('hiddentag');
		$("div#wpcf7-f249-p172-o3").removeClass('shroud');
		$("div#wpcf7-f459-p172-o3").removeClass('shroud');
		//$('span.check-bg-one').addClass('diff');
		/*$('select#cursus').val(optVal);*/
		if(optVal == 24 || optVal== 19 || optVal == 364 || optVal == 76){
			$("div#wpcf7-f249-p172-o3").hide();
			$("div#wpcf7-f459-p172-o3").hide();
		}
		else{
			$("div#wpcf7-f249-p172-o3").show();			
			$("div#wpcf7-f459-p172-o3").show();			
		}
		if(optVal == 24){
			$('select#cursus').val();
		}
		if(optVal == 19){
			$('select#cursus').val();
		}
		if(optVal == 364){
			$('select#cursus').val();
		}
		if(optVal == 76){
			$('select#cursus').val();
		}
		$('.motor-cf7').toggleClass('is-visible', optVal == 24);
		$('.liability-cf7').toggleClass('is-visible', optVal == 19);
		$('.fire-cf7').toggleClass('is-visible', optVal == 364);
		$('.marine-cf7').toggleClass('is-visible', optVal == 76);
		$('.is-visible').removeClass('shroud');

		/*$('select#incursus').val('-99');*/

	});

	$('select#incursus').on('change', function(){
		$(this).removeClass('wpcf7-not-valid');
		var inoptVal = $('option:selected', this).attr('hiddentag');
		$('.liability-cf7, .fire-cf7, .marine-cf7').removeClass('is-visible');
		if(inoptVal == 610){
			$('.motor-cf7').removeClass('shroud');
			$("div#wpcf7-f249-p172-o3").addClass('shroud');
			$("div#wpcf7-f459-p172-o3").addClass('shroud');
			$('.motor-cf7').addClass('is-visible');
			/*$('select#incursus').val('610');*/
		}
		else{
			$("div#wpcf7-f249-p172-o3").removeClass('shroud');
			$("div#wpcf7-f459-p172-o3").removeClass('shroud');
			$('.motor-cf7').removeClass('is-visible');
		}
		$("div#wpcf7-f249-p172-o3").show();
		$("div#wpcf7-f459-p172-o3").show();
		//$('span.check-bg-two').addClass('diff');
		if(!inoptVal == 610){
			$('.is-visible').addClass('shroud');
		}
		/*$('select#cursus').val('-99');*/
	})

	$('.claim-form-wrap button[type="submit"]').on('click', function(){
		var radioinVal = $('select#incursus option:selected').val();
		var radioVal = $('select#cursus option:selected').val();
		if(radioVal == '' && radioinVal == ''){
			$('select#incrsus, select#cursus').addClass('wpcf7-not-valid');
		}else{
			$('select#incrsus, select#cursus').removeClass('wpcf7-not-valid');
		}
	})

});